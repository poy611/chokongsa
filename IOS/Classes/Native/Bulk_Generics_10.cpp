﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2045451540;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3787551078;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t4040850731;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t938164665;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t4230795212;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t4230808090;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3312854530;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t1083620227;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t132831245;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t374511678;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t835879620;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Director.Playable>
struct List_1_t1439018250;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1433993036;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1187093738;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1317283468;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24013503581MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21049882445MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_101070088MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21220774118MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487883MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_UInt3224667981MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21919813716MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp4145961110.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_300380471MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078.h"
#include "mscorlib_System_ObjectDisposedException1794727681MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException1794727681.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4060523501.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4060523501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4040850731.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "PlayMaker_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957837435.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957837435MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen938164665.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "Newtonsoft.Json_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1864657040.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1864657040MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1844984270.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250467982.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250467982MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250480860.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4250480860MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230808090.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3332527299.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3332527299MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3312854529.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1103292997.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1103292997MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1083620227.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen132831245.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen374511678.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1986712010.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1986712010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1967039240.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat855552390.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat855552390MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen835879620.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1458691020.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1458691020MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1439018250.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playab70832698.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1453665806.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1453665806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1433993036.h"
#include "UnityEngine_UnityEngine_UICharInfo65807484.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1206766508.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1206766508MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1187093738.h"
#include "UnityEngine_UnityEngine_UILineInfo4113875482.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1336956238.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1336956238MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1317283468.h"
#include "UnityEngine_UnityEngine_UIVertex4244065212.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1374957591.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1374957591MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1355284821.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m676088996_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1797699333((KeyValuePair_2_t3076810564 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1872035973((KeyValuePair_2_t3076810564 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m676088996_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	KeyValuePair_2__ctor_m676088996(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1851180740_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1851180740_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1851180740(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1797699333_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1797699333_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	KeyValuePair_2_set_Key_m1797699333(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m454436868_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m454436868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	return KeyValuePair_2_get_Value_m454436868(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1872035973_gshared (KeyValuePair_2_t3076810564 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1872035973_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	KeyValuePair_2_set_Value_m1872035973(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m464210813_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m464210813_gshared (KeyValuePair_2_t3076810564 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m464210813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1851180740((KeyValuePair_2_t3076810564 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1851180740((KeyValuePair_2_t3076810564 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		int32_t L_9 = KeyValuePair_2_get_Value_m454436868((KeyValuePair_2_t3076810564 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
	}
	{
		int32_t L_10 = KeyValuePair_2_get_Value_m454436868((KeyValuePair_2_t3076810564 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_10;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_11);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_14 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		ArrayElementTypeCheck (L_14, _stringLiteral93);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m464210813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3076810564 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3076810564 *>(__this + 1);
	return KeyValuePair_2_ToString_m464210813(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1883880326_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = ___key0;
		KeyValuePair_2_set_Key_m362021603((KeyValuePair_2_t4013503581 *)__this, (TypeNameKey_t2971844791 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1218260323((KeyValuePair_2_t4013503581 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1883880326_AdjustorThunk (Il2CppObject * __this, TypeNameKey_t2971844791  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2__ctor_m1883880326(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Key()
extern "C"  TypeNameKey_t2971844791  KeyValuePair_2_get_Key_m2652238242_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = (TypeNameKey_t2971844791 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeNameKey_t2971844791  KeyValuePair_2_get_Key_m2652238242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2652238242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m362021603_gshared (KeyValuePair_2_t4013503581 * __this, TypeNameKey_t2971844791  ___value0, const MethodInfo* method)
{
	{
		TypeNameKey_t2971844791  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m362021603_AdjustorThunk (Il2CppObject * __this, TypeNameKey_t2971844791  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2_set_Key_m362021603(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3736459618_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3736459618_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3736459618(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1218260323_gshared (KeyValuePair_2_t4013503581 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1218260323_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	KeyValuePair_2_set_Value_m1218260323(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2354625887_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2354625887_gshared (KeyValuePair_2_t4013503581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2354625887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeNameKey_t2971844791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		TypeNameKey_t2971844791  L_2 = KeyValuePair_2_get_Key_m2652238242((KeyValuePair_2_t4013503581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TypeNameKey_t2971844791  L_3 = KeyValuePair_2_get_Key_m2652238242((KeyValuePair_2_t4013503581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TypeNameKey_t2971844791 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3736459618((KeyValuePair_2_t4013503581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m3736459618((KeyValuePair_2_t4013503581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2354625887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4013503581 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4013503581 *>(__this + 1);
	return KeyValuePair_2_ToString_m2354625887(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3343032681_gshared (KeyValuePair_2_t2893931855 * __this, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ResolverContractKey_t473801005  L_0 = ___key0;
		KeyValuePair_2_set_Key_m2701507360((KeyValuePair_2_t2893931855 *)__this, (ResolverContractKey_t473801005 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m3620634400((KeyValuePair_2_t2893931855 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3343032681_AdjustorThunk (Il2CppObject * __this, ResolverContractKey_t473801005  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	KeyValuePair_2__ctor_m3343032681(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Key()
extern "C"  ResolverContractKey_t473801005  KeyValuePair_2_get_Key_m546969567_gshared (KeyValuePair_2_t2893931855 * __this, const MethodInfo* method)
{
	{
		ResolverContractKey_t473801005  L_0 = (ResolverContractKey_t473801005 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  ResolverContractKey_t473801005  KeyValuePair_2_get_Key_m546969567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	return KeyValuePair_2_get_Key_m546969567(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2701507360_gshared (KeyValuePair_2_t2893931855 * __this, ResolverContractKey_t473801005  ___value0, const MethodInfo* method)
{
	{
		ResolverContractKey_t473801005  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2701507360_AdjustorThunk (Il2CppObject * __this, ResolverContractKey_t473801005  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	KeyValuePair_2_set_Key_m2701507360(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3270771651_gshared (KeyValuePair_2_t2893931855 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3270771651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3270771651(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3620634400_gshared (KeyValuePair_2_t2893931855 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3620634400_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	KeyValuePair_2_set_Value_m3620634400(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2151616104_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2151616104_gshared (KeyValuePair_2_t2893931855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2151616104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResolverContractKey_t473801005  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		ResolverContractKey_t473801005  L_2 = KeyValuePair_2_get_Key_m546969567((KeyValuePair_2_t2893931855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		ResolverContractKey_t473801005  L_3 = KeyValuePair_2_get_Key_m546969567((KeyValuePair_2_t2893931855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (ResolverContractKey_t473801005 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3270771651((KeyValuePair_2_t2893931855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m3270771651((KeyValuePair_2_t2893931855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2151616104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2893931855 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2893931855 *>(__this + 1);
	return KeyValuePair_2_ToString_m2151616104(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2411535999_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = ___key0;
		KeyValuePair_2_set_Key_m1634396234((KeyValuePair_2_t2815902970 *)__this, (TypeConvertKey_t866134174 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1512376138((KeyValuePair_2_t2815902970 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2411535999_AdjustorThunk (Il2CppObject * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2__ctor_m2411535999(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  TypeConvertKey_t866134174  KeyValuePair_2_get_Key_m1364625929_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = (TypeConvertKey_t866134174 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  TypeConvertKey_t866134174  KeyValuePair_2_get_Key_m1364625929_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1364625929(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1634396234_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___value0, const MethodInfo* method)
{
	{
		TypeConvertKey_t866134174  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1634396234_AdjustorThunk (Il2CppObject * __this, TypeConvertKey_t866134174  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2_set_Key_m1634396234(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1894627949_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1894627949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1894627949(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1512376138_gshared (KeyValuePair_2_t2815902970 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1512376138_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	KeyValuePair_2_set_Value_m1512376138(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m52328446_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m52328446_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m52328446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TypeConvertKey_t866134174  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		TypeConvertKey_t866134174  L_2 = KeyValuePair_2_get_Key_m1364625929((KeyValuePair_2_t2815902970 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		TypeConvertKey_t866134174  L_3 = KeyValuePair_2_get_Key_m1364625929((KeyValuePair_2_t2815902970 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (TypeConvertKey_t866134174 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1894627949((KeyValuePair_2_t2815902970 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m1894627949((KeyValuePair_2_t2815902970 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m52328446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2815902970 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2815902970 *>(__this + 1);
	return KeyValuePair_2_ToString_m52328446(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1072433347_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3084624774((KeyValuePair_2_t1049882445 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1192084614((KeyValuePair_2_t1049882445 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1072433347_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2__ctor_m1072433347(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1702622021_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1702622021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1702622021(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3084624774_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3084624774_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2_set_Key_m3084624774(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3723762473_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3723762473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3723762473(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1192084614_gshared (KeyValuePair_2_t1049882445 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1192084614_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	KeyValuePair_2_set_Value_m1192084614(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2441409026_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2441409026_gshared (KeyValuePair_2_t1049882445 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2441409026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1702622021((KeyValuePair_2_t1049882445 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1702622021((KeyValuePair_2_t1049882445 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3723762473((KeyValuePair_2_t1049882445 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3723762473((KeyValuePair_2_t1049882445 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2441409026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1049882445 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1049882445 *>(__this + 1);
	return KeyValuePair_2_ToString_m2441409026(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m11197230_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m4229413435((KeyValuePair_2_t4066860316 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1296398523((KeyValuePair_2_t4066860316 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m11197230_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2__ctor_m11197230(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Key_m494458106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4229413435_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4229413435_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Key_m4229413435(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1563175098(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1296398523_gshared (KeyValuePair_2_t4066860316 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1296398523_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Value_m1296398523(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m491888647_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m491888647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m494458106((KeyValuePair_2_t4066860316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m494458106((KeyValuePair_2_t4066860316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m1563175098((KeyValuePair_2_t4066860316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1563175098((KeyValuePair_2_t4066860316 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_ToString_m491888647(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3368609491_gshared (KeyValuePair_2_t101070088 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1825586038((KeyValuePair_2_t101070088 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m3205817462((KeyValuePair_2_t101070088 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3368609491_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	KeyValuePair_2__ctor_m3368609491(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3321873205_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3321873205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3321873205(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1825586038_gshared (KeyValuePair_2_t101070088 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1825586038_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	KeyValuePair_2_set_Key_m1825586038(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2372325657_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2372325657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2372325657(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3205817462_gshared (KeyValuePair_2_t101070088 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3205817462_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	KeyValuePair_2_set_Value_m3205817462(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1079106322_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1079106322_gshared (KeyValuePair_2_t101070088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1079106322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3321873205((KeyValuePair_2_t101070088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3321873205((KeyValuePair_2_t101070088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m2372325657((KeyValuePair_2_t101070088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m2372325657((KeyValuePair_2_t101070088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1079106322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t101070088 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t101070088 *>(__this + 1);
	return KeyValuePair_2_ToString_m1079106322(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m876371164_gshared (KeyValuePair_2_t1220774118 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1955292109((KeyValuePair_2_t1220774118 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2848328013((KeyValuePair_2_t1220774118 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m876371164_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	KeyValuePair_2__ctor_m876371164(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1333639052_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1333639052_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1333639052(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1955292109_gshared (KeyValuePair_2_t1220774118 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1955292109_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	KeyValuePair_2_set_Key_m1955292109(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3015219660_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3015219660_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3015219660(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2848328013_gshared (KeyValuePair_2_t1220774118 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2848328013_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	KeyValuePair_2_set_Value_m2848328013(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.ReadType>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1519115317_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1519115317_gshared (KeyValuePair_2_t1220774118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1519115317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1333639052((KeyValuePair_2_t1220774118 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1333639052((KeyValuePair_2_t1220774118 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3015219660((KeyValuePair_2_t1220774118 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3015219660((KeyValuePair_2_t1220774118 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1519115317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1220774118 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1220774118 *>(__this + 1);
	return KeyValuePair_2_ToString_m1519115317(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m32323851_gshared (KeyValuePair_2_t666182096 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2246010430((KeyValuePair_2_t666182096 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m327248702((KeyValuePair_2_t666182096 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m32323851_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2__ctor_m32323851(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m509596157_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m509596157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_get_Key_m509596157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2246010430_gshared (KeyValuePair_2_t666182096 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2246010430_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2_set_Key_m2246010430(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1901960161_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1901960161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1901960161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m327248702_gshared (KeyValuePair_2_t666182096 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m327248702_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	KeyValuePair_2_set_Value_m327248702(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3189658186_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3189658186_gshared (KeyValuePair_2_t666182096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3189658186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m509596157((KeyValuePair_2_t666182096 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m509596157((KeyValuePair_2_t666182096 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m1901960161((KeyValuePair_2_t666182096 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m1901960161((KeyValuePair_2_t666182096 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3189658186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t666182096 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t666182096 *>(__this + 1);
	return KeyValuePair_2_ToString_m3189658186(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m853598568_gshared (KeyValuePair_2_t203144266 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m4226889665((KeyValuePair_2_t203144266 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2709046081((KeyValuePair_2_t203144266 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m853598568_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	KeyValuePair_2__ctor_m853598568(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m324263168_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m324263168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	return KeyValuePair_2_get_Key_m324263168(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4226889665_gshared (KeyValuePair_2_t203144266 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4226889665_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	KeyValuePair_2_set_Key_m4226889665(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m552188452_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m552188452_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	return KeyValuePair_2_get_Value_m552188452(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2709046081_gshared (KeyValuePair_2_t203144266 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2709046081_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	KeyValuePair_2_set_Value_m2709046081(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m4212323239_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4212323239_gshared (KeyValuePair_2_t203144266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4212323239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m324263168((KeyValuePair_2_t203144266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m324263168((KeyValuePair_2_t203144266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m552188452((KeyValuePair_2_t203144266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m552188452((KeyValuePair_2_t203144266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4212323239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t203144266 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t203144266 *>(__this + 1);
	return KeyValuePair_2_ToString_m4212323239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2040323320_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1751794225((KeyValuePair_2_t2545618620 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m3162969521((KeyValuePair_2_t2545618620 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2040323320_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2__ctor_m2040323320(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Key_m700889072(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1751794225_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1751794225_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Key_m1751794225(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3809014448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3162969521_gshared (KeyValuePair_2_t2545618620 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3162969521_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Value_m3162969521(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3396952209_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3396952209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m700889072((KeyValuePair_2_t2545618620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m700889072((KeyValuePair_2_t2545618620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m3809014448((KeyValuePair_2_t2545618620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m3809014448((KeyValuePair_2_t2545618620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_ToString_m3396952209(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2730552978_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1188304983((KeyValuePair_2_t3222658402 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m137193687((KeyValuePair_2_t3222658402 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2730552978_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2__ctor_m2730552978(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4285571350(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1188304983_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1188304983_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Key_m1188304983(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2690735574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m137193687_gshared (KeyValuePair_2_t3222658402 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m137193687_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Value_m137193687(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2052282219_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2052282219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4285571350((KeyValuePair_2_t3222658402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m4285571350((KeyValuePair_2_t3222658402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m2690735574((KeyValuePair_2_t3222658402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m2690735574((KeyValuePair_2_t3222658402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_ToString_m2052282219(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4168265535_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1278074762((KeyValuePair_2_t1944668977 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2954518154((KeyValuePair_2_t1944668977 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4168265535_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2__ctor_m4168265535(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2940899609_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2940899609_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2940899609(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1278074762_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1278074762_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Key_m1278074762(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4250204908_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4250204908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4250204908(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2954518154_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2954518154_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Value_m2954518154(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1313859518_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1313859518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2940899609((KeyValuePair_2_t1944668977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m4250204908((KeyValuePair_2_t1944668977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m4250204908((KeyValuePair_2_t1944668977 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_ToString_m1313859518(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2908028374_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m662474387((KeyValuePair_2_t2065771578 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		KeyValuePair_2_set_Value_m3606602003((KeyValuePair_2_t2065771578 *)__this, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2908028374_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2__ctor_m2908028374(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m975404242_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m975404242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_get_Key_m975404242(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m662474387_gshared (KeyValuePair_2_t2065771578 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m662474387_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2_set_Key_m662474387(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m2222463222_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m2222463222_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2222463222(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3606602003_gshared (KeyValuePair_2_t2065771578 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3606602003_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	KeyValuePair_2_set_Value_m3606602003(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3615079765_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3615079765_gshared (KeyValuePair_2_t2065771578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3615079765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m975404242((KeyValuePair_2_t2065771578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m975404242((KeyValuePair_2_t2065771578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		float L_8 = KeyValuePair_2_get_Value_m2222463222((KeyValuePair_2_t2065771578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = KeyValuePair_2_get_Value_m2222463222((KeyValuePair_2_t2065771578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m5736032((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3615079765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2065771578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2065771578 *>(__this + 1);
	return KeyValuePair_2_ToString_m3615079765(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1597091749_gshared (KeyValuePair_2_t2093487883 * __this, Il2CppObject * ___key0, uint32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1019066212((KeyValuePair_2_t2093487883 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2733029732((KeyValuePair_2_t2093487883 *)__this, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1597091749_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, uint32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	KeyValuePair_2__ctor_m1597091749(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1874048547_gshared (KeyValuePair_2_t2093487883 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1874048547_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1874048547(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1019066212_gshared (KeyValuePair_2_t2093487883 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1019066212_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	KeyValuePair_2_set_Key_m1019066212(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::get_Value()
extern "C"  uint32_t KeyValuePair_2_get_Value_m2531213831_gshared (KeyValuePair_2_t2093487883 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  uint32_t KeyValuePair_2_get_Value_m2531213831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2531213831(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2733029732_gshared (KeyValuePair_2_t2093487883 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2733029732_AdjustorThunk (Il2CppObject * __this, uint32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	KeyValuePair_2_set_Value_m2733029732(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1408282148_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1408282148_gshared (KeyValuePair_2_t2093487883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1408282148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	uint32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1874048547((KeyValuePair_2_t2093487883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1874048547((KeyValuePair_2_t2093487883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		uint32_t L_8 = KeyValuePair_2_get_Value_m2531213831((KeyValuePair_2_t2093487883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint32_t L_9 = KeyValuePair_2_get_Value_m2531213831((KeyValuePair_2_t2093487883 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint32_t)L_9;
		String_t* L_10 = UInt32_ToString_m904380337((uint32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1408282148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487883 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487883 *>(__this + 1);
	return KeyValuePair_2_ToString_m1408282148(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3693433134_gshared (KeyValuePair_2_t3443564286 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m4016800827((KeyValuePair_2_t3443564286 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t1374744384  L_1 = ___value1;
		KeyValuePair_2_set_Value_m2107916987((KeyValuePair_2_t3443564286 *)__this, (RaycastHit2D_t1374744384 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3693433134_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	KeyValuePair_2__ctor_m3693433134(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2490056442_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2490056442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2490056442(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4016800827_gshared (KeyValuePair_2_t3443564286 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4016800827_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	KeyValuePair_2_set_Key_m4016800827(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Value()
extern "C"  RaycastHit2D_t1374744384  KeyValuePair_2_get_Value_m1935801786_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t1374744384  L_0 = (RaycastHit2D_t1374744384 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  RaycastHit2D_t1374744384  KeyValuePair_2_get_Value_m1935801786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1935801786(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2107916987_gshared (KeyValuePair_2_t3443564286 * __this, RaycastHit2D_t1374744384  ___value0, const MethodInfo* method)
{
	{
		RaycastHit2D_t1374744384  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2107916987_AdjustorThunk (Il2CppObject * __this, RaycastHit2D_t1374744384  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	KeyValuePair_2_set_Value_m2107916987(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3008316039_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3008316039_gshared (KeyValuePair_2_t3443564286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3008316039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	RaycastHit2D_t1374744384  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2490056442((KeyValuePair_2_t3443564286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2490056442((KeyValuePair_2_t3443564286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		RaycastHit2D_t1374744384  L_8 = KeyValuePair_2_get_Value_m1935801786((KeyValuePair_2_t3443564286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		RaycastHit2D_t1374744384  L_9 = KeyValuePair_2_get_Value_m1935801786((KeyValuePair_2_t3443564286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (RaycastHit2D_t1374744384 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3008316039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3443564286 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3443564286 *>(__this + 1);
	return KeyValuePair_2_ToString_m3008316039(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2418427527_gshared (KeyValuePair_2_t1919813716 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m979032898((KeyValuePair_2_t1919813716 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2205335106((KeyValuePair_2_t1919813716 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2418427527_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2__ctor_m2418427527(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1474304257_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1474304257_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1474304257(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m979032898_gshared (KeyValuePair_2_t1919813716 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m979032898_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2_set_Key_m979032898(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2789648485_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2789648485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2789648485(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2205335106_gshared (KeyValuePair_2_t1919813716 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2205335106_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	KeyValuePair_2_set_Value_m2205335106(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3626653574_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3626653574_gshared (KeyValuePair_2_t1919813716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3626653574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1474304257((KeyValuePair_2_t1919813716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1474304257((KeyValuePair_2_t1919813716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m2789648485((KeyValuePair_2_t1919813716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m2789648485((KeyValuePair_2_t1919813716 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3626653574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1919813716 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1919813716 *>(__this + 1);
	return KeyValuePair_2_ToString_m3626653574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3704436026_gshared (KeyValuePair_2_t300380471 * __this, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___key0;
		KeyValuePair_2_set_Key_m2433763503((KeyValuePair_2_t300380471 *)__this, (HandleRef_t1780819301 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2108271919((KeyValuePair_2_t300380471 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3704436026_AdjustorThunk (Il2CppObject * __this, HandleRef_t1780819301  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	KeyValuePair_2__ctor_m3704436026(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Key()
extern "C"  HandleRef_t1780819301  KeyValuePair_2_get_Key_m2927877486_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = (HandleRef_t1780819301 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  HandleRef_t1780819301  KeyValuePair_2_get_Key_m2927877486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2927877486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2433763503_gshared (KeyValuePair_2_t300380471 * __this, HandleRef_t1780819301  ___value0, const MethodInfo* method)
{
	{
		HandleRef_t1780819301  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2433763503_AdjustorThunk (Il2CppObject * __this, HandleRef_t1780819301  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	KeyValuePair_2_set_Key_m2433763503(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3910513454_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3910513454_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3910513454(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2108271919_gshared (KeyValuePair_2_t300380471 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2108271919_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	KeyValuePair_2_set_Value_m2108271919(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Runtime.InteropServices.HandleRef,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2948487315_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2948487315_gshared (KeyValuePair_2_t300380471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2948487315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandleRef_t1780819301  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		HandleRef_t1780819301  L_2 = KeyValuePair_2_get_Key_m2927877486((KeyValuePair_2_t300380471 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		HandleRef_t1780819301  L_3 = KeyValuePair_2_get_Key_m2927877486((KeyValuePair_2_t300380471 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (HandleRef_t1780819301 )L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3910513454((KeyValuePair_2_t300380471 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m3910513454((KeyValuePair_2_t300380471 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2948487315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t300380471 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t300380471 *>(__this + 1);
	return KeyValuePair_2_ToString_m2948487315(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m857368315_gshared (Enumerator_t3528606100 * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2045451540 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m857368315_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator__ctor_m857368315(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_Current_m1124073047((Enumerator_t3528606100 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1753810300(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4062113552(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1124073047_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1124073047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t3787551078 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t3787551078 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_get_Current_m1124073047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m2358966120_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m2358966120_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2358966120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2045451540 * L_7 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_9 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t3787551078 * L_10 = (LinkedListNode_1_t3787551078 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		LinkedList_1_t2045451540 * L_12 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t3787551078 * L_14 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2358966120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_MoveNext_m2358966120(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m272587367_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m272587367_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m272587367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_list_0((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m272587367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_Dispose_m272587367(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2955457271_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2955457271_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2955457271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3369579448_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m3369579448_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3369579448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared (LinkedList_1_t2045451540 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m3939775124_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t3787551078 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		LinkedList_1_t2045451540 * L_3 = ((  LinkedList_1_t2045451540 * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2045451540 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2045451540 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_AddLast_m4070107716_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t3787551078 *)L_2;
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_back_3();
		LinkedListNode_1_t3787551078 * L_7 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, LinkedListNode_1_t3787551078 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t3787551078 *)L_6, (LinkedListNode_1_t3787551078 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t3787551078 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m361590562_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3484410556_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t3787551078 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_6;
		LinkedListNode_1_t3787551078 * L_7 = V_0;
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m3470139544_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3470139544_gshared (LinkedList_1_t2045451540 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3470139544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t1108656482* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t1108656482* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t1108656482* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t1108656482* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t928607144 * L_14 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t3787551078 * L_15 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_15;
		LinkedListNode_1_t3787551078 * L_16 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t1108656482* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t3787551078 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t3787551078 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t3787551078 * L_23 = (LinkedListNode_1_t3787551078 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_23;
		LinkedListNode_1_t3787551078 * L_24 = V_0;
		LinkedListNode_1_t3787551078 * L_25 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_Find_m2643247334_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t3787551078 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_11;
		LinkedListNode_1_t3787551078 * L_12 = V_0;
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t3528606100  LinkedList_1_GetEnumerator_m3713737734_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3528606100  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m857368315(&L_0, (LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m3974480661_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3974480661_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3974480661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t2185721892 * L_2 = ___info0;
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2185721892 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2185721892 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m3445006959_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t1108656482*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t1108656482* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t1108656482* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2185721892 * L_14 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2185721892 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2185721892 *)L_14, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t2185721892 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3283493303_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		LinkedListNode_1_t3787551078 * L_1 = ((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t3787551078 *)L_1;
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m4034790180_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t3787551078 * L_3 = ___node0;
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_8);
		((  void (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t3787551078 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2573038887_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m1368924491_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_get_First_m3278587786_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_Last()
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_get_Last_m270176030_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * G_B3_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_back_3();
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = ((LinkedListNode_1_t3787551078 *)(NULL));
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m648136130_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t3787551078 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m448391458_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t3787551078 * ___previousNode2, LinkedListNode_1_t3787551078 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t3787551078 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t3787551078 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t3787551078 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3406254942_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t3787551078 *)NULL;
		__this->set_back_3((LinkedListNode_1_t3787551078 *)NULL);
		LinkedListNode_1_t3787551078 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2045451540 * LinkedListNode_1_get_List_m3467110818_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Previous()
extern "C"  LinkedListNode_1_t3787551078 * LinkedListNode_1_get_Previous_m3755155549_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * G_B4_0 = NULL;
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		LinkedList_1_t2045451540 * L_1 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t3787551078 *)__this) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_2)))
		{
			goto IL_0027;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		G_B4_0 = L_3;
		goto IL_0028;
	}

IL_0027:
	{
		G_B4_0 = ((LinkedListNode_1_t3787551078 *)(NULL));
	}

IL_0028:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m702633824_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m58948428_gshared (Enumerator_t4060523501 * __this, List_1_t4040850731 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4040850731 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4040850731 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m58948428_AdjustorThunk (Il2CppObject * __this, List_1_t4040850731 * ___l0, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	Enumerator__ctor_m58948428(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2166798726_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2183322346((Enumerator_t4060523501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2166798726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2166798726(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m341195762_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m341195762_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m341195762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2183322346((Enumerator_t4060523501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m341195762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m341195762(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void Enumerator_Dispose_m82484273_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4040850731 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m82484273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	Enumerator_Dispose_m82484273(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2183322346_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2183322346_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2183322346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4040850731 * L_0 = (List_1_t4040850731 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4060523501  L_1 = (*(Enumerator_t4060523501 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4040850731 * L_7 = (List_1_t4040850731 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2183322346_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	Enumerator_VerifyState_m2183322346(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1548774386_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2183322346((Enumerator_t4060523501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4040850731 * L_2 = (List_1_t4040850731 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4040850731 * L_4 = (List_1_t4040850731 *)__this->get_l_0();
		NullCheck(L_4);
		ParamDataTypeU5BU5D_t3909450202* L_5 = (ParamDataTypeU5BU5D_t3909450202*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1548774386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	return Enumerator_MoveNext_m1548774386(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m689559969_gshared (Enumerator_t4060523501 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m689559969_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4060523501 * _thisAdjusted = reinterpret_cast<Enumerator_t4060523501 *>(__this + 1);
	return Enumerator_get_Current_m689559969(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3119637849_gshared (Enumerator_t957837435 * __this, List_1_t938164665 * ___l0, const MethodInfo* method)
{
	{
		List_1_t938164665 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t938164665 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3119637849_AdjustorThunk (Il2CppObject * __this, List_1_t938164665 * ___l0, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	Enumerator__ctor_m3119637849(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1612151641_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m781543863((Enumerator_t957837435 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1612151641_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1612151641(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m781543863((Enumerator_t957837435 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		JsonPosition_t3864946409  L_2 = (JsonPosition_t3864946409 )__this->get_current_3();
		JsonPosition_t3864946409  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3556448271(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::Dispose()
extern "C"  void Enumerator_Dispose_m1969658238_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t938164665 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1969658238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	Enumerator_Dispose_m1969658238(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m781543863_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m781543863_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m781543863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t938164665 * L_0 = (List_1_t938164665 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t957837435  L_1 = (*(Enumerator_t957837435 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t938164665 * L_7 = (List_1_t938164665 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m781543863_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	Enumerator_VerifyState_m781543863(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2587609597_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m781543863((Enumerator_t957837435 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t938164665 * L_2 = (List_1_t938164665 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t938164665 * L_4 = (List_1_t938164665 *)__this->get_l_0();
		NullCheck(L_4);
		JsonPositionU5BU5D_t3412818772* L_5 = (JsonPositionU5BU5D_t3412818772*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		JsonPosition_t3864946409  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2587609597_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	return Enumerator_MoveNext_m2587609597(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::get_Current()
extern "C"  JsonPosition_t3864946409  Enumerator_get_Current_m957964607_gshared (Enumerator_t957837435 * __this, const MethodInfo* method)
{
	{
		JsonPosition_t3864946409  L_0 = (JsonPosition_t3864946409 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  JsonPosition_t3864946409  Enumerator_get_Current_m957964607_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957837435 * _thisAdjusted = reinterpret_cast<Enumerator_t957837435 *>(__this + 1);
	return Enumerator_get_Current_m957964607(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1283739068_gshared (Enumerator_t1864657040 * __this, List_1_t1844984270 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1844984270 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1844984270 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1283739068_AdjustorThunk (Il2CppObject * __this, List_1_t1844984270 * ___l0, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator__ctor_m1283739068(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m430448918_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m204378458((Enumerator_t1864657040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m430448918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m430448918(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m721140940_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m721140940_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m721140940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m204378458((Enumerator_t1864657040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		bool L_2 = (bool)__this->get_current_3();
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m721140940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m721140940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2662533793_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1844984270 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2662533793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_Dispose_m2662533793(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m204378458_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m204378458_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m204378458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1844984270 * L_0 = (List_1_t1844984270 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1864657040  L_1 = (*(Enumerator_t1864657040 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1844984270 * L_7 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m204378458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	Enumerator_VerifyState_m204378458(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4018573382_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m204378458((Enumerator_t1864657040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1844984270 * L_2 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1844984270 * L_4 = (List_1_t1844984270 *)__this->get_l_0();
		NullCheck(L_4);
		BooleanU5BU5D_t3456302923* L_5 = (BooleanU5BU5D_t3456302923*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		bool L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4018573382_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_MoveNext_m4018573382(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m2589636915_gshared (Enumerator_t1864657040 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_current_3();
		return L_0;
	}
}
extern "C"  bool Enumerator_get_Current_m2589636915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1864657040 * _thisAdjusted = reinterpret_cast<Enumerator_t1864657040 *>(__this + 1);
	return Enumerator_get_Current_m2589636915(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2573176462_gshared (Enumerator_t4250467982 * __this, List_1_t4230795212 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4230795212 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4230795212 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2573176462_AdjustorThunk (Il2CppObject * __this, List_1_t4230795212 * ___l0, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator__ctor_m2573176462(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m595076356_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m932359596((Enumerator_t4250467982 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m595076356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m595076356(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m722657776_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m722657776_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m722657776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m932359596((Enumerator_t4250467982 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint8_t L_2 = (uint8_t)__this->get_current_3();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m722657776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m722657776(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::Dispose()
extern "C"  void Enumerator_Dispose_m254156787_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4230795212 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m254156787_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_Dispose_m254156787(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m932359596_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m932359596_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m932359596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4230795212 * L_0 = (List_1_t4230795212 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4250467982  L_1 = (*(Enumerator_t4250467982 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4230795212 * L_7 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m932359596_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	Enumerator_VerifyState_m932359596(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2960702064_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m932359596((Enumerator_t4250467982 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4230795212 * L_2 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4230795212 * L_4 = (List_1_t4230795212 *)__this->get_l_0();
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_5 = (ByteU5BU5D_t4260760469*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2960702064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_MoveNext_m2960702064(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Byte>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m2433825123_gshared (Enumerator_t4250467982 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint8_t Enumerator_get_Current_m2433825123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250467982 * _thisAdjusted = reinterpret_cast<Enumerator_t4250467982 *>(__this + 1);
	return Enumerator_get_Current_m2433825123(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1074784348_gshared (Enumerator_t4250480860 * __this, List_1_t4230808090 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4230808090 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4230808090 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1074784348_AdjustorThunk (Il2CppObject * __this, List_1_t4230808090 * ___l0, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	Enumerator__ctor_m1074784348(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m682517110_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4172317690((Enumerator_t4250480860 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m682517110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m682517110(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1517142498_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1517142498_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1517142498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4172317690((Enumerator_t4250480860 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppChar L_2 = (Il2CppChar)__this->get_current_3();
		Il2CppChar L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1517142498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1517142498(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m1381453121_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4230808090 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1381453121_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	Enumerator_Dispose_m1381453121(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m4172317690_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4172317690_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4172317690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4230808090 * L_0 = (List_1_t4230808090 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4250480860  L_1 = (*(Enumerator_t4250480860 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4230808090 * L_7 = (List_1_t4230808090 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4172317690_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	Enumerator_VerifyState_m4172317690(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3547150050_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4172317690((Enumerator_t4250480860 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4230808090 * L_2 = (List_1_t4230808090 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4230808090 * L_4 = (List_1_t4230808090 *)__this->get_l_0();
		NullCheck(L_4);
		CharU5BU5D_t3324145743* L_5 = (CharU5BU5D_t3324145743*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Il2CppChar L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3547150050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	return Enumerator_MoveNext_m3547150050(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m1378815921_gshared (Enumerator_t4250480860 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppChar Enumerator_get_Current_m1378815921_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4250480860 * _thisAdjusted = reinterpret_cast<Enumerator_t4250480860 *>(__this + 1);
	return Enumerator_get_Current_m1378815921(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2249551728_gshared (Enumerator_t3332527299 * __this, List_1_t3312854530 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3312854530 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3312854530 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2249551728_AdjustorThunk (Il2CppObject * __this, List_1_t3312854530 * ___l0, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator__ctor_m2249551728(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1882046178_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1553851918((Enumerator_t3332527299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1882046178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1882046178(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m602451608_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1553851918((Enumerator_t3332527299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t1944668977  L_2 = (KeyValuePair_2_t1944668977 )__this->get_current_3();
		KeyValuePair_2_t1944668977  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m602451608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m602451608(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3838892373_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3312854530 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3838892373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_Dispose_m3838892373(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1553851918_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1553851918_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1553851918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3312854530 * L_0 = (List_1_t3312854530 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3332527299  L_1 = (*(Enumerator_t3332527299 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3312854530 * L_7 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1553851918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	Enumerator_VerifyState_m1553851918(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1524607506_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1553851918((Enumerator_t3332527299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3312854530 * L_2 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3312854530 * L_4 = (List_1_t3312854530 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2483180780* L_5 = (KeyValuePair_2U5BU5D_t2483180780*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		KeyValuePair_2_t1944668977  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1524607506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_MoveNext_m1524607506(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1944668977  Enumerator_get_Current_m3227842279_gshared (Enumerator_t3332527299 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1944668977  L_0 = (KeyValuePair_2_t1944668977 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1944668977  Enumerator_get_Current_m3227842279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3332527299 * _thisAdjusted = reinterpret_cast<Enumerator_t3332527299 *>(__this + 1);
	return Enumerator_get_Current_m3227842279(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1242988386_gshared (Enumerator_t2541696822 * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2522024052 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2522024052 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1242988386_AdjustorThunk (Il2CppObject * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator__ctor_m1242988386(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m936708480((Enumerator_t2541696822 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1509621680(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m936708480((Enumerator_t2541696822 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m262228262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3304555975_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2522024052 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3304555975_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_Dispose_m3304555975(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m936708480_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m936708480_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m936708480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2522024052 * L_0 = (List_1_t2522024052 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2541696822  L_1 = (*(Enumerator_t2541696822 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2522024052 * L_7 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m936708480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_VerifyState_m936708480(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3487355488_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m936708480((Enumerator_t2541696822 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2522024052 * L_2 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2522024052 * L_4 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3487355488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_MoveNext_m3487355488(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1266922905_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1266922905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_get_Current_m1266922905(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3131791237_gshared (Enumerator_t1103292997 * __this, List_1_t1083620227 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1083620227 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1083620227 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3131791237_AdjustorThunk (Il2CppObject * __this, List_1_t1083620227 * ___l0, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	Enumerator__ctor_m3131791237(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1446141357_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1686286563((Enumerator_t1103292997 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1446141357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1446141357(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2615577_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2615577_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2615577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1686286563((Enumerator_t1103292997 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IntPtr_t L_2 = (IntPtr_t)__this->get_current_3();
		IntPtr_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2615577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2615577(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::Dispose()
extern "C"  void Enumerator_Dispose_m1829656234_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1083620227 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1829656234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	Enumerator_Dispose_m1829656234(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.IntPtr>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1686286563_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1686286563_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1686286563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1083620227 * L_0 = (List_1_t1083620227 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1103292997  L_1 = (*(Enumerator_t1103292997 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1083620227 * L_7 = (List_1_t1083620227 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1686286563_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	Enumerator_VerifyState_m1686286563(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.IntPtr>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1890566937_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1686286563((Enumerator_t1103292997 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1083620227 * L_2 = (List_1_t1083620227 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1083620227 * L_4 = (List_1_t1083620227 *)__this->get_l_0();
		NullCheck(L_4);
		IntPtrU5BU5D_t3228729122* L_5 = (IntPtrU5BU5D_t3228729122*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IntPtr_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1890566937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	return Enumerator_MoveNext_m1890566937(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.IntPtr>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m67455194_gshared (Enumerator_t1103292997 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (IntPtr_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  IntPtr_t Enumerator_get_Current_m67455194_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1103292997 * _thisAdjusted = reinterpret_cast<Enumerator_t1103292997 *>(__this + 1);
	return Enumerator_get_Current_m67455194(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1029849669_gshared (Enumerator_t1263707397 * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1244034627 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1029849669_AdjustorThunk (Il2CppObject * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator__ctor_m1029849669(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1522854819((Enumerator_t1263707397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m771996397(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1522854819((Enumerator_t1263707397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3561903705(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2904289642_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1244034627 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2904289642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_Dispose_m2904289642(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1522854819_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1522854819_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1522854819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1263707397  L_1 = (*(Enumerator_t1263707397 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1244034627 * L_7 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1522854819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_VerifyState_m1522854819(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m844464217_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1522854819((Enumerator_t1263707397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1244034627 * L_4 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m844464217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_MoveNext_m844464217(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4198990746_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m4198990746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_get_Current_m4198990746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m256124610_gshared (Enumerator_t152504015 * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	{
		List_1_t132831245 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t132831245 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m256124610_AdjustorThunk (Il2CppObject * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator__ctor_m256124610(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m508287200((Enumerator_t152504015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4026154064(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m508287200((Enumerator_t152504015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t3059612989  L_2 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		CustomAttributeNamedArgument_t3059612989  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m143716486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m855442727_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t132831245 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m855442727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_Dispose_m855442727(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m508287200_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m508287200_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m508287200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t132831245 * L_0 = (List_1_t132831245 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t152504015  L_1 = (*(Enumerator_t152504015 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t132831245 * L_7 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m508287200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_VerifyState_m508287200(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090636416_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m508287200((Enumerator_t152504015 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t132831245 * L_2 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t132831245 * L_4 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t3059612989  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3090636416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_MoveNext_m3090636416(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_get_Current_m473447609(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m444414259_gshared (Enumerator_t394184448 * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	{
		List_1_t374511678 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t374511678 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m444414259_AdjustorThunk (Il2CppObject * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator__ctor_m444414259(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1438062353((Enumerator_t394184448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1403627327(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1438062353((Enumerator_t394184448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t3301293422  L_2 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		CustomAttributeTypedArgument_t3301293422  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1685728309(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3403219928_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t374511678 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3403219928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_Dispose_m3403219928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1438062353_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1438062353_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1438062353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t374511678 * L_0 = (List_1_t374511678 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t394184448  L_1 = (*(Enumerator_t394184448 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t374511678 * L_7 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1438062353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_VerifyState_m1438062353(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m467351023_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1438062353((Enumerator_t394184448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t374511678 * L_2 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t374511678 * L_4 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t3301293422  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m467351023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_MoveNext_m467351023(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_get_Current_m1403222762(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m336902162_gshared (Enumerator_t1986712010 * __this, List_1_t1967039240 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1967039240 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1967039240 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m336902162_AdjustorThunk (Il2CppObject * __this, List_1_t1967039240 * ___l0, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator__ctor_m336902162(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2580027648_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m431664((Enumerator_t1986712010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2580027648_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2580027648(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m431664((Enumerator_t1986712010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t598853688  L_2 = (Color32_t598853688 )__this->get_current_3();
		Color32_t598853688  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1963314668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1963314668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m2747984503_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1967039240 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2747984503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_Dispose_m2747984503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m431664_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m431664_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m431664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1967039240 * L_0 = (List_1_t1967039240 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1986712010  L_1 = (*(Enumerator_t1986712010 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1967039240 * L_7 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m431664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	Enumerator_VerifyState_m431664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2384339564_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m431664((Enumerator_t1986712010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1967039240 * L_2 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1967039240 * L_4 = (List_1_t1967039240 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t2960766953* L_5 = (Color32U5BU5D_t2960766953*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Color32_t598853688  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2384339564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_MoveNext_m2384339564(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t598853688  Enumerator_get_Current_m2092495591_gshared (Enumerator_t1986712010 * __this, const MethodInfo* method)
{
	{
		Color32_t598853688  L_0 = (Color32_t598853688 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t598853688  Enumerator_get_Current_m2092495591_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1986712010 * _thisAdjusted = reinterpret_cast<Enumerator_t1986712010 *>(__this + 1);
	return Enumerator_get_Current_m2092495591(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m538558412_gshared (Enumerator_t855552390 * __this, List_1_t835879620 * ___l0, const MethodInfo* method)
{
	{
		List_1_t835879620 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t835879620 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m538558412_AdjustorThunk (Il2CppObject * __this, List_1_t835879620 * ___l0, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	Enumerator__ctor_m538558412(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4023887110_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4092329834((Enumerator_t855552390 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4023887110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4023887110(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m907574716_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m907574716_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m907574716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4092329834((Enumerator_t855552390 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RaycastResult_t3762661364  L_2 = (RaycastResult_t3762661364 )__this->get_current_3();
		RaycastResult_t3762661364  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m907574716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m907574716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m234487985_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t835879620 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m234487985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	Enumerator_Dispose_m234487985(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m4092329834_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4092329834_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4092329834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t835879620 * L_0 = (List_1_t835879620 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t855552390  L_1 = (*(Enumerator_t855552390 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t835879620 * L_7 = (List_1_t835879620 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4092329834_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	Enumerator_VerifyState_m4092329834(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2684876342_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4092329834((Enumerator_t855552390 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t835879620 * L_2 = (List_1_t835879620 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t835879620 * L_4 = (List_1_t835879620 *)__this->get_l_0();
		NullCheck(L_4);
		RaycastResultU5BU5D_t2754024893* L_5 = (RaycastResultU5BU5D_t2754024893*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		RaycastResult_t3762661364  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2684876342_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	return Enumerator_MoveNext_m2684876342(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t3762661364  Enumerator_get_Current_m3588674627_gshared (Enumerator_t855552390 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t3762661364  L_0 = (RaycastResult_t3762661364 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  RaycastResult_t3762661364  Enumerator_get_Current_m3588674627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t855552390 * _thisAdjusted = reinterpret_cast<Enumerator_t855552390 *>(__this + 1);
	return Enumerator_get_Current_m3588674627(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1904416116_gshared (Enumerator_t1458691020 * __this, List_1_t1439018250 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1439018250 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1439018250 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1904416116_AdjustorThunk (Il2CppObject * __this, List_1_t1439018250 * ___l0, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	Enumerator__ctor_m1904416116(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2458478174_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2318347026((Enumerator_t1458691020 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2458478174_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2458478174(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1161443988_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1161443988_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1161443988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2318347026((Enumerator_t1458691020 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Playable_t70832698  L_2 = (Playable_t70832698 )__this->get_current_3();
		Playable_t70832698  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1161443988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1161443988(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void Enumerator_Dispose_m4172628057_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1439018250 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4172628057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	Enumerator_Dispose_m4172628057(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2318347026_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2318347026_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2318347026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1439018250 * L_0 = (List_1_t1439018250 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1458691020  L_1 = (*(Enumerator_t1458691020 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1439018250 * L_7 = (List_1_t1439018250 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2318347026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	Enumerator_VerifyState_m2318347026(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3793050254_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2318347026((Enumerator_t1458691020 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1439018250 * L_2 = (List_1_t1439018250 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1439018250 * L_4 = (List_1_t1439018250 *)__this->get_l_0();
		NullCheck(L_4);
		PlayableU5BU5D_t910723999* L_5 = (PlayableU5BU5D_t910723999*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Playable_t70832698  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3793050254_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	return Enumerator_MoveNext_m3793050254(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::get_Current()
extern "C"  Playable_t70832698  Enumerator_get_Current_m4025657195_gshared (Enumerator_t1458691020 * __this, const MethodInfo* method)
{
	{
		Playable_t70832698  L_0 = (Playable_t70832698 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Playable_t70832698  Enumerator_get_Current_m4025657195_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1458691020 * _thisAdjusted = reinterpret_cast<Enumerator_t1458691020 *>(__this + 1);
	return Enumerator_get_Current_m4025657195(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m834755074_gshared (Enumerator_t1453665806 * __this, List_1_t1433993036 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1433993036 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1433993036 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m834755074_AdjustorThunk (Il2CppObject * __this, List_1_t1433993036 * ___l0, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	Enumerator__ctor_m834755074(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2925134096_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2594585632((Enumerator_t1453665806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2925134096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2925134096(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m272053318_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m272053318_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m272053318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2594585632((Enumerator_t1453665806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UICharInfo_t65807484  L_2 = (UICharInfo_t65807484 )__this->get_current_3();
		UICharInfo_t65807484  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m272053318_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m272053318(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m4169861223_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1433993036 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4169861223_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	Enumerator_Dispose_m4169861223(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2594585632_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2594585632_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2594585632_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1433993036 * L_0 = (List_1_t1433993036 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1453665806  L_1 = (*(Enumerator_t1453665806 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1433993036 * L_7 = (List_1_t1433993036 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2594585632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	Enumerator_VerifyState_m2594585632(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1002056000_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2594585632((Enumerator_t1453665806 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1433993036 * L_2 = (List_1_t1433993036 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1433993036 * L_4 = (List_1_t1433993036 *)__this->get_l_0();
		NullCheck(L_4);
		UICharInfoU5BU5D_t4214337045* L_5 = (UICharInfoU5BU5D_t4214337045*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UICharInfo_t65807484  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1002056000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	return Enumerator_MoveNext_m1002056000(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t65807484  Enumerator_get_Current_m3015766777_gshared (Enumerator_t1453665806 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t65807484  L_0 = (UICharInfo_t65807484 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UICharInfo_t65807484  Enumerator_get_Current_m3015766777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1453665806 * _thisAdjusted = reinterpret_cast<Enumerator_t1453665806 *>(__this + 1);
	return Enumerator_get_Current_m3015766777(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2414007072_gshared (Enumerator_t1206766508 * __this, List_1_t1187093738 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1187093738 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1187093738 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2414007072_AdjustorThunk (Il2CppObject * __this, List_1_t1187093738 * ___l0, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	Enumerator__ctor_m2414007072(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1880935730_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4058766782((Enumerator_t1206766508 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1880935730_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1880935730(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1984225000_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1984225000_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1984225000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4058766782((Enumerator_t1206766508 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UILineInfo_t4113875482  L_2 = (UILineInfo_t4113875482 )__this->get_current_3();
		UILineInfo_t4113875482  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1984225000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1984225000(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3450743045_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1187093738 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3450743045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	Enumerator_Dispose_m3450743045(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m4058766782_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4058766782_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4058766782_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1187093738 * L_0 = (List_1_t1187093738 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1206766508  L_1 = (*(Enumerator_t1206766508 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1187093738 * L_7 = (List_1_t1187093738 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4058766782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	Enumerator_VerifyState_m4058766782(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m184228962_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4058766782((Enumerator_t1206766508 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1187093738 * L_2 = (List_1_t1187093738 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1187093738 * L_4 = (List_1_t1187093738 *)__this->get_l_0();
		NullCheck(L_4);
		UILineInfoU5BU5D_t2354741311* L_5 = (UILineInfoU5BU5D_t2354741311*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UILineInfo_t4113875482  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m184228962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	return Enumerator_MoveNext_m184228962(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t4113875482  Enumerator_get_Current_m184980631_gshared (Enumerator_t1206766508 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t4113875482  L_0 = (UILineInfo_t4113875482 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UILineInfo_t4113875482  Enumerator_get_Current_m184980631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1206766508 * _thisAdjusted = reinterpret_cast<Enumerator_t1206766508 *>(__this + 1);
	return Enumerator_get_Current_m184980631(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3371656898_gshared (Enumerator_t1336956238 * __this, List_1_t1317283468 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1317283468 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1317283468 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3371656898_AdjustorThunk (Il2CppObject * __this, List_1_t1317283468 * ___l0, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	Enumerator__ctor_m3371656898(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1128201296_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2502891744((Enumerator_t1336956238 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1128201296_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1128201296(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3208054982_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3208054982_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3208054982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2502891744((Enumerator_t1336956238 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UIVertex_t4244065212  L_2 = (UIVertex_t4244065212 )__this->get_current_3();
		UIVertex_t4244065212  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3208054982_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3208054982(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C"  void Enumerator_Dispose_m494945575_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1317283468 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m494945575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	Enumerator_Dispose_m494945575(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m2502891744_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2502891744_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2502891744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1317283468 * L_0 = (List_1_t1317283468 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1336956238  L_1 = (*(Enumerator_t1336956238 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1317283468 * L_7 = (List_1_t1317283468 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2502891744_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	Enumerator_VerifyState_m2502891744(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1624726784_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2502891744((Enumerator_t1336956238 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1317283468 * L_2 = (List_1_t1317283468 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1317283468 * L_4 = (List_1_t1317283468 *)__this->get_l_0();
		NullCheck(L_4);
		UIVertexU5BU5D_t1796391381* L_5 = (UIVertexU5BU5D_t1796391381*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UIVertex_t4244065212  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1624726784_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	return Enumerator_MoveNext_m1624726784(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t4244065212  Enumerator_get_Current_m3961787385_gshared (Enumerator_t1336956238 * __this, const MethodInfo* method)
{
	{
		UIVertex_t4244065212  L_0 = (UIVertex_t4244065212 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UIVertex_t4244065212  Enumerator_get_Current_m3961787385_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1336956238 * _thisAdjusted = reinterpret_cast<Enumerator_t1336956238 *>(__this + 1);
	return Enumerator_get_Current_m3961787385(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1470568351_gshared (Enumerator_t1374957591 * __this, List_1_t1355284821 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1355284821 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1355284821 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1470568351_AdjustorThunk (Il2CppObject * __this, List_1_t1355284821 * ___l0, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	Enumerator__ctor_m1470568351(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4015093075_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1277934205((Enumerator_t1374957591 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4015093075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4015093075(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2747228159_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2747228159_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2747228159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1277934205((Enumerator_t1374957591 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector2_t4282066565  L_2 = (Vector2_t4282066565 )__this->get_current_3();
		Vector2_t4282066565  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2747228159_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2747228159(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C"  void Enumerator_Dispose_m1403010372_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1355284821 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1403010372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	Enumerator_Dispose_m1403010372(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1277934205_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1277934205_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1277934205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1355284821 * L_0 = (List_1_t1355284821 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1374957591  L_1 = (*(Enumerator_t1374957591 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1355284821 * L_7 = (List_1_t1355284821 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1277934205_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	Enumerator_VerifyState_m1277934205(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3639814463_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1277934205((Enumerator_t1374957591 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1355284821 * L_2 = (List_1_t1355284821 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1355284821 * L_4 = (List_1_t1355284821 *)__this->get_l_0();
		NullCheck(L_4);
		Vector2U5BU5D_t4024180168* L_5 = (Vector2U5BU5D_t4024180168*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Vector2_t4282066565  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3639814463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	return Enumerator_MoveNext_m3639814463(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t4282066565  Enumerator_get_Current_m3369998132_gshared (Enumerator_t1374957591 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = (Vector2_t4282066565 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  Enumerator_get_Current_m3369998132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1374957591 * _thisAdjusted = reinterpret_cast<Enumerator_t1374957591 *>(__this + 1);
	return Enumerator_get_Current_m3369998132(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
