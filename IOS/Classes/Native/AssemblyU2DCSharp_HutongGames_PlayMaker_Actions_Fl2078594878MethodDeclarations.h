﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatSwitch
struct FloatSwitch_t2078594878;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::.ctor()
extern "C"  void FloatSwitch__ctor_m1133337016 (FloatSwitch_t2078594878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::Reset()
extern "C"  void FloatSwitch_Reset_m3074737253 (FloatSwitch_t2078594878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnEnter()
extern "C"  void FloatSwitch_OnEnter_m1901875087 (FloatSwitch_t2078594878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnUpdate()
extern "C"  void FloatSwitch_OnUpdate_m2257112212 (FloatSwitch_t2078594878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::DoFloatSwitch()
extern "C"  void FloatSwitch_DoFloatSwitch_m863132603 (FloatSwitch_t2078594878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
