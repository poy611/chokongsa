﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3602735635(__this, ___l0, method) ((  void (*) (Enumerator_t3873106235 *, List_1_t3853433465 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3328930399(__this, method) ((  void (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2269741525(__this, method) ((  Il2CppObject * (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::Dispose()
#define Enumerator_Dispose_m3985204408(__this, method) ((  void (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::VerifyState()
#define Enumerator_VerifyState_m4119594993(__this, method) ((  void (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::MoveNext()
#define Enumerator_MoveNext_m2277884431(__this, method) ((  bool (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.NativeEvent>::get_Current()
#define Enumerator_get_Current_m1531937866(__this, method) ((  NativeEvent_t2485247913 * (*) (Enumerator_t3873106235 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
