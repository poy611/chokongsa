﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48
struct U3CGetUserEmailU3Ec__AnonStorey48_t183568840;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48::.ctor()
extern "C"  void U3CGetUserEmailU3Ec__AnonStorey48__ctor_m1132146339 (U3CGetUserEmailU3Ec__AnonStorey48_t183568840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetUserEmail>c__AnonStorey47/<GetUserEmail>c__AnonStorey48::<>m__33()
extern "C"  void U3CGetUserEmailU3Ec__AnonStorey48_U3CU3Em__33_m3674008332 (U3CGetUserEmailU3Ec__AnonStorey48_t183568840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
