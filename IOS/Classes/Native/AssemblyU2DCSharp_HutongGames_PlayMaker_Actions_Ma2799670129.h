﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerGetHostData
struct  MasterServerGetHostData_t2799670129  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::hostIndex
	FsmInt_t1596138449 * ___hostIndex_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetHostData::useNat
	FsmBool_t1075959796 * ___useNat_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::gameType
	FsmString_t952858651 * ___gameType_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::gameName
	FsmString_t952858651 * ___gameName_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::connectedPlayers
	FsmInt_t1596138449 * ___connectedPlayers_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::playerLimit
	FsmInt_t1596138449 * ___playerLimit_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::ipAddress
	FsmString_t952858651 * ___ipAddress_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerGetHostData::port
	FsmInt_t1596138449 * ___port_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerGetHostData::passwordProtected
	FsmBool_t1075959796 * ___passwordProtected_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::comment
	FsmString_t952858651 * ___comment_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerGetHostData::guid
	FsmString_t952858651 * ___guid_21;

public:
	inline static int32_t get_offset_of_hostIndex_11() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___hostIndex_11)); }
	inline FsmInt_t1596138449 * get_hostIndex_11() const { return ___hostIndex_11; }
	inline FsmInt_t1596138449 ** get_address_of_hostIndex_11() { return &___hostIndex_11; }
	inline void set_hostIndex_11(FsmInt_t1596138449 * value)
	{
		___hostIndex_11 = value;
		Il2CppCodeGenWriteBarrier(&___hostIndex_11, value);
	}

	inline static int32_t get_offset_of_useNat_12() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___useNat_12)); }
	inline FsmBool_t1075959796 * get_useNat_12() const { return ___useNat_12; }
	inline FsmBool_t1075959796 ** get_address_of_useNat_12() { return &___useNat_12; }
	inline void set_useNat_12(FsmBool_t1075959796 * value)
	{
		___useNat_12 = value;
		Il2CppCodeGenWriteBarrier(&___useNat_12, value);
	}

	inline static int32_t get_offset_of_gameType_13() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___gameType_13)); }
	inline FsmString_t952858651 * get_gameType_13() const { return ___gameType_13; }
	inline FsmString_t952858651 ** get_address_of_gameType_13() { return &___gameType_13; }
	inline void set_gameType_13(FsmString_t952858651 * value)
	{
		___gameType_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameType_13, value);
	}

	inline static int32_t get_offset_of_gameName_14() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___gameName_14)); }
	inline FsmString_t952858651 * get_gameName_14() const { return ___gameName_14; }
	inline FsmString_t952858651 ** get_address_of_gameName_14() { return &___gameName_14; }
	inline void set_gameName_14(FsmString_t952858651 * value)
	{
		___gameName_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_14, value);
	}

	inline static int32_t get_offset_of_connectedPlayers_15() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___connectedPlayers_15)); }
	inline FsmInt_t1596138449 * get_connectedPlayers_15() const { return ___connectedPlayers_15; }
	inline FsmInt_t1596138449 ** get_address_of_connectedPlayers_15() { return &___connectedPlayers_15; }
	inline void set_connectedPlayers_15(FsmInt_t1596138449 * value)
	{
		___connectedPlayers_15 = value;
		Il2CppCodeGenWriteBarrier(&___connectedPlayers_15, value);
	}

	inline static int32_t get_offset_of_playerLimit_16() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___playerLimit_16)); }
	inline FsmInt_t1596138449 * get_playerLimit_16() const { return ___playerLimit_16; }
	inline FsmInt_t1596138449 ** get_address_of_playerLimit_16() { return &___playerLimit_16; }
	inline void set_playerLimit_16(FsmInt_t1596138449 * value)
	{
		___playerLimit_16 = value;
		Il2CppCodeGenWriteBarrier(&___playerLimit_16, value);
	}

	inline static int32_t get_offset_of_ipAddress_17() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___ipAddress_17)); }
	inline FsmString_t952858651 * get_ipAddress_17() const { return ___ipAddress_17; }
	inline FsmString_t952858651 ** get_address_of_ipAddress_17() { return &___ipAddress_17; }
	inline void set_ipAddress_17(FsmString_t952858651 * value)
	{
		___ipAddress_17 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_17, value);
	}

	inline static int32_t get_offset_of_port_18() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___port_18)); }
	inline FsmInt_t1596138449 * get_port_18() const { return ___port_18; }
	inline FsmInt_t1596138449 ** get_address_of_port_18() { return &___port_18; }
	inline void set_port_18(FsmInt_t1596138449 * value)
	{
		___port_18 = value;
		Il2CppCodeGenWriteBarrier(&___port_18, value);
	}

	inline static int32_t get_offset_of_passwordProtected_19() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___passwordProtected_19)); }
	inline FsmBool_t1075959796 * get_passwordProtected_19() const { return ___passwordProtected_19; }
	inline FsmBool_t1075959796 ** get_address_of_passwordProtected_19() { return &___passwordProtected_19; }
	inline void set_passwordProtected_19(FsmBool_t1075959796 * value)
	{
		___passwordProtected_19 = value;
		Il2CppCodeGenWriteBarrier(&___passwordProtected_19, value);
	}

	inline static int32_t get_offset_of_comment_20() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___comment_20)); }
	inline FsmString_t952858651 * get_comment_20() const { return ___comment_20; }
	inline FsmString_t952858651 ** get_address_of_comment_20() { return &___comment_20; }
	inline void set_comment_20(FsmString_t952858651 * value)
	{
		___comment_20 = value;
		Il2CppCodeGenWriteBarrier(&___comment_20, value);
	}

	inline static int32_t get_offset_of_guid_21() { return static_cast<int32_t>(offsetof(MasterServerGetHostData_t2799670129, ___guid_21)); }
	inline FsmString_t952858651 * get_guid_21() const { return ___guid_21; }
	inline FsmString_t952858651 ** get_address_of_guid_21() { return &___guid_21; }
	inline void set_guid_21(FsmString_t952858651 * value)
	{
		___guid_21 = value;
		Il2CppCodeGenWriteBarrier(&___guid_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
