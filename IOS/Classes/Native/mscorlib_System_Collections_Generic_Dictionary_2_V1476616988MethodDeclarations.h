﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1476616988.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m232045584_gshared (Enumerator_t1476616988 * __this, Dictionary_2_t3544783580 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m232045584(__this, ___host0, method) ((  void (*) (Enumerator_t1476616988 *, Dictionary_2_t3544783580 *, const MethodInfo*))Enumerator__ctor_m232045584_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1464355419_gshared (Enumerator_t1476616988 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1464355419(__this, method) ((  Il2CppObject * (*) (Enumerator_t1476616988 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1464355419_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4075645605_gshared (Enumerator_t1476616988 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4075645605(__this, method) ((  void (*) (Enumerator_t1476616988 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4075645605_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m786081458_gshared (Enumerator_t1476616988 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m786081458(__this, method) ((  void (*) (Enumerator_t1476616988 *, const MethodInfo*))Enumerator_Dispose_m786081458_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3812623061_gshared (Enumerator_t1476616988 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3812623061(__this, method) ((  bool (*) (Enumerator_t1476616988 *, const MethodInfo*))Enumerator_MoveNext_m3812623061_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  RaycastHit2D_t1374744384  Enumerator_get_Current_m2359648565_gshared (Enumerator_t1476616988 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2359648565(__this, method) ((  RaycastHit2D_t1374744384  (*) (Enumerator_t1476616988 *, const MethodInfo*))Enumerator_get_Current_m2359648565_gshared)(__this, method)
