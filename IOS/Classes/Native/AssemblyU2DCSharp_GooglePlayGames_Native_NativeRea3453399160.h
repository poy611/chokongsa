﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t3078724631;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey66
struct U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64
struct U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t2043220689;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65
struct  U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::helper
	RealTimeEventListenerHelper_t3078724631 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey66 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::<>f__ref$102
	U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161 * ___U3CU3Ef__refU24102_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey64 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::<>f__ref$100
	U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159 * ___U3CU3Ef__refU24100_2;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey65::<>f__this
	NativeRealtimeMultiplayerClient_t2043220689 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_helper_0() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160, ___helper_0)); }
	inline RealTimeEventListenerHelper_t3078724631 * get_helper_0() const { return ___helper_0; }
	inline RealTimeEventListenerHelper_t3078724631 ** get_address_of_helper_0() { return &___helper_0; }
	inline void set_helper_0(RealTimeEventListenerHelper_t3078724631 * value)
	{
		___helper_0 = value;
		Il2CppCodeGenWriteBarrier(&___helper_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24102_1() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160, ___U3CU3Ef__refU24102_1)); }
	inline U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161 * get_U3CU3Ef__refU24102_1() const { return ___U3CU3Ef__refU24102_1; }
	inline U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161 ** get_address_of_U3CU3Ef__refU24102_1() { return &___U3CU3Ef__refU24102_1; }
	inline void set_U3CU3Ef__refU24102_1(U3CCreateQuickGameU3Ec__AnonStorey66_t3453399161 * value)
	{
		___U3CU3Ef__refU24102_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24102_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24100_2() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160, ___U3CU3Ef__refU24100_2)); }
	inline U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159 * get_U3CU3Ef__refU24100_2() const { return ___U3CU3Ef__refU24100_2; }
	inline U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159 ** get_address_of_U3CU3Ef__refU24100_2() { return &___U3CU3Ef__refU24100_2; }
	inline void set_U3CU3Ef__refU24100_2(U3CCreateQuickGameU3Ec__AnonStorey64_t3453399159 * value)
	{
		___U3CU3Ef__refU24100_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24100_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCreateQuickGameU3Ec__AnonStorey65_t3453399160, ___U3CU3Ef__this_3)); }
	inline NativeRealtimeMultiplayerClient_t2043220689 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline NativeRealtimeMultiplayerClient_t2043220689 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(NativeRealtimeMultiplayerClient_t2043220689 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
