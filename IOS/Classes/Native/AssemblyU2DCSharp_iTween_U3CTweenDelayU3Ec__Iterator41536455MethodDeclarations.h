﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTween/<TweenDelay>c__IteratorA
struct U3CTweenDelayU3Ec__IteratorA_t41536455;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void iTween/<TweenDelay>c__IteratorA::.ctor()
extern "C"  void U3CTweenDelayU3Ec__IteratorA__ctor_m3444472708 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1934205518 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTweenDelayU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m2557870562 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenDelay>c__IteratorA::MoveNext()
extern "C"  bool U3CTweenDelayU3Ec__IteratorA_MoveNext_m350733296 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__IteratorA::Dispose()
extern "C"  void U3CTweenDelayU3Ec__IteratorA_Dispose_m2911737537 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__IteratorA::Reset()
extern "C"  void U3CTweenDelayU3Ec__IteratorA_Reset_m1090905649 (U3CTweenDelayU3Ec__IteratorA_t41536455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
