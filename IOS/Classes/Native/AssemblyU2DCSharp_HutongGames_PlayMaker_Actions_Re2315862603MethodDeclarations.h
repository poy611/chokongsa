﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectContains
struct RectContains_t2315862603;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectContains::.ctor()
extern "C"  void RectContains__ctor_m1413730427 (RectContains_t2315862603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::Reset()
extern "C"  void RectContains_Reset_m3355130664 (RectContains_t2315862603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::OnEnter()
extern "C"  void RectContains_OnEnter_m777003410 (RectContains_t2315862603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::OnUpdate()
extern "C"  void RectContains_OnUpdate_m1745828593 (RectContains_t2315862603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::DoRectContains()
extern "C"  void RectContains_DoRectContains_m985799735 (RectContains_t2315862603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
