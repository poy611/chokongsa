﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.XPath.XmlDocumentEditableNavigator
struct XmlDocumentEditableNavigator_t792096397;
// Mono.Xml.XPath.XPathEditableDocument
struct XPathEditableDocument_t61026146;
// System.String
struct String_t;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1075073278;
// System.Xml.XmlNode
struct XmlNode_t856910923;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml_XPath_XPathEditableDocument61026146.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentEditableNaviga792096397.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope1935109964.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XmlNodeOrder444538963.h"

// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.ctor(Mono.Xml.XPath.XPathEditableDocument)
extern "C"  void XmlDocumentEditableNavigator__ctor_m2729288836 (XmlDocumentEditableNavigator_t792096397 * __this, XPathEditableDocument_t61026146 * ___doc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.ctor(Mono.Xml.XPath.XmlDocumentEditableNavigator)
extern "C"  void XmlDocumentEditableNavigator__ctor_m94457073 (XmlDocumentEditableNavigator_t792096397 * __this, XmlDocumentEditableNavigator_t792096397 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.cctor()
extern "C"  void XmlDocumentEditableNavigator__cctor_m2314429568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_LocalName()
extern "C"  String_t* XmlDocumentEditableNavigator_get_LocalName_m2552684987 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_Name()
extern "C"  String_t* XmlDocumentEditableNavigator_get_Name_m1443563688 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_NamespaceURI()
extern "C"  String_t* XmlDocumentEditableNavigator_get_NamespaceURI_m3195230894 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeType Mono.Xml.XPath.XmlDocumentEditableNavigator::get_NodeType()
extern "C"  int32_t XmlDocumentEditableNavigator_get_NodeType_m2717197373 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_Value()
extern "C"  String_t* XmlDocumentEditableNavigator_get_Value_m310507990 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_XmlLang()
extern "C"  String_t* XmlDocumentEditableNavigator_get_XmlLang_m989037834 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::get_HasChildren()
extern "C"  bool XmlDocumentEditableNavigator_get_HasChildren_m2696128707 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::get_HasAttributes()
extern "C"  bool XmlDocumentEditableNavigator_get_HasAttributes_m4215613787 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNavigator Mono.Xml.XPath.XmlDocumentEditableNavigator::Clone()
extern "C"  XPathNavigator_t1075073278 * XmlDocumentEditableNavigator_Clone_m1441948730 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode Mono.Xml.XPath.XmlDocumentEditableNavigator::GetNode()
extern "C"  XmlNode_t856910923 * XmlDocumentEditableNavigator_GetNode_m964706321 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentEditableNavigator_IsSamePosition_m2054489737 (XmlDocumentEditableNavigator_t792096397 * __this, XPathNavigator_t1075073278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveTo(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentEditableNavigator_MoveTo_m4020108982 (XmlDocumentEditableNavigator_t792096397 * __this, XPathNavigator_t1075073278 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstAttribute()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstAttribute_m2891360487 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstChild()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstChild_m2536638567 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstNamespace_m296870135 (XmlDocumentEditableNavigator_t792096397 * __this, int32_t ___scope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToId(System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToId_m451508044 (XmlDocumentEditableNavigator_t792096397 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNext()
extern "C"  bool XmlDocumentEditableNavigator_MoveToNext_m4016586958 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNextAttribute()
extern "C"  bool XmlDocumentEditableNavigator_MoveToNextAttribute_m3261782864 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentEditableNavigator_MoveToNextNamespace_m2229981920 (XmlDocumentEditableNavigator_t792096397 * __this, int32_t ___scope0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToParent()
extern "C"  bool XmlDocumentEditableNavigator_MoveToParent_m2811571781 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToRoot()
extern "C"  void XmlDocumentEditableNavigator_MoveToRoot_m4087425029 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNamespace(System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToNamespace_m4006976512 (XmlDocumentEditableNavigator_t792096397 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirst()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirst_m1559067991 (XmlDocumentEditableNavigator_t792096397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToAttribute_m1216160411 (XmlDocumentEditableNavigator_t792096397 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XmlDocumentEditableNavigator_IsDescendant_m1298640123 (XmlDocumentEditableNavigator_t792096397 * __this, XPathNavigator_t1075073278 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeOrder Mono.Xml.XPath.XmlDocumentEditableNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern "C"  int32_t XmlDocumentEditableNavigator_ComparePosition_m205748080 (XmlDocumentEditableNavigator_t792096397 * __this, XPathNavigator_t1075073278 * ___nav0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
