﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseRect
struct EaseRect_t3254272922;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseRect::.ctor()
extern "C"  void EaseRect__ctor_m865838092 (EaseRect_t3254272922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::Reset()
extern "C"  void EaseRect_Reset_m2807238329 (EaseRect_t3254272922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnEnter()
extern "C"  void EaseRect_OnEnter_m2533446883 (EaseRect_t3254272922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnExit()
extern "C"  void EaseRect_OnExit_m2584495701 (EaseRect_t3254272922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnUpdate()
extern "C"  void EaseRect_OnUpdate_m361001408 (EaseRect_t3254272922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
