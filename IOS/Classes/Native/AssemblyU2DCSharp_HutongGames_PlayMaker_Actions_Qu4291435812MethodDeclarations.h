﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionAngleAxis
struct QuaternionAngleAxis_t4291435812;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::.ctor()
extern "C"  void QuaternionAngleAxis__ctor_m3910790802 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::Reset()
extern "C"  void QuaternionAngleAxis_Reset_m1557223743 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnEnter()
extern "C"  void QuaternionAngleAxis_OnEnter_m3860272617 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnUpdate()
extern "C"  void QuaternionAngleAxis_OnUpdate_m2837893498 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnLateUpdate()
extern "C"  void QuaternionAngleAxis_OnLateUpdate_m3764341312 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnFixedUpdate()
extern "C"  void QuaternionAngleAxis_OnFixedUpdate_m1606409006 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::DoQuatAngleAxis()
extern "C"  void QuaternionAngleAxis_DoQuatAngleAxis_m1479967010 (QuaternionAngleAxis_t4291435812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
