﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawDebugLine
struct  DrawDebugLine_t2835919313  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DrawDebugLine::fromObject
	FsmGameObject_t1697147867 * ___fromObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugLine::fromPosition
	FsmVector3_t533912882 * ___fromPosition_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DrawDebugLine::toObject
	FsmGameObject_t1697147867 * ___toObject_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugLine::toPosition
	FsmVector3_t533912882 * ___toPosition_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DrawDebugLine::color
	FsmColor_t2131419205 * ___color_15;

public:
	inline static int32_t get_offset_of_fromObject_11() { return static_cast<int32_t>(offsetof(DrawDebugLine_t2835919313, ___fromObject_11)); }
	inline FsmGameObject_t1697147867 * get_fromObject_11() const { return ___fromObject_11; }
	inline FsmGameObject_t1697147867 ** get_address_of_fromObject_11() { return &___fromObject_11; }
	inline void set_fromObject_11(FsmGameObject_t1697147867 * value)
	{
		___fromObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___fromObject_11, value);
	}

	inline static int32_t get_offset_of_fromPosition_12() { return static_cast<int32_t>(offsetof(DrawDebugLine_t2835919313, ___fromPosition_12)); }
	inline FsmVector3_t533912882 * get_fromPosition_12() const { return ___fromPosition_12; }
	inline FsmVector3_t533912882 ** get_address_of_fromPosition_12() { return &___fromPosition_12; }
	inline void set_fromPosition_12(FsmVector3_t533912882 * value)
	{
		___fromPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___fromPosition_12, value);
	}

	inline static int32_t get_offset_of_toObject_13() { return static_cast<int32_t>(offsetof(DrawDebugLine_t2835919313, ___toObject_13)); }
	inline FsmGameObject_t1697147867 * get_toObject_13() const { return ___toObject_13; }
	inline FsmGameObject_t1697147867 ** get_address_of_toObject_13() { return &___toObject_13; }
	inline void set_toObject_13(FsmGameObject_t1697147867 * value)
	{
		___toObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___toObject_13, value);
	}

	inline static int32_t get_offset_of_toPosition_14() { return static_cast<int32_t>(offsetof(DrawDebugLine_t2835919313, ___toPosition_14)); }
	inline FsmVector3_t533912882 * get_toPosition_14() const { return ___toPosition_14; }
	inline FsmVector3_t533912882 ** get_address_of_toPosition_14() { return &___toPosition_14; }
	inline void set_toPosition_14(FsmVector3_t533912882 * value)
	{
		___toPosition_14 = value;
		Il2CppCodeGenWriteBarrier(&___toPosition_14, value);
	}

	inline static int32_t get_offset_of_color_15() { return static_cast<int32_t>(offsetof(DrawDebugLine_t2835919313, ___color_15)); }
	inline FsmColor_t2131419205 * get_color_15() const { return ___color_15; }
	inline FsmColor_t2131419205 ** get_address_of_color_15() { return &___color_15; }
	inline void set_color_15(FsmColor_t2131419205 * value)
	{
		___color_15 = value;
		Il2CppCodeGenWriteBarrier(&___color_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
