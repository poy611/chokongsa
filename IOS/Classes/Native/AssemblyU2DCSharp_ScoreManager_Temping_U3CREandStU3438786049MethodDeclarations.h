﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Temping/<REandSt>c__Iterator22
struct U3CREandStU3Ec__Iterator22_t438786049;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::.ctor()
extern "C"  void U3CREandStU3Ec__Iterator22__ctor_m3813340298 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<REandSt>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CREandStU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2859661448 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Temping/<REandSt>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CREandStU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m142019612 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Temping/<REandSt>c__Iterator22::MoveNext()
extern "C"  bool U3CREandStU3Ec__Iterator22_MoveNext_m419205930 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::Dispose()
extern "C"  void U3CREandStU3Ec__Iterator22_Dispose_m911205959 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::Reset()
extern "C"  void U3CREandStU3Ec__Iterator22_Reset_m1459773239 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
