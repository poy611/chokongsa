﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t1809280638;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void MethodCall_2__ctor_m4173381250_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define MethodCall_2__ctor_m4173381250(__this, ___object0, ___method1, method) ((  void (*) (MethodCall_2_t1809280638 *, Il2CppObject *, IntPtr_t, const MethodInfo*))MethodCall_2__ctor_m4173381250_gshared)(__this, ___object0, ___method1, method)
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::Invoke(T,System.Object[])
extern "C"  Il2CppObject * MethodCall_2_Invoke_m1327760016_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method);
#define MethodCall_2_Invoke_m1327760016(__this, ___target0, ___args1, method) ((  Il2CppObject * (*) (MethodCall_2_t1809280638 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, const MethodInfo*))MethodCall_2_Invoke_m1327760016_gshared)(__this, ___target0, ___args1, method)
// System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MethodCall_2_BeginInvoke_m4137169899_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___target0, ObjectU5BU5D_t1108656482* ___args1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define MethodCall_2_BeginInvoke_m4137169899(__this, ___target0, ___args1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (MethodCall_2_t1809280638 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))MethodCall_2_BeginInvoke_m4137169899_gshared)(__this, ___target0, ___args1, ___callback2, ___object3, method)
// TResult Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * MethodCall_2_EndInvoke_m2253508144_gshared (MethodCall_2_t1809280638 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define MethodCall_2_EndInvoke_m2253508144(__this, ___result0, method) ((  Il2CppObject * (*) (MethodCall_2_t1809280638 *, Il2CppObject *, const MethodInfo*))MethodCall_2_EndInvoke_m2253508144_gshared)(__this, ___result0, method)
