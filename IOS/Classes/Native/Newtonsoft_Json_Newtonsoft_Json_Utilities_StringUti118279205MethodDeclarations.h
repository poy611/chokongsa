﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>
struct U3CU3Ec__DisplayClass15_0_1_t118279205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15_0_1__ctor_m2755217215_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass15_0_1__ctor_m2755217215(__this, method) ((  void (*) (U3CU3Ec__DisplayClass15_0_1_t118279205 *, const MethodInfo*))U3CU3Ec__DisplayClass15_0_1__ctor_m2755217215_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::<ForgivingCaseSensitiveFind>b__0(TSource)
extern "C"  bool U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, Il2CppObject * ___s0, const MethodInfo* method);
#define U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652(__this, ___s0, method) ((  bool (*) (U3CU3Ec__DisplayClass15_0_1_t118279205 *, Il2CppObject *, const MethodInfo*))U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__0_m3394484652_gshared)(__this, ___s0, method)
// System.Boolean Newtonsoft.Json.Utilities.StringUtils/<>c__DisplayClass15_0`1<System.Object>::<ForgivingCaseSensitiveFind>b__1(TSource)
extern "C"  bool U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147_gshared (U3CU3Ec__DisplayClass15_0_1_t118279205 * __this, Il2CppObject * ___s0, const MethodInfo* method);
#define U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147(__this, ___s0, method) ((  bool (*) (U3CU3Ec__DisplayClass15_0_1_t118279205 *, Il2CppObject *, const MethodInfo*))U3CU3Ec__DisplayClass15_0_1_U3CForgivingCaseSensitiveFindU3Eb__1_m3197971147_gshared)(__this, ___s0, method)
