﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey5C
struct U3CRegisterInvitationDelegateU3Ec__AnonStorey5C_t323376100;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t2200833403;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2200833403.h"

// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey5C::.ctor()
extern "C"  void U3CRegisterInvitationDelegateU3Ec__AnonStorey5C__ctor_m3679212551 (U3CRegisterInvitationDelegateU3Ec__AnonStorey5C_t323376100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey5C::<>m__32(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C"  void U3CRegisterInvitationDelegateU3Ec__AnonStorey5C_U3CU3Em__32_m1173551311 (U3CRegisterInvitationDelegateU3Ec__AnonStorey5C_t323376100 * __this, Invitation_t2200833403 * ___invitation0, bool ___autoAccept1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
