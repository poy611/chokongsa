﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Boolean HutongGames.Utility.ColorUtils::Approximately(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool ColorUtils_Approximately_m3793551808 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___color10, Color_t4194546905  ___color21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.Utility.ColorUtils::FromIntRGBA(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Color_t4194546905  ColorUtils_FromIntRGBA_m387845783 (Il2CppObject * __this /* static, unused */, int32_t ___r0, int32_t ___g1, int32_t ___b2, int32_t ___a3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
