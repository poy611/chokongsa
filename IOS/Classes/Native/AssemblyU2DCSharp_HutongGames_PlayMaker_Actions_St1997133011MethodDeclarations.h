﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartServer
struct StartServer_t1997133011;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartServer::.ctor()
extern "C"  void StartServer__ctor_m1416876995 (StartServer_t1997133011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartServer::Reset()
extern "C"  void StartServer_Reset_m3358277232 (StartServer_t1997133011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartServer::OnEnter()
extern "C"  void StartServer_OnEnter_m3800855258 (StartServer_t1997133011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
