﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Animator
struct Animator_t2776330603;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t274649809;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal2036631794.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo323110318.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2817229998.h"
#include "UnityEngine_UnityEngine_AvatarTarget2373143374.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask258413904.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1606609988.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode3217251138.h"

// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C"  bool Animator_get_isHuman_m4030001272 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_humanScale()
extern "C"  float Animator_get_humanScale_m13697776 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C"  float Animator_GetFloat_m2965884705 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C"  void Animator_SetFloat_m57191598 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m2671615160 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C"  bool Animator_GetBool_m1246282447 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C"  void Animator_SetBool_m1802007004 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C"  int32_t Animator_GetInteger_m3944178743 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C"  void Animator_SetInteger_m812217484 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m514363822 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m4152421915 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C"  bool Animator_IsParameterControlledByCurve_m3328157587 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C"  Vector3_t4282066566  Animator_get_deltaPosition_m1658225602 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_deltaPosition_m3729275047 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C"  Quaternion_t1553702882  Animator_get_deltaRotation_m1583110423 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_deltaRotation_m3667880600 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C"  Vector3_t4282066566  Animator_get_rootPosition_m425633900 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_rootPosition_m623412333 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C"  Quaternion_t1553702882  Animator_get_rootRotation_m3309843777 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_rootRotation_m3392225554 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C"  bool Animator_get_applyRootMotion_m2388146907 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m394805828 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C"  float Animator_get_gravityWeight_m1393695637 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C"  Vector3_t4282066566  Animator_get_bodyPosition_m2404872492 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_bodyPosition_m2325119987 (Animator_t2776330603 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetBodyPositionInternal()
extern "C"  Vector3_t4282066566  Animator_GetBodyPositionInternal_m2309356744 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyPositionInternal_m890317748 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBodyPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetBodyPositionInternal_m2543113867 (Animator_t2776330603 * __this, Vector3_t4282066566  ___bodyPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyPositionInternal_m793480488 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___bodyPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C"  Quaternion_t1553702882  Animator_get_bodyRotation_m994115073 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_bodyRotation_m2385096902 (Animator_t2776330603 * __this, Quaternion_t1553702882  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetBodyRotationInternal()
extern "C"  Quaternion_t1553702882  Animator_GetBodyRotationInternal_m2551889181 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyRotationInternal_m3136479669 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Quaternion_t1553702882 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBodyRotationInternal(UnityEngine.Quaternion)
extern "C"  void Animator_SetBodyRotationInternal_m1236865582 (Animator_t2776330603 * __this, Quaternion_t1553702882  ___bodyRotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyRotationInternal_m180722625 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Quaternion_t1553702882 * ___bodyRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPosition_m385128518 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPositionInternal(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t4282066566  Animator_GetIKPositionInternal_m142003939 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKPositionInternal_m976909533 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPosition_m2542416671 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPositionInternal_m335958844 (Animator_t2776330603 * __this, int32_t ___goal0, Vector3_t4282066566  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKPositionInternal_m647475433 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Vector3_t4282066566 * ___goalPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotation_m3297713819 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotationInternal(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t1553702882  Animator_GetIKRotationInternal_m2296655928 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetIKRotationInternal_m110687330 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotation_m2768899344 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationInternal(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotationInternal_m1596039379 (Animator_t2776330603 * __this, int32_t ___goal0, Quaternion_t1553702882  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetIKRotationInternal_m4234652886 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, int32_t ___goal1, Quaternion_t1553702882 * ___goalRotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeight_m2079943564 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeightInternal_m464452649 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeight_m1110244361 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeightInternal_m1498324454 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeight_m3997781473 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeightInternal_m1941289342 (Animator_t2776330603 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeight_m2279843230 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeightInternal_m35709563 (Animator_t2776330603 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPosition_m384162552 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPositionInternal_m1453739067 (Animator_t2776330603 * __this, Vector3_t4282066566  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m816040728 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___lookAtPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m881446346 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m96208805 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3985034432 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C"  void Animator_SetLookAtWeight_m2035469595 (Animator_t2776330603 * __this, float ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m2843438895 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeightInternal_m3334195378 (Animator_t2776330603 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C"  void Animator_set_stabilizeFeet_m2533795611 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m3326924613 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C"  String_t* Animator_GetLayerName_m3480300056 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C"  float Animator_GetLayerWeight_m3878421230 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C"  void Animator_SetLayerWeight_m3838560187 (Animator_t2776330603 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetCurrentAnimatorStateInfo_m3061859448 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t323110318  Animator_GetNextAnimatorStateInfo_m791156688 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2817229998  Animator_GetAnimatorTransitionInfo_m3858104711 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C"  bool Animator_IsInTransition_m2609196857 (Animator_t2776330603 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C"  float Animator_get_feetPivotActive_m4087239689 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C"  void Animator_set_feetPivotActive_m293215914 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C"  float Animator_get_pivotWeight_m1500566793 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C"  Vector3_t4282066566  Animator_get_pivotPosition_m1479929804 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_pivotPosition_m1941126065 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C"  void Animator_MatchTarget_m4236652838 (Animator_t2776330603 * __this, Vector3_t4282066566  ___matchPosition0, Quaternion_t1553702882  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t258413904  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C"  void Animator_INTERNAL_CALL_MatchTarget_m2875283135 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___self0, Vector3_t4282066566 * ___matchPosition1, Quaternion_t1553702882 * ___matchRotation2, int32_t ___targetBodyPart3, MatchTargetWeightMask_t258413904 * ___weightMask4, float ___startNormalizedTime5, float ___targetNormalizedTime6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C"  void Animator_InterruptMatchTarget_m3501603944 (Animator_t2776330603 * __this, bool ___completeMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C"  bool Animator_get_isMatchingTarget_m3696235301 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_speed()
extern "C"  float Animator_get_speed_m1893369654 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C"  void Animator_set_speed_m2513936029 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m1281797781 (Animator_t2776330603 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m358767974 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m3631644044 (Animator_t2776330603 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m330123001 (Animator_t2776330603 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C"  void Animator_SetTarget_m3335381339 (Animator_t2776330603 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C"  Vector3_t4282066566  Animator_get_targetPosition_m3699919451 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_targetPosition_m656463196 (Animator_t2776330603 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C"  Quaternion_t1553702882  Animator_get_targetRotation_m2080040752 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_targetRotation_m168007107 (Animator_t2776330603 * __this, Quaternion_t1553702882 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C"  Transform_t1659122786 * Animator_GetBoneTransform_m3449809847 (Animator_t2776330603 * __this, int32_t ___humanBoneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C"  int32_t Animator_get_cullingMode_m1008819440 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C"  void Animator_set_cullingMode_m773355747 (Animator_t2776330603 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartPlayback()
extern "C"  void Animator_StartPlayback_m3009121761 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopPlayback()
extern "C"  void Animator_StopPlayback_m3612462619 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C"  float Animator_get_playbackTime_m3871048475 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C"  void Animator_set_playbackTime_m281288968 (Animator_t2776330603 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C"  void Animator_StartRecording_m2916226974 (Animator_t2776330603 * __this, int32_t ___frameCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopRecording()
extern "C"  void Animator_StopRecording_m4232410323 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C"  float Animator_get_recorderStartTime_m1936545440 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C"  float Animator_get_recorderStopTime_m3086287392 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t274649809 * Animator_get_runtimeAnimatorController_m1822082727 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m4020897098 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C"  void Animator_CheckIfInIKPass_m2875692449 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C"  bool Animator_CheckIfInIKPassInternal_m914167192 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C"  void Animator_SetFloatID_m819072393 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C"  float Animator_GetFloatID_m373159228 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C"  void Animator_SetBoolID_m516262497 (Animator_t2776330603 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C"  bool Animator_GetBoolID_m1397798250 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C"  void Animator_SetIntegerID_m276290769 (Animator_t2776330603 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C"  int32_t Animator_GetIntegerID_m4210859218 (Animator_t2776330603 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m1378271133 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1817269834 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C"  bool Animator_IsParameterControlledByCurveString_m1231885762 (Animator_t2776330603 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatIDDamp_m4107886867 (Animator_t2776330603 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C"  bool Animator_get_layersAffectMassCenter_m3409296173 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C"  void Animator_set_layersAffectMassCenter_m1296962674 (Animator_t2776330603 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C"  float Animator_get_leftFeetBottomHeight_m4166530042 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C"  float Animator_get_rightFeetBottomHeight_m3839016939 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C"  bool Animator_get_logWarnings_m1817672208 (Animator_t2776330603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
