﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLowPassFilter
struct QuaternionLowPassFilter_t3523731693;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::.ctor()
extern "C"  void QuaternionLowPassFilter__ctor_m3048515817 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::Reset()
extern "C"  void QuaternionLowPassFilter_Reset_m694948758 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnEnter()
extern "C"  void QuaternionLowPassFilter_OnEnter_m4142700160 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnUpdate()
extern "C"  void QuaternionLowPassFilter_OnUpdate_m3003212739 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnLateUpdate()
extern "C"  void QuaternionLowPassFilter_OnLateUpdate_m2057670665 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnFixedUpdate()
extern "C"  void QuaternionLowPassFilter_OnFixedUpdate_m239226501 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::DoQuatLowPassFilter()
extern "C"  void QuaternionLowPassFilter_DoQuatLowPassFilter_m2497370306 (QuaternionLowPassFilter_t3523731693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
