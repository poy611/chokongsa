﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PerSecond
struct  PerSecond_t38067007  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::floatValue
	FsmFloat_t2134102846 * ___floatValue_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::storeResult
	FsmFloat_t2134102846 * ___storeResult_12;
	// System.Boolean HutongGames.PlayMaker.Actions.PerSecond::everyFrame
	bool ___everyFrame_13;

public:
	inline static int32_t get_offset_of_floatValue_11() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___floatValue_11)); }
	inline FsmFloat_t2134102846 * get_floatValue_11() const { return ___floatValue_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatValue_11() { return &___floatValue_11; }
	inline void set_floatValue_11(FsmFloat_t2134102846 * value)
	{
		___floatValue_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_11, value);
	}

	inline static int32_t get_offset_of_storeResult_12() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___storeResult_12)); }
	inline FsmFloat_t2134102846 * get_storeResult_12() const { return ___storeResult_12; }
	inline FsmFloat_t2134102846 ** get_address_of_storeResult_12() { return &___storeResult_12; }
	inline void set_storeResult_12(FsmFloat_t2134102846 * value)
	{
		___storeResult_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(PerSecond_t38067007, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
