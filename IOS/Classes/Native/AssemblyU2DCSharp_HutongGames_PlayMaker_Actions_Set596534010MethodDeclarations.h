﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetParent
struct SetParent_t596534010;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetParent::.ctor()
extern "C"  void SetParent__ctor_m3599063932 (SetParent_t596534010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetParent::Reset()
extern "C"  void SetParent_Reset_m1245496873 (SetParent_t596534010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetParent::OnEnter()
extern "C"  void SetParent_OnEnter_m643493971 (SetParent_t596534010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
