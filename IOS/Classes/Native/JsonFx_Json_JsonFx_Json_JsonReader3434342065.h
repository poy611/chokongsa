﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JsonFx.Json.JsonReaderSettings
struct JsonReaderSettings_t24058356;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonFx.Json.JsonReader
struct  JsonReader_t3434342065  : public Il2CppObject
{
public:
	// JsonFx.Json.JsonReaderSettings JsonFx.Json.JsonReader::Settings
	JsonReaderSettings_t24058356 * ___Settings_0;
	// System.String JsonFx.Json.JsonReader::Source
	String_t* ___Source_1;
	// System.Int32 JsonFx.Json.JsonReader::SourceLength
	int32_t ___SourceLength_2;
	// System.Int32 JsonFx.Json.JsonReader::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_Settings_0() { return static_cast<int32_t>(offsetof(JsonReader_t3434342065, ___Settings_0)); }
	inline JsonReaderSettings_t24058356 * get_Settings_0() const { return ___Settings_0; }
	inline JsonReaderSettings_t24058356 ** get_address_of_Settings_0() { return &___Settings_0; }
	inline void set_Settings_0(JsonReaderSettings_t24058356 * value)
	{
		___Settings_0 = value;
		Il2CppCodeGenWriteBarrier(&___Settings_0, value);
	}

	inline static int32_t get_offset_of_Source_1() { return static_cast<int32_t>(offsetof(JsonReader_t3434342065, ___Source_1)); }
	inline String_t* get_Source_1() const { return ___Source_1; }
	inline String_t** get_address_of_Source_1() { return &___Source_1; }
	inline void set_Source_1(String_t* value)
	{
		___Source_1 = value;
		Il2CppCodeGenWriteBarrier(&___Source_1, value);
	}

	inline static int32_t get_offset_of_SourceLength_2() { return static_cast<int32_t>(offsetof(JsonReader_t3434342065, ___SourceLength_2)); }
	inline int32_t get_SourceLength_2() const { return ___SourceLength_2; }
	inline int32_t* get_address_of_SourceLength_2() { return &___SourceLength_2; }
	inline void set_SourceLength_2(int32_t value)
	{
		___SourceLength_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(JsonReader_t3434342065, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
