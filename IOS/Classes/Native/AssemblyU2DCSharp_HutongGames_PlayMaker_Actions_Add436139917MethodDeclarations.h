﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddMixingTransform
struct AddMixingTransform_t436139917;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::.ctor()
extern "C"  void AddMixingTransform__ctor_m1655337849 (AddMixingTransform_t436139917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::Reset()
extern "C"  void AddMixingTransform_Reset_m3596738086 (AddMixingTransform_t436139917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::OnEnter()
extern "C"  void AddMixingTransform_OnEnter_m1033501968 (AddMixingTransform_t436139917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::DoAddMixingTransform()
extern "C"  void AddMixingTransform_DoAddMixingTransform_m2494144955 (AddMixingTransform_t436139917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
