﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t3078724631;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D
struct U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E
struct U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F
struct  U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::helper
	RealTimeEventListenerHelper_t3078724631 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::<>f__ref$109
	U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * ___U3CU3Ef__refU24109_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6E GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D/<AcceptFromInbox>c__AnonStorey6F::<>f__ref$110
	U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487 * ___U3CU3Ef__refU24110_2;

public:
	inline static int32_t get_offset_of_helper_0() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488, ___helper_0)); }
	inline RealTimeEventListenerHelper_t3078724631 * get_helper_0() const { return ___helper_0; }
	inline RealTimeEventListenerHelper_t3078724631 ** get_address_of_helper_0() { return &___helper_0; }
	inline void set_helper_0(RealTimeEventListenerHelper_t3078724631 * value)
	{
		___helper_0 = value;
		Il2CppCodeGenWriteBarrier(&___helper_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24109_1() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488, ___U3CU3Ef__refU24109_1)); }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * get_U3CU3Ef__refU24109_1() const { return ___U3CU3Ef__refU24109_1; }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 ** get_address_of_U3CU3Ef__refU24109_1() { return &___U3CU3Ef__refU24109_1; }
	inline void set_U3CU3Ef__refU24109_1(U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440 * value)
	{
		___U3CU3Ef__refU24109_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24109_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24110_2() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6F_t4066396488, ___U3CU3Ef__refU24110_2)); }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487 * get_U3CU3Ef__refU24110_2() const { return ___U3CU3Ef__refU24110_2; }
	inline U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487 ** get_address_of_U3CU3Ef__refU24110_2() { return &___U3CU3Ef__refU24110_2; }
	inline void set_U3CU3Ef__refU24110_2(U3CAcceptFromInboxU3Ec__AnonStorey6E_t4066396487 * value)
	{
		___U3CU3Ef__refU24110_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24110_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
