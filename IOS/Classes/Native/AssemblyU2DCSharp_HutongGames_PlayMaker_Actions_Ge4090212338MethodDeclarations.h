﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
struct GetAnimatorLeftFootBottomHeight_t4090212338;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::.ctor()
extern "C"  void GetAnimatorLeftFootBottomHeight__ctor_m1634568068 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::Reset()
extern "C"  void GetAnimatorLeftFootBottomHeight_Reset_m3575968305 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnEnter()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnEnter_m2548578907 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnLateUpdate_m3660389646 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::_getLeftFootBottonHeight()
extern "C"  void GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m3696279871 (GetAnimatorLeftFootBottomHeight_t4090212338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
