﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.ReadType>
struct Dictionary_2_t1321993412;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3548794116.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3685755938_gshared (Enumerator_t3548794116 * __this, Dictionary_2_t1321993412 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3685755938(__this, ___host0, method) ((  void (*) (Enumerator_t3548794116 *, Dictionary_2_t1321993412 *, const MethodInfo*))Enumerator__ctor_m3685755938_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2708535433_gshared (Enumerator_t3548794116 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2708535433(__this, method) ((  Il2CppObject * (*) (Enumerator_t3548794116 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2708535433_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2080887635_gshared (Enumerator_t3548794116 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2080887635(__this, method) ((  void (*) (Enumerator_t3548794116 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2080887635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::Dispose()
extern "C"  void Enumerator_Dispose_m3924631364_gshared (Enumerator_t3548794116 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3924631364(__this, method) ((  void (*) (Enumerator_t3548794116 *, const MethodInfo*))Enumerator_Dispose_m3924631364_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2323422339_gshared (Enumerator_t3548794116 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2323422339(__this, method) ((  bool (*) (Enumerator_t3548794116 *, const MethodInfo*))Enumerator_MoveNext_m2323422339_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Newtonsoft.Json.ReadType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m298139847_gshared (Enumerator_t3548794116 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m298139847(__this, method) ((  int32_t (*) (Enumerator_t3548794116 *, const MethodInfo*))Enumerator_get_Current_m298139847_gshared)(__this, method)
