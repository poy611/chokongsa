﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenShakeRotation
struct  iTweenShakeRotation_t559909872  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenShakeRotation::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenShakeRotation::id
	FsmString_t952858651 * ___id_20;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenShakeRotation::vector
	FsmVector3_t533912882 * ___vector_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenShakeRotation::time
	FsmFloat_t2134102846 * ___time_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenShakeRotation::delay
	FsmFloat_t2134102846 * ___delay_23;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenShakeRotation::loopType
	int32_t ___loopType_24;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenShakeRotation::space
	int32_t ___space_25;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___gameObject_19)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_19, value);
	}

	inline static int32_t get_offset_of_id_20() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___id_20)); }
	inline FsmString_t952858651 * get_id_20() const { return ___id_20; }
	inline FsmString_t952858651 ** get_address_of_id_20() { return &___id_20; }
	inline void set_id_20(FsmString_t952858651 * value)
	{
		___id_20 = value;
		Il2CppCodeGenWriteBarrier(&___id_20, value);
	}

	inline static int32_t get_offset_of_vector_21() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___vector_21)); }
	inline FsmVector3_t533912882 * get_vector_21() const { return ___vector_21; }
	inline FsmVector3_t533912882 ** get_address_of_vector_21() { return &___vector_21; }
	inline void set_vector_21(FsmVector3_t533912882 * value)
	{
		___vector_21 = value;
		Il2CppCodeGenWriteBarrier(&___vector_21, value);
	}

	inline static int32_t get_offset_of_time_22() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___time_22)); }
	inline FsmFloat_t2134102846 * get_time_22() const { return ___time_22; }
	inline FsmFloat_t2134102846 ** get_address_of_time_22() { return &___time_22; }
	inline void set_time_22(FsmFloat_t2134102846 * value)
	{
		___time_22 = value;
		Il2CppCodeGenWriteBarrier(&___time_22, value);
	}

	inline static int32_t get_offset_of_delay_23() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___delay_23)); }
	inline FsmFloat_t2134102846 * get_delay_23() const { return ___delay_23; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_23() { return &___delay_23; }
	inline void set_delay_23(FsmFloat_t2134102846 * value)
	{
		___delay_23 = value;
		Il2CppCodeGenWriteBarrier(&___delay_23, value);
	}

	inline static int32_t get_offset_of_loopType_24() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___loopType_24)); }
	inline int32_t get_loopType_24() const { return ___loopType_24; }
	inline int32_t* get_address_of_loopType_24() { return &___loopType_24; }
	inline void set_loopType_24(int32_t value)
	{
		___loopType_24 = value;
	}

	inline static int32_t get_offset_of_space_25() { return static_cast<int32_t>(offsetof(iTweenShakeRotation_t559909872, ___space_25)); }
	inline int32_t get_space_25() const { return ___space_25; }
	inline int32_t* get_address_of_space_25() { return &___space_25; }
	inline void set_space_25(int32_t value)
	{
		___space_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
