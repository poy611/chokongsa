﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d
struct GetNextOverlapPoint2d_t3860527186;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::.ctor()
extern "C"  void GetNextOverlapPoint2d__ctor_m754199844 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::Reset()
extern "C"  void GetNextOverlapPoint2d_Reset_m2695600081 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::OnEnter()
extern "C"  void GetNextOverlapPoint2d_OnEnter_m2623272955 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::DoGetNextCollider()
extern "C"  void GetNextOverlapPoint2d_DoGetNextCollider_m563281588 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::GetOverlapPointAll()
extern "C"  Collider2DU5BU5D_t1758559887* GetNextOverlapPoint2d_GetOverlapPointAll_m2207819524 (GetNextOverlapPoint2d_t3860527186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
