﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<HandleInvitation>c__AnonStorey46
struct U3CHandleInvitationU3Ec__AnonStorey46_t1890800216;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<HandleInvitation>c__AnonStorey46::.ctor()
extern "C"  void U3CHandleInvitationU3Ec__AnonStorey46__ctor_m3416414739 (U3CHandleInvitationU3Ec__AnonStorey46_t1890800216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<HandleInvitation>c__AnonStorey46::<>m__1A()
extern "C"  void U3CHandleInvitationU3Ec__AnonStorey46_U3CU3Em__1A_m4127606348 (U3CHandleInvitationU3Ec__AnonStorey46_t1890800216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
