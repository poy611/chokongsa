﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// PopupManagement
struct PopupManagement_t282433007;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManagerMiniGame
struct  ScoreManagerMiniGame_t3434204900  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreManagerMiniGame::nowNum
	int32_t ___nowNum_2;
	// System.String ScoreManagerMiniGame::nowScene
	String_t* ___nowScene_3;
	// System.Single ScoreManagerMiniGame::ticks
	float ___ticks_4;
	// UnityEngine.GameObject ScoreManagerMiniGame::ready
	GameObject_t3674682005 * ___ready_5;
	// StructforMinigame ScoreManagerMiniGame::stuff
	StructforMinigame_t1286533533 * ___stuff_6;
	// ConstforMinigame ScoreManagerMiniGame::cuff
	ConstforMinigame_t2789090703 * ___cuff_7;
	// PopupManagement ScoreManagerMiniGame::pop
	PopupManagement_t282433007 * ___pop_8;
	// System.Diagnostics.Stopwatch ScoreManagerMiniGame::sw
	Stopwatch_t3420517611 * ___sw_9;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_nowScene_3() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___nowScene_3)); }
	inline String_t* get_nowScene_3() const { return ___nowScene_3; }
	inline String_t** get_address_of_nowScene_3() { return &___nowScene_3; }
	inline void set_nowScene_3(String_t* value)
	{
		___nowScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_3, value);
	}

	inline static int32_t get_offset_of_ticks_4() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___ticks_4)); }
	inline float get_ticks_4() const { return ___ticks_4; }
	inline float* get_address_of_ticks_4() { return &___ticks_4; }
	inline void set_ticks_4(float value)
	{
		___ticks_4 = value;
	}

	inline static int32_t get_offset_of_ready_5() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___ready_5)); }
	inline GameObject_t3674682005 * get_ready_5() const { return ___ready_5; }
	inline GameObject_t3674682005 ** get_address_of_ready_5() { return &___ready_5; }
	inline void set_ready_5(GameObject_t3674682005 * value)
	{
		___ready_5 = value;
		Il2CppCodeGenWriteBarrier(&___ready_5, value);
	}

	inline static int32_t get_offset_of_stuff_6() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___stuff_6)); }
	inline StructforMinigame_t1286533533 * get_stuff_6() const { return ___stuff_6; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_6() { return &___stuff_6; }
	inline void set_stuff_6(StructforMinigame_t1286533533 * value)
	{
		___stuff_6 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_6, value);
	}

	inline static int32_t get_offset_of_cuff_7() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___cuff_7)); }
	inline ConstforMinigame_t2789090703 * get_cuff_7() const { return ___cuff_7; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_7() { return &___cuff_7; }
	inline void set_cuff_7(ConstforMinigame_t2789090703 * value)
	{
		___cuff_7 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_7, value);
	}

	inline static int32_t get_offset_of_pop_8() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___pop_8)); }
	inline PopupManagement_t282433007 * get_pop_8() const { return ___pop_8; }
	inline PopupManagement_t282433007 ** get_address_of_pop_8() { return &___pop_8; }
	inline void set_pop_8(PopupManagement_t282433007 * value)
	{
		___pop_8 = value;
		Il2CppCodeGenWriteBarrier(&___pop_8, value);
	}

	inline static int32_t get_offset_of_sw_9() { return static_cast<int32_t>(offsetof(ScoreManagerMiniGame_t3434204900, ___sw_9)); }
	inline Stopwatch_t3420517611 * get_sw_9() const { return ___sw_9; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_9() { return &___sw_9; }
	inline void set_sw_9(Stopwatch_t3420517611 * value)
	{
		___sw_9 = value;
		Il2CppCodeGenWriteBarrier(&___sw_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
