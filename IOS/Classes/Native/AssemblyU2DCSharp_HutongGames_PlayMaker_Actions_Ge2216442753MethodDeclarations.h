﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRectFields
struct GetRectFields_t2216442753;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRectFields::.ctor()
extern "C"  void GetRectFields__ctor_m3013048021 (GetRectFields_t2216442753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::Reset()
extern "C"  void GetRectFields_Reset_m659480962 (GetRectFields_t2216442753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnEnter()
extern "C"  void GetRectFields_OnEnter_m122919276 (GetRectFields_t2216442753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnUpdate()
extern "C"  void GetRectFields_OnUpdate_m2944056919 (GetRectFields_t2216442753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::DoGetRectFields()
extern "C"  void GetRectFields_DoGetRectFields_m207739483 (GetRectFields_t2216442753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
