﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.BasicApi.ScorePageToken
struct ScorePageToken_t1995225314;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb3617536533.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2320727150.h"

// System.Void GooglePlayGames.BasicApi.ScorePageToken::.ctor(System.Object,System.String,GooglePlayGames.BasicApi.LeaderboardCollection,GooglePlayGames.BasicApi.LeaderboardTimeSpan)
extern "C"  void ScorePageToken__ctor_m2874806508 (ScorePageToken_t1995225314 * __this, Il2CppObject * ___internalObject0, String_t* ___id1, int32_t ___collection2, int32_t ___timespan3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.LeaderboardCollection GooglePlayGames.BasicApi.ScorePageToken::get_Collection()
extern "C"  int32_t ScorePageToken_get_Collection_m4260916982 (ScorePageToken_t1995225314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.LeaderboardTimeSpan GooglePlayGames.BasicApi.ScorePageToken::get_TimeSpan()
extern "C"  int32_t ScorePageToken_get_TimeSpan_m34440616 (ScorePageToken_t1995225314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.ScorePageToken::get_LeaderboardId()
extern "C"  String_t* ScorePageToken_get_LeaderboardId_m3952163259 (ScorePageToken_t1995225314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GooglePlayGames.BasicApi.ScorePageToken::get_InternalObject()
extern "C"  Il2CppObject * ScorePageToken_get_InternalObject_m231552009 (ScorePageToken_t1995225314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
