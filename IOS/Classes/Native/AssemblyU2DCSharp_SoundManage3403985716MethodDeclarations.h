﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManage
struct SoundManage_t3403985716;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void SoundManage::.ctor()
extern "C"  void SoundManage__ctor_m2177573559 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::.cctor()
extern "C"  void SoundManage__cctor_m2598174678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::Awake()
extern "C"  void SoundManage_Awake_m2415178778 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::efxButtonOn()
extern "C"  void SoundManage_efxButtonOn_m2962933949 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::efxOkOn()
extern "C"  void SoundManage_efxOkOn_m3935479783 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::efxBadOn()
extern "C"  void SoundManage_efxBadOn_m2820389210 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::PlayBgm(System.String)
extern "C"  void SoundManage_PlayBgm_m180107993 (SoundManage_t3403985716 * __this, String_t* ___sceneName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManage::OffBgm()
extern "C"  void SoundManage_OffBgm_m2683352902 (SoundManage_t3403985716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
