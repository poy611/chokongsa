﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3192061892(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t716150752 *, String_t*, float, const MethodInfo*))KeyValuePair_2__ctor_m2908028374_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m2959420255(__this, method) ((  String_t* (*) (KeyValuePair_2_t716150752 *, const MethodInfo*))KeyValuePair_2_get_Key_m975404242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3730083813(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t716150752 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m662474387_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m573709234(__this, method) ((  float (*) (KeyValuePair_2_t716150752 *, const MethodInfo*))KeyValuePair_2_get_Value_m2222463222_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1953196389(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t716150752 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m3606602003_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Single>::ToString()
#define KeyValuePair_2_ToString_m2550631747(__this, method) ((  String_t* (*) (KeyValuePair_2_t716150752 *, const MethodInfo*))KeyValuePair_2_ToString_m3615079765_gshared)(__this, method)
