﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>
struct DefaultComparer_t4117488347;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void DefaultComparer__ctor_m3188230175_gshared (DefaultComparer_t4117488347 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3188230175(__this, method) ((  void (*) (DefaultComparer_t4117488347 *, const MethodInfo*))DefaultComparer__ctor_m3188230175_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3600333300_gshared (DefaultComparer_t4117488347 * __this, RaycastHit2D_t1374744384  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3600333300(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4117488347 *, RaycastHit2D_t1374744384 , const MethodInfo*))DefaultComparer_GetHashCode_m3600333300_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3635145396_gshared (DefaultComparer_t4117488347 * __this, RaycastHit2D_t1374744384  ___x0, RaycastHit2D_t1374744384  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3635145396(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4117488347 *, RaycastHit2D_t1374744384 , RaycastHit2D_t1374744384 , const MethodInfo*))DefaultComparer_Equals_m3635145396_gshared)(__this, ___x0, ___y1, method)
