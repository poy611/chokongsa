﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F
struct U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50
struct  U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024  : public Il2CppObject
{
public:
	// GooglePlayGames.BasicApi.CommonStatusCodes GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50::responseCode
	int32_t ___responseCode_0;
	// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F/<GetPlayerStats>c__AnonStorey50::<>f__ref$79
	U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * ___U3CU3Ef__refU2479_1;

public:
	inline static int32_t get_offset_of_responseCode_0() { return static_cast<int32_t>(offsetof(U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024, ___responseCode_0)); }
	inline int32_t get_responseCode_0() const { return ___responseCode_0; }
	inline int32_t* get_address_of_responseCode_0() { return &___responseCode_0; }
	inline void set_responseCode_0(int32_t value)
	{
		___responseCode_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2479_1() { return static_cast<int32_t>(offsetof(U3CGetPlayerStatsU3Ec__AnonStorey50_t4053278024, ___U3CU3Ef__refU2479_1)); }
	inline U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * get_U3CU3Ef__refU2479_1() const { return ___U3CU3Ef__refU2479_1; }
	inline U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 ** get_address_of_U3CU3Ef__refU2479_1() { return &___U3CU3Ef__refU2479_1; }
	inline void set_U3CU3Ef__refU2479_1(U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * value)
	{
		___U3CU3Ef__refU2479_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2479_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
