﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey83
struct U3CCommitUpdateU3Ec__AnonStorey83_t532623073;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t2801162465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S2801162465.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey83::.ctor()
extern "C"  void U3CCommitUpdateU3Ec__AnonStorey83__ctor_m1665128154 (U3CCommitUpdateU3Ec__AnonStorey83_t532623073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey83::<>m__6C(GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse)
extern "C"  void U3CCommitUpdateU3Ec__AnonStorey83_U3CU3Em__6C_m2619991617 (U3CCommitUpdateU3Ec__AnonStorey83_t532623073 * __this, CommitResponse_t2801162465 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
