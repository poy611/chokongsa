﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneindexInc
struct SceneindexInc_t2309712408;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneindexInc::.ctor()
extern "C"  void SceneindexInc__ctor_m1562956371 (SceneindexInc_t2309712408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneindexInc::.cctor()
extern "C"  void SceneindexInc__cctor_m724911034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneindexInc SceneindexInc::getSingleton()
extern "C"  SceneindexInc_t2309712408 * SceneindexInc_getSingleton_m1216717995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SceneindexInc::getIndexInc()
extern "C"  int32_t SceneindexInc_getIndexInc_m4109664065 (SceneindexInc_t2309712408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SceneindexInc::getIndex()
extern "C"  int32_t SceneindexInc_getIndex_m1359659583 (SceneindexInc_t2309712408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
