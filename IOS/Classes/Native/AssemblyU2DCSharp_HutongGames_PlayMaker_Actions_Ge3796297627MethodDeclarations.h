﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVariables
struct GetFsmVariables_t3796297627;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::.ctor()
extern "C"  void GetFsmVariables__ctor_m2056957435 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::Reset()
extern "C"  void GetFsmVariables_Reset_m3998357672 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::InitFsmVars()
extern "C"  void GetFsmVariables_InitFsmVars_m2275039861 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnEnter()
extern "C"  void GetFsmVariables_OnEnter_m442867474 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnUpdate()
extern "C"  void GetFsmVariables_OnUpdate_m4272516465 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::DoGetFsmVariables()
extern "C"  void GetFsmVariables_DoGetFsmVariables_m45106459 (GetFsmVariables_t3796297627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
