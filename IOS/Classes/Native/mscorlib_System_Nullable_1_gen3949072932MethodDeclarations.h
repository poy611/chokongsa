﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3949072932.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.JsonPosition>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2488632452_gshared (Nullable_1_t3949072932 * __this, JsonPosition_t3864946409  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2488632452(__this, ___value0, method) ((  void (*) (Nullable_1_t3949072932 *, JsonPosition_t3864946409 , const MethodInfo*))Nullable_1__ctor_m2488632452_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonPosition>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3630736551_gshared (Nullable_1_t3949072932 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3630736551(__this, method) ((  bool (*) (Nullable_1_t3949072932 *, const MethodInfo*))Nullable_1_get_HasValue_m3630736551_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonPosition>::get_Value()
extern "C"  JsonPosition_t3864946409  Nullable_1_get_Value_m48642796_gshared (Nullable_1_t3949072932 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m48642796(__this, method) ((  JsonPosition_t3864946409  (*) (Nullable_1_t3949072932 *, const MethodInfo*))Nullable_1_get_Value_m48642796_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonPosition>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4263207802_gshared (Nullable_1_t3949072932 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4263207802(__this, ___other0, method) ((  bool (*) (Nullable_1_t3949072932 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m4263207802_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.JsonPosition>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3870306949_gshared (Nullable_1_t3949072932 * __this, Nullable_1_t3949072932  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3870306949(__this, ___other0, method) ((  bool (*) (Nullable_1_t3949072932 *, Nullable_1_t3949072932 , const MethodInfo*))Nullable_1_Equals_m3870306949_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.JsonPosition>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2758775506_gshared (Nullable_1_t3949072932 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2758775506(__this, method) ((  int32_t (*) (Nullable_1_t3949072932 *, const MethodInfo*))Nullable_1_GetHashCode_m2758775506_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonPosition>::GetValueOrDefault()
extern "C"  JsonPosition_t3864946409  Nullable_1_GetValueOrDefault_m120885880_gshared (Nullable_1_t3949072932 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m120885880(__this, method) ((  JsonPosition_t3864946409  (*) (Nullable_1_t3949072932 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m120885880_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.JsonPosition>::GetValueOrDefault(T)
extern "C"  JsonPosition_t3864946409  Nullable_1_GetValueOrDefault_m1671579383_gshared (Nullable_1_t3949072932 * __this, JsonPosition_t3864946409  ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1671579383(__this, ___defaultValue0, method) ((  JsonPosition_t3864946409  (*) (Nullable_1_t3949072932 *, JsonPosition_t3864946409 , const MethodInfo*))Nullable_1_GetValueOrDefault_m1671579383_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.JsonPosition>::ToString()
extern "C"  String_t* Nullable_1_ToString_m729632710_gshared (Nullable_1_t3949072932 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m729632710(__this, method) ((  String_t* (*) (Nullable_1_t3949072932 *, const MethodInfo*))Nullable_1_ToString_m729632710_gshared)(__this, method)
