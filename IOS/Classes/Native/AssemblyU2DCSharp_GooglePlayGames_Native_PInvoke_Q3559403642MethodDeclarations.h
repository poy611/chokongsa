﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
struct FetchResponse_t3559403642;
// GooglePlayGames.Native.PInvoke.NativeQuest
struct NativeQuest_t2496300529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::.ctor(System.IntPtr)
extern "C"  void FetchResponse__ctor_m1277107651 (FetchResponse_t3559403642 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::ResponseStatus()
extern "C"  int32_t FetchResponse_ResponseStatus_m129689981 (FetchResponse_t3559403642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::Data()
extern "C"  NativeQuest_t2496300529 * FetchResponse_Data_m433895304 (FetchResponse_t3559403642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::RequestSucceeded()
extern "C"  bool FetchResponse_RequestSucceeded_m4218856401 (FetchResponse_t3559403642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void FetchResponse_CallDispose_m1627960397 (FetchResponse_t3559403642 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse::FromPointer(System.IntPtr)
extern "C"  FetchResponse_t3559403642 * FetchResponse_FromPointer_m1545433817 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
