﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddAnimationClip
struct  AddAnimationClip_t922763643  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddAnimationClip::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AddAnimationClip::animationClip
	FsmObject_t821476169 * ___animationClip_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddAnimationClip::animationName
	FsmString_t952858651 * ___animationName_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AddAnimationClip::firstFrame
	FsmInt_t1596138449 * ___firstFrame_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AddAnimationClip::lastFrame
	FsmInt_t1596138449 * ___lastFrame_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddAnimationClip::addLoopFrame
	FsmBool_t1075959796 * ___addLoopFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_animationClip_12() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___animationClip_12)); }
	inline FsmObject_t821476169 * get_animationClip_12() const { return ___animationClip_12; }
	inline FsmObject_t821476169 ** get_address_of_animationClip_12() { return &___animationClip_12; }
	inline void set_animationClip_12(FsmObject_t821476169 * value)
	{
		___animationClip_12 = value;
		Il2CppCodeGenWriteBarrier(&___animationClip_12, value);
	}

	inline static int32_t get_offset_of_animationName_13() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___animationName_13)); }
	inline FsmString_t952858651 * get_animationName_13() const { return ___animationName_13; }
	inline FsmString_t952858651 ** get_address_of_animationName_13() { return &___animationName_13; }
	inline void set_animationName_13(FsmString_t952858651 * value)
	{
		___animationName_13 = value;
		Il2CppCodeGenWriteBarrier(&___animationName_13, value);
	}

	inline static int32_t get_offset_of_firstFrame_14() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___firstFrame_14)); }
	inline FsmInt_t1596138449 * get_firstFrame_14() const { return ___firstFrame_14; }
	inline FsmInt_t1596138449 ** get_address_of_firstFrame_14() { return &___firstFrame_14; }
	inline void set_firstFrame_14(FsmInt_t1596138449 * value)
	{
		___firstFrame_14 = value;
		Il2CppCodeGenWriteBarrier(&___firstFrame_14, value);
	}

	inline static int32_t get_offset_of_lastFrame_15() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___lastFrame_15)); }
	inline FsmInt_t1596138449 * get_lastFrame_15() const { return ___lastFrame_15; }
	inline FsmInt_t1596138449 ** get_address_of_lastFrame_15() { return &___lastFrame_15; }
	inline void set_lastFrame_15(FsmInt_t1596138449 * value)
	{
		___lastFrame_15 = value;
		Il2CppCodeGenWriteBarrier(&___lastFrame_15, value);
	}

	inline static int32_t get_offset_of_addLoopFrame_16() { return static_cast<int32_t>(offsetof(AddAnimationClip_t922763643, ___addLoopFrame_16)); }
	inline FsmBool_t1075959796 * get_addLoopFrame_16() const { return ___addLoopFrame_16; }
	inline FsmBool_t1075959796 ** get_address_of_addLoopFrame_16() { return &___addLoopFrame_16; }
	inline void set_addLoopFrame_16(FsmBool_t1075959796 * value)
	{
		___addLoopFrame_16 = value;
		Il2CppCodeGenWriteBarrier(&___addLoopFrame_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
