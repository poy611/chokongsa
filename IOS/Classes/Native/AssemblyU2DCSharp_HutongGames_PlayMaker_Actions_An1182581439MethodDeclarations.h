﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFloatV2
struct AnimateFloatV2_t1182581439;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::.ctor()
extern "C"  void AnimateFloatV2__ctor_m656141447 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::Reset()
extern "C"  void AnimateFloatV2_Reset_m2597541684 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnEnter()
extern "C"  void AnimateFloatV2_OnEnter_m2878433950 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnExit()
extern "C"  void AnimateFloatV2_OnExit_m378867002 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnUpdate()
extern "C"  void AnimateFloatV2_OnUpdate_m2465665893 (AnimateFloatV2_t1182581439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
