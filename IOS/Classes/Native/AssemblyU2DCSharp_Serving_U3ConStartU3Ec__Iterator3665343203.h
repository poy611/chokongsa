﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.Object
struct Il2CppObject;
// Serving
struct Serving_t3648806892;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Serving/<onStart>c__Iterator28
struct  U3ConStartU3Ec__Iterator28_t3665343203  : public Il2CppObject
{
public:
	// UnityEngine.Sprite Serving/<onStart>c__Iterator28::<sour>__0
	Sprite_t3199167241 * ___U3CsourU3E__0_0;
	// UnityEngine.Sprite[] Serving/<onStart>c__Iterator28::<source>__1
	SpriteU5BU5D_t2761310900* ___U3CsourceU3E__1_1;
	// System.Int32 Serving/<onStart>c__Iterator28::$PC
	int32_t ___U24PC_2;
	// System.Object Serving/<onStart>c__Iterator28::$current
	Il2CppObject * ___U24current_3;
	// Serving Serving/<onStart>c__Iterator28::<>f__this
	Serving_t3648806892 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CsourU3E__0_0() { return static_cast<int32_t>(offsetof(U3ConStartU3Ec__Iterator28_t3665343203, ___U3CsourU3E__0_0)); }
	inline Sprite_t3199167241 * get_U3CsourU3E__0_0() const { return ___U3CsourU3E__0_0; }
	inline Sprite_t3199167241 ** get_address_of_U3CsourU3E__0_0() { return &___U3CsourU3E__0_0; }
	inline void set_U3CsourU3E__0_0(Sprite_t3199167241 * value)
	{
		___U3CsourU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsourU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CsourceU3E__1_1() { return static_cast<int32_t>(offsetof(U3ConStartU3Ec__Iterator28_t3665343203, ___U3CsourceU3E__1_1)); }
	inline SpriteU5BU5D_t2761310900* get_U3CsourceU3E__1_1() const { return ___U3CsourceU3E__1_1; }
	inline SpriteU5BU5D_t2761310900** get_address_of_U3CsourceU3E__1_1() { return &___U3CsourceU3E__1_1; }
	inline void set_U3CsourceU3E__1_1(SpriteU5BU5D_t2761310900* value)
	{
		___U3CsourceU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsourceU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3ConStartU3Ec__Iterator28_t3665343203, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3ConStartU3Ec__Iterator28_t3665343203, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3ConStartU3Ec__Iterator28_t3665343203, ___U3CU3Ef__this_4)); }
	inline Serving_t3648806892 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline Serving_t3648806892 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(Serving_t3648806892 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
