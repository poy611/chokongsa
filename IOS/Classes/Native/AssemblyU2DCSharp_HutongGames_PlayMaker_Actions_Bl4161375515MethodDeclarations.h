﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BlendAnimation
struct BlendAnimation_t4161375515;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::.ctor()
extern "C"  void BlendAnimation__ctor_m817301931 (BlendAnimation_t4161375515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::Reset()
extern "C"  void BlendAnimation_Reset_m2758702168 (BlendAnimation_t4161375515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::OnEnter()
extern "C"  void BlendAnimation_OnEnter_m3134836418 (BlendAnimation_t4161375515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::OnUpdate()
extern "C"  void BlendAnimation_OnUpdate_m1824207809 (BlendAnimation_t4161375515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::DoBlendAnimation(UnityEngine.GameObject)
extern "C"  void BlendAnimation_DoBlendAnimation_m236696207 (BlendAnimation_t4161375515 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
