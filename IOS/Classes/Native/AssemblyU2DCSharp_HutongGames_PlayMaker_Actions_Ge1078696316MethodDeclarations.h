﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRaycastHitInfo
struct GetRaycastHitInfo_t1078696316;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::.ctor()
extern "C"  void GetRaycastHitInfo__ctor_m154070842 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::Reset()
extern "C"  void GetRaycastHitInfo_Reset_m2095471079 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::StoreRaycastInfo()
extern "C"  void GetRaycastHitInfo_StoreRaycastInfo_m2476388064 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnEnter()
extern "C"  void GetRaycastHitInfo_OnEnter_m1424919697 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnUpdate()
extern "C"  void GetRaycastHitInfo_OnUpdate_m356397010 (GetRaycastHitInfo_t1078696316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
