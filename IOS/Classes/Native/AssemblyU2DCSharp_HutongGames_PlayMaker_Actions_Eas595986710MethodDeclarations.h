﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseFsmAction
struct EaseFsmAction_t595986710;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::.ctor()
extern "C"  void EaseFsmAction__ctor_m1487085792 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::Reset()
extern "C"  void EaseFsmAction_Reset_m3428486029 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnEnter()
extern "C"  void EaseFsmAction_OnEnter_m2552032439 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnExit()
extern "C"  void EaseFsmAction_OnExit_m368337921 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnUpdate()
extern "C"  void EaseFsmAction_OnUpdate_m937153644 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::UpdatePercentage()
extern "C"  void EaseFsmAction_UpdatePercentage_m268259175 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::SetEasingFunction()
extern "C"  void EaseFsmAction_SetEasingFunction_m1325706339 (EaseFsmAction_t595986710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::linear(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_linear_m2867094712 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::clerp(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_clerp_m3165956593 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::spring(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_spring_m1912221904 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuad_m558969507 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuad_m4049571076 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuad_m2004719049 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInCubic_m239140756 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutCubic_m1073606995 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutCubic_m2107703598 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuart_m3744997887 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuart_m284496830 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuart_m1318593433 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuint_m1081181427 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuint_m1915647666 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuint_m2949744269 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInSine_m4156630429 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutSine_m3352264702 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutSine_m1307412675 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInExpo_m999423256 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutExpo_m195057529 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutExpo_m2445172798 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInCirc_m2227763411 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutCirc_m1423397684 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutCirc_m3673512953 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::bounce(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_bounce_m353328501 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInBack_m1267354371 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutBack_m462988644 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutBack_m2713103913 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::punch(System.Single,System.Single)
extern "C"  float EaseFsmAction_punch_m1071373240 (EaseFsmAction_t595986710 * __this, float ___amplitude0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::elastic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_elastic_m1445158614 (EaseFsmAction_t595986710 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
