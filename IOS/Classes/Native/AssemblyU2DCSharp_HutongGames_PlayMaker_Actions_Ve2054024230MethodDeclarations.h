﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Normalize
struct Vector2Normalize_t2054024230;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::.ctor()
extern "C"  void Vector2Normalize__ctor_m4284170496 (Vector2Normalize_t2054024230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::Reset()
extern "C"  void Vector2Normalize_Reset_m1930603437 (Vector2Normalize_t2054024230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::OnEnter()
extern "C"  void Vector2Normalize_OnEnter_m1900905687 (Vector2Normalize_t2054024230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::OnUpdate()
extern "C"  void Vector2Normalize_OnUpdate_m2227060812 (Vector2Normalize_t2054024230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
