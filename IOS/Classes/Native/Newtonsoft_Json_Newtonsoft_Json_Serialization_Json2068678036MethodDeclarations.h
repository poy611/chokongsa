﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t25693564;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t18794611;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalBase__ctor_m1880745372 (JsonSerializerInternalBase_t2068678036 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_DefaultReferenceMappings()
extern "C"  BidirectionalDictionary_2_t25693564 * JsonSerializerInternalBase_get_DefaultReferenceMappings_m159315198 (JsonSerializerInternalBase_t2068678036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::GetErrorContext(System.Object,System.Object,System.String,System.Exception)
extern "C"  ErrorContext_t18794611 * JsonSerializerInternalBase_GetErrorContext_m3309051589 (JsonSerializerInternalBase_t2068678036 * __this, Il2CppObject * ___currentObject0, Il2CppObject * ___member1, String_t* ___path2, Exception_t3991598821 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ClearErrorContext()
extern "C"  void JsonSerializerInternalBase_ClearErrorContext_m476412477 (JsonSerializerInternalBase_t2068678036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::IsErrorHandled(System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,Newtonsoft.Json.IJsonLineInfo,System.String,System.Exception)
extern "C"  bool JsonSerializerInternalBase_IsErrorHandled_m499582104 (JsonSerializerInternalBase_t2068678036 * __this, Il2CppObject * ___currentObject0, JsonContract_t1328848902 * ___contract1, Il2CppObject * ___keyValue2, Il2CppObject * ___lineInfo3, String_t* ___path4, Exception_t3991598821 * ___ex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
