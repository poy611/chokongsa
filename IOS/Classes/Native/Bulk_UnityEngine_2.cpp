﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t667441552;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.Motion
struct Motion_t3026528250;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// UnityEngine.NetworkPlayer[]
struct NetworkPlayerU5BU5D_t132745896;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4125766536;
// UnityEngine.Networking.DownloadHandlerAssetBundle
struct DownloadHandlerAssetBundle_t946683804;
// UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t1243312272;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t4019763368;
// UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_t2552687013;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t1890284502;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t4062689071;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>
struct List_1_t3975180852;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t16481323;
// UnityEngine.NetworkView
struct NetworkView_t3656680617;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Type
struct Type_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t4269758102;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2697150633;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t889400257;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1758559887;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t3680716996;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t3531521085;
// UnityEngine.RangeAttribute
struct RangeAttribute_t912008995;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Collider2D
struct Collider2D_t1552025098;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UnityEngine.RectOffset
struct RectOffset_t3056157787;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t779639188;
// UnityEngine.RectTransform
struct RectTransform_t972643934;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Canvas
struct Canvas_t2727140764;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.RequireComponent
struct RequireComponent_t1687166108;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3731857623;
// UnityEngine.RPC
struct RPC_t3134615963;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2970544072;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t3165457172;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3197444790;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t204085987;
// UnityEngine.GUILayer
struct GUILayer_t2983897946;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t2216353654;
// UnityEngine.SerializeField
struct SerializeField_t3754825534;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2835496234;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1486273289;
// UnityEngine.Event
struct Event_t4196595728;
// UnityEngine.SliderState
struct SliderState_t1233388262;
// UnityEngine.SocialPlatforms.ISocialPlatform
struct ISocialPlatform_t277815499;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2654168339;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t3570684786;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t1726768202;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t1670395707;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t229750097;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2349069933;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t645920862;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t3799088250;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3814920354;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2378268441;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t2957812780;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t344600729;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t2116066607;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t1820874799;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t1185876199;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t3396031228;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t2280656072;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t4279057999;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t250104726;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t1307362368;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3419104218;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion3026528250.h"
#include "UnityEngine_UnityEngine_Motion3026528250MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Network1492793700.h"
#include "UnityEngine_UnityEngine_Network1492793700MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "mscorlib_System_UInt3224667981.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792.h"
#include "mscorlib_System_Double3868226565.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1049203712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection468395618MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4125766536.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4125766536MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"
#include "UnityEngine_UnityEngine_AudioType794660134.h"
#include "mscorlib_System_GC1614687344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandlerA946683804.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandlerA946683804MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Hash128346790303MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1243312272.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1243312272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4019763368.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler4019763368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2552687013.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2552687013MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest1890284502.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest1890284502MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler4062689071.h"
#include "System_System_Text_RegularExpressions_Regex2161232213MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRaw16481323MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRaw16481323.h"
#include "mscorlib_System_Text_Encoding2012439129MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler4062689071MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWWForm461342257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2144973319MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_726430633MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3975180852.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3975180852MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3994853622.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3994853622MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3819726735.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1974256870.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_FormatException3455918062.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_StringComparer4230573202MethodDeclarations.h"
#include "mscorlib_System_StringComparer4230573202.h"
#include "mscorlib_System_Char2862622538.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest_947498394.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest_947498394MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3819726735MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel2722760996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType796297792MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_Type2863145774.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "mscorlib_System_IntPtr4010401971MethodDeclarations.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel4269758102.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335.h"
#include "UnityEngine_UnityEngine_PhysicMaterial211873335MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Physics2D9846735.h"
#include "UnityEngine_UnityEngine_Physics2D9846735MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3111957221MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3111957221.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D1327932246MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngine_Plane4206452690MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3680716996.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555.h"
#include "UnityEngine_UnityEngine_PlayMode1155122555MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3531521085MethodDeclarations.h"
#include "mscorlib_System_Attribute2523058482MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString3369712284MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction577895768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995.h"
#include "UnityEngine_UnityEngine_RangeAttribute912008995MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Collider2D1552025098MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826.h"
#include "UnityEngine_UnityEngine_GUIStyle2990928826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform972643934MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048.h"
#include "UnityEngine_UnityEngine_RectTransformUtility3025555048MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Canvas2727140764.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask3214369688MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2661816155MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy3609427819.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy3609427819MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3324967291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode77252893.h"
#include "UnityEngine_UnityEngine_RenderMode77252893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108.h"
#include "UnityEngine_UnityEngine_RequireComponent1687166108MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ForceMode2134283300.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535.h"
#include "UnityEngine_UnityEngine_ForceMode2D665452726.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D774977535MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_RPC3134615963MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187.h"
#include "UnityEngine_UnityEngine_ScaleMode3023293187MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3067001883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1937996761MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1937996761.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen974975297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen974975297.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen4246757468MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen4246757468.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative3165457172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3197444790MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute204085987MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3965481900MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2093155523.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo3209134097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2216353654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391.h"
#include "UnityEngine_UnityEngine_SetupCoroutine1459791391MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Reflection_Binder1074302268.h"
#include "mscorlib_System_Reflection_ParameterModifier741930026.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Shader3191267369MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1486273289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229.h"
#include "UnityEngine_UnityEngine_SkeletonBone421858229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703.h"
#include "UnityEngine_UnityEngine_SliderHandler783692703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventType637886954.h"
#include "UnityEngine_UnityEngine_Event4196595728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility1028319349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClock4036018645MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_Event4196595728.h"
#include "UnityEngine_UnityEngine_SliderState1233388262.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418.h"
#include "UnityEngine_UnityEngine_GUIContent2094828418MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState1233388262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Social3197796273.h"
#include "UnityEngine_UnityEngine_Social3197796273MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_ActivePlat2714626623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_ActivePlat2714626623.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3189060351.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope1305796361.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2242891083MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie2116066607MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen229750097.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_657441114MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local1307362368.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915.h"
#include "mscorlib_System_Action_1_gen2349069933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729.h"
#include "mscorlib_System_Action_1_gen2349069933.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3481375915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590.h"
#include "mscorlib_System_Action_1_gen645920862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228.h"
#include "mscorlib_System_Action_1_gen645920862.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2181296590MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP2280656072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade1185876199.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope1608660171.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3208733121MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3814920354.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achiev344600729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score3396031228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState1609153288.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range1533311935MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(__this, method) ((  ParticleSystem_t381473177 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t2983897946_m2891371969(__this, method) ((  GUILayer_t2983897946 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2022291967 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m1624944828(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m972795186 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m972795186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3154030143 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3154030143_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m2825545578 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m2825545578_ftn) (MonoBehaviour_t667441552 *, String_t*, float);
	static MonoBehaviour_Invoke_m2825545578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m2825545578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m1115468640 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m1115468640_ftn) (MonoBehaviour_t667441552 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m1115468640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m1115468640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m3230208631 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m972795186(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2461959659 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2461959659_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2461959659_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2461959659_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1460913732 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1460913732_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_IsInvoking_m1460913732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1460913732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m3881121150 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3154030143(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2135303124 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t3621161934 * L_1 = MonoBehaviour_StartCoroutine_Auto_m1831125106(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_Auto_m1831125106 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m1831125106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2964903975 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t3621161934 * (*MonoBehaviour_StartCoroutine_m2964903975_ftn) (MonoBehaviour_t667441552 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m2964903975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m2964903975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t3621161934 * MonoBehaviour_StartCoroutine_m2272783641 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t3621161934 * L_2 = MonoBehaviour_StartCoroutine_m2964903975(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m2790918991 (MonoBehaviour_t667441552 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m2790918991_ftn) (MonoBehaviour_t667441552 *, String_t*);
	static MonoBehaviour_StopCoroutine_m2790918991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m2790918991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1340700766 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1762309278 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t3621161934 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1074098068(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074 (MonoBehaviour_t667441552 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn) (MonoBehaviour_t667441552 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2181342074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1074098068 (MonoBehaviour_t667441552 * __this, Coroutine_t3621161934 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn) (MonoBehaviour_t667441552 *, Coroutine_t3621161934 *);
	static MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1074098068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1437893335 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1437893335_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_StopAllCoroutines_m1437893335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1437893335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m1497342762_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m1497342762 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m1497342762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m4058409766 (MonoBehaviour_t667441552 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m4058409766_ftn) (MonoBehaviour_t667441552 *);
	static MonoBehaviour_get_useGUILayout_m4058409766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m4058409766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m589898551 (MonoBehaviour_t667441552 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m589898551_ftn) (MonoBehaviour_t667441552 *, bool);
	static MonoBehaviour_set_useGUILayout_m589898551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m589898551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Motion::.ctor()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Motion__ctor_m2540972279_MetadataUsageId;
extern "C"  void Motion__ctor_m2540972279 (Motion_t3026528250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Motion__ctor_m2540972279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2183624243 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		NavMeshAgent_INTERNAL_set_velocity_m2849407365(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m2849407365 (NavMeshAgent_t588466745 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn) (NavMeshAgent_t588466745 *, Vector3_t4282066566 *);
	static NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_INTERNAL_set_velocity_m2849407365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Network_InitializeServer_m1086070119 (Il2CppObject * __this /* static, unused */, int32_t ___connections0, int32_t ___listenPort1, bool ___useNat2, const MethodInfo* method)
{
	typedef int32_t (*Network_InitializeServer_m1086070119_ftn) (int32_t, int32_t, bool);
	static Network_InitializeServer_m1086070119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeServer_m1086070119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)");
	return _il2cpp_icall_func(___connections0, ___listenPort1, ___useNat2);
}
// System.Void UnityEngine.Network::set_incomingPassword(System.String)
extern "C"  void Network_set_incomingPassword_m515962091 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_incomingPassword_m515962091_ftn) (String_t*);
	static Network_set_incomingPassword_m515962091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_incomingPassword_m515962091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_incomingPassword(System.String)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)
extern "C"  void Network_set_logLevel_m294590405 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_logLevel_m294590405_ftn) (int32_t);
	static Network_set_logLevel_m294590405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_logLevel_m294590405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::InitializeSecurity()
extern "C"  void Network_InitializeSecurity_m64123113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Network_InitializeSecurity_m64123113_ftn) ();
	static Network_InitializeSecurity_m64123113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeSecurity_m64123113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeSecurity()");
	_il2cpp_icall_func();
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)
extern "C"  int32_t Network_Internal_ConnectToSingleIP_m3562152815 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, int32_t ___localPort2, String_t* ___password3, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_ConnectToSingleIP_m3562152815_ftn) (String_t*, int32_t, int32_t, String_t*);
	static Network_Internal_ConnectToSingleIP_m3562152815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_ConnectToSingleIP_m3562152815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)");
	return _il2cpp_icall_func(___IP0, ___remotePort1, ___localPort2, ___password3);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Connect(System.String,System.Int32,System.String)
extern "C"  int32_t Network_Connect_m3659141856 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, String_t* ___password2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___IP0;
		int32_t L_1 = ___remotePort1;
		String_t* L_2 = ___password2;
		int32_t L_3 = Network_Internal_ConnectToSingleIP_m3562152815(NULL /*static, unused*/, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Network::Disconnect(System.Int32)
extern "C"  void Network_Disconnect_m2062052710 (Il2CppObject * __this /* static, unused */, int32_t ___timeout0, const MethodInfo* method)
{
	typedef void (*Network_Disconnect_m2062052710_ftn) (int32_t);
	static Network_Disconnect_m2062052710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Disconnect_m2062052710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Disconnect(System.Int32)");
	_il2cpp_icall_func(___timeout0);
}
// System.Void UnityEngine.Network::Disconnect()
extern "C"  void Network_Disconnect_m2068213653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)200);
		int32_t L_0 = V_0;
		Network_Disconnect_m2062052710(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)
extern "C"  void Network_CloseConnection_m1870717282 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___target0, bool ___sendDisconnectionNotification1, const MethodInfo* method)
{
	typedef void (*Network_CloseConnection_m1870717282_ftn) (NetworkPlayer_t3231273765 , bool);
	static Network_CloseConnection_m1870717282_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_CloseConnection_m1870717282_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)");
	_il2cpp_icall_func(___target0, ___sendDisconnectionNotification1);
}
// UnityEngine.NetworkPlayer[] UnityEngine.Network::get_connections()
extern "C"  NetworkPlayerU5BU5D_t132745896* Network_get_connections_m4276471278 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef NetworkPlayerU5BU5D_t132745896* (*Network_get_connections_m4276471278_ftn) ();
	static Network_get_connections_m4276471278_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_connections_m4276471278_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_connections()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::Internal_GetPlayer()
extern "C"  int32_t Network_Internal_GetPlayer_m776499950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_GetPlayer_m776499950_ftn) ();
	static Network_Internal_GetPlayer_m776499950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetPlayer_m776499950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetPlayer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPlayer UnityEngine.Network::get_player()
extern "C"  NetworkPlayer_t3231273765  Network_get_player_m1200723560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Network_Internal_GetPlayer_m776499950(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_index_0(L_0);
		NetworkPlayer_t3231273765  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Network::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  Object_t3071478659 * Network_Instantiate_m2753034863 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	{
		Object_t3071478659 * L_0 = ___prefab0;
		int32_t L_1 = ___group3;
		Object_t3071478659 * L_2 = Network_INTERNAL_CALL_Instantiate_m4087140154(NULL /*static, unused*/, L_0, (&___position1), (&___rotation2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  Object_t3071478659 * Network_INTERNAL_CALL_Instantiate_m4087140154 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___prefab0, Vector3_t4282066566 * ___position1, Quaternion_t1553702882 * ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Network_INTERNAL_CALL_Instantiate_m4087140154_ftn) (Object_t3071478659 *, Vector3_t4282066566 *, Quaternion_t1553702882 *, int32_t);
	static Network_INTERNAL_CALL_Instantiate_m4087140154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Instantiate_m4087140154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)");
	return _il2cpp_icall_func(___prefab0, ___position1, ___rotation2, ___group3);
}
// System.Void UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)
extern "C"  void Network_DestroyPlayerObjects_m73641416 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method)
{
	typedef void (*Network_DestroyPlayerObjects_m73641416_ftn) (NetworkPlayer_t3231273765 );
	static Network_DestroyPlayerObjects_m73641416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_DestroyPlayerObjects_m73641416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)");
	_il2cpp_icall_func(___playerID0);
}
// System.Void UnityEngine.Network::Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID,System.UInt32)
extern "C"  void Network_Internal_RemoveRPCs_m3085275014 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436  ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = ___playerID0;
		uint32_t L_1 = ___channelMask2;
		Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935(NULL /*static, unused*/, L_0, (&___viewID1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)
extern "C"  void Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, NetworkViewID_t3400394436 * ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	typedef void (*Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn) (NetworkPlayer_t3231273765 , NetworkViewID_t3400394436 *, uint32_t);
	static Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Internal_RemoveRPCs_m315609935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)");
	_il2cpp_icall_func(___playerID0, ___viewID1, ___channelMask2);
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkPlayer)
extern "C"  void Network_RemoveRPCs_m3795657583 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___playerID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = ___playerID0;
		NetworkViewID_t3400394436  L_1 = NetworkViewID_get_unassigned_m2302461037(NULL /*static, unused*/, /*hidden argument*/NULL);
		Network_Internal_RemoveRPCs_m3085275014(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkViewID)
extern "C"  void Network_RemoveRPCs_m448463792 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = NetworkPlayer_get_unassigned_m865556847(NULL /*static, unused*/, /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_1 = ___viewID0;
		Network_Internal_RemoveRPCs_m3085275014(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Network::get_isClient()
extern "C"  bool Network_get_isClient_m4143111185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isClient_m4143111185_ftn) ();
	static Network_get_isClient_m4143111185_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isClient_m4143111185_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isClient()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Network::get_isServer()
extern "C"  bool Network_get_isServer_m318839177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isServer_m318839177_ftn) ();
	static Network_get_isServer_m318839177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isServer_m318839177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isServer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPeerType UnityEngine.Network::get_peerType()
extern "C"  int32_t Network_get_peerType_m115170590 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_peerType_m115170590_ftn) ();
	static Network_get_peerType_m115170590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_peerType_m115170590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_peerType()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::SetLevelPrefix(System.Int32)
extern "C"  void Network_SetLevelPrefix_m3013656638 (Il2CppObject * __this /* static, unused */, int32_t ___prefix0, const MethodInfo* method)
{
	typedef void (*Network_SetLevelPrefix_m3013656638_ftn) (int32_t);
	static Network_SetLevelPrefix_m3013656638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_SetLevelPrefix_m3013656638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::SetLevelPrefix(System.Int32)");
	_il2cpp_icall_func(___prefix0);
}
// System.Int32 UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetLastPing_m3340324755 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetLastPing_m3340324755_ftn) (NetworkPlayer_t3231273765 );
	static Network_GetLastPing_m3340324755_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetLastPing_m3340324755_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Int32 UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetAveragePing_m3083558248 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetAveragePing_m3083558248_ftn) (NetworkPlayer_t3231273765 );
	static Network_GetAveragePing_m3083558248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetAveragePing_m3083558248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Single UnityEngine.Network::get_sendRate()
extern "C"  float Network_get_sendRate_m1227422814 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Network_get_sendRate_m1227422814_ftn) ();
	static Network_get_sendRate_m1227422814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_sendRate_m1227422814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_sendRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_sendRate(System.Single)
extern "C"  void Network_set_sendRate_m2111190029 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_sendRate_m2111190029_ftn) (float);
	static Network_set_sendRate_m2111190029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_sendRate_m2111190029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_sendRate(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::get_isMessageQueueRunning()
extern "C"  bool Network_get_isMessageQueueRunning_m5081841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isMessageQueueRunning_m5081841_ftn) ();
	static Network_get_isMessageQueueRunning_m5081841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isMessageQueueRunning_m5081841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isMessageQueueRunning()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)
extern "C"  void Network_set_isMessageQueueRunning_m1610613326 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_isMessageQueueRunning_m1610613326_ftn) (bool);
	static Network_set_isMessageQueueRunning_m1610613326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_isMessageQueueRunning_m1610613326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::Internal_GetTime(System.Double&)
extern "C"  void Network_Internal_GetTime_m2753432228 (Il2CppObject * __this /* static, unused */, double* ___t0, const MethodInfo* method)
{
	typedef void (*Network_Internal_GetTime_m2753432228_ftn) (double*);
	static Network_Internal_GetTime_m2753432228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetTime_m2753432228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetTime(System.Double&)");
	_il2cpp_icall_func(___t0);
}
// System.Double UnityEngine.Network::get_time()
extern "C"  double Network_get_time_m4227697516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Network_Internal_GetTime_m2753432228(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		double L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.Network::get_minimumAllocatableViewIDs()
extern "C"  int32_t Network_get_minimumAllocatableViewIDs_m2986052927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_minimumAllocatableViewIDs_m2986052927_ftn) ();
	static Network_get_minimumAllocatableViewIDs_m2986052927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_minimumAllocatableViewIDs_m2986052927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_minimumAllocatableViewIDs()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)
extern "C"  void Network_set_minimumAllocatableViewIDs_m2130893980 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_minimumAllocatableViewIDs_m2130893980_ftn) (int32_t);
	static Network_set_minimumAllocatableViewIDs_m2130893980_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_minimumAllocatableViewIDs_m2130893980_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::HavePublicAddress()
extern "C"  bool Network_HavePublicAddress_m1494895090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_HavePublicAddress_m1494895090_ftn) ();
	static Network_HavePublicAddress_m1494895090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_HavePublicAddress_m1494895090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::HavePublicAddress()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::get_maxConnections()
extern "C"  int32_t Network_get_maxConnections_m3468981299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_maxConnections_m3468981299_ftn) ();
	static Network_get_maxConnections_m3468981299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_maxConnections_m3468981299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_maxConnections()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_maxConnections(System.Int32)
extern "C"  void Network_set_maxConnections_m2445464568 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_maxConnections_m2445464568_ftn) (int32_t);
	static Network_set_maxConnections_m2445464568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_maxConnections_m2445464568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_maxConnections(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Networking.DownloadHandler::.ctor()
extern "C"  void DownloadHandler__ctor_m2530543973 (DownloadHandler_t4125766536 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateBuffer()
extern "C"  void DownloadHandler_InternalCreateBuffer_m90273976 (DownloadHandler_t4125766536 * __this, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateBuffer_m90273976_ftn) (DownloadHandler_t4125766536 *);
	static DownloadHandler_InternalCreateBuffer_m90273976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateBuffer_m90273976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateBuffer()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateTexture(System.Boolean)
extern "C"  void DownloadHandler_InternalCreateTexture_m3794532220 (DownloadHandler_t4125766536 * __this, bool ___readable0, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateTexture_m3794532220_ftn) (DownloadHandler_t4125766536 *, bool);
	static DownloadHandler_InternalCreateTexture_m3794532220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateTexture_m3794532220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateTexture(System.Boolean)");
	_il2cpp_icall_func(__this, ___readable0);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,System.UInt32)
extern "C"  void DownloadHandler_InternalCreateAssetBundle_m2968064314 (DownloadHandler_t4125766536 * __this, String_t* ___url0, uint32_t ___crc1, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateAssetBundle_m2968064314_ftn) (DownloadHandler_t4125766536 *, String_t*, uint32_t);
	static DownloadHandler_InternalCreateAssetBundle_m2968064314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateAssetBundle_m2968064314_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,System.UInt32)");
	_il2cpp_icall_func(__this, ___url0, ___crc1);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,UnityEngine.Hash128,System.UInt32)
extern "C"  void DownloadHandler_InternalCreateAssetBundle_m4164801108 (DownloadHandler_t4125766536 * __this, String_t* ___url0, Hash128_t346790303  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		uint32_t L_1 = ___crc2;
		DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m900957164(NULL /*static, unused*/, __this, L_0, (&___hash1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::INTERNAL_CALL_InternalCreateAssetBundle(UnityEngine.Networking.DownloadHandler,System.String,UnityEngine.Hash128&,System.UInt32)
extern "C"  void DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m900957164 (Il2CppObject * __this /* static, unused */, DownloadHandler_t4125766536 * ___self0, String_t* ___url1, Hash128_t346790303 * ___hash2, uint32_t ___crc3, const MethodInfo* method)
{
	typedef void (*DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m900957164_ftn) (DownloadHandler_t4125766536 *, String_t*, Hash128_t346790303 *, uint32_t);
	static DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m900957164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m900957164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::INTERNAL_CALL_InternalCreateAssetBundle(UnityEngine.Networking.DownloadHandler,System.String,UnityEngine.Hash128&,System.UInt32)");
	_il2cpp_icall_func(___self0, ___url1, ___hash2, ___crc3);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAudioClip(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandler_InternalCreateAudioClip_m3659398341 (DownloadHandler_t4125766536 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateAudioClip_m3659398341_ftn) (DownloadHandler_t4125766536 *, String_t*, int32_t);
	static DownloadHandler_InternalCreateAudioClip_m3659398341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateAudioClip_m3659398341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateAudioClip(System.String,UnityEngine.AudioType)");
	_il2cpp_icall_func(__this, ___url0, ___audioType1);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalDestroy()
extern "C"  void DownloadHandler_InternalDestroy_m2735807296 (DownloadHandler_t4125766536 * __this, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalDestroy_m2735807296_ftn) (DownloadHandler_t4125766536 *);
	static DownloadHandler_InternalDestroy_m2735807296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalDestroy_m2735807296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.DownloadHandler::Finalize()
extern "C"  void DownloadHandler_Finalize_m2272026845 (DownloadHandler_t4125766536 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DownloadHandler_InternalDestroy_m2735807296(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::Dispose()
extern "C"  void DownloadHandler_Dispose_m799551586 (DownloadHandler_t4125766536 * __this, const MethodInfo* method)
{
	{
		DownloadHandler_InternalDestroy_m2735807296(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t4125766536_marshal_pinvoke(const DownloadHandler_t4125766536& unmarshaled, DownloadHandler_t4125766536_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t4125766536_marshal_pinvoke_back(const DownloadHandler_t4125766536_marshaled_pinvoke& marshaled, DownloadHandler_t4125766536& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t4125766536_marshal_pinvoke_cleanup(DownloadHandler_t4125766536_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t4125766536_marshal_com(const DownloadHandler_t4125766536& unmarshaled, DownloadHandler_t4125766536_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t4125766536_marshal_com_back(const DownloadHandler_t4125766536_marshaled_com& marshaled, DownloadHandler_t4125766536& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t4125766536_marshal_com_cleanup(DownloadHandler_t4125766536_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m1343790375 (DownloadHandlerAssetBundle_t946683804 * __this, String_t* ___url0, uint32_t ___crc1, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		uint32_t L_1 = ___crc1;
		DownloadHandler_InternalCreateAssetBundle_m2968064314(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m3076523803 (DownloadHandlerAssetBundle_t946683804 * __this, String_t* ___url0, uint32_t ___version1, uint32_t ___crc2, const MethodInfo* method)
{
	Hash128_t346790303  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		uint32_t L_0 = ___version1;
		Hash128__ctor_m425960966((&V_0), 0, 0, 0, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___url0;
		Hash128_t346790303  L_2 = V_0;
		uint32_t L_3 = ___crc2;
		DownloadHandler_InternalCreateAssetBundle_m4164801108(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,UnityEngine.Hash128,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m3772065217 (DownloadHandlerAssetBundle_t946683804 * __this, String_t* ___url0, Hash128_t346790303  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		Hash128_t346790303  L_1 = ___hash1;
		uint32_t L_2 = ___crc2;
		DownloadHandler_InternalCreateAssetBundle_m4164801108(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_pinvoke(const DownloadHandlerAssetBundle_t946683804& unmarshaled, DownloadHandlerAssetBundle_t946683804_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_pinvoke_back(const DownloadHandlerAssetBundle_t946683804_marshaled_pinvoke& marshaled, DownloadHandlerAssetBundle_t946683804& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_pinvoke_cleanup(DownloadHandlerAssetBundle_t946683804_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_com(const DownloadHandlerAssetBundle_t946683804& unmarshaled, DownloadHandlerAssetBundle_t946683804_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_com_back(const DownloadHandlerAssetBundle_t946683804_marshaled_com& marshaled, DownloadHandlerAssetBundle_t946683804& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t946683804_marshal_com_cleanup(DownloadHandlerAssetBundle_t946683804_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::.ctor(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip__ctor_m2530782554 (DownloadHandlerAudioClip_t1243312272 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		int32_t L_1 = ___audioType1;
		DownloadHandler_InternalCreateAudioClip_m3659398341(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_pinvoke(const DownloadHandlerAudioClip_t1243312272& unmarshaled, DownloadHandlerAudioClip_t1243312272_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_pinvoke_back(const DownloadHandlerAudioClip_t1243312272_marshaled_pinvoke& marshaled, DownloadHandlerAudioClip_t1243312272& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_pinvoke_cleanup(DownloadHandlerAudioClip_t1243312272_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_com(const DownloadHandlerAudioClip_t1243312272& unmarshaled, DownloadHandlerAudioClip_t1243312272_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_com_back(const DownloadHandlerAudioClip_t1243312272_marshaled_com& marshaled, DownloadHandlerAudioClip_t1243312272& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1243312272_marshal_com_cleanup(DownloadHandlerAudioClip_t1243312272_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m890192773 (DownloadHandlerBuffer_t4019763368 * __this, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		DownloadHandler_InternalCreateBuffer_m90273976(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_pinvoke(const DownloadHandlerBuffer_t4019763368& unmarshaled, DownloadHandlerBuffer_t4019763368_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_pinvoke_back(const DownloadHandlerBuffer_t4019763368_marshaled_pinvoke& marshaled, DownloadHandlerBuffer_t4019763368& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_pinvoke_cleanup(DownloadHandlerBuffer_t4019763368_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_com(const DownloadHandlerBuffer_t4019763368& unmarshaled, DownloadHandlerBuffer_t4019763368_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_com_back(const DownloadHandlerBuffer_t4019763368_marshaled_com& marshaled, DownloadHandlerBuffer_t4019763368& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t4019763368_marshal_com_cleanup(DownloadHandlerBuffer_t4019763368_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerTexture::.ctor(System.Boolean)
extern "C"  void DownloadHandlerTexture__ctor_m1620444605 (DownloadHandlerTexture_t2552687013 * __this, bool ___readable0, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m2530543973(__this, /*hidden argument*/NULL);
		bool L_0 = ___readable0;
		DownloadHandler_InternalCreateTexture_m3794532220(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2552687013_marshal_pinvoke(const DownloadHandlerTexture_t2552687013& unmarshaled, DownloadHandlerTexture_t2552687013_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerTexture_t2552687013_marshal_pinvoke_back(const DownloadHandlerTexture_t2552687013_marshaled_pinvoke& marshaled, DownloadHandlerTexture_t2552687013& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2552687013_marshal_pinvoke_cleanup(DownloadHandlerTexture_t2552687013_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2552687013_marshal_com(const DownloadHandlerTexture_t2552687013& unmarshaled, DownloadHandlerTexture_t2552687013_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerTexture_t2552687013_marshal_com_back(const DownloadHandlerTexture_t2552687013_marshaled_com& marshaled, DownloadHandlerTexture_t2552687013& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2552687013_marshal_com_cleanup(DownloadHandlerTexture_t2552687013_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor()
extern "C"  void UnityWebRequest__ctor_m1876705495 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2803869286(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m2130746124(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String)
extern "C"  void UnityWebRequest__ctor_m800174987 (UnityWebRequest_t1890284502 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2803869286(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m2130746124(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2069928379(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
extern "C"  void UnityWebRequest__ctor_m2883079303 (UnityWebRequest_t1890284502 * __this, String_t* ___url0, String_t* ___method1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2803869286(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m2130746124(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2069928379(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___method1;
		UnityWebRequest_set_method_m1722009751(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Networking.DownloadHandler,UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest__ctor_m3561636576 (UnityWebRequest_t1890284502 * __this, String_t* ___url0, String_t* ___method1, DownloadHandler_t4125766536 * ___downloadHandler2, UploadHandler_t4062689071 * ___uploadHandler3, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2803869286(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m2130746124(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2069928379(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___method1;
		UnityWebRequest_set_method_m1722009751(__this, L_1, /*hidden argument*/NULL);
		DownloadHandler_t4125766536 * L_2 = ___downloadHandler2;
		UnityWebRequest_set_downloadHandler_m1376606139(__this, L_2, /*hidden argument*/NULL);
		UploadHandler_t4062689071 * L_3 = ___uploadHandler3;
		UnityWebRequest_set_uploadHandler_m2165710363(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.cctor()
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1366882409;
extern Il2CppCodeGenString* _stringLiteral124285319;
extern Il2CppCodeGenString* _stringLiteral1742157935;
extern Il2CppCodeGenString* _stringLiteral892651224;
extern Il2CppCodeGenString* _stringLiteral3519315678;
extern Il2CppCodeGenString* _stringLiteral3162187450;
extern Il2CppCodeGenString* _stringLiteral3076014;
extern Il2CppCodeGenString* _stringLiteral99626;
extern Il2CppCodeGenString* _stringLiteral3005803609;
extern Il2CppCodeGenString* _stringLiteral3208616;
extern Il2CppCodeGenString* _stringLiteral211181701;
extern Il2CppCodeGenString* _stringLiteral3286347558;
extern Il2CppCodeGenString* _stringLiteral1085069613;
extern Il2CppCodeGenString* _stringLiteral3697;
extern Il2CppCodeGenString* _stringLiteral3227751731;
extern Il2CppCodeGenString* _stringLiteral1274458357;
extern Il2CppCodeGenString* _stringLiteral4063795740;
extern Il2CppCodeGenString* _stringLiteral486342275;
extern Il2CppCodeGenString* _stringLiteral116750;
extern Il2CppCodeGenString* _stringLiteral1376014827;
extern const uint32_t UnityWebRequest__cctor_m1861199286_MetadataUsageId;
extern "C"  void UnityWebRequest__cctor_m1861199286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest__cctor_m1861199286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Regex_t2161232213 * L_0 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
		Regex__ctor_m574010660(L_0, _stringLiteral1366882409, /*hidden argument*/NULL);
		((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->set_domainRegex_7(L_0);
		StringU5BU5D_t4054002952* L_1 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19)));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral124285319);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral124285319);
		StringU5BU5D_t4054002952* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1742157935);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1742157935);
		StringU5BU5D_t4054002952* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral892651224);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral892651224);
		StringU5BU5D_t4054002952* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, _stringLiteral3519315678);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3519315678);
		StringU5BU5D_t4054002952* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 4);
		ArrayElementTypeCheck (L_5, _stringLiteral3162187450);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3162187450);
		StringU5BU5D_t4054002952* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 5);
		ArrayElementTypeCheck (L_6, _stringLiteral3076014);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral3076014);
		StringU5BU5D_t4054002952* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 6);
		ArrayElementTypeCheck (L_7, _stringLiteral99626);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral99626);
		StringU5BU5D_t4054002952* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 7);
		ArrayElementTypeCheck (L_8, _stringLiteral3005803609);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral3005803609);
		StringU5BU5D_t4054002952* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 8);
		ArrayElementTypeCheck (L_9, _stringLiteral3208616);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral3208616);
		StringU5BU5D_t4054002952* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)9));
		ArrayElementTypeCheck (L_10, _stringLiteral211181701);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral211181701);
		StringU5BU5D_t4054002952* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)10));
		ArrayElementTypeCheck (L_11, _stringLiteral3286347558);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral3286347558);
		StringU5BU5D_t4054002952* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)11));
		ArrayElementTypeCheck (L_12, _stringLiteral1085069613);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral1085069613);
		StringU5BU5D_t4054002952* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)12));
		ArrayElementTypeCheck (L_13, _stringLiteral3697);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral3697);
		StringU5BU5D_t4054002952* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)13));
		ArrayElementTypeCheck (L_14, _stringLiteral3227751731);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral3227751731);
		StringU5BU5D_t4054002952* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)14));
		ArrayElementTypeCheck (L_15, _stringLiteral1274458357);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral1274458357);
		StringU5BU5D_t4054002952* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)15));
		ArrayElementTypeCheck (L_16, _stringLiteral4063795740);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral4063795740);
		StringU5BU5D_t4054002952* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)16));
		ArrayElementTypeCheck (L_17, _stringLiteral486342275);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral486342275);
		StringU5BU5D_t4054002952* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)17));
		ArrayElementTypeCheck (L_18, _stringLiteral116750);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral116750);
		StringU5BU5D_t4054002952* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)18));
		ArrayElementTypeCheck (L_19, _stringLiteral1376014827);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral1376014827);
		((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->set_forbiddenHeaderKeys_8(L_19);
		return;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_Get_m242769177_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Get_m242769177 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Get_m242769177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t4019763368 * L_1 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_1, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_2 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_2, L_0, _stringLiteral70454, L_1, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_2;
		UnityWebRequest_t1890284502 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Delete(System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2012838315;
extern const uint32_t UnityWebRequest_Delete_m263716936_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Delete_m263716936 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Delete_m263716936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2012838315, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t1890284502 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Head(System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2213344;
extern const uint32_t UnityWebRequest_Head_m587951091_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Head_m587951091 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Head_m587951091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2213344, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t1890284502 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetTexture(System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GetTexture_m3181785294_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetTexture_m3181785294 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetTexture_m3181785294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest_t1890284502 * L_1 = UnityWebRequest_GetTexture_m1775545583(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetTexture(System.String,System.Boolean)
extern Il2CppClass* DownloadHandlerTexture_t2552687013_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_GetTexture_m1775545583_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetTexture_m1775545583 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, bool ___nonReadable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetTexture_m1775545583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		bool L_1 = ___nonReadable1;
		DownloadHandlerTexture_t2552687013 * L_2 = (DownloadHandlerTexture_t2552687013 *)il2cpp_codegen_object_new(DownloadHandlerTexture_t2552687013_il2cpp_TypeInfo_var);
		DownloadHandlerTexture__ctor_m1620444605(L_2, L_1, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_3 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_3, L_0, _stringLiteral70454, L_2, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_3;
		UnityWebRequest_t1890284502 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAudioClip(System.String,UnityEngine.AudioType)
extern Il2CppClass* DownloadHandlerAudioClip_t1243312272_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_GetAudioClip_m1644470838_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetAudioClip_m1644470838 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, int32_t ___audioType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAudioClip_m1644470838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		int32_t L_2 = ___audioType1;
		DownloadHandlerAudioClip_t1243312272 * L_3 = (DownloadHandlerAudioClip_t1243312272 *)il2cpp_codegen_object_new(DownloadHandlerAudioClip_t1243312272_il2cpp_TypeInfo_var);
		DownloadHandlerAudioClip__ctor_m2530782554(L_3, L_1, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_4 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_4, L_0, _stringLiteral70454, L_3, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t1890284502 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GetAssetBundle_m3001500023_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetAssetBundle_m3001500023 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m3001500023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest_t1890284502 * L_1 = UnityWebRequest_GetAssetBundle_m3192971115(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_GetAssetBundle_m3192971115_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetAssetBundle_m3192971115 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, uint32_t ___crc1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m3192971115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		uint32_t L_2 = ___crc1;
		DownloadHandlerAssetBundle_t946683804 * L_3 = (DownloadHandlerAssetBundle_t946683804 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m1343790375(L_3, L_1, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_4 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_4, L_0, _stringLiteral70454, L_3, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t1890284502 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,System.UInt32,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_GetAssetBundle_m1397031519_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetAssetBundle_m1397031519 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, uint32_t ___version1, uint32_t ___crc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m1397031519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		uint32_t L_2 = ___version1;
		uint32_t L_3 = ___crc2;
		DownloadHandlerAssetBundle_t946683804 * L_4 = (DownloadHandlerAssetBundle_t946683804 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m3076523803(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_5 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_5, L_0, _stringLiteral70454, L_4, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		UnityWebRequest_t1890284502 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,UnityEngine.Hash128,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral70454;
extern const uint32_t UnityWebRequest_GetAssetBundle_m3405356549_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_GetAssetBundle_m3405356549 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, Hash128_t346790303  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m3405356549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		Hash128_t346790303  L_2 = ___hash1;
		uint32_t L_3 = ___crc2;
		DownloadHandlerAssetBundle_t946683804 * L_4 = (DownloadHandlerAssetBundle_t946683804 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t946683804_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m3772065217(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_5 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_5, L_0, _stringLiteral70454, L_4, (UploadHandler_t4062689071 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		UnityWebRequest_t1890284502 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.Byte[])
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral79599;
extern const uint32_t UnityWebRequest_Put_m2257537347_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Put_m2257537347 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, ByteU5BU5D_t4260760469* ___bodyData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Put_m2257537347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t4019763368 * L_1 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_2 = ___bodyData1;
		UploadHandlerRaw_t16481323 * L_3 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_3, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_4 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_4, L_0, _stringLiteral79599, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t1890284502 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.String)
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral79599;
extern const uint32_t UnityWebRequest_Put_m20375676_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Put_m20375676 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, String_t* ___bodyData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Put_m20375676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t4019763368 * L_1 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_2 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___bodyData1;
		NullCheck(L_2);
		ByteU5BU5D_t4260760469* L_4 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, L_3);
		UploadHandlerRaw_t16481323 * L_5 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_5, L_4, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_6 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3561636576(L_6, L_0, _stringLiteral79599, L_1, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		UnityWebRequest_t1890284502 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t609724394_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern Il2CppCodeGenString* _stringLiteral2809397470;
extern const uint32_t UnityWebRequest_Post_m1220303919_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Post_m1220303919 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, String_t* ___postData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m1220303919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2461856, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___postData1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_3 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t609724394_il2cpp_TypeInfo_var);
		String_t* L_4 = WWWTranscoder_URLEncode_m3301913818(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		UnityWebRequest_t1890284502 * L_5 = V_0;
		Encoding_t2012439129 * L_6 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t4260760469* L_8 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, L_7);
		UploadHandlerRaw_t16481323 * L_9 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_9, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityWebRequest_set_uploadHandler_m2165710363(L_5, L_9, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_10 = V_0;
		NullCheck(L_10);
		UploadHandler_t4062689071 * L_11 = UnityWebRequest_get_uploadHandler_m1049527436(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		UploadHandler_set_contentType_m3087243632(L_11, _stringLiteral2809397470, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_12 = V_0;
		DownloadHandlerBuffer_t4019763368 * L_13 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityWebRequest_set_downloadHandler_m1376606139(L_12, L_13, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_14 = V_0;
		return L_14;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2144973319_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern const uint32_t UnityWebRequest_Post_m1253059835_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Post_m1253059835 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, WWWForm_t461342257 * ___formData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m1253059835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	Dictionary_2_t827649927 * V_1 = NULL;
	KeyValuePair_2_t726430633  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t2144973319  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2461856, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t1890284502 * L_2 = V_0;
		WWWForm_t461342257 * L_3 = ___formData1;
		NullCheck(L_3);
		ByteU5BU5D_t4260760469* L_4 = WWWForm_get_data_m2893811951(L_3, /*hidden argument*/NULL);
		UploadHandlerRaw_t16481323 * L_5 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityWebRequest_set_uploadHandler_m2165710363(L_2, L_5, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_6 = V_0;
		DownloadHandlerBuffer_t4019763368 * L_7 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityWebRequest_set_downloadHandler_m1376606139(L_6, L_7, /*hidden argument*/NULL);
		WWWForm_t461342257 * L_8 = ___formData1;
		NullCheck(L_8);
		Dictionary_2_t827649927 * L_9 = WWWForm_get_headers_m370408569(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Dictionary_2_t827649927 * L_10 = V_1;
		NullCheck(L_10);
		Enumerator_t2144973319  L_11 = Dictionary_2_GetEnumerator_m2759194411(L_10, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_3 = L_11;
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0057;
		}

IL_003b:
		{
			KeyValuePair_2_t726430633  L_12 = Enumerator_get_Current_m2871721525((&V_3), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_2 = L_12;
			UnityWebRequest_t1890284502 * L_13 = V_0;
			String_t* L_14 = KeyValuePair_2_get_Key_m1739472607((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			String_t* L_15 = KeyValuePair_2_get_Value_m730091314((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			NullCheck(L_13);
			UnityWebRequest_SetRequestHeader_m704789911(L_13, L_14, L_15, /*hidden argument*/NULL);
		}

IL_0057:
		{
			bool L_16 = Enumerator_MoveNext_m2577713898((&V_3), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_16)
			{
				goto IL_003b;
			}
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		Enumerator_t2144973319  L_17 = V_3;
		Enumerator_t2144973319  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t2144973319_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(104)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0074:
	{
		UnityWebRequest_t1890284502 * L_20 = V_0;
		return L_20;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_Post_m2284933306_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Post_m2284933306 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, List_1_t3975180852 * ___multipartFormSections1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m2284933306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_0 = UnityWebRequest_GenerateBoundary_m3873910070(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___uri0;
		List_1_t3975180852 * L_2 = ___multipartFormSections1;
		ByteU5BU5D_t4260760469* L_3 = V_0;
		UnityWebRequest_t1890284502 * L_4 = UnityWebRequest_Post_m1323437117(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern Il2CppCodeGenString* _stringLiteral1604320318;
extern const uint32_t UnityWebRequest_Post_m1323437117_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Post_m1323437117 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, List_1_t3975180852 * ___multipartFormSections1, ByteU5BU5D_t4260760469* ___boundary2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m1323437117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	UploadHandler_t4062689071 * V_2 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2461856, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t3975180852 * L_2 = ___multipartFormSections1;
		ByteU5BU5D_t4260760469* L_3 = ___boundary2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_4 = UnityWebRequest_SerializeFormSections_m1305467155(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		ByteU5BU5D_t4260760469* L_5 = V_1;
		UploadHandlerRaw_t16481323 * L_6 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_6, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		UploadHandler_t4062689071 * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_8 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_9 = ___boundary2;
		ByteU5BU5D_t4260760469* L_10 = ___boundary2;
		NullCheck(L_10);
		NullCheck(L_8);
		String_t* L_11 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1604320318, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		UploadHandler_set_contentType_m3087243632(L_7, L_12, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_13 = V_0;
		UploadHandler_t4062689071 * L_14 = V_2;
		NullCheck(L_13);
		UnityWebRequest_set_uploadHandler_m2165710363(L_13, L_14, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_15 = V_0;
		DownloadHandlerBuffer_t4019763368 * L_16 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		UnityWebRequest_set_downloadHandler_m1376606139(L_15, L_16, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern Il2CppCodeGenString* _stringLiteral2809397470;
extern const uint32_t UnityWebRequest_Post_m3509843812_MetadataUsageId;
extern "C"  UnityWebRequest_t1890284502 * UnityWebRequest_Post_m3509843812 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, Dictionary_2_t827649927 * ___formFields1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m3509843812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t1890284502 * V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	UploadHandler_t4062689071 * V_2 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t1890284502 * L_1 = (UnityWebRequest_t1890284502 *)il2cpp_codegen_object_new(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2883079303(L_1, L_0, _stringLiteral2461856, /*hidden argument*/NULL);
		V_0 = L_1;
		Dictionary_2_t827649927 * L_2 = ___formFields1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_3 = UnityWebRequest_SerializeSimpleForm_m302816210(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		ByteU5BU5D_t4260760469* L_4 = V_1;
		UploadHandlerRaw_t16481323 * L_5 = (UploadHandlerRaw_t16481323 *)il2cpp_codegen_object_new(UploadHandlerRaw_t16481323_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m1657900425(L_5, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		UploadHandler_t4062689071 * L_6 = V_2;
		NullCheck(L_6);
		UploadHandler_set_contentType_m3087243632(L_6, _stringLiteral2809397470, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_7 = V_0;
		UploadHandler_t4062689071 * L_8 = V_2;
		NullCheck(L_7);
		UnityWebRequest_set_uploadHandler_m2165710363(L_7, L_8, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_9 = V_0;
		DownloadHandlerBuffer_t4019763368 * L_10 = (DownloadHandlerBuffer_t4019763368 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t4019763368_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m890192773(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityWebRequest_set_downloadHandler_m1376606139(L_9, L_10, /*hidden argument*/NULL);
		UnityWebRequest_t1890284502 * L_11 = V_0;
		return L_11;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeFormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3994853622_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4230795212_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4024305532_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1077715830_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1723286054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m67201024_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m326996843_MethodInfo_var;
extern const MethodInfo* List_1_TrimExcess_m1706927532_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m782372363_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral1763073811;
extern Il2CppCodeGenString* _stringLiteral3143036;
extern Il2CppCodeGenString* _stringLiteral257360521;
extern Il2CppCodeGenString* _stringLiteral1324075733;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral4209993937;
extern Il2CppCodeGenString* _stringLiteral1491620852;
extern const uint32_t UnityWebRequest_SerializeFormSections_m1305467155_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* UnityWebRequest_SerializeFormSections_m1305467155 (Il2CppObject * __this /* static, unused */, List_1_t3975180852 * ___multipartFormSections0, ByteU5BU5D_t4260760469* ___boundary1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SerializeFormSections_m1305467155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	Enumerator_t3994853622  V_3;
	memset(&V_3, 0, sizeof(V_3));
	List_1_t4230795212 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Enumerator_t3994853622  V_6;
	memset(&V_6, 0, sizeof(V_6));
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral413);
		V_0 = L_1;
		V_1 = 0;
		List_1_t3975180852 * L_2 = ___multipartFormSections0;
		NullCheck(L_2);
		Enumerator_t3994853622  L_3 = List_1_GetEnumerator_m4024305532(L_2, /*hidden argument*/List_1_GetEnumerator_m4024305532_MethodInfo_var);
		V_3 = L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_001e:
		{
			Il2CppObject * L_4 = Enumerator_get_Current_m1077715830((&V_3), /*hidden argument*/Enumerator_get_Current_m1077715830_MethodInfo_var);
			V_2 = L_4;
			int32_t L_5 = V_1;
			Il2CppObject * L_6 = V_2;
			NullCheck(L_6);
			ByteU5BU5D_t4260760469* L_7 = InterfaceFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(1 /* System.Byte[] UnityEngine.Networking.IMultipartFormSection::get_sectionData() */, IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var, L_6);
			NullCheck(L_7);
			V_1 = ((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)((int32_t)64)+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))))));
		}

IL_0034:
		{
			bool L_8 = Enumerator_MoveNext_m1723286054((&V_3), /*hidden argument*/Enumerator_MoveNext_m1723286054_MethodInfo_var);
			if (L_8)
			{
				goto IL_001e;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_t3994853622  L_9 = V_3;
		Enumerator_t3994853622  L_10 = L_9;
		Il2CppObject * L_11 = Box(Enumerator_t3994853622_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Il2CppObject *)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0051:
	{
		int32_t L_12 = V_1;
		List_1_t4230795212 * L_13 = (List_1_t4230795212 *)il2cpp_codegen_object_new(List_1_t4230795212_il2cpp_TypeInfo_var);
		List_1__ctor_m67201024(L_13, L_12, /*hidden argument*/List_1__ctor_m67201024_MethodInfo_var);
		V_4 = L_13;
		List_1_t3975180852 * L_14 = ___multipartFormSections0;
		NullCheck(L_14);
		Enumerator_t3994853622  L_15 = List_1_GetEnumerator_m4024305532(L_14, /*hidden argument*/List_1_GetEnumerator_m4024305532_MethodInfo_var);
		V_6 = L_15;
	}

IL_0061:
	try
	{ // begin try (depth: 1)
		{
			goto IL_015c;
		}

IL_0066:
		{
			Il2CppObject * L_16 = Enumerator_get_Current_m1077715830((&V_6), /*hidden argument*/Enumerator_get_Current_m1077715830_MethodInfo_var);
			V_5 = L_16;
			V_7 = _stringLiteral1763073811;
			Il2CppObject * L_17 = V_5;
			NullCheck(L_17);
			String_t* L_18 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Networking.IMultipartFormSection::get_sectionName() */, IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var, L_17);
			V_8 = L_18;
			Il2CppObject * L_19 = V_5;
			NullCheck(L_19);
			String_t* L_20 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String UnityEngine.Networking.IMultipartFormSection::get_fileName() */, IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var, L_19);
			V_9 = L_20;
			String_t* L_21 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_22 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			if (L_22)
			{
				goto IL_009b;
			}
		}

IL_0094:
		{
			V_7 = _stringLiteral3143036;
		}

IL_009b:
		{
			String_t* L_23 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral257360521, L_23, /*hidden argument*/NULL);
			V_10 = L_24;
			String_t* L_25 = V_8;
			bool L_26 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			if (L_26)
			{
				goto IL_00ca;
			}
		}

IL_00b5:
		{
			String_t* L_27 = V_10;
			String_t* L_28 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_29 = String_Concat_m2933632197(NULL /*static, unused*/, L_27, _stringLiteral1324075733, L_28, _stringLiteral34, /*hidden argument*/NULL);
			V_10 = L_29;
		}

IL_00ca:
		{
			String_t* L_30 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_31 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			if (L_31)
			{
				goto IL_00eb;
			}
		}

IL_00d6:
		{
			String_t* L_32 = V_10;
			String_t* L_33 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_34 = String_Concat_m2933632197(NULL /*static, unused*/, L_32, _stringLiteral4209993937, L_33, _stringLiteral34, /*hidden argument*/NULL);
			V_10 = L_34;
		}

IL_00eb:
		{
			String_t* L_35 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m138640077(NULL /*static, unused*/, L_35, _stringLiteral413, /*hidden argument*/NULL);
			V_10 = L_36;
			Il2CppObject * L_37 = V_5;
			NullCheck(L_37);
			String_t* L_38 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Networking.IMultipartFormSection::get_contentType() */, IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var, L_37);
			V_11 = L_38;
			String_t* L_39 = V_11;
			bool L_40 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
			if (L_40)
			{
				goto IL_0123;
			}
		}

IL_010e:
		{
			String_t* L_41 = V_10;
			String_t* L_42 = V_11;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_43 = String_Concat_m2933632197(NULL /*static, unused*/, L_41, _stringLiteral1491620852, L_42, _stringLiteral413, /*hidden argument*/NULL);
			V_10 = L_43;
		}

IL_0123:
		{
			List_1_t4230795212 * L_44 = V_4;
			ByteU5BU5D_t4260760469* L_45 = ___boundary1;
			NullCheck(L_44);
			List_1_AddRange_m326996843(L_44, (Il2CppObject*)(Il2CppObject*)L_45, /*hidden argument*/List_1_AddRange_m326996843_MethodInfo_var);
			List_1_t4230795212 * L_46 = V_4;
			ByteU5BU5D_t4260760469* L_47 = V_0;
			NullCheck(L_46);
			List_1_AddRange_m326996843(L_46, (Il2CppObject*)(Il2CppObject*)L_47, /*hidden argument*/List_1_AddRange_m326996843_MethodInfo_var);
			List_1_t4230795212 * L_48 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
			Encoding_t2012439129 * L_49 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_50 = V_10;
			NullCheck(L_49);
			ByteU5BU5D_t4260760469* L_51 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_49, L_50);
			NullCheck(L_48);
			List_1_AddRange_m326996843(L_48, (Il2CppObject*)(Il2CppObject*)L_51, /*hidden argument*/List_1_AddRange_m326996843_MethodInfo_var);
			List_1_t4230795212 * L_52 = V_4;
			ByteU5BU5D_t4260760469* L_53 = V_0;
			NullCheck(L_52);
			List_1_AddRange_m326996843(L_52, (Il2CppObject*)(Il2CppObject*)L_53, /*hidden argument*/List_1_AddRange_m326996843_MethodInfo_var);
			List_1_t4230795212 * L_54 = V_4;
			Il2CppObject * L_55 = V_5;
			NullCheck(L_55);
			ByteU5BU5D_t4260760469* L_56 = InterfaceFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(1 /* System.Byte[] UnityEngine.Networking.IMultipartFormSection::get_sectionData() */, IMultipartFormSection_t2606995300_il2cpp_TypeInfo_var, L_55);
			NullCheck(L_54);
			List_1_AddRange_m326996843(L_54, (Il2CppObject*)(Il2CppObject*)L_56, /*hidden argument*/List_1_AddRange_m326996843_MethodInfo_var);
		}

IL_015c:
		{
			bool L_57 = Enumerator_MoveNext_m1723286054((&V_6), /*hidden argument*/Enumerator_MoveNext_m1723286054_MethodInfo_var);
			if (L_57)
			{
				goto IL_0066;
			}
		}

IL_0168:
		{
			IL2CPP_LEAVE(0x17A, FINALLY_016d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_016d;
	}

FINALLY_016d:
	{ // begin finally (depth: 1)
		Enumerator_t3994853622  L_58 = V_6;
		Enumerator_t3994853622  L_59 = L_58;
		Il2CppObject * L_60 = Box(Enumerator_t3994853622_il2cpp_TypeInfo_var, &L_59);
		NullCheck((Il2CppObject *)L_60);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_60);
		IL2CPP_END_FINALLY(365)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(365)
	{
		IL2CPP_JUMP_TBL(0x17A, IL_017a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_017a:
	{
		List_1_t4230795212 * L_61 = V_4;
		NullCheck(L_61);
		List_1_TrimExcess_m1706927532(L_61, /*hidden argument*/List_1_TrimExcess_m1706927532_MethodInfo_var);
		List_1_t4230795212 * L_62 = V_4;
		NullCheck(L_62);
		ByteU5BU5D_t4260760469* L_63 = List_1_ToArray_m782372363(L_62, /*hidden argument*/List_1_ToArray_m782372363_MethodInfo_var);
		return L_63;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::GenerateBoundary()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GenerateBoundary_m3873910070_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* UnityWebRequest_GenerateBoundary_m3873910070 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GenerateBoundary_m3873910070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)40)));
		V_1 = 0;
		goto IL_003a;
	}

IL_000f:
	{
		int32_t L_0 = Random_Range_m75452833(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_2 = V_2;
		V_2 = ((int32_t)((int32_t)L_2+(int32_t)7));
	}

IL_0025:
	{
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)6));
	}

IL_0031:
	{
		ByteU5BU5D_t4260760469* L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)(((int32_t)((uint8_t)L_7))));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)40))))
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_10 = V_0;
		return L_10;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeSimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2144973319_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral61;
extern const uint32_t UnityWebRequest_SerializeSimpleForm_m302816210_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* UnityWebRequest_SerializeSimpleForm_m302816210 (Il2CppObject * __this /* static, unused */, Dictionary_2_t827649927 * ___formFields0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SerializeSimpleForm_m302816210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t726430633  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2144973319  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Dictionary_2_t827649927 * L_1 = ___formFields0;
		NullCheck(L_1);
		Enumerator_t2144973319  L_2 = Dictionary_2_GetEnumerator_m2759194411(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_2 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0012:
		{
			KeyValuePair_2_t726430633  L_3 = Enumerator_get_Current_m2871721525((&V_2), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_1 = L_3;
			String_t* L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
			if ((((int32_t)L_5) <= ((int32_t)0)))
			{
				goto IL_0032;
			}
		}

IL_0026:
		{
			String_t* L_6 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_7 = String_Concat_m138640077(NULL /*static, unused*/, L_6, _stringLiteral38, /*hidden argument*/NULL);
			V_0 = L_7;
		}

IL_0032:
		{
			String_t* L_8 = V_0;
			String_t* L_9 = KeyValuePair_2_get_Key_m1739472607((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
			String_t* L_10 = Uri_EscapeDataString_m3238747918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			String_t* L_11 = KeyValuePair_2_get_Value_m730091314((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			String_t* L_12 = Uri_EscapeDataString_m3238747918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Concat_m2933632197(NULL /*static, unused*/, L_8, L_10, _stringLiteral61, L_12, /*hidden argument*/NULL);
			V_0 = L_13;
		}

IL_0056:
		{
			bool L_14 = Enumerator_MoveNext_m2577713898((&V_2), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_14)
			{
				goto IL_0012;
			}
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		Enumerator_t2144973319  L_15 = V_2;
		Enumerator_t2144973319  L_16 = L_15;
		Il2CppObject * L_17 = Box(Enumerator_t2144973319_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Il2CppObject *)L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
		IL2CPP_END_FINALLY(103)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_18 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = V_0;
		NullCheck(L_18);
		ByteU5BU5D_t4260760469* L_20 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_18, L_19);
		return L_20;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeDownloadHandlerOnDispose_m2347066627 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeDownloadHandlerOnDispose_m4263077100 (UnityWebRequest_t1890284502 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeUploadHandlerOnDispose_m2617950012 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeUploadHandlerOnDispose_m3879152613 (UnityWebRequest_t1890284502 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalCreate()
extern "C"  void UnityWebRequest_InternalCreate_m2803869286 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalCreate_m2803869286_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalCreate_m2803869286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalCreate_m2803869286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalCreate()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy()
extern "C"  void UnityWebRequest_InternalDestroy_m2409049138 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalDestroy_m2409049138_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalDestroy_m2409049138_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalDestroy_m2409049138_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetDefaults()
extern "C"  void UnityWebRequest_InternalSetDefaults_m2130746124 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		UnityWebRequest_set_disposeDownloadHandlerOnDispose_m4263077100(__this, (bool)1, /*hidden argument*/NULL);
		UnityWebRequest_set_disposeUploadHandlerOnDispose_m3879152613(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Finalize()
extern "C"  void UnityWebRequest_Finalize_m1446616107 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		UnityWebRequest_InternalDestroy_m2409049138(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
extern "C"  void UnityWebRequest_Dispose_m3820966740 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	DownloadHandler_t4125766536 * V_0 = NULL;
	UploadHandler_t4062689071 * V_1 = NULL;
	{
		bool L_0 = UnityWebRequest_get_disposeDownloadHandlerOnDispose_m2347066627(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		DownloadHandler_t4125766536 * L_1 = UnityWebRequest_get_downloadHandler_m2799190910(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		DownloadHandler_t4125766536 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		DownloadHandler_t4125766536 * L_3 = V_0;
		NullCheck(L_3);
		DownloadHandler_Dispose_m799551586(L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		bool L_4 = UnityWebRequest_get_disposeUploadHandlerOnDispose_m2617950012(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		UploadHandler_t4062689071 * L_5 = UnityWebRequest_get_uploadHandler_m1049527436(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		UploadHandler_t4062689071 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		UploadHandler_t4062689071 * L_7 = V_1;
		NullCheck(L_7);
		UploadHandler_Dispose_m2552457883(L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		UnityWebRequest_InternalDestroy_m2409049138(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Networking.UnityWebRequest::InternalBegin()
extern "C"  AsyncOperation_t3699081103 * UnityWebRequest_InternalBegin_m3730330388 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*UnityWebRequest_InternalBegin_m3730330388_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalBegin_m3730330388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalBegin_m3730330388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalBegin()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalAbort()
extern "C"  void UnityWebRequest_InternalAbort_m2162080680 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalAbort_m2162080680_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalAbort_m2162080680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalAbort_m2162080680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalAbort()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.AsyncOperation UnityEngine.Networking.UnityWebRequest::Send()
extern "C"  AsyncOperation_t3699081103 * UnityWebRequest_Send_m3727536514 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		AsyncOperation_t3699081103 * L_0 = UnityWebRequest_InternalBegin_m3730330388(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Abort()
extern "C"  void UnityWebRequest_Abort_m1526250789 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		UnityWebRequest_InternalAbort_m2162080680(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)
extern "C"  void UnityWebRequest_InternalSetMethod_m2772078444 (UnityWebRequest_t1890284502 * __this, int32_t ___methodType0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetMethod_m2772078444_ftn) (UnityWebRequest_t1890284502 *, int32_t);
	static UnityWebRequest_InternalSetMethod_m2772078444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetMethod_m2772078444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)");
	_il2cpp_icall_func(__this, ___methodType0);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
extern "C"  void UnityWebRequest_InternalSetCustomMethod_m1516182 (UnityWebRequest_t1890284502 * __this, String_t* ___customMethodName0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetCustomMethod_m1516182_ftn) (UnityWebRequest_t1890284502 *, String_t*);
	static UnityWebRequest_InternalSetCustomMethod_m1516182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetCustomMethod_m1516182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)");
	_il2cpp_icall_func(__this, ___customMethodName0);
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::InternalGetMethod()
extern "C"  int32_t UnityWebRequest_InternalGetMethod_m3295898255 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_InternalGetMethod_m3295898255_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalGetMethod_m3295898255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetMethod_m3295898255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetMethod()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::InternalGetCustomMethod()
extern "C"  String_t* UnityWebRequest_InternalGetCustomMethod_m1233973213 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_InternalGetCustomMethod_m1233973213_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalGetCustomMethod_m1233973213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetCustomMethod_m1233973213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetCustomMethod()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_method()
extern Il2CppCodeGenString* _stringLiteral70454;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern Il2CppCodeGenString* _stringLiteral79599;
extern Il2CppCodeGenString* _stringLiteral2213344;
extern const uint32_t UnityWebRequest_get_method_m2179159226_MetadataUsageId;
extern "C"  String_t* UnityWebRequest_get_method_m2179159226 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_get_method_m2179159226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = UnityWebRequest_InternalGetMethod_m3295898255(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0024;
		}
		if (L_2 == 1)
		{
			goto IL_002a;
		}
		if (L_2 == 2)
		{
			goto IL_0030;
		}
		if (L_2 == 3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003c;
	}

IL_0024:
	{
		return _stringLiteral70454;
	}

IL_002a:
	{
		return _stringLiteral2461856;
	}

IL_0030:
	{
		return _stringLiteral79599;
	}

IL_0036:
	{
		return _stringLiteral2213344;
	}

IL_003c:
	{
		String_t* L_3 = UnityWebRequest_InternalGetCustomMethod_m1233973213(__this, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_method(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1974256870_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4235384975_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m337170132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4070750959;
extern Il2CppCodeGenString* _stringLiteral70454;
extern Il2CppCodeGenString* _stringLiteral2461856;
extern Il2CppCodeGenString* _stringLiteral79599;
extern Il2CppCodeGenString* _stringLiteral2213344;
extern const uint32_t UnityWebRequest_set_method_m1722009751_MetadataUsageId;
extern "C"  void UnityWebRequest_set_method_m1722009751 (UnityWebRequest_t1890284502 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_set_method_m1722009751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t1974256870 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, _stringLiteral4070750959, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		String_t* L_4 = String_ToUpper_m1841663596(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_00c7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		Dictionary_2_t1974256870 * L_6 = ((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_11();
		if (L_6)
		{
			goto IL_006a;
		}
	}
	{
		Dictionary_2_t1974256870 * L_7 = (Dictionary_2_t1974256870 *)il2cpp_codegen_object_new(Dictionary_2_t1974256870_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_7, 4, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_7;
		Dictionary_2_t1974256870 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m4235384975(L_8, _stringLiteral70454, 0, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m4235384975(L_9, _stringLiteral2461856, 1, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_Add_m4235384975(L_10, _stringLiteral79599, 2, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_11 = V_1;
		NullCheck(L_11);
		Dictionary_2_Add_m4235384975(L_11, _stringLiteral2213344, 3, /*hidden argument*/Dictionary_2_Add_m4235384975_MethodInfo_var);
		Dictionary_2_t1974256870 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map1_11(L_12);
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		Dictionary_2_t1974256870 * L_13 = ((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_11();
		String_t* L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = Dictionary_2_TryGetValue_m337170132(L_13, L_14, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m337170132_MethodInfo_var);
		if (!L_15)
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_16 = V_2;
		if (L_16 == 0)
		{
			goto IL_0097;
		}
		if (L_16 == 1)
		{
			goto IL_00a3;
		}
		if (L_16 == 2)
		{
			goto IL_00af;
		}
		if (L_16 == 3)
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_00c7;
	}

IL_0097:
	{
		UnityWebRequest_InternalSetMethod_m2772078444(__this, 0, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00a3:
	{
		UnityWebRequest_InternalSetMethod_m2772078444(__this, 1, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00af:
	{
		UnityWebRequest_InternalSetMethod_m2772078444(__this, 2, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00bb:
	{
		UnityWebRequest_InternalSetMethod_m2772078444(__this, 3, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00c7:
	{
		String_t* L_17 = ___value0;
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m1841663596(L_17, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetCustomMethod_m1516182(__this, L_18, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00d8:
	{
		return;
	}
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::InternalGetError()
extern "C"  int32_t UnityWebRequest_InternalGetError_m2798049916 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_InternalGetError_m2798049916_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalGetError_m2798049916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetError_m2798049916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetError()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C"  String_t* UnityWebRequest_get_error_m3454762737 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_get_error_m3454762737_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_error_m3454762737_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_error_m3454762737_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_error()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_useHttpContinue()
extern "C"  bool UnityWebRequest_get_useHttpContinue_m1179324476 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_useHttpContinue_m1179324476_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_useHttpContinue_m1179324476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_useHttpContinue_m1179324476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_useHttpContinue()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_useHttpContinue(System.Boolean)
extern "C"  void UnityWebRequest_set_useHttpContinue_m2050235493 (UnityWebRequest_t1890284502 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_useHttpContinue_m2050235493_ftn) (UnityWebRequest_t1890284502 *, bool);
	static UnityWebRequest_set_useHttpContinue_m2050235493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_useHttpContinue_m2050235493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_useHttpContinue(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_url()
extern "C"  String_t* UnityWebRequest_get_url_m2722270872 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = UnityWebRequest_InternalGetUrl_m3811648454(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_url(System.String)
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t3455918062_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2601158286;
extern Il2CppCodeGenString* _stringLiteral1504;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral57242;
extern const uint32_t UnityWebRequest_set_url_m2069928379_MetadataUsageId;
extern "C"  void UnityWebRequest_set_url_m2069928379 (UnityWebRequest_t1890284502 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_set_url_m2069928379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Uri_t1116831938 * V_2 = NULL;
	Uri_t1116831938 * V_3 = NULL;
	FormatException_t3455918062 * V_4 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___value0;
		V_0 = L_0;
		V_1 = _stringLiteral2601158286;
		String_t* L_1 = V_1;
		Uri_t1116831938 * L_2 = (Uri_t1116831938 *)il2cpp_codegen_object_new(Uri_t1116831938_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_2, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1500793453(L_3, _stringLiteral1504, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		Uri_t1116831938 * L_5 = V_2;
		NullCheck(L_5);
		String_t* L_6 = Uri_get_Scheme_m2606456870(L_5, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1825781833(NULL /*static, unused*/, L_6, _stringLiteral58, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0031:
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1500793453(L_9, _stringLiteral47, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Uri_t1116831938 * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = Uri_get_Scheme_m2606456870(L_11, /*hidden argument*/NULL);
		Uri_t1116831938 * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = Uri_get_Host_m3136333645(L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2933632197(NULL /*static, unused*/, L_12, _stringLiteral57242, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		Regex_t2161232213 * L_17 = ((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->get_domainRegex_7();
		String_t* L_18 = V_0;
		NullCheck(L_17);
		bool L_19 = Regex_IsMatch_m2967892253(L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_007b;
		}
	}
	{
		Uri_t1116831938 * L_20 = V_2;
		NullCheck(L_20);
		String_t* L_21 = Uri_get_Scheme_m2606456870(L_20, /*hidden argument*/NULL);
		String_t* L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1825781833(NULL /*static, unused*/, L_21, _stringLiteral57242, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_007b:
	{
		V_3 = (Uri_t1116831938 *)NULL;
	}

IL_007d:
	try
	{ // begin try (depth: 1)
		String_t* L_24 = V_0;
		Uri_t1116831938 * L_25 = (Uri_t1116831938 *)il2cpp_codegen_object_new(Uri_t1116831938_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_25, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		goto IL_00a6;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t3455918062_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0089;
		throw e;
	}

CATCH_0089:
	{ // begin catch(System.FormatException)
		{
			V_4 = ((FormatException_t3455918062 *)__exception_local);
		}

IL_008b:
		try
		{ // begin try (depth: 2)
			Uri_t1116831938 * L_26 = V_2;
			String_t* L_27 = V_0;
			Uri_t1116831938 * L_28 = (Uri_t1116831938 *)il2cpp_codegen_object_new(Uri_t1116831938_il2cpp_TypeInfo_var);
			Uri__ctor_m2876888702(L_28, L_26, L_27, /*hidden argument*/NULL);
			V_3 = L_28;
			goto IL_00a1;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (FormatException_t3455918062_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0098;
			throw e;
		}

CATCH_0098:
		{ // begin catch(System.FormatException)
			{
				FormatException_t3455918062 * L_29 = V_4;
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
			}

IL_009c:
			{
				goto IL_00a1;
			}
		} // end catch (depth: 2)

IL_00a1:
		{
			goto IL_00a6;
		}
	} // end catch (depth: 1)

IL_00a6:
	{
		Uri_t1116831938 * L_30 = V_3;
		NullCheck(L_30);
		String_t* L_31 = Uri_get_AbsoluteUri_m2228269430(L_30, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetUrl_m786692491(__this, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::InternalGetUrl()
extern "C"  String_t* UnityWebRequest_InternalGetUrl_m3811648454 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_InternalGetUrl_m3811648454_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalGetUrl_m3811648454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetUrl_m3811648454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetUrl()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)
extern "C"  void UnityWebRequest_InternalSetUrl_m786692491 (UnityWebRequest_t1890284502 * __this, String_t* ___url0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetUrl_m786692491_ftn) (UnityWebRequest_t1890284502 *, String_t*);
	static UnityWebRequest_InternalSetUrl_m786692491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetUrl_m786692491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)");
	_il2cpp_icall_func(__this, ___url0);
}
// System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
extern "C"  int64_t UnityWebRequest_get_responseCode_m3154397829 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef int64_t (*UnityWebRequest_get_responseCode_m3154397829_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_responseCode_m3154397829_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_responseCode_m3154397829_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_responseCode()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Networking.UnityWebRequest::get_uploadProgress()
extern "C"  float UnityWebRequest_get_uploadProgress_m3424452368 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef float (*UnityWebRequest_get_uploadProgress_m3424452368_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_uploadProgress_m3424452368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadProgress_m3424452368_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadProgress()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable()
extern "C"  bool UnityWebRequest_get_isModifiable_m1721967914 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isModifiable_m1721967914_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_isModifiable_m1721967914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isModifiable_m1721967914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isModifiable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
extern "C"  bool UnityWebRequest_get_isDone_m3067820008 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isDone_m3067820008_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_isDone_m3067820008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isDone_m3067820008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isError()
extern "C"  bool UnityWebRequest_get_isError_m1590593988 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isError_m1590593988_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_isError_m1590593988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isError_m1590593988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isError()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Networking.UnityWebRequest::get_downloadProgress()
extern "C"  float UnityWebRequest_get_downloadProgress_m2117726359 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef float (*UnityWebRequest_get_downloadProgress_m2117726359_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_downloadProgress_m2117726359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadProgress_m2117726359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadProgress()");
	return _il2cpp_icall_func(__this);
}
// System.UInt64 UnityEngine.Networking.UnityWebRequest::get_uploadedBytes()
extern "C"  uint64_t UnityWebRequest_get_uploadedBytes_m320219387 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef uint64_t (*UnityWebRequest_get_uploadedBytes_m320219387_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_uploadedBytes_m320219387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadedBytes_m320219387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadedBytes()");
	return _il2cpp_icall_func(__this);
}
// System.UInt64 UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()
extern "C"  uint64_t UnityWebRequest_get_downloadedBytes_m835542676 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef uint64_t (*UnityWebRequest_get_downloadedBytes_m835542676_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_downloadedBytes_m835542676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadedBytes_m835542676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::get_redirectLimit()
extern "C"  int32_t UnityWebRequest_get_redirectLimit_m800465451 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_get_redirectLimit_m800465451_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_redirectLimit_m800465451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_redirectLimit_m800465451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_redirectLimit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32)
extern "C"  void UnityWebRequest_set_redirectLimit_m1472631432 (UnityWebRequest_t1890284502 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_redirectLimit_m1472631432_ftn) (UnityWebRequest_t1890284502 *, int32_t);
	static UnityWebRequest_set_redirectLimit_m1472631432_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_redirectLimit_m1472631432_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_chunkedTransfer()
extern "C"  bool UnityWebRequest_get_chunkedTransfer_m3211590493 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_chunkedTransfer_m3211590493_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_chunkedTransfer_m3211590493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_chunkedTransfer_m3211590493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_chunkedTransfer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_chunkedTransfer(System.Boolean)
extern "C"  void UnityWebRequest_set_chunkedTransfer_m3848763846 (UnityWebRequest_t1890284502 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_chunkedTransfer_m3848763846_ftn) (UnityWebRequest_t1890284502 *, bool);
	static UnityWebRequest_set_chunkedTransfer_m3848763846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_chunkedTransfer_m3848763846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_chunkedTransfer(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Networking.UnityWebRequest::GetRequestHeader(System.String)
extern "C"  String_t* UnityWebRequest_GetRequestHeader_m3609839564 (UnityWebRequest_t1890284502 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_GetRequestHeader_m3609839564_ftn) (UnityWebRequest_t1890284502 *, String_t*);
	static UnityWebRequest_GetRequestHeader_m3609839564_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_GetRequestHeader_m3609839564_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::GetRequestHeader(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_InternalSetRequestHeader_m2365080026 (UnityWebRequest_t1890284502 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetRequestHeader_m2365080026_ftn) (UnityWebRequest_t1890284502 *, String_t*, String_t*);
	static UnityWebRequest_InternalSetRequestHeader_m2365080026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetRequestHeader_m2365080026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)");
	_il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3672883731;
extern Il2CppCodeGenString* _stringLiteral400642104;
extern Il2CppCodeGenString* _stringLiteral1405805607;
extern Il2CppCodeGenString* _stringLiteral2417738468;
extern Il2CppCodeGenString* _stringLiteral1023641100;
extern const uint32_t UnityWebRequest_SetRequestHeader_m704789911_MetadataUsageId;
extern "C"  void UnityWebRequest_SetRequestHeader_m704789911 (UnityWebRequest_t1890284502 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SetRequestHeader_m704789911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, _stringLiteral3672883731, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value1;
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t928607144 * L_4 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_4, _stringLiteral400642104, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		String_t* L_5 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		bool L_6 = UnityWebRequest_IsHeaderNameLegal_m1178792156(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_7 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1405805607, L_7, _stringLiteral2417738468, /*hidden argument*/NULL);
		ArgumentException_t928607144 * L_9 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0048:
	{
		String_t* L_10 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		bool L_11 = UnityWebRequest_IsHeaderValueLegal_m2225854992(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005e;
		}
	}
	{
		ArgumentException_t928607144 * L_12 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_12, _stringLiteral1023641100, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005e:
	{
		String_t* L_13 = ___name0;
		String_t* L_14 = ___value1;
		UnityWebRequest_InternalSetRequestHeader_m2365080026(__this, L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::GetResponseHeader(System.String)
extern "C"  String_t* UnityWebRequest_GetResponseHeader_m2057835244 (UnityWebRequest_t1890284502 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_GetResponseHeader_m2057835244_ftn) (UnityWebRequest_t1890284502 *, String_t*);
	static UnityWebRequest_GetResponseHeader_m2057835244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_GetResponseHeader_m2057835244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::GetResponseHeader(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.String[] UnityEngine.Networking.UnityWebRequest::InternalGetResponseHeaderKeys()
extern "C"  StringU5BU5D_t4054002952* UnityWebRequest_InternalGetResponseHeaderKeys_m3036141519 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef StringU5BU5D_t4054002952* (*UnityWebRequest_InternalGetResponseHeaderKeys_m3036141519_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_InternalGetResponseHeaderKeys_m3036141519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetResponseHeaderKeys_m3036141519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetResponseHeaderKeys()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Networking.UnityWebRequest::GetResponseHeaders()
extern Il2CppClass* StringComparer_t4230573202_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3526041211_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m346907281_MethodInfo_var;
extern const uint32_t UnityWebRequest_GetResponseHeaders_m3107028988_MetadataUsageId;
extern "C"  Dictionary_2_t827649927 * UnityWebRequest_GetResponseHeaders_m3107028988 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetResponseHeaders_m3107028988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	Dictionary_2_t827649927 * V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = UnityWebRequest_InternalGetResponseHeaderKeys_m3036141519(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		StringU5BU5D_t4054002952* L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (Dictionary_2_t827649927 *)NULL;
	}

IL_000f:
	{
		StringU5BU5D_t4054002952* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t4230573202_il2cpp_TypeInfo_var);
		StringComparer_t4230573202 * L_3 = StringComparer_get_OrdinalIgnoreCase_m2513153269(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t827649927 * L_4 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3526041211(L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), L_3, /*hidden argument*/Dictionary_2__ctor_m3526041211_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		goto IL_003c;
	}

IL_0024:
	{
		StringU5BU5D_t4054002952* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		String_t* L_9 = UnityWebRequest_GetResponseHeader_m2057835244(__this, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Dictionary_2_t827649927 * L_10 = V_1;
		StringU5BU5D_t4054002952* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		String_t* L_15 = V_3;
		NullCheck(L_10);
		Dictionary_2_Add_m346907281(L_10, L_14, L_15, /*hidden argument*/Dictionary_2_Add_m346907281_MethodInfo_var);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_17 = V_2;
		StringU5BU5D_t4054002952* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		Dictionary_2_t827649927 * L_19 = V_1;
		return L_19;
	}
}
// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::get_uploadHandler()
extern "C"  UploadHandler_t4062689071 * UnityWebRequest_get_uploadHandler_m1049527436 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef UploadHandler_t4062689071 * (*UnityWebRequest_get_uploadHandler_m1049527436_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_uploadHandler_m1049527436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadHandler_m1049527436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadHandler()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest_set_uploadHandler_m2165710363 (UnityWebRequest_t1890284502 * __this, UploadHandler_t4062689071 * ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_uploadHandler_m2165710363_ftn) (UnityWebRequest_t1890284502 *, UploadHandler_t4062689071 *);
	static UnityWebRequest_set_uploadHandler_m2165710363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_uploadHandler_m2165710363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t4125766536 * UnityWebRequest_get_downloadHandler_m2799190910 (UnityWebRequest_t1890284502 * __this, const MethodInfo* method)
{
	typedef DownloadHandler_t4125766536 * (*UnityWebRequest_get_downloadHandler_m2799190910_ftn) (UnityWebRequest_t1890284502 *);
	static UnityWebRequest_get_downloadHandler_m2799190910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadHandler_m2799190910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadHandler()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
extern "C"  void UnityWebRequest_set_downloadHandler_m1376606139 (UnityWebRequest_t1890284502 * __this, DownloadHandler_t4125766536 * ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_downloadHandler_m1376606139_ftn) (UnityWebRequest_t1890284502 *, DownloadHandler_t4125766536 *);
	static UnityWebRequest_set_downloadHandler_m1376606139_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_downloadHandler_m1376606139_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::ContainsForbiddenCharacters(System.String,System.Int32)
extern "C"  bool UnityWebRequest_ContainsForbiddenCharacters_m630797512 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___firstAllowedCharCode1, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___s0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0026;
	}

IL_0009:
	{
		String_t* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m3015341861(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppChar L_4 = V_0;
		int32_t L_5 = ___firstAllowedCharCode1;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppChar L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)127)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (bool)1;
	}

IL_0022:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_8 = V_2;
		String_t* L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2979997331(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::IsHeaderNameLegal(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3526140;
extern Il2CppCodeGenString* _stringLiteral3315172223;
extern const uint32_t UnityWebRequest_IsHeaderNameLegal_m1178792156_MetadataUsageId;
extern "C"  bool UnityWebRequest_IsHeaderNameLegal_m1178792156 (Il2CppObject * __this /* static, unused */, String_t* ___headerName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_IsHeaderNameLegal_m1178792156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___headerName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		String_t* L_2 = ___headerName0;
		NullCheck(L_2);
		String_t* L_3 = String_ToLower_m2421900555(L_2, /*hidden argument*/NULL);
		___headerName0 = L_3;
		String_t* L_4 = ___headerName0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		bool L_5 = UnityWebRequest_ContainsForbiddenCharacters_m630797512(NULL /*static, unused*/, L_4, ((int32_t)33), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		String_t* L_6 = ___headerName0;
		NullCheck(L_6);
		bool L_7 = String_StartsWith_m1500793453(L_6, _stringLiteral3526140, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_8 = ___headerName0;
		NullCheck(L_8);
		bool L_9 = String_StartsWith_m1500793453(L_8, _stringLiteral3315172223, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0046;
		}
	}

IL_0044:
	{
		return (bool)0;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		StringU5BU5D_t4054002952* L_10 = ((UnityWebRequest_t1890284502_StaticFields*)UnityWebRequest_t1890284502_il2cpp_TypeInfo_var->static_fields)->get_forbiddenHeaderKeys_8();
		V_1 = L_10;
		V_2 = 0;
		goto IL_0069;
	}

IL_0053:
	{
		StringU5BU5D_t4054002952* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = L_14;
		String_t* L_15 = ___headerName0;
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_Equals_m1002918753(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0065;
		}
	}
	{
		return (bool)0;
	}

IL_0065:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0069:
	{
		int32_t L_19 = V_2;
		StringU5BU5D_t4054002952* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::IsHeaderValueLegal(System.String)
extern Il2CppClass* UnityWebRequest_t1890284502_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_IsHeaderValueLegal_m2225854992_MetadataUsageId;
extern "C"  bool UnityWebRequest_IsHeaderValueLegal_m2225854992 (Il2CppObject * __this /* static, unused */, String_t* ___headerValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_IsHeaderValueLegal_m2225854992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___headerValue0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t1890284502_il2cpp_TypeInfo_var);
		bool L_1 = UnityWebRequest_ContainsForbiddenCharacters_m630797512(NULL /*static, unused*/, L_0, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		return (bool)1;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::GetErrorDescription(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError)
extern Il2CppCodeGenString* _stringLiteral3507245801;
extern Il2CppCodeGenString* _stringLiteral4105688187;
extern Il2CppCodeGenString* _stringLiteral234342368;
extern Il2CppCodeGenString* _stringLiteral3171180777;
extern Il2CppCodeGenString* _stringLiteral3916089169;
extern Il2CppCodeGenString* _stringLiteral3187963024;
extern Il2CppCodeGenString* _stringLiteral2893261125;
extern Il2CppCodeGenString* _stringLiteral822965253;
extern Il2CppCodeGenString* _stringLiteral4152508176;
extern Il2CppCodeGenString* _stringLiteral1733288993;
extern Il2CppCodeGenString* _stringLiteral3812645704;
extern Il2CppCodeGenString* _stringLiteral4072787224;
extern Il2CppCodeGenString* _stringLiteral1002489873;
extern Il2CppCodeGenString* _stringLiteral2375081285;
extern Il2CppCodeGenString* _stringLiteral2212451623;
extern Il2CppCodeGenString* _stringLiteral4162316597;
extern Il2CppCodeGenString* _stringLiteral2960162492;
extern Il2CppCodeGenString* _stringLiteral296893764;
extern Il2CppCodeGenString* _stringLiteral2307651436;
extern Il2CppCodeGenString* _stringLiteral2960302138;
extern Il2CppCodeGenString* _stringLiteral1775835880;
extern Il2CppCodeGenString* _stringLiteral3851188946;
extern Il2CppCodeGenString* _stringLiteral1209898337;
extern Il2CppCodeGenString* _stringLiteral2922949545;
extern Il2CppCodeGenString* _stringLiteral3596427562;
extern Il2CppCodeGenString* _stringLiteral1840488781;
extern Il2CppCodeGenString* _stringLiteral2512794967;
extern Il2CppCodeGenString* _stringLiteral1033953938;
extern const uint32_t UnityWebRequest_GetErrorDescription_m2694082268_MetadataUsageId;
extern "C"  String_t* UnityWebRequest_GetErrorDescription_m2694082268 (Il2CppObject * __this /* static, unused */, int32_t ___errorCode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetErrorDescription_m2694082268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___errorCode0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_007d;
		}
		if (L_1 == 1)
		{
			goto IL_011f;
		}
		if (L_1 == 2)
		{
			goto IL_0083;
		}
		if (L_1 == 3)
		{
			goto IL_0089;
		}
		if (L_1 == 4)
		{
			goto IL_008f;
		}
		if (L_1 == 5)
		{
			goto IL_0095;
		}
		if (L_1 == 6)
		{
			goto IL_009b;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00a7;
		}
		if (L_1 == 9)
		{
			goto IL_00ad;
		}
		if (L_1 == 10)
		{
			goto IL_00b3;
		}
		if (L_1 == 11)
		{
			goto IL_00b9;
		}
		if (L_1 == 12)
		{
			goto IL_00bf;
		}
		if (L_1 == 13)
		{
			goto IL_00c5;
		}
		if (L_1 == 14)
		{
			goto IL_00cb;
		}
		if (L_1 == 15)
		{
			goto IL_00d1;
		}
		if (L_1 == 16)
		{
			goto IL_00d7;
		}
		if (L_1 == 17)
		{
			goto IL_00dd;
		}
		if (L_1 == 18)
		{
			goto IL_00e3;
		}
		if (L_1 == 19)
		{
			goto IL_00e9;
		}
		if (L_1 == 20)
		{
			goto IL_00ef;
		}
		if (L_1 == 21)
		{
			goto IL_00f5;
		}
		if (L_1 == 22)
		{
			goto IL_00fb;
		}
		if (L_1 == 23)
		{
			goto IL_0101;
		}
		if (L_1 == 24)
		{
			goto IL_0107;
		}
		if (L_1 == 25)
		{
			goto IL_010d;
		}
		if (L_1 == 26)
		{
			goto IL_0113;
		}
		if (L_1 == 27)
		{
			goto IL_0119;
		}
	}
	{
		goto IL_011f;
	}

IL_007d:
	{
		return _stringLiteral3507245801;
	}

IL_0083:
	{
		return _stringLiteral4105688187;
	}

IL_0089:
	{
		return _stringLiteral234342368;
	}

IL_008f:
	{
		return _stringLiteral3171180777;
	}

IL_0095:
	{
		return _stringLiteral3916089169;
	}

IL_009b:
	{
		return _stringLiteral3187963024;
	}

IL_00a1:
	{
		return _stringLiteral2893261125;
	}

IL_00a7:
	{
		return _stringLiteral822965253;
	}

IL_00ad:
	{
		return _stringLiteral4152508176;
	}

IL_00b3:
	{
		return _stringLiteral1733288993;
	}

IL_00b9:
	{
		return _stringLiteral3812645704;
	}

IL_00bf:
	{
		return _stringLiteral4072787224;
	}

IL_00c5:
	{
		return _stringLiteral1002489873;
	}

IL_00cb:
	{
		return _stringLiteral2375081285;
	}

IL_00d1:
	{
		return _stringLiteral2212451623;
	}

IL_00d7:
	{
		return _stringLiteral4162316597;
	}

IL_00dd:
	{
		return _stringLiteral2960162492;
	}

IL_00e3:
	{
		return _stringLiteral296893764;
	}

IL_00e9:
	{
		return _stringLiteral2307651436;
	}

IL_00ef:
	{
		return _stringLiteral2960302138;
	}

IL_00f5:
	{
		return _stringLiteral1775835880;
	}

IL_00fb:
	{
		return _stringLiteral3851188946;
	}

IL_0101:
	{
		return _stringLiteral1209898337;
	}

IL_0107:
	{
		return _stringLiteral2922949545;
	}

IL_010d:
	{
		return _stringLiteral3596427562;
	}

IL_0113:
	{
		return _stringLiteral1840488781;
	}

IL_0119:
	{
		return _stringLiteral2512794967;
	}

IL_011f:
	{
		return _stringLiteral1033953938;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t1890284502_marshal_pinvoke(const UnityWebRequest_t1890284502& unmarshaled, UnityWebRequest_t1890284502_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_6 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_6()).get_m_value_0());
	marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9 = unmarshaled.get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
	marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10 = unmarshaled.get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
}
extern "C" void UnityWebRequest_t1890284502_marshal_pinvoke_back(const UnityWebRequest_t1890284502_marshaled_pinvoke& marshaled, UnityWebRequest_t1890284502& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_6));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_6(unmarshaled_m_Ptr_temp_0);
	bool unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = false;
	unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9;
	unmarshaled.set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1);
	bool unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = false;
	unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10;
	unmarshaled.set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t1890284502_marshal_pinvoke_cleanup(UnityWebRequest_t1890284502_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t1890284502_marshal_com(const UnityWebRequest_t1890284502& unmarshaled, UnityWebRequest_t1890284502_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_6 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_6()).get_m_value_0());
	marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9 = unmarshaled.get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
	marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10 = unmarshaled.get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
}
extern "C" void UnityWebRequest_t1890284502_marshal_com_back(const UnityWebRequest_t1890284502_marshaled_com& marshaled, UnityWebRequest_t1890284502& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_6));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_6(unmarshaled_m_Ptr_temp_0);
	bool unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = false;
	unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9;
	unmarshaled.set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1);
	bool unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = false;
	unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10;
	unmarshaled.set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t1890284502_marshal_com_cleanup(UnityWebRequest_t1890284502_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UploadHandler::.ctor()
extern "C"  void UploadHandler__ctor_m4145774046 (UploadHandler_t4062689071 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::InternalCreateRaw(System.Byte[])
extern "C"  void UploadHandler_InternalCreateRaw_m3364909982 (UploadHandler_t4062689071 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	typedef void (*UploadHandler_InternalCreateRaw_m3364909982_ftn) (UploadHandler_t4062689071 *, ByteU5BU5D_t4260760469*);
	static UploadHandler_InternalCreateRaw_m3364909982_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandler_InternalCreateRaw_m3364909982_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandler::InternalCreateRaw(System.Byte[])");
	_il2cpp_icall_func(__this, ___data0);
}
// System.Void UnityEngine.Networking.UploadHandler::InternalDestroy()
extern "C"  void UploadHandler_InternalDestroy_m3840871545 (UploadHandler_t4062689071 * __this, const MethodInfo* method)
{
	typedef void (*UploadHandler_InternalDestroy_m3840871545_ftn) (UploadHandler_t4062689071 *);
	static UploadHandler_InternalDestroy_m3840871545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandler_InternalDestroy_m3840871545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandler::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UploadHandler::Finalize()
extern "C"  void UploadHandler_Finalize_m777547204 (UploadHandler_t4062689071 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		UploadHandler_InternalDestroy_m3840871545(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::Dispose()
extern "C"  void UploadHandler_Dispose_m2552457883 (UploadHandler_t4062689071 * __this, const MethodInfo* method)
{
	{
		UploadHandler_InternalDestroy_m3840871545(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::set_contentType(System.String)
extern "C"  void UploadHandler_set_contentType_m3087243632 (UploadHandler_t4062689071 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.Networking.UploadHandler::SetContentType(System.String) */, __this, L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::SetContentType(System.String)
extern "C"  void UploadHandler_SetContentType_m1034215851 (UploadHandler_t4062689071 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t4062689071_marshal_pinvoke(const UploadHandler_t4062689071& unmarshaled, UploadHandler_t4062689071_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandler_t4062689071_marshal_pinvoke_back(const UploadHandler_t4062689071_marshaled_pinvoke& marshaled, UploadHandler_t4062689071& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t4062689071_marshal_pinvoke_cleanup(UploadHandler_t4062689071_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t4062689071_marshal_com(const UploadHandler_t4062689071& unmarshaled, UploadHandler_t4062689071_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandler_t4062689071_marshal_com_back(const UploadHandler_t4062689071_marshaled_com& marshaled, UploadHandler_t4062689071& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t4062689071_marshal_com_cleanup(UploadHandler_t4062689071_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
extern "C"  void UploadHandlerRaw__ctor_m1657900425 (UploadHandlerRaw_t16481323 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	{
		UploadHandler__ctor_m4145774046(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_0 = ___data0;
		UploadHandler_InternalCreateRaw_m3364909982(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)
extern "C"  void UploadHandlerRaw_InternalSetContentType_m4172044752 (UploadHandlerRaw_t16481323 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	typedef void (*UploadHandlerRaw_InternalSetContentType_m4172044752_ftn) (UploadHandlerRaw_t16481323 *, String_t*);
	static UploadHandlerRaw_InternalSetContentType_m4172044752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandlerRaw_InternalSetContentType_m4172044752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)");
	_il2cpp_icall_func(__this, ___newContentType0);
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::SetContentType(System.String)
extern "C"  void UploadHandlerRaw_SetContentType_m2065574029 (UploadHandlerRaw_t16481323 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___newContentType0;
		UploadHandlerRaw_InternalSetContentType_m4172044752(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t16481323_marshal_pinvoke(const UploadHandlerRaw_t16481323& unmarshaled, UploadHandlerRaw_t16481323_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandlerRaw_t16481323_marshal_pinvoke_back(const UploadHandlerRaw_t16481323_marshaled_pinvoke& marshaled, UploadHandlerRaw_t16481323& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t16481323_marshal_pinvoke_cleanup(UploadHandlerRaw_t16481323_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t16481323_marshal_com(const UploadHandlerRaw_t16481323& unmarshaled, UploadHandlerRaw_t16481323_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandlerRaw_t16481323_marshal_com_back(const UploadHandlerRaw_t16481323_marshaled_com& marshaled, UploadHandlerRaw_t16481323& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t16481323_marshal_com_cleanup(UploadHandlerRaw_t16481323_marshaled_com& marshaled)
{
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_m_TimeStamp_0();
		return L_0;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m3597535726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m3597535726(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765  L_0 = __this->get_m_Sender_1();
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkMessageInfo_get_sender_m2720861079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2720861079(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral907697851;
extern const uint32_t NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId;
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m221874487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NetworkViewID_t3400394436  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3400394436  L_1 = NetworkViewID_get_unassigned_m2302461037(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m776200489(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral907697851, /*hidden argument*/NULL);
		NetworkView_t3656680617 * L_3 = NetworkMessageInfo_NullNetworkView_m516412409(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0026:
	{
		NetworkViewID_t3400394436  L_4 = __this->get_m_ViewID_2();
		NetworkView_t3656680617 * L_5 = NetworkView_Find_m2926682619(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_get_networkView_m221874487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m221874487(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409 (NetworkMessageInfo_t3807997963 * __this, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkMessageInfo_NullNetworkView_m516412409_ftn) (NetworkMessageInfo_t3807997963 *);
	static NetworkMessageInfo_NullNetworkView_m516412409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m516412409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t3656680617 * NetworkMessageInfo_NullNetworkView_m516412409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t3807997963 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t3807997963 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m516412409(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_pinvoke(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_back(const NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_pinvoke_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_pinvoke_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_pinvoke_cleanup(NetworkMessageInfo_t3807997963_marshaled_pinvoke& marshaled)
{
	NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_pinvoke_cleanup(marshaled.___m_ViewID_2);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com(const NetworkMessageInfo_t3807997963& unmarshaled, NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t3231273765_marshal_com(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_back(const NetworkMessageInfo_t3807997963_marshaled_com& marshaled, NetworkMessageInfo_t3807997963& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t3231273765  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t3231273765_marshal_com_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3400394436  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3400394436_marshal_com_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t3807997963_marshal_com_cleanup(NetworkMessageInfo_t3807997963_marshaled_com& marshaled)
{
	NetworkPlayer_t3231273765_marshal_com_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3400394436_marshal_com_cleanup(marshaled.___m_ViewID_2);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2814584925;
extern const uint32_t NetworkPlayer__ctor_m978047135_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m978047135 (NetworkPlayer_t3231273765 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m978047135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral2814584925, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m978047135_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	NetworkPlayer__ctor_m978047135(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2705922677 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2705922677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m2646691040 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m2646691040_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m2646691040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m2646691040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m4147820797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m4147820797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m152642746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m152642746_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m152642746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m152642746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m3142644809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m3142644809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m4290647008 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m4290647008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m1155344837 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m1155344837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m82698821 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m82698821_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m82698821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m82698821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m668021995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m668021995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m3396943446(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2832997817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2832997817(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t3231273765_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1380117345_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1380117345 (NetworkPlayer_t3231273765 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1380117345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkPlayer_t3231273765 *)((NetworkPlayer_t3231273765 *)UnBox (L_1, NetworkPlayer_t3231273765_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_0)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool NetworkPlayer_Equals_m1380117345_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_Equals_m1380117345(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m3142644809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2705922677(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m206939407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m206939407(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m4260923428 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m4290647008(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m2646691040(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m4260923428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_port_m4260923428(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m1155344837(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m668021995(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m82698821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1193250345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_guid_m1193250345(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m4115863651 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m1286526384(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m4115863651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_ToString_m4115863651(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m4147820797(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m420672594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m420672594(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863 (NetworkPlayer_t3231273765 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m152642746(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m2392166863_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t3231273765 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m2392166863(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t3231273765  NetworkPlayer_get_unassigned_m865556847 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m1190866952 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3921017795 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t3231273765  ___lhs0, NetworkPlayer_t3231273765  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_back(const NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_pinvoke_cleanup(NetworkPlayer_t3231273765_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com(const NetworkPlayer_t3231273765& unmarshaled, NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t3231273765_marshal_com_back(const NetworkPlayer_t3231273765_marshaled_com& marshaled, NetworkPlayer_t3231273765& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t3231273765_marshal_com_cleanup(NetworkPlayer_t3231273765_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)
extern "C"  void NetworkView_Internal_GetViewID_m3457400931 (NetworkView_t3656680617 * __this, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	typedef void (*NetworkView_Internal_GetViewID_m3457400931_ftn) (NetworkView_t3656680617 *, NetworkViewID_t3400394436 *);
	static NetworkView_Internal_GetViewID_m3457400931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_Internal_GetViewID_m3457400931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(__this, ___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkView::get_viewID()
extern "C"  NetworkViewID_t3400394436  NetworkView_get_viewID_m148368939 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkView_Internal_GetViewID_m3457400931(__this, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkView::get_isMine()
extern "C"  bool NetworkView_get_isMine_m4033327326 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3400394436  L_0 = NetworkView_get_viewID_m148368939(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = NetworkViewID_get_isMine_m2754407673((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkView::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkView_get_owner_m1924541257 (NetworkView_t3656680617 * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3400394436  L_0 = NetworkView_get_viewID_m148368939(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		NetworkPlayer_t3231273765  L_1 = NetworkViewID_get_owner_m576069262((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t3656680617 * NetworkView_Find_m2926682619 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___viewID0, const MethodInfo* method)
{
	{
		NetworkView_t3656680617 * L_0 = NetworkView_INTERNAL_CALL_Find_m752431216(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t3656680617 * NetworkView_INTERNAL_CALL_Find_m752431216 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t3656680617 * (*NetworkView_INTERNAL_CALL_Find_m752431216_ftn) (NetworkViewID_t3400394436 *);
	static NetworkView_INTERNAL_CALL_Find_m752431216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m752431216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3400394436  NetworkViewID_get_unassigned_m2302461037 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_INTERNAL_get_unassigned_m3953385100(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3400394436  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m3953385100 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m3953385100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m619939661 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m2052798080_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m874421061 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t3231273765 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, NetworkPlayer_t3231273765 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn) (NetworkViewID_t3400394436 *, NetworkPlayer_t3231273765 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m2405750590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m679444096 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn) (NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m1523080203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m2541220858 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436 * ___lhs0, NetworkViewID_t3400394436 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn) (NetworkViewID_t3400394436 *, NetworkViewID_t3400394436 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m1319175441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m2420635706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_GetHashCode_m2420635706(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3400394436_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m2612049122_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m2612049122 (NetworkViewID_t3400394436 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m2612049122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkViewID_t3400394436  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3400394436_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkViewID_t3400394436 *)((NetworkViewID_t3400394436 *)UnBox (L_1, NetworkViewID_t3400394436_il2cpp_TypeInfo_var))));
		NetworkViewID_t3400394436  L_2 = V_0;
		bool L_3 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool NetworkViewID_Equals_m2612049122_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_Equals_m2612049122(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m2754407673 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m619939661(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m2754407673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_isMine_m2754407673(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	NetworkPlayer_t3231273765  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_Internal_GetOwner_m874421061(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t3231273765  L_0 = V_0;
		return L_0;
	}
}
extern "C"  NetworkPlayer_t3231273765  NetworkViewID_get_owner_m576069262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_get_owner_m576069262(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3783538050 (NetworkViewID_t3400394436 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m679444096(NULL /*static, unused*/, (*(NetworkViewID_t3400394436 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3783538050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3400394436 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3400394436 *>(__this + 1);
	return NetworkViewID_ToString_m3783538050(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m776200489 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m4224856356 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3400394436  ___lhs0, NetworkViewID_t3400394436  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3400394436  L_0 = ___lhs0;
		NetworkViewID_t3400394436  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m2541220858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_back(const NetworkViewID_t3400394436_marshaled_pinvoke& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_pinvoke_cleanup(NetworkViewID_t3400394436_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com(const NetworkViewID_t3400394436& unmarshaled, NetworkViewID_t3400394436_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3400394436_marshal_com_back(const NetworkViewID_t3400394436_marshaled_com& marshaled, NetworkViewID_t3400394436& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3400394436_marshal_com_cleanup(NetworkViewID_t3400394436_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m570634126 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object__cctor_m27692511_MetadataUsageId;
extern "C"  void Object__cctor_m27692511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m27692511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Object_t3071478659_StaticFields*)Object_t3071478659_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t3071478659 * Object_Internal_CloneSingle_m3129073756 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_Internal_CloneSingle_m3129073756_ftn) (Object_t3071478659 *);
	static Object_Internal_CloneSingle_m3129073756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m3129073756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t3071478659 * Object_Internal_CloneSingleWithParent_m1998670682 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Transform_t1659122786 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_Internal_CloneSingleWithParent_m1998670682_ftn) (Object_t3071478659 *, Transform_t1659122786 *, bool);
	static Object_Internal_CloneSingleWithParent_m1998670682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m1998670682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m3047563925_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Internal_InstantiateSingle_m3047563925 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566  ___pos1, Quaternion_t1553702882  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m3047563925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3071478659 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Vector3_t4282066566 * ___pos1, Quaternion_t1553702882 * ___rot2, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn) (Object_t3071478659 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m1201424140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m2082722510_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Internal_InstantiateSingleWithParent_m2082722510 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Transform_t1659122786 * ___parent1, Vector3_t4282066566  ___pos2, Quaternion_t1553702882  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m2082722510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___data0;
		Transform_t1659122786 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3885041925(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t3071478659 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3885041925 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___data0, Transform_t1659122786 * ___parent1, Vector3_t4282066566 * ___pos2, Quaternion_t1553702882 * ___rot3, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3885041925_ftn) (Object_t3071478659 *, Transform_t1659122786 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3885041925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3885041925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1992516375 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1992516375_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1992516375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1992516375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m3486236431 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m3486236431_ftn) (Object_t3071478659 *);
	static Object_EnsureRunningOnMainThread_m3486236431_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m3486236431_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m2260435093 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m2260435093_ftn) (Object_t3071478659 *, float);
	static Object_Destroy_m2260435093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m2260435093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Destroy_m176400816_MetadataUsageId;
extern "C"  void Object_Destroy_m176400816 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m176400816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m1826427014 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m1826427014_ftn) (Object_t3071478659 *, bool);
	static Object_DestroyImmediate_m1826427014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m1826427014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyImmediate_m349958679_MetadataUsageId;
extern "C"  void Object_DestroyImmediate_m349958679 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m349958679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t3071478659 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1826427014(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfType_m975740280 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfType_m975740280_ftn) (Type_t *);
	static Object_FindObjectsOfType_m975740280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m975740280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3709440845 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m3709440845_ftn) (Object_t3071478659 *);
	static Object_get_name_m3709440845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m3709440845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m1123518500 (Object_t3071478659 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m1123518500_ftn) (Object_t3071478659 *, String_t*);
	static Object_set_name_m1123518500_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m1123518500_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m4064482788 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m4064482788_ftn) (Object_t3071478659 *);
	static Object_DontDestroyOnLoad_m4064482788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m4064482788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m1893459363 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m1893459363_ftn) (Object_t3071478659 *);
	static Object_get_hideFlags_m1893459363_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m1893459363_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m41317712 (Object_t3071478659 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m41317712_ftn) (Object_t3071478659 *, int32_t);
	static Object_set_hideFlags_m41317712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m41317712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m3324336244 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m3324336244_ftn) (Object_t3071478659 *, float);
	static Object_DestroyObject_m3324336244_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m3324336244_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyObject_m3900253135_MetadataUsageId;
extern "C"  void Object_DestroyObject_m3900253135 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m3900253135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t3071478659 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DestroyObject_m3324336244(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindSceneObjectsOfType_m2168852346 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindSceneObjectsOfType_m2168852346_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m2168852346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m2168852346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Object_FindObjectsOfTypeIncludingAssets_m1276784656 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m1276784656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m2155033093 (Object_t3071478659 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m2155033093_ftn) (Object_t3071478659 *);
	static Object_ToString_m2155033093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m2155033093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m3762691104 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m3762691104_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m3762691104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m3762691104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m200424466_MetadataUsageId;
extern "C"  int32_t Object_GetInstanceID_m200424466 (Object_t3071478659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m200424466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return 0;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t3071478659_StaticFields*)Object_t3071478659_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1992516375(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t3071478659_StaticFields*)Object_t3071478659_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_002c:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m2695254659(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t3071478659_StaticFields*)Object_t3071478659_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m2136603816(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (*((int32_t*)L_9));
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m1758859581 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m500842593(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m1697651929_MetadataUsageId;
extern "C"  bool Object_Equals_m1697651929 (Object_t3071478659 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m1697651929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, __this, ((Object_t3071478659 *)IsInstClass(L_0, Object_t3071478659_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_CompareBaseObjects_m2625210252_MetadataUsageId;
extern "C"  bool Object_CompareBaseObjects_m2625210252 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___lhs0, Object_t3071478659 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m2625210252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t3071478659 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t3071478659 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t3071478659 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t3071478659 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t3071478659 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m434626365(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t3071478659 * L_10 = ___lhs0;
		Object_t3071478659 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m434626365_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m434626365 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m434626365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m1583421857(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m10053967(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m1583421857 (Object_t3071478659 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248549935;
extern const uint32_t Object_Instantiate_m2255090103_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m2255090103 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2255090103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3248549935, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Vector3_t4282066566  L_2 = ___position1;
		Quaternion_t1553702882  L_3 = ___rotation2;
		Object_t3071478659 * L_4 = Object_Internal_InstantiateSingle_m3047563925(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248549935;
extern const uint32_t Object_Instantiate_m1991490798_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m1991490798 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Transform_t1659122786 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m1991490798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Object_t3071478659 * L_2 = ___original0;
		Vector3_t4282066566  L_3 = ___position1;
		Quaternion_t1553702882  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_5 = Object_Internal_InstantiateSingle_m3047563925(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0015:
	{
		Object_t3071478659 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_6, _stringLiteral3248549935, /*hidden argument*/NULL);
		Object_t3071478659 * L_7 = ___original0;
		Transform_t1659122786 * L_8 = ___parent3;
		Vector3_t4282066566  L_9 = ___position1;
		Quaternion_t1553702882  L_10 = ___rotation2;
		Object_t3071478659 * L_11 = Object_Internal_InstantiateSingleWithParent_m2082722510(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248549935;
extern const uint32_t Object_Instantiate_m3040600263_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m3040600263 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3040600263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_0, _stringLiteral3248549935, /*hidden argument*/NULL);
		Object_t3071478659 * L_1 = ___original0;
		Object_t3071478659 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_m871497726_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m871497726 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Transform_t1659122786 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m871497726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___original0;
		Transform_t1659122786 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_2 = Object_Instantiate_m1911199679(NULL /*static, unused*/, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3248549935;
extern const uint32_t Object_Instantiate_m1911199679_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_Instantiate_m1911199679 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___original0, Transform_t1659122786 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m1911199679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Object_t3071478659 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_t3071478659 * L_3 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		Object_t3071478659 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, L_4, _stringLiteral3248549935, /*hidden argument*/NULL);
		Object_t3071478659 * L_5 = ___original0;
		Transform_t1659122786 * L_6 = ___parent1;
		bool L_7 = ___worldPositionStays2;
		Object_t3071478659 * L_8 = Object_Internal_CloneSingleWithParent_m1998670682(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m264735768_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m264735768 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m264735768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_m3820159265_MetadataUsageId;
extern "C"  Object_t3071478659 * Object_FindObjectOfType_m3820159265 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m3820159265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1015136018* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1015136018* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1015136018* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1015136018* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		Object_t3071478659 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}

IL_0014:
	{
		return (Object_t3071478659 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Implicit_m2106766291_MetadataUsageId;
extern "C"  bool Object_op_Implicit_m2106766291 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2106766291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Equality_m3964590952_MetadataUsageId;
extern "C"  bool Object_op_Equality_m3964590952 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3964590952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Inequality_m1296218211_MetadataUsageId;
extern "C"  bool Object_op_Inequality_m1296218211 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___x0, Object_t3071478659 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m1296218211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t3071478659 * L_0 = ___x0;
		Object_t3071478659 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m2625210252(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_pinvoke_back(const Object_t3071478659_marshaled_pinvoke& marshaled, Object_t3071478659& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_pinvoke_cleanup(Object_t3071478659_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com(const Object_t3071478659& unmarshaled, Object_t3071478659_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t3071478659_marshal_com_back(const Object_t3071478659_marshaled_com& marshaled, Object_t3071478659& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t3071478659_marshal_com_cleanup(Object_t3071478659_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.ParticleSystem::get_loop()
extern "C"  bool ParticleSystem_get_loop_m2126537855 (ParticleSystem_t381473177 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_loop_m2126537855_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_get_loop_m2126537855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_loop_m2126537855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_loop()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m594025146 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Play_m594025146_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_Internal_Play_m594025146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Play_m594025146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Stop_m4175525512 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Stop_m4175525512_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_Internal_Stop_m4175525512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Stop_m4175525512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Clear_m3791641553 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Clear_m3791641553_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_Internal_Clear_m3791641553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Clear_m3791641553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_IsAlive_m3898640007 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_IsAlive_m3898640007_ftn) (ParticleSystem_t381473177 *);
	static ParticleSystem_Internal_IsAlive_m3898640007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_IsAlive_m3898640007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern Il2CppClass* ParticleSystem_t381473177_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t4269758102_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CPlayU3Em__1_m745236472_MethodInfo_var;
extern const uint32_t ParticleSystem_Play_m1637509303_MetadataUsageId;
extern "C"  void ParticleSystem_Play_m1637509303 (ParticleSystem_t381473177 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Play_m1637509303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t381473177 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t381473177 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t4269758102 * L_1 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CPlayU3Em__1_m745236472_MethodInfo_var);
		IteratorDelegate_t4269758102 * L_3 = (IteratorDelegate_t4269758102 *)il2cpp_codegen_object_new(IteratorDelegate_t4269758102_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m2534678614(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		IteratorDelegate_t4269758102 * L_4 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2232250723(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern Il2CppClass* ParticleSystem_t381473177_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t4269758102_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CStopU3Em__2_m2892570987_MethodInfo_var;
extern const uint32_t ParticleSystem_Stop_m1876199813_MetadataUsageId;
extern "C"  void ParticleSystem_Stop_m1876199813 (ParticleSystem_t381473177 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Stop_m1876199813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t381473177 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t381473177 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t4269758102 * L_1 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CStopU3Em__2_m2892570987_MethodInfo_var);
		IteratorDelegate_t4269758102 * L_3 = (IteratorDelegate_t4269758102 *)il2cpp_codegen_object_new(IteratorDelegate_t4269758102_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m2534678614(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		IteratorDelegate_t4269758102 * L_4 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2232250723(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern Il2CppClass* ParticleSystem_t381473177_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t4269758102_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CClearU3Em__4_m898235192_MethodInfo_var;
extern const uint32_t ParticleSystem_Clear_m2190317978_MetadataUsageId;
extern "C"  void ParticleSystem_Clear_m2190317978 (ParticleSystem_t381473177 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Clear_m2190317978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t381473177 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t381473177 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t4269758102 * L_1 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CClearU3Em__4_m898235192_MethodInfo_var);
		IteratorDelegate_t4269758102 * L_3 = (IteratorDelegate_t4269758102 *)il2cpp_codegen_object_new(IteratorDelegate_t4269758102_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m2534678614(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		IteratorDelegate_t4269758102 * L_4 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2232250723(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern Il2CppClass* ParticleSystem_t381473177_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t4269758102_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CIsAliveU3Em__5_m576010307_MethodInfo_var;
extern const uint32_t ParticleSystem_IsAlive_m2723817258_MetadataUsageId;
extern "C"  bool ParticleSystem_IsAlive_m2723817258 (ParticleSystem_t381473177 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IsAlive_m2723817258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t381473177 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t381473177 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t4269758102 * L_1 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CIsAliveU3Em__5_m576010307_MethodInfo_var);
		IteratorDelegate_t4269758102 * L_3 = (IteratorDelegate_t4269758102 *)il2cpp_codegen_object_new(IteratorDelegate_t4269758102_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m2534678614(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_6(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		IteratorDelegate_t4269758102 * L_4 = ((ParticleSystem_t381473177_StaticFields*)ParticleSystem_t381473177_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		NullCheck(G_B2_1);
		bool L_5 = ParticleSystem_IterateParticleSystems_m2232250723(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m2232250723 (ParticleSystem_t381473177 * __this, bool ___recurse0, IteratorDelegate_t4269758102 * ___func1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		IteratorDelegate_t4269758102 * L_0 = ___func1;
		NullCheck(L_0);
		bool L_1 = IteratorDelegate_Invoke_m3641019902(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___recurse0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		bool L_3 = V_0;
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		IteratorDelegate_t4269758102 * L_5 = ___func1;
		bool L_6 = ParticleSystem_IterateParticleSystemsRecursive_m389741621(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_3|(int32_t)L_6));
	}

IL_001d:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var;
extern const uint32_t ParticleSystem_IterateParticleSystemsRecursive_m389741621_MetadataUsageId;
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m389741621 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___transform0, IteratorDelegate_t4269758102 * ___func1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IterateParticleSystemsRecursive_m389741621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Transform_t1659122786 * V_3 = NULL;
	ParticleSystem_t381473177 * V_4 = NULL;
	{
		V_0 = (bool)0;
		Transform_t1659122786 * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m2107810675(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0052;
	}

IL_0010:
	{
		Transform_t1659122786 * L_2 = ___transform0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Transform_t1659122786 * L_4 = Transform_GetChild_m4040462992(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		Transform_t1659122786 * L_5 = V_3;
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ParticleSystem_t381473177 * L_7 = GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(L_6, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var);
		V_4 = L_7;
		ParticleSystem_t381473177 * L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		IteratorDelegate_t4269758102 * L_10 = ___func1;
		ParticleSystem_t381473177 * L_11 = V_4;
		NullCheck(L_10);
		bool L_12 = IteratorDelegate_Invoke_m3641019902(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0059;
	}

IL_0046:
	{
		Transform_t1659122786 * L_14 = V_3;
		IteratorDelegate_t4269758102 * L_15 = ___func1;
		ParticleSystem_IterateParticleSystemsRecursive_m389741621(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_004e:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0010;
		}
	}

IL_0059:
	{
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Play>m__1(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPlayU3Em__1_m745236472 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Play_m594025146(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Stop>m__2(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CStopU3Em__2_m2892570987 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Stop_m4175525512(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Clear>m__4(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CClearU3Em__4_m898235192 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Clear_m3791641553(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<IsAlive>m__5(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CIsAliveU3Em__5_m576010307 (Il2CppObject * __this /* static, unused */, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	{
		ParticleSystem_t381473177 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_IsAlive_m3898640007(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m2534678614 (IteratorDelegate_t4269758102 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m3641019902 (IteratorDelegate_t4269758102 * __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		IteratorDelegate_Invoke_m3641019902((IteratorDelegate_t4269758102 *)__this->get_prev_9(),___ps0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, ParticleSystem_t381473177 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.ParticleSystem/IteratorDelegate::BeginInvoke(UnityEngine.ParticleSystem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IteratorDelegate_BeginInvoke_m3274238639 (IteratorDelegate_t4269758102 * __this, ParticleSystem_t381473177 * ___ps0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ps0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool IteratorDelegate_EndInvoke_m3581459136 (IteratorDelegate_t4269758102 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t4282066566  Physics_get_gravity_m2907531023 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Physics_INTERNAL_get_gravity_m3320492712(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Physics::set_gravity(UnityEngine.Vector3)
extern "C"  void Physics_set_gravity_m2814881048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Physics_INTERNAL_set_gravity_m2886173364(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_get_gravity_m3320492712 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m3320492712_ftn) (Vector3_t4282066566 *);
	static Physics_INTERNAL_get_gravity_m3320492712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m3320492712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2886173364 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_set_gravity_m2886173364_ftn) (Vector3_t4282066566 *);
	static Physics_INTERNAL_set_gravity_m2886173364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_set_gravity_m2886173364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m267364350 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Vector3_t4282066566  L_0 = ___origin0;
		Vector3_t4282066566  L_1 = ___direction1;
		RaycastHit_t4003175726 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m1758069759(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1758069759 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___origin0;
		Vector3_t4282066566  L_1 = ___direction1;
		RaycastHit_t4003175726 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m3365413907(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1600345803 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t3134616544  L_0 = ___ray0;
		RaycastHit_t4003175726 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m165875788(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m1235528076 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3134616544  L_0 = ___ray0;
		RaycastHit_t4003175726 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m165875788(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m165875788 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, RaycastHit_t4003175726 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m1758069759(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1771931441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t3134616544  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t528650843* L_4 = Physics_RaycastAll_m1269007794(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m1269007794 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t528650843* L_5 = Physics_RaycastAll_m892728677(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m892728677 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t528650843* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2642095530(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_RaycastAll_m2195936356 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t528650843* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2642095530(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t528650843* Physics_INTERNAL_CALL_RaycastAll_m2642095530 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t528650843* (*Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2642095530_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern "C"  ColliderU5BU5D_t2697150633* Physics_OverlapSphere_m359079608 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, float ___radius1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = (-1);
		float L_0 = ___radius1;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		ColliderU5BU5D_t2697150633* L_3 = Physics_INTERNAL_CALL_OverlapSphere_m4255329177(NULL /*static, unused*/, (&___position0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t2697150633* Physics_INTERNAL_CALL_OverlapSphere_m4255329177 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	typedef ColliderU5BU5D_t2697150633* (*Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn) (Vector3_t4282066566 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_OverlapSphere_m4255329177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___position0, ___radius1, ___layerMask2, ___queryTriggerInteraction3);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m3365413907 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		RaycastHit_t4003175726 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m1291554392(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m1291554392 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___origin0, Vector3_t4282066566 * ___direction1, RaycastHit_t4003175726 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, RaycastHit_t4003175726 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m1291554392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern Il2CppClass* List_1_t3111957221_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1832106448_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m2087591309_MetadataUsageId;
extern "C"  void Physics2D__cctor_m2087591309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m2087591309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3111957221 * L_0 = (List_1_t3111957221 *)il2cpp_codegen_object_new(List_1_t3111957221_il2cpp_TypeInfo_var);
		List_1__ctor_m1832106448(L_0, /*hidden argument*/List_1__ctor_m1832106448_MethodInfo_var);
		((Physics2D_t9846735_StaticFields*)Physics2D_t9846735_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Physics2D::set_gravity(UnityEngine.Vector2)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_set_gravity_m2019264139_MetadataUsageId;
extern "C"  void Physics2D_set_gravity_m2019264139 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_set_gravity_m2019264139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_set_gravity_m3872262661(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
extern "C"  void Physics2D_INTERNAL_set_gravity_m3872262661 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_set_gravity_m3872262661_ftn) (Vector2_t4282066565 *);
	static Physics2D_INTERNAL_set_gravity_m3872262661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_set_gravity_m3872262661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Physics2D::Internal_Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Linecast_m3653471167_MetadataUsageId;
extern "C"  void Physics2D_Internal_Linecast_m3653471167 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Linecast_m3653471167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		RaycastHit2D_t1374744384 * L_3 = ___raycastHit5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t1374744384 * ___raycastHit5, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, int32_t, float, float, RaycastHit2D_t1374744384 *);
	static Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Linecast_m2627767048_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___start0, ___end1, ___layerMask2, ___minDepth3, ___maxDepth4, ___raycastHit5);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Linecast_m4170255972_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m4170255972 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m4170255972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t4282066565  L_0 = ___start0;
		Vector2_t4282066565  L_1 = ___end1;
		int32_t L_2 = ___layerMask2;
		float L_3 = V_1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_5 = Physics2D_Linecast_m1824977262(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Linecast_m1824977262_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Linecast_m1824977262 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m1824977262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___start0;
		Vector2_t4282066565  L_1 = ___end1;
		int32_t L_2 = ___layerMask2;
		float L_3 = ___minDepth3;
		float L_4 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_Internal_Linecast_m3653471167(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t1374744384  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_LinecastAll_m2367926937_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m2367926937 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m2367926937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_3 = Physics2D_INTERNAL_CALL_LinecastAll_m3511439778(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_LinecastAll_m740601359_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_LinecastAll_m740601359 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___start0, Vector2_t4282066565  ___end1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m740601359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask2;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_3 = Physics2D_INTERNAL_CALL_LinecastAll_m3511439778(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_LinecastAll_m3511439778 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___start0, Vector2_t4282066565 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t889400257* (*Physics2D_INTERNAL_CALL_LinecastAll_m3511439778_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_LinecastAll_m3511439778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_LinecastAll_m3511439778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___start0, ___end1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Raycast_m4294843026_MetadataUsageId;
extern "C"  void Physics2D_Internal_Raycast_m4294843026 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m4294843026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		RaycastHit2D_t1374744384 * L_4 = ___raycastHit6;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t1374744384 * ___raycastHit6, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, float, int32_t, float, float, RaycastHit2D_t1374744384 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m1210233913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5, ___raycastHit6);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m1435321255_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m1435321255 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1435321255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t4282066565  L_0 = ___origin0;
		Vector2_t4282066565  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_6 = Physics2D_Raycast_m301626417(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m301626417_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_Raycast_m301626417 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m301626417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___origin0;
		Vector2_t4282066565  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m4294843026(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t1374744384  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_RaycastAll_m1583954576_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m1583954576 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m1583954576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m765742583(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_RaycastAll_m3437166214_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_RaycastAll_m3437166214 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___origin0, Vector2_t4282066565  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m3437166214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m765742583(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_RaycastAll_m765742583 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___origin0, Vector2_t4282066565 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t889400257* (*Physics2D_INTERNAL_CALL_RaycastAll_m765742583_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m765742583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m765742583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5);
}
// System.Void UnityEngine.Physics2D::Internal_GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_GetRayIntersection_m2828566441_MetadataUsageId;
extern "C"  void Physics2D_Internal_GetRayIntersection_m2828566441 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_GetRayIntersection_m2828566441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		RaycastHit2D_t1374744384 * L_2 = ___raycastHit3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840(NULL /*static, unused*/, (&___ray0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t1374744384 * ___raycastHit3, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840_ftn) (Ray_t3134616544 *, float, int32_t, RaycastHit2D_t1374744384 *);
	static Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m1206199840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___ray0, ___distance1, ___layerMask2, ___raycastHit3);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersection_m2375445855_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m2375445855 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m2375445855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)-5);
		Ray_t3134616544  L_0 = ___ray0;
		float L_1 = ___distance1;
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2D_t1374744384  L_3 = Physics2D_GetRayIntersection_m3849560536(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersection_m3849560536_MetadataUsageId;
extern "C"  RaycastHit2D_t1374744384  Physics2D_GetRayIntersection_m3849560536 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m3849560536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t1374744384  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Ray_t3134616544  L_0 = ___ray0;
		float L_1 = ___distance1;
		int32_t L_2 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Physics2D_Internal_GetRayIntersection_m2828566441(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t1374744384  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2520210479_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_GetRayIntersectionAll_m2520210479 (Il2CppObject * __this /* static, unused */, Ray_t3134616544  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2520210479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t889400257* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t889400257* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304 (Il2CppObject * __this /* static, unused */, Ray_t3134616544 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t889400257* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn) (Ray_t3134616544 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m2968135304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapPointAll_m4265382893_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m4265382893 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m4265382893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask1;
		float L_1 = ___minDepth2;
		float L_2 = ___maxDepth3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_3 = Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapPointAll_m3910604131_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapPointAll_m3910604131 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, int32_t ___layerMask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m3910604131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask1;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_3 = Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t1758559887* (*Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752_ftn) (Vector2_t4282066565 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapPointAll_m1965895752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___point0, ___layerMask1, ___minDepth2, ___maxDepth3);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapCircleAll_m2585342016_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m2585342016 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m2585342016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___radius1;
		int32_t L_1 = ___layerMask2;
		float L_2 = ___minDepth3;
		float L_3 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_4 = Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapCircleAll_m1000299574_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapCircleAll_m1000299574 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m1000299574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___radius1;
		int32_t L_1 = ___layerMask2;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_4 = Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t1758559887* (*Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157_ftn) (Vector2_t4282066565 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapCircleAll_m3702630157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___point0, ___radius1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapAreaAll_m2584047348_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2584047348 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m2584047348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_3 = Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779(NULL /*static, unused*/, (&___pointA0), (&___pointB1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t9846735_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapAreaAll_m2026172650_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_OverlapAreaAll_m2026172650 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___pointA0, Vector2_t4282066565  ___pointB1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m2026172650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask2;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t9846735_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1758559887* L_3 = Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779(NULL /*static, unused*/, (&___pointA0), (&___pointB1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t1758559887* Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___pointA0, Vector2_t4282066565 * ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t1758559887* (*Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779_ftn) (Vector2_t4282066565 *, Vector2_t4282066565 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapAreaAll_m2088674779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___pointA0, ___pointB1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2201046863 (Plane_t4206452690 * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___inNormal0;
		Vector3_t4282066566  L_1 = Vector3_Normalize_m3047997355(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t4282066566  L_2 = ___inNormal0;
		Vector3_t4282066566  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2201046863_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___inNormal0, Vector3_t4282066566  ___inPoint1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	Plane__ctor_m2201046863(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Plane_get_normal_m3534129213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_normal_m3534129213(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m2612484153 (Plane_t4206452690 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_1();
		return L_0;
	}
}
extern "C"  float Plane_get_distance_m2612484153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_get_distance_m2612484153(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2829769106_MetadataUsageId;
extern "C"  bool Plane_Raycast_m2829769106 (Plane_t4206452690 * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2829769106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t4282066566  L_0 = Ray_get_direction_m3201866877((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t4282066566  L_3 = Ray_get_origin_m3064983562((&___ray0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Plane_get_normal_m3534129213(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m2370485424(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m2612484153(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		return (bool)0;
	}

IL_0047:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		return (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
extern "C"  bool Plane_Raycast_m2829769106_AdjustorThunk (Il2CppObject * __this, Ray_t3134616544  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t4206452690 * _thisAdjusted = reinterpret_cast<Plane_t4206452690 *>(__this + 1);
	return Plane_Raycast_m2829769106(_thisAdjusted, ___ray0, ___enter1, method);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_pinvoke_back(const Plane_t4206452690_marshaled_pinvoke& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_pinvoke_cleanup(Plane_t4206452690_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Normal_0);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com(const Plane_t4206452690& unmarshaled, Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t4206452690_marshal_com_back(const Plane_t4206452690_marshaled_com& marshaled, Plane_t4206452690& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t4206452690_marshal_com_cleanup(Plane_t4206452690_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Normal_0);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C"  bool PlayerPrefs_TrySetInt_m2066630347 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m2066630347_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m2066630347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m2066630347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
extern "C"  bool PlayerPrefs_TrySetFloat_m96551332 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetFloat_m96551332_ftn) (String_t*, float);
	static PlayerPrefs_TrySetFloat_m96551332_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetFloat_m96551332_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C"  bool PlayerPrefs_TrySetSetString_m452988068 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m452988068_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m452988068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m452988068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetInt_m3485171996_MetadataUsageId;
extern "C"  void PlayerPrefs_SetInt_m3485171996 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetInt_m3485171996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetInt_m2066630347(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m3632746280 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m3632746280_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m3632746280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m3632746280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m1334009359 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key0;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m3632746280(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetFloat_m1687591347_MetadataUsageId;
extern "C"  void PlayerPrefs_SetFloat_m1687591347 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetFloat_m1687591347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		float L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetFloat_m96551332(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
extern "C"  float PlayerPrefs_GetFloat_m1210224051 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, const MethodInfo* method)
{
	typedef float (*PlayerPrefs_GetFloat_m1210224051_ftn) (String_t*, float);
	static PlayerPrefs_GetFloat_m1210224051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetFloat_m1210224051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String)
extern "C"  float PlayerPrefs_GetFloat_m4179026766 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		String_t* L_0 = ___key0;
		float L_1 = V_0;
		float L_2 = PlayerPrefs_GetFloat_m1210224051(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern Il2CppClass* PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2586178071;
extern const uint32_t PlayerPrefs_SetString_m989974275_MetadataUsageId;
extern "C"  void PlayerPrefs_SetString_m989974275 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetString_m989974275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetSetString_m452988068(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3680716996 * L_3 = (PlayerPrefsException_t3680716996 *)il2cpp_codegen_object_new(PlayerPrefsException_t3680716996_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m3661687413(L_3, _stringLiteral2586178071, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C"  String_t* PlayerPrefs_GetString_m3230559948 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m3230559948_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m3230559948_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m3230559948_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefs_GetString_m378864272_MetadataUsageId;
extern "C"  String_t* PlayerPrefs_GetString_m378864272 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_GetString_m378864272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ___key0;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m2032560073 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m2032560073_ftn) (String_t*);
	static PlayerPrefs_HasKey_m2032560073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m2032560073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C"  void PlayerPrefs_DeleteKey_m1547199302 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m1547199302_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m1547199302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m1547199302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
extern "C"  void PlayerPrefs_DeleteAll_m2619453438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteAll_m2619453438_ftn) ();
	static PlayerPrefs_DeleteAll_m2619453438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteAll_m2619453438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C"  void PlayerPrefsException__ctor_m3661687413 (PlayerPrefsException_t3680716996 * __this, String_t* ___error0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m1741701746 (PropertyAttribute_t3531521085 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m1100844011 (Quaternion_t1553702882 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m1100844011_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	Quaternion__ctor_m1100844011(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t1553702882  Quaternion_get_identity_m1743882806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m1100844011(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m580284 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_AngleAxis_m644124247 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566  ___axis1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m1562314763(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m1562314763 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t4282066566 * ___axis1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn) (float, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m1562314763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_FromToRotation_m2335489018 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___fromDirection0, Vector3_t4282066566  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m3717286698(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m3717286698 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___fromDirection0, Vector3_t4282066566 * ___toDirection1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m3717286698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m2869326048 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, Vector3_t4282066566  ___upwards1, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_LookRotation_m1257501645 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___forward0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1553702882  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t4282066566  L_0 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m1501255504(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1501255504 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___forward0, Vector3_t4282066566 * ___upwards1, Quaternion_t1553702882 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn) (Vector3_t4282066566 *, Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1501255504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Slerp_m844700366 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m2927410052(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m2927410052 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m2927410052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_SlerpUnclamped_m1752874085 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_SlerpUnclamped_m2836994545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Lerp_m1693481477 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m3955033425(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m3955033425 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___a0, Quaternion_t1553702882 * ___b1, float ___t2, Quaternion_t1553702882 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *, float, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m3955033425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_RotateTowards_m180339351_MetadataUsageId;
extern "C"  Quaternion_t1553702882  Quaternion_RotateTowards_m180339351 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___from0, Quaternion_t1553702882  ___to1, float ___maxDegreesDelta2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_RotateTowards_m180339351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t1553702882  L_0 = ___from0;
		Quaternion_t1553702882  L_1 = ___to1;
		float L_2 = Quaternion_Angle_m835424754(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		Quaternion_t1553702882  L_4 = ___to1;
		return L_4;
	}

IL_0015:
	{
		float L_5 = ___maxDegreesDelta2;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m2322067385(NULL /*static, unused*/, (1.0f), ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		Quaternion_t1553702882  L_8 = ___from0;
		Quaternion_t1553702882  L_9 = ___to1;
		float L_10 = V_1;
		Quaternion_t1553702882  L_11 = Quaternion_SlerpUnclamped_m1752874085(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_Inverse_m3542515566 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Inverse_m4175627710(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m4175627710 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn) (Quaternion_t1553702882 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m4175627710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral843281963;
extern const uint32_t Quaternion_ToString_m1793285860_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m1793285860 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m1793285860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral843281963, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m1793285860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_ToString_m1793285860(_thisAdjusted, method);
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Angle_m835424754_MetadataUsageId;
extern "C"  float Quaternion_Angle_m835424754 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___a0, Quaternion_t1553702882  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m835424754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t1553702882  L_0 = ___a0;
		Quaternion_t1553702882  L_1 = ___b1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		return ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = Quaternion_Internal_ToEulerRad_m1608666215(NULL /*static, unused*/, (*(Quaternion_t1553702882 *)__this), /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Quaternion_Internal_MakePositive_m4059160381(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t4282066566  Quaternion_get_eulerAngles_m997303795_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_get_eulerAngles_m997303795(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1204688217 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t4282066566  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2926210380(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Euler_m1940911101 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___euler0;
		Vector3_t4282066566  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_2 = Quaternion_Internal_FromEulerRad_m3681319598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Quaternion_Internal_MakePositive_m4059160381 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0033;
		}
	}
	{
		Vector3_t4282066566 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0053;
	}

IL_0033:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0053;
		}
	}
	{
		Vector3_t4282066566 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0053:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0078;
		}
	}
	{
		Vector3_t4282066566 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0098;
	}

IL_0078:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0098;
		}
	}
	{
		Vector3_t4282066566 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0098:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00bd;
		}
	}
	{
		Vector3_t4282066566 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00dd;
	}

IL_00bd:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00dd;
		}
	}
	{
		Vector3_t4282066566 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00dd:
	{
		Vector3_t4282066566  L_25 = ___euler0;
		return L_25;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t4282066566  Quaternion_Internal_ToEulerRad_m1608666215 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882 * ___rotation0, Vector3_t4282066566 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn) (Quaternion_t1553702882 *, Vector3_t4282066566 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1660134351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t1553702882  Quaternion_Internal_FromEulerRad_m3681319598 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___euler0, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t1553702882  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566 * ___euler0, Quaternion_t1553702882 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn) (Vector3_t4282066566 *, Quaternion_t1553702882 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1312441940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3823258238 (Quaternion_t1553702882 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m65342520(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m65342520(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m65342520(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m65342520(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m3823258238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_GetHashCode_m3823258238(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t1553702882_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3843409946_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3843409946 (Quaternion_t1553702882 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3843409946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t1553702882_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Quaternion_t1553702882 *)((Quaternion_t1553702882 *)UnBox (L_1, Quaternion_t1553702882_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m2110115959(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m2110115959(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m2110115959(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m2110115959(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Quaternion_Equals_m3843409946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t1553702882 * _thisAdjusted = reinterpret_cast<Quaternion_t1553702882 *>(__this + 1);
	return Quaternion_Equals_m3843409946(_thisAdjusted, ___other0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t1553702882  Quaternion_op_Multiply_m3077481361 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_w_4();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_x_1();
		float L_3 = (&___rhs1)->get_w_4();
		float L_4 = (&___lhs0)->get_y_2();
		float L_5 = (&___rhs1)->get_z_3();
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_y_2();
		float L_8 = (&___lhs0)->get_w_4();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_w_4();
		float L_12 = (&___lhs0)->get_z_3();
		float L_13 = (&___rhs1)->get_x_1();
		float L_14 = (&___lhs0)->get_x_1();
		float L_15 = (&___rhs1)->get_z_3();
		float L_16 = (&___lhs0)->get_w_4();
		float L_17 = (&___rhs1)->get_z_3();
		float L_18 = (&___lhs0)->get_z_3();
		float L_19 = (&___rhs1)->get_w_4();
		float L_20 = (&___lhs0)->get_x_1();
		float L_21 = (&___rhs1)->get_y_2();
		float L_22 = (&___lhs0)->get_y_2();
		float L_23 = (&___rhs1)->get_x_1();
		float L_24 = (&___lhs0)->get_w_4();
		float L_25 = (&___rhs1)->get_w_4();
		float L_26 = (&___lhs0)->get_x_1();
		float L_27 = (&___rhs1)->get_x_1();
		float L_28 = (&___lhs0)->get_y_2();
		float L_29 = (&___rhs1)->get_y_2();
		float L_30 = (&___lhs0)->get_z_3();
		float L_31 = (&___rhs1)->get_z_3();
		Quaternion_t1553702882  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m1100844011(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Quaternion_op_Multiply_m3771288979 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___rotation0, Vector3_t4282066566  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t4282066566  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = (&___rotation0)->get_x_1();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_2();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_3();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_1();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_2();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_3();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_1();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_1();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_2();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_4();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_4();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_4();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t4282066566  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m4197259746 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___lhs0, Quaternion_t1553702882  ___rhs1, const MethodInfo* method)
{
	{
		Quaternion_t1553702882  L_0 = ___lhs0;
		Quaternion_t1553702882  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m580284(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_pinvoke_back(const Quaternion_t1553702882_marshaled_pinvoke& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_pinvoke_cleanup(Quaternion_t1553702882_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com(const Quaternion_t1553702882& unmarshaled, Quaternion_t1553702882_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t1553702882_marshal_com_back(const Quaternion_t1553702882_marshaled_com& marshaled, Quaternion_t1553702882& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t1553702882_marshal_com_cleanup(Quaternion_t1553702882_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m3362417303 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m3362417303_ftn) (float, float);
	static Random_Range_m3362417303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m3362417303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m75452833 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m1203631415(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m1203631415 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m1203631415_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m1203631415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m1203631415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1279576482 (RangeAttribute_t912008995 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m1741701746(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2662468509 (Ray_t3134616544 * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t4282066566  L_1 = Vector3_get_normalized_m2650940353((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m2662468509_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___origin0, Vector3_t4282066566  ___direction1, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	Ray__ctor_m2662468509(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_origin_m3064983562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_origin_m3064983562(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  Ray_get_direction_m3201866877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_get_direction_m3201866877(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822 (Ray_t3134616544 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector3_t4282066566  Ray_GetPoint_m1171104822_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_GetPoint_m1171104822(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1741034756;
extern const uint32_t Ray_ToString_m1391619614_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m1391619614 (Ray_t3134616544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m1391619614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t4282066566  L_1 = __this->get_m_Origin_0();
		Vector3_t4282066566  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		Vector3_t4282066566  L_5 = __this->get_m_Direction_1();
		Vector3_t4282066566  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral1741034756, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m1391619614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3134616544 * _thisAdjusted = reinterpret_cast<Ray_t3134616544 *>(__this + 1);
	return Ray_ToString_m1391619614(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_pinvoke_back(const Ray_t3134616544_marshaled_pinvoke& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_pinvoke_cleanup(Ray_t3134616544_marshaled_pinvoke& marshaled)
{
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com(const Ray_t3134616544& unmarshaled, Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t3134616544_marshal_com_back(const Ray_t3134616544_marshaled_com& marshaled, Ray_t3134616544& unmarshaled)
{
	Vector3_t4282066566  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t4282066566  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t3134616544_marshal_com_cleanup(Ray_t3134616544_marshaled_com& marshaled)
{
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_point_m4165497838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_point_m4165497838(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Vector3_t4282066566  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t4282066566  RaycastHit_get_normal_m1346998891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_normal_m1346998891(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m800944203 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit_get_distance_m800944203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_distance_m800944203(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider_t2939674232 * RaycastHit_get_collider_m3116882274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_collider_m3116882274(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_rigidbody_m4137883432_MetadataUsageId;
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m4137883432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t3346577219 * G_B3_0 = NULL;
	{
		Collider_t2939674232 * L_0 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t2939674232 * L_2 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t3346577219 * L_3 = Collider_get_attachedRigidbody_m2821754842(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t3346577219 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody_t3346577219 * RaycastHit_get_rigidbody_m4137883432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_rigidbody_m4137883432(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_transform_m905149094_MetadataUsageId;
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094 (RaycastHit_t4003175726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_transform_m905149094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t3346577219 * V_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = RaycastHit_get_rigidbody_m4137883432(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t3346577219 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t3346577219 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t2939674232 * L_5 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t2939674232 * L_7 = RaycastHit_get_collider_m3116882274(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit_get_transform_m905149094_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t4003175726 * _thisAdjusted = reinterpret_cast<RaycastHit_t4003175726 *>(__this + 1);
	return RaycastHit_get_transform_m905149094(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_back(const RaycastHit_t4003175726_marshaled_pinvoke& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_pinvoke_cleanup(RaycastHit_t4003175726_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com(const RaycastHit_t4003175726& unmarshaled, RaycastHit_t4003175726_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t4003175726_marshal_com_back(const RaycastHit_t4003175726_marshaled_com& marshaled, RaycastHit_t4003175726& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t4003175726_marshal_com_cleanup(RaycastHit_t4003175726_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Point_1();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_point_m2072691227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_point_m2072691227(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Vector2_t4282066565  L_0 = __this->get_m_Normal_2();
		return L_0;
	}
}
extern "C"  Vector2_t4282066565  RaycastHit2D_get_normal_m894503390_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_normal_m894503390(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C"  float RaycastHit2D_get_distance_m467570589 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_distance_m467570589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_distance_m467570589(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m2313516650 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Fraction_4();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m2313516650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_fraction_m2313516650(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	{
		Collider2D_t1552025098 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider2D_t1552025098 * RaycastHit2D_get_collider_m789902306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_collider_m789902306(_thisAdjusted, method);
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit2D_get_rigidbody_m1059160360_MetadataUsageId;
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_rigidbody_m1059160360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody2D_t1743771669 * G_B3_0 = NULL;
	{
		Collider2D_t1552025098 * L_0 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t1552025098 * L_2 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t1743771669 * L_3 = Collider2D_get_attachedRigidbody_m2908627162(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t1743771669 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody2D_t1743771669 * RaycastHit2D_get_rigidbody_m1059160360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_rigidbody_m1059160360(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit2D_get_transform_m1318597140_MetadataUsageId;
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140 (RaycastHit2D_t1374744384 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_transform_m1318597140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody2D_t1743771669 * V_0 = NULL;
	{
		Rigidbody2D_t1743771669 * L_0 = RaycastHit2D_get_rigidbody_m1059160360(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t1743771669 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t1743771669 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1552025098 * L_5 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t1552025098 * L_7 = RaycastHit2D_get_collider_m789902306(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t1659122786 *)NULL;
	}
}
extern "C"  Transform_t1659122786 * RaycastHit2D_get_transform_m1318597140_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t1374744384 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t1374744384 *>(__this + 1);
	return RaycastHit2D_get_transform_m1318597140(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_back(const RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_pinvoke_cleanup(RaycastHit2D_t1374744384_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com(const RaycastHit2D_t1374744384& unmarshaled, RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t1374744384_marshal_com_back(const RaycastHit2D_t1374744384_marshaled_com& marshaled, RaycastHit2D_t1374744384& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t1374744384_marshal_com_cleanup(RaycastHit2D_t1374744384_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m3291325233 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3291325233_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3291325233(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3858381006 (Rect_t4241904616 * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3858381006_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___source0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect__ctor_m3858381006(_thisAdjusted, ___source0, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t4241904616  Rect_MinMaxRect_m1534690677 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect_Set_m3128045745 (Rect_t4241904616 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect_Set_m3128045745_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_Set_m3128045745(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m982385354 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_x_m982385354_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_x_m982385354(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m577970569 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m577970569_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_x_m577970569(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m982386315 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_y_m982386315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_y_m982386315(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m67436392 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m67436392_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_y_m67436392(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_position_m2933356232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_position_m2933356232(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t4282066565  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m1517109030(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_center_m610643572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_center_m610643572(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_min_m275942709_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_min_m275942709(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_max_m275713991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_max_m275713991(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m2824209432 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		return L_0;
	}
}
extern "C"  float Rect_get_width_m2824209432_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_width_m2824209432(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m3771513595 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m3771513595_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_width_m3771513595(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m2154960823 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		return L_0;
	}
}
extern "C"  float Rect_get_height_m2154960823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_height_m2154960823(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m3398820332 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m3398820332_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_height_m3398820332(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t4282066565  Rect_get_size_m136480416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_size_m136480416(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m371109962 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_xMin_m371109962_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMin_m371109962(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m265803321 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m265803321_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMin_m265803321(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m399739113 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_yMin_m399739113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMin_m399739113(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m3716298746 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m3716298746_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMin_m3716298746(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m370881244 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_xMax_m370881244_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_xMax_m370881244(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m1513853159 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m1513853159_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_xMax_m1513853159(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m399510395 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_yMax_m399510395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_get_yMax_m399510395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m669381288 (Rect_t4241904616 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m669381288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	Rect_set_yMax_m669381288(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2721079081;
extern const uint32_t Rect_ToString_m2093687658_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2093687658 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2093687658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		float L_9 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		float L_13 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2721079081, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2093687658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_ToString_m2093687658(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3556594010 (Rect_t4241904616 * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594010_AdjustorThunk (Il2CppObject * __this, Vector2_t4282066565  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594010(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m3556594041 (Rect_t4241904616 * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m3556594041_AdjustorThunk (Il2CppObject * __this, Vector3_t4282066566  ___point0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Contains_m3556594041(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t4241904616  Rect_OrderMinMax_m3424313368 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0031;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m370881244((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m265803321((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m1513853159((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		float L_5 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0062;
		}
	}
	{
		float L_7 = Rect_get_yMin_m399739113((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m399510395((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m3716298746((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m669381288((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Rect_t4241904616  L_10 = ___rect0;
		return L_10;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m669681106 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m370881244((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m371109962(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = Rect_get_xMin_m371109962((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m370881244(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = Rect_get_yMax_m399510395((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m399739113(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = Rect_get_yMin_m399739113((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m399510395(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Overlaps_m669681106_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m669681106(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m2751672171 (Rect_t4241904616 * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		V_0 = (*(Rect_t4241904616 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Rect_t4241904616  L_1 = V_0;
		Rect_t4241904616  L_2 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t4241904616  L_3 = ___other0;
		Rect_t4241904616  L_4 = Rect_OrderMinMax_m3424313368(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001c:
	{
		Rect_t4241904616  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m669681106((&V_0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  bool Rect_Overlaps_m2751672171_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Overlaps_m2751672171(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m89026168 (Rect_t4241904616 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m65342520((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m65342520((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m65342520((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m65342520((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Rect_GetHashCode_m89026168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_GetHashCode_m89026168(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t4241904616_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m1091722644_MetadataUsageId;
extern "C"  bool Rect_Equals_m1091722644 (Rect_t4241904616 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m1091722644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t4241904616_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Rect_t4241904616 *)((Rect_t4241904616 *)UnBox (L_1, Rect_t4241904616_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m982385354(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m2110115959((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m982386315(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m2110115959((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m2824209432(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m2110115959((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m2154960823(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m2110115959((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Rect_Equals_m1091722644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t4241904616 * _thisAdjusted = reinterpret_cast<Rect_t4241904616 *>(__this + 1);
	return Rect_Equals_m1091722644(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m2236552616 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1552341101 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___lhs0, Rect_t4241904616  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m982385354((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m982385354((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m982386315((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m982386315((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m2824209432((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2824209432((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m2154960823((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2154960823((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_pinvoke& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_pinvoke_back(const Rect_t4241904616_marshaled_pinvoke& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_pinvoke_cleanup(Rect_t4241904616_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com(const Rect_t4241904616& unmarshaled, Rect_t4241904616_marshaled_com& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t4241904616_marshal_com_back(const Rect_t4241904616_marshaled_com& marshaled, Rect_t4241904616& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t4241904616_marshal_com_cleanup(Rect_t4241904616_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2395783478 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m358348983 (RectOffset_t3056157787 * __this, GUIStyle_t2990928826 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m2631865360 (RectOffset_t3056157787 * __this, int32_t ___left0, int32_t ___right1, int32_t ___top2, int32_t ___bottom3, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3353955934(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left0;
		RectOffset_set_left_m901965251(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right1;
		RectOffset_set_right_m2119805444(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top2;
		RectOffset_set_top_m3043172093(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom3;
		RectOffset_set_bottom_m3840454247(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3353955934 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m3353955934_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Init_m3353955934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3353955934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m2914212664 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m2914212664_ftn) (RectOffset_t3056157787 *);
	static RectOffset_Cleanup_m2914212664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m2914212664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m4104523390 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4104523390_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_left_m4104523390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4104523390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m901965251 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m901965251_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_left_m901965251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m901965251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m3831383975 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m3831383975_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_right_m3831383975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m3831383975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m2119805444 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m2119805444_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_right_m2119805444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m2119805444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m140097312 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m140097312_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_top_m140097312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m140097312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3043172093 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3043172093_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_top_m3043172093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3043172093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m2106858018 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m2106858018_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_bottom_m2106858018_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m2106858018_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m3840454247 (RectOffset_t3056157787 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m3840454247_ftn) (RectOffset_t3056157787 *, int32_t);
	static RectOffset_set_bottom_m3840454247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m3840454247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m1186440923 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m1186440923_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_horizontal_m1186440923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m1186440923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3650431789 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3650431789_ftn) (RectOffset_t3056157787 *);
	static RectOffset_get_vertical_m3650431789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3650431789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Add_m396754502 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Add_m775012682(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m775012682 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Add_m775012682_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Add_m775012682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m775012682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t4241904616  RectOffset_Remove_m843726027 (RectOffset_t3056157787 * __this, Rect_t4241904616  ___rect0, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Remove_m1782283077(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1782283077 (Il2CppObject * __this /* static, unused */, RectOffset_t3056157787 * ___self0, Rect_t4241904616 * ___rect1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn) (RectOffset_t3056157787 *, Rect_t4241904616 *, Rect_t4241904616 *);
	static RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1782283077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m3416542060 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t2990928826 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m2914212664(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2126166178;
extern const uint32_t RectOffset_ToString_m2231965149_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m2231965149 (RectOffset_t3056157787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m2231965149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m4104523390(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t1108656482* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m3831383975(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m140097312(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m2106858018(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m427603113(NULL /*static, unused*/, _stringLiteral2126166178, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_pinvoke_back(const RectOffset_t3056157787_marshaled_pinvoke& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_pinvoke_cleanup(RectOffset_t3056157787_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com(const RectOffset_t3056157787& unmarshaled, RectOffset_t3056157787_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3056157787_marshal_com_back(const RectOffset_t3056157787_marshaled_com& marshaled, RectOffset_t3056157787& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3056157787_marshal_com_cleanup(RectOffset_t3056157787_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId;
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1968705467 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1968705467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId;
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m2607613076 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t779639188 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m2607613076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t779639188 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t779639188 *)CastclassSealed(L_2, ReapplyDrivenProperties_t779639188_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t4241904616  RectTransform_get_rect_m1566017036 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_rect_m1980775561(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1980775561 (RectTransform_t972643934 * __this, Rect_t4241904616 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1980775561_ftn) (RectTransform_t972643934 *, Rect_t4241904616 *);
	static RectTransform_INTERNAL_get_rect_m1980775561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1980775561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMin_m688674174 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMin_m1139643287(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m989253483 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m370577571(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m1139643287 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m1139643287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m370577571 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m370577571_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMin_m370577571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m370577571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t4282066565  RectTransform_get_anchorMax_m688445456 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMax_m1238440233(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m715345817 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m469374517(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m1238440233 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m1238440233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m469374517 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m469374517_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchorMax_m469374517_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m469374517_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t4282066565  RectTransform_get_anchoredPosition_m2318455998 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m840986985(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m1498949997 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m2329865949(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m840986985 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m840986985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m2329865949 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m2329865949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t4282066565  RectTransform_get_sizeDelta_m4279424984 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_sizeDelta_m4117062897(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m1223846609 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m3347997181(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m4117062897 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m4117062897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m3347997181 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m3347997181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t4282066565  RectTransform_get_pivot_m3785570595 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_pivot_m322514492(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m457344806 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m237146440(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m322514492 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m322514492_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_get_pivot_m322514492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m322514492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m237146440 (RectTransform_t972643934 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m237146440_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *);
	static RectTransform_INTERNAL_set_pivot_m237146440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m237146440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m2261331528 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m2261331528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t779639188 * L_0 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t779639188 * L_1 = ((RectTransform_t972643934_StaticFields*)RectTransform_t972643934_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t972643934 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3880635155(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014291545;
extern const uint32_t RectTransform_GetLocalCorners_m1867617311_MetadataUsageId;
extern "C"  void RectTransform_GetLocalCorners_m1867617311 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1867617311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral4014291545, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t4241904616  L_2 = RectTransform_get_rect_m1566017036(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m982385354((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m982386315((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t4282066566  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t215400611* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t4282066566  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2926210380(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t215400611* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t4282066566  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2926210380(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t215400611* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t4282066566  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2926210380(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1157240146;
extern const uint32_t RectTransform_GetWorldCorners_m1829917190_MetadataUsageId;
extern "C"  void RectTransform_GetWorldCorners_m1829917190 (RectTransform_t972643934 * __this, Vector3U5BU5D_t215400611* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m1829917190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t1659122786 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t215400611* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral1157240146, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t215400611* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1867617311(__this, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t215400611* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t1659122786 * L_6 = V_0;
		Vector3U5BU5D_t215400611* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t4282066566  L_9 = Transform_TransformPoint_m437395512(L_6, (*(Vector3_t4282066566 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m951793481 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t4282066565  L_7 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_0;
		Vector2_t4282066565  L_9 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_9, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_11 = V_0;
		Vector2_t4282066565  L_12 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_14 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m677885815 (RectTransform_t972643934 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___value0;
		Vector2_t4282066565  L_1 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_2 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t4282066565  L_4 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_5 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t4282066565  L_6 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = Vector2_op_Subtraction_m2097149401(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t4282066565  L_9 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = V_0;
		Vector2_t4282066565  L_11 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m1223846609(__this, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_0;
		Vector2_t4282066565  L_14 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_15 = Vector2_Scale_m1743563745(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = Vector2_op_Addition_m1173049553(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1498949997(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m1924354604 (RectTransform_t972643934 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t4282066565  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t4282066565  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t4282066565 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t4282066565 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t4282066565 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t4282066565  L_5 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t4282066565  L_8 = V_3;
		RectTransform_set_anchorMin_m989253483(__this, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_9 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m2767519328((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t4282066565  L_12 = V_3;
		RectTransform_set_anchorMax_m715345817(__this, L_12, /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m2767519328((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t4282066565  L_16 = V_4;
		RectTransform_set_sizeDelta_m1223846609(__this, L_16, /*hidden argument*/NULL);
		Vector2_t4282066565  L_17 = RectTransform_get_anchoredPosition_m2318455998(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t4282066565  L_22 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2185542843((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t4282066565  L_27 = RectTransform_get_pivot_m3785570595(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2185542843((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m2767519328(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t4282066565  L_30 = V_5;
		RectTransform_set_anchoredPosition_m1498949997(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m4019722691 (RectTransform_t972643934 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t4282066565  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t4282066565  L_1 = RectTransform_get_sizeDelta_m4279424984(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t4282066565  L_4 = RectTransform_GetParentSize_m3092718635(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2185542843((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t4282066565  L_7 = RectTransform_get_anchorMax_m688445456(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2185542843((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = RectTransform_get_anchorMin_m688674174(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2185542843((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t4282066565  L_13 = V_1;
		RectTransform_set_sizeDelta_m1223846609(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m3092718635_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransform_GetParentSize_m3092718635 (RectTransform_t972643934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m3092718635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t972643934 * V_0 = NULL;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t1659122786 * L_0 = Transform_get_parent_m2236876972(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t972643934 *)IsInstSealed(L_0, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t4282066565  L_3 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t972643934 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t4241904616  L_5 = RectTransform_get_rect_m1566017036(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t4282066565  L_6 = Rect_get_size_m136480416((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m3710908308 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3880635155 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3880635155((ReapplyDrivenProperties_t779639188 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t972643934 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m1851329218 (ReapplyDrivenProperties_t779639188 * __this, RectTransform_t972643934 * ___driven0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m3686159268 (ReapplyDrivenProperties_t779639188 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m4293768260_MetadataUsageId;
extern "C"  void RectTransformUtility__cctor_m4293768260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m4293768260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t3025555048_StaticFields*)RectTransformUtility_t3025555048_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1460676684 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1460676684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		Camera_t2727095145 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565 * ___screenPoint1, Camera_t2727095145 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn) (RectTransform_t972643934 *, Vector2_t4282066565 *, Camera_t2727095145 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m1592514141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId;
extern "C"  Vector2_t4282066565  RectTransformUtility_PixelAdjustPoint_m2518308759 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m2518308759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565  L_0 = ___point0;
		Transform_t1659122786 * L_1 = ___elementTransform1;
		Canvas_t2727140764 * L_2 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m1313063708(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId;
extern "C"  void RectTransformUtility_PixelAdjustPoint_m1313063708 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m1313063708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = ___elementTransform1;
		Canvas_t2727140764 * L_1 = ___canvas2;
		Vector2_t4282066565 * L_2 = ___output3;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565 * ___point0, Transform_t1659122786 * ___elementTransform1, Canvas_t2727140764 * ___canvas2, Vector2_t4282066565 * ___output3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn) (Vector2_t4282066565 *, Transform_t1659122786 *, Canvas_t2727140764 *, Vector2_t4282066565 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m1509640863_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___output3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId;
extern "C"  Rect_t4241904616  RectTransformUtility_PixelAdjustRect_m3727716130 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m3727716130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_t972643934 * L_0 = ___rectTransform0;
		Canvas_t2727140764 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rectTransform0, Canvas_t2727140764 * ___canvas1, Rect_t4241904616 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn) (RectTransform_t972643934 *, Canvas_t2727140764 *, Rect_t4241904616 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1038323288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector3_t4282066566 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t3134616544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t4206452690  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_t4282066566 * L_0 = ___worldPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_0) = L_2;
		Camera_t2727095145 * L_3 = ___cam2;
		Vector2_t4282066565  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Ray_t3134616544  L_5 = RectTransformUtility_ScreenPointToRay_m1216104542(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t972643934 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t1553702882  L_7 = Transform_get_rotation_m11483428(L_6, /*hidden argument*/NULL);
		Vector3_t4282066566  L_8 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		Plane__ctor_m2201046863((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t3134616544  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2829769106((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0046:
	{
		Vector3_t4282066566 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t4282066566  L_16 = Ray_GetPoint_m1171104822((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)L_14) = L_16;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, Vector2_t4282066565  ___screenPoint1, Camera_t2727095145 * ___cam2, Vector2_t4282066565 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t4282066565 * L_0 = ___localPoint3;
		Vector2_t4282066565  L_1 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_0) = L_1;
		RectTransform_t972643934 * L_2 = ___rect0;
		Vector2_t4282066565  L_3 = ___screenPoint1;
		Camera_t2727095145 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m3856201718(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t4282066565 * L_6 = ___localPoint3;
		RectTransform_t972643934 * L_7 = ___rect0;
		Vector3_t4282066566  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t4282066566  L_9 = Transform_InverseTransformPoint_m1626812000(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t4282066565  L_10 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)L_6) = L_10;
		return (bool)1;
	}

IL_002e:
	{
		return (bool)0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m1216104542_MetadataUsageId;
extern "C"  Ray_t3134616544  RectTransformUtility_ScreenPointToRay_m1216104542 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, Vector2_t4282066565  ___screenPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m1216104542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t2727095145 * L_2 = ___cam0;
		Vector2_t4282066565  L_3 = ___screenPos1;
		Vector3_t4282066566  L_4 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3134616544  L_5 = Camera_ScreenPointToRay_m1733083890(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t4282066565  L_6 = ___screenPos1;
		Vector3_t4282066566  L_7 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t4282066566 * L_8 = (&V_0);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t4282066566  L_10 = V_0;
		Vector3_t4282066566  L_11 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3134616544  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m2662468509(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3487429352 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3487429352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t4282066565  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t4282066565  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3487429352(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t972643934 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m2107810675(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2185542843((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_19 = ___rect0;
		Vector2_t4282066565  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m457344806(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t4282066565  L_23 = RectTransform_get_anchoredPosition_m2318455998(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2185542843((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_27 = ___rect0;
		Vector2_t4282066565  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m1498949997(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t4282066565  L_30 = RectTransform_get_anchorMin_m688674174(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2185542843((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2185542843((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m2767519328((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m2767519328((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t972643934 * L_40 = ___rect0;
		Vector2_t4282066565  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m989253483(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_42 = ___rect0;
		Vector2_t4282066565  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m715345817(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t972643934_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t3025555048_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutAxes_m2163490602 (Il2CppObject * __this /* static, unused */, RectTransform_t972643934 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m2163490602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t972643934 * V_1 = NULL;
	{
		RectTransform_t972643934 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t972643934 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t1659122786 * L_5 = Transform_GetChild_m4040462992(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t972643934 *)IsInstSealed(L_5, RectTransform_t972643934_il2cpp_TypeInfo_var));
		RectTransform_t972643934 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t972643934 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m2163490602(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t972643934 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m2107810675(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t972643934 * L_13 = ___rect0;
		RectTransform_t972643934 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t4282066565  L_15 = RectTransform_get_pivot_m3785570595(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_16 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m457344806(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_17 = ___rect0;
		RectTransform_t972643934 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = RectTransform_get_sizeDelta_m4279424984(L_18, /*hidden argument*/NULL);
		Vector2_t4282066565  L_20 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m1223846609(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t972643934 * L_22 = ___rect0;
		RectTransform_t972643934 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t4282066565  L_24 = RectTransform_get_anchoredPosition_m2318455998(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t3025555048_il2cpp_TypeInfo_var);
		Vector2_t4282066565  L_25 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m1498949997(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_26 = ___rect0;
		RectTransform_t972643934 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t4282066565  L_28 = RectTransform_get_anchorMin_m688674174(L_27, /*hidden argument*/NULL);
		Vector2_t4282066565  L_29 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m989253483(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t972643934 * L_30 = ___rect0;
		RectTransform_t972643934 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t4282066565  L_32 = RectTransform_get_anchorMax_m688445456(L_31, /*hidden argument*/NULL);
		Vector2_t4282066565  L_33 = RectTransformUtility_GetTransposed_m2060823533(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m715345817(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t4282066565  RectTransformUtility_GetTransposed_m2060823533 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___input0, const MethodInfo* method)
{
	{
		float L_0 = (&___input0)->get_y_2();
		float L_1 = (&___input0)->get_x_1();
		Vector2_t4282066565  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m1517109030(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m1971819706 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m1971819706_ftn) (Renderer_t3076687687 *);
	static Renderer_get_enabled_m1971819706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m1971819706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2514140131 (Renderer_t3076687687 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2514140131_ftn) (Renderer_t3076687687 *, bool);
	static Renderer_set_enabled_m2514140131_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2514140131_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t3870600107 * Renderer_get_material_m2720864603 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_material_m2720864603_ftn) (Renderer_t3076687687 *);
	static Renderer_get_material_m2720864603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2720864603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1012580896 (Renderer_t3076687687 * __this, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1012580896_ftn) (Renderer_t3076687687 *, Material_t3870600107 *);
	static Renderer_set_material_m1012580896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1012580896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t3870600107 * Renderer_get_sharedMaterial_m835478880 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef Material_t3870600107 * (*Renderer_get_sharedMaterial_m835478880_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterial_m835478880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m835478880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_materials_m3755041148 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_materials_m3755041148_ftn) (Renderer_t3076687687 *);
	static Renderer_get_materials_m3755041148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m3755041148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m268031319 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m268031319_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_materials_m268031319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m268031319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t170856778* Renderer_get_sharedMaterials_m1981818007 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t170856778* (*Renderer_get_sharedMaterials_m1981818007_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sharedMaterials_m1981818007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m1981818007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m1255100914 (Renderer_t3076687687 * __this, MaterialU5BU5D_t170856778* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m1255100914_ftn) (Renderer_t3076687687 *, MaterialU5BU5D_t170856778*);
	static Renderer_set_sharedMaterials_m1255100914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m1255100914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
extern "C"  bool Renderer_get_isVisible_m1011967393 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_isVisible_m1011967393_ftn) (Renderer_t3076687687 *);
	static Renderer_get_isVisible_m1011967393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_isVisible_m1011967393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_isVisible()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m1954594923 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m1954594923_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingLayerID_m1954594923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m1954594923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m3623465101 (Renderer_t3076687687 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m3623465101_ftn) (Renderer_t3076687687 *);
	static Renderer_get_sortingOrder_m3623465101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m3623465101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m1757489802 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fog_m1757489802_ftn) (bool);
	static RenderSettings_set_fog_m1757489802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fog_m1757489802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fog(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m1507677940 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_fogColor_m2299508764(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2299508764 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_fogColor_m2299508764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m998518996 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogDensity_m998518996_ftn) (float);
	static RenderSettings_set_fogDensity_m998518996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogDensity_m998518996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogDensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientLight_m791635003 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientLight_m2620477877(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientLight_m2620477877 (Il2CppObject * __this /* static, unused */, Color_t4194546905 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn) (Color_t4194546905 *);
	static RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientLight_m2620477877_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
extern "C"  void RenderSettings_set_haloStrength_m1179623713 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_haloStrength_m1179623713_ftn) (float);
	static RenderSettings_set_haloStrength_m1179623713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_haloStrength_m1179623713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_haloStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
extern "C"  void RenderSettings_set_flareStrength_m38596007 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_flareStrength_m38596007_ftn) (float);
	static RenderSettings_set_flareStrength_m38596007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_flareStrength_m38596007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_flareStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m3777670233 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_skybox_m3777670233_ftn) (Material_t3870600107 *);
	static RenderSettings_set_skybox_m3777670233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_skybox_m3777670233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m1030655936 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m1030655936_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetWidth_m1030655936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m1030655936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m1940011033 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m1940011033_ftn) (RenderTexture_t1963041563 *);
	static RenderTexture_Internal_GetHeight_m1940011033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m1940011033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1498578543 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m1030655936(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4010076224 (RenderTexture_t1963041563 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m1940011033(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m2023271172 (RequireComponent_t1687166108 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m2863879896 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m162101250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t3071478659 * ResourceRequest_get_asset_m670320982 (ResourceRequest_t3731857623 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_back(const ResourceRequest_t3731857623_marshaled_pinvoke& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_pinvoke_cleanup(ResourceRequest_t3731857623_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com(const ResourceRequest_t3731857623& unmarshaled, ResourceRequest_t3731857623_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3731857623_marshal_com_back(const ResourceRequest_t3731857623_marshaled_com& marshaled, ResourceRequest_t3731857623& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3731857623_marshal_com_cleanup(ResourceRequest_t3731857623_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2187391845_MetadataUsageId;
extern "C"  Object_t3071478659 * Resources_Load_m2187391845 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2187391845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		Object_t3071478659 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t3071478659 * Resources_Load_m3601699608 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_Load_m3601699608_ftn) (String_t*, Type_t *);
	static Resources_Load_m3601699608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m3601699608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1015136018* Resources_LoadAll_m2472905169 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1015136018* (*Resources_LoadAll_m2472905169_ftn) (String_t*, Type_t *);
	static Resources_LoadAll_m2472905169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_LoadAll_m2472905169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::LoadAll(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String)
extern const Il2CppType* Object_t3071478659_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_LoadAll_m2517792670_MetadataUsageId;
extern "C"  ObjectU5BU5D_t1015136018* Resources_LoadAll_m2517792670 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_LoadAll_m2517792670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Object_t3071478659_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t1015136018* L_2 = Resources_LoadAll_m2472905169(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t3071478659 * Resources_GetBuiltinResource_m1250078467 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t3071478659 * (*Resources_GetBuiltinResource_m1250078467_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m1250078467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m1250078467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern "C"  AsyncOperation_t3699081103 * Resources_UnloadUnusedAssets_m3831138427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*Resources_UnloadUnusedAssets_m3831138427_ftn) ();
	static Resources_UnloadUnusedAssets_m3831138427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_UnloadUnusedAssets_m3831138427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::UnloadUnusedAssets()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t4282066566  Rigidbody_get_velocity_m2696244068 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_velocity_m1063590501(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m799562119 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m484592601(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1063590501 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1063590501_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_velocity_m1063590501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1063590501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m484592601 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m484592601_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_set_velocity_m484592601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m484592601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_drag()
extern "C"  float Rigidbody_get_drag_m3710595753 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_drag_m3710595753_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_drag_m3710595753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_drag_m3710595753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_drag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m4061586082 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m4061586082_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_drag_m4061586082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m4061586082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_angularDrag()
extern "C"  float Rigidbody_get_angularDrag_m2925737411 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_angularDrag_m2925737411_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_angularDrag_m2925737411_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_angularDrag_m2925737411_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_angularDrag()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern "C"  void Rigidbody_set_angularDrag_m2909317064 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_angularDrag_m2909317064_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_angularDrag_m2909317064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularDrag_m2909317064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularDrag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_mass()
extern "C"  float Rigidbody_get_mass_m3953106025 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_mass_m3953106025_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_mass_m3953106025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m3953106025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m1579962594 (Rigidbody_t3346577219 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m1579962594_ftn) (Rigidbody_t3346577219 *, float);
	static Rigidbody_set_mass_m1579962594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m1579962594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2620827635 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2620827635_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_useGravity_m2620827635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2620827635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m3963857442 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m3963857442_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_get_isKinematic_m3963857442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m3963857442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m294703295 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m294703295_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_isKinematic_m294703295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m294703295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m3989473889 (Rigidbody_t3346577219 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m3989473889_ftn) (Rigidbody_t3346577219 *, bool);
	static Rigidbody_set_freezeRotation_m3989473889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m3989473889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m557267180 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3651654387(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3651654387 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3651654387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m2803598808 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeForce_m3589442951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddTorque_m3009708185 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddTorque_m4194065160(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m4194065160 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m4194065160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeTorque_m3926511917 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeTorque_m2265510644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m1266619363 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___force0, Vector3_t4282066566  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___force1, Vector3_t4282066566 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *, Vector3_t4282066566 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1493622644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddExplosionForce_m196999228 (Rigidbody_t3346577219 * __this, float ___explosionForce0, Vector3_t4282066566  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const MethodInfo* method)
{
	{
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = ___mode4;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, float ___explosionForce1, Vector3_t4282066566 * ___explosionPosition2, float ___explosionRadius3, float ___upwardsModifier4, int32_t ___mode5, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn) (Rigidbody_t3346577219 *, float, Vector3_t4282066566 *, float, float, int32_t);
	static Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddExplosionForce_m3109367769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___explosionForce1, ___explosionPosition2, ___explosionRadius3, ___upwardsModifier4, ___mode5);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
extern "C"  Vector3_t4282066566  Rigidbody_get_centerOfMass_m3434430695 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_centerOfMass_m3640748392(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_centerOfMass_m3640748392 (Rigidbody_t3346577219 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_centerOfMass_m3640748392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_centerOfMass(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1515094375 (Rigidbody_t3346577219 * __this, Vector3_t4282066566  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2416276686(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2416276686 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Vector3_t4282066566 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn) (Rigidbody_t3346577219 *, Vector3_t4282066566 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2416276686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m38358738 (Rigidbody_t3346577219 * __this, Quaternion_t1553702882  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, Quaternion_t1553702882 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn) (Rigidbody_t3346577219 *, Quaternion_t1553702882 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m4110814929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m4049131361 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m1292822714(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m1292822714 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m1292822714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern "C"  bool Rigidbody_IsSleeping_m435617895 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
extern "C"  bool Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef bool (*Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_IsSleeping_m4112513622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::WakeUp()
extern "C"  void Rigidbody_WakeUp_m2643728503 (Rigidbody_t3346577219 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_WakeUp_m2627563334(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_WakeUp_m2627563334 (Il2CppObject * __this /* static, unused */, Rigidbody_t3346577219 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn) (Rigidbody_t3346577219 *);
	static Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_WakeUp_m2627563334_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t4282066565  Rigidbody2D_get_velocity_m416159605 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_velocity_m715507538(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t4282066565  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m100625302 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_velocity_m136509638(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_velocity_m715507538 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_velocity_m715507538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m136509638 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *);
	static Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_velocity_m136509638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C"  float Rigidbody2D_get_mass_m3688503547 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_mass_m3688503547_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_mass_m3688503547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_mass_m3688503547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern "C"  void Rigidbody2D_set_mass_m610116752 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_mass_m610116752_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_mass_m610116752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_mass_m610116752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern "C"  void Rigidbody2D_set_gravityScale_m2024998120 (Rigidbody2D_t1743771669 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_gravityScale_m2024998120_ftn) (Rigidbody2D_t1743771669 *, float);
	static Rigidbody2D_set_gravityScale_m2024998120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_gravityScale_m2024998120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_gravityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern "C"  bool Rigidbody2D_get_isKinematic_m957232848 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_isKinematic_m957232848_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_isKinematic_m957232848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_isKinematic_m957232848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody2D_set_isKinematic_m222467693 (Rigidbody2D_t1743771669 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_isKinematic_m222467693_ftn) (Rigidbody2D_t1743771669 *, bool);
	static Rigidbody2D_set_isKinematic_m222467693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_isKinematic_m222467693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern "C"  int32_t Rigidbody2D_get_constraints_m3440684386 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_constraints_m3440684386_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_get_constraints_m3440684386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_constraints_m3440684386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_constraints()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern "C"  void Rigidbody2D_set_constraints_m3508094335 (Rigidbody2D_t1743771669 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_constraints_m3508094335_ftn) (Rigidbody2D_t1743771669 *, int32_t);
	static Rigidbody2D_set_constraints_m3508094335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_constraints_m3508094335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern "C"  bool Rigidbody2D_IsSleeping_m4134977273 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsSleeping_m4134977273_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_IsSleeping_m4134977273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsSleeping_m4134977273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsSleeping()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::Sleep()
extern "C"  void Rigidbody2D_Sleep_m1892894479 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_Sleep_m1892894479_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_Sleep_m1892894479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_Sleep_m1892894479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::Sleep()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::WakeUp()
extern "C"  void Rigidbody2D_WakeUp_m224894601 (Rigidbody2D_t1743771669 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_WakeUp_m224894601_ftn) (Rigidbody2D_t1743771669 *);
	static Rigidbody2D_WakeUp_m224894601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_WakeUp_m224894601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::WakeUp()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m4161385513 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForce_m2763823108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForceAtPosition_m174512961 (Rigidbody2D_t1743771669 * __this, Vector2_t4282066565  ___force0, Vector2_t4282066565  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t1743771669 * ___self0, Vector2_t4282066565 * ___force1, Vector2_t4282066565 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn) (Rigidbody2D_t1743771669 *, Vector2_t4282066565 *, Vector2_t4282066565 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m1988757918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddTorque_m3138315435 (Rigidbody2D_t1743771669 * __this, float ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_AddTorque_m3138315435_ftn) (Rigidbody2D_t1743771669 *, float, int32_t);
	static Rigidbody2D_AddTorque_m3138315435_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddTorque_m3138315435_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___torque0, ___mode1);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m281827604 (RPC_t3134615963 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m2277248521 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_get_handle_m2277248521_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_handle_m2277248521(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m894591657 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m2496405436(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* Scene_get_name_m894591657_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_name_m894591657(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3533090789 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		int32_t L_1 = Scene_GetBuildIndexInternal_m76376146(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t Scene_get_buildIndex_m3533090789_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_get_buildIndex_m3533090789(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m2000109307 (Scene_t1080795294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_GetHashCode_m2000109307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_GetHashCode_m2000109307(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern Il2CppClass* Scene_t1080795294_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m93578403_MetadataUsageId;
extern "C"  bool Scene_Equals_m93578403 (Scene_t1080795294 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m93578403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1080795294_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Scene_t1080795294 *)((Scene_t1080795294 *)UnBox (L_1, Scene_t1080795294_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m2277248521(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m2277248521((&V_0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool Scene_Equals_m93578403_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1080795294 * _thisAdjusted = reinterpret_cast<Scene_t1080795294 *>(__this + 1);
	return Scene_Equals_m93578403(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m2496405436 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef String_t* (*Scene_GetNameInternal_m2496405436_ftn) (int32_t);
	static Scene_GetNameInternal_m2496405436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m2496405436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern "C"  int32_t Scene_GetBuildIndexInternal_m76376146 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef int32_t (*Scene_GetBuildIndexInternal_m76376146_ftn) (int32_t);
	static Scene_GetBuildIndexInternal_m76376146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetBuildIndexInternal_m76376146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_pinvoke_back(const Scene_t1080795294_marshaled_pinvoke& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_pinvoke_cleanup(Scene_t1080795294_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com(const Scene_t1080795294& unmarshaled, Scene_t1080795294_marshaled_com& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1080795294_marshal_com_back(const Scene_t1080795294_marshaled_com& marshaled, Scene_t1080795294& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1080795294_marshal_com_cleanup(Scene_t1080795294_marshaled_com& marshaled)
{
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1080795294  SceneManager_GetActiveScene_m3062973092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1080795294  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006 (Il2CppObject * __this /* static, unused */, Scene_t1080795294 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn) (Scene_t1080795294 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m3594338006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2167814033 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m3907168970(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m3907168970 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m193744610 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m2455768283(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m2455768283 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	Il2CppObject * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Il2CppObject * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Il2CppObject * G_B3_2 = NULL;
	{
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = ___mode1;
		G_B1_0 = L_0;
		G_B1_1 = NULL;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = L_0;
			G_B2_1 = NULL;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, (String_t*)G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsync_m1685951617 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		AsyncOperation_t3699081103 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3699081103 * SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3699081103 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3775081569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern Il2CppClass* SceneManager_t2940962239_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1664755053_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m1398790415_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneLoaded_m1398790415 (Il2CppObject * __this /* static, unused */, Scene_t1080795294  ___scene0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m1398790415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_2_t1937996761 * L_0 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityAction_2_t1937996761 * L_1 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		Scene_t1080795294  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1664755053(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1664755053_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t2940962239_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m4076964868_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m3773648285_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneUnloaded_m3773648285 (Il2CppObject * __this /* static, unused */, Scene_t1080795294  ___scene0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m3773648285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_1_t974975297 * L_0 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		UnityAction_1_t974975297 * L_1 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		Scene_t1080795294  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m4076964868(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m4076964868_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t2940962239_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1752698864_MethodInfo_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m3583151927_MetadataUsageId;
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m3583151927 (Il2CppObject * __this /* static, unused */, Scene_t1080795294  ___previousActiveScene0, Scene_t1080795294  ___newActiveScene1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m3583151927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_2_t4246757468 * L_0 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityAction_2_t4246757468 * L_1 = ((SceneManager_t2940962239_StaticFields*)SceneManager_t2940962239_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		Scene_t1080795294  L_2 = ___previousActiveScene0;
		Scene_t1080795294  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1752698864(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1752698864_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3080333084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m3080333084_ftn) ();
	static Screen_get_width_m3080333084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m3080333084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1504859443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1504859443_ftn) ();
	static Screen_get_height_m1504859443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1504859443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3780069159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3780069159_ftn) ();
	static Screen_get_dpi_m3780069159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3780069159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_sleepTimeout()
extern "C"  int32_t Screen_get_sleepTimeout_m4077361558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_sleepTimeout_m4077361558_ftn) ();
	static Screen_get_sleepTimeout_m4077361558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_sleepTimeout_m4077361558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_sleepTimeout()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject__ctor_m1827087273_MetadataUsageId;
extern "C"  void ScriptableObject__ctor_m1827087273 (ScriptableObject_t2970544072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m1827087273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object__ctor_m570634126(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m2334361070(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m2334361070 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2970544072 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn) (ScriptableObject_t2970544072 *);
	static ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m2334361070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m750914562 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstance_m750914562_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m750914562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m750914562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstance_m3255479417 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2970544072 * L_1 = ScriptableObject_CreateInstanceFromType_m3795352533(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2970544072 * ScriptableObject_CreateInstanceFromType_m3795352533 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t2970544072 * (*ScriptableObject_CreateInstanceFromType_m3795352533_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3795352533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3795352533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_back(const ScriptableObject_t2970544072_marshaled_pinvoke& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_pinvoke_cleanup(ScriptableObject_t2970544072_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com(const ScriptableObject_t2970544072& unmarshaled, ScriptableObject_t2970544072_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t2970544072_marshal_com_back(const ScriptableObject_t2970544072_marshaled_com& marshaled, ScriptableObject_t2970544072& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2970544072_marshal_com_cleanup(ScriptableObject_t2970544072_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m940065582 (RequiredByNativeCodeAttribute_t3165457172 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m3320039756 (UsedByNativeCodeAttribute_t3197444790 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m3830336046 (SelectionBaseAttribute_t204085987 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m2731695210_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m2731695210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m2731695210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t3209134097  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t3209134097  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t3209134097  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t3209134097  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t3209134097  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t3209134097  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t3209134097  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t3209134097  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t3452915852* L_0 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t3209134097  L_1 = V_0;
		(*(HitInfo_t3209134097 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t3452915852* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t3209134097  L_3 = V_1;
		(*(HitInfo_t3209134097 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t3452915852* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_5 = V_2;
		(*(HitInfo_t3209134097 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t3452915852* L_6 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t3209134097  L_7 = V_3;
		(*(HitInfo_t3209134097 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t3209134097  L_9 = V_4;
		(*(HitInfo_t3209134097 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t3452915852* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t3209134097  L_11 = V_5;
		(*(HitInfo_t3209134097 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t3452915852* L_12 = ((HitInfoU5BU5D_t3452915852*)SZArrayNew(HitInfoU5BU5D_t3452915852_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t3209134097  L_13 = V_6;
		(*(HitInfo_t3209134097 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t3452915852* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t3209134097  L_15 = V_7;
		(*(HitInfo_t3209134097 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t3452915852* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t3209134097  L_17 = V_8;
		(*(HitInfo_t3209134097 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m2590456785 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m2590456785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m4104134333 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m4104134333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t2727095145 * V_3 = NULL;
	CameraU5BU5D_t2716570836* V_4 = NULL;
	int32_t V_5 = 0;
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	GUILayer_t2983897946 * V_7 = NULL;
	GUIElement_t3775428101 * V_8 = NULL;
	Ray_t3134616544  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t3674682005 * V_12 = NULL;
	GameObject_t3674682005 * V_13 = NULL;
	int32_t V_14 = 0;
	HitInfo_t3209134097  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t4282066566  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m3993434431(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_2 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_3 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t2716570836*)SZArrayNew(CameraU5BU5D_t2716570836_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_6 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m3771867787(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_7 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t3209134097  L_9 = V_15;
		(*(HitInfo_t3209134097 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_12 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		CameraU5BU5D_t2716570836* L_14 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t2716570836* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Camera_t2727095145 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = L_18;
		Camera_t2727095145 * L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t2727095145 * L_22 = V_3;
		NullCheck(L_22);
		RenderTexture_t1963041563 * L_23 = Camera_get_targetTexture_m1468336738(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_23, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t2727095145 * L_25 = V_3;
		NullCheck(L_25);
		Rect_t4241904616  L_26 = Camera_get_pixelRect_m936851539(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t4282066566  L_27 = V_0;
		bool L_28 = Rect_Contains_m3556594041((&V_6), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t2727095145 * L_29 = V_3;
		NullCheck(L_29);
		GUILayer_t2983897946 * L_30 = Component_GetComponent_TisGUILayer_t2983897946_m2891371969(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t2983897946_m2891371969_MethodInfo_var);
		V_7 = L_30;
		GUILayer_t2983897946 * L_31 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t2983897946 * L_33 = V_7;
		Vector3_t4282066566  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3775428101 * L_35 = GUILayer_HitTest_m3356120918(L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		GUIElement_t3775428101 * L_36 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_38 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		GUIElement_t3775428101 * L_39 = V_8;
		NullCheck(L_39);
		GameObject_t3674682005 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t3452915852* L_41 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		Camera_t2727095145 * L_42 = V_3;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_44 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_0145:
	{
		Camera_t2727095145 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m3669132771(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t2727095145 * L_47 = V_3;
		Vector3_t4282066566  L_48 = V_0;
		NullCheck(L_47);
		Ray_t3134616544  L_49 = Camera_ScreenPointToRay_m1733083890(L_47, L_48, /*hidden argument*/NULL);
		V_9 = L_49;
		Vector3_t4282066566  L_50 = Ray_get_direction_m3201866877((&V_9), /*hidden argument*/NULL);
		V_16 = L_50;
		float L_51 = (&V_16)->get_z_3();
		V_10 = L_51;
		float L_52 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1395529776(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t2727095145 * L_54 = V_3;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m388706726(L_54, /*hidden argument*/NULL);
		Camera_t2727095145 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m4074655061(L_56, /*hidden argument*/NULL);
		float L_58 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B23_0 = L_59;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t2727095145 * L_60 = V_3;
		Ray_t3134616544  L_61 = V_9;
		float L_62 = V_11;
		Camera_t2727095145 * L_63 = V_3;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m1045975289(L_63, /*hidden argument*/NULL);
		Camera_t2727095145 * L_65 = V_3;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m3669132771(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t3674682005 * L_67 = Camera_RaycastTry_m569221064(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_12 = L_67;
		GameObject_t3674682005 * L_68 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_68, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_70 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		GameObject_t3674682005 * L_71 = V_12;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t3452915852* L_72 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		Camera_t2727095145 * L_73 = V_3;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t2727095145 * L_74 = V_3;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m192466552(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t2727095145 * L_76 = V_3;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m192466552(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_78 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_79 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_022a:
	{
		Camera_t2727095145 * L_80 = V_3;
		Ray_t3134616544  L_81 = V_9;
		float L_82 = V_11;
		Camera_t2727095145 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m1045975289(L_83, /*hidden argument*/NULL);
		Camera_t2727095145 * L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m3669132771(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t3674682005 * L_87 = Camera_RaycastTry2D_m3256311322(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_13 = L_87;
		GameObject_t3674682005 * L_88 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_88, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_90 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 2);
		GameObject_t3674682005 * L_91 = V_13;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t3452915852* L_92 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 2);
		Camera_t2727095145 * L_93 = V_3;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t2727095145 * L_94 = V_3;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m192466552(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t2727095145 * L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m192466552(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_98 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t3674682005 *)NULL);
		HitInfoU5BU5D_t3452915852* L_99 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 2);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t2727095145 *)NULL);
	}

IL_02b2:
	{
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_101 = V_5;
		CameraU5BU5D_t2716570836* L_102 = V_4;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_103 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_104 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		SendMouseEvents_SendEvents_m3877180750(NULL /*static, unused*/, L_103, (*(HitInfo_t3209134097 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_14;
		V_14 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_107 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_108 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3965481900_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t3209134097_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2222972840;
extern Il2CppCodeGenString* _stringLiteral2438936645;
extern Il2CppCodeGenString* _stringLiteral288346913;
extern Il2CppCodeGenString* _stringLiteral2222975034;
extern Il2CppCodeGenString* _stringLiteral2223306714;
extern Il2CppCodeGenString* _stringLiteral2223010852;
extern Il2CppCodeGenString* _stringLiteral193571986;
extern const uint32_t SendMouseEvents_SendEvents_m3877180750_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m3877180750 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t3209134097  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m3877180750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t3209134097  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m2031691843(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m4080958081(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t3209134097  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_5 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t3209134097  L_7 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t3452915852* L_8 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m2569183060(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral2222972840, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_11 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t3209134097  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_15 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_14, (*(HitInfo_t3209134097 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_18 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m2569183060(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral2438936645, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_20 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m2569183060(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral288346913, /*hidden argument*/NULL);
		HitInfoU5BU5D_t3452915852* L_22 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t3209134097_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t3209134097  L_24 = V_2;
		(*(HitInfo_t3209134097 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_25 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_28 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m2569183060(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral2222975034, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t3209134097  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_31 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m4083757090(NULL /*static, unused*/, L_30, (*(HitInfo_t3209134097 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t3209134097  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_36 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, (*(HitInfo_t3209134097 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_39 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m2569183060(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral2223010852, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t3209134097  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1943931337(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral193571986, /*hidden argument*/NULL);
		HitInfo_SendMessage_m2569183060((&___hit1), _stringLiteral2223306714, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3965481900_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t3452915852* L_43 = ((SendMouseEvents_t3965481900_StaticFields*)SendMouseEvents_t3965481900_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t3209134097  L_45 = ___hit1;
		(*(HitInfo_t3209134097 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m2569183060 (HitInfo_t3209134097 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m423373689(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m2569183060_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t3209134097 * _thisAdjusted = reinterpret_cast<HitInfo_t3209134097 *>(__this + 1);
	HitInfo_SendMessage_m2569183060(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_Compare_m4083757090_MetadataUsageId;
extern "C"  bool HitInfo_Compare_m4083757090 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___lhs0, HitInfo_t3209134097  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m4083757090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t3674682005 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t2727095145 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t2727095145 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_op_Implicit_m1943931337_MetadataUsageId;
extern "C"  bool HitInfo_op_Implicit_m1943931337 (Il2CppObject * __this /* static, unused */, HitInfo_t3209134097  ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m1943931337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		GameObject_t3674682005 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t2727095145 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_pinvoke_back(const HitInfo_t3209134097_marshaled_pinvoke& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_pinvoke_cleanup(HitInfo_t3209134097_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com(const HitInfo_t3209134097& unmarshaled, HitInfo_t3209134097_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t3209134097_marshal_com_back(const HitInfo_t3209134097_marshaled_com& marshaled, HitInfo_t3209134097& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t3209134097_marshal_com_cleanup(HitInfo_t3209134097_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m2703462003 (FormerlySerializedAsAttribute_t2216353654 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m2935479929 (FormerlySerializedAsAttribute_t2216353654 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_oldName_0();
		return L_0;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m4068807987 (SerializeField_t3754825534 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m889466149 (SerializePrivateVariables_t2835496234 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1859371625;
extern Il2CppCodeGenString* _stringLiteral2712312467;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m3556339879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m3556339879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m72843924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t928607144 * L_3 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_3, _stringLiteral1859371625, _stringLiteral2712312467, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m2322222010(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m3691215301 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m3691215301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		V_0 = (ObjectU5BU5D_t1108656482*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0013:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m2022236990(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t1108656482* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t1074302268 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, ParameterModifierU5BU5D_t3896472559*, CultureInfo_t1065375142 *, StringU5BU5D_t4054002952* >::Invoke(89 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1074302268 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t3896472559*)(ParameterModifierU5BU5D_t3896472559*)NULL, (CultureInfo_t1065375142 *)NULL, (StringU5BU5D_t4054002952*)(StringU5BU5D_t4054002952*)NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m3019342011 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m3019342011_ftn) (String_t*);
	static Shader_PropertyToID_m3019342011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m3019342011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m2764338918 (SharedBetweenAnimatorsAttribute_t1486273289 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_back(const SkeletonBone_t421858229_marshaled_pinvoke& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_pinvoke_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_pinvoke_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_pinvoke_cleanup(SkeletonBone_t421858229_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_pinvoke_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_pinvoke_cleanup(marshaled.___scale_3);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com(const SkeletonBone_t421858229& unmarshaled, SkeletonBone_t421858229_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	Vector3_t4282066566_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t1553702882_marshal_com(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t421858229_marshal_com_back(const SkeletonBone_t421858229_marshaled_com& marshaled, SkeletonBone_t421858229& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t4282066566  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t4282066566_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t1553702882  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t1553702882_marshal_com_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t4282066566  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t4282066566_marshal_com_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t421858229_marshal_com_cleanup(SkeletonBone_t421858229_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___position_1);
	Quaternion_t1553702882_marshal_com_cleanup(marshaled.___rotation_2);
	Vector3_t4282066566_marshal_com_cleanup(marshaled.___scale_3);
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C"  void SliderHandler__ctor_m4217573891 (SliderHandler_t783692703 * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	{
		Rect_t4241904616  L_0 = ___position0;
		__this->set_position_0(L_0);
		float L_1 = ___currentValue1;
		__this->set_currentValue_1(L_1);
		float L_2 = ___size2;
		__this->set_size_2(L_2);
		float L_3 = ___start3;
		__this->set_start_3(L_3);
		float L_4 = ___end4;
		__this->set_end_4(L_4);
		GUIStyle_t2990928826 * L_5 = ___slider5;
		__this->set_slider_5(L_5);
		GUIStyle_t2990928826 * L_6 = ___thumb6;
		__this->set_thumb_6(L_6);
		bool L_7 = ___horiz7;
		__this->set_horiz_7(L_7);
		int32_t L_8 = ___id8;
		__this->set_id_8(L_8);
		return;
	}
}
extern "C"  void SliderHandler__ctor_m4217573891_AdjustorThunk (Il2CppObject * __this, Rect_t4241904616  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t2990928826 * ___slider5, GUIStyle_t2990928826 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler__ctor_m4217573891(_thisAdjusted, ___position0, ___currentValue1, ___size2, ___start3, ___end4, ___slider5, ___thumb6, ___horiz7, ___id8, method);
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C"  float SliderHandler_Handle_m2409586512 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m3978233689(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m3958002946(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m3960111380(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m1318588539(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m4264550822(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = __this->get_currentValue_1();
		return L_9;
	}
}
extern "C"  float SliderHandler_Handle_m2409586512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Handle_m2409586512(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDown_m3958002946_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDown_m3958002946 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDown_m3958002946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Rect_t4241904616  L_0 = __this->get_position_0();
		V_1 = L_0;
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m3556594010((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = __this->get_currentValue_1();
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbSelectionRect_m3697323546(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t4196595728 * L_9 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t4282066565  L_10 = Event_get_mousePosition_m3610425949(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m3556594010((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_currentValue_1();
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t1233388262 * L_15 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_16 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t4283661327  L_17 = DateTime_AddMilliseconds_m1717403134((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m1228634973(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m493574741(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m3874943933(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m4218954710(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
extern "C"  float SliderHandler_OnMouseDown_m3958002946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDown_m3958002946(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDrag_m3960111380 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDrag_m3960111380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t1233388262 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_0017:
	{
		SliderState_t1233388262 * L_3 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t1233388262 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = L_4->get_isDragging_2();
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = __this->get_currentValue_1();
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Event_t4196595728 * L_7 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m310777444(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		SliderState_t1233388262 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = L_9->get_dragStartPos_0();
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t1233388262 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = L_11->get_dragStartValue_1();
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m4218954710(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  float SliderHandler_OnMouseDrag_m3960111380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseDrag_m3960111380(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseUp_m1318588539_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseUp_m1318588539 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseUp_m1318588539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t4196595728 * L_2 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m310777444(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = __this->get_currentValue_1();
		return L_3;
	}
}
extern "C"  float SliderHandler_OnMouseUp_m1318588539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnMouseUp_m1318588539(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern Il2CppClass* GUIContent_t2094828418_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t4036018645_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnRepaint_m4264550822_MetadataUsageId;
extern "C"  float SliderHandler_OnRepaint_m4264550822 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnRepaint_m4264550822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUIStyle_t2990928826 * L_0 = __this->get_slider_5();
		Rect_t4241904616  L_1 = __this->get_position_0();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_2 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_3 = __this->get_id_8();
		NullCheck(L_0);
		GUIStyle_Draw_m2994577084(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		Rect_t4241904616  L_6 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t2094828418_il2cpp_TypeInfo_var);
		GUIContent_t2094828418 * L_7 = ((GUIContent_t2094828418_StaticFields*)GUIContent_t2094828418_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_8 = __this->get_id_8();
		NullCheck(L_5);
		GUIStyle_Draw_m2994577084(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m4135893409(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_id_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_0 = L_11;
		Event_t4196595728 * L_12 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t4282066565  L_13 = Event_get_mousePosition_m3610425949(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m3556594010((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m2523580504(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = __this->get_currentValue_1();
		return L_16;
	}

IL_0083:
	{
		Rect_t4241904616  L_17 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t4196595728 * L_18 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t4282066565  L_19 = Event_get_mousePosition_m3610425949(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m3556594010((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m300477798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = __this->get_currentValue_1();
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m3223206407(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_23 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t4283661327  L_24 = GUI_get_nextScrollStepTime_m719800559(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m35073816(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = __this->get_currentValue_1();
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m1606169264(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m3369891864(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = __this->get_currentValue_1();
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t4036018645_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_30 = SystemClock_get_now_m175136990(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t4283661327  L_31 = DateTime_AddMilliseconds_m1717403134((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m3820512796(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m4216612037(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t1233388262 * L_33 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_changed_m727947722(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m2660885357(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
extern "C"  float SliderHandler_OnRepaint_m4264550822_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_OnRepaint_m4264550822(_thisAdjusted, method);
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m854773288(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t SliderHandler_CurrentEventType_m3978233689_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEventType_m3978233689(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t4282066565  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_x_1();
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t4196595728 * L_4 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t4282066565  L_5 = Event_get_mousePosition_m3610425949(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = (&V_3)->get_y_2();
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = __this->get_horiz_7();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t4241904616  L_10 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m1606169264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentScrollTroughSide_m1606169264(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		return (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
extern "C"  bool SliderHandler_IsEmptySlider_m2523580504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_IsEmptySlider_m2523580504(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId;
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SupportsPageMovements_m4216612037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_size_2();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m944581596(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool SliderHandler_SupportsPageMovements_m4216612037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SupportsPageMovements_m4216612037(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C"  float SliderHandler_PageMovementValue_m2660885357 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_currentValue_1();
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m2543741343(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = __this->get_size_2();
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = __this->get_size_2();
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m4218954710(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  float SliderHandler_PageMovementValue_m2660885357_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageMovementValue_m2660885357(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m370881244((&V_0), /*hidden argument*/NULL);
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t4241904616  L_5 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m399510395((&V_2), /*hidden argument*/NULL);
		Rect_t4241904616  L_7 = __this->get_position_0();
		V_3 = L_7;
		float L_8 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
extern "C"  float SliderHandler_PageUpMovementBound_m2543741343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_PageUpMovementBound_m2543741343(_thisAdjusted, method);
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		Event_t4196595728 * L_0 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Event_t4196595728 * SliderHandler_CurrentEvent_m721596197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_CurrentEvent_m721596197(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_2 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_start_3();
		float L_6 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_8 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_start_3();
		float L_12 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m493574741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValueForCurrentMousePosition_m493574741(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_Clamp_m4218954710_MetadataUsageId;
extern "C"  float SliderHandler_Clamp_m4218954710 (SliderHandler_t783692703 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_Clamp_m4218954710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		float L_1 = SliderHandler_MinValue_m2516678631(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m44679317(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  float SliderHandler_Clamp_m4218954710_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_Clamp_m4218954710(_thisAdjusted, ___value0, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Rect_t4241904616  L_0 = SliderHandler_ThumbRect_m1881961532(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t4241904616 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m982385354(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m577970569(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m3771513595((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t4241904616 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m982386315(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m2154960823((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m67436392(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m3398820332((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t4241904616  L_15 = V_0;
		return L_15;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbSelectionRect_m3697323546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSelectionRect_m3697323546(_thisAdjusted, method);
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933 (SliderHandler_t783692703 * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderState_t1233388262 * V_0 = NULL;
	{
		SliderState_t1233388262 * L_0 = SliderHandler_SliderState_m1456191352(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t1233388262 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m303335016(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_dragStartPos_0(L_2);
		SliderState_t1233388262 * L_3 = V_0;
		float L_4 = ___dragStartValue0;
		NullCheck(L_3);
		L_3->set_dragStartValue_1(L_4);
		SliderState_t1233388262 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isDragging_2((bool)1);
		return;
	}
}
extern "C"  void SliderHandler_StartDraggingWithValue_m3874943933_AdjustorThunk (Il2CppObject * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	SliderHandler_StartDraggingWithValue_m3874943933(_thisAdjusted, ___dragStartValue0, method);
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t1233388262_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t1028319349_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderState_t1233388262_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SliderState_m1456191352_MetadataUsageId;
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SliderState_m1456191352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SliderState_t1233388262_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t1028319349_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = GUIUtility_GetStateObject_m2379308309(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t1233388262 *)CastclassClass(L_2, SliderState_t1233388262_il2cpp_TypeInfo_var));
	}
}
extern "C"  SliderState_t1233388262 * SliderHandler_SliderState_m1456191352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_SliderState_m1456191352(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t4241904616  L_1 = SliderHandler_HorizontalThumbRect_m2689559608(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t4241904616  L_2 = SliderHandler_VerticalThumbRect_m1406344678(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_ThumbRect_m1881961532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbRect_m1881961532(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t4241904616  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_5 = __this->get_slider_5();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m4104523390(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_start_3();
		float L_10 = V_0;
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		Rect_t4241904616  L_16 = __this->get_position_0();
		V_3 = L_16;
		float L_17 = Rect_get_width_m2824209432((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_18 = __this->get_slider_5();
		NullCheck(L_18);
		RectOffset_t3056157787 * L_19 = GUIStyle_get_padding_m3072941276(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m1186440923(L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_size_2();
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t4241904616  L_25 = __this->get_position_0();
		V_4 = L_25;
		float L_26 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_27 = __this->get_slider_5();
		NullCheck(L_27);
		RectOffset_t3056157787 * L_28 = GUIStyle_get_padding_m3072941276(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m4104523390(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_31 = __this->get_size_2();
		float L_32 = __this->get_start_3();
		float L_33 = V_0;
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_36 = __this->get_slider_5();
		NullCheck(L_36);
		RectOffset_t3056157787 * L_37 = GUIStyle_get_padding_m3072941276(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m140097312(L_37, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_width_m2824209432((&V_6), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_41 = __this->get_slider_5();
		NullCheck(L_41);
		RectOffset_t3056157787 * L_42 = GUIStyle_get_padding_m3072941276(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m1186440923(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_size_2();
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m3291325233(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_VerticalThumbRect_m1406344678_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_VerticalThumbRect_m1406344678(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t4241904616  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t4241904616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t4241904616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t4241904616  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m41869331(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_start_3();
		float L_5 = V_0;
		Rect_t4241904616  L_6 = __this->get_position_0();
		V_1 = L_6;
		float L_7 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_8 = __this->get_slider_5();
		NullCheck(L_8);
		RectOffset_t3056157787 * L_9 = GUIStyle_get_padding_m3072941276(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m4104523390(L_9, /*hidden argument*/NULL);
		Rect_t4241904616  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m982386315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3056157787 * L_14 = GUIStyle_get_padding_m3072941276(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m140097312(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_size_2();
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_19 = __this->get_position_0();
		V_3 = L_19;
		float L_20 = Rect_get_height_m2154960823((&V_3), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_21 = __this->get_slider_5();
		NullCheck(L_21);
		RectOffset_t3056157787 * L_22 = GUIStyle_get_padding_m3072941276(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m3650431789(L_22, /*hidden argument*/NULL);
		Rect_t4241904616  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m3291325233(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m3006511340(__this, /*hidden argument*/NULL);
		float L_26 = __this->get_size_2();
		float L_27 = __this->get_start_3();
		float L_28 = V_0;
		Rect_t4241904616  L_29 = __this->get_position_0();
		V_4 = L_29;
		float L_30 = Rect_get_x_m982385354((&V_4), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_31 = __this->get_slider_5();
		NullCheck(L_31);
		RectOffset_t3056157787 * L_32 = GUIStyle_get_padding_m3072941276(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m4104523390(L_32, /*hidden argument*/NULL);
		Rect_t4241904616  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m982386315((&V_5), /*hidden argument*/NULL);
		float L_36 = __this->get_size_2();
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		Rect_t4241904616  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_height_m2154960823((&V_6), /*hidden argument*/NULL);
		Rect_t4241904616  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m3291325233(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
extern "C"  Rect_t4241904616  SliderHandler_HorizontalThumbRect_m2689559608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_HorizontalThumbRect_m2689559608(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentValue_1();
		float L_1 = SliderHandler_Clamp_m4218954710(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  float SliderHandler_ClampedCurrentValue_m3006511340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ClampedCurrentValue_m3006511340(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C"  float SliderHandler_MousePosition_m303335016 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Vector2_t4282066565  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t4282066565  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t4241904616  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t4196595728 * L_1 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t4282066565  L_2 = Event_get_mousePosition_m3610425949(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		Rect_t4241904616  L_4 = __this->get_position_0();
		V_1 = L_4;
		float L_5 = Rect_get_x_m982385354((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t4196595728 * L_6 = SliderHandler_CurrentEvent_m721596197(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t4282066565  L_7 = Event_get_mousePosition_m3610425949(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_3 = L_9;
		float L_10 = Rect_get_y_m982386315((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
extern "C"  float SliderHandler_MousePosition_m303335016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MousePosition_m303335016(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C"  float SliderHandler_ValuesPerPixel_m41869331 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	Rect_t4241904616  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t4241904616  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t4241904616  L_1 = __this->get_position_0();
		V_0 = L_1;
		float L_2 = Rect_get_width_m2824209432((&V_0), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_3 = __this->get_slider_5();
		NullCheck(L_3);
		RectOffset_t3056157787 * L_4 = GUIStyle_get_padding_m3072941276(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m1186440923(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_end_4();
		float L_8 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t4241904616  L_9 = __this->get_position_0();
		V_1 = L_9;
		float L_10 = Rect_get_height_m2154960823((&V_1), /*hidden argument*/NULL);
		GUIStyle_t2990928826 * L_11 = __this->get_slider_5();
		NullCheck(L_11);
		RectOffset_t3056157787 * L_12 = GUIStyle_get_padding_m3072941276(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3650431789(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m3034411697(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_end_4();
		float L_16 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
extern "C"  float SliderHandler_ValuesPerPixel_m41869331_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ValuesPerPixel_m41869331(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C"  float SliderHandler_ThumbSize_m3034411697 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t2990928826 * L_1 = __this->get_thumb_6();
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m3249098964(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t2990928826 * L_3 = __this->get_thumb_6();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m3249098964(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t2990928826 * L_5 = __this->get_thumb_6();
		NullCheck(L_5);
		RectOffset_t3056157787 * L_6 = GUIStyle_get_padding_m3072941276(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m1186440923(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t2990928826 * L_8 = __this->get_thumb_6();
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m2441634427(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t2990928826 * L_10 = __this->get_thumb_6();
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m2441634427(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t2990928826 * L_12 = __this->get_thumb_6();
		NullCheck(L_12);
		RectOffset_t3056157787 * L_13 = GUIStyle_get_padding_m3072941276(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m3650431789(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
extern "C"  float SliderHandler_ThumbSize_m3034411697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_ThumbSize_m3034411697(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MaxValue_m44679317_MetadataUsageId;
extern "C"  float SliderHandler_MaxValue_m44679317 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MaxValue_m44679317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_size_2();
		return ((float)((float)L_2-(float)L_3));
	}
}
extern "C"  float SliderHandler_MaxValue_m44679317_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MaxValue_m44679317(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MinValue_m2516678631_MetadataUsageId;
extern "C"  float SliderHandler_MinValue_m2516678631 (SliderHandler_t783692703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MinValue_m2516678631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float SliderHandler_MinValue_m2516678631_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t783692703 * _thisAdjusted = reinterpret_cast<SliderHandler_t783692703 *>(__this + 1);
	return SliderHandler_MinValue_m2516678631(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_pinvoke_back(const SliderHandler_t783692703_marshaled_pinvoke& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_pinvoke_cleanup(SliderHandler_t783692703_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com(const SliderHandler_t783692703& unmarshaled, SliderHandler_t783692703_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t783692703_marshal_com_back(const SliderHandler_t783692703_marshaled_com& marshaled, SliderHandler_t783692703& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t783692703_marshal_com_cleanup(SliderHandler_t783692703_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m3732503849 (SliderState_t1233388262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.Social::get_Active()
extern "C"  Il2CppObject * Social_get_Active_m590102927 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ActivePlatform_get_Instance_m486239621(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Social::set_Active(UnityEngine.SocialPlatforms.ISocialPlatform)
extern "C"  void Social_set_Active_m3574014644 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		ActivePlatform_set_Instance_m2785258238(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.Social::get_localUser()
extern Il2CppClass* ISocialPlatform_t277815499_il2cpp_TypeInfo_var;
extern const uint32_t Social_get_localUser_m2966161563_MetadataUsageId;
extern "C"  Il2CppObject * Social_get_localUser_m2966161563 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Social_get_localUser_m2966161563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = Social_get_Active_m590102927(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.ISocialPlatform::get_localUser() */, ISocialPlatform_t277815499_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
extern Il2CppClass* ActivePlatform_t2714626623_il2cpp_TypeInfo_var;
extern const uint32_t ActivePlatform_get_Instance_m486239621_MetadataUsageId;
extern "C"  Il2CppObject * ActivePlatform_get_Instance_m486239621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_get_Instance_m486239621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ((ActivePlatform_t2714626623_StaticFields*)ActivePlatform_t2714626623_il2cpp_TypeInfo_var->static_fields)->get__active_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ActivePlatform_SelectSocialPlatform_m922879939(NULL /*static, unused*/, /*hidden argument*/NULL);
		((ActivePlatform_t2714626623_StaticFields*)ActivePlatform_t2714626623_il2cpp_TypeInfo_var->static_fields)->set__active_0(L_1);
	}

IL_0014:
	{
		Il2CppObject * L_2 = ((ActivePlatform_t2714626623_StaticFields*)ActivePlatform_t2714626623_il2cpp_TypeInfo_var->static_fields)->get__active_0();
		return L_2;
	}
}
// System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
extern Il2CppClass* ActivePlatform_t2714626623_il2cpp_TypeInfo_var;
extern const uint32_t ActivePlatform_set_Instance_m2785258238_MetadataUsageId;
extern "C"  void ActivePlatform_set_Instance_m2785258238 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_set_Instance_m2785258238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		((ActivePlatform_t2714626623_StaticFields*)ActivePlatform_t2714626623_il2cpp_TypeInfo_var->static_fields)->set__active_0(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t ActivePlatform_SelectSocialPlatform_m922879939_MetadataUsageId;
extern "C"  Il2CppObject * ActivePlatform_SelectSocialPlatform_m922879939 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActivePlatform_SelectSocialPlatform_m922879939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameCenterPlatform_t3570684786 * L_0 = (GameCenterPlatform_t3570684786 *)il2cpp_codegen_object_new(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform__ctor_m573039859(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m573039859 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3189060351_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1721872494_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m102270234_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m102270234 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m102270234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_friends_10(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_users_11(((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t3189060351 * L_0 = (List_1_t3189060351 *)il2cpp_codegen_object_new(List_1_t3189060351_il2cpp_TypeInfo_var);
		List_1__ctor_m1721872494(L_0, /*hidden argument*/List_1__ctor_m1721872494_MethodInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_14(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2468032119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_FriendsCallback_1(L_0);
		GameCenterPlatform_Internal_LoadFriends_m1921936862(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___user0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m305325355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m582381960(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m582381960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m582381960_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m582381960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m582381960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m2780967960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m2780967960_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m2780967960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m2780967960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m1252299660 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m1252299660_ftn) ();
	static GameCenterPlatform_Internal_UserName_m1252299660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m1252299660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m385481212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m385481212_ftn) ();
	static GameCenterPlatform_Internal_UserID_m385481212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m385481212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m4169738944 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m4169738944_ftn) ();
	static GameCenterPlatform_Internal_Underage_m4169738944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m4169738944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3884108195 * GameCenterPlatform_Internal_UserImage_m3175776130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3884108195 * (*GameCenterPlatform_Internal_UserImage_m3175776130_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m3175776130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m3175776130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m1921936862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m1921936862_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m817891229 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m817891229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m2511520970 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m2511520970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id0, ___progress1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m408601947 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m408601947_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m408601947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m408601947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score0, ___category1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m523283944 (Il2CppObject * __this /* static, unused */, String_t* ___category0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m523283944_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m523283944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m523283944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m4218820079 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___userIds0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn) (StringU5BU5D_t4054002952*);
	static GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m4218820079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m165059209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m165059209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m878609996 (Il2CppObject * __this /* static, unused */, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m878609996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t872614854 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_12(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m165059209(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2516168699_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m3108376897(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3791866548 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3791866548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m1768304742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m3158758843 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m3158758843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m3174496109 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t2242891083  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m3174496109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_1 = ___number1;
		AchievementDescription_t2116066607 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m3125480712((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t2116066607 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2873143867;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m3728674360 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m3728674360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2873143867, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		AchievementDescription_t2116066607 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3884108195 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1092175896(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral48254646;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m1497473051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t759444790* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral48254646, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t229750097 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		AchievementDescriptionU5BU5D_t759444790* L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_3);
		Action_1_Invoke_m2766540343(L_3, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)L_4, /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m2896042779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B3_0 = NULL;
	Action_1_t872614854 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t872614854 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m3594021162(G_B4_1, (bool)G_B4_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_1_t872614854 *)NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1761222218 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1761222218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m3378244042 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m3378244042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m4228294663 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m4228294663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m3845044787_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t872614854 * G_B5_0 = NULL;
	Action_1_t872614854 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t872614854 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2378268441* L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		NullCheck(L_1);
		LocalUser_SetFriends_m3475409220(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		int32_t L_5 = ___result0;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3594021162(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3379715660;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m2031411110 (Il2CppObject * __this /* static, unused */, GcAchievementDataU5BU5D_t1726768202* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m2031411110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t912418020* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1726768202* L_1 = ___result0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3379715660, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1726768202* L_2 = ___result0;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t912418020* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_5 = ___result0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t344600729 * L_7 = GcAchievementData_ToAchievement_m3239514930(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t344600729 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1726768202* L_10 = ___result0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t2349069933 * L_11 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		AchievementU5BU5D_t912418020* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m222677977(L_11, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)L_12, /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m165794409 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m165794409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2797312324 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2797312324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053 (Il2CppObject * __this /* static, unused */, GcScoreDataU5BU5D_t1670395707* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m2588839053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___result0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___result0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___result0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t645920862 * L_10 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m50340758(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t1307362368_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m1634439374 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m1634439374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t1307362368 * L_1 = (LocalUser_t1307362368 *)il2cpp_codegen_object_new(LocalUser_t1307362368_il2cpp_TypeInfo_var);
		LocalUser__ctor_m1052633066(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_13(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_3 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m2095754825(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral48, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2583301917(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2583301917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2583301917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		LocalUser_t1307362368 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m2780967960(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m653377406(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m1252299660(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m914181770(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_4 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m385481212(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m1515238170(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_7 = GameCenterPlatform_Internal_Underage_m4169738944(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m2968368872(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t1307362368 * L_8 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		Texture2D_t3884108195 * L_9 = GameCenterPlatform_Internal_UserImage_m3175776130(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m1928130753(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2766540343_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m232801667 (GameCenterPlatform_t3570684786 * __this, Action_1_t229750097 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m232801667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t229750097 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m2766540343(L_1, (IAchievementDescriptionU5BU5D_t4128901257*)(IAchievementDescriptionU5BU5D_t4128901257*)((AchievementDescriptionU5BU5D_t759444790*)SZArrayNew(AchievementDescriptionU5BU5D_t759444790_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2766540343_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t229750097 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementDescriptionLoaderCallback_2(L_2);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m1394384079(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m4110499833 (GameCenterPlatform_t3570684786 * __this, String_t* ___id0, double ___progress1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m4110499833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ProgressCallback_4(L_2);
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		GameCenterPlatform_Internal_ReportProgress_m2511520970(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m222677977_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m2745782249 (GameCenterPlatform_t3570684786 * __this, Action_1_t2349069933 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m2745782249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t2349069933 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m222677977(L_1, (IAchievementU5BU5D_t1953253797*)(IAchievementU5BU5D_t1953253797*)((AchievementU5BU5D_t912418020*)SZArrayNew(AchievementU5BU5D_t912418020_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m222677977_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t2349069933 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementLoaderCallback_3(L_2);
		GameCenterPlatform_Internal_LoadAchievements_m817891229(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m1009544586 (GameCenterPlatform_t3570684786 * __this, int64_t ___score0, String_t* ___board1, Action_1_t872614854 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m1009544586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreCallback_5(L_2);
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		GameCenterPlatform_Internal_ReportScore_m408601947(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m50340758_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m3562614827 (GameCenterPlatform_t3570684786 * __this, String_t* ___category0, Action_1_t645920862 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m3562614827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t645920862 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m50340758(L_1, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m50340758_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t645920862 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreLoaderCallback_6(L_2);
		String_t* L_3 = ___category0;
		GameCenterPlatform_Internal_LoadScores_m523283944(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t1820874799_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t3799088250_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1416233310_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2883394111 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, Action_1_t872614854 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2883394111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	GcLeaderboard_t1820874799 * V_1 = NULL;
	StringU5BU5D_t4054002952* V_2 = NULL;
	Range_t1533311935  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Range_t1533311935  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t872614854 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t872614854 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_LeaderboardCallback_7(L_2);
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t1185876199 *)CastclassClass(L_3, Leaderboard_t1185876199_il2cpp_TypeInfo_var));
		Leaderboard_t1185876199 * L_4 = V_0;
		GcLeaderboard_t1820874799 * L_5 = (GcLeaderboard_t1820874799 *)il2cpp_codegen_object_new(GcLeaderboard_t1820874799_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m4042810199(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t3189060351 * L_6 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		GcLeaderboard_t1820874799 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1416233310(L_6, L_7, /*hidden argument*/List_1_Add_m1416233310_MethodInfo_var);
		Leaderboard_t1185876199 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t4054002952* L_9 = Leaderboard_GetUserFilter_m3119905721(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		StringU5BU5D_t4054002952* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))
		{
			goto IL_0043;
		}
	}
	{
		V_2 = (StringU5BU5D_t4054002952*)NULL;
	}

IL_0043:
	{
		GcLeaderboard_t1820874799 * L_11 = V_1;
		Il2CppObject * L_12 = ___board0;
		NullCheck(L_12);
		String_t* L_13 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_12);
		Il2CppObject * L_14 = ___board0;
		NullCheck(L_14);
		Range_t1533311935  L_15 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(3 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_14);
		V_3 = L_15;
		int32_t L_16 = (&V_3)->get_from_0();
		Il2CppObject * L_17 = ___board0;
		NullCheck(L_17);
		Range_t1533311935  L_18 = InterfaceFuncInvoker0< Range_t1533311935  >::Invoke(3 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_17);
		V_4 = L_18;
		int32_t L_19 = (&V_4)->get_count_1();
		StringU5BU5D_t4054002952* L_20 = V_2;
		Il2CppObject * L_21 = ___board0;
		NullCheck(L_21);
		int32_t L_22 = InterfaceFuncInvoker0< int32_t >::Invoke(2 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_21);
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		int32_t L_24 = InterfaceFuncInvoker0< int32_t >::Invoke(4 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t3799088250_il2cpp_TypeInfo_var, L_23);
		NullCheck(L_11);
		GcLeaderboard_Internal_LoadScores_m1086410269(L_11, L_13, L_16, L_19, L_20, L_22, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m2165153529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3208733121_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m173797613_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3622080807_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4196185109_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2084830155 (GameCenterPlatform_t3570684786 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2084830155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t1820874799 * V_0 = NULL;
	Enumerator_t3208733121  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		List_1_t3189060351 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		NullCheck(L_1);
		Enumerator_t3208733121  L_2 = List_1_GetEnumerator_m173797613(L_1, /*hidden argument*/List_1_GetEnumerator_m173797613_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t1820874799 * L_3 = Enumerator_get_Current_m3622080807((&V_1), /*hidden argument*/Enumerator_get_Current_m3622080807_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t1820874799 * L_4 = V_0;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m100384368(L_4, ((Leaderboard_t1185876199 *)CastclassClass(L_5, Leaderboard_t1185876199_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t1820874799 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m294711596(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m4196185109((&V_1), /*hidden argument*/Enumerator_MoveNext_m4196185109_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t3208733121  L_10 = V_1;
		Enumerator_t3208733121  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3208733121_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2654168339_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral708567292;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4096949980 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4096949980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m1634439374(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2654168339_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral708567292, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m2437339590 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m2437339590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m1934331464(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m302984165 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m302984165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3057704739(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m4212910653 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m4212910653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2546851889(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m847940592_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m847940592 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t657441114  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m847940592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m3757655355((&___data0), (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3066589434 (Il2CppObject * __this /* static, unused */, Texture2D_t3884108195 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3066589434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Texture2D_t3884108195 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m3650098397(NULL /*static, unused*/, (((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2446471631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t3814920354 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		UserProfileU5BU5D_t2378268441* L_2 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_users_11();
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)L_2, /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2276731850_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m981272890 (GameCenterPlatform_t3570684786 * __this, StringU5BU5D_t4054002952* ___userIds0, Action_1_t3814920354 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m981272890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4096949980(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3814920354 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m2276731850(L_1, (IUserProfileU5BU5D_t3419104218*)(IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m2276731850_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3814920354 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->set_s_UsersCallback_8(L_2);
		StringU5BU5D_t4054002952* L_3 = ___userIds0;
		GameCenterPlatform_Internal_LoadUsers_m4218820079(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral183309550;
extern Il2CppCodeGenString* _stringLiteral2135574971;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m3650098397 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, Texture2D_t3884108195 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m3650098397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral183309550, /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_3 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t2378268441** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_7)), L_8);
		int32_t L_9 = L_8;
		UserProfile_t2280656072 * L_10 = ((*((UserProfileU5BU5D_t2378268441**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3884108195 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m1928130753(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral2135574971, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2546851889 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2546851889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2378268441**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t1185876199_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m2295049883 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m2295049883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t1185876199 * V_0 = NULL;
	{
		Leaderboard_t1185876199 * L_0 = (Leaderboard_t1185876199 *)il2cpp_codegen_object_new(Leaderboard_t1185876199_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m596857571(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t1185876199 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m1828880347 (GameCenterPlatform_t3570684786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m1828880347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t344600729 * V_0 = NULL;
	{
		Achievement_t344600729 * L_0 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m3345265521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t344600729 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m1285257317 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m1285257317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_0 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var);
		Action_1_t872614854 * L_1 = ((GameCenterPlatform_t3570684786_StaticFields*)GameCenterPlatform_t3570684786_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3594021162(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t344600729_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m3239514930_MetadataUsageId;
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930 (GcAchievementData_t3481375915 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m3239514930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t344600729 * L_6 = (Achievement_t344600729 *)il2cpp_codegen_object_new(Achievement_t344600729_il2cpp_TypeInfo_var);
		Achievement__ctor_m377036415(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  Achievement_t344600729 * GcAchievementData_ToAchievement_m3239514930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t3481375915 * _thisAdjusted = reinterpret_cast<GcAchievementData_t3481375915 *>(__this + 1);
	return GcAchievementData_ToAchievement_m3239514930(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_back(const GcAchievementData_t3481375915_marshaled_pinvoke& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_pinvoke_cleanup(GcAchievementData_t3481375915_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com(const GcAchievementData_t3481375915& unmarshaled, GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t3481375915_marshal_com_back(const GcAchievementData_t3481375915_marshaled_com& marshaled, GcAchievementData_t3481375915& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t3481375915_marshal_com_cleanup(GcAchievementData_t3481375915_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern Il2CppClass* AchievementDescription_t2116066607_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId;
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712 (GcAchievementDescriptionData_t2242891083 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementDescriptionData_ToAchievementDescription_m3125480712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Texture2D_t3884108195 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Texture2D_t3884108195 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	Texture2D_t3884108195 * G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	String_t* G_B3_5 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		String_t* L_1 = __this->get_m_Title_1();
		Texture2D_t3884108195 * L_2 = __this->get_m_Image_2();
		String_t* L_3 = __this->get_m_AchievedDescription_3();
		String_t* L_4 = __this->get_m_UnachievedDescription_4();
		int32_t L_5 = __this->get_m_Hidden_5();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = __this->get_m_Points_6();
		AchievementDescription_t2116066607 * L_7 = (AchievementDescription_t2116066607 *)il2cpp_codegen_object_new(AchievementDescription_t2116066607_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m3032164909(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, (bool)G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  AchievementDescription_t2116066607 * GcAchievementDescriptionData_ToAchievementDescription_m3125480712_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementDescriptionData_t2242891083 * _thisAdjusted = reinterpret_cast<GcAchievementDescriptionData_t2242891083 *>(__this + 1);
	return GcAchievementDescriptionData_ToAchievementDescription_m3125480712(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_back(const GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_pinvoke_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com(const GcAchievementDescriptionData_t2242891083& unmarshaled, GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_back(const GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled, GcAchievementDescriptionData_t2242891083& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t2242891083_marshal_com_cleanup(GcAchievementDescriptionData_t2242891083_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  void GcLeaderboard__ctor_m4042810199 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_t1185876199 * L_0 = ___board0;
		__this->set_m_GenericLeaderboard_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C"  void GcLeaderboard_Finalize_m4015840938 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m301614325(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  bool GcLeaderboard_Contains_m100384368 (GcLeaderboard_t1820874799 * __this, Leaderboard_t1185876199 * ___board0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		Leaderboard_t1185876199 * L_1 = ___board0;
		return (bool)((((Il2CppObject*)(Leaderboard_t1185876199 *)L_0) == ((Il2CppObject*)(Leaderboard_t1185876199 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern const uint32_t GcLeaderboard_SetScores_m1734279820_MetadataUsageId;
extern "C"  void GcLeaderboard_SetScores_m1734279820 (GcLeaderboard_t1820874799 * __this, GcScoreDataU5BU5D_t1670395707* ___scoreDatas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcLeaderboard_SetScores_m1734279820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t2926278037* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1670395707* L_1 = ___scoreDatas0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t2926278037* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_4 = ___scoreDatas0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t3396031228 * L_6 = GcScoreData_ToScore_m2728389301(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t3396031228 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1670395707* L_9 = ___scoreDatas0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t1185876199 * L_10 = __this->get_m_GenericLeaderboard_1();
		ScoreU5BU5D_t2926278037* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m1463319879(L_10, (IScoreU5BU5D_t250104726*)(IScoreU5BU5D_t250104726*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C"  void GcLeaderboard_SetLocalScore_m3970132532 (GcLeaderboard_t1820874799 * __this, GcScoreData_t2181296590  ___scoreData0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		Score_t3396031228 * L_2 = GcScoreData_ToScore_m2728389301((&___scoreData0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m700491556(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C"  void GcLeaderboard_SetMaxRange_m3160374921 (GcLeaderboard_t1820874799 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		uint32_t L_2 = ___maxRange0;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m3779908734(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C"  void GcLeaderboard_SetTitle_m3781988000 (GcLeaderboard_t1820874799 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		Leaderboard_t1185876199 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t1185876199 * L_1 = __this->get_m_GenericLeaderboard_1();
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		Leaderboard_SetTitle_m771163339(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32)
extern "C"  void GcLeaderboard_Internal_LoadScores_m1086410269 (GcLeaderboard_t1820874799 * __this, String_t* ___category0, int32_t ___from1, int32_t ___count2, StringU5BU5D_t4054002952* ___userIDs3, int32_t ___playerScope4, int32_t ___timeScope5, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m1086410269_ftn) (GcLeaderboard_t1820874799 *, String_t*, int32_t, int32_t, StringU5BU5D_t4054002952*, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m1086410269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m1086410269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category0, ___from1, ___count2, ___userIDs3, ___playerScope4, ___timeScope5);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C"  bool GcLeaderboard_Loading_m294711596 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m294711596_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Loading_m294711596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m294711596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C"  void GcLeaderboard_Dispose_m301614325 (GcLeaderboard_t1820874799 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m301614325_ftn) (GcLeaderboard_t1820874799 *);
	static GcLeaderboard_Dispose_m301614325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m301614325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_back(const GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_pinvoke_cleanup(GcLeaderboard_t1820874799_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com(const GcLeaderboard_t1820874799& unmarshaled, GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t1820874799_marshal_com_back(const GcLeaderboard_t1820874799_marshaled_com& marshaled, GcLeaderboard_t1820874799& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t1820874799_marshal_com_cleanup(GcLeaderboard_t1820874799_marshaled_com& marshaled)
{
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern const uint32_t GcScoreData_ToScore_m2728389301_MetadataUsageId;
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301 (GcScoreData_t2181296590 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcScoreData_ToScore_m2728389301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t4283661327  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_Category_0();
		int32_t L_1 = __this->get_m_ValueHigh_2();
		uint32_t L_2 = __this->get_m_ValueLow_1();
		String_t* L_3 = __this->get_m_PlayerID_5();
		DateTime__ctor_m1594789867((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_Date_3();
		DateTime_t4283661327  L_5 = DateTime_AddSeconds_m2515640243((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = __this->get_m_FormattedValue_4();
		int32_t L_7 = __this->get_m_Rank_6();
		Score_t3396031228 * L_8 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m3768037481(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((uint64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Score_t3396031228 * GcScoreData_ToScore_m2728389301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcScoreData_t2181296590 * _thisAdjusted = reinterpret_cast<GcScoreData_t2181296590 *>(__this + 1);
	return GcScoreData_ToScore_m2728389301(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_back(const GcScoreData_t2181296590_marshaled_pinvoke& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0));
	uint32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_pinvoke_cleanup(GcScoreData_t2181296590_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com(const GcScoreData_t2181296590& unmarshaled, GcScoreData_t2181296590_marshaled_com& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t2181296590_marshal_com_back(const GcScoreData_t2181296590_marshaled_com& marshaled, GcScoreData_t2181296590& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Category_0));
	uint32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_bstring_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_bstring_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t2181296590_marshal_com_cleanup(GcScoreData_t2181296590_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern Il2CppClass* UserProfile_t2280656072_il2cpp_TypeInfo_var;
extern const uint32_t GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId;
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721 (GcUserProfileData_t657441114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_ToUserProfile_m1509702721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = __this->get_userName_0();
		String_t* L_1 = __this->get_userID_1();
		int32_t L_2 = __this->get_isFriend_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t3884108195 * L_3 = __this->get_image_3();
		UserProfile_t2280656072 * L_4 = (UserProfile_t2280656072 *)il2cpp_codegen_object_new(UserProfile_t2280656072_il2cpp_TypeInfo_var);
		UserProfile__ctor_m2682768015(L_4, G_B3_2, G_B3_1, (bool)G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  UserProfile_t2280656072 * GcUserProfileData_ToUserProfile_m1509702721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	return GcUserProfileData_ToUserProfile_m1509702721(_thisAdjusted, method);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596528006;
extern const uint32_t GcUserProfileData_AddToArray_m3757655355_MetadataUsageId;
extern "C"  void GcUserProfileData_AddToArray_m3757655355 (GcUserProfileData_t657441114 * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_AddToArray_m3757655355_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2378268441** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_0)));
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2378268441**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t2378268441** L_3 = ___array0;
		int32_t L_4 = ___number1;
		UserProfile_t2280656072 * L_5 = GcUserProfileData_ToUserProfile_m1509702721(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t2378268441**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2378268441**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t2378268441**)L_3)), L_5);
		((*((UserProfileU5BU5D_t2378268441**)L_3)))->SetAt(static_cast<il2cpp_array_size_t>(L_4), (UserProfile_t2280656072 *)L_5);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1596528006, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
extern "C"  void GcUserProfileData_AddToArray_m3757655355_AdjustorThunk (Il2CppObject * __this, UserProfileU5BU5D_t2378268441** ___array0, int32_t ___number1, const MethodInfo* method)
{
	GcUserProfileData_t657441114 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t657441114 *>(__this + 1);
	GcUserProfileData_AddToArray_m3757655355(_thisAdjusted, ___array0, ___number1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_back(const GcUserProfileData_t657441114_marshaled_pinvoke& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_pinvoke_cleanup(GcUserProfileData_t657441114_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com(const GcUserProfileData_t657441114& unmarshaled, GcUserProfileData_t657441114_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t657441114_marshal_com_back(const GcUserProfileData_t657441114_marshaled_com& marshaled, GcUserProfileData_t657441114& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t657441114_marshal_com_cleanup(GcUserProfileData_t657441114_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C"  void Achievement__ctor_m377036415 (Achievement_t344600729 * __this, String_t* ___id0, double ___percentCompleted1, bool ___completed2, bool ___hidden3, DateTime_t4283661327  ___lastReportedDate4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percentCompleted1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___completed2;
		__this->set_m_Completed_0(L_2);
		bool L_3 = ___hidden3;
		__this->set_m_Hidden_1(L_3);
		DateTime_t4283661327  L_4 = ___lastReportedDate4;
		__this->set_m_LastReportedDate_2(L_4);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t Achievement__ctor_m2960680429_MetadataUsageId;
extern "C"  void Achievement__ctor_m2960680429 (Achievement_t344600729 * __this, String_t* ___id0, double ___percent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m2960680429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m3123429815(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percent1;
		Achievement_set_percentCompleted_m1005987436(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_Hidden_1((bool)0);
		__this->set_m_Completed_0((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = ((DateTime_t4283661327_StaticFields*)DateTime_t4283661327_il2cpp_TypeInfo_var->static_fields)->get_MinValue_3();
		__this->set_m_LastReportedDate_2(L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral4010126410;
extern const uint32_t Achievement__ctor_m3345265521_MetadataUsageId;
extern "C"  void Achievement__ctor_m3345265521 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m3345265521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m2960680429(__this, _stringLiteral4010126410, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t Achievement_ToString_m2974157186_MetadataUsageId;
extern "C"  String_t* Achievement_ToString_m2974157186 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement_ToString_m2974157186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		String_t* L_1 = Achievement_get_id_m1680539866(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		double L_4 = Achievement_get_percentCompleted_m2492759109(__this, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t1108656482* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral32179);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_8 = L_7;
		bool L_9 = Achievement_get_completed_m3689853355(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral32179);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_13 = L_12;
		bool L_14 = Achievement_get_hidden_m2954555244(__this, /*hidden argument*/NULL);
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral32179);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		DateTime_t4283661327  L_19 = Achievement_get_lastReportedDate_m445111052(__this, /*hidden argument*/NULL);
		DateTime_t4283661327  L_20 = L_19;
		Il2CppObject * L_21 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3016520001(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C"  String_t* Achievement_get_id_m1680539866 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C"  void Achievement_set_id_m3123429815 (Achievement_t344600729 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C"  double Achievement_get_percentCompleted_m2492759109 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CpercentCompletedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C"  void Achievement_set_percentCompleted_m1005987436 (Achievement_t344600729 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpercentCompletedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C"  bool Achievement_get_completed_m3689853355 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Completed_0();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C"  bool Achievement_get_hidden_m2954555244 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_1();
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C"  DateTime_t4283661327  Achievement_get_lastReportedDate_m445111052 (Achievement_t344600729 * __this, const MethodInfo* method)
{
	{
		DateTime_t4283661327  L_0 = __this->get_m_LastReportedDate_2();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C"  void AchievementDescription__ctor_m3032164909 (AchievementDescription_t2116066607 * __this, String_t* ___id0, String_t* ___title1, Texture2D_t3884108195 * ___image2, String_t* ___achievedDescription3, String_t* ___unachievedDescription4, bool ___hidden5, int32_t ___points6, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		AchievementDescription_set_id_m2215766207(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___title1;
		__this->set_m_Title_0(L_1);
		Texture2D_t3884108195 * L_2 = ___image2;
		__this->set_m_Image_1(L_2);
		String_t* L_3 = ___achievedDescription3;
		__this->set_m_AchievedDescription_2(L_3);
		String_t* L_4 = ___unachievedDescription4;
		__this->set_m_UnachievedDescription_3(L_4);
		bool L_5 = ___hidden5;
		__this->set_m_Hidden_4(L_5);
		int32_t L_6 = ___points6;
		__this->set_m_Points_5(L_6);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t AchievementDescription_ToString_m1633092820_MetadataUsageId;
extern "C"  String_t* AchievementDescription_ToString_m1633092820 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AchievementDescription_ToString_m1633092820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		String_t* L_1 = AchievementDescription_get_id_m3162941612(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = AchievementDescription_get_title_m1294089737(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		String_t* L_7 = AchievementDescription_get_achievedDescription_m4248956218(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral32179);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		String_t* L_10 = AchievementDescription_get_unachievedDescription_m3429874753(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral32179);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		int32_t L_13 = AchievementDescription_get_points_m4158769271(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral32179);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		bool L_18 = AchievementDescription_get_hidden_m734162136(__this, /*hidden argument*/NULL);
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C"  void AchievementDescription_SetImage_m1092175896 (AchievementDescription_t2116066607 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_1(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C"  String_t* AchievementDescription_get_id_m3162941612 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C"  void AchievementDescription_set_id_m2215766207 (AchievementDescription_t2116066607 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C"  String_t* AchievementDescription_get_title_m1294089737 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Title_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C"  String_t* AchievementDescription_get_achievedDescription_m4248956218 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_AchievedDescription_2();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C"  String_t* AchievementDescription_get_unachievedDescription_m3429874753 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UnachievedDescription_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C"  bool AchievementDescription_get_hidden_m734162136 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_4();
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C"  int32_t AchievementDescription_get_points_m4158769271 (AchievementDescription_t2116066607 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Points_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern Il2CppClass* Score_t3396031228_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3624438231;
extern const uint32_t Leaderboard__ctor_m596857571_MetadataUsageId;
extern "C"  void Leaderboard__ctor_m596857571 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard__ctor_m596857571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Leaderboard_set_id_m2022535593(__this, _stringLiteral3624438231, /*hidden argument*/NULL);
		Range_t1533311935  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Range__ctor_m872630735(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		Leaderboard_set_range_m2830170470(__this, L_0, /*hidden argument*/NULL);
		Leaderboard_set_userScope_m3914830286(__this, 0, /*hidden argument*/NULL);
		Leaderboard_set_timeScope_m3669793618(__this, 2, /*hidden argument*/NULL);
		__this->set_m_Loading_0((bool)0);
		Score_t3396031228 * L_1 = (Score_t3396031228 *)il2cpp_codegen_object_new(Score_t3396031228_il2cpp_TypeInfo_var);
		Score__ctor_m113497156(L_1, _stringLiteral3624438231, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->set_m_LocalUserScore_1(L_1);
		__this->set_m_MaxRange_2(0);
		__this->set_m_Scores_3((IScoreU5BU5D_t250104726*)((ScoreU5BU5D_t2926278037*)SZArrayNew(ScoreU5BU5D_t2926278037_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Title_4(_stringLiteral3624438231);
		__this->set_m_UserIDs_5(((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* UserScope_t1608660171_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeScope_t1305796361_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69499590;
extern Il2CppCodeGenString* _stringLiteral1411513442;
extern Il2CppCodeGenString* _stringLiteral1472989374;
extern Il2CppCodeGenString* _stringLiteral3534372753;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral2271991173;
extern Il2CppCodeGenString* _stringLiteral778717479;
extern Il2CppCodeGenString* _stringLiteral4163059537;
extern Il2CppCodeGenString* _stringLiteral978617427;
extern Il2CppCodeGenString* _stringLiteral1548524389;
extern const uint32_t Leaderboard_ToString_m114482384_MetadataUsageId;
extern "C"  String_t* Leaderboard_ToString_m114482384 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_ToString_m114482384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Range_t1533311935  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Range_t1533311935  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral69499590);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral69499590);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		String_t* L_2 = Leaderboard_get_id_m2379239336(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t1108656482* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1411513442);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1411513442);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		String_t* L_5 = __this->get_m_Title_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t1108656482* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1472989374);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1472989374);
		ObjectU5BU5D_t1108656482* L_7 = L_6;
		bool L_8 = __this->get_m_Loading_0();
		bool L_9 = L_8;
		Il2CppObject * L_10 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t1108656482* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral3534372753);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3534372753);
		ObjectU5BU5D_t1108656482* L_12 = L_11;
		Range_t1533311935  L_13 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&V_0)->get_from_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral44);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral44);
		ObjectU5BU5D_t1108656482* L_18 = L_17;
		Range_t1533311935  L_19 = Leaderboard_get_range_m234965965(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = (&V_1)->get_count_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_22);
		ObjectU5BU5D_t1108656482* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral2271991173);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral2271991173);
		ObjectU5BU5D_t1108656482* L_24 = L_23;
		uint32_t L_25 = __this->get_m_MaxRange_2();
		uint32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t1108656482* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral778717479);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral778717479);
		ObjectU5BU5D_t1108656482* L_29 = L_28;
		IScoreU5BU5D_t250104726* L_30 = __this->get_m_Scores_3();
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))));
		Il2CppObject * L_32 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_32);
		ObjectU5BU5D_t1108656482* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral4163059537);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral4163059537);
		ObjectU5BU5D_t1108656482* L_34 = L_33;
		int32_t L_35 = Leaderboard_get_userScope_m3547770469(__this, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(UserScope_t1608660171_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral978617427);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral978617427);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		int32_t L_40 = Leaderboard_get_timeScope_m2356081377(__this, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(TimeScope_t1305796361_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_42);
		ObjectU5BU5D_t1108656482* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral1548524389);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral1548524389);
		ObjectU5BU5D_t1108656482* L_44 = L_43;
		StringU5BU5D_t4054002952* L_45 = __this->get_m_UserIDs_5();
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))));
		Il2CppObject * L_47 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3016520001(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Leaderboard::get_loading()
extern Il2CppClass* ISocialPlatform_t277815499_il2cpp_TypeInfo_var;
extern const uint32_t Leaderboard_get_loading_m69476878_MetadataUsageId;
extern "C"  bool Leaderboard_get_loading_m69476878 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_get_loading_m69476878_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ActivePlatform_get_Instance_m486239621(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(3 /* System.Boolean UnityEngine.SocialPlatforms.ISocialPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard) */, ISocialPlatform_t277815499_il2cpp_TypeInfo_var, L_0, __this);
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C"  void Leaderboard_SetLocalUserScore_m700491556 (Leaderboard_t1185876199 * __this, Il2CppObject * ___score0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___score0;
		__this->set_m_LocalUserScore_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C"  void Leaderboard_SetMaxRange_m3779908734 (Leaderboard_t1185876199 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange0;
		__this->set_m_MaxRange_2(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C"  void Leaderboard_SetScores_m1463319879 (Leaderboard_t1185876199 * __this, IScoreU5BU5D_t250104726* ___scores0, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t250104726* L_0 = ___scores0;
		__this->set_m_Scores_3(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C"  void Leaderboard_SetTitle_m771163339 (Leaderboard_t1185876199 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_m_Title_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C"  StringU5BU5D_t4054002952* Leaderboard_GetUserFilter_m3119905721 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t4054002952* L_0 = __this->get_m_UserIDs_5();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C"  String_t* Leaderboard_get_id_m2379239336 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C"  void Leaderboard_set_id_m2022535593 (Leaderboard_t1185876199 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C"  int32_t Leaderboard_get_userScope_m3547770469 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CuserScopeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void Leaderboard_set_userScope_m3914830286 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuserScopeU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C"  Range_t1533311935  Leaderboard_get_range_m234965965 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = __this->get_U3CrangeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void Leaderboard_set_range_m2830170470 (Leaderboard_t1185876199 * __this, Range_t1533311935  ___value0, const MethodInfo* method)
{
	{
		Range_t1533311935  L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C"  int32_t Leaderboard_get_timeScope_m2356081377 (Leaderboard_t1185876199 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CtimeScopeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void Leaderboard_set_timeScope_m3669793618 (Leaderboard_t1185876199 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CtimeScopeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern Il2CppClass* UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var;
extern const uint32_t LocalUser__ctor_m1052633066_MetadataUsageId;
extern "C"  void LocalUser__ctor_m1052633066 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalUser__ctor_m1052633066_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m1280449570(__this, /*hidden argument*/NULL);
		__this->set_m_Friends_5((IUserProfileU5BU5D_t3419104218*)((UserProfileU5BU5D_t2378268441*)SZArrayNew(UserProfileU5BU5D_t2378268441_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Authenticated_6((bool)0);
		__this->set_m_Underage_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::Authenticate(System.Action`1<System.Boolean>)
extern Il2CppClass* ISocialPlatform_t277815499_il2cpp_TypeInfo_var;
extern const uint32_t LocalUser_Authenticate_m272630564_MetadataUsageId;
extern "C"  void LocalUser_Authenticate_m272630564 (LocalUser_t1307362368 * __this, Action_1_t872614854 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalUser_Authenticate_m272630564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ActivePlatform_get_Instance_m486239621(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t872614854 * L_1 = ___callback0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppObject *, Action_1_t872614854 * >::Invoke(1 /* System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>) */, ISocialPlatform_t277815499_il2cpp_TypeInfo_var, L_0, __this, L_1);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C"  void LocalUser_SetFriends_m3475409220 (LocalUser_t1307362368 * __this, IUserProfileU5BU5D_t3419104218* ___friends0, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t3419104218* L_0 = ___friends0;
		__this->set_m_Friends_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C"  void LocalUser_SetAuthenticated_m653377406 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Authenticated_6(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C"  void LocalUser_SetUnderage_m2968368872 (LocalUser_t1307362368 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Underage_7(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C"  bool LocalUser_get_authenticated_m3657159816 (LocalUser_t1307362368 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Authenticated_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t Score__ctor_m113497156_MetadataUsageId;
extern "C"  void Score__ctor_m113497156 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score__ctor_m113497156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int64_t L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Score__ctor_m3768037481(__this, L_0, L_1, _stringLiteral48, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C"  void Score__ctor_m3768037481 (Score_t3396031228 * __this, String_t* ___leaderboardID0, int64_t ___value1, String_t* ___userID2, DateTime_t4283661327  ___date3, String_t* ___formattedValue4, int32_t ___rank5, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID0;
		Score_set_leaderboardID_m3646875387(__this, L_0, /*hidden argument*/NULL);
		int64_t L_1 = ___value1;
		Score_set_value_m2229956626(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___userID2;
		__this->set_m_UserID_2(L_2);
		DateTime_t4283661327  L_3 = ___date3;
		__this->set_m_Date_0(L_3);
		String_t* L_4 = ___formattedValue4;
		__this->set_m_FormattedValue_1(L_4);
		int32_t L_5 = ___rank5;
		__this->set_m_Rank_3(L_5);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2642717173;
extern Il2CppCodeGenString* _stringLiteral1871350441;
extern Il2CppCodeGenString* _stringLiteral188508522;
extern Il2CppCodeGenString* _stringLiteral3481064940;
extern Il2CppCodeGenString* _stringLiteral1272038458;
extern const uint32_t Score_ToString_m3380639973_MetadataUsageId;
extern "C"  String_t* Score_ToString_m3380639973 (Score_t3396031228 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score_ToString_m3380639973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2642717173);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2642717173);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		int32_t L_2 = __this->get_m_Rank_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1871350441);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1871350441);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int64_t L_7 = Score_get_value_m3381234835(__this, /*hidden argument*/NULL);
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral188508522);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral188508522);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		String_t* L_12 = Score_get_leaderboardID_m1097777176(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_12);
		ObjectU5BU5D_t1108656482* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral3481064940);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3481064940);
		ObjectU5BU5D_t1108656482* L_14 = L_13;
		String_t* L_15 = __this->get_m_UserID_2();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral1272038458);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral1272038458);
		ObjectU5BU5D_t1108656482* L_17 = L_16;
		DateTime_t4283661327  L_18 = __this->get_m_Date_0();
		DateTime_t4283661327  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3016520001(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C"  String_t* Score_get_leaderboardID_m1097777176 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CleaderboardIDU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C"  void Score_set_leaderboardID_m3646875387 (Score_t3396031228 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CleaderboardIDU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C"  int64_t Score_get_value_m3381234835 (Score_t3396031228 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CvalueU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C"  void Score_set_value_m2229956626 (Score_t3396031228 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern Il2CppClass* Texture2D_t3884108195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3378765435;
extern Il2CppCodeGenString* _stringLiteral48;
extern const uint32_t UserProfile__ctor_m1280449570_MetadataUsageId;
extern "C"  void UserProfile__ctor_m1280449570 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile__ctor_m1280449570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_m_UserName_0(_stringLiteral3378765435);
		__this->set_m_ID_1(_stringLiteral48);
		__this->set_m_IsFriend_2((bool)0);
		__this->set_m_State_3(3);
		Texture2D_t3884108195 * L_0 = (Texture2D_t3884108195 *)il2cpp_codegen_object_new(Texture2D_t3884108195_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1883511258(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C"  void UserProfile__ctor_m2682768015 (UserProfile_t2280656072 * __this, String_t* ___name0, String_t* ___id1, bool ___friend2, int32_t ___state3, Texture2D_t3884108195 * ___image4, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		String_t* L_1 = ___id1;
		__this->set_m_ID_1(L_1);
		bool L_2 = ___friend2;
		__this->set_m_IsFriend_2(L_2);
		int32_t L_3 = ___state3;
		__this->set_m_State_3(L_3);
		Texture2D_t3884108195 * L_4 = ___image4;
		__this->set_m_Image_4(L_4);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* UserState_t1609153288_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32179;
extern const uint32_t UserProfile_ToString_m2563774257_MetadataUsageId;
extern "C"  String_t* UserProfile_ToString_m2563774257 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_ToString_m2563774257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = UserProfile_get_id_m2095754825(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t1108656482* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral32179);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_3 = L_2;
		String_t* L_4 = UserProfile_get_userName_m3149753764(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral32179);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		bool L_7 = UserProfile_get_isFriend_m1712941209(__this, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral32179);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral32179);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		int32_t L_12 = UserProfile_get_state_m1340538601(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(UserState_t1609153288_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C"  void UserProfile_SetUserName_m914181770 (UserProfile_t2280656072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C"  void UserProfile_SetUserID_m1515238170 (UserProfile_t2280656072 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		__this->set_m_ID_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C"  void UserProfile_SetImage_m1928130753 (UserProfile_t2280656072 * __this, Texture2D_t3884108195 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = ___image0;
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C"  String_t* UserProfile_get_userName_m3149753764 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UserName_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C"  String_t* UserProfile_get_id_m2095754825 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ID_1();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C"  bool UserProfile_get_isFriend_m1712941209 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsFriend_2();
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C"  int32_t UserProfile_get_state_m1340538601 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_State_3();
		return L_0;
	}
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.Impl.UserProfile::get_image()
extern "C"  Texture2D_t3884108195 * UserProfile_get_image_m413513909 (UserProfile_t2280656072 * __this, const MethodInfo* method)
{
	{
		Texture2D_t3884108195 * L_0 = __this->get_m_Image_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
