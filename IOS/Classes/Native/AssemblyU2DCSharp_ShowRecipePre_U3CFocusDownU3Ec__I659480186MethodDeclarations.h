﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowRecipePre/<FocusDown>c__Iterator2D
struct U3CFocusDownU3Ec__Iterator2D_t659480186;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::.ctor()
extern "C"  void U3CFocusDownU3Ec__Iterator2D__ctor_m1503383393 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShowRecipePre/<FocusDown>c__Iterator2D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFocusDownU3Ec__Iterator2D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3613956443 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShowRecipePre/<FocusDown>c__Iterator2D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFocusDownU3Ec__Iterator2D_System_Collections_IEnumerator_get_Current_m1903604463 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowRecipePre/<FocusDown>c__Iterator2D::MoveNext()
extern "C"  bool U3CFocusDownU3Ec__Iterator2D_MoveNext_m413530075 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::Dispose()
extern "C"  void U3CFocusDownU3Ec__Iterator2D_Dispose_m1540712286 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::Reset()
extern "C"  void U3CFocusDownU3Ec__Iterator2D_Reset_m3444783630 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
