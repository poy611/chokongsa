﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
struct OnParticipantStatusChangedCallback_t2709967058;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
struct OnP2PDisconnectedCallback_t401589328;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
struct OnDataReceivedCallback_t176853902;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
struct OnRoomStatusChangedCallback_t1130665710;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
struct OnP2PConnectedCallback_t507387838;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
struct OnRoomConnectedSetChangedCallback_t3651478055;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2709967058.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R401589328.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R176853902.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1130665710.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R507387838.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3651478055.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m2774103205 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnParticipantStatusChangedCallback_t2709967058 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Construct()
extern "C"  IntPtr_t RealTimeEventListenerHelper_RealTimeEventListenerHelper_Construct_m1869499991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback_m2344363819 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnP2PDisconnectedCallback_t401589328 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnDataReceivedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnDataReceivedCallback_m2919741101 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnDataReceivedCallback_t176853902 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m2861445547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnRoomStatusChangedCallback_t1130665710 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PConnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PConnectedCallback_m3416472141 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnP2PConnectedCallback_t507387838 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m4025949195 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnRoomConnectedSetChangedCallback_t3651478055 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_Dispose_m239894535 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
