﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D
struct U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t1088765775;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t3582269991;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey7D__ctor_m1108989970 (U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey7D::<>m__68(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey7D_U3CU3Em__68_m1678130367 (U3CToOnGameThreadU3Ec__AnonStorey7D_t3088451753 * __this, Il2CppObject * ___resolver0, Il2CppObject * ___original1, ByteU5BU5D_t4260760469* ___originalData2, Il2CppObject * ___unmerged3, ByteU5BU5D_t4260760469* ___unmergedData4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
