﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseFsmVariableAction
struct BaseFsmVariableAction_t2224198959;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"

// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::.ctor()
extern "C"  void BaseFsmVariableAction__ctor_m2293414119 (BaseFsmVariableAction_t2224198959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::Reset()
extern "C"  void BaseFsmVariableAction_Reset_m4234814356 (BaseFsmVariableAction_t2224198959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.BaseFsmVariableAction::UpdateCache(UnityEngine.GameObject,System.String)
extern "C"  bool BaseFsmVariableAction_UpdateCache_m3373753190 (BaseFsmVariableAction_t2224198959 * __this, GameObject_t3674682005 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::DoVariableNotFound(System.String)
extern "C"  void BaseFsmVariableAction_DoVariableNotFound_m1188175663 (BaseFsmVariableAction_t2224198959 * __this, String_t* ___variableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
