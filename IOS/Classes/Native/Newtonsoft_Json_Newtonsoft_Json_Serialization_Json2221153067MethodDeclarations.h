﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t2221153067;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass18_0__ctor_m4259546741 (U3CU3Ec__DisplayClass18_0_t2221153067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::<GetJsonConverterCreator>b__0(System.Object[])
extern "C"  JsonConverter_t2159686854 * U3CU3Ec__DisplayClass18_0_U3CGetJsonConverterCreatorU3Eb__0_m3847788273 (U3CU3Ec__DisplayClass18_0_t2221153067 * __this, ObjectU5BU5D_t1108656482* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
