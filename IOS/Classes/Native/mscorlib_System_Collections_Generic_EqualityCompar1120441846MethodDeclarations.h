﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>
struct DefaultComparer_t1120441846;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3577670435_gshared (DefaultComparer_t1120441846 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3577670435(__this, method) ((  void (*) (DefaultComparer_t1120441846 *, const MethodInfo*))DefaultComparer__ctor_m3577670435_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3194129192_gshared (DefaultComparer_t1120441846 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3194129192(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1120441846 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3194129192_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2783955572_gshared (DefaultComparer_t1120441846 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2783955572(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1120441846 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2783955572_gshared)(__this, ___x0, ___y1, method)
