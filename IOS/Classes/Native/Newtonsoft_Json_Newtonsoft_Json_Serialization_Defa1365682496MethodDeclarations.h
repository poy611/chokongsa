﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_t1365682496;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass34_0__ctor_m3497442880 (U3CU3Ec__DisplayClass34_0_t1365682496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
