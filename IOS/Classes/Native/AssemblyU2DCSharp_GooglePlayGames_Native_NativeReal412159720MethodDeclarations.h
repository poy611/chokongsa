﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72
struct U3CAcceptInvitationU3Ec__AnonStorey72_t412159720;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey70/<AcceptInvitation>c__AnonStorey72::.ctor()
extern "C"  void U3CAcceptInvitationU3Ec__AnonStorey72__ctor_m865238707 (U3CAcceptInvitationU3Ec__AnonStorey72_t412159720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
