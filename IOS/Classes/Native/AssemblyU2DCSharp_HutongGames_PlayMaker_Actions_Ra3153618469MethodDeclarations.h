﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomEvent
struct RandomEvent_t3153618469;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomEvent::.ctor()
extern "C"  void RandomEvent__ctor_m2480840369 (RandomEvent_t3153618469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::Reset()
extern "C"  void RandomEvent_Reset_m127273310 (RandomEvent_t3153618469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::OnEnter()
extern "C"  void RandomEvent_OnEnter_m4067441224 (RandomEvent_t3153618469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::OnUpdate()
extern "C"  void RandomEvent_OnUpdate_m670185723 (RandomEvent_t3153618469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RandomEvent::GetRandomEvent()
extern "C"  FsmEvent_t2133468028 * RandomEvent_GetRandomEvent_m1086302141 (RandomEvent_t3153618469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
