﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1109560158.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2327217482.h"

// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1209494991_gshared (InternalEnumerator_1_t1109560158 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1209494991(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1109560158 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1209494991_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297(__this, method) ((  void (*) (InternalEnumerator_1_t1109560158 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1526333297_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1109560158 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4059633831_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2262703718_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2262703718(__this, method) ((  void (*) (InternalEnumerator_1_t1109560158 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2262703718_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3581448865_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3581448865(__this, method) ((  bool (*) (InternalEnumerator_1_t1109560158 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3581448865_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1070987576_gshared (InternalEnumerator_1_t1109560158 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1070987576(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1109560158 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1070987576_gshared)(__this, method)
