﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFlareStrength
struct SetFlareStrength_t2048001173;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::.ctor()
extern "C"  void SetFlareStrength__ctor_m648689009 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::Reset()
extern "C"  void SetFlareStrength_Reset_m2590089246 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnEnter()
extern "C"  void SetFlareStrength_OnEnter_m11608328 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnUpdate()
extern "C"  void SetFlareStrength_OnUpdate_m3788384827 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::DoSetFlareStrength()
extern "C"  void SetFlareStrength_DoSetFlareStrength_m1645466571 (SetFlareStrength_t2048001173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
