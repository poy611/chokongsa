﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEnumValue
struct SetEnumValue_t1055353046;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::.ctor()
extern "C"  void SetEnumValue__ctor_m3285243472 (SetEnumValue_t1055353046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::Reset()
extern "C"  void SetEnumValue_Reset_m931676413 (SetEnumValue_t1055353046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::OnEnter()
extern "C"  void SetEnumValue_OnEnter_m4004709927 (SetEnumValue_t1055353046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::OnUpdate()
extern "C"  void SetEnumValue_OnUpdate_m3020482812 (SetEnumValue_t1055353046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::DoSetEnumValue()
extern "C"  void SetEnumValue_DoSetEnumValue_m2685699533 (SetEnumValue_t1055353046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
