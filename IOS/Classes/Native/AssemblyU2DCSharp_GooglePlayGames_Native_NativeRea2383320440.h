﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t1352686482;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t2043220689;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D
struct  U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440  : public Il2CppObject
{
public:
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D::newRoom
	RoomSession_t1352686482 * ___newRoom_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey6D::<>f__this
	NativeRealtimeMultiplayerClient_t2043220689 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_newRoom_0() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440, ___newRoom_0)); }
	inline RoomSession_t1352686482 * get_newRoom_0() const { return ___newRoom_0; }
	inline RoomSession_t1352686482 ** get_address_of_newRoom_0() { return &___newRoom_0; }
	inline void set_newRoom_0(RoomSession_t1352686482 * value)
	{
		___newRoom_0 = value;
		Il2CppCodeGenWriteBarrier(&___newRoom_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CAcceptFromInboxU3Ec__AnonStorey6D_t2383320440, ___U3CU3Ef__this_1)); }
	inline NativeRealtimeMultiplayerClient_t2043220689 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline NativeRealtimeMultiplayerClient_t2043220689 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(NativeRealtimeMultiplayerClient_t2043220689 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
