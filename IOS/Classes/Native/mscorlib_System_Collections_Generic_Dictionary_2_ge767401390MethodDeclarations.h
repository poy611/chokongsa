﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Dictionary_2_t767401390;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct IDictionary_2_t345274735;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>[]
struct KeyValuePair_2U5BU5D_t1551469809;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>>
struct IEnumerator_1_t2578047145;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct KeyCollection_t2394160841;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct ValueCollection_t3762974399;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_666182096.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2084724782.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor()
extern "C"  void Dictionary_2__ctor_m1215942259_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1215942259(__this, method) ((  void (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2__ctor_m1215942259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2942280042_gshared (Dictionary_2_t767401390 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2942280042(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2942280042_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m1032093957_gshared (Dictionary_2_t767401390 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m1032093957(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1032093957_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m94269252_gshared (Dictionary_2_t767401390 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m94269252(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t767401390 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m94269252_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1279236120_gshared (Dictionary_2_t767401390 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1279236120(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1279236120_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3728905017_gshared (Dictionary_2_t767401390 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3728905017(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t767401390 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3728905017_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m4150992180_gshared (Dictionary_2_t767401390 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m4150992180(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t767401390 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m4150992180_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1514420795_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1514420795(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1514420795_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1418101369_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1418101369(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1418101369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3828117867_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3828117867(__this, method) ((  bool (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3828117867_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m522808565_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m522808565(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m522808565_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2161438618_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2161438618(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2161438618_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1156347511_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1156347511(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1156347511_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1106553119_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1106553119(__this, ___key0, method) ((  bool (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1106553119_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3162365400_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3162365400(__this, ___key0, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3162365400_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1158132373_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1158132373(__this, method) ((  bool (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1158132373_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1490535169_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1490535169(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1490535169_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2270779033_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2270779033(__this, method) ((  bool (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2270779033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2948513390_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2_t666182096  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2948513390(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t767401390 *, KeyValuePair_2_t666182096 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2948513390_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3354222868_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2_t666182096  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3354222868(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t767401390 *, KeyValuePair_2_t666182096 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3354222868_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m488517330_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2U5BU5D_t1551469809* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m488517330(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t767401390 *, KeyValuePair_2U5BU5D_t1551469809*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m488517330_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754474809_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2_t666182096  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754474809(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t767401390 *, KeyValuePair_2_t666182096 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3754474809_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2282779185_gshared (Dictionary_2_t767401390 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2282779185(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2282779185_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m475150316_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m475150316(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m475150316_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1564928169_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1564928169(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1564928169_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m410088964_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m410088964(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m410088964_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1709903195_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1709903195(__this, method) ((  int32_t (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_get_Count_m1709903195_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m1919320496_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1919320496(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1919320496_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1304313587_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1304313587(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1304313587_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1682017771_gshared (Dictionary_2_t767401390 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1682017771(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t767401390 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1682017771_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3500056876_gshared (Dictionary_2_t767401390 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3500056876(__this, ___size0, method) ((  void (*) (Dictionary_2_t767401390 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3500056876_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2670329896_gshared (Dictionary_2_t767401390 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2670329896(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2670329896_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t666182096  Dictionary_2_make_pair_m4283168372_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4283168372(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t666182096  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m4283168372_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m380421634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m380421634(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m380421634_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m3970845890_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3970845890(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m3970845890_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2619176615_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2U5BU5D_t1551469809* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2619176615(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t767401390 *, KeyValuePair_2U5BU5D_t1551469809*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2619176615_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Resize()
extern "C"  void Dictionary_2_Resize_m3392268325_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3392268325(__this, method) ((  void (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_Resize_m3392268325_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1907554210_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1907554210(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_Add_m1907554210_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Clear()
extern "C"  void Dictionary_2_Clear_m2917042846_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2917042846(__this, method) ((  void (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_Clear_m2917042846_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1024276868_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1024276868(__this, ___key0, method) ((  bool (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m1024276868_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m224930692_gshared (Dictionary_2_t767401390 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m224930692(__this, ___value0, method) ((  bool (*) (Dictionary_2_t767401390 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m224930692_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3139825809_gshared (Dictionary_2_t767401390 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3139825809(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t767401390 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3139825809_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1395534451_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1395534451(__this, ___sender0, method) ((  void (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1395534451_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3683523820_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3683523820(__this, ___key0, method) ((  bool (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m3683523820_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2677657053_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2677657053(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t767401390 *, Il2CppObject *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m2677657053_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Keys()
extern "C"  KeyCollection_t2394160841 * Dictionary_2_get_Keys_m624357058_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m624357058(__this, method) ((  KeyCollection_t2394160841 * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_get_Keys_m624357058_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Values()
extern "C"  ValueCollection_t3762974399 * Dictionary_2_get_Values_m1791277890_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1791277890(__this, method) ((  ValueCollection_t3762974399 * (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_get_Values_m1791277890_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m4125247837_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4125247837(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4125247837_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m2283160029_gshared (Dictionary_2_t767401390 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2283160029(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t767401390 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2283160029_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1263659727_gshared (Dictionary_2_t767401390 * __this, KeyValuePair_2_t666182096  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1263659727(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t767401390 *, KeyValuePair_2_t666182096 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1263659727_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetEnumerator()
extern "C"  Enumerator_t2084724782  Dictionary_2_GetEnumerator_m3945185976_gshared (Dictionary_2_t767401390 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3945185976(__this, method) ((  Enumerator_t2084724782  (*) (Dictionary_2_t767401390 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3945185976_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m3863998471_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3863998471(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3863998471_gshared)(__this /* static, unused */, ___key0, ___value1, method)
