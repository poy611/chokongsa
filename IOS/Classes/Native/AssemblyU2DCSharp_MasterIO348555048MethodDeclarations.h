﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MasterIO
struct MasterIO_t348555048;
// StructforMinigame
struct StructforMinigame_t1286533533;

#include "codegen/il2cpp-codegen.h"

// System.Void MasterIO::.ctor()
extern "C"  void MasterIO__ctor_m3634087027 (MasterIO_t348555048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MasterIO::Start()
extern "C"  void MasterIO_Start_m2581224819 (MasterIO_t348555048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// StructforMinigame MasterIO::getStruct(System.Int32)
extern "C"  StructforMinigame_t1286533533 * MasterIO_getStruct_m17338999 (MasterIO_t348555048 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
