﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// UnityEngine.Component
struct Component_t3501516275;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.Behaviour
struct Behaviour_t200106419;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableBehaviour
struct  EnableBehaviour_t2067166088  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableBehaviour::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableBehaviour::behaviour
	FsmString_t952858651 * ___behaviour_12;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.EnableBehaviour::component
	Component_t3501516275 * ___component_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::enable
	FsmBool_t1075959796 * ___enable_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::resetOnExit
	FsmBool_t1075959796 * ___resetOnExit_15;
	// UnityEngine.Behaviour HutongGames.PlayMaker.Actions.EnableBehaviour::componentTarget
	Behaviour_t200106419 * ___componentTarget_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_behaviour_12() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___behaviour_12)); }
	inline FsmString_t952858651 * get_behaviour_12() const { return ___behaviour_12; }
	inline FsmString_t952858651 ** get_address_of_behaviour_12() { return &___behaviour_12; }
	inline void set_behaviour_12(FsmString_t952858651 * value)
	{
		___behaviour_12 = value;
		Il2CppCodeGenWriteBarrier(&___behaviour_12, value);
	}

	inline static int32_t get_offset_of_component_13() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___component_13)); }
	inline Component_t3501516275 * get_component_13() const { return ___component_13; }
	inline Component_t3501516275 ** get_address_of_component_13() { return &___component_13; }
	inline void set_component_13(Component_t3501516275 * value)
	{
		___component_13 = value;
		Il2CppCodeGenWriteBarrier(&___component_13, value);
	}

	inline static int32_t get_offset_of_enable_14() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___enable_14)); }
	inline FsmBool_t1075959796 * get_enable_14() const { return ___enable_14; }
	inline FsmBool_t1075959796 ** get_address_of_enable_14() { return &___enable_14; }
	inline void set_enable_14(FsmBool_t1075959796 * value)
	{
		___enable_14 = value;
		Il2CppCodeGenWriteBarrier(&___enable_14, value);
	}

	inline static int32_t get_offset_of_resetOnExit_15() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___resetOnExit_15)); }
	inline FsmBool_t1075959796 * get_resetOnExit_15() const { return ___resetOnExit_15; }
	inline FsmBool_t1075959796 ** get_address_of_resetOnExit_15() { return &___resetOnExit_15; }
	inline void set_resetOnExit_15(FsmBool_t1075959796 * value)
	{
		___resetOnExit_15 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_15, value);
	}

	inline static int32_t get_offset_of_componentTarget_16() { return static_cast<int32_t>(offsetof(EnableBehaviour_t2067166088, ___componentTarget_16)); }
	inline Behaviour_t200106419 * get_componentTarget_16() const { return ___componentTarget_16; }
	inline Behaviour_t200106419 ** get_address_of_componentTarget_16() { return &___componentTarget_16; }
	inline void set_componentTarget_16(Behaviour_t200106419 * value)
	{
		___componentTarget_16 = value;
		Il2CppCodeGenWriteBarrier(&___componentTarget_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
