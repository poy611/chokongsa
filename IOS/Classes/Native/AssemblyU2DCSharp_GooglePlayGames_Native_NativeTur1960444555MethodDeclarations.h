﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllMatches>c__AnonStorey8B
struct U3CGetAllMatchesU3Ec__AnonStorey8B_t1960444555;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t3460122515;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_T3460122515.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllMatches>c__AnonStorey8B::.ctor()
extern "C"  void U3CGetAllMatchesU3Ec__AnonStorey8B__ctor_m481828464 (U3CGetAllMatchesU3Ec__AnonStorey8B_t1960444555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<GetAllMatches>c__AnonStorey8B::<>m__7A(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse)
extern "C"  void U3CGetAllMatchesU3Ec__AnonStorey8B_U3CU3Em__7A_m262005360 (U3CGetAllMatchesU3Ec__AnonStorey8B_t1960444555 * __this, TurnBasedMatchesResponse_t3460122515 * ___allMatches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
