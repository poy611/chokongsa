﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3460711275(__this, ___l0, method) ((  void (*) (Enumerator_t1065944292 *, List_1_t1046271522 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m469834247(__this, method) ((  void (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2220069555(__this, method) ((  Il2CppObject * (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::Dispose()
#define Enumerator_Dispose_m3598588944(__this, method) ((  void (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::VerifyState()
#define Enumerator_VerifyState_m1545950025(__this, method) ((  void (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::MoveNext()
#define Enumerator_MoveNext_m1233126899(__this, method) ((  bool (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::get_Current()
#define Enumerator_get_Current_m4155146496(__this, method) ((  FieldInfo_t * (*) (Enumerator_t1065944292 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
