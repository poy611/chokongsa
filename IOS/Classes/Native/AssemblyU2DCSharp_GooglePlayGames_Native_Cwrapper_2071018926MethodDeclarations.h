﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3579266924.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_EventId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_EventId_m2634184396 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CurrentCount(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t QuestMilestone_QuestMilestone_CurrentCount_m2780596114 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Copy(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestMilestone_QuestMilestone_Copy_m2920690680 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestMilestone_QuestMilestone_Dispose_m3246914755 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_TargetCount(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t QuestMilestone_QuestMilestone_TargetCount_m2990276734 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_QuestId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_QuestId_m2078821764 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CompletionRewardData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_CompletionRewardData_m640186953 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_State(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestMilestone_QuestMilestone_State_m409042012 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool QuestMilestone_QuestMilestone_Valid_m279486356 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_Id_m327259042 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
