﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadLeaderboardData>c__AnonStoreyA1
struct U3CLoadLeaderboardDataU3Ec__AnonStoreyA1_t280358138;
// GooglePlayGames.Native.PInvoke.FetchResponse
struct FetchResponse_t642400449;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Fe642400449.h"

// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadLeaderboardData>c__AnonStoreyA1::.ctor()
extern "C"  void U3CLoadLeaderboardDataU3Ec__AnonStoreyA1__ctor_m3027992801 (U3CLoadLeaderboardDataU3Ec__AnonStoreyA1_t280358138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.LeaderboardManager/<LoadLeaderboardData>c__AnonStoreyA1::<>m__A4(GooglePlayGames.Native.PInvoke.FetchResponse)
extern "C"  void U3CLoadLeaderboardDataU3Ec__AnonStoreyA1_U3CU3Em__A4_m4247852453 (U3CLoadLeaderboardDataU3Ec__AnonStoreyA1_t280358138 * __this, FetchResponse_t642400449 * ___rsp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
