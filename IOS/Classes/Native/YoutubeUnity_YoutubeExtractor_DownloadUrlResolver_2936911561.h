﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12
struct  U3CU3Ec__DisplayClass12_t2936911561  : public Il2CppObject
{
public:
	// System.Int32 YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12::formatCode
	int32_t ___formatCode_0;

public:
	inline static int32_t get_offset_of_formatCode_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_t2936911561, ___formatCode_0)); }
	inline int32_t get_formatCode_0() const { return ___formatCode_0; }
	inline int32_t* get_address_of_formatCode_0() { return &___formatCode_0; }
	inline void set_formatCode_0(int32_t value)
	{
		___formatCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
