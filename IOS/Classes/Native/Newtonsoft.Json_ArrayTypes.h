﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t871800199;
// Newtonsoft.Json.Utilities.TypeInformation
struct TypeInformation_t2222045584;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>
struct BidirectionalDictionary_2_t157076046;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry
struct Entry_t1172375224;
// Newtonsoft.Json.Utilities.ReflectionMember
struct ReflectionMember_t3070212469;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.Serialization.SerializationCallback
struct SerializationCallback_t799500859;
// Newtonsoft.Json.Serialization.SerializationErrorCallback
struct SerializationErrorCallback_t1476028681;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext
struct CreatorPropertyContext_t3090945776;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t3124613658;
// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t4293821333;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;

#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter2159686854.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"
#include "Newtonsoft.Json_ArrayTypes.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter_State671991922.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeInfo2222045584.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Bidirecti157076046.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Property1172375224.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3070212469.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Resol473801005.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1328848902.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seria799500859.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seri1476028681.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonP902655177.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2971844791.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType3446921512.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3090945776.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2892329490.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3124613658.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonProperty4293821333.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonToken455725415.h"

#pragma once
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonConverter_t2159686854 * m_Items[1];

public:
	inline JsonConverter_t2159686854 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonConverter_t2159686854 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonConverter_t2159686854 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonPosition[]
struct JsonPositionU5BU5D_t3412818772  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonPosition_t3864946409  m_Items[1];

public:
	inline JsonPosition_t3864946409  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonPosition_t3864946409 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonPosition_t3864946409  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t494809214  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateU5BU5D_t871800199* m_Items[1];

public:
	inline StateU5BU5D_t871800199* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateU5BU5D_t871800199** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateU5BU5D_t871800199* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.JsonWriter/State[]
struct StateU5BU5D_t871800199  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Utilities.PrimitiveTypeCode[]
struct PrimitiveTypeCodeU5BU5D_t2402767813  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Utilities.TypeInformation[]
struct TypeInformationU5BU5D_t995548721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeInformation_t2222045584 * m_Items[1];

public:
	inline TypeInformation_t2222045584 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeInformation_t2222045584 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeInformation_t2222045584 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey[]
struct TypeConvertKeyU5BU5D_t1979890475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeConvertKey_t866134174  m_Items[1];

public:
	inline TypeConvertKey_t866134174  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeConvertKey_t866134174 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeConvertKey_t866134174  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>[]
struct BidirectionalDictionary_2U5BU5D_t3532811451  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BidirectionalDictionary_2_t157076046 * m_Items[1];

public:
	inline BidirectionalDictionary_2_t157076046 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BidirectionalDictionary_2_t157076046 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BidirectionalDictionary_2_t157076046 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t3715182441  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Entry_t1172375224 * m_Items[1];

public:
	inline Entry_t1172375224 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Entry_t1172375224 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Entry_t1172375224 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Utilities.ReflectionMember[]
struct ReflectionMemberU5BU5D_t1214361880  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReflectionMember_t3070212469 * m_Items[1];

public:
	inline ReflectionMember_t3070212469 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReflectionMember_t3070212469 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReflectionMember_t3070212469 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.ResolverContractKey[]
struct ResolverContractKeyU5BU5D_t2057919360  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ResolverContractKey_t473801005  m_Items[1];

public:
	inline ResolverContractKey_t473801005  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ResolverContractKey_t473801005 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ResolverContractKey_t473801005  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Serialization.JsonContract[]
struct JsonContractU5BU5D_t4247040291  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonContract_t1328848902 * m_Items[1];

public:
	inline JsonContract_t1328848902 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonContract_t1328848902 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonContract_t1328848902 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.SerializationCallback[]
struct SerializationCallbackU5BU5D_t3876533882  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SerializationCallback_t799500859 * m_Items[1];

public:
	inline SerializationCallback_t799500859 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SerializationCallback_t799500859 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SerializationCallback_t799500859 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.SerializationErrorCallback[]
struct SerializationErrorCallbackU5BU5D_t1215925428  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SerializationErrorCallback_t1476028681 * m_Items[1];

public:
	inline SerializationErrorCallback_t1476028681 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SerializationErrorCallback_t1476028681 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SerializationErrorCallback_t1476028681 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonProperty[]
struct JsonPropertyU5BU5D_t1037910516  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonProperty_t902655177 * m_Items[1];

public:
	inline JsonProperty_t902655177 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonProperty_t902655177 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonProperty_t902655177 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey[]
struct TypeNameKeyU5BU5D_t3177491086  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TypeNameKey_t2971844791  m_Items[1];

public:
	inline TypeNameKey_t2971844791  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeNameKey_t2971844791 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeNameKey_t2971844791  value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.ReadType[]
struct ReadTypeU5BU5D_t2190873913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext[]
struct CreatorPropertyContextU5BU5D_t1212907345  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CreatorPropertyContext_t3090945776 * m_Items[1];

public:
	inline CreatorPropertyContext_t3090945776 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CreatorPropertyContext_t3090945776 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CreatorPropertyContext_t3090945776 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence[]
struct PropertyPresenceU5BU5D_t176945511  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t2853253222  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JToken_t3412245951 * m_Items[1];

public:
	inline JToken_t3412245951 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JToken_t3412245951 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JToken_t3412245951 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1069135460  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Newtonsoft.Json.Utilities.ReflectionObject[]
struct ReflectionObjectU5BU5D_t2000367679  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReflectionObject_t3124613658 * m_Items[1];

public:
	inline ReflectionObject_t3124613658 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReflectionObject_t3124613658 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReflectionObject_t3124613658 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonProperty[]
struct BsonPropertyU5BU5D_t2882591352  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonProperty_t4293821333 * m_Items[1];

public:
	inline BsonProperty_t4293821333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonProperty_t4293821333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonProperty_t4293821333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Newtonsoft.Json.Bson.BsonToken[]
struct BsonTokenU5BU5D_t1977569566  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) BsonToken_t455725415 * m_Items[1];

public:
	inline BsonToken_t455725415 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BsonToken_t455725415 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BsonToken_t455725415 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
