﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PopupManagement
struct PopupManagement_t282433007;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_ButtonManager_StateTitle672268027.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonManager
struct  ButtonManager_t2890896379  : public MonoBehaviour_t667441552
{
public:
	// PopupManagement ButtonManager::pop
	PopupManagement_t282433007 * ___pop_2;
	// ButtonManager/StateTitle ButtonManager::state
	int32_t ___state_3;

public:
	inline static int32_t get_offset_of_pop_2() { return static_cast<int32_t>(offsetof(ButtonManager_t2890896379, ___pop_2)); }
	inline PopupManagement_t282433007 * get_pop_2() const { return ___pop_2; }
	inline PopupManagement_t282433007 ** get_address_of_pop_2() { return &___pop_2; }
	inline void set_pop_2(PopupManagement_t282433007 * value)
	{
		___pop_2 = value;
		Il2CppCodeGenWriteBarrier(&___pop_2, value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(ButtonManager_t2890896379, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
