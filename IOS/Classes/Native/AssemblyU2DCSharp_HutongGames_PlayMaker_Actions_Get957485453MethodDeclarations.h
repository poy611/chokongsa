﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorPivot
struct GetAnimatorPivot_t957485453;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::.ctor()
extern "C"  void GetAnimatorPivot__ctor_m1612529529 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::Reset()
extern "C"  void GetAnimatorPivot_Reset_m3553929766 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnEnter()
extern "C"  void GetAnimatorPivot_OnEnter_m2844379408 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnActionUpdate()
extern "C"  void GetAnimatorPivot_OnActionUpdate_m1855499465 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::DoCheckPivot()
extern "C"  void GetAnimatorPivot_DoCheckPivot_m312014448 (GetAnimatorPivot_t957485453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
