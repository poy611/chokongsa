﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct GetAnimatorIsLayerInTransition_t3666075020;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::.ctor()
extern "C"  void GetAnimatorIsLayerInTransition__ctor_m3017439322 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::Reset()
extern "C"  void GetAnimatorIsLayerInTransition_Reset_m663872263 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnEnter()
extern "C"  void GetAnimatorIsLayerInTransition_OnEnter_m47992241 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnActionUpdate()
extern "C"  void GetAnimatorIsLayerInTransition_OnActionUpdate_m3301368072 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::DoCheckIsInTransition()
extern "C"  void GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m701433465 (GetAnimatorIsLayerInTransition_t3666075020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
