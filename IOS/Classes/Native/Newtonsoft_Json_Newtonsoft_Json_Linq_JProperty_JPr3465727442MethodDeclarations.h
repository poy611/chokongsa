﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1
struct U3CGetEnumeratorU3Ed__1_t3465727442;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::.ctor(System.Int32)
extern "C"  void U3CGetEnumeratorU3Ed__1__ctor_m4155597669 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::System.IDisposable.Dispose()
extern "C"  void U3CGetEnumeratorU3Ed__1_System_IDisposable_Dispose_m1646704651 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ed__1_MoveNext_m3893271294 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CGetEnumeratorU3Ed__1_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m704704829 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
extern "C"  void U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_Reset_m3931227750 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_get_Current_m3782946972 (U3CGetEnumeratorU3Ed__1_t3465727442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
