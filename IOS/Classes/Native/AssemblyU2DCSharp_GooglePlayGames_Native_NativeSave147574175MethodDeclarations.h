﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m3379422511_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m3379422511(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey86_2__ctor_m3379422511_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey85`2/<ToOnGameThread>c__AnonStorey86`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::<>m__72()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m2937977939_gshared (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m2937977939(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey86_2_t147574175 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey86_2_U3CU3Em__72_m2937977939_gshared)(__this, method)
