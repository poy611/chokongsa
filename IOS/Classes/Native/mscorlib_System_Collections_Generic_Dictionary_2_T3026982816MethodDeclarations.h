﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>
struct Transform_1_t3026982816;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3448075042_gshared (Transform_1_t3026982816 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3448075042(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3026982816 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3448075042_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m575809814_gshared (Transform_1_t3026982816 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m575809814(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t3026982816 *, Il2CppObject *, RaycastHit2D_t1374744384 , const MethodInfo*))Transform_1_Invoke_m575809814_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2720051009_gshared (Transform_1_t3026982816 * __this, Il2CppObject * ___key0, RaycastHit2D_t1374744384  ___value1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2720051009(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3026982816 *, Il2CppObject *, RaycastHit2D_t1374744384 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2720051009_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3185118644_gshared (Transform_1_t3026982816 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3185118644(__this, ___result0, method) ((  Il2CppObject * (*) (Transform_1_t3026982816 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3185118644_gshared)(__this, ___result0, method)
