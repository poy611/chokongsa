﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HasComponent
struct HasComponent_t1091840427;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void HutongGames.PlayMaker.Actions.HasComponent::.ctor()
extern "C"  void HasComponent__ctor_m3373741339 (HasComponent_t1091840427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::Reset()
extern "C"  void HasComponent_Reset_m1020174280 (HasComponent_t1091840427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnEnter()
extern "C"  void HasComponent_OnEnter_m3151814194 (HasComponent_t1091840427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnUpdate()
extern "C"  void HasComponent_OnUpdate_m2350518865 (HasComponent_t1091840427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnExit()
extern "C"  void HasComponent_OnExit_m3020085030 (HasComponent_t1091840427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::DoHasComponent(UnityEngine.GameObject)
extern "C"  void HasComponent_DoHasComponent_m1765689135 (HasComponent_t1091840427 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
