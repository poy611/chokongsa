﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScaleGUI
struct ScaleGUI_t3150497849;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::.ctor()
extern "C"  void ScaleGUI__ctor_m3686589005 (ScaleGUI_t3150497849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::Reset()
extern "C"  void ScaleGUI_Reset_m1333021946 (ScaleGUI_t3150497849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::OnGUI()
extern "C"  void ScaleGUI_OnGUI_m3181987655 (ScaleGUI_t3150497849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::OnUpdate()
extern "C"  void ScaleGUI_OnUpdate_m2316304351 (ScaleGUI_t3150497849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
