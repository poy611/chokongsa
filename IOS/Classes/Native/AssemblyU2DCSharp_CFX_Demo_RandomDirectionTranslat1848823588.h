﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_RandomDirectionTranslate
struct  CFX_Demo_RandomDirectionTranslate_t1848823588  : public MonoBehaviour_t667441552
{
public:
	// System.Single CFX_Demo_RandomDirectionTranslate::speed
	float ___speed_2;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::baseDir
	Vector3_t4282066566  ___baseDir_3;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::axis
	Vector3_t4282066566  ___axis_4;
	// System.Boolean CFX_Demo_RandomDirectionTranslate::gravity
	bool ___gravity_5;
	// UnityEngine.Vector3 CFX_Demo_RandomDirectionTranslate::dir
	Vector3_t4282066566  ___dir_6;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDirectionTranslate_t1848823588, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_baseDir_3() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDirectionTranslate_t1848823588, ___baseDir_3)); }
	inline Vector3_t4282066566  get_baseDir_3() const { return ___baseDir_3; }
	inline Vector3_t4282066566 * get_address_of_baseDir_3() { return &___baseDir_3; }
	inline void set_baseDir_3(Vector3_t4282066566  value)
	{
		___baseDir_3 = value;
	}

	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDirectionTranslate_t1848823588, ___axis_4)); }
	inline Vector3_t4282066566  get_axis_4() const { return ___axis_4; }
	inline Vector3_t4282066566 * get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(Vector3_t4282066566  value)
	{
		___axis_4 = value;
	}

	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDirectionTranslate_t1848823588, ___gravity_5)); }
	inline bool get_gravity_5() const { return ___gravity_5; }
	inline bool* get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(bool value)
	{
		___gravity_5 = value;
	}

	inline static int32_t get_offset_of_dir_6() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDirectionTranslate_t1848823588, ___dir_6)); }
	inline Vector3_t4282066566  get_dir_6() const { return ___dir_6; }
	inline Vector3_t4282066566 * get_address_of_dir_6() { return &___dir_6; }
	inline void set_dir_6(Vector3_t4282066566  value)
	{
		___dir_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
