﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ResultAllData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2496557141(__this, ___l0, method) ((  void (*) (Enumerator_t365839232 *, List_1_t346166462 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultAllData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1813447389(__this, method) ((  void (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ResultAllData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1785195081(__this, method) ((  Il2CppObject * (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultAllData>::Dispose()
#define Enumerator_Dispose_m3299386234(__this, method) ((  void (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ResultAllData>::VerifyState()
#define Enumerator_VerifyState_m2575963571(__this, method) ((  void (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ResultAllData>::MoveNext()
#define Enumerator_MoveNext_m207556681(__this, method) ((  bool (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ResultAllData>::get_Current()
#define Enumerator_get_Current_m957132202(__this, method) ((  ResultAllData_t3272948206 * (*) (Enumerator_t365839232 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
