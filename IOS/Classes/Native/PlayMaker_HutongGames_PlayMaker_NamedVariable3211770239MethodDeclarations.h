﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType3118725144.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"

// System.String HutongGames.PlayMaker.NamedVariable::get_Name()
extern "C"  String_t* NamedVariable_get_Name_m3214577877 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Name(System.String)
extern "C"  void NamedVariable_set_Name_m926849846 (NamedVariable_t3211770239 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_VariableType()
extern "C"  int32_t NamedVariable_get_VariableType_m65280228 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.NamedVariable::get_ObjectType()
extern "C"  Type_t * NamedVariable_get_ObjectType_m3080144652 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_ObjectType(System.Type)
extern "C"  void NamedVariable_set_ObjectType_m2203531999 (NamedVariable_t3211770239 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_TypeConstraint()
extern "C"  int32_t NamedVariable_get_TypeConstraint_m3835641413 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_RawValue(System.Object)
extern "C"  void NamedVariable_set_RawValue_m3168388714 (NamedVariable_t3211770239 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.NamedVariable::get_RawValue()
extern "C"  Il2CppObject * NamedVariable_get_RawValue_m215409825 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.NamedVariable::get_Tooltip()
extern "C"  String_t* NamedVariable_get_Tooltip_m3321065371 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Tooltip(System.String)
extern "C"  void NamedVariable_set_Tooltip_m2642183102 (NamedVariable_t3211770239 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UseVariable()
extern "C"  bool NamedVariable_get_UseVariable_m692522704 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_UseVariable(System.Boolean)
extern "C"  void NamedVariable_set_UseVariable_m4266138971 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_ShowInInspector()
extern "C"  bool NamedVariable_get_ShowInInspector_m2101101378 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_ShowInInspector(System.Boolean)
extern "C"  void NamedVariable_set_ShowInInspector_m2820077645 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_NetworkSync()
extern "C"  bool NamedVariable_get_NetworkSync_m3720647190 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_NetworkSync(System.Boolean)
extern "C"  void NamedVariable_set_NetworkSync_m1835232801 (NamedVariable_t3211770239 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::IsNullOrNone(HutongGames.PlayMaker.NamedVariable)
extern "C"  bool NamedVariable_IsNullOrNone_m2778814161 (Il2CppObject * __this /* static, unused */, NamedVariable_t3211770239 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_IsNone()
extern "C"  bool NamedVariable_get_IsNone_m281035543 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UsesVariable()
extern "C"  bool NamedVariable_get_UsesVariable_m3517877629 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor()
extern "C"  void NamedVariable__ctor_m438771392 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(System.String)
extern "C"  void NamedVariable__ctor_m395475010 (NamedVariable_t3211770239 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(HutongGames.PlayMaker.NamedVariable)
extern "C"  void NamedVariable__ctor_m3930624905 (NamedVariable_t3211770239 * __this, NamedVariable_t3211770239 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool NamedVariable_TestTypeConstraint_m196465542 (NamedVariable_t3211770239 * __this, int32_t ___variableType0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::SafeAssign(System.Object)
extern "C"  void NamedVariable_SafeAssign_m2321048948 (NamedVariable_t3211770239 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.NamedVariable::Clone()
extern "C"  NamedVariable_t3211770239 * NamedVariable_Clone_m3584669925 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.NamedVariable::GetDisplayName()
extern "C"  String_t* NamedVariable_GetDisplayName_m3040233336 (NamedVariable_t3211770239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.NamedVariable::CompareTo(System.Object)
extern "C"  int32_t NamedVariable_CompareTo_m3543297572 (NamedVariable_t3211770239 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
