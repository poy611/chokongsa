﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F
struct U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482;
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
struct OpenResponse_t1933021492;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1933021492.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F::.ctor()
extern "C"  void U3CInternalManualOpenU3Ec__AnonStorey7F__ctor_m4113249497 (U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey7F::<>m__69(GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse)
extern "C"  void U3CInternalManualOpenU3Ec__AnonStorey7F_U3CU3Em__69_m2495557817 (U3CInternalManualOpenU3Ec__AnonStorey7F_t971412482 * __this, OpenResponse_t1933021492 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
