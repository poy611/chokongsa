﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw410382178.h"
#include "AssemblyU2DCSharp_iTween_EaseType2734598229.h"
#include "AssemblyU2DCSharp_iTween_LoopType1485160459.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3260241011.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenMoveAdd
struct  iTweenMoveAdd_t1025491740  : public iTweenFsmAction_t410382178
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenMoveAdd::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.iTweenMoveAdd::id
	FsmString_t952858651 * ___id_20;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenMoveAdd::vector
	FsmVector3_t533912882 * ___vector_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveAdd::time
	FsmFloat_t2134102846 * ___time_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveAdd::delay
	FsmFloat_t2134102846 * ___delay_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveAdd::speed
	FsmFloat_t2134102846 * ___speed_24;
	// iTween/EaseType HutongGames.PlayMaker.Actions.iTweenMoveAdd::easeType
	int32_t ___easeType_25;
	// iTween/LoopType HutongGames.PlayMaker.Actions.iTweenMoveAdd::loopType
	int32_t ___loopType_26;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenMoveAdd::space
	int32_t ___space_27;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.iTweenMoveAdd::orientToPath
	FsmBool_t1075959796 * ___orientToPath_28;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenMoveAdd::lookAtObject
	FsmGameObject_t1697147867 * ___lookAtObject_29;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenMoveAdd::lookAtVector
	FsmVector3_t533912882 * ___lookAtVector_30;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenMoveAdd::lookTime
	FsmFloat_t2134102846 * ___lookTime_31;
	// HutongGames.PlayMaker.Actions.iTweenFsmAction/AxisRestriction HutongGames.PlayMaker.Actions.iTweenMoveAdd::axis
	int32_t ___axis_32;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___gameObject_19)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_19, value);
	}

	inline static int32_t get_offset_of_id_20() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___id_20)); }
	inline FsmString_t952858651 * get_id_20() const { return ___id_20; }
	inline FsmString_t952858651 ** get_address_of_id_20() { return &___id_20; }
	inline void set_id_20(FsmString_t952858651 * value)
	{
		___id_20 = value;
		Il2CppCodeGenWriteBarrier(&___id_20, value);
	}

	inline static int32_t get_offset_of_vector_21() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___vector_21)); }
	inline FsmVector3_t533912882 * get_vector_21() const { return ___vector_21; }
	inline FsmVector3_t533912882 ** get_address_of_vector_21() { return &___vector_21; }
	inline void set_vector_21(FsmVector3_t533912882 * value)
	{
		___vector_21 = value;
		Il2CppCodeGenWriteBarrier(&___vector_21, value);
	}

	inline static int32_t get_offset_of_time_22() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___time_22)); }
	inline FsmFloat_t2134102846 * get_time_22() const { return ___time_22; }
	inline FsmFloat_t2134102846 ** get_address_of_time_22() { return &___time_22; }
	inline void set_time_22(FsmFloat_t2134102846 * value)
	{
		___time_22 = value;
		Il2CppCodeGenWriteBarrier(&___time_22, value);
	}

	inline static int32_t get_offset_of_delay_23() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___delay_23)); }
	inline FsmFloat_t2134102846 * get_delay_23() const { return ___delay_23; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_23() { return &___delay_23; }
	inline void set_delay_23(FsmFloat_t2134102846 * value)
	{
		___delay_23 = value;
		Il2CppCodeGenWriteBarrier(&___delay_23, value);
	}

	inline static int32_t get_offset_of_speed_24() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___speed_24)); }
	inline FsmFloat_t2134102846 * get_speed_24() const { return ___speed_24; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_24() { return &___speed_24; }
	inline void set_speed_24(FsmFloat_t2134102846 * value)
	{
		___speed_24 = value;
		Il2CppCodeGenWriteBarrier(&___speed_24, value);
	}

	inline static int32_t get_offset_of_easeType_25() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___easeType_25)); }
	inline int32_t get_easeType_25() const { return ___easeType_25; }
	inline int32_t* get_address_of_easeType_25() { return &___easeType_25; }
	inline void set_easeType_25(int32_t value)
	{
		___easeType_25 = value;
	}

	inline static int32_t get_offset_of_loopType_26() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___loopType_26)); }
	inline int32_t get_loopType_26() const { return ___loopType_26; }
	inline int32_t* get_address_of_loopType_26() { return &___loopType_26; }
	inline void set_loopType_26(int32_t value)
	{
		___loopType_26 = value;
	}

	inline static int32_t get_offset_of_space_27() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___space_27)); }
	inline int32_t get_space_27() const { return ___space_27; }
	inline int32_t* get_address_of_space_27() { return &___space_27; }
	inline void set_space_27(int32_t value)
	{
		___space_27 = value;
	}

	inline static int32_t get_offset_of_orientToPath_28() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___orientToPath_28)); }
	inline FsmBool_t1075959796 * get_orientToPath_28() const { return ___orientToPath_28; }
	inline FsmBool_t1075959796 ** get_address_of_orientToPath_28() { return &___orientToPath_28; }
	inline void set_orientToPath_28(FsmBool_t1075959796 * value)
	{
		___orientToPath_28 = value;
		Il2CppCodeGenWriteBarrier(&___orientToPath_28, value);
	}

	inline static int32_t get_offset_of_lookAtObject_29() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___lookAtObject_29)); }
	inline FsmGameObject_t1697147867 * get_lookAtObject_29() const { return ___lookAtObject_29; }
	inline FsmGameObject_t1697147867 ** get_address_of_lookAtObject_29() { return &___lookAtObject_29; }
	inline void set_lookAtObject_29(FsmGameObject_t1697147867 * value)
	{
		___lookAtObject_29 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtObject_29, value);
	}

	inline static int32_t get_offset_of_lookAtVector_30() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___lookAtVector_30)); }
	inline FsmVector3_t533912882 * get_lookAtVector_30() const { return ___lookAtVector_30; }
	inline FsmVector3_t533912882 ** get_address_of_lookAtVector_30() { return &___lookAtVector_30; }
	inline void set_lookAtVector_30(FsmVector3_t533912882 * value)
	{
		___lookAtVector_30 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtVector_30, value);
	}

	inline static int32_t get_offset_of_lookTime_31() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___lookTime_31)); }
	inline FsmFloat_t2134102846 * get_lookTime_31() const { return ___lookTime_31; }
	inline FsmFloat_t2134102846 ** get_address_of_lookTime_31() { return &___lookTime_31; }
	inline void set_lookTime_31(FsmFloat_t2134102846 * value)
	{
		___lookTime_31 = value;
		Il2CppCodeGenWriteBarrier(&___lookTime_31, value);
	}

	inline static int32_t get_offset_of_axis_32() { return static_cast<int32_t>(offsetof(iTweenMoveAdd_t1025491740, ___axis_32)); }
	inline int32_t get_axis_32() const { return ___axis_32; }
	inline int32_t* get_address_of_axis_32() { return &___axis_32; }
	inline void set_axis_32(int32_t value)
	{
		___axis_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
