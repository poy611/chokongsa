﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JsonFx.Json.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t3884844371;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonFx.Json.JsonWriterSettings
struct  JsonWriterSettings_t323204516  : public Il2CppObject
{
public:
	// JsonFx.Json.WriteDelegate`1<System.DateTime> JsonFx.Json.JsonWriterSettings::dateTimeSerializer
	WriteDelegate_1_t3884844371 * ___dateTimeSerializer_0;
	// System.Int32 JsonFx.Json.JsonWriterSettings::maxDepth
	int32_t ___maxDepth_1;
	// System.String JsonFx.Json.JsonWriterSettings::newLine
	String_t* ___newLine_2;
	// System.Boolean JsonFx.Json.JsonWriterSettings::prettyPrint
	bool ___prettyPrint_3;
	// System.String JsonFx.Json.JsonWriterSettings::tab
	String_t* ___tab_4;
	// System.String JsonFx.Json.JsonWriterSettings::typeHintName
	String_t* ___typeHintName_5;
	// System.Boolean JsonFx.Json.JsonWriterSettings::useXmlSerializationAttributes
	bool ___useXmlSerializationAttributes_6;

public:
	inline static int32_t get_offset_of_dateTimeSerializer_0() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___dateTimeSerializer_0)); }
	inline WriteDelegate_1_t3884844371 * get_dateTimeSerializer_0() const { return ___dateTimeSerializer_0; }
	inline WriteDelegate_1_t3884844371 ** get_address_of_dateTimeSerializer_0() { return &___dateTimeSerializer_0; }
	inline void set_dateTimeSerializer_0(WriteDelegate_1_t3884844371 * value)
	{
		___dateTimeSerializer_0 = value;
		Il2CppCodeGenWriteBarrier(&___dateTimeSerializer_0, value);
	}

	inline static int32_t get_offset_of_maxDepth_1() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___maxDepth_1)); }
	inline int32_t get_maxDepth_1() const { return ___maxDepth_1; }
	inline int32_t* get_address_of_maxDepth_1() { return &___maxDepth_1; }
	inline void set_maxDepth_1(int32_t value)
	{
		___maxDepth_1 = value;
	}

	inline static int32_t get_offset_of_newLine_2() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___newLine_2)); }
	inline String_t* get_newLine_2() const { return ___newLine_2; }
	inline String_t** get_address_of_newLine_2() { return &___newLine_2; }
	inline void set_newLine_2(String_t* value)
	{
		___newLine_2 = value;
		Il2CppCodeGenWriteBarrier(&___newLine_2, value);
	}

	inline static int32_t get_offset_of_prettyPrint_3() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___prettyPrint_3)); }
	inline bool get_prettyPrint_3() const { return ___prettyPrint_3; }
	inline bool* get_address_of_prettyPrint_3() { return &___prettyPrint_3; }
	inline void set_prettyPrint_3(bool value)
	{
		___prettyPrint_3 = value;
	}

	inline static int32_t get_offset_of_tab_4() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___tab_4)); }
	inline String_t* get_tab_4() const { return ___tab_4; }
	inline String_t** get_address_of_tab_4() { return &___tab_4; }
	inline void set_tab_4(String_t* value)
	{
		___tab_4 = value;
		Il2CppCodeGenWriteBarrier(&___tab_4, value);
	}

	inline static int32_t get_offset_of_typeHintName_5() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___typeHintName_5)); }
	inline String_t* get_typeHintName_5() const { return ___typeHintName_5; }
	inline String_t** get_address_of_typeHintName_5() { return &___typeHintName_5; }
	inline void set_typeHintName_5(String_t* value)
	{
		___typeHintName_5 = value;
		Il2CppCodeGenWriteBarrier(&___typeHintName_5, value);
	}

	inline static int32_t get_offset_of_useXmlSerializationAttributes_6() { return static_cast<int32_t>(offsetof(JsonWriterSettings_t323204516, ___useXmlSerializationAttributes_6)); }
	inline bool get_useXmlSerializationAttributes_6() const { return ___useXmlSerializationAttributes_6; }
	inline bool* get_address_of_useXmlSerializationAttributes_6() { return &___useXmlSerializationAttributes_6; }
	inline void set_useXmlSerializationAttributes_6(bool value)
	{
		___useXmlSerializationAttributes_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
