﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ingredient
struct Ingredient_t1787055601;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Ingredient::.ctor()
extern "C"  void Ingredient__ctor_m3589368266 (Ingredient_t1787055601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ingredient::GetIngredientByNum(System.Int32)
extern "C"  String_t* Ingredient_GetIngredientByNum_m930423446 (Ingredient_t1787055601 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ingredient::GetIngredientUnit(System.Int32)
extern "C"  String_t* Ingredient_GetIngredientUnit_m1214740577 (Ingredient_t1787055601 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
