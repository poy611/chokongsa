﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B
struct U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::.ctor()
extern "C"  void U3CHandleConnectedSetChangedU3Ec__AnonStorey7B__ctor_m3666110291 (U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::<>m__60(System.String)
extern "C"  bool U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_U3CU3Em__60_m1541585656 (U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104 * __this, String_t* ___peerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::<>m__61(System.String)
extern "C"  bool U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_U3CU3Em__61_m1031051479 (U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104 * __this, String_t* ___peerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::<>m__62(System.String)
extern "C"  bool U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_U3CU3Em__62_m520517302 (U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104 * __this, String_t* ___peer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey7B::<>m__63(System.String)
extern "C"  bool U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_U3CU3Em__63_m9983125 (U3CHandleConnectedSetChangedU3Ec__AnonStorey7B_t1081038104 * __this, String_t* ___peer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
