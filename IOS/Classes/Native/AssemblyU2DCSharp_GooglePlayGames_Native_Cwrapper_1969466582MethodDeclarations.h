﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetDescription_m3675148389 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___description1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Construct()
extern "C"  IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Construct_m2235991890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetPlayedTime(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetPlayedTime_m1518428836 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___played_time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetCoverImageFromPngData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetCoverImageFromPngData_m841557980 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___png_data1, UIntPtr_t  ___png_data_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Create_m2813109216 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Dispose_m2684579340 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
