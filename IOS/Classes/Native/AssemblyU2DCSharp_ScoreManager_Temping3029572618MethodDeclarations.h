﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Temping
struct ScoreManager_Temping_t3029572618;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Temping::.ctor()
extern "C"  void ScoreManager_Temping__ctor_m599960017 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::Start()
extern "C"  void ScoreManager_Temping_Start_m3842065105 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::ReadyAndStart()
extern "C"  void ScoreManager_Temping_ReadyAndStart_m1125869821 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Temping::REandSt()
extern "C"  Il2CppObject * ScoreManager_Temping_REandSt_m4176850764 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Temping::TimerCheck()
extern "C"  Il2CppObject * ScoreManager_Temping_TimerCheck_m1109030942 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScoreManager_Temping::TimeEnd()
extern "C"  Il2CppObject * ScoreManager_Temping_TimeEnd_m653211509 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::Confirm()
extern "C"  void ScoreManager_Temping_Confirm_m4174217775 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::EndOfTemping()
extern "C"  void ScoreManager_Temping_EndOfTemping_m311035759 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::ButtonPassClick()
extern "C"  void ScoreManager_Temping_ButtonPassClick_m1576181012 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::ButtonRetryClick()
extern "C"  void ScoreManager_Temping_ButtonRetryClick_m2574026341 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Temping::EndOfExtracting()
extern "C"  void ScoreManager_Temping_EndOfExtracting_m1906868770 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScoreManager_Temping::SwitchErrorNum(System.Single)
extern "C"  int32_t ScoreManager_Temping_SwitchErrorNum_m387527416 (ScoreManager_Temping_t3029572618 * __this, float ___errorRate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
