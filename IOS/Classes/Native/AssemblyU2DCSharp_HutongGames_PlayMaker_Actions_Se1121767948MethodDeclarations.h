﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmFloat
struct SetFsmFloat_t1121767948;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::.ctor()
extern "C"  void SetFsmFloat__ctor_m2919737002 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::Reset()
extern "C"  void SetFsmFloat_Reset_m566169943 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnEnter()
extern "C"  void SetFsmFloat_OnEnter_m645343233 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::DoSetFsmFloat()
extern "C"  void SetFsmFloat_DoSetFsmFloat_m3866498555 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnUpdate()
extern "C"  void SetFsmFloat_OnUpdate_m1959330402 (SetFsmFloat_t1121767948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
