﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConstforMinigame
struct ConstforMinigame_t2789090703;

#include "codegen/il2cpp-codegen.h"

// System.Void ConstforMinigame::.ctor()
extern "C"  void ConstforMinigame__ctor_m2050413036 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::.ctor(System.Int32,System.Int32)
extern "C"  void ConstforMinigame__ctor_m1546601146 (ConstforMinigame_t2789090703 * __this, int32_t ___i0, int32_t ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::.cctor()
extern "C"  void ConstforMinigame__cctor_m2951165761 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateTemping(System.Int32)
extern "C"  void ConstforMinigame_locateTemping_m2603226967 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateGrinding(System.Int32)
extern "C"  void ConstforMinigame_locateGrinding_m3918212105 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateLobby(System.Int32)
extern "C"  void ConstforMinigame_locateLobby_m3268637535 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateIcing(System.Int32)
extern "C"  void ConstforMinigame_locateIcing_m2567681169 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateVariation(System.Int32)
extern "C"  void ConstforMinigame_locateVariation_m1578997596 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConstforMinigame::locateMiniGame(System.Int32)
extern "C"  void ConstforMinigame_locateMiniGame_m3255038308 (ConstforMinigame_t2789090703 * __this, int32_t ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConstforMinigame::get_Besttime()
extern "C"  float ConstforMinigame_get_Besttime_m1687094942 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConstforMinigame::get_Limittime()
extern "C"  float ConstforMinigame_get_Limittime_m4054875741 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConstforMinigame::get_BestErrorPercent()
extern "C"  float ConstforMinigame_get_BestErrorPercent_m3513216270 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConstforMinigame::get_LimitErrorPercent()
extern "C"  float ConstforMinigame_get_LimitErrorPercent_m472954317 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConstforMinigame::get_Errorpenalty()
extern "C"  float ConstforMinigame_get_Errorpenalty_m3689665390 (ConstforMinigame_t2789090703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ConstforMinigame ConstforMinigame::getSingleton(System.Int32,System.Int32)
extern "C"  ConstforMinigame_t2789090703 * ConstforMinigame_getSingleton_m1109573803 (Il2CppObject * __this /* static, unused */, int32_t ___index0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
