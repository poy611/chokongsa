﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1702563344.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct  SetBackgroundColor_t3440566907  : public ComponentAction_1_t1702563344
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetBackgroundColor::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetBackgroundColor::backgroundColor
	FsmColor_t2131419205 * ___backgroundColor_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetBackgroundColor::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_backgroundColor_14() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___backgroundColor_14)); }
	inline FsmColor_t2131419205 * get_backgroundColor_14() const { return ___backgroundColor_14; }
	inline FsmColor_t2131419205 ** get_address_of_backgroundColor_14() { return &___backgroundColor_14; }
	inline void set_backgroundColor_14(FsmColor_t2131419205 * value)
	{
		___backgroundColor_14 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundColor_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t3440566907, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
