﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.MethodBase
struct MethodBase_t318515428;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t1191579649  : public Il2CppObject
{
public:
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::c
	ConstructorInfo_t4136801618 * ___c_0;
	// System.Reflection.MethodBase Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass3_0::method
	MethodBase_t318515428 * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1191579649, ___c_0)); }
	inline ConstructorInfo_t4136801618 * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t4136801618 ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t4136801618 * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier(&___c_0, value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1191579649, ___method_1)); }
	inline MethodBase_t318515428 * get_method_1() const { return ___method_1; }
	inline MethodBase_t318515428 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t318515428 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
