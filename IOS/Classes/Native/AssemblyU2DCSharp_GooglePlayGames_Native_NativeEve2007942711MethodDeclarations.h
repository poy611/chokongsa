﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey5E
struct U3CFetchEventU3Ec__AnonStorey5E_t2007942711;
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
struct FetchResponse_t3476068802;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_E3476068802.h"

// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey5E::.ctor()
extern "C"  void U3CFetchEventU3Ec__AnonStorey5E__ctor_m867832388 (U3CFetchEventU3Ec__AnonStorey5E_t2007942711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey5E::<>m__3B(GooglePlayGames.Native.PInvoke.EventManager/FetchResponse)
extern "C"  void U3CFetchEventU3Ec__AnonStorey5E_U3CU3Em__3B_m3182920564 (U3CFetchEventU3Ec__AnonStorey5E_t2007942711 * __this, FetchResponse_t3476068802 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
