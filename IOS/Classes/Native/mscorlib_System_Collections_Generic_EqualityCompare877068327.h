﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompar1537128541.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct  DefaultComparer_t877068327  : public EqualityComparer_1_t1537128541
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
