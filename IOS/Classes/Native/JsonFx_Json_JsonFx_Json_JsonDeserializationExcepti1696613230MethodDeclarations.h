﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonFx.Json.JsonDeserializationException
struct JsonDeserializationException_t1696613230;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void JsonFx.Json.JsonDeserializationException::.ctor(System.String,System.Int32)
extern "C"  void JsonDeserializationException__ctor_m393045188 (JsonDeserializationException_t1696613230 * __this, String_t* ___message0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
