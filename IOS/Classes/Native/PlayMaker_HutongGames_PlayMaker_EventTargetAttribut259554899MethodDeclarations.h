﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.EventTargetAttribute
struct EventTargetAttribute_t259554899;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3998278035.h"

// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.EventTargetAttribute::get_Target()
extern "C"  int32_t EventTargetAttribute_get_Target_m664849036 (EventTargetAttribute_t259554899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.EventTargetAttribute::.ctor(HutongGames.PlayMaker.FsmEventTarget/EventTarget)
extern "C"  void EventTargetAttribute__ctor_m3289614509 (EventTargetAttribute_t259554899 * __this, int32_t ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
