﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3222482060(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1980846253 *, String_t*, Achievement_t1261647177 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Key()
#define KeyValuePair_2_get_Key_m1903632860(__this, method) ((  String_t* (*) (KeyValuePair_2_t1980846253 *, const MethodInfo*))KeyValuePair_2_get_Key_m2940899609_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m356353565(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1980846253 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Value()
#define KeyValuePair_2_get_Value_m1856903964(__this, method) ((  Achievement_t1261647177 * (*) (KeyValuePair_2_t1980846253 *, const MethodInfo*))KeyValuePair_2_get_Value_m4250204908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2231272349(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1980846253 *, Achievement_t1261647177 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::ToString()
#define KeyValuePair_2_ToString_m1846790245(__this, method) ((  String_t* (*) (KeyValuePair_2_t1980846253 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
