﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfoType
struct  UserInfoType_t3869313555  : public Il2CppObject
{
public:
	// System.String UserInfoType::userId
	String_t* ___userId_0;
	// System.String UserInfoType::username
	String_t* ___username_1;

public:
	inline static int32_t get_offset_of_userId_0() { return static_cast<int32_t>(offsetof(UserInfoType_t3869313555, ___userId_0)); }
	inline String_t* get_userId_0() const { return ___userId_0; }
	inline String_t** get_address_of_userId_0() { return &___userId_0; }
	inline void set_userId_0(String_t* value)
	{
		___userId_0 = value;
		Il2CppCodeGenWriteBarrier(&___userId_0, value);
	}

	inline static int32_t get_offset_of_username_1() { return static_cast<int32_t>(offsetof(UserInfoType_t3869313555, ___username_1)); }
	inline String_t* get_username_1() const { return ___username_1; }
	inline String_t** get_address_of_username_1() { return &___username_1; }
	inline void set_username_1(String_t* value)
	{
		___username_1 = value;
		Il2CppCodeGenWriteBarrier(&___username_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
