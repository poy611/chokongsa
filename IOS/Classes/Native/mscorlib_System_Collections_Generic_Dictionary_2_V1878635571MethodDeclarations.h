﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct ValueCollection_t1878635571;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t3178029858;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct IEnumerator_1_t1645582438;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t360142240;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl4028684685.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1109863266.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2995005505_gshared (ValueCollection_t1878635571 * __this, Dictionary_2_t3178029858 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2995005505(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1878635571 *, Dictionary_2_t3178029858 *, const MethodInfo*))ValueCollection__ctor_m2995005505_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2737014737_gshared (ValueCollection_t1878635571 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2737014737(__this, ___item0, method) ((  void (*) (ValueCollection_t1878635571 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2737014737_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m904922394_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m904922394(__this, method) ((  void (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m904922394_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2196486841_gshared (ValueCollection_t1878635571 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2196486841(__this, ___item0, method) ((  bool (*) (ValueCollection_t1878635571 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2196486841_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m280374686_gshared (ValueCollection_t1878635571 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m280374686(__this, ___item0, method) ((  bool (*) (ValueCollection_t1878635571 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m280374686_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1297137114_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1297137114(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1297137114_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1826524574_gshared (ValueCollection_t1878635571 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1826524574(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1878635571 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1826524574_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4074147117_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4074147117(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4074147117_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m471662700_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m471662700(__this, method) ((  bool (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m471662700_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4079978572_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4079978572(__this, method) ((  bool (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4079978572_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1521801150_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1521801150(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1521801150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1015161928_gshared (ValueCollection_t1878635571 * __this, ParticipantStatusU5BU5D_t360142240* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1015161928(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1878635571 *, ParticipantStatusU5BU5D_t360142240*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1015161928_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
extern "C"  Enumerator_t1109863266  ValueCollection_GetEnumerator_m3664584369_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3664584369(__this, method) ((  Enumerator_t1109863266  (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_GetEnumerator_m3664584369_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3163334534_gshared (ValueCollection_t1878635571 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3163334534(__this, method) ((  int32_t (*) (ValueCollection_t1878635571 *, const MethodInfo*))ValueCollection_get_Count_m3163334534_gshared)(__this, method)
