﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerExit
struct PlayMakerTriggerExit_t3495223174;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void PlayMakerTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerExit_OnTriggerExit_m3825211201 (PlayMakerTriggerExit_t3495223174 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerExit::.ctor()
extern "C"  void PlayMakerTriggerExit__ctor_m3906372759 (PlayMakerTriggerExit_t3495223174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
