﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>
struct ValueCollection_t895312890;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t2194707177;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t1936533030;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va126540585.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3882881716_gshared (ValueCollection_t895312890 * __this, Dictionary_2_t2194707177 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3882881716(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t895312890 *, Dictionary_2_t2194707177 *, const MethodInfo*))ValueCollection__ctor_m3882881716_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3735487230_gshared (ValueCollection_t895312890 * __this, uint32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3735487230(__this, ___item0, method) ((  void (*) (ValueCollection_t895312890 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3735487230_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2963414727_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2963414727(__this, method) ((  void (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2963414727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1634252776_gshared (ValueCollection_t895312890 * __this, uint32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1634252776(__this, ___item0, method) ((  bool (*) (ValueCollection_t895312890 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1634252776_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4150176397_gshared (ValueCollection_t895312890 * __this, uint32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4150176397(__this, ___item0, method) ((  bool (*) (ValueCollection_t895312890 *, uint32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4150176397_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m288523477_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m288523477(__this, method) ((  Il2CppObject* (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m288523477_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2769433355_gshared (ValueCollection_t895312890 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2769433355(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t895312890 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2769433355_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26128838_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26128838(__this, method) ((  Il2CppObject * (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26128838_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4204395931_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4204395931(__this, method) ((  bool (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4204395931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060686331_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060686331(__this, method) ((  bool (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2060686331_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3168702375_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3168702375(__this, method) ((  Il2CppObject * (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3168702375_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3021523451_gshared (ValueCollection_t895312890 * __this, UInt32U5BU5D_t3230734560* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3021523451(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t895312890 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3021523451_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::GetEnumerator()
extern "C"  Enumerator_t126540585  ValueCollection_GetEnumerator_m1405102622_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1405102622(__this, method) ((  Enumerator_t126540585  (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_GetEnumerator_m1405102622_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1341366081_gshared (ValueCollection_t895312890 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1341366081(__this, method) ((  int32_t (*) (ValueCollection_t895312890 *, const MethodInfo*))ValueCollection_get_Count_m1341366081_gshared)(__this, method)
