﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionCompare
struct QuaternionCompare_t2711346229;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::.ctor()
extern "C"  void QuaternionCompare__ctor_m2585504929 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::Reset()
extern "C"  void QuaternionCompare_Reset_m231937870 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnEnter()
extern "C"  void QuaternionCompare_OnEnter_m1570868280 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnUpdate()
extern "C"  void QuaternionCompare_OnUpdate_m585835787 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnLateUpdate()
extern "C"  void QuaternionCompare_OnLateUpdate_m1908239697 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnFixedUpdate()
extern "C"  void QuaternionCompare_OnFixedUpdate_m4196801085 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::DoQuatCompare()
extern "C"  void QuaternionCompare_DoQuatCompare_m1897904386 (QuaternionCompare_t2711346229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
