﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayAddRange
struct ArrayAddRange_t613168227;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::.ctor()
extern "C"  void ArrayAddRange__ctor_m4000603187 (ArrayAddRange_t613168227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::Reset()
extern "C"  void ArrayAddRange_Reset_m1647036128 (ArrayAddRange_t613168227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::OnEnter()
extern "C"  void ArrayAddRange_OnEnter_m4270628682 (ArrayAddRange_t613168227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::DoAddRange()
extern "C"  void ArrayAddRange_DoAddRange_m1318692792 (ArrayAddRange_t613168227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
