﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_Valid_m1111996920 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerStats_PlayerStats_Dispose_m1849030439 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasAverageSessionLength(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasAverageSessionLength_m4033552885 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_AverageSessionLength(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_AverageSessionLength_m4087324967 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasChurnProbability(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasChurnProbability_m2631311647 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_ChurnProbability(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_ChurnProbability_m2206107985 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasDaysSinceLastPlayed(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasDaysSinceLastPlayed_m2056999314 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_DaysSinceLastPlayed(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_DaysSinceLastPlayed_m123098082 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasNumberOfPurchases(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasNumberOfPurchases_m3293499896 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_NumberOfPurchases(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_NumberOfPurchases_m3246794824 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasNumberOfSessions(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasNumberOfSessions_m3953189427 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_NumberOfSessions(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_NumberOfSessions_m3120398819 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasSessionPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasSessionPercentile_m887392441 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_SessionPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_SessionPercentile_m590980807 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasSpendPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasSpendPercentile_m704060801 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_SpendPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_SpendPercentile_m1937270543 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
