﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3308144514;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t1868568199;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t977971617;
// System.Object
struct Il2CppObject;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3956694567;
// YoutubeExtractor.VideoInfo
struct VideoInfo_t2099471191;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo>
struct IEnumerable_1_t1105416852;
// System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo>
struct List_1_t3467656743;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>
struct IEnumerable_1_t4226351297;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Func`2<YoutubeExtractor.VideoInfo,System.Boolean>
struct Func_2_t3856024784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t785513668;
// System.Exception
struct Exception_t3991598821;
// YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12
struct U3CU3Ec__DisplayClass12_t2936911561;
// YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15
struct U3CU3Ec__DisplayClass15_t2936911564;
// YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1
struct U3CExtractDownloadUrlsU3Ed__1_t1414174443;
// System.Collections.Generic.IEnumerator`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>
struct IEnumerator_1_t2837303389;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo
struct ExtractionInfo_t925438340;
// System.Uri
struct Uri_t1116831938;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t405523272;
// YoutubeExtractor.VideoNotAvailableException
struct VideoNotAvailableException_t1343733958;
// YoutubeExtractor.YoutubeParseException
struct YoutubeParseException_t1485001709;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "YoutubeUnity_U3CModuleU3E86524790.h"
#include "YoutubeUnity_U3CModuleU3E86524790MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_AdaptiveType3319469144.h"
#include "YoutubeUnity_YoutubeExtractor_AdaptiveType3319469144MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_AudioType955051966.h"
#include "YoutubeUnity_YoutubeExtractor_AudioType955051966MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_Decipherer1774992449.h"
#include "YoutubeUnity_YoutubeExtractor_Decipherer1774992449MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_HttpHelper4274725662MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex2161232213MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollecti982584267MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Capture754001812MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable839044124MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Group2151468941MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_System_Text_RegularExpressions_Match2156507859.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_Text_RegularExpressions_Match2156507859MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollecti982584267.h"
#include "System_System_Text_RegularExpressions_Group2151468941.h"
#include "mscorlib_System_Int321153838500.h"
#include "System_System_Text_RegularExpressions_RegexOptions3066443743.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "System_Core_System_Func_3_gen977971617MethodDeclarations.h"
#include "mscorlib_System_StringSplitOptions3126613641.h"
#include "System_Core_System_Func_3_gen977971617.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "System_System_Text_RegularExpressions_Regex2161232213.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver3917844667.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver3917844667MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_YoutubeParseExceptio1485001709MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "YoutubeUnity_YoutubeExtractor_YoutubeParseExceptio1485001709.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3467656743MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3467656743.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3487329513.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3487329513MethodDeclarations.h"
#include "System_System_Net_WebException2331648373.h"
#include "YoutubeUnity_YoutubeExtractor_VideoNotAvailableExc1343733958.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "System_System_Uri1116831938.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_1414174443MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_1414174443.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject1798544199MethodDeclarations.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_VideoNotAvailableExc1343733958MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_E925438340MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_E925438340.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911561MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3856024784MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911561.h"
#include "System_Core_System_Func_2_gen3856024784.h"
#include "System_Xml_System_Xml_XmlDocument730752740MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode856910923MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911564MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument730752740.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"
#include "System_Xml_System_Xml_XmlNodeList991860617.h"
#include "System_Xml_System_Xml_XmlElement280387747.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver_2936911564.h"
#include "System_Xml_System_Xml_XmlNameTable1216706026.h"
#include "System_Xml_System_Xml_XmlNodeList991860617MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlElement280387747MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread1973216770MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread1973216770.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "YoutubeUnity_YoutubeExtractor_HttpHelper4274725662.h"
#include "System_System_Net_WebClient220232441MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2012439129MethodDeclarations.h"
#include "System_System_Net_WebClient220232441.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge827649927.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763MethodDeclarations.h"

// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Skip_TisIl2CppObject_m61313731_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Skip_TisIl2CppObject_m61313731(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Skip_TisIl2CppObject_m61313731_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Skip_TisString_t_m1099163441(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Skip_TisIl2CppObject_m61313731_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Take_TisIl2CppObject_m996232891_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Take_TisIl2CppObject_m996232891(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisIl2CppObject_m996232891_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisString_t_m2034082601(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisIl2CppObject_m996232891_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Reverse_TisChar_t2862622538_m47265344_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Reverse_TisChar_t2862622538_m47265344(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Reverse_TisChar_t2862622538_m47265344_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  CharU5BU5D_t3324145743* Enumerable_ToArray_TisChar_t2862622538_m2226225303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisChar_t2862622538_m2226225303(__this /* static, unused */, p0, method) ((  CharU5BU5D_t3324145743* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisChar_t2862622538_m2226225303_gshared)(__this /* static, unused */, p0, method)
// !!1 System.Linq.Enumerable::Aggregate<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!1,System.Func`3<!!1,!!0,!!1>)
extern "C"  Il2CppObject * Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m432075151_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, Func_3_t3956694567 * p2, const MethodInfo* method);
#define Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m432075151(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, Func_3_t3956694567 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m432075151_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!1 System.Linq.Enumerable::Aggregate<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,!!1,System.Func`3<!!1,!!0,!!1>)
#define Enumerable_Aggregate_TisString_t_TisString_t_m170222315(__this /* static, unused */, p0, p1, p2, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, String_t*, Func_3_t977971617 *, const MethodInfo*))Enumerable_Aggregate_TisIl2CppObject_TisIl2CppObject_m432075151_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1244034627 * Enumerable_ToList_TisIl2CppObject_m3795766066_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3795766066(__this /* static, unused */, p0, method) ((  List_1_t1244034627 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3795766066_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<YoutubeExtractor.VideoInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisVideoInfo_t2099471191_m2202490119(__this /* static, unused */, p0, method) ((  List_1_t3467656743 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3795766066_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::SingleOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_SingleOrDefault_TisIl2CppObject_m2283275255_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t785513668 * p1, const MethodInfo* method);
#define Enumerable_SingleOrDefault_TisIl2CppObject_m2283275255(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t785513668 *, const MethodInfo*))Enumerable_SingleOrDefault_TisIl2CppObject_m2283275255_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::SingleOrDefault<YoutubeExtractor.VideoInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470(__this /* static, unused */, p0, p1, method) ((  VideoInfo_t2099471191 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3856024784 *, const MethodInfo*))Enumerable_SingleOrDefault_TisIl2CppObject_m2283275255_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String YoutubeExtractor.Decipherer::DecipherWithVersion(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3308144514_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1919096606_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Skip_TisString_t_m1099163441_MethodInfo_var;
extern const MethodInfo* Enumerable_Take_TisString_t_m2034082601_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1264886162;
extern Il2CppCodeGenString* _stringLiteral452039338;
extern Il2CppCodeGenString* _stringLiteral36;
extern Il2CppCodeGenString* _stringLiteral92;
extern Il2CppCodeGenString* _stringLiteral1213608675;
extern Il2CppCodeGenString* _stringLiteral0;
extern Il2CppCodeGenString* _stringLiteral4106260251;
extern Il2CppCodeGenString* _stringLiteral1942042701;
extern Il2CppCodeGenString* _stringLiteral3236386296;
extern Il2CppCodeGenString* _stringLiteral187587420;
extern Il2CppCodeGenString* _stringLiteral119;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral115;
extern Il2CppCodeGenString* _stringLiteral3566;
extern const uint32_t Decipherer_DecipherWithVersion_m2393510710_MetadataUsageId;
extern "C"  String_t* Decipherer_DecipherWithVersion_m2393510710 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___cipherVersion1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_DecipherWithVersion_m2393510710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	StringU5BU5D_t4054002952* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Match_t2156507859 * V_15 = NULL;
	Match_t2156507859 * V_16 = NULL;
	String_t* V_17 = NULL;
	String_t* V_18 = NULL;
	bool V_19 = false;
	CharU5BU5D_t3324145743* V_20 = NULL;
	Il2CppObject* V_21 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B8_0 = 0;
	int32_t G_B28_0 = 0;
	int32_t G_B33_0 = 0;
	{
		String_t* L_0 = ___cipherVersion1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1264886162, L_0, /*hidden argument*/NULL);
		String_t* L_2 = HttpHelper_DownloadString_m189277324(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = _stringLiteral452039338;
		String_t* L_3 = V_0;
		String_t* L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
		Match_t2156507859 * L_5 = Regex_Match_m2072979584(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GroupCollection_t982584267 * L_6 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_5);
		NullCheck(L_6);
		Group_t2151468941 * L_7 = GroupCollection_get_Item_m180115662(L_6, 1, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Capture_get_Value_m2241099810(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = V_2;
		NullCheck(L_9);
		bool L_10 = String_Contains_m3032019141(L_9, _stringLiteral36, /*hidden argument*/NULL);
		V_19 = (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_19;
		if (L_11)
		{
			goto IL_0050;
		}
	}
	{
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral92, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
	}

IL_0050:
	{
		String_t* L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, L_14, _stringLiteral1213608675, /*hidden argument*/NULL);
		V_3 = L_15;
		String_t* L_16 = V_0;
		String_t* L_17 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
		Match_t2156507859 * L_18 = Regex_Match_m563114696(NULL /*static, unused*/, L_16, L_17, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Capture_get_Value_m2241099810(L_18, /*hidden argument*/NULL);
		V_20 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		CharU5BU5D_t3324145743* L_20 = V_20;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
		CharU5BU5D_t3324145743* L_21 = V_20;
		NullCheck(L_19);
		StringU5BU5D_t4054002952* L_22 = String_Split_m290179486(L_19, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		V_5 = _stringLiteral0;
		V_6 = _stringLiteral0;
		V_7 = _stringLiteral0;
		V_8 = _stringLiteral0;
		StringU5BU5D_t4054002952* L_23 = V_4;
		Il2CppObject* L_24 = Enumerable_Skip_TisString_t_m1099163441(NULL /*static, unused*/, ((Il2CppObject*)Castclass(L_23, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var)), 1, /*hidden argument*/Enumerable_Skip_TisString_t_m1099163441_MethodInfo_var);
		StringU5BU5D_t4054002952* L_25 = V_4;
		NullCheck(L_25);
		Il2CppObject* L_26 = Enumerable_Take_TisString_t_m2034082601(NULL /*static, unused*/, L_24, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length))))-(int32_t)2)), /*hidden argument*/Enumerable_Take_TisString_t_m2034082601_MethodInfo_var);
		NullCheck(L_26);
		Il2CppObject* L_27 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var, L_26);
		V_21 = L_27;
	}

IL_00bd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_017d;
		}

IL_00c2:
		{
			Il2CppObject* L_28 = V_21;
			NullCheck(L_28);
			String_t* L_29 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t1919096606_il2cpp_TypeInfo_var, L_28);
			V_9 = L_29;
			String_t* L_30 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_31 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			if (L_31)
			{
				goto IL_00ea;
			}
		}

IL_00d5:
		{
			String_t* L_32 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_33 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			if (L_33)
			{
				goto IL_00ea;
			}
		}

IL_00de:
		{
			String_t* L_34 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_35 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
			G_B8_0 = ((((int32_t)L_35) == ((int32_t)0))? 1 : 0);
			goto IL_00eb;
		}

IL_00ea:
		{
			G_B8_0 = 0;
		}

IL_00eb:
		{
			V_19 = (bool)G_B8_0;
			bool L_36 = V_19;
			if (L_36)
			{
				goto IL_017a;
			}
		}

IL_00f5:
		{
			String_t* L_37 = V_9;
			String_t* L_38 = Decipherer_GetFunctionFromLine_m3216534868(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			V_10 = L_38;
			String_t* L_39 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_40 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral4106260251, L_39, /*hidden argument*/NULL);
			V_11 = L_40;
			String_t* L_41 = V_10;
			String_t* L_42 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1942042701, L_41, /*hidden argument*/NULL);
			V_12 = L_42;
			String_t* L_43 = V_10;
			String_t* L_44 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3236386296, L_43, /*hidden argument*/NULL);
			V_13 = L_44;
			String_t* L_45 = V_0;
			String_t* L_46 = V_11;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			Match_t2156507859 * L_47 = Regex_Match_m2072979584(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
			NullCheck(L_47);
			bool L_48 = Group_get_Success_m139536080(L_47, /*hidden argument*/NULL);
			V_19 = (bool)((((int32_t)L_48) == ((int32_t)0))? 1 : 0);
			bool L_49 = V_19;
			if (L_49)
			{
				goto IL_0143;
			}
		}

IL_013f:
		{
			String_t* L_50 = V_10;
			V_5 = L_50;
		}

IL_0143:
		{
			String_t* L_51 = V_0;
			String_t* L_52 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			Match_t2156507859 * L_53 = Regex_Match_m2072979584(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
			NullCheck(L_53);
			bool L_54 = Group_get_Success_m139536080(L_53, /*hidden argument*/NULL);
			V_19 = (bool)((((int32_t)L_54) == ((int32_t)0))? 1 : 0);
			bool L_55 = V_19;
			if (L_55)
			{
				goto IL_015d;
			}
		}

IL_0159:
		{
			String_t* L_56 = V_10;
			V_6 = L_56;
		}

IL_015d:
		{
			String_t* L_57 = V_0;
			String_t* L_58 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			Match_t2156507859 * L_59 = Regex_Match_m2072979584(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
			NullCheck(L_59);
			bool L_60 = Group_get_Success_m139536080(L_59, /*hidden argument*/NULL);
			V_19 = (bool)((((int32_t)L_60) == ((int32_t)0))? 1 : 0);
			bool L_61 = V_19;
			if (L_61)
			{
				goto IL_0177;
			}
		}

IL_0173:
		{
			String_t* L_62 = V_10;
			V_7 = L_62;
		}

IL_0177:
		{
			goto IL_017c;
		}

IL_017a:
		{
			goto IL_018d;
		}

IL_017c:
		{
		}

IL_017d:
		{
			Il2CppObject* L_63 = V_21;
			NullCheck(L_63);
			bool L_64 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_63);
			V_19 = L_64;
			bool L_65 = V_19;
			if (L_65)
			{
				goto IL_00c2;
			}
		}

IL_018d:
		{
			IL2CPP_LEAVE(0x1A3, FINALLY_018f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_018f;
	}

FINALLY_018f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_66 = V_21;
			V_19 = (bool)((((Il2CppObject*)(Il2CppObject*)L_66) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_67 = V_19;
			if (L_67)
			{
				goto IL_01a2;
			}
		}

IL_019a:
		{
			Il2CppObject* L_68 = V_21;
			NullCheck(L_68);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_68);
		}

IL_01a2:
		{
			IL2CPP_END_FINALLY(399)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(399)
	{
		IL2CPP_JUMP_TBL(0x1A3, IL_01a3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_01a3:
	{
		StringU5BU5D_t4054002952* L_69 = V_4;
		Il2CppObject* L_70 = Enumerable_Skip_TisString_t_m1099163441(NULL /*static, unused*/, ((Il2CppObject*)Castclass(L_69, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var)), 1, /*hidden argument*/Enumerable_Skip_TisString_t_m1099163441_MethodInfo_var);
		StringU5BU5D_t4054002952* L_71 = V_4;
		NullCheck(L_71);
		Il2CppObject* L_72 = Enumerable_Take_TisString_t_m2034082601(NULL /*static, unused*/, L_70, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_71)->max_length))))-(int32_t)2)), /*hidden argument*/Enumerable_Take_TisString_t_m2034082601_MethodInfo_var);
		NullCheck(L_72);
		Il2CppObject* L_73 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var, L_72);
		V_21 = L_73;
	}

IL_01c4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02a7;
		}

IL_01c9:
		{
			Il2CppObject* L_74 = V_21;
			NullCheck(L_74);
			String_t* L_75 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t1919096606_il2cpp_TypeInfo_var, L_74);
			V_14 = L_75;
			String_t* L_76 = V_14;
			String_t* L_77 = Decipherer_GetFunctionFromLine_m3216534868(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
			V_10 = L_77;
			String_t* L_78 = V_14;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			Match_t2156507859 * L_79 = Regex_Match_m2072979584(NULL /*static, unused*/, L_78, _stringLiteral187587420, /*hidden argument*/NULL);
			Match_t2156507859 * L_80 = L_79;
			V_15 = L_80;
			NullCheck(L_80);
			bool L_81 = Group_get_Success_m139536080(L_80, /*hidden argument*/NULL);
			if (!L_81)
			{
				goto IL_0200;
			}
		}

IL_01f2:
		{
			String_t* L_82 = V_10;
			String_t* L_83 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_84 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
			G_B28_0 = ((((int32_t)L_84) == ((int32_t)0))? 1 : 0);
			goto IL_0201;
		}

IL_0200:
		{
			G_B28_0 = 1;
		}

IL_0201:
		{
			V_19 = (bool)G_B28_0;
			bool L_85 = V_19;
			if (L_85)
			{
				goto IL_0231;
			}
		}

IL_0208:
		{
			String_t* L_86 = V_8;
			Match_t2156507859 * L_87 = V_15;
			NullCheck(L_87);
			GroupCollection_t982584267 * L_88 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_87);
			NullCheck(L_88);
			Group_t2151468941 * L_89 = GroupCollection_get_Item_m4186628929(L_88, _stringLiteral100346066, /*hidden argument*/NULL);
			NullCheck(L_89);
			String_t* L_90 = Capture_get_Value_m2241099810(L_89, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_91 = String_Concat_m2933632197(NULL /*static, unused*/, L_86, _stringLiteral119, L_90, _stringLiteral32, /*hidden argument*/NULL);
			V_8 = L_91;
		}

IL_0231:
		{
			String_t* L_92 = V_14;
			IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
			Match_t2156507859 * L_93 = Regex_Match_m2072979584(NULL /*static, unused*/, L_92, _stringLiteral187587420, /*hidden argument*/NULL);
			Match_t2156507859 * L_94 = L_93;
			V_16 = L_94;
			NullCheck(L_94);
			bool L_95 = Group_get_Success_m139536080(L_94, /*hidden argument*/NULL);
			if (!L_95)
			{
				goto IL_0255;
			}
		}

IL_0247:
		{
			String_t* L_96 = V_10;
			String_t* L_97 = V_6;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_98 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_96, L_97, /*hidden argument*/NULL);
			G_B33_0 = ((((int32_t)L_98) == ((int32_t)0))? 1 : 0);
			goto IL_0256;
		}

IL_0255:
		{
			G_B33_0 = 1;
		}

IL_0256:
		{
			V_19 = (bool)G_B33_0;
			bool L_99 = V_19;
			if (L_99)
			{
				goto IL_0286;
			}
		}

IL_025d:
		{
			String_t* L_100 = V_8;
			Match_t2156507859 * L_101 = V_16;
			NullCheck(L_101);
			GroupCollection_t982584267 * L_102 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_101);
			NullCheck(L_102);
			Group_t2151468941 * L_103 = GroupCollection_get_Item_m4186628929(L_102, _stringLiteral100346066, /*hidden argument*/NULL);
			NullCheck(L_103);
			String_t* L_104 = Capture_get_Value_m2241099810(L_103, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_105 = String_Concat_m2933632197(NULL /*static, unused*/, L_100, _stringLiteral115, L_104, _stringLiteral32, /*hidden argument*/NULL);
			V_8 = L_105;
		}

IL_0286:
		{
			String_t* L_106 = V_10;
			String_t* L_107 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_108 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_106, L_107, /*hidden argument*/NULL);
			V_19 = (bool)((((int32_t)L_108) == ((int32_t)0))? 1 : 0);
			bool L_109 = V_19;
			if (L_109)
			{
				goto IL_02a6;
			}
		}

IL_0298:
		{
			String_t* L_110 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_111 = String_Concat_m138640077(NULL /*static, unused*/, L_110, _stringLiteral3566, /*hidden argument*/NULL);
			V_8 = L_111;
		}

IL_02a6:
		{
		}

IL_02a7:
		{
			Il2CppObject* L_112 = V_21;
			NullCheck(L_112);
			bool L_113 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_112);
			V_19 = L_113;
			bool L_114 = V_19;
			if (L_114)
			{
				goto IL_01c9;
			}
		}

IL_02b7:
		{
			IL2CPP_LEAVE(0x2CD, FINALLY_02b9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_02b9;
	}

FINALLY_02b9:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_115 = V_21;
			V_19 = (bool)((((Il2CppObject*)(Il2CppObject*)L_115) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_116 = V_19;
			if (L_116)
			{
				goto IL_02cc;
			}
		}

IL_02c4:
		{
			Il2CppObject* L_117 = V_21;
			NullCheck(L_117);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_117);
		}

IL_02cc:
		{
			IL2CPP_END_FINALLY(697)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(697)
	{
		IL2CPP_JUMP_TBL(0x2CD, IL_02cd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_02cd:
	{
		String_t* L_118 = V_8;
		NullCheck(L_118);
		String_t* L_119 = String_Trim_m1030489823(L_118, /*hidden argument*/NULL);
		V_17 = L_119;
		String_t* L_120 = ___cipher0;
		String_t* L_121 = V_17;
		String_t* L_122 = Decipherer_DecipherWithOperations_m1663412512(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		V_18 = L_122;
		goto IL_02e3;
	}

IL_02e3:
	{
		String_t* L_123 = V_18;
		return L_123;
	}
}
// System.String YoutubeExtractor.Decipherer::ApplyOperation(System.String,System.String)
extern Il2CppClass* IEnumerable_1_t1868568199_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Reverse_TisChar_t2862622538_m47265344_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisChar_t2862622538_m2226225303_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral661095227;
extern const uint32_t Decipherer_ApplyOperation_m1048729765_MetadataUsageId;
extern "C"  String_t* Decipherer_ApplyOperation_m1048729765 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___op1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_ApplyOperation_m1048729765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	Il2CppChar V_3 = 0x0;
	{
		String_t* L_0 = ___op1;
		NullCheck(L_0);
		Il2CppChar L_1 = String_get_Chars_m3015341861(L_0, 0, /*hidden argument*/NULL);
		V_3 = L_1;
		Il2CppChar L_2 = V_3;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_0021;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_003e;
		}
	}
	{
		Il2CppChar L_3 = V_3;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)119))))
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0060;
	}

IL_0021:
	{
		String_t* L_4 = ___cipher0;
		NullCheck(L_4);
		CharU5BU5D_t3324145743* L_5 = String_ToCharArray_m1208288742(L_4, /*hidden argument*/NULL);
		Il2CppObject* L_6 = Enumerable_Reverse_TisChar_t2862622538_m47265344(NULL /*static, unused*/, ((Il2CppObject*)Castclass(L_5, IEnumerable_1_t1868568199_il2cpp_TypeInfo_var)), /*hidden argument*/Enumerable_Reverse_TisChar_t2862622538_m47265344_MethodInfo_var);
		CharU5BU5D_t3324145743* L_7 = Enumerable_ToArray_TisChar_t2862622538_m2226225303(NULL /*static, unused*/, L_6, /*hidden argument*/Enumerable_ToArray_TisChar_t2862622538_m2226225303_MethodInfo_var);
		String_t* L_8 = String_CreateString_m578950865(NULL, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_006b;
	}

IL_003e:
	{
		String_t* L_9 = ___op1;
		int32_t L_10 = Decipherer_GetOpIndex_m2765359674(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = ___cipher0;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m2809233063(L_11, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		goto IL_006b;
	}

IL_004f:
	{
		String_t* L_14 = ___op1;
		int32_t L_15 = Decipherer_GetOpIndex_m2765359674(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		String_t* L_16 = ___cipher0;
		int32_t L_17 = V_1;
		String_t* L_18 = Decipherer_SwapFirstChar_m643924970(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		goto IL_006b;
	}

IL_0060:
	{
		NotImplementedException_t1912495542 * L_19 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_19, _stringLiteral661095227, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_006b:
	{
		String_t* L_20 = V_2;
		return L_20;
	}
}
// System.String YoutubeExtractor.Decipherer::DecipherWithOperations(System.String,System.String)
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3308144514_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t977971617_il2cpp_TypeInfo_var;
extern const MethodInfo* Decipherer_ApplyOperation_m1048729765_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1046079695_MethodInfo_var;
extern const MethodInfo* Enumerable_Aggregate_TisString_t_TisString_t_m170222315_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t Decipherer_DecipherWithOperations_m1663412512_MetadataUsageId;
extern "C"  String_t* Decipherer_DecipherWithOperations_m1663412512 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, String_t* ___operations1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_DecipherWithOperations_m1663412512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t4054002952* V_1 = NULL;
	{
		String_t* L_0 = ___operations1;
		V_1 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)1));
		StringU5BU5D_t4054002952* L_1 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral32);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_2 = V_1;
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_3 = String_Split_m459616251(L_0, L_2, 1, /*hidden argument*/NULL);
		String_t* L_4 = ___cipher0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)Decipherer_ApplyOperation_m1048729765_MethodInfo_var);
		Func_3_t977971617 * L_6 = (Func_3_t977971617 *)il2cpp_codegen_object_new(Func_3_t977971617_il2cpp_TypeInfo_var);
		Func_3__ctor_m1046079695(L_6, NULL, L_5, /*hidden argument*/Func_3__ctor_m1046079695_MethodInfo_var);
		String_t* L_7 = Enumerable_Aggregate_TisString_t_TisString_t_m170222315(NULL /*static, unused*/, ((Il2CppObject*)Castclass(L_3, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var)), L_4, L_6, /*hidden argument*/Enumerable_Aggregate_TisString_t_TisString_t_m170222315_MethodInfo_var);
		V_0 = L_7;
		goto IL_0032;
	}

IL_0032:
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
// System.String YoutubeExtractor.Decipherer::GetFunctionFromLine(System.String)
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3714830733;
extern Il2CppCodeGenString* _stringLiteral4232177395;
extern const uint32_t Decipherer_GetFunctionFromLine_m3216534868_MetadataUsageId;
extern "C"  String_t* Decipherer_GetFunctionFromLine_m3216534868 (Il2CppObject * __this /* static, unused */, String_t* ___currentLine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_GetFunctionFromLine_m3216534868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Regex_t2161232213 * L_0 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
		Regex__ctor_m574010660(L_0, _stringLiteral3714830733, /*hidden argument*/NULL);
		String_t* L_1 = ___currentLine0;
		NullCheck(L_0);
		Match_t2156507859 * L_2 = Regex_Match_m2003175236(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GroupCollection_t982584267 * L_3 = VirtFuncInvoker0< GroupCollection_t982584267 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_2);
		NullCheck(L_3);
		Group_t2151468941 * L_4 = GroupCollection_get_Item_m4186628929(L_3, _stringLiteral4232177395, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Capture_get_Value_m2241099810(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0028;
	}

IL_0028:
	{
		String_t* L_6 = V_0;
		return L_6;
	}
}
// System.Int32 YoutubeExtractor.Decipherer::GetOpIndex(System.String)
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1356720032;
extern Il2CppCodeGenString* _stringLiteral1165;
extern const uint32_t Decipherer_GetOpIndex_m2765359674_MetadataUsageId;
extern "C"  int32_t Decipherer_GetOpIndex_m2765359674 (Il2CppObject * __this /* static, unused */, String_t* ___op0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_GetOpIndex_m2765359674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Regex_t2161232213 * L_0 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
		Regex__ctor_m574010660(L_0, _stringLiteral1356720032, /*hidden argument*/NULL);
		String_t* L_1 = ___op0;
		NullCheck(L_0);
		Match_t2156507859 * L_2 = Regex_Match_m2003175236(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Text.RegularExpressions.Match::Result(System.String) */, L_2, _stringLiteral1165);
		int32_t L_4 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0023;
	}

IL_0023:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.String YoutubeExtractor.Decipherer::SwapFirstChar(System.String,System.Int32)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern const uint32_t Decipherer_SwapFirstChar_m643924970_MetadataUsageId;
extern "C"  String_t* Decipherer_SwapFirstChar_m643924970 (Il2CppObject * __this /* static, unused */, String_t* ___cipher0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Decipherer_SwapFirstChar_m643924970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___cipher0;
		StringBuilder_t243639308 * L_1 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1143895062(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t243639308 * L_2 = V_0;
		String_t* L_3 = ___cipher0;
		int32_t L_4 = ___index1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_set_Chars_m1845996850(L_2, 0, L_5, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_6 = V_0;
		int32_t L_7 = ___index1;
		String_t* L_8 = ___cipher0;
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m3015341861(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_set_Chars_m1845996850(L_6, L_7, L_9, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		V_1 = L_11;
		goto IL_002f;
	}

IL_002f:
	{
		String_t* L_12 = V_1;
		return L_12;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver::DecryptDownloadUrl(YoutubeExtractor.VideoInfo)
extern Il2CppClass* IDictionary_2_t405523272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1073584312;
extern const uint32_t DownloadUrlResolver_DecryptDownloadUrl_m1994299011_MetadataUsageId;
extern "C"  void DownloadUrlResolver_DecryptDownloadUrl_m1994299011 (Il2CppObject * __this /* static, unused */, VideoInfo_t2099471191 * ___videoInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_DecryptDownloadUrl_m1994299011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	{
		VideoInfo_t2099471191 * L_0 = ___videoInfo0;
		NullCheck(L_0);
		String_t* L_1 = VideoInfo_get_DownloadUrl_m2189851804(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_2 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_3, _stringLiteral1073584312);
		V_3 = (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		bool L_5 = V_3;
		if (L_5)
		{
			goto IL_0056;
		}
	}
	{
		Il2CppObject* L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_6, _stringLiteral1073584312);
		V_1 = L_7;
		String_t* L_8 = V_1;
		VideoInfo_t2099471191 * L_9 = ___videoInfo0;
		NullCheck(L_9);
		String_t* L_10 = VideoInfo_get_HtmlPlayerVersion_m1135409505(L_9, /*hidden argument*/NULL);
		String_t* L_11 = DownloadUrlResolver_DecryptSignature_m2435384427(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		VideoInfo_t2099471191 * L_12 = ___videoInfo0;
		VideoInfo_t2099471191 * L_13 = ___videoInfo0;
		NullCheck(L_13);
		String_t* L_14 = VideoInfo_get_DownloadUrl_m2189851804(L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_1;
		String_t* L_16 = V_2;
		NullCheck(L_14);
		String_t* L_17 = String_Replace_m2915759397(L_14, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		VideoInfo_set_DownloadUrl_m2337044983(L_12, L_17, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_18 = ___videoInfo0;
		NullCheck(L_18);
		VideoInfo_set_RequiresDecryption_m3847149662(L_18, (bool)0, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::DecryptSignature(System.String,System.String)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* YoutubeParseException_t1485001709_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2878102576;
extern const uint32_t DownloadUrlResolver_DecryptSignature_m2435384427_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_DecryptSignature_m2435384427 (Il2CppObject * __this /* static, unused */, String_t* ___signature0, String_t* ___htmlPlayerVersion1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_DecryptSignature_m2435384427_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___signature0;
			NullCheck(L_0);
			int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
			V_2 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)((int32_t)81)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_2 = V_2;
			if (L_2)
			{
				goto IL_0018;
			}
		}

IL_0013:
		{
			String_t* L_3 = ___signature0;
			V_1 = L_3;
			goto IL_0030;
		}

IL_0018:
		{
			String_t* L_4 = ___signature0;
			String_t* L_5 = ___htmlPlayerVersion1;
			String_t* L_6 = Decipherer_DecipherWithVersion_m2393510710(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			goto IL_0030;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0022;
		throw e;
	}

CATCH_0022:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t3991598821 *)__exception_local);
		Exception_t3991598821 * L_7 = V_0;
		YoutubeParseException_t1485001709 * L_8 = (YoutubeParseException_t1485001709 *)il2cpp_codegen_object_new(YoutubeParseException_t1485001709_il2cpp_TypeInfo_var);
		YoutubeParseException__ctor_m627752129(L_8, _stringLiteral2878102576, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	} // end catch (depth: 1)

IL_0030:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo> YoutubeExtractor.DownloadUrlResolver::GetDownloadUrls(System.String,System.Boolean)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* WebException_t2331648373_il2cpp_TypeInfo_var;
extern Il2CppClass* VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisVideoInfo_t2099471191_m2202490119_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3637148792_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2489758556_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1869127936_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1731683860_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1151378164;
extern Il2CppCodeGenString* _stringLiteral2198459748;
extern Il2CppCodeGenString* _stringLiteral48779;
extern Il2CppCodeGenString* _stringLiteral364007398;
extern const uint32_t DownloadUrlResolver_GetDownloadUrls_m3448163278_MetadataUsageId;
extern "C"  Il2CppObject* DownloadUrlResolver_GetDownloadUrls_m3448163278 (Il2CppObject * __this /* static, unused */, String_t* ___videoUrl0, bool ___decryptSignature1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetDownloadUrls_m3448163278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	JObject_t1798544199 * V_1 = NULL;
	String_t* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	List_1_t3467656743 * V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	VideoInfo_t2099471191 * V_9 = NULL;
	Exception_t3991598821 * V_10 = NULL;
	Il2CppObject* V_11 = NULL;
	bool V_12 = false;
	Enumerator_t3487329513  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B13_0 = 0;
	int32_t G_B23_0 = 0;
	{
		String_t* L_0 = ___videoUrl0;
		V_12 = (bool)((((int32_t)((((Il2CppObject*)(String_t*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_12;
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_2 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, _stringLiteral1151378164, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		String_t* L_3 = ___videoUrl0;
		bool L_4 = DownloadUrlResolver_TryNormalizeYoutubeUrl_m2637065517(NULL /*static, unused*/, L_3, (&___videoUrl0), /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		V_12 = L_5;
		bool L_6 = V_12;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		ArgumentException_t928607144 * L_7 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_7, _stringLiteral2198459748, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_8 = ___videoUrl0;
			JObject_t1798544199 * L_9 = DownloadUrlResolver_LoadJson_m2844638074(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_1 = L_9;
			JObject_t1798544199 * L_10 = V_1;
			String_t* L_11 = DownloadUrlResolver_GetVideoTitle_m3499604573(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_2 = L_11;
			JObject_t1798544199 * L_12 = V_1;
			Il2CppObject* L_13 = DownloadUrlResolver_ExtractDownloadUrls_m2099554026(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			V_3 = L_13;
			Il2CppObject* L_14 = V_3;
			String_t* L_15 = V_2;
			List_1_t3467656743 * L_16 = DownloadUrlResolver_GetVideoInfos_m3217043869(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			List_1_t3467656743 * L_17 = Enumerable_ToList_TisVideoInfo_t2099471191_m2202490119(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_ToList_TisVideoInfo_t2099471191_m2202490119_MethodInfo_var);
			V_4 = L_17;
			JObject_t1798544199 * L_18 = V_1;
			String_t* L_19 = DownloadUrlResolver_GetDashManifest_m3557462241(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			V_5 = L_19;
			JObject_t1798544199 * L_20 = V_1;
			String_t* L_21 = DownloadUrlResolver_GetHtml5PlayerVersion_m1659753901(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			V_6 = L_21;
			String_t* L_22 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_23 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
			V_12 = L_23;
			bool L_24 = V_12;
			if (L_24)
			{
				goto IL_00c2;
			}
		}

IL_0076:
		{
			String_t* L_25 = V_5;
			String_t* L_26 = DownloadUrlResolver_ExtractSignatureFromManifest_m3925939498(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			V_7 = L_26;
			String_t* L_27 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_28 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			V_12 = L_28;
			bool L_29 = V_12;
			if (L_29)
			{
				goto IL_00b6;
			}
		}

IL_008d:
		{
			String_t* L_30 = V_7;
			String_t* L_31 = V_6;
			String_t* L_32 = DownloadUrlResolver_DecryptSignature_m2435384427(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
			V_8 = L_32;
			String_t* L_33 = V_5;
			String_t* L_34 = V_7;
			String_t* L_35 = V_8;
			NullCheck(L_33);
			String_t* L_36 = String_Replace_m2915759397(L_33, L_34, L_35, /*hidden argument*/NULL);
			NullCheck(L_36);
			String_t* L_37 = String_Replace_m2915759397(L_36, _stringLiteral48779, _stringLiteral364007398, /*hidden argument*/NULL);
			V_5 = L_37;
		}

IL_00b6:
		{
			String_t* L_38 = V_5;
			List_1_t3467656743 * L_39 = V_4;
			String_t* L_40 = V_2;
			DownloadUrlResolver_ParseDashManifest_m3210265877(NULL /*static, unused*/, L_38, L_39, L_40, /*hidden argument*/NULL);
		}

IL_00c2:
		{
			List_1_t3467656743 * L_41 = V_4;
			NullCheck(L_41);
			Enumerator_t3487329513  L_42 = List_1_GetEnumerator_m3637148792(L_41, /*hidden argument*/List_1_GetEnumerator_m3637148792_MethodInfo_var);
			V_13 = L_42;
		}

IL_00cc:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0104;
			}

IL_00ce:
			{
				VideoInfo_t2099471191 * L_43 = Enumerator_get_Current_m2489758556((&V_13), /*hidden argument*/Enumerator_get_Current_m2489758556_MethodInfo_var);
				V_9 = L_43;
				VideoInfo_t2099471191 * L_44 = V_9;
				String_t* L_45 = V_6;
				NullCheck(L_44);
				VideoInfo_set_HtmlPlayerVersion_m3328316562(L_44, L_45, /*hidden argument*/NULL);
				bool L_46 = ___decryptSignature1;
				if (!L_46)
				{
					goto IL_00f1;
				}
			}

IL_00e5:
			{
				VideoInfo_t2099471191 * L_47 = V_9;
				NullCheck(L_47);
				bool L_48 = VideoInfo_get_RequiresDecryption_m4226650415(L_47, /*hidden argument*/NULL);
				G_B13_0 = ((((int32_t)L_48) == ((int32_t)0))? 1 : 0);
				goto IL_00f2;
			}

IL_00f1:
			{
				G_B13_0 = 1;
			}

IL_00f2:
			{
				V_12 = (bool)G_B13_0;
				bool L_49 = V_12;
				if (L_49)
				{
					goto IL_0103;
				}
			}

IL_00f9:
			{
				VideoInfo_t2099471191 * L_50 = V_9;
				DownloadUrlResolver_DecryptDownloadUrl_m1994299011(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
			}

IL_0103:
			{
			}

IL_0104:
			{
				bool L_51 = Enumerator_MoveNext_m1869127936((&V_13), /*hidden argument*/Enumerator_MoveNext_m1869127936_MethodInfo_var);
				V_12 = L_51;
				bool L_52 = V_12;
				if (L_52)
				{
					goto IL_00ce;
				}
			}

IL_0111:
			{
				IL2CPP_LEAVE(0x122, FINALLY_0113);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0113;
		}

FINALLY_0113:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m1731683860((&V_13), /*hidden argument*/Enumerator_Dispose_m1731683860_MethodInfo_var);
			IL2CPP_END_FINALLY(275)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(275)
		{
			IL2CPP_JUMP_TBL(0x122, IL_0122)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_0122:
		{
			List_1_t3467656743 * L_53 = V_4;
			V_11 = L_53;
			goto IL_0161;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0129;
		throw e;
	}

CATCH_0129:
	{ // begin catch(System.Exception)
		{
			V_10 = ((Exception_t3991598821 *)__exception_local);
			Exception_t3991598821 * L_54 = V_10;
			if (((WebException_t2331648373 *)IsInstClass(L_54, WebException_t2331648373_il2cpp_TypeInfo_var)))
			{
				goto IL_0144;
			}
		}

IL_0135:
		{
			Exception_t3991598821 * L_55 = V_10;
			G_B23_0 = ((((int32_t)((!(((Il2CppObject*)(VideoNotAvailableException_t1343733958 *)((VideoNotAvailableException_t1343733958 *)IsInstClass(L_55, VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			goto IL_0145;
		}

IL_0144:
		{
			G_B23_0 = 0;
		}

IL_0145:
		{
			V_12 = (bool)G_B23_0;
			bool L_56 = V_12;
			if (L_56)
			{
				goto IL_014f;
			}
		}

IL_014c:
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_014f:
		{
			Exception_t3991598821 * L_57 = V_10;
			String_t* L_58 = ___videoUrl0;
			DownloadUrlResolver_ThrowYoutubeParseException_m2477564202(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
			goto IL_015b;
		}
	} // end catch (depth: 1)

IL_015b:
	{
		V_11 = (Il2CppObject*)NULL;
		goto IL_0161;
	}

IL_0161:
	{
		Il2CppObject* L_59 = V_11;
		return L_59;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver::TryNormalizeYoutubeUrl(System.String,System.String&)
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t405523272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral414010430;
extern Il2CppCodeGenString* _stringLiteral1726497168;
extern Il2CppCodeGenString* _stringLiteral530285004;
extern Il2CppCodeGenString* _stringLiteral3303222051;
extern Il2CppCodeGenString* _stringLiteral1780815983;
extern Il2CppCodeGenString* _stringLiteral48872;
extern Il2CppCodeGenString* _stringLiteral1234698980;
extern Il2CppCodeGenString* _stringLiteral1494102278;
extern Il2CppCodeGenString* _stringLiteral2263004707;
extern Il2CppCodeGenString* _stringLiteral2263004735;
extern Il2CppCodeGenString* _stringLiteral118;
extern Il2CppCodeGenString* _stringLiteral2126589602;
extern const uint32_t DownloadUrlResolver_TryNormalizeYoutubeUrl_m2637065517_MetadataUsageId;
extern "C"  bool DownloadUrlResolver_TryNormalizeYoutubeUrl_m2637065517 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t** ___normalizedUrl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_TryNormalizeYoutubeUrl_m2637065517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	{
		String_t* L_0 = ___url0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1030489823(L_0, /*hidden argument*/NULL);
		___url0 = L_1;
		String_t* L_2 = ___url0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m2915759397(L_2, _stringLiteral414010430, _stringLiteral1726497168, /*hidden argument*/NULL);
		___url0 = L_3;
		String_t* L_4 = ___url0;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m2915759397(L_4, _stringLiteral530285004, _stringLiteral3303222051, /*hidden argument*/NULL);
		___url0 = L_5;
		String_t* L_6 = ___url0;
		NullCheck(L_6);
		String_t* L_7 = String_Replace_m2915759397(L_6, _stringLiteral1780815983, _stringLiteral1726497168, /*hidden argument*/NULL);
		___url0 = L_7;
		String_t* L_8 = ___url0;
		NullCheck(L_8);
		bool L_9 = String_Contains_m3032019141(L_8, _stringLiteral48872, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		bool L_10 = V_3;
		if (L_10)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_11 = ___url0;
		Uri_t1116831938 * L_12 = (Uri_t1116831938 *)il2cpp_codegen_object_new(Uri_t1116831938_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_12, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Uri_get_AbsolutePath_m198419197(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = String_Replace_m2915759397(L_13, _stringLiteral48872, _stringLiteral1494102278, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1234698980, L_14, /*hidden argument*/NULL);
		___url0 = L_15;
	}

IL_0079:
	{
		String_t* L_16 = ___url0;
		NullCheck(L_16);
		String_t* L_17 = String_Replace_m2915759397(L_16, _stringLiteral2263004707, _stringLiteral2263004735, /*hidden argument*/NULL);
		___url0 = L_17;
		String_t* L_18 = ___url0;
		Il2CppObject* L_19 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		Il2CppObject* L_20 = V_0;
		NullCheck(L_20);
		bool L_21 = InterfaceFuncInvoker2< bool, String_t*, String_t** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_20, _stringLiteral118, (&V_1));
		V_3 = L_21;
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_00ab;
		}
	}
	{
		String_t** L_23 = ___normalizedUrl1;
		*((Il2CppObject **)(L_23)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_23), (Il2CppObject *)NULL);
		V_2 = (bool)0;
		goto IL_00bc;
	}

IL_00ab:
	{
		String_t** L_24 = ___normalizedUrl1;
		String_t* L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2126589602, L_25, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_24)) = (Il2CppObject *)L_26;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_24), (Il2CppObject *)L_26);
		V_2 = (bool)1;
		goto IL_00bc;
	}

IL_00bc:
	{
		bool L_27 = V_2;
		return L_27;
	}
}
// System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo> YoutubeExtractor.DownloadUrlResolver::ExtractDownloadUrls(Newtonsoft.Json.Linq.JObject)
extern Il2CppClass* U3CExtractDownloadUrlsU3Ed__1_t1414174443_il2cpp_TypeInfo_var;
extern const uint32_t DownloadUrlResolver_ExtractDownloadUrls_m2099554026_MetadataUsageId;
extern "C"  Il2CppObject* DownloadUrlResolver_ExtractDownloadUrls_m2099554026 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_ExtractDownloadUrls_m2099554026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CExtractDownloadUrlsU3Ed__1_t1414174443 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	{
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_0 = (U3CExtractDownloadUrlsU3Ed__1_t1414174443 *)il2cpp_codegen_object_new(U3CExtractDownloadUrlsU3Ed__1_t1414174443_il2cpp_TypeInfo_var);
		U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205(L_0, ((int32_t)-2), /*hidden argument*/NULL);
		V_0 = L_0;
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_1 = V_0;
		JObject_t1798544199 * L_2 = ___json0;
		NullCheck(L_1);
		L_1->set_U3CU3E3__json_4(L_2);
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_3 = V_0;
		V_1 = L_3;
		goto IL_0013;
	}

IL_0013:
	{
		Il2CppObject* L_4 = V_1;
		return L_4;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::ExtractSignatureFromManifest(System.String)
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115;
extern const uint32_t DownloadUrlResolver_ExtractSignatureFromManifest_m3925939498_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_ExtractSignatureFromManifest_m3925939498 (Il2CppObject * __this /* static, unused */, String_t* ___manifestUrl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_ExtractSignatureFromManifest_m3925939498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t4054002952* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	CharU5BU5D_t3324145743* V_3 = NULL;
	bool V_4 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___manifestUrl0;
		V_3 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		CharU5BU5D_t3324145743* L_1 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
		CharU5BU5D_t3324145743* L_2 = V_3;
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_3 = String_Split_m290179486(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_004b;
	}

IL_0019:
	{
		StringU5BU5D_t4054002952* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, _stringLiteral115, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_9 = V_1;
		StringU5BU5D_t4054002952* L_10 = V_0;
		NullCheck(L_10);
		G_B4_0 = ((((int32_t)((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0037;
	}

IL_0036:
	{
		G_B4_0 = 1;
	}

IL_0037:
	{
		V_4 = (bool)G_B4_0;
		bool L_11 = V_4;
		if (L_11)
		{
			goto IL_0046;
		}
	}
	{
		StringU5BU5D_t4054002952* L_12 = V_0;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)));
		int32_t L_14 = ((int32_t)((int32_t)L_13+(int32_t)1));
		String_t* L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		goto IL_005f;
	}

IL_0046:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_17 = V_1;
		StringU5BU5D_t4054002952* L_18 = V_0;
		NullCheck(L_18);
		V_4 = (bool)((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))))? 1 : 0);
		bool L_19 = V_4;
		if (L_19)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_20;
		goto IL_005f;
	}

IL_005f:
	{
		String_t* L_21 = V_2;
		return L_21;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::GetHtml5PlayerVersion(Newtonsoft.Json.Linq.JObject)
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1016841876;
extern Il2CppCodeGenString* _stringLiteral2886759299;
extern Il2CppCodeGenString* _stringLiteral3401;
extern Il2CppCodeGenString* _stringLiteral1165;
extern const uint32_t DownloadUrlResolver_GetHtml5PlayerVersion_m1659753901_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_GetHtml5PlayerVersion_m1659753901 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetHtml5PlayerVersion_m1659753901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Regex_t2161232213 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		Regex_t2161232213 * L_0 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
		Regex__ctor_m574010660(L_0, _stringLiteral1016841876, /*hidden argument*/NULL);
		V_0 = L_0;
		JObject_t1798544199 * L_1 = ___json0;
		NullCheck(L_1);
		JToken_t3412245951 * L_2 = JObject_get_Item_m1550513101(L_1, _stringLiteral2886759299, /*hidden argument*/NULL);
		NullCheck(L_2);
		JToken_t3412245951 * L_3 = VirtFuncInvoker1< JToken_t3412245951 *, Il2CppObject * >::Invoke(13 /* Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object) */, L_2, _stringLiteral3401);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_1 = L_4;
		Regex_t2161232213 * L_5 = V_0;
		String_t* L_6 = V_1;
		NullCheck(L_5);
		Match_t2156507859 * L_7 = Regex_Match_m2003175236(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Text.RegularExpressions.Match::Result(System.String) */, L_7, _stringLiteral1165);
		V_2 = L_8;
		goto IL_003b;
	}

IL_003b:
	{
		String_t* L_9 = V_2;
		return L_9;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::GetStreamMap(Newtonsoft.Json.Linq.JObject)
extern Il2CppClass* VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3002589;
extern Il2CppCodeGenString* _stringLiteral3750992688;
extern Il2CppCodeGenString* _stringLiteral2055457281;
extern Il2CppCodeGenString* _stringLiteral951857060;
extern const uint32_t DownloadUrlResolver_GetStreamMap_m1317351358_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_GetStreamMap_m1317351358 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetStreamMap_m1317351358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JToken_t3412245951 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	String_t* G_B3_0 = NULL;
	int32_t G_B6_0 = 0;
	{
		JObject_t1798544199 * L_0 = ___json0;
		NullCheck(L_0);
		JToken_t3412245951 * L_1 = JObject_get_Item_m1550513101(L_0, _stringLiteral3002589, /*hidden argument*/NULL);
		NullCheck(L_1);
		JToken_t3412245951 * L_2 = VirtFuncInvoker1< JToken_t3412245951 *, Il2CppObject * >::Invoke(13 /* Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object) */, L_1, _stringLiteral3750992688);
		V_0 = L_2;
		JToken_t3412245951 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		JToken_t3412245951 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		G_B3_0 = L_5;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0023:
	{
		V_1 = G_B3_0;
		String_t* L_6 = V_1;
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = String_Contains_m3032019141(L_7, _stringLiteral2055457281, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		goto IL_0039;
	}

IL_0038:
	{
		G_B6_0 = 0;
	}

IL_0039:
	{
		V_3 = (bool)G_B6_0;
		bool L_9 = V_3;
		if (L_9)
		{
			goto IL_004a;
		}
	}
	{
		VideoNotAvailableException_t1343733958 * L_10 = (VideoNotAvailableException_t1343733958 *)il2cpp_codegen_object_new(VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var);
		VideoNotAvailableException__ctor_m3454474022(L_10, _stringLiteral951857060, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_004a:
	{
		String_t* L_11 = V_1;
		V_2 = L_11;
		goto IL_004e;
	}

IL_004e:
	{
		String_t* L_12 = V_2;
		return L_12;
	}
}
// System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo> YoutubeExtractor.DownloadUrlResolver::GetVideoInfos(System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>,System.String)
extern Il2CppClass* List_1_t3467656743_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4226351297_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2837303389_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t405523272_il2cpp_TypeInfo_var;
extern Il2CppClass* VideoInfo_t2099471191_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1602161635_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1296522451_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242641;
extern const uint32_t DownloadUrlResolver_GetVideoInfos_m3217043869_MetadataUsageId;
extern "C"  List_1_t3467656743 * DownloadUrlResolver_GetVideoInfos_m3217043869 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___extractionInfos0, String_t* ___videoTitle1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetVideoInfos_m3217043869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3467656743 * V_0 = NULL;
	ExtractionInfo_t925438340 * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	int32_t V_3 = 0;
	VideoInfo_t2099471191 * V_4 = NULL;
	VideoInfo_t2099471191 * V_5 = NULL;
	VideoInfo_t2099471191 * V_6 = NULL;
	List_1_t3467656743 * V_7 = NULL;
	Il2CppObject* V_8 = NULL;
	bool V_9 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3467656743 * L_0 = (List_1_t3467656743 *)il2cpp_codegen_object_new(List_1_t3467656743_il2cpp_TypeInfo_var);
		List_1__ctor_m1602161635(L_0, /*hidden argument*/List_1__ctor_m1602161635_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject* L_1 = ___extractionInfos0;
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>::GetEnumerator() */, IEnumerable_1_t4226351297_il2cpp_TypeInfo_var, L_1);
		V_8 = L_2;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cb;
		}

IL_0015:
		{
			Il2CppObject* L_3 = V_8;
			NullCheck(L_3);
			ExtractionInfo_t925438340 * L_4 = InterfaceFuncInvoker0< ExtractionInfo_t925438340 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo>::get_Current() */, IEnumerator_1_t2837303389_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			ExtractionInfo_t925438340 * L_5 = V_1;
			NullCheck(L_5);
			Uri_t1116831938 * L_6 = ExtractionInfo_get_Uri_m2410459868(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			String_t* L_7 = Uri_get_Query_m1454422281(L_6, /*hidden argument*/NULL);
			Il2CppObject* L_8 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			Il2CppObject* L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_9, _stringLiteral3242641);
			int32_t L_11 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_3 = L_11;
			int32_t L_12 = V_3;
			ExtractionInfo_t925438340 * L_13 = V_1;
			NullCheck(L_13);
			Uri_t1116831938 * L_14 = ExtractionInfo_get_Uri_m2410459868(L_13, /*hidden argument*/NULL);
			NullCheck(L_14);
			String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
			String_t* L_16 = ___videoTitle1;
			ExtractionInfo_t925438340 * L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = ExtractionInfo_get_RequiresDecryption_m2265378519(L_17, /*hidden argument*/NULL);
			VideoInfo_t2099471191 * L_19 = DownloadUrlResolver_GetSingleVideoInfo_m3669702907(NULL /*static, unused*/, L_12, L_15, L_16, L_18, /*hidden argument*/NULL);
			V_4 = L_19;
			VideoInfo_t2099471191 * L_20 = V_4;
			V_9 = (bool)((((Il2CppObject*)(VideoInfo_t2099471191 *)L_20) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_21 = V_9;
			if (L_21)
			{
				goto IL_00a0;
			}
		}

IL_0065:
		{
			VideoInfo_t2099471191 * L_22 = V_4;
			VideoInfo_t2099471191 * L_23 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
			VideoInfo__ctor_m731028359(L_23, L_22, /*hidden argument*/NULL);
			V_5 = L_23;
			VideoInfo_t2099471191 * L_24 = V_5;
			ExtractionInfo_t925438340 * L_25 = V_1;
			NullCheck(L_25);
			Uri_t1116831938 * L_26 = ExtractionInfo_get_Uri_m2410459868(L_25, /*hidden argument*/NULL);
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_26);
			NullCheck(L_24);
			VideoInfo_set_DownloadUrl_m2337044983(L_24, L_27, /*hidden argument*/NULL);
			VideoInfo_t2099471191 * L_28 = V_5;
			String_t* L_29 = ___videoTitle1;
			NullCheck(L_28);
			VideoInfo_set_Title_m3044007174(L_28, L_29, /*hidden argument*/NULL);
			VideoInfo_t2099471191 * L_30 = V_5;
			ExtractionInfo_t925438340 * L_31 = V_1;
			NullCheck(L_31);
			bool L_32 = ExtractionInfo_get_RequiresDecryption_m2265378519(L_31, /*hidden argument*/NULL);
			NullCheck(L_30);
			VideoInfo_set_RequiresDecryption_m3847149662(L_30, L_32, /*hidden argument*/NULL);
			VideoInfo_t2099471191 * L_33 = V_5;
			V_4 = L_33;
			goto IL_00c1;
		}

IL_00a0:
		{
			int32_t L_34 = V_3;
			VideoInfo_t2099471191 * L_35 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
			VideoInfo__ctor_m2160009652(L_35, L_34, /*hidden argument*/NULL);
			V_6 = L_35;
			VideoInfo_t2099471191 * L_36 = V_6;
			ExtractionInfo_t925438340 * L_37 = V_1;
			NullCheck(L_37);
			Uri_t1116831938 * L_38 = ExtractionInfo_get_Uri_m2410459868(L_37, /*hidden argument*/NULL);
			NullCheck(L_38);
			String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_38);
			NullCheck(L_36);
			VideoInfo_set_DownloadUrl_m2337044983(L_36, L_39, /*hidden argument*/NULL);
			VideoInfo_t2099471191 * L_40 = V_6;
			V_4 = L_40;
		}

IL_00c1:
		{
			List_1_t3467656743 * L_41 = V_0;
			VideoInfo_t2099471191 * L_42 = V_4;
			NullCheck(L_41);
			List_1_Add_m1296522451(L_41, L_42, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		}

IL_00cb:
		{
			Il2CppObject* L_43 = V_8;
			NullCheck(L_43);
			bool L_44 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_43);
			V_9 = L_44;
			bool L_45 = V_9;
			if (L_45)
			{
				goto IL_0015;
			}
		}

IL_00db:
		{
			IL2CPP_LEAVE(0xF1, FINALLY_00dd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00dd;
	}

FINALLY_00dd:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_46 = V_8;
			V_9 = (bool)((((Il2CppObject*)(Il2CppObject*)L_46) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_47 = V_9;
			if (L_47)
			{
				goto IL_00f0;
			}
		}

IL_00e8:
		{
			Il2CppObject* L_48 = V_8;
			NullCheck(L_48);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_48);
		}

IL_00f0:
		{
			IL2CPP_END_FINALLY(221)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(221)
	{
		IL2CPP_JUMP_TBL(0xF1, IL_00f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00f1:
	{
		List_1_t3467656743 * L_49 = V_0;
		V_7 = L_49;
		goto IL_00f7;
	}

IL_00f7:
	{
		List_1_t3467656743 * L_50 = V_7;
		return L_50;
	}
}
// YoutubeExtractor.VideoInfo YoutubeExtractor.DownloadUrlResolver::GetSingleVideoInfo(System.Int32,System.String,System.String,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass12_t2936911561_il2cpp_TypeInfo_var;
extern Il2CppClass* VideoInfo_t2099471191_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3856024784_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t405523272_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass12_U3CGetSingleVideoInfoU3Eb__11_m780004071_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3138348687_MethodInfo_var;
extern const MethodInfo* Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3056338;
extern const uint32_t DownloadUrlResolver_GetSingleVideoInfo_m3669702907_MetadataUsageId;
extern "C"  VideoInfo_t2099471191 * DownloadUrlResolver_GetSingleVideoInfo_m3669702907 (Il2CppObject * __this /* static, unused */, int32_t ___formatCode0, String_t* ___queryUrl1, String_t* ___videoTitle2, bool ___requiresDecryption3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetSingleVideoInfo_m3669702907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	VideoInfo_t2099471191 * V_1 = NULL;
	int64_t V_2 = 0;
	VideoInfo_t2099471191 * V_3 = NULL;
	VideoInfo_t2099471191 * V_4 = NULL;
	U3CU3Ec__DisplayClass12_t2936911561 * V_5 = NULL;
	VideoInfo_t2099471191 * V_6 = NULL;
	bool V_7 = false;
	int64_t G_B4_0 = 0;
	{
		U3CU3Ec__DisplayClass12_t2936911561 * L_0 = (U3CU3Ec__DisplayClass12_t2936911561 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass12_t2936911561_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass12__ctor_m1949650966(L_0, /*hidden argument*/NULL);
		V_5 = L_0;
		U3CU3Ec__DisplayClass12_t2936911561 * L_1 = V_5;
		int32_t L_2 = ___formatCode0;
		NullCheck(L_1);
		L_1->set_formatCode_0(L_2);
		String_t* L_3 = ___queryUrl1;
		Il2CppObject* L_4 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = ((VideoInfo_t2099471191_StaticFields*)VideoInfo_t2099471191_il2cpp_TypeInfo_var->static_fields)->get_Defaults_0();
		U3CU3Ec__DisplayClass12_t2936911561 * L_6 = V_5;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass12_U3CGetSingleVideoInfoU3Eb__11_m780004071_MethodInfo_var);
		Func_2_t3856024784 * L_8 = (Func_2_t3856024784 *)il2cpp_codegen_object_new(Func_2_t3856024784_il2cpp_TypeInfo_var);
		Func_2__ctor_m3138348687(L_8, L_6, L_7, /*hidden argument*/Func_2__ctor_m3138348687_MethodInfo_var);
		VideoInfo_t2099471191 * L_9 = Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470_MethodInfo_var);
		V_1 = L_9;
		VideoInfo_t2099471191 * L_10 = V_1;
		V_7 = (bool)((((Il2CppObject*)(VideoInfo_t2099471191 *)L_10) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_11 = V_7;
		if (L_11)
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject* L_12 = V_0;
		NullCheck(L_12);
		bool L_13 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_12, _stringLiteral3056338);
		if (L_13)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = (((int64_t)((int64_t)0)));
		goto IL_005b;
	}

IL_004b:
	{
		Il2CppObject* L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_14, _stringLiteral3056338);
		int64_t L_16 = Int64_Parse_m2426231402(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		G_B4_0 = L_16;
	}

IL_005b:
	{
		V_2 = G_B4_0;
		VideoInfo_t2099471191 * L_17 = V_1;
		VideoInfo_t2099471191 * L_18 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m731028359(L_18, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		VideoInfo_t2099471191 * L_19 = V_3;
		String_t* L_20 = ___queryUrl1;
		NullCheck(L_19);
		VideoInfo_set_DownloadUrl_m2337044983(L_19, L_20, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_21 = V_3;
		String_t* L_22 = ___videoTitle2;
		NullCheck(L_21);
		VideoInfo_set_Title_m3044007174(L_21, L_22, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_23 = V_3;
		bool L_24 = ___requiresDecryption3;
		NullCheck(L_23);
		VideoInfo_set_RequiresDecryption_m3847149662(L_23, L_24, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_25 = V_3;
		int64_t L_26 = V_2;
		NullCheck(L_25);
		VideoInfo_set_FileSize_m741874861(L_25, L_26, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_27 = V_3;
		V_1 = L_27;
		goto IL_00a5;
	}

IL_0089:
	{
		U3CU3Ec__DisplayClass12_t2936911561 * L_28 = V_5;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_formatCode_0();
		VideoInfo_t2099471191 * L_30 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2160009652(L_30, L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		VideoInfo_t2099471191 * L_31 = V_4;
		String_t* L_32 = ___queryUrl1;
		NullCheck(L_31);
		VideoInfo_set_DownloadUrl_m2337044983(L_31, L_32, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_33 = V_4;
		V_1 = L_33;
	}

IL_00a5:
	{
		VideoInfo_t2099471191 * L_34 = V_1;
		V_6 = L_34;
		goto IL_00aa;
	}

IL_00aa:
	{
		VideoInfo_t2099471191 * L_35 = V_6;
		return L_35;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::GetVideoTitle(Newtonsoft.Json.Linq.JObject)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3002589;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern const uint32_t DownloadUrlResolver_GetVideoTitle_m3499604573_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_GetVideoTitle_m3499604573 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetVideoTitle_m3499604573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JToken_t3412245951 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		JObject_t1798544199 * L_0 = ___json0;
		NullCheck(L_0);
		JToken_t3412245951 * L_1 = JObject_get_Item_m1550513101(L_0, _stringLiteral3002589, /*hidden argument*/NULL);
		NullCheck(L_1);
		JToken_t3412245951 * L_2 = VirtFuncInvoker1< JToken_t3412245951 *, Il2CppObject * >::Invoke(13 /* Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object) */, L_1, _stringLiteral110371416);
		V_0 = L_2;
		JToken_t3412245951 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		JToken_t3412245951 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		G_B3_0 = L_5;
		goto IL_0027;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
	}

IL_0027:
	{
		V_1 = G_B3_0;
		goto IL_002b;
	}

IL_002b:
	{
		String_t* L_7 = V_1;
		return L_7;
	}
}
// System.String YoutubeExtractor.DownloadUrlResolver::GetDashManifest(Newtonsoft.Json.Linq.JObject)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3002589;
extern Il2CppCodeGenString* _stringLiteral1442494031;
extern const uint32_t DownloadUrlResolver_GetDashManifest_m3557462241_MetadataUsageId;
extern "C"  String_t* DownloadUrlResolver_GetDashManifest_m3557462241 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_GetDashManifest_m3557462241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JToken_t3412245951 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		JObject_t1798544199 * L_0 = ___json0;
		NullCheck(L_0);
		JToken_t3412245951 * L_1 = JObject_get_Item_m1550513101(L_0, _stringLiteral3002589, /*hidden argument*/NULL);
		NullCheck(L_1);
		JToken_t3412245951 * L_2 = VirtFuncInvoker1< JToken_t3412245951 *, Il2CppObject * >::Invoke(13 /* Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object) */, L_1, _stringLiteral1442494031);
		V_0 = L_2;
		JToken_t3412245951 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		JToken_t3412245951 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		G_B3_0 = L_5;
		goto IL_0027;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
	}

IL_0027:
	{
		V_1 = G_B3_0;
		goto IL_002b;
	}

IL_002b:
	{
		String_t* L_7 = V_1;
		return L_7;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver::IsVideoUnavailable(System.String)
extern Il2CppCodeGenString* _stringLiteral2630780297;
extern const uint32_t DownloadUrlResolver_IsVideoUnavailable_m1282682818_MetadataUsageId;
extern "C"  bool DownloadUrlResolver_IsVideoUnavailable_m1282682818 (Il2CppObject * __this /* static, unused */, String_t* ___pageSource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_IsVideoUnavailable_m1282682818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___pageSource0;
		NullCheck(L_0);
		bool L_1 = String_Contains_m3032019141(L_0, _stringLiteral2630780297, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// Newtonsoft.Json.Linq.JObject YoutubeExtractor.DownloadUrlResolver::LoadJson(System.String)
extern Il2CppClass* VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral610828807;
extern Il2CppCodeGenString* _stringLiteral1165;
extern const uint32_t DownloadUrlResolver_LoadJson_m2844638074_MetadataUsageId;
extern "C"  JObject_t1798544199 * DownloadUrlResolver_LoadJson_m2844638074 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_LoadJson_m2844638074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Regex_t2161232213 * V_1 = NULL;
	String_t* V_2 = NULL;
	JObject_t1798544199 * V_3 = NULL;
	bool V_4 = false;
	{
		String_t* L_0 = ___url0;
		String_t* L_1 = HttpHelper_DownloadString_m189277324(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = DownloadUrlResolver_IsVideoUnavailable_m1282682818(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_4;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		VideoNotAvailableException_t1343733958 * L_5 = (VideoNotAvailableException_t1343733958 *)il2cpp_codegen_object_new(VideoNotAvailableException_t1343733958_il2cpp_TypeInfo_var);
		VideoNotAvailableException__ctor_m1707482460(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		Regex_t2161232213 * L_6 = (Regex_t2161232213 *)il2cpp_codegen_object_new(Regex_t2161232213_il2cpp_TypeInfo_var);
		Regex__ctor_m2068483208(L_6, _stringLiteral610828807, 2, /*hidden argument*/NULL);
		V_1 = L_6;
		Regex_t2161232213 * L_7 = V_1;
		String_t* L_8 = V_0;
		NullCheck(L_7);
		Match_t2156507859 * L_9 = Regex_Match_m2003175236(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Text.RegularExpressions.Match::Result(System.String) */, L_9, _stringLiteral1165);
		V_2 = L_10;
		String_t* L_11 = V_2;
		JObject_t1798544199 * L_12 = JObject_Parse_m1991467294(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		goto IL_0045;
	}

IL_0045:
	{
		JObject_t1798544199 * L_13 = V_3;
		return L_13;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver::ParseDashManifest(System.String,System.Collections.Generic.List`1<YoutubeExtractor.VideoInfo>,System.String)
extern Il2CppClass* XmlDocument_t730752740_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNamespaceManager_t1467853467_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlElement_t280387747_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CU3Ec__DisplayClass15_t2936911564_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3856024784_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass15_U3CParseDashManifestU3Eb__14_m3016585392_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3138348687_MethodInfo_var;
extern const MethodInfo* Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3861655610_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1296522451_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral116081;
extern Il2CppCodeGenString* _stringLiteral2829781396;
extern Il2CppCodeGenString* _stringLiteral3851;
extern Il2CppCodeGenString* _stringLiteral1899223656;
extern Il2CppCodeGenString* _stringLiteral4026142710;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral1332026558;
extern Il2CppCodeGenString* _stringLiteral3073937703;
extern Il2CppCodeGenString* _stringLiteral545057773;
extern const uint32_t DownloadUrlResolver_ParseDashManifest_m3210265877_MetadataUsageId;
extern "C"  void DownloadUrlResolver_ParseDashManifest_m3210265877 (Il2CppObject * __this /* static, unused */, String_t* ___dashManifestUrl0, List_1_t3467656743 * ___previousFormats1, String_t* ___videoTitle2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_ParseDashManifest_m3210265877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	XmlDocument_t730752740 * V_1 = NULL;
	XmlNamespaceManager_t1467853467 * V_2 = NULL;
	XmlNodeList_t991860617 * V_3 = NULL;
	XmlElement_t280387747 * V_4 = NULL;
	XmlNode_t856910923 * V_5 = NULL;
	VideoInfo_t2099471191 * V_6 = NULL;
	VideoInfo_t2099471191 * V_7 = NULL;
	U3CU3Ec__DisplayClass15_t2936911564 * V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	bool V_10 = false;
	Il2CppObject * V_11 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___dashManifestUrl0;
		String_t* L_1 = HttpHelper_DownloadString_m189277324(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		XmlDocument_t730752740 * L_2 = (XmlDocument_t730752740 *)il2cpp_codegen_object_new(XmlDocument_t730752740_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m467220425(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		XmlDocument_t730752740 * L_3 = V_1;
		NullCheck(L_3);
		XmlNameTable_t1216706026 * L_4 = XmlDocument_get_NameTable_m4115788656(L_3, /*hidden argument*/NULL);
		XmlNamespaceManager_t1467853467 * L_5 = (XmlNamespaceManager_t1467853467 *)il2cpp_codegen_object_new(XmlNamespaceManager_t1467853467_il2cpp_TypeInfo_var);
		XmlNamespaceManager__ctor_m1896995422(L_5, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		XmlNamespaceManager_t1467853467 * L_6 = V_2;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(8 /* System.Void System.Xml.XmlNamespaceManager::AddNamespace(System.String,System.String) */, L_6, _stringLiteral116081, _stringLiteral2829781396);
		XmlNamespaceManager_t1467853467 * L_7 = V_2;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(8 /* System.Void System.Xml.XmlNamespaceManager::AddNamespace(System.String,System.String) */, L_7, _stringLiteral3851, _stringLiteral1899223656);
		XmlDocument_t730752740 * L_8 = V_1;
		String_t* L_9 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(60 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_8, L_9);
		XmlDocument_t730752740 * L_10 = V_1;
		XmlNamespaceManager_t1467853467 * L_11 = V_2;
		NullCheck(L_10);
		XmlNodeList_t991860617 * L_12 = XmlNode_SelectNodes_m3306632150(L_10, _stringLiteral4026142710, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		XmlNodeList_t991860617 * L_13 = V_3;
		NullCheck(L_13);
		Il2CppObject * L_14 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_13);
		V_9 = L_14;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0147;
		}

IL_005f:
		{
			Il2CppObject * L_15 = V_9;
			NullCheck(L_15);
			Il2CppObject * L_16 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_15);
			V_4 = ((XmlElement_t280387747 *)CastclassClass(L_16, XmlElement_t280387747_il2cpp_TypeInfo_var));
			U3CU3Ec__DisplayClass15_t2936911564 * L_17 = (U3CU3Ec__DisplayClass15_t2936911564 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass15_t2936911564_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass15__ctor_m1360110451(L_17, /*hidden argument*/NULL);
			V_8 = L_17;
			U3CU3Ec__DisplayClass15_t2936911564 * L_18 = V_8;
			XmlElement_t280387747 * L_19 = V_4;
			NullCheck(L_19);
			String_t* L_20 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(44 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_19, _stringLiteral3355);
			int32_t L_21 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			NullCheck(L_18);
			L_18->set_FormatCode_0(L_21);
			XmlElement_t280387747 * L_22 = V_4;
			NullCheck(L_22);
			XmlNodeList_t991860617 * L_23 = VirtFuncInvoker1< XmlNodeList_t991860617 *, String_t* >::Invoke(45 /* System.Xml.XmlNodeList System.Xml.XmlElement::GetElementsByTagName(System.String) */, L_22, _stringLiteral1332026558);
			NullCheck(L_23);
			XmlNode_t856910923 * L_24 = VirtFuncInvoker1< XmlNode_t856910923 *, int32_t >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNodeList::Item(System.Int32) */, L_23, 0);
			V_5 = L_24;
			U3CU3Ec__DisplayClass15_t2936911564 * L_25 = V_8;
			NullCheck(L_25);
			int32_t L_26 = L_25->get_FormatCode_0();
			XmlNode_t856910923 * L_27 = V_5;
			NullCheck(L_27);
			String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_27);
			String_t* L_29 = ___videoTitle2;
			VideoInfo_t2099471191 * L_30 = DownloadUrlResolver_GetSingleVideoInfo_m3669702907(NULL /*static, unused*/, L_26, L_28, L_29, (bool)0, /*hidden argument*/NULL);
			V_6 = L_30;
			XmlElement_t280387747 * L_31 = V_4;
			NullCheck(L_31);
			bool L_32 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlElement::HasAttribute(System.String) */, L_31, _stringLiteral3073937703);
			V_10 = (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
			bool L_33 = V_10;
			if (L_33)
			{
				goto IL_00e6;
			}
		}

IL_00cd:
		{
			VideoInfo_t2099471191 * L_34 = V_6;
			XmlElement_t280387747 * L_35 = V_4;
			NullCheck(L_35);
			String_t* L_36 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(44 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_35, _stringLiteral3073937703);
			int32_t L_37 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
			NullCheck(L_34);
			VideoInfo_set_Resolution_m1832971451(L_34, L_37, /*hidden argument*/NULL);
		}

IL_00e6:
		{
			XmlElement_t280387747 * L_38 = V_4;
			NullCheck(L_38);
			bool L_39 = VirtFuncInvoker1< bool, String_t* >::Invoke(46 /* System.Boolean System.Xml.XmlElement::HasAttribute(System.String) */, L_38, _stringLiteral545057773);
			V_10 = (bool)((((int32_t)L_39) == ((int32_t)0))? 1 : 0);
			bool L_40 = V_10;
			if (L_40)
			{
				goto IL_0114;
			}
		}

IL_00fb:
		{
			VideoInfo_t2099471191 * L_41 = V_6;
			XmlElement_t280387747 * L_42 = V_4;
			NullCheck(L_42);
			String_t* L_43 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(44 /* System.String System.Xml.XmlElement::GetAttribute(System.String) */, L_42, _stringLiteral545057773);
			int32_t L_44 = Int32_Parse_m3837759498(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
			NullCheck(L_41);
			VideoInfo_set_FrameRate_m26869282(L_41, L_44, /*hidden argument*/NULL);
		}

IL_0114:
		{
			List_1_t3467656743 * L_45 = ___previousFormats1;
			U3CU3Ec__DisplayClass15_t2936911564 * L_46 = V_8;
			IntPtr_t L_47;
			L_47.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass15_U3CParseDashManifestU3Eb__14_m3016585392_MethodInfo_var);
			Func_2_t3856024784 * L_48 = (Func_2_t3856024784 *)il2cpp_codegen_object_new(Func_2_t3856024784_il2cpp_TypeInfo_var);
			Func_2__ctor_m3138348687(L_48, L_46, L_47, /*hidden argument*/Func_2__ctor_m3138348687_MethodInfo_var);
			VideoInfo_t2099471191 * L_49 = Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470(NULL /*static, unused*/, L_45, L_48, /*hidden argument*/Enumerable_SingleOrDefault_TisVideoInfo_t2099471191_m1048240470_MethodInfo_var);
			V_7 = L_49;
			VideoInfo_t2099471191 * L_50 = V_7;
			V_10 = (bool)((((Il2CppObject*)(VideoInfo_t2099471191 *)L_50) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_51 = V_10;
			if (L_51)
			{
				goto IL_013d;
			}
		}

IL_0134:
		{
			List_1_t3467656743 * L_52 = ___previousFormats1;
			VideoInfo_t2099471191 * L_53 = V_7;
			NullCheck(L_52);
			List_1_Remove_m3861655610(L_52, L_53, /*hidden argument*/List_1_Remove_m3861655610_MethodInfo_var);
		}

IL_013d:
		{
			List_1_t3467656743 * L_54 = ___previousFormats1;
			VideoInfo_t2099471191 * L_55 = V_6;
			NullCheck(L_54);
			List_1_Add_m1296522451(L_54, L_55, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		}

IL_0147:
		{
			Il2CppObject * L_56 = V_9;
			NullCheck(L_56);
			bool L_57 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_56);
			V_10 = L_57;
			bool L_58 = V_10;
			if (L_58)
			{
				goto IL_005f;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x176, FINALLY_0159);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0159;
	}

FINALLY_0159:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_59 = V_9;
			V_11 = ((Il2CppObject *)IsInst(L_59, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			Il2CppObject * L_60 = V_11;
			V_10 = (bool)((((Il2CppObject*)(Il2CppObject *)L_60) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_61 = V_10;
			if (L_61)
			{
				goto IL_0175;
			}
		}

IL_016d:
		{
			Il2CppObject * L_62 = V_11;
			NullCheck(L_62);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_62);
		}

IL_0175:
		{
			IL2CPP_END_FINALLY(345)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(345)
	{
		IL2CPP_JUMP_TBL(0x176, IL_0176)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0176:
	{
		return;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver::ThrowYoutubeParseException(System.Exception,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* YoutubeParseException_t1485001709_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2555262676;
extern Il2CppCodeGenString* _stringLiteral1018800709;
extern const uint32_t DownloadUrlResolver_ThrowYoutubeParseException_m2477564202_MetadataUsageId;
extern "C"  void DownloadUrlResolver_ThrowYoutubeParseException_m2477564202 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___innerException0, String_t* ___videoUrl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadUrlResolver_ThrowYoutubeParseException_m2477564202_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___videoUrl1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2555262676, L_0, _stringLiteral1018800709, /*hidden argument*/NULL);
		Exception_t3991598821 * L_2 = ___innerException0;
		YoutubeParseException_t1485001709 * L_3 = (YoutubeParseException_t1485001709 *)il2cpp_codegen_object_new(YoutubeParseException_t1485001709_il2cpp_TypeInfo_var);
		YoutubeParseException__ctor_m627752129(L_3, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12::.ctor()
extern "C"  void U3CU3Ec__DisplayClass12__ctor_m1949650966 (U3CU3Ec__DisplayClass12_t2936911561 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass12::<GetSingleVideoInfo>b__11(YoutubeExtractor.VideoInfo)
extern "C"  bool U3CU3Ec__DisplayClass12_U3CGetSingleVideoInfoU3Eb__11_m780004071 (U3CU3Ec__DisplayClass12_t2936911561 * __this, VideoInfo_t2099471191 * ___videoInfo0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		VideoInfo_t2099471191 * L_0 = ___videoInfo0;
		NullCheck(L_0);
		int32_t L_1 = VideoInfo_get_FormatCode_m1039643044(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_formatCode_0();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15__ctor_m1360110451 (U3CU3Ec__DisplayClass15_t2936911564 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<>c__DisplayClass15::<ParseDashManifest>b__14(YoutubeExtractor.VideoInfo)
extern "C"  bool U3CU3Ec__DisplayClass15_U3CParseDashManifestU3Eb__14_m3016585392 (U3CU3Ec__DisplayClass15_t2936911564 * __this, VideoInfo_t2099471191 * ___v0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		VideoInfo_t2099471191 * L_0 = ___v0;
		NullCheck(L_0);
		int32_t L_1 = VideoInfo_get_FormatCode_m1039643044(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_FormatCode_0();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<YoutubeExtractor.DownloadUrlResolver/ExtractionInfo> YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.Generic.IEnumerable<YoutubeExtractor.DownloadUrlResolver.ExtractionInfo>.GetEnumerator()
extern Il2CppClass* Thread_t1973216770_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CExtractDownloadUrlsU3Ed__1_t1414174443_il2cpp_TypeInfo_var;
extern const uint32_t U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumerableU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_GetEnumerator_m2632622343_MetadataUsageId;
extern "C"  Il2CppObject* U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumerableU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_GetEnumerator_m2632622343 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumerableU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_GetEnumerator_m2632622343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CExtractDownloadUrlsU3Ed__1_t1414174443 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1973216770_il2cpp_TypeInfo_var);
		Thread_t1973216770 * L_0 = Thread_get_CurrentThread_m1523593825(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m3915632970(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_U3CU3El__initialThreadId_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = __this->get_U3CU3E1__state_1();
		G_B3_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)((int32_t)-2)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		V_2 = (bool)G_B3_0;
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		__this->set_U3CU3E1__state_1(0);
		V_0 = __this;
		goto IL_0039;
	}

IL_0032:
	{
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_5 = (U3CExtractDownloadUrlsU3Ed__1_t1414174443 *)il2cpp_codegen_object_new(U3CExtractDownloadUrlsU3Ed__1_t1414174443_il2cpp_TypeInfo_var);
		U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205(L_5, 0, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0039:
	{
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_6 = V_0;
		JObject_t1798544199 * L_7 = __this->get_U3CU3E3__json_4();
		NullCheck(L_6);
		L_6->set_json_3(L_7);
		U3CExtractDownloadUrlsU3Ed__1_t1414174443 * L_8 = V_0;
		V_1 = L_8;
		goto IL_0049;
	}

IL_0049:
	{
		Il2CppObject* L_9 = V_1;
		return L_9;
	}
}
// System.Collections.IEnumerator YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerable_GetEnumerator_m2559629573 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumerableU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_GetEnumerator_m2632622343(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::MoveNext()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t405523272_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExtractionInfo_t925438340_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral115;
extern Il2CppCodeGenString* _stringLiteral113873;
extern Il2CppCodeGenString* _stringLiteral3988319410;
extern Il2CppCodeGenString* _stringLiteral116079;
extern Il2CppCodeGenString* _stringLiteral1073584312;
extern Il2CppCodeGenString* _stringLiteral562758885;
extern Il2CppCodeGenString* _stringLiteral1718050206;
extern Il2CppCodeGenString* _stringLiteral2509803976;
extern Il2CppCodeGenString* _stringLiteral787662828;
extern Il2CppCodeGenString* _stringLiteral119527;
extern const uint32_t U3CExtractDownloadUrlsU3Ed__1_MoveNext_m1534962416_MetadataUsageId;
extern "C"  bool U3CExtractDownloadUrlsU3Ed__1_MoveNext_m1534962416 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExtractDownloadUrlsU3Ed__1_MoveNext_m1534962416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	CharU5BU5D_t3324145743* V_4 = NULL;
	bool V_5 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	String_t* G_B13_0 = NULL;
	String_t* G_B16_0 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = __this->get_U3CU3E1__state_1();
			V_3 = L_0;
			int32_t L_1 = V_3;
			if (L_1 == 0)
			{
				goto IL_0020;
			}
			if (L_1 == 1)
			{
				goto IL_0022;
			}
			if (L_1 == 2)
			{
				goto IL_001b;
			}
		}

IL_0019:
		{
			goto IL_0022;
		}

IL_001b:
		{
			goto IL_0261;
		}

IL_0020:
		{
			goto IL_0027;
		}

IL_0022:
		{
			goto IL_0298;
		}

IL_0027:
		{
			__this->set_U3CU3E1__state_1((-1));
			JObject_t1798544199 * L_2 = __this->get_json_3();
			String_t* L_3 = DownloadUrlResolver_GetStreamMap_m1317351358(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_4 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
			CharU5BU5D_t3324145743* L_4 = V_4;
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
			CharU5BU5D_t3324145743* L_5 = V_4;
			NullCheck(L_3);
			StringU5BU5D_t4054002952* L_6 = String_Split_m290179486(L_3, L_5, /*hidden argument*/NULL);
			__this->set_U3CsplitByUrlsU3E5__2_5(L_6);
			__this->set_U3CU3E1__state_1(1);
			StringU5BU5D_t4054002952* L_7 = __this->get_U3CsplitByUrlsU3E5__2_5();
			__this->set_U3CU3E7__wrap9_12(L_7);
			__this->set_U3CU3E7__wrapa_13(0);
			goto IL_0277;
		}

IL_0075:
		{
			StringU5BU5D_t4054002952* L_8 = __this->get_U3CU3E7__wrap9_12();
			int32_t L_9 = __this->get_U3CU3E7__wrapa_13();
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
			int32_t L_10 = L_9;
			String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
			__this->set_U3CsU3E5__3_6(L_11);
			String_t* L_12 = __this->get_U3CsU3E5__3_6();
			Il2CppObject* L_13 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			__this->set_U3CqueriesU3E5__4_7(L_13);
			__this->set_U3CrequiresDecryptionU3E5__6_9((bool)0);
			Il2CppObject* L_14 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_14, _stringLiteral115);
			if (L_15)
			{
				goto IL_00c8;
			}
		}

IL_00b3:
		{
			Il2CppObject* L_16 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_16, _stringLiteral113873);
			G_B9_0 = ((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
			goto IL_00c9;
		}

IL_00c8:
		{
			G_B9_0 = 0;
		}

IL_00c9:
		{
			V_5 = (bool)G_B9_0;
			bool L_18 = V_5;
			if (L_18)
			{
				goto IL_0190;
			}
		}

IL_00d3:
		{
			Il2CppObject* L_19 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_19, _stringLiteral115);
			__this->set_U3CrequiresDecryptionU3E5__6_9(L_20);
			Il2CppObject* L_21 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_21);
			bool L_22 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_21, _stringLiteral115);
			if (L_22)
			{
				goto IL_010e;
			}
		}

IL_00fc:
		{
			Il2CppObject* L_23 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_23);
			String_t* L_24 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_23, _stringLiteral113873);
			G_B13_0 = L_24;
			goto IL_011e;
		}

IL_010e:
		{
			Il2CppObject* L_25 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_25);
			String_t* L_26 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_25, _stringLiteral115);
			G_B13_0 = L_26;
		}

IL_011e:
		{
			V_0 = G_B13_0;
			Il2CppObject* L_27 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_27);
			String_t* L_28 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_27, _stringLiteral116079);
			String_t* L_29 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3988319410, L_28, _stringLiteral1073584312, L_29, /*hidden argument*/NULL);
			__this->set_U3CurlU3E5__5_8(L_30);
			Il2CppObject* L_31 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_31);
			bool L_32 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_31, _stringLiteral562758885);
			if (L_32)
			{
				goto IL_015f;
			}
		}

IL_0158:
		{
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			G_B16_0 = L_33;
			goto IL_0179;
		}

IL_015f:
		{
			Il2CppObject* L_34 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_34);
			String_t* L_35 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_34, _stringLiteral562758885);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral1718050206, L_35, /*hidden argument*/NULL);
			G_B16_0 = L_36;
		}

IL_0179:
		{
			V_1 = G_B16_0;
			String_t* L_37 = __this->get_U3CurlU3E5__5_8();
			String_t* L_38 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_39 = String_Concat_m138640077(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
			__this->set_U3CurlU3E5__5_8(L_39);
			goto IL_01a8;
		}

IL_0190:
		{
			Il2CppObject* L_40 = __this->get_U3CqueriesU3E5__4_7();
			NullCheck(L_40);
			String_t* L_41 = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(4 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Item(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_40, _stringLiteral116079);
			__this->set_U3CurlU3E5__5_8(L_41);
		}

IL_01a8:
		{
			String_t* L_42 = __this->get_U3CurlU3E5__5_8();
			String_t* L_43 = HttpHelper_UrlDecode_m2955848320(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
			__this->set_U3CurlU3E5__5_8(L_43);
			String_t* L_44 = __this->get_U3CurlU3E5__5_8();
			String_t* L_45 = HttpHelper_UrlDecode_m2955848320(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
			__this->set_U3CurlU3E5__5_8(L_45);
			String_t* L_46 = __this->get_U3CurlU3E5__5_8();
			Il2CppObject* L_47 = HttpHelper_ParseQueryString_m2936859467(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
			__this->set_U3CparametersU3E5__7_10(L_47);
			Il2CppObject* L_48 = __this->get_U3CparametersU3E5__7_10();
			NullCheck(L_48);
			bool L_49 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.String>::ContainsKey(!0) */, IDictionary_2_t405523272_il2cpp_TypeInfo_var, L_48, _stringLiteral2509803976);
			V_5 = L_49;
			bool L_50 = V_5;
			if (L_50)
			{
				goto IL_0216;
			}
		}

IL_01f1:
		{
			String_t* L_51 = __this->get_U3CurlU3E5__5_8();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_52 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral787662828, _stringLiteral2509803976, _stringLiteral119527, /*hidden argument*/NULL);
			String_t* L_53 = String_Concat_m138640077(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
			__this->set_U3CurlU3E5__5_8(L_53);
		}

IL_0216:
		{
			ExtractionInfo_t925438340 * L_54 = (ExtractionInfo_t925438340 *)il2cpp_codegen_object_new(ExtractionInfo_t925438340_il2cpp_TypeInfo_var);
			ExtractionInfo__ctor_m443894211(L_54, /*hidden argument*/NULL);
			__this->set_U3CU3Eg__initLocal0_11(L_54);
			ExtractionInfo_t925438340 * L_55 = __this->get_U3CU3Eg__initLocal0_11();
			bool L_56 = __this->get_U3CrequiresDecryptionU3E5__6_9();
			NullCheck(L_55);
			ExtractionInfo_set_RequiresDecryption_m50076542(L_55, L_56, /*hidden argument*/NULL);
			ExtractionInfo_t925438340 * L_57 = __this->get_U3CU3Eg__initLocal0_11();
			String_t* L_58 = __this->get_U3CurlU3E5__5_8();
			Uri_t1116831938 * L_59 = (Uri_t1116831938 *)il2cpp_codegen_object_new(Uri_t1116831938_il2cpp_TypeInfo_var);
			Uri__ctor_m1721267859(L_59, L_58, /*hidden argument*/NULL);
			NullCheck(L_57);
			ExtractionInfo_set_Uri_m4068311779(L_57, L_59, /*hidden argument*/NULL);
			ExtractionInfo_t925438340 * L_60 = __this->get_U3CU3Eg__initLocal0_11();
			__this->set_U3CU3E2__current_0(L_60);
			__this->set_U3CU3E1__state_1(2);
			V_2 = (bool)1;
			goto IL_02a4;
		}

IL_0261:
		{
			__this->set_U3CU3E1__state_1(1);
			int32_t L_61 = __this->get_U3CU3E7__wrapa_13();
			__this->set_U3CU3E7__wrapa_13(((int32_t)((int32_t)L_61+(int32_t)1)));
		}

IL_0277:
		{
			int32_t L_62 = __this->get_U3CU3E7__wrapa_13();
			StringU5BU5D_t4054002952* L_63 = __this->get_U3CU3E7__wrap9_12();
			NullCheck(L_63);
			V_5 = (bool)((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))))? 1 : 0);
			bool L_64 = V_5;
			if (L_64)
			{
				goto IL_0075;
			}
		}

IL_0290:
		{
			U3CExtractDownloadUrlsU3Ed__1_U3CU3Em__Finally8_m1123724274(__this, /*hidden argument*/NULL);
		}

IL_0298:
		{
			V_2 = (bool)0;
			goto IL_02a4;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FAULT_029c;
	}

FAULT_029c:
	{ // begin fault (depth: 1)
		U3CExtractDownloadUrlsU3Ed__1_System_IDisposable_Dispose_m2005597931(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(668)
	} // end fault
	IL2CPP_CLEANUP(668)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_02a4:
	{
		bool L_65 = V_2;
		return L_65;
	}
}
// YoutubeExtractor.DownloadUrlResolver/ExtractionInfo YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.Generic.IEnumerator<YoutubeExtractor.DownloadUrlResolver.ExtractionInfo>.get_Current()
extern "C"  ExtractionInfo_t925438340 * U3CExtractDownloadUrlsU3Ed__1_System_Collections_Generic_IEnumeratorU3CYoutubeExtractor_DownloadUrlResolver_ExtractionInfoU3E_get_Current_m4284446846 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	ExtractionInfo_t925438340 * V_0 = NULL;
	{
		ExtractionInfo_t925438340 * L_0 = __this->get_U3CU3E2__current_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		ExtractionInfo_t925438340 * L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerator.Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_Reset_m1672672582_MetadataUsageId;
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_Reset_m1672672582 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_Reset_m1672672582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.IDisposable.Dispose()
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_System_IDisposable_Dispose_m2005597931 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_001b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_001d;
	}

IL_0019:
	{
		goto IL_001f;
	}

IL_001b:
	{
		goto IL_001f;
	}

IL_001d:
	{
		goto IL_0026;
	}

IL_001f:
	{
		U3CExtractDownloadUrlsU3Ed__1_U3CU3Em__Finally8_m1123724274(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Object YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExtractDownloadUrlsU3Ed__1_System_Collections_IEnumerator_get_Current_m1829989116 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		ExtractionInfo_t925438340 * L_0 = __this->get_U3CU3E2__current_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::.ctor(System.Int32)
extern Il2CppClass* Thread_t1973216770_il2cpp_TypeInfo_var;
extern const uint32_t U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205_MetadataUsageId;
extern "C"  void U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExtractDownloadUrlsU3Ed__1__ctor_m2256003205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1973216770_il2cpp_TypeInfo_var);
		Thread_t1973216770 * L_1 = Thread_get_CurrentThread_m1523593825(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m3915632970(L_1, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_2);
		return;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/<ExtractDownloadUrls>d__1::<>m__Finally8()
extern "C"  void U3CExtractDownloadUrlsU3Ed__1_U3CU3Em__Finally8_m1123724274 (U3CExtractDownloadUrlsU3Ed__1_t1414174443 * __this, const MethodInfo* method)
{
	{
		__this->set_U3CU3E1__state_1((-1));
		return;
	}
}
// System.Boolean YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::get_RequiresDecryption()
extern "C"  bool ExtractionInfo_get_RequiresDecryption_m2265378519 (ExtractionInfo_t925438340 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CRequiresDecryptionU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::set_RequiresDecryption(System.Boolean)
extern "C"  void ExtractionInfo_set_RequiresDecryption_m50076542 (ExtractionInfo_t925438340 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiresDecryptionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Uri YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::get_Uri()
extern "C"  Uri_t1116831938 * ExtractionInfo_get_Uri_m2410459868 (ExtractionInfo_t925438340 * __this, const MethodInfo* method)
{
	Uri_t1116831938 * V_0 = NULL;
	{
		Uri_t1116831938 * L_0 = __this->get_U3CUriU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Uri_t1116831938 * L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::set_Uri(System.Uri)
extern "C"  void ExtractionInfo_set_Uri_m4068311779 (ExtractionInfo_t925438340 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method)
{
	{
		Uri_t1116831938 * L_0 = ___value0;
		__this->set_U3CUriU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void YoutubeExtractor.DownloadUrlResolver/ExtractionInfo::.ctor()
extern "C"  void ExtractionInfo__ctor_m443894211 (ExtractionInfo_t925438340 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String YoutubeExtractor.HttpHelper::DownloadString(System.String)
extern Il2CppClass* WebClient_t220232441_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t HttpHelper_DownloadString_m189277324_MetadataUsageId;
extern "C"  String_t* HttpHelper_DownloadString_m189277324 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HttpHelper_DownloadString_m189277324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebClient_t220232441 * V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebClient_t220232441 * L_0 = (WebClient_t220232441 *)il2cpp_codegen_object_new(WebClient_t220232441_il2cpp_TypeInfo_var);
		WebClient__ctor_m2477960557(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		WebClient_t220232441 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_2 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		WebClient_set_Encoding_m1478904018(L_1, L_2, /*hidden argument*/NULL);
		WebClient_t220232441 * L_3 = V_0;
		String_t* L_4 = ___url0;
		NullCheck(L_3);
		String_t* L_5 = WebClient_DownloadString_m2198465167(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		IL2CPP_LEAVE(0x2E, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		{
			WebClient_t220232441 * L_6 = V_0;
			V_2 = (bool)((((Il2CppObject*)(WebClient_t220232441 *)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_7 = V_2;
			if (L_7)
			{
				goto IL_002d;
			}
		}

IL_0026:
		{
			WebClient_t220232441 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_8);
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(30)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_002e:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.String> YoutubeExtractor.HttpHelper::ParseQueryString(System.String)
extern Il2CppClass* Dictionary_2_t827649927_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t2161232213_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m346907281_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral61;
extern const uint32_t HttpHelper_ParseQueryString_m2936859467_MetadataUsageId;
extern "C"  Il2CppObject* HttpHelper_ParseQueryString_m2936859467 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HttpHelper_ParseQueryString_m2936859467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t827649927 * V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t4054002952* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	bool V_4 = false;
	StringU5BU5D_t4054002952* V_5 = NULL;
	int32_t V_6 = 0;
	String_t* G_B5_0 = NULL;
	Dictionary_2_t827649927 * G_B5_1 = NULL;
	String_t* G_B4_0 = NULL;
	Dictionary_2_t827649927 * G_B4_1 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	Dictionary_2_t827649927 * G_B6_2 = NULL;
	{
		String_t* L_0 = ___s0;
		NullCheck(L_0);
		bool L_1 = String_Contains_m3032019141(L_0, _stringLiteral63, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_4;
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = ___s0;
		String_t* L_4 = ___s0;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m2775210486(L_4, ((int32_t)63), /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_6 = String_Substring_m2809233063(L_3, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		___s0 = L_6;
	}

IL_0027:
	{
		Dictionary_2_t827649927 * L_7 = (Dictionary_2_t827649927 *)il2cpp_codegen_object_new(Dictionary_2_t827649927_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_7, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		V_0 = L_7;
		String_t* L_8 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
		StringU5BU5D_t4054002952* L_9 = Regex_Split_m3595096453(NULL /*static, unused*/, L_8, _stringLiteral38, /*hidden argument*/NULL);
		V_5 = L_9;
		V_6 = 0;
		goto IL_007a;
	}

IL_0040:
	{
		StringU5BU5D_t4054002952* L_10 = V_5;
		int32_t L_11 = V_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_1 = L_13;
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t2161232213_il2cpp_TypeInfo_var);
		StringU5BU5D_t4054002952* L_15 = Regex_Split_m3595096453(NULL /*static, unused*/, L_14, _stringLiteral61, /*hidden argument*/NULL);
		V_2 = L_15;
		Dictionary_2_t827649927 * L_16 = V_0;
		StringU5BU5D_t4054002952* L_17 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		String_t* L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		StringU5BU5D_t4054002952* L_20 = V_2;
		NullCheck(L_20);
		G_B4_0 = L_19;
		G_B4_1 = L_16;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))) == ((int32_t)2)))
		{
			G_B5_0 = L_19;
			G_B5_1 = L_16;
			goto IL_0064;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_21;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_006c;
	}

IL_0064:
	{
		StringU5BU5D_t4054002952* L_22 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 1);
		int32_t L_23 = 1;
		String_t* L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		String_t* L_25 = HttpHelper_UrlDecode_m2955848320(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		G_B6_0 = L_25;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_006c:
	{
		NullCheck(G_B6_2);
		Dictionary_2_Add_m346907281(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/Dictionary_2_Add_m346907281_MethodInfo_var);
		int32_t L_26 = V_6;
		V_6 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_6;
		StringU5BU5D_t4054002952* L_28 = V_5;
		NullCheck(L_28);
		V_4 = (bool)((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))))? 1 : 0);
		bool L_29 = V_4;
		if (L_29)
		{
			goto IL_0040;
		}
	}
	{
		Dictionary_2_t827649927 * L_30 = V_0;
		V_3 = L_30;
		goto IL_008c;
	}

IL_008c:
	{
		Il2CppObject* L_31 = V_3;
		return L_31;
	}
}
// System.String YoutubeExtractor.HttpHelper::UrlDecode(System.String)
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t HttpHelper_UrlDecode_m2955848320_MetadataUsageId;
extern "C"  String_t* HttpHelper_UrlDecode_m2955848320 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HttpHelper_UrlDecode_m2955848320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___url0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m2915759397(L_0, _stringLiteral43, _stringLiteral32, /*hidden argument*/NULL);
		___url0 = L_1;
		String_t* L_2 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		String_t* L_3 = Uri_UnescapeDataString_m207009557(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001c;
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// YoutubeExtractor.AdaptiveType YoutubeExtractor.VideoInfo::get_AdaptiveType()
extern "C"  int32_t VideoInfo_get_AdaptiveType_m2103200368 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CAdaptiveTypeU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_AdaptiveType(YoutubeExtractor.AdaptiveType)
extern "C"  void VideoInfo_set_AdaptiveType_m2963356775 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CAdaptiveTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 YoutubeExtractor.VideoInfo::get_AudioBitrate()
extern "C"  int32_t VideoInfo_get_AudioBitrate_m4289100119 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CAudioBitrateU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_AudioBitrate(System.Int32)
extern "C"  void VideoInfo_set_AudioBitrate_m3700425798 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CAudioBitrateU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_FrameRate(System.Int32)
extern "C"  void VideoInfo_set_FrameRate_m26869282 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameRateU3Ek__BackingField_3(L_0);
		return;
	}
}
// YoutubeExtractor.AudioType YoutubeExtractor.VideoInfo::get_AudioType()
extern "C"  int32_t VideoInfo_get_AudioType_m3458747046 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CAudioTypeU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_AudioType(YoutubeExtractor.AudioType)
extern "C"  void VideoInfo_set_AudioType_m945538481 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CAudioTypeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String YoutubeExtractor.VideoInfo::get_DownloadUrl()
extern "C"  String_t* VideoInfo_get_DownloadUrl_m2189851804 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CDownloadUrlU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_DownloadUrl(System.String)
extern "C"  void VideoInfo_set_DownloadUrl_m2337044983 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDownloadUrlU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 YoutubeExtractor.VideoInfo::get_FormatCode()
extern "C"  int32_t VideoInfo_get_FormatCode_m1039643044 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CFormatCodeU3Ek__BackingField_6();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_FormatCode(System.Int32)
extern "C"  void VideoInfo_set_FormatCode_m2346426515 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFormatCodeU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean YoutubeExtractor.VideoInfo::get_Is3D()
extern "C"  bool VideoInfo_get_Is3D_m862911713 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CIs3DU3Ek__BackingField_7();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_Is3D(System.Boolean)
extern "C"  void VideoInfo_set_Is3D_m3760699024 (VideoInfo_t2099471191 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIs3DU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean YoutubeExtractor.VideoInfo::get_RequiresDecryption()
extern "C"  bool VideoInfo_get_RequiresDecryption_m4226650415 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CRequiresDecryptionU3Ek__BackingField_8();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_RequiresDecryption(System.Boolean)
extern "C"  void VideoInfo_set_RequiresDecryption_m3847149662 (VideoInfo_t2099471191 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CRequiresDecryptionU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_FileSize(System.Int64)
extern "C"  void VideoInfo_set_FileSize_m741874861 (VideoInfo_t2099471191 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CFileSizeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Int32 YoutubeExtractor.VideoInfo::get_Resolution()
extern "C"  int32_t VideoInfo_get_Resolution_m3843029452 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CResolutionU3Ek__BackingField_10();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_Resolution(System.Int32)
extern "C"  void VideoInfo_set_Resolution_m1832971451 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CResolutionU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String YoutubeExtractor.VideoInfo::get_Title()
extern "C"  String_t* VideoInfo_get_Title_m1409263085 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CTitleU3Ek__BackingField_11();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_Title(System.String)
extern "C"  void VideoInfo_set_Title_m3044007174 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String YoutubeExtractor.VideoInfo::get_VideoExtension()
extern Il2CppCodeGenString* _stringLiteral1422702;
extern Il2CppCodeGenString* _stringLiteral1471874;
extern Il2CppCodeGenString* _stringLiteral1478659;
extern Il2CppCodeGenString* _stringLiteral46127303;
extern const uint32_t VideoInfo_get_VideoExtension_m2516305905_MetadataUsageId;
extern "C"  String_t* VideoInfo_get_VideoExtension_m2516305905 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoInfo_get_VideoExtension_m2516305905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VideoInfo_get_VideoType_m2599923248(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = V_1;
		if (L_1 == 0)
		{
			goto IL_0020;
		}
		if (L_1 == 1)
		{
			goto IL_0028;
		}
		if (L_1 == 2)
		{
			goto IL_0030;
		}
		if (L_1 == 3)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_0040;
	}

IL_0020:
	{
		V_0 = _stringLiteral1422702;
		goto IL_0044;
	}

IL_0028:
	{
		V_0 = _stringLiteral1471874;
		goto IL_0044;
	}

IL_0030:
	{
		V_0 = _stringLiteral1478659;
		goto IL_0044;
	}

IL_0038:
	{
		V_0 = _stringLiteral46127303;
		goto IL_0044;
	}

IL_0040:
	{
		V_0 = (String_t*)NULL;
		goto IL_0044;
	}

IL_0044:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// YoutubeExtractor.VideoType YoutubeExtractor.VideoInfo::get_VideoType()
extern "C"  int32_t VideoInfo_get_VideoType_m2599923248 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CVideoTypeU3Ek__BackingField_12();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_VideoType(YoutubeExtractor.VideoType)
extern "C"  void VideoInfo_set_VideoType_m2967638225 (VideoInfo_t2099471191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CVideoTypeU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String YoutubeExtractor.VideoInfo::get_HtmlPlayerVersion()
extern "C"  String_t* VideoInfo_get_HtmlPlayerVersion_m1135409505 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CHtmlPlayerVersionU3Ek__BackingField_13();
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void YoutubeExtractor.VideoInfo::set_HtmlPlayerVersion(System.String)
extern "C"  void VideoInfo_set_HtmlPlayerVersion_m3328316562 (VideoInfo_t2099471191 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CHtmlPlayerVersionU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32)
extern "C"  void VideoInfo__ctor_m2160009652 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___formatCode0;
		VideoInfo__ctor_m2543604979(__this, L_0, 4, 0, (bool)0, 4, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::.ctor(YoutubeExtractor.VideoInfo)
extern "C"  void VideoInfo__ctor_m731028359 (VideoInfo_t2099471191 * __this, VideoInfo_t2099471191 * ___info0, const MethodInfo* method)
{
	{
		VideoInfo_t2099471191 * L_0 = ___info0;
		NullCheck(L_0);
		int32_t L_1 = VideoInfo_get_FormatCode_m1039643044(L_0, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_2 = ___info0;
		NullCheck(L_2);
		int32_t L_3 = VideoInfo_get_VideoType_m2599923248(L_2, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_4 = ___info0;
		NullCheck(L_4);
		int32_t L_5 = VideoInfo_get_Resolution_m3843029452(L_4, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_6 = ___info0;
		NullCheck(L_6);
		bool L_7 = VideoInfo_get_Is3D_m862911713(L_6, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_8 = ___info0;
		NullCheck(L_8);
		int32_t L_9 = VideoInfo_get_AudioType_m3458747046(L_8, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_10 = ___info0;
		NullCheck(L_10);
		int32_t L_11 = VideoInfo_get_AudioBitrate_m4289100119(L_10, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_12 = ___info0;
		NullCheck(L_12);
		int32_t L_13 = VideoInfo_get_AdaptiveType_m2103200368(L_12, /*hidden argument*/NULL);
		VideoInfo__ctor_m2543604979(__this, L_1, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32,YoutubeExtractor.VideoType,System.Int32,System.Boolean,YoutubeExtractor.AudioType,System.Int32,YoutubeExtractor.AdaptiveType)
extern "C"  void VideoInfo__ctor_m2543604979 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, int32_t ___videoType1, int32_t ___resolution2, bool ___is3D3, int32_t ___audioType4, int32_t ___audioBitrate5, int32_t ___adaptiveType6, const MethodInfo* method)
{
	{
		int32_t L_0 = ___formatCode0;
		int32_t L_1 = ___videoType1;
		int32_t L_2 = ___resolution2;
		bool L_3 = ___is3D3;
		int32_t L_4 = ___audioType4;
		int32_t L_5 = ___audioBitrate5;
		int32_t L_6 = ___adaptiveType6;
		VideoInfo__ctor_m3233131204(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeExtractor.VideoInfo::.ctor(System.Int32,YoutubeExtractor.VideoType,System.Int32,System.Boolean,YoutubeExtractor.AudioType,System.Int32,YoutubeExtractor.AdaptiveType,System.Int32)
extern "C"  void VideoInfo__ctor_m3233131204 (VideoInfo_t2099471191 * __this, int32_t ___formatCode0, int32_t ___videoType1, int32_t ___resolution2, bool ___is3D3, int32_t ___audioType4, int32_t ___audioBitrate5, int32_t ___adaptiveType6, int32_t ___frameRate7, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___formatCode0;
		VideoInfo_set_FormatCode_m2346426515(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___videoType1;
		VideoInfo_set_VideoType_m2967638225(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___resolution2;
		VideoInfo_set_Resolution_m1832971451(__this, L_2, /*hidden argument*/NULL);
		bool L_3 = ___is3D3;
		VideoInfo_set_Is3D_m3760699024(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___audioType4;
		VideoInfo_set_AudioType_m945538481(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___audioBitrate5;
		VideoInfo_set_AudioBitrate_m3700425798(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___adaptiveType6;
		VideoInfo_set_AdaptiveType_m2963356775(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = ___frameRate7;
		VideoInfo_set_FrameRate_m26869282(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String YoutubeExtractor.VideoInfo::ToString()
extern Il2CppClass* VideoType_t2099809763_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3362083470;
extern const uint32_t VideoInfo_ToString_m1167467600_MetadataUsageId;
extern "C"  String_t* VideoInfo_ToString_m1167467600 (VideoInfo_t2099471191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoInfo_ToString_m1167467600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = VideoInfo_get_VideoType_m2599923248(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(VideoType_t2099809763_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = VideoInfo_get_Resolution_m3843029452(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = VideoInfo_get_Title_m1409263085(__this, /*hidden argument*/NULL);
		String_t* L_7 = VideoInfo_get_VideoExtension_m2516305905(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m138640077(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral3362083470, L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0035;
	}

IL_0035:
	{
		String_t* L_10 = V_0;
		return L_10;
	}
}
// System.Void YoutubeExtractor.VideoInfo::.cctor()
extern Il2CppClass* List_1_t3467656743_il2cpp_TypeInfo_var;
extern Il2CppClass* VideoInfo_t2099471191_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1602161635_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1296522451_MethodInfo_var;
extern const uint32_t VideoInfo__cctor_m1582972714_MetadataUsageId;
extern "C"  void VideoInfo__cctor_m1582972714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoInfo__cctor_m1582972714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3467656743 * V_0 = NULL;
	{
		List_1_t3467656743 * L_0 = (List_1_t3467656743 *)il2cpp_codegen_object_new(List_1_t3467656743_il2cpp_TypeInfo_var);
		List_1__ctor_m1602161635(L_0, /*hidden argument*/List_1__ctor_m1602161635_MethodInfo_var);
		V_0 = L_0;
		List_1_t3467656743 * L_1 = V_0;
		VideoInfo_t2099471191 * L_2 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_2, 5, 1, ((int32_t)240), (bool)0, 1, ((int32_t)64), 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m1296522451(L_1, L_2, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_3 = V_0;
		VideoInfo_t2099471191 * L_4 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_4, 6, 1, ((int32_t)270), (bool)0, 1, ((int32_t)64), 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_Add_m1296522451(L_3, L_4, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_5 = V_0;
		VideoInfo_t2099471191 * L_6 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_6, ((int32_t)13), 0, 0, (bool)0, 0, 0, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m1296522451(L_5, L_6, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_7 = V_0;
		VideoInfo_t2099471191 * L_8 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_8, ((int32_t)17), 0, ((int32_t)144), (bool)0, 0, ((int32_t)24), 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m1296522451(L_7, L_8, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_9 = V_0;
		VideoInfo_t2099471191 * L_10 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_10, ((int32_t)18), 2, ((int32_t)360), (bool)0, 0, ((int32_t)96), 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m1296522451(L_9, L_10, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_11 = V_0;
		VideoInfo_t2099471191 * L_12 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_12, ((int32_t)22), 2, ((int32_t)720), (bool)0, 0, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_m1296522451(L_11, L_12, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_13 = V_0;
		VideoInfo_t2099471191 * L_14 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_14, ((int32_t)34), 1, ((int32_t)360), (bool)0, 0, ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_m1296522451(L_13, L_14, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_15 = V_0;
		VideoInfo_t2099471191 * L_16 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_16, ((int32_t)35), 1, ((int32_t)480), (bool)0, 0, ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_15);
		List_1_Add_m1296522451(L_15, L_16, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_17 = V_0;
		VideoInfo_t2099471191 * L_18 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_18, ((int32_t)36), 0, ((int32_t)240), (bool)0, 0, ((int32_t)38), 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		List_1_Add_m1296522451(L_17, L_18, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_19 = V_0;
		VideoInfo_t2099471191 * L_20 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_20, ((int32_t)37), 2, ((int32_t)1080), (bool)0, 0, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_Add_m1296522451(L_19, L_20, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_21 = V_0;
		VideoInfo_t2099471191 * L_22 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_22, ((int32_t)38), 2, ((int32_t)3072), (bool)0, 0, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_Add_m1296522451(L_21, L_22, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_23 = V_0;
		VideoInfo_t2099471191 * L_24 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_24, ((int32_t)43), 3, ((int32_t)360), (bool)0, 2, ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_m1296522451(L_23, L_24, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_25 = V_0;
		VideoInfo_t2099471191 * L_26 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_26, ((int32_t)44), 3, ((int32_t)480), (bool)0, 2, ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_Add_m1296522451(L_25, L_26, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_27 = V_0;
		VideoInfo_t2099471191 * L_28 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_28, ((int32_t)45), 3, ((int32_t)720), (bool)0, 2, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_27);
		List_1_Add_m1296522451(L_27, L_28, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_29 = V_0;
		VideoInfo_t2099471191 * L_30 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_30, ((int32_t)46), 3, ((int32_t)1080), (bool)0, 2, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_m1296522451(L_29, L_30, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_31 = V_0;
		VideoInfo_t2099471191 * L_32 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_32, ((int32_t)82), 2, ((int32_t)360), (bool)1, 0, ((int32_t)96), 0, /*hidden argument*/NULL);
		NullCheck(L_31);
		List_1_Add_m1296522451(L_31, L_32, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_33 = V_0;
		VideoInfo_t2099471191 * L_34 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_34, ((int32_t)83), 2, ((int32_t)240), (bool)1, 0, ((int32_t)96), 0, /*hidden argument*/NULL);
		NullCheck(L_33);
		List_1_Add_m1296522451(L_33, L_34, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_35 = V_0;
		VideoInfo_t2099471191 * L_36 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_36, ((int32_t)84), 2, ((int32_t)720), (bool)1, 0, ((int32_t)152), 0, /*hidden argument*/NULL);
		NullCheck(L_35);
		List_1_Add_m1296522451(L_35, L_36, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_37 = V_0;
		VideoInfo_t2099471191 * L_38 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_38, ((int32_t)85), 2, ((int32_t)520), (bool)1, 0, ((int32_t)152), 0, /*hidden argument*/NULL);
		NullCheck(L_37);
		List_1_Add_m1296522451(L_37, L_38, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_39 = V_0;
		VideoInfo_t2099471191 * L_40 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_40, ((int32_t)100), 3, ((int32_t)360), (bool)1, 2, ((int32_t)128), 0, /*hidden argument*/NULL);
		NullCheck(L_39);
		List_1_Add_m1296522451(L_39, L_40, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_41 = V_0;
		VideoInfo_t2099471191 * L_42 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_42, ((int32_t)101), 3, ((int32_t)360), (bool)1, 2, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_41);
		List_1_Add_m1296522451(L_41, L_42, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_43 = V_0;
		VideoInfo_t2099471191 * L_44 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_44, ((int32_t)102), 3, ((int32_t)720), (bool)1, 2, ((int32_t)192), 0, /*hidden argument*/NULL);
		NullCheck(L_43);
		List_1_Add_m1296522451(L_43, L_44, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_45 = V_0;
		VideoInfo_t2099471191 * L_46 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_46, ((int32_t)133), 2, ((int32_t)240), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_45);
		List_1_Add_m1296522451(L_45, L_46, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_47 = V_0;
		VideoInfo_t2099471191 * L_48 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_48, ((int32_t)134), 2, ((int32_t)360), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_Add_m1296522451(L_47, L_48, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_49 = V_0;
		VideoInfo_t2099471191 * L_50 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_50, ((int32_t)135), 2, ((int32_t)480), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_49);
		List_1_Add_m1296522451(L_49, L_50, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_51 = V_0;
		VideoInfo_t2099471191 * L_52 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_52, ((int32_t)136), 2, ((int32_t)720), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_51);
		List_1_Add_m1296522451(L_51, L_52, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_53 = V_0;
		VideoInfo_t2099471191 * L_54 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_54, ((int32_t)137), 2, ((int32_t)1080), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_53);
		List_1_Add_m1296522451(L_53, L_54, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_55 = V_0;
		VideoInfo_t2099471191 * L_56 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_56, ((int32_t)138), 2, ((int32_t)2160), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_55);
		List_1_Add_m1296522451(L_55, L_56, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_57 = V_0;
		VideoInfo_t2099471191 * L_58 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_58, ((int32_t)160), 2, ((int32_t)144), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_57);
		List_1_Add_m1296522451(L_57, L_58, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_59 = V_0;
		VideoInfo_t2099471191 * L_60 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_60, ((int32_t)242), 3, ((int32_t)240), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_Add_m1296522451(L_59, L_60, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_61 = V_0;
		VideoInfo_t2099471191 * L_62 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_62, ((int32_t)243), 3, ((int32_t)360), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_61);
		List_1_Add_m1296522451(L_61, L_62, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_63 = V_0;
		VideoInfo_t2099471191 * L_64 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_64, ((int32_t)244), 3, ((int32_t)480), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_63);
		List_1_Add_m1296522451(L_63, L_64, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_65 = V_0;
		VideoInfo_t2099471191 * L_66 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_66, ((int32_t)247), 3, ((int32_t)720), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_65);
		List_1_Add_m1296522451(L_65, L_66, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_67 = V_0;
		VideoInfo_t2099471191 * L_68 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_68, ((int32_t)248), 3, ((int32_t)1080), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_67);
		List_1_Add_m1296522451(L_67, L_68, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_69 = V_0;
		VideoInfo_t2099471191 * L_70 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_70, ((int32_t)264), 2, ((int32_t)1440), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_69);
		List_1_Add_m1296522451(L_69, L_70, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_71 = V_0;
		VideoInfo_t2099471191 * L_72 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_72, ((int32_t)271), 3, ((int32_t)1440), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_71);
		List_1_Add_m1296522451(L_71, L_72, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_73 = V_0;
		VideoInfo_t2099471191 * L_74 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_74, ((int32_t)272), 3, ((int32_t)2160), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_73);
		List_1_Add_m1296522451(L_73, L_74, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_75 = V_0;
		VideoInfo_t2099471191 * L_76 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_76, ((int32_t)278), 3, ((int32_t)144), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_75);
		List_1_Add_m1296522451(L_75, L_76, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_77 = V_0;
		VideoInfo_t2099471191 * L_78 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_78, ((int32_t)302), 3, ((int32_t)720), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_77);
		List_1_Add_m1296522451(L_77, L_78, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_79 = V_0;
		VideoInfo_t2099471191 * L_80 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_80, ((int32_t)303), 3, ((int32_t)1080), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_Add_m1296522451(L_79, L_80, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_81 = V_0;
		VideoInfo_t2099471191 * L_82 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_82, ((int32_t)308), 3, ((int32_t)1440), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_81);
		List_1_Add_m1296522451(L_81, L_82, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_83 = V_0;
		VideoInfo_t2099471191 * L_84 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_84, ((int32_t)313), 3, ((int32_t)2160), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_83);
		List_1_Add_m1296522451(L_83, L_84, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_85 = V_0;
		VideoInfo_t2099471191 * L_86 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_86, ((int32_t)315), 3, ((int32_t)2160), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_85);
		List_1_Add_m1296522451(L_85, L_86, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_87 = V_0;
		VideoInfo_t2099471191 * L_88 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_88, ((int32_t)298), 2, ((int32_t)720), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_87);
		List_1_Add_m1296522451(L_87, L_88, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_89 = V_0;
		VideoInfo_t2099471191 * L_90 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m3233131204(L_90, ((int32_t)299), 2, ((int32_t)1080), (bool)0, 4, 0, 2, ((int32_t)60), /*hidden argument*/NULL);
		NullCheck(L_89);
		List_1_Add_m1296522451(L_89, L_90, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_91 = V_0;
		VideoInfo_t2099471191 * L_92 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_92, ((int32_t)266), 2, ((int32_t)2160), (bool)0, 4, 0, 2, /*hidden argument*/NULL);
		NullCheck(L_91);
		List_1_Add_m1296522451(L_91, L_92, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_93 = V_0;
		VideoInfo_t2099471191 * L_94 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_94, ((int32_t)139), 2, 0, (bool)0, 0, ((int32_t)48), 1, /*hidden argument*/NULL);
		NullCheck(L_93);
		List_1_Add_m1296522451(L_93, L_94, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_95 = V_0;
		VideoInfo_t2099471191 * L_96 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_96, ((int32_t)140), 2, 0, (bool)0, 0, ((int32_t)128), 1, /*hidden argument*/NULL);
		NullCheck(L_95);
		List_1_Add_m1296522451(L_95, L_96, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_97 = V_0;
		VideoInfo_t2099471191 * L_98 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_98, ((int32_t)141), 2, 0, (bool)0, 0, ((int32_t)256), 1, /*hidden argument*/NULL);
		NullCheck(L_97);
		List_1_Add_m1296522451(L_97, L_98, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_99 = V_0;
		VideoInfo_t2099471191 * L_100 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_100, ((int32_t)171), 3, 0, (bool)0, 2, ((int32_t)128), 1, /*hidden argument*/NULL);
		NullCheck(L_99);
		List_1_Add_m1296522451(L_99, L_100, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_101 = V_0;
		VideoInfo_t2099471191 * L_102 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_102, ((int32_t)172), 3, 0, (bool)0, 2, ((int32_t)192), 1, /*hidden argument*/NULL);
		NullCheck(L_101);
		List_1_Add_m1296522451(L_101, L_102, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_103 = V_0;
		VideoInfo_t2099471191 * L_104 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_104, ((int32_t)249), 3, 0, (bool)0, 3, ((int32_t)50), 1, /*hidden argument*/NULL);
		NullCheck(L_103);
		List_1_Add_m1296522451(L_103, L_104, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_105 = V_0;
		VideoInfo_t2099471191 * L_106 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_106, ((int32_t)250), 3, 0, (bool)0, 3, ((int32_t)70), 1, /*hidden argument*/NULL);
		NullCheck(L_105);
		List_1_Add_m1296522451(L_105, L_106, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_107 = V_0;
		VideoInfo_t2099471191 * L_108 = (VideoInfo_t2099471191 *)il2cpp_codegen_object_new(VideoInfo_t2099471191_il2cpp_TypeInfo_var);
		VideoInfo__ctor_m2543604979(L_108, ((int32_t)251), 3, 0, (bool)0, 3, ((int32_t)160), 1, /*hidden argument*/NULL);
		NullCheck(L_107);
		List_1_Add_m1296522451(L_107, L_108, /*hidden argument*/List_1_Add_m1296522451_MethodInfo_var);
		List_1_t3467656743 * L_109 = V_0;
		((VideoInfo_t2099471191_StaticFields*)VideoInfo_t2099471191_il2cpp_TypeInfo_var->static_fields)->set_Defaults_0(L_109);
		return;
	}
}
// System.Void YoutubeExtractor.VideoNotAvailableException::.ctor()
extern "C"  void VideoNotAvailableException__ctor_m1707482460 (VideoNotAvailableException_t1343733958 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m3223090658(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeExtractor.VideoNotAvailableException::.ctor(System.String)
extern "C"  void VideoNotAvailableException__ctor_m3454474022 (VideoNotAvailableException_t1343733958 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeExtractor.YoutubeParseException::.ctor(System.String,System.Exception)
extern "C"  void YoutubeParseException__ctor_m627752129 (YoutubeParseException_t1485001709 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		Exception__ctor_m1328171222(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
