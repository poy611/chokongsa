﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4163934672.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2938557483.h"
#include "mscorlib_System_IntPtr4010401971.h"

// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerParticipant_MultiplayerParticipant_Status_m4290365423 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchRank(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerParticipant_MultiplayerParticipant_MatchRank_m1891349798 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_IsConnectedToRoom(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_IsConnectedToRoom_m2181604547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_DisplayName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_DisplayName_m1235596894 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasPlayer(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_HasPlayer_m2153678729 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_AvatarUrl_m2286680377 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___resolution1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchResult(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerParticipant_MultiplayerParticipant_MatchResult_m3302970052 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Player(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t MultiplayerParticipant_MultiplayerParticipant_Player_m3369922382 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MultiplayerParticipant_MultiplayerParticipant_Dispose_m4157444057 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_Valid_m3857586218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasMatchResult(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_HasMatchResult_m3021916118 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_Id_m3448685496 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
