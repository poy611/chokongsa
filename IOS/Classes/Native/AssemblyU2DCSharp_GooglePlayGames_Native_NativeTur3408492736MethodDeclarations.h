﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey88
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey88_t3408492736;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t3573041681;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl3573041681.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey88::.ctor()
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey88__ctor_m4051853995 (U3CCreateWithInvitationScreenU3Ec__AnonStorey88_t3408492736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey88::<>m__77(GooglePlayGames.BasicApi.UIStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch)
extern "C"  void U3CCreateWithInvitationScreenU3Ec__AnonStorey88_U3CU3Em__77_m3451149169 (U3CCreateWithInvitationScreenU3Ec__AnonStorey88_t3408492736 * __this, int32_t ___status0, TurnBasedMatch_t3573041681 * ___match1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
