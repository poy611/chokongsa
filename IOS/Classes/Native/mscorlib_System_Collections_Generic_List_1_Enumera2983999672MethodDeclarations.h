﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3112258819(__this, ___l0, method) ((  void (*) (Enumerator_t2983999672 *, List_1_t2964326902 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2495377775(__this, method) ((  void (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3327698341(__this, method) ((  Il2CppObject * (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::Dispose()
#define Enumerator_Dispose_m1996688296(__this, method) ((  void (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::VerifyState()
#define Enumerator_VerifyState_m1257813729(__this, method) ((  void (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::MoveNext()
#define Enumerator_MoveNext_m2979246026(__this, method) ((  bool (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLog>::get_Current()
#define Enumerator_get_Current_m1351573970(__this, method) ((  FsmLog_t1596141350 * (*) (Enumerator_t2983999672 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
