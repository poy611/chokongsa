﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3971612065.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatFormatHandlin3887485542.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1062732401_gshared (Nullable_1_t3971612065 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1062732401(__this, ___value0, method) ((  void (*) (Nullable_1_t3971612065 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1062732401_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3894084462_gshared (Nullable_1_t3971612065 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3894084462(__this, method) ((  bool (*) (Nullable_1_t3971612065 *, const MethodInfo*))Nullable_1_get_HasValue_m3894084462_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3709207507_gshared (Nullable_1_t3971612065 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3709207507(__this, method) ((  int32_t (*) (Nullable_1_t3971612065 *, const MethodInfo*))Nullable_1_get_Value_m3709207507_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m140595771_gshared (Nullable_1_t3971612065 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m140595771(__this, ___other0, method) ((  bool (*) (Nullable_1_t3971612065 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m140595771_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1564825764_gshared (Nullable_1_t3971612065 * __this, Nullable_1_t3971612065  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1564825764(__this, ___other0, method) ((  bool (*) (Nullable_1_t3971612065 *, Nullable_1_t3971612065 , const MethodInfo*))Nullable_1_Equals_m1564825764_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m4183136799_gshared (Nullable_1_t3971612065 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m4183136799(__this, method) ((  int32_t (*) (Nullable_1_t3971612065 *, const MethodInfo*))Nullable_1_GetHashCode_m4183136799_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m218359005_gshared (Nullable_1_t3971612065 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m218359005(__this, method) ((  int32_t (*) (Nullable_1_t3971612065 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m218359005_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1559794160_gshared (Nullable_1_t3971612065 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1559794160(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3971612065 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1559794160_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3916221895_gshared (Nullable_1_t3971612065 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3916221895(__this, method) ((  String_t* (*) (Nullable_1_t3971612065 *, const MethodInfo*))Nullable_1_ToString_m3916221895_gshared)(__this, method)
