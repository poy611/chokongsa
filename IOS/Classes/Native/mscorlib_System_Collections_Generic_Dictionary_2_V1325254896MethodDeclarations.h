﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1816121769(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1325254896 *, Dictionary_2_t2624649183 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2283344233(__this, ___item0, method) ((  void (*) (ValueCollection_t1325254896 *, Participant_t1804230813 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3482983090(__this, method) ((  void (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3845173025(__this, ___item0, method) ((  bool (*) (ValueCollection_t1325254896 *, Participant_t1804230813 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1761418246(__this, ___item0, method) ((  bool (*) (ValueCollection_t1325254896 *, Participant_t1804230813 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2710415090(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2759623990(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1325254896 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3018728901(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2120348884(__this, method) ((  bool (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1899092148(__this, method) ((  bool (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m611720358(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2354099120(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1325254896 *, ParticipantU5BU5D_t366346320*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3234058239(__this, method) ((  Enumerator_t556482591  (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Count()
#define ValueCollection_get_Count_m2063807214(__this, method) ((  int32_t (*) (ValueCollection_t1325254896 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
