﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionEuler
struct QuaternionEuler_t1604107257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::.ctor()
extern "C"  void QuaternionEuler__ctor_m2978669405 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::Reset()
extern "C"  void QuaternionEuler_Reset_m625102346 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnEnter()
extern "C"  void QuaternionEuler_OnEnter_m1444807668 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnUpdate()
extern "C"  void QuaternionEuler_OnUpdate_m972924111 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnLateUpdate()
extern "C"  void QuaternionEuler_OnLateUpdate_m3091360533 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnFixedUpdate()
extern "C"  void QuaternionEuler_OnFixedUpdate_m2218841337 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::DoQuatEuler()
extern "C"  void QuaternionEuler_DoQuatEuler_m260547394 (QuaternionEuler_t1604107257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
