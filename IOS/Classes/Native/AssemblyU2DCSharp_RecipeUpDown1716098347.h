﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecipeUpDown
struct  RecipeUpDown_t1716098347  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject RecipeUpDown::obj
	GameObject_t3674682005 * ___obj_2;
	// UnityEngine.Transform RecipeUpDown::pos1
	Transform_t1659122786 * ___pos1_3;
	// UnityEngine.Transform RecipeUpDown::pos2
	Transform_t1659122786 * ___pos2_4;
	// UnityEngine.UI.Image RecipeUpDown::buttonImage
	Image_t538875265 * ___buttonImage_5;
	// UnityEngine.Sprite[] RecipeUpDown::_btImg
	SpriteU5BU5D_t2761310900* ____btImg_6;
	// System.Boolean RecipeUpDown::isUp
	bool ___isUp_7;

public:
	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ___obj_2)); }
	inline GameObject_t3674682005 * get_obj_2() const { return ___obj_2; }
	inline GameObject_t3674682005 ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(GameObject_t3674682005 * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier(&___obj_2, value);
	}

	inline static int32_t get_offset_of_pos1_3() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ___pos1_3)); }
	inline Transform_t1659122786 * get_pos1_3() const { return ___pos1_3; }
	inline Transform_t1659122786 ** get_address_of_pos1_3() { return &___pos1_3; }
	inline void set_pos1_3(Transform_t1659122786 * value)
	{
		___pos1_3 = value;
		Il2CppCodeGenWriteBarrier(&___pos1_3, value);
	}

	inline static int32_t get_offset_of_pos2_4() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ___pos2_4)); }
	inline Transform_t1659122786 * get_pos2_4() const { return ___pos2_4; }
	inline Transform_t1659122786 ** get_address_of_pos2_4() { return &___pos2_4; }
	inline void set_pos2_4(Transform_t1659122786 * value)
	{
		___pos2_4 = value;
		Il2CppCodeGenWriteBarrier(&___pos2_4, value);
	}

	inline static int32_t get_offset_of_buttonImage_5() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ___buttonImage_5)); }
	inline Image_t538875265 * get_buttonImage_5() const { return ___buttonImage_5; }
	inline Image_t538875265 ** get_address_of_buttonImage_5() { return &___buttonImage_5; }
	inline void set_buttonImage_5(Image_t538875265 * value)
	{
		___buttonImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonImage_5, value);
	}

	inline static int32_t get_offset_of__btImg_6() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ____btImg_6)); }
	inline SpriteU5BU5D_t2761310900* get__btImg_6() const { return ____btImg_6; }
	inline SpriteU5BU5D_t2761310900** get_address_of__btImg_6() { return &____btImg_6; }
	inline void set__btImg_6(SpriteU5BU5D_t2761310900* value)
	{
		____btImg_6 = value;
		Il2CppCodeGenWriteBarrier(&____btImg_6, value);
	}

	inline static int32_t get_offset_of_isUp_7() { return static_cast<int32_t>(offsetof(RecipeUpDown_t1716098347, ___isUp_7)); }
	inline bool get_isUp_7() const { return ___isUp_7; }
	inline bool* get_address_of_isUp_7() { return &___isUp_7; }
	inline void set_isUp_7(bool value)
	{
		___isUp_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
