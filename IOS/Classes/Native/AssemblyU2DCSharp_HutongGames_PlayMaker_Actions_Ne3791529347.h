﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetPlayerPing
struct  NetworkGetPlayerPing_t3791529347  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::playerIndex
	FsmInt_t1596138449 * ___playerIndex_11;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::cachePlayerReference
	bool ___cachePlayerReference_12;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::everyFrame
	bool ___everyFrame_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::averagePing
	FsmInt_t1596138449 * ___averagePing_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::PlayerNotFoundEvent
	FsmEvent_t2133468028 * ___PlayerNotFoundEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::PlayerFoundEvent
	FsmEvent_t2133468028 * ___PlayerFoundEvent_16;
	// UnityEngine.NetworkPlayer HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::_player
	NetworkPlayer_t3231273765  ____player_17;

public:
	inline static int32_t get_offset_of_playerIndex_11() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___playerIndex_11)); }
	inline FsmInt_t1596138449 * get_playerIndex_11() const { return ___playerIndex_11; }
	inline FsmInt_t1596138449 ** get_address_of_playerIndex_11() { return &___playerIndex_11; }
	inline void set_playerIndex_11(FsmInt_t1596138449 * value)
	{
		___playerIndex_11 = value;
		Il2CppCodeGenWriteBarrier(&___playerIndex_11, value);
	}

	inline static int32_t get_offset_of_cachePlayerReference_12() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___cachePlayerReference_12)); }
	inline bool get_cachePlayerReference_12() const { return ___cachePlayerReference_12; }
	inline bool* get_address_of_cachePlayerReference_12() { return &___cachePlayerReference_12; }
	inline void set_cachePlayerReference_12(bool value)
	{
		___cachePlayerReference_12 = value;
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_averagePing_14() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___averagePing_14)); }
	inline FsmInt_t1596138449 * get_averagePing_14() const { return ___averagePing_14; }
	inline FsmInt_t1596138449 ** get_address_of_averagePing_14() { return &___averagePing_14; }
	inline void set_averagePing_14(FsmInt_t1596138449 * value)
	{
		___averagePing_14 = value;
		Il2CppCodeGenWriteBarrier(&___averagePing_14, value);
	}

	inline static int32_t get_offset_of_PlayerNotFoundEvent_15() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___PlayerNotFoundEvent_15)); }
	inline FsmEvent_t2133468028 * get_PlayerNotFoundEvent_15() const { return ___PlayerNotFoundEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_PlayerNotFoundEvent_15() { return &___PlayerNotFoundEvent_15; }
	inline void set_PlayerNotFoundEvent_15(FsmEvent_t2133468028 * value)
	{
		___PlayerNotFoundEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerNotFoundEvent_15, value);
	}

	inline static int32_t get_offset_of_PlayerFoundEvent_16() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ___PlayerFoundEvent_16)); }
	inline FsmEvent_t2133468028 * get_PlayerFoundEvent_16() const { return ___PlayerFoundEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_PlayerFoundEvent_16() { return &___PlayerFoundEvent_16; }
	inline void set_PlayerFoundEvent_16(FsmEvent_t2133468028 * value)
	{
		___PlayerFoundEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerFoundEvent_16, value);
	}

	inline static int32_t get_offset_of__player_17() { return static_cast<int32_t>(offsetof(NetworkGetPlayerPing_t3791529347, ____player_17)); }
	inline NetworkPlayer_t3231273765  get__player_17() const { return ____player_17; }
	inline NetworkPlayer_t3231273765 * get_address_of__player_17() { return &____player_17; }
	inline void set__player_17(NetworkPlayer_t3231273765  value)
	{
		____player_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
