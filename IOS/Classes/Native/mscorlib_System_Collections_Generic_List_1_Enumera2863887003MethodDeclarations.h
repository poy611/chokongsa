﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4248676451(__this, ___l0, method) ((  void (*) (Enumerator_t2863887003 *, List_1_t2844214233 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1389132303(__this, method) ((  void (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2728436933(__this, method) ((  Il2CppObject * (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::Dispose()
#define Enumerator_Dispose_m1535393544(__this, method) ((  void (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::VerifyState()
#define Enumerator_VerifyState_m3673242177(__this, method) ((  void (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::MoveNext()
#define Enumerator_MoveNext_m2509669951(__this, method) ((  bool (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.Serialization.SerializationErrorCallback>::get_Current()
#define Enumerator_get_Current_m492239066(__this, method) ((  SerializationErrorCallback_t1476028681 * (*) (Enumerator_t2863887003 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
