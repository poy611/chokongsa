﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Translate
struct  Translate_t1163392156  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Translate::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Translate::vector
	FsmVector3_t533912882 * ___vector_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::x
	FsmFloat_t2134102846 * ___x_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::y
	FsmFloat_t2134102846 * ___y_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::z
	FsmFloat_t2134102846 * ___z_15;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Translate::space
	int32_t ___space_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::perSecond
	bool ___perSecond_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::everyFrame
	bool ___everyFrame_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::lateUpdate
	bool ___lateUpdate_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::fixedUpdate
	bool ___fixedUpdate_20;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_vector_12() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___vector_12)); }
	inline FsmVector3_t533912882 * get_vector_12() const { return ___vector_12; }
	inline FsmVector3_t533912882 ** get_address_of_vector_12() { return &___vector_12; }
	inline void set_vector_12(FsmVector3_t533912882 * value)
	{
		___vector_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector_12, value);
	}

	inline static int32_t get_offset_of_x_13() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___x_13)); }
	inline FsmFloat_t2134102846 * get_x_13() const { return ___x_13; }
	inline FsmFloat_t2134102846 ** get_address_of_x_13() { return &___x_13; }
	inline void set_x_13(FsmFloat_t2134102846 * value)
	{
		___x_13 = value;
		Il2CppCodeGenWriteBarrier(&___x_13, value);
	}

	inline static int32_t get_offset_of_y_14() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___y_14)); }
	inline FsmFloat_t2134102846 * get_y_14() const { return ___y_14; }
	inline FsmFloat_t2134102846 ** get_address_of_y_14() { return &___y_14; }
	inline void set_y_14(FsmFloat_t2134102846 * value)
	{
		___y_14 = value;
		Il2CppCodeGenWriteBarrier(&___y_14, value);
	}

	inline static int32_t get_offset_of_z_15() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___z_15)); }
	inline FsmFloat_t2134102846 * get_z_15() const { return ___z_15; }
	inline FsmFloat_t2134102846 ** get_address_of_z_15() { return &___z_15; }
	inline void set_z_15(FsmFloat_t2134102846 * value)
	{
		___z_15 = value;
		Il2CppCodeGenWriteBarrier(&___z_15, value);
	}

	inline static int32_t get_offset_of_space_16() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___space_16)); }
	inline int32_t get_space_16() const { return ___space_16; }
	inline int32_t* get_address_of_space_16() { return &___space_16; }
	inline void set_space_16(int32_t value)
	{
		___space_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___perSecond_17)); }
	inline bool get_perSecond_17() const { return ___perSecond_17; }
	inline bool* get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(bool value)
	{
		___perSecond_17 = value;
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_19() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___lateUpdate_19)); }
	inline bool get_lateUpdate_19() const { return ___lateUpdate_19; }
	inline bool* get_address_of_lateUpdate_19() { return &___lateUpdate_19; }
	inline void set_lateUpdate_19(bool value)
	{
		___lateUpdate_19 = value;
	}

	inline static int32_t get_offset_of_fixedUpdate_20() { return static_cast<int32_t>(offsetof(Translate_t1163392156, ___fixedUpdate_20)); }
	inline bool get_fixedUpdate_20() const { return ___fixedUpdate_20; }
	inline bool* get_address_of_fixedUpdate_20() { return &___fixedUpdate_20; }
	inline void set_fixedUpdate_20(bool value)
	{
		___fixedUpdate_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
