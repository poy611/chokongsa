﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SceneindexInc
struct SceneindexInc_t2309712408;
// ScoreManager_Grinding
struct ScoreManager_Grinding_t1076868946;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// ScoreManager_Grinding/<confirm>c__Iterator1A
struct U3CconfirmU3Ec__Iterator1A_t3696604900;
// System.Object
struct Il2CppObject;
// ScoreManager_Icing
struct ScoreManager_Icing_t3849909252;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// ScoreManager_Icing/<Finish>c__Iterator1B
struct U3CFinishU3Ec__Iterator1B_t1048149352;
// ScoreManager_Icing/<TimeEnd>c__Iterator1D
struct U3CTimeEndU3Ec__Iterator1D_t664746339;
// ScoreManager_Icing/<TimerCheck>c__Iterator1C
struct U3CTimerCheckU3Ec__Iterator1C_t3854228761;
// ScoreManager_Lobby
struct ScoreManager_Lobby_t3853030226;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3726781579;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t4039083868;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// System.String
struct String_t;
// UnityEngine.UI.Button
struct Button_t3896396478;
// ScoreManager_Lobby/<RS>c__Iterator21
struct U3CRSU3Ec__Iterator21_t4268541650;
// ScoreManager_Lobby/<TimeEnd>c__Iterator1F
struct U3CTimeEndU3Ec__Iterator1F_t583418455;
// ScoreManager_Lobby/<TimerCheck>c__Iterator1E
struct U3CTimerCheckU3Ec__Iterator1E_t3376731881;
// ScoreManager_Lobby/<wait>c__Iterator20
struct U3CwaitU3Ec__Iterator20_t1632431941;
// ScoreManager_Temping
struct ScoreManager_Temping_t3029572618;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// UnityEngine.UI.Image
struct Image_t538875265;
// TempingPointer
struct TempingPointer_t2678149871;
// ScoreManager_Temping/<REandSt>c__Iterator22
struct U3CREandStU3Ec__Iterator22_t438786049;
// ScoreManager_Temping/<TimeEnd>c__Iterator24
struct U3CTimeEndU3Ec__Iterator24_t3910199980;
// ScoreManager_Temping/<TimerCheck>c__Iterator23
struct U3CTimerCheckU3Ec__Iterator23_t859433390;
// ScoreManager_Variation
struct ScoreManager_Variation_t1665153935;
// ScoreManager_Variation/<CheckIngredient>c__Iterator25
struct U3CCheckIngredientU3Ec__Iterator25_t55506035;
// ScoreManager_Variation/<TimeEnd>c__Iterator27
struct U3CTimeEndU3Ec__Iterator27_t1567663498;
// ScoreManager_Variation/<TimerCheck>c__Iterator26
struct U3CTimerCheckU3Ec__Iterator26_t3278601462;
// ScoreManagerMiniGame
struct ScoreManagerMiniGame_t3434204900;
// SeekBarCtrl
struct SeekBarCtrl_t3766823782;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// Serving
struct Serving_t3648806892;
// ResultData
struct ResultData_t1421128071;
// Serving/<okNet>c__Iterator2B
struct U3CokNetU3Ec__Iterator2B_t599730315;
// Serving/<onStart>c__Iterator28
struct U3ConStartU3Ec__Iterator28_t3665343203;
// Serving/<ShowData>c__Iterator29
struct U3CShowDataU3Ec__Iterator29_t2136060378;
// Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A
struct U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680;
// ShowRecipe
struct ShowRecipe_t2733639435;
// ShowRecipePre
struct ShowRecipePre_t977589144;
// ShowRecipePre/<enableCoru>c__Iterator2C
struct U3CenableCoruU3Ec__Iterator2C_t1488953467;
// ShowRecipePre/<FocusDown>c__Iterator2D
struct U3CFocusDownU3Ec__Iterator2D_t659480186;
// SoundManage
struct SoundManage_t3403985716;
// SphereMirror
struct SphereMirror_t167396876;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// start_mini_game
struct start_mini_game_t3518442301;
// StartFirst
struct StartFirst_t396912526;
// StartFirst/<BlackBlink>c__Iterator2E
struct U3CBlackBlinkU3Ec__Iterator2E_t2642540702;
// StartImageBGMS
struct StartImageBGMS_t2059898468;
// StartImageDisapear
struct StartImageDisapear_t2918831698;
// StructforMinigame
struct StructforMinigame_t1286533533;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;
// TempingLastObj
struct TempingLastObj_t3031672627;
// TempingPointer/<MainCameraMove>c__Iterator2F
struct U3CMainCameraMoveU3Ec__Iterator2F_t3998083026;
// TempingPointer/<TempingSlideDown>c__Iterator31
struct U3CTempingSlideDownU3Ec__Iterator31_t3382326578;
// TempingPointer/<TempingSlideUp>c__Iterator30
struct U3CTempingSlideUpU3Ec__Iterator30_t3867951914;
// TimerPref
struct TimerPref_t2057114344;
// TrackMouseMovement
struct TrackMouseMovement_t1693767945;
// UserExperience
struct UserExperience_t3961908149;
// UserId
struct UserId_t2542803558;
// UserInfo
struct UserInfo_t4092807993;
// UserInfoCall
struct UserInfoCall_t3868783927;
// UserInfoType
struct UserInfoType_t3869313555;
// VariationResultWindow
struct VariationResultWindow_t1568540672;
// YoutubeEasyMovieTexture
struct YoutubeEasyMovieTexture_t1195908624;
// YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33
struct U3CCheckPlayU3Ec__Iterator33_t4170198610;
// YoutubeStrim
struct YoutubeStrim_t2074917330;
// YoutubeVideo
struct YoutubeVideo_t2077346616;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SCENE78717036.h"
#include "AssemblyU2DCSharp_SCENE78717036MethodDeclarations.h"
#include "AssemblyU2DCSharp_SceneindexInc2309712408.h"
#include "AssemblyU2DCSharp_SceneindexInc2309712408MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding1076868946.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding1076868946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "AssemblyU2DCSharp_StructforMinigame1286533533MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen2778891315MethodDeclarations.h"
#include "AssemblyU2DCSharp_ConstforMinigame2789090703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1080795294MethodDeclarations.h"
#include "AssemblyU2DCSharp_StructforMinigame1286533533.h"
#include "AssemblyU2DCSharp_MainUserInfo2526075922.h"
#include "AssemblyU2DCSharp_ConstforMinigame2789090703.h"
#include "AssemblyU2DCSharp_PopupManagement282433007.h"
#include "AssemblyU2DCSharp_PopupManagement282433007MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManage3403985716MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManage3403985716.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding_U3Cconfirm3696604900MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Grinding_U3Cconfirm3696604900.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime3698499994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs1845493509MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime3698499994.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PP_popupIndex3545488117.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing3849909252.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing3849909252MethodDeclarations.h"
#include "System_System_Diagnostics_Stopwatch3420517611MethodDeclarations.h"
#include "System_System_Diagnostics_Stopwatch3420517611.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CFinishU3Ec1048149352MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CFinishU3Ec1048149352.h"
#include "AssemblyU2DCSharp_RecipeList1641733996MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowRecipePre977589144MethodDeclarations.h"
#include "PlayMaker_PlayMakerFSM3799847376MethodDeclarations.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426MethodDeclarations.h"
#include "AssemblyU2DCSharp_RecipeList1641733996.h"
#include "AssemblyU2DCSharp_ShowRecipePre977589144.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimerCheck3854228761MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimerCheck3854228761.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimeEndU3Ec664746339MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Icing_U3CTimeEndU3Ec664746339.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_Double3868226565.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby3853030226.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby3853030226MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen969614734MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1907060817MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_menu3396474364.h"
#include "mscorlib_System_Collections_Generic_List_1_gen969614734.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1907060817.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimerCheck3376731881MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimerCheck3376731881.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimeEndU3Ec583418455MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CTimeEndU3Ec583418455.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1199519538MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSMng946704145MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton_1_gen1768030745MethodDeclarations.h"
#include "AssemblyU2DCSharp_NetworkMng1515215352MethodDeclarations.h"
#include "AssemblyU2DCSharp_GPGSMng946704145.h"
#include "AssemblyU2DCSharp_NetworkMng1515215352.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CwaitU3Ec__1632431941MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CwaitU3Ec__1632431941.h"
#include "UnityEngine_UnityEngine_Random3156561159MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources2918352667MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1885181538MethodDeclarations.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock508458230.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock508458230MethodDeclarations.h"
#include "AssemblyU2DCSharp_LobbyTalkBoxAnimation432405659MethodDeclarations.h"
#include "AssemblyU2DCSharp_LobbyDeskAnimation4061140181MethodDeclarations.h"
#include "AssemblyU2DCSharp_LobbyTalkBoxAnimation432405659.h"
#include "AssemblyU2DCSharp_LobbyDeskAnimation4061140181.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CRSU3Ec__It4268541650MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_U3CRSU3Ec__It4268541650.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_menu3396474364MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping3029572618.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping3029572618MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CREandStU3438786049MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CREandStU3438786049.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimerChec859433390MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimerChec859433390.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimeEndU3910199980MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Temping_U3CTimeEndU3910199980.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_TempingPointer2678149871MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer2678149871.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation1665153935.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation1665153935MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CCheckIng55506035MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CCheckIng55506035.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimerC3278601462MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimerC3278601462.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimeEn1567663498MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreManager_Variation_U3CTimeEn1567663498.h"
#include "AssemblyU2DCSharp_ScoreManagerMiniGame3434204900.h"
#include "AssemblyU2DCSharp_ScoreManagerMiniGame3434204900MethodDeclarations.h"
#include "AssemblyU2DCSharp_SeekBarCtrl3766823782.h"
#include "AssemblyU2DCSharp_SeekBarCtrl3766823782MethodDeclarations.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536MethodDeclarations.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl3572035536.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "AssemblyU2DCSharp_Serving3648806892.h"
#include "AssemblyU2DCSharp_Serving3648806892MethodDeclarations.h"
#include "AssemblyU2DCSharp_ResultData1421128071.h"
#include "AssemblyU2DCSharp_Serving_U3ConStartU3Ec__Iterator3665343203MethodDeclarations.h"
#include "AssemblyU2DCSharp_Serving_U3ConStartU3Ec__Iterator3665343203.h"
#include "mscorlib_System_Collections_Generic_List_1_gen199460084MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen199460084.h"
#include "AssemblyU2DCSharp_ResultSaveData3126241828.h"
#include "AssemblyU2DCSharp_Serving_U3CShowDataU3Ec__Iterato2136060378MethodDeclarations.h"
#include "AssemblyU2DCSharp_Serving_U3CShowDataU3Ec__Iterato2136060378.h"
#include "AssemblyU2DCSharp_Serving_U3CShowmetheCoffeSizeUpC3955755680MethodDeclarations.h"
#include "AssemblyU2DCSharp_Serving_U3CShowmetheCoffeSizeUpC3955755680.h"
#include "AssemblyU2DCSharp_Singleton_1_gen2764559344MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameStepManager2511743951MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameStepManager2511743951.h"
#include "AssemblyU2DCSharp_Serving_U3CokNetU3Ec__Iterator2B599730315MethodDeclarations.h"
#include "AssemblyU2DCSharp_Serving_U3CokNetU3Ec__Iterator2B599730315.h"
#include "AssemblyU2DCSharp_GradeDataInfo1480749135MethodDeclarations.h"
#include "AssemblyU2DCSharp_MenuList3755595453MethodDeclarations.h"
#include "AssemblyU2DCSharp_GradeDataInfo1480749135.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_GradeData483491841.h"
#include "AssemblyU2DCSharp_MenuList3755595453.h"
#include "AssemblyU2DCSharp_SFM82010.h"
#include "AssemblyU2DCSharp_SFM82010MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowRecipe2733639435.h"
#include "AssemblyU2DCSharp_ShowRecipe2733639435MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ingredient1787055601MethodDeclarations.h"
#include "AssemblyU2DCSharp_Ingredient1787055601.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1375417109.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CenableCoruU3Ec_1488953467MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CenableCoruU3Ec_1488953467.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CFocusDownU3Ec__I659480186MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShowRecipePre_U3CFocusDownU3Ec__I659480186.h"
#include "AssemblyU2DCSharp_RecipeUpDown1716098347MethodDeclarations.h"
#include "AssemblyU2DCSharp_RecipeUpDown1716098347.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "AssemblyU2DCSharp_SphereMirror167396876.h"
#include "AssemblyU2DCSharp_SphereMirror167396876MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "AssemblyU2DCSharp_start_mini_game3518442301.h"
#include "AssemblyU2DCSharp_start_mini_game3518442301MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartFirst396912526.h"
#include "AssemblyU2DCSharp_StartFirst396912526MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartFirst_U3CBlackBlinkU3Ec__It2642540702MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartFirst_U3CBlackBlinkU3Ec__It2642540702.h"
#include "AssemblyU2DCSharp_StartImageBGMS2059898468.h"
#include "AssemblyU2DCSharp_StartImageBGMS2059898468MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartImageDisapear2918831698.h"
#include "AssemblyU2DCSharp_StartImageDisapear2918831698MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "AssemblyU2DCSharp_StructforPopup2315336792.h"
#include "AssemblyU2DCSharp_StructforPopup2315336792MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingLastObj3031672627.h"
#include "AssemblyU2DCSharp_TempingLastObj3031672627MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CMainCameraMove3998083026MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CMainCameraMove3998083026.h"
#include "AssemblyU2DCSharp_iTween3087282050MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideUp3867951914MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideUp3867951914.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideDo3382326578MethodDeclarations.h"
#include "AssemblyU2DCSharp_TempingPointer_U3CTempingSlideDo3382326578.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "mscorlib_System_Collections_Hashtable1407064410.h"
#include "AssemblyU2DCSharp_TimerPref2057114344.h"
#include "AssemblyU2DCSharp_TimerPref2057114344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh2567681854.h"
#include "AssemblyU2DCSharp_TrackMouseMovement1693767945.h"
#include "AssemblyU2DCSharp_TrackMouseMovement1693767945MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserExperience3961908149.h"
#include "AssemblyU2DCSharp_UserExperience3961908149MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserId2542803558.h"
#include "AssemblyU2DCSharp_UserId2542803558MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserInfo4092807993.h"
#include "AssemblyU2DCSharp_UserInfo4092807993MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserInfoCall3868783927.h"
#include "AssemblyU2DCSharp_UserInfoCall3868783927MethodDeclarations.h"
#include "AssemblyU2DCSharp_UserInfoType3869313555.h"
#include "AssemblyU2DCSharp_UserInfoType3869313555MethodDeclarations.h"
#include "AssemblyU2DCSharp_VariationResultWindow1568540672.h"
#include "AssemblyU2DCSharp_VariationResultWindow1568540672MethodDeclarations.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture1195908624.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture1195908624MethodDeclarations.h"
#include "AssemblyU2DCSharp_YoutubeVideo2077346616MethodDeclarations.h"
#include "AssemblyU2DCSharp_YoutubeVideo2077346616.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT1488282328.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture_U3CCheck4170198610MethodDeclarations.h"
#include "AssemblyU2DCSharp_YoutubeEasyMovieTexture_U3CCheck4170198610.h"
#include "AssemblyU2DCSharp_YoutubeStrim2074917330.h"
#include "AssemblyU2DCSharp_YoutubeStrim2074917330MethodDeclarations.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657MethodDeclarations.h"
#include "System_System_Net_ServicePointManager165502476MethodDeclarations.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_DownloadUrlResolver3917844667MethodDeclarations.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191MethodDeclarations.h"
#include "System_System_Uri1116831938.h"
#include "YoutubeUnity_YoutubeExtractor_VideoInfo2099471191.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "System_System_UriKind238866934.h"
#include "YoutubeUnity_YoutubeExtractor_VideoType2099809763.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate676713451MethodDeclarations.h"
#include "System_ArrayTypes.h"
#include "System_System_Security_Cryptography_X509Certificate766901931.h"
#include "System_System_Security_Cryptography_X509Certificate766901931MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat2303373610.h"
#include "System_System_Security_Cryptography_X509Certificate676713451.h"
#include "System_System_Security_Cryptography_X509Certificate326021344.h"
#include "System_System_Security_Cryptography_X509Certificate326232855.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_TimeSpan413522987MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat2783749380.h"
#include "System_System_Security_Cryptography_X509Certificate160474609.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"

// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t1108656482* GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430(__this, method) ((  ObjectU5BU5D_t1108656482* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.UI.Button>()
#define GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495(__this, method) ((  ButtonU5BU5D_t3726781579* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.UI.Image>()
#define GameObject_GetComponentsInChildren_TisImage_t538875265_m1150406570(__this, method) ((  ImageU5BU5D_t4039083868* (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
#define Resources_Load_TisSprite_t3199167241_m3887230130(__this /* static, unused */, p0, method) ((  Sprite_t3199167241 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t9039225_m1610753993(__this, method) ((  Text_t9039225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Resources::LoadAll<System.Object>(System.String)
extern "C"  ObjectU5BU5D_t1108656482* Resources_LoadAll_TisIl2CppObject_m3347850875_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_LoadAll_TisIl2CppObject_m3347850875(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t1108656482* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m3347850875_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Resources::LoadAll<UnityEngine.Sprite>(System.String)
#define Resources_LoadAll_TisSprite_t3199167241_m634693541(__this /* static, unused */, p0, method) ((  SpriteU5BU5D_t2761310900* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m3347850875_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t538875265_m3706520426(__this, method) ((  Image_t538875265 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TempingPointer>()
#define GameObject_GetComponent_TisTempingPointer_t2678149871_m846601486(__this, method) ((  TempingPointer_t2678149871 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3839065225_m3816897097(__this, method) ((  MeshFilter_t3839065225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayMakerFSM>()
#define GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963(__this, method) ((  PlayMakerFSM_t3799847376 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t538875265_m2140199269(__this, method) ((  Image_t538875265 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ScoreManager_Icing>()
#define GameObject_GetComponent_TisScoreManager_Icing_t3849909252_m2889672921(__this, method) ((  ScoreManager_Icing_t3849909252 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ScoreManager_Variation>()
#define GameObject_GetComponent_TisScoreManager_Variation_t1665153935_m389509742(__this, method) ((  ScoreManager_Variation_t1665153935 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ScoreManager_Temping>()
#define GameObject_GetComponent_TisScoreManager_Temping_t3029572618_m1205013395(__this, method) ((  ScoreManager_Temping_t3029572618 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ScoreManager_Temping>()
#define Component_GetComponent_TisScoreManager_Temping_t3029572618_m1898272059(__this, method) ((  ScoreManager_Temping_t3029572618 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SceneindexInc::.ctor()
extern "C"  void SceneindexInc__ctor_m1562956371 (SceneindexInc_t2309712408 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_index_1((-1));
		return;
	}
}
// System.Void SceneindexInc::.cctor()
extern Il2CppClass* SceneindexInc_t2309712408_il2cpp_TypeInfo_var;
extern const uint32_t SceneindexInc__cctor_m724911034_MetadataUsageId;
extern "C"  void SceneindexInc__cctor_m724911034 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneindexInc__cctor_m724911034_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneindexInc_t2309712408 * L_0 = (SceneindexInc_t2309712408 *)il2cpp_codegen_object_new(SceneindexInc_t2309712408_il2cpp_TypeInfo_var);
		SceneindexInc__ctor_m1562956371(L_0, /*hidden argument*/NULL);
		((SceneindexInc_t2309712408_StaticFields*)SceneindexInc_t2309712408_il2cpp_TypeInfo_var->static_fields)->set_n_0(L_0);
		return;
	}
}
// SceneindexInc SceneindexInc::getSingleton()
extern Il2CppClass* SceneindexInc_t2309712408_il2cpp_TypeInfo_var;
extern const uint32_t SceneindexInc_getSingleton_m1216717995_MetadataUsageId;
extern "C"  SceneindexInc_t2309712408 * SceneindexInc_getSingleton_m1216717995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneindexInc_getSingleton_m1216717995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SceneindexInc_t2309712408_il2cpp_TypeInfo_var);
		SceneindexInc_t2309712408 * L_0 = ((SceneindexInc_t2309712408_StaticFields*)SceneindexInc_t2309712408_il2cpp_TypeInfo_var->static_fields)->get_n_0();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		SceneindexInc_t2309712408 * L_1 = (SceneindexInc_t2309712408 *)il2cpp_codegen_object_new(SceneindexInc_t2309712408_il2cpp_TypeInfo_var);
		SceneindexInc__ctor_m1562956371(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SceneindexInc_t2309712408_il2cpp_TypeInfo_var);
		((SceneindexInc_t2309712408_StaticFields*)SceneindexInc_t2309712408_il2cpp_TypeInfo_var->static_fields)->set_n_0(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SceneindexInc_t2309712408_il2cpp_TypeInfo_var);
		SceneindexInc_t2309712408 * L_2 = ((SceneindexInc_t2309712408_StaticFields*)SceneindexInc_t2309712408_il2cpp_TypeInfo_var->static_fields)->get_n_0();
		return L_2;
	}
}
// System.Int32 SceneindexInc::getIndexInc()
extern "C"  int32_t SceneindexInc_getIndexInc_m4109664065 (SceneindexInc_t2309712408 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_1();
		if ((((int32_t)L_0) <= ((int32_t)5)))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_index_1((-1));
	}

IL_0013:
	{
		int32_t L_1 = __this->get_index_1();
		int32_t L_2 = ((int32_t)((int32_t)L_1+(int32_t)1));
		V_0 = L_2;
		__this->set_index_1(L_2);
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 SceneindexInc::getIndex()
extern "C"  int32_t SceneindexInc_getIndex_m1359659583 (SceneindexInc_t2309712408 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_1();
		return L_0;
	}
}
// System.Void ScoreManager_Grinding::.ctor()
extern "C"  void ScoreManager_Grinding__ctor_m2273009241 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Grinding::Awake()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t ScoreManager_Grinding_Awake_m2510614460_MetadataUsageId;
extern "C"  void ScoreManager_Grinding_Awake_m2510614460 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Grinding_Awake_m2510614460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_5(((int32_t)((int32_t)L_1-(int32_t)1)));
		int32_t L_2 = __this->get_nowNum_5();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_3 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_stuff_2(L_3);
		int32_t L_4 = __this->get_nowNum_5();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_5 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_7 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_4, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_3(L_7);
		return;
	}
}
// System.Void ScoreManager_Grinding::Start()
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Grinding_Start_m1220147033_MetadataUsageId;
extern "C"  void ScoreManager_Grinding_Start_m1220147033 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Grinding_Start_m1220147033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PopupManagement_t282433007 * L_0 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_popup_4(L_0);
		return;
	}
}
// System.Void ScoreManager_Grinding::ClickSound()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Grinding_ClickSound_m3785992946_MetadataUsageId;
extern "C"  void ScoreManager_Grinding_ClickSound_m3785992946 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Grinding_ClickSound_m3785992946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Grinding::OnEnable()
extern "C"  void ScoreManager_Grinding_OnEnable_m2835488397 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ScoreManager_Grinding_confirm_m2331669279(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Grinding::confirm()
extern Il2CppClass* U3CconfirmU3Ec__Iterator1A_t3696604900_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Grinding_confirm_m2331669279_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Grinding_confirm_m2331669279 (ScoreManager_Grinding_t1076868946 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Grinding_confirm_m2331669279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CconfirmU3Ec__Iterator1A_t3696604900 * V_0 = NULL;
	{
		U3CconfirmU3Ec__Iterator1A_t3696604900 * L_0 = (U3CconfirmU3Ec__Iterator1A_t3696604900 *)il2cpp_codegen_object_new(U3CconfirmU3Ec__Iterator1A_t3696604900_il2cpp_TypeInfo_var);
		U3CconfirmU3Ec__Iterator1A__ctor_m4009319479(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CconfirmU3Ec__Iterator1A_t3696604900 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_9(__this);
		U3CconfirmU3Ec__Iterator1A_t3696604900 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::.ctor()
extern "C"  void U3CconfirmU3Ec__Iterator1A__ctor_m4009319479 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Grinding/<confirm>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CconfirmU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2074749573 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object ScoreManager_Grinding/<confirm>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CconfirmU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m2260981785 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Boolean ScoreManager_Grinding/<confirm>c__Iterator1A::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral3395374200;
extern Il2CppCodeGenString* _stringLiteral3209242635;
extern Il2CppCodeGenString* _stringLiteral2532443403;
extern Il2CppCodeGenString* _stringLiteral3769361361;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern Il2CppCodeGenString* _stringLiteral1943403585;
extern Il2CppCodeGenString* _stringLiteral3531583;
extern Il2CppCodeGenString* _stringLiteral3084718;
extern const uint32_t U3CconfirmU3Ec__Iterator1A_MoveNext_m2322144325_MetadataUsageId;
extern "C"  bool U3CconfirmU3Ec__Iterator1A_MoveNext_m2322144325 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CconfirmU3Ec__Iterator1A_MoveNext_m2322144325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	DateTime_t4283661327  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0336;
	}

IL_0021:
	{
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_8(L_2);
		__this->set_U24PC_7(1);
		goto IL_0338;
	}

IL_003d:
	{
		ObjectU5BU5D_t1108656482* L_3 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral32);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_4 = L_3;
		float L_5 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3395374200, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t1108656482* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral32);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_9 = L_8;
		float L_10 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3209242635, /*hidden argument*/NULL);
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t1108656482* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral32);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_14 = L_13;
		float L_15 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral2532443403, /*hidden argument*/NULL);
		float L_16 = L_15;
		Il2CppObject * L_17 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_17);
		ObjectU5BU5D_t1108656482* L_18 = L_14;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral32);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_19 = L_18;
		float L_20 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3769361361, /*hidden argument*/NULL);
		float L_21 = L_20;
		Il2CppObject * L_22 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3016520001(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		__this->set_U3CtimmerMinusU3E__0_0((0.0f));
		__this->set_U3CerrorRateU3E__1_1((0.0f));
		__this->set_U3CerrorMinusU3E__2_2((0.0f));
		float L_24 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3395374200, /*hidden argument*/NULL);
		__this->set_U3ClimitTimeU3E__3_3(L_24);
		float L_25 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3209242635, /*hidden argument*/NULL);
		__this->set_U3CgoalPointU3E__4_4(L_25);
		ScoreManager_Grinding_t1076868946 * L_26 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_26);
		ConstforMinigame_t2789090703 * L_27 = L_26->get_cuff_3();
		NullCheck(L_27);
		float L_28 = ConstforMinigame_get_Limittime_m4054875741(L_27, /*hidden argument*/NULL);
		float L_29 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral2532443403, /*hidden argument*/NULL);
		__this->set_U3CcurrentTimeU3E__5_5(((float)((float)L_28-(float)L_29)));
		float L_30 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3769361361, /*hidden argument*/NULL);
		__this->set_U3CgaugePointU3E__6_6(L_30);
		ScoreManager_Grinding_t1076868946 * L_31 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_31);
		StructforMinigame_t1286533533 * L_32 = L_31->get_stuff_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_33 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_33);
		int32_t L_34 = L_33->get_gameId_6();
		NullCheck(L_32);
		StructforMinigame_set_Gameid_m1686107441(L_32, L_34, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_35 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_35);
		StructforMinigame_t1286533533 * L_36 = L_35->get_stuff_2();
		MainUserInfo_t2526075922 * L_37 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_37);
		int32_t L_38 = L_37->get_grade_5();
		NullCheck(L_36);
		StructforMinigame_set_Level_m547274884(L_36, L_38, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_39 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_39);
		StructforMinigame_t1286533533 * L_40 = L_39->get_stuff_2();
		NullCheck(L_40);
		StructforMinigame_set_Misscount_m2498396499(L_40, 0, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_41 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_41);
		StructforMinigame_t1286533533 * L_42 = L_41->get_stuff_2();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_43 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_43;
		String_t* L_44 = DateTime_ToString_m3415116655((&V_1), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_42);
		StructforMinigame_set_RecordDate_m450818768(L_42, L_44, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_45 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_45);
		StructforMinigame_t1286533533 * L_46 = L_45->get_stuff_2();
		float L_47 = __this->get_U3CcurrentTimeU3E__5_5();
		NullCheck(L_46);
		StructforMinigame_set_Playtime_m3659740247(L_46, L_47, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_48 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_48);
		StructforMinigame_t1286533533 * L_49 = L_48->get_stuff_2();
		NullCheck(L_49);
		float L_50 = StructforMinigame_get_Playtime_m2221809428(L_49, /*hidden argument*/NULL);
		float L_51 = L_50;
		Il2CppObject * L_52 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_51);
		String_t* L_53 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1943403585, L_52, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		float L_54 = __this->get_U3CcurrentTimeU3E__5_5();
		ScoreManager_Grinding_t1076868946 * L_55 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_55);
		ConstforMinigame_t2789090703 * L_56 = L_55->get_cuff_3();
		NullCheck(L_56);
		float L_57 = ConstforMinigame_get_Besttime_m1687094942(L_56, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_58 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_58);
		ConstforMinigame_t2789090703 * L_59 = L_58->get_cuff_3();
		NullCheck(L_59);
		float L_60 = ConstforMinigame_get_Limittime_m4054875741(L_59, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_61 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_61);
		ConstforMinigame_t2789090703 * L_62 = L_61->get_cuff_3();
		NullCheck(L_62);
		float L_63 = ConstforMinigame_get_Besttime_m1687094942(L_62, /*hidden argument*/NULL);
		float L_64 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_54-(float)L_57))/(float)((float)((float)L_60-(float)L_63)))), /*hidden argument*/NULL);
		float L_65 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_64, /*hidden argument*/NULL);
		__this->set_U3CtimmerMinusU3E__0_0(L_65);
		ScoreManager_Grinding_t1076868946 * L_66 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_66);
		StructforMinigame_t1286533533 * L_67 = L_66->get_stuff_2();
		float L_68 = __this->get_U3CgaugePointU3E__6_6();
		float L_69 = __this->get_U3CgoalPointU3E__4_4();
		float L_70 = __this->get_U3CgoalPointU3E__4_4();
		float L_71 = fabsf(((float)((float)((float)((float)((float)((float)L_68-(float)L_69))/(float)L_70))*(float)(100.0f))));
		NullCheck(L_67);
		StructforMinigame_set_ErrorRate_m2862618580(L_67, L_71, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_72 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_72);
		StructforMinigame_t1286533533 * L_73 = L_72->get_stuff_2();
		NullCheck(L_73);
		float L_74 = StructforMinigame_get_ErrorRate_m2504854007(L_73, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_75 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_75);
		ConstforMinigame_t2789090703 * L_76 = L_75->get_cuff_3();
		NullCheck(L_76);
		float L_77 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_76, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_78 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_78);
		ConstforMinigame_t2789090703 * L_79 = L_78->get_cuff_3();
		NullCheck(L_79);
		float L_80 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_79, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_81 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_81);
		ConstforMinigame_t2789090703 * L_82 = L_81->get_cuff_3();
		NullCheck(L_82);
		float L_83 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_82, /*hidden argument*/NULL);
		float L_84 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_74-(float)L_77))/(float)((float)((float)L_80-(float)L_83)))), /*hidden argument*/NULL);
		float L_85 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_84, /*hidden argument*/NULL);
		__this->set_U3CerrorMinusU3E__2_2(L_85);
		float L_86 = __this->get_U3CtimmerMinusU3E__0_0();
		float L_87 = L_86;
		Il2CppObject * L_88 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_87);
		String_t* L_89 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3531583, L_88, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		float L_90 = __this->get_U3CerrorMinusU3E__2_2();
		float L_91 = L_90;
		Il2CppObject * L_92 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_91);
		String_t* L_93 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3084718, L_92, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		float L_94 = __this->get_U3CerrorRateU3E__1_1();
		float L_95 = L_94;
		Il2CppObject * L_96 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_95);
		String_t* L_97 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3084718, L_96, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_98 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_98);
		StructforMinigame_t1286533533 * L_99 = L_98->get_stuff_2();
		float L_100 = __this->get_U3CtimmerMinusU3E__0_0();
		float L_101 = __this->get_U3CerrorMinusU3E__2_2();
		NullCheck(L_99);
		StructforMinigame_set_Score_m1127316490(L_99, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_100+(float)L_101))*(float)(50.0f))))), /*hidden argument*/NULL);
		ScoreManager_Grinding_t1076868946 * L_102 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_102);
		PopupManagement_t282433007 * L_103 = L_102->get_popup_4();
		NullCheck(L_103);
		PopupManagement_makeIt_m1215430652(L_103, 4, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_0336:
	{
		return (bool)0;
	}

IL_0338:
	{
		return (bool)1;
	}
	// Dead block : IL_033a: ldloc.2
}
// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::Dispose()
extern "C"  void U3CconfirmU3Ec__Iterator1A_Dispose_m268637876 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void ScoreManager_Grinding/<confirm>c__Iterator1A::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CconfirmU3Ec__Iterator1A_Reset_m1655752420_MetadataUsageId;
extern "C"  void U3CconfirmU3Ec__Iterator1A_Reset_m1655752420 (U3CconfirmU3Ec__Iterator1A_t3696604900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CconfirmU3Ec__Iterator1A_Reset_m1655752420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Icing::.ctor()
extern "C"  void ScoreManager_Icing__ctor_m1789219095 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Icing::Start()
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t ScoreManager_Icing_Start_m736356887_MetadataUsageId;
extern "C"  void ScoreManager_Icing_Start_m736356887 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_Start_m736356887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1080795294  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Scene_t1080795294  L_2 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = Scene_get_name_m894591657((&V_1), /*hidden argument*/NULL);
		__this->set_nowScene_3(L_3);
		PopupManagement_t282433007 * L_4 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_pop_10(L_4);
		int32_t L_5 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_6 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_stuff_8(L_6);
		int32_t L_7 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_10 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_7, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_9(L_10);
		StructforMinigame_t1286533533 * L_11 = __this->get_stuff_8();
		NullCheck(L_11);
		Stopwatch_t3420517611 * L_12 = StructforMinigame_get_Sw_m1790624722(L_11, /*hidden argument*/NULL);
		__this->set_sw_14(L_12);
		Stopwatch_t3420517611 * L_13 = __this->get_sw_14();
		NullCheck(L_13);
		Stopwatch_Reset_m2376504733(L_13, /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_14 = __this->get_pop_10();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Icing::buttonOkClickSound()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Icing_buttonOkClickSound_m783903394_MetadataUsageId;
extern "C"  void ScoreManager_Icing_buttonOkClickSound_m783903394 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_buttonOkClickSound_m783903394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Icing::startIcing()
extern "C"  void ScoreManager_Icing_startIcing_m1880382451 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_ready_7();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Icing::ButtonClickEvent(PlayMakerFSM)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1008886500;
extern const uint32_t ScoreManager_Icing_ButtonClickEvent_m3158103361_MetadataUsageId;
extern "C"  void ScoreManager_Icing_ButtonClickEvent_m3158103361 (ScoreManager_Icing_t3849909252 * __this, PlayMakerFSM_t3799847376 * ____myFsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_ButtonClickEvent_m3158103361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = ____myFsm0;
		__this->set_myFsm_11(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1008886500, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Icing::Finish()
extern Il2CppClass* U3CFinishU3Ec__Iterator1B_t1048149352_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Icing_Finish_m3471962216_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Icing_Finish_m3471962216 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_Finish_m3471962216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFinishU3Ec__Iterator1B_t1048149352 * V_0 = NULL;
	{
		U3CFinishU3Ec__Iterator1B_t1048149352 * L_0 = (U3CFinishU3Ec__Iterator1B_t1048149352 *)il2cpp_codegen_object_new(U3CFinishU3Ec__Iterator1B_t1048149352_il2cpp_TypeInfo_var);
		U3CFinishU3Ec__Iterator1B__ctor_m2451273267(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFinishU3Ec__Iterator1B_t1048149352 * L_1 = V_0;
		return L_1;
	}
}
// System.Void ScoreManager_Icing::isRightSelect(System.Int32)
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2524;
extern Il2CppCodeGenString* _stringLiteral65509;
extern const uint32_t ScoreManager_Icing_isRightSelect_m2982517332_MetadataUsageId;
extern "C"  void ScoreManager_Icing_isRightSelect_m2982517332 (ScoreManager_Icing_t3849909252 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_isRightSelect_m2982517332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_currentIngredient_13();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_1 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Int32U5BU5DU5BU5D_t1820556512* L_2 = L_1->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_3 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_menu_10();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_4);
		int32_t L_5 = L_4;
		Int32U5BU5D_t3230847821* L_6 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		if ((((int32_t)L_0) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_7 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Int32U5BU5DU5BU5D_t1820556512* L_8 = L_7->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_9 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_menu_10();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
		int32_t L_11 = L_10;
		Int32U5BU5D_t3230847821* L_12 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		int32_t L_13 = __this->get_currentIngredient_13();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		int32_t L_16 = ___num0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_00a6;
		}
	}
	{
		ShowRecipePre_t977589144 * L_17 = __this->get_recipePre_6();
		NullCheck(L_17);
		ShowRecipePre_IcingRightChoice_m3568626182(L_17, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_currentIngredient_13();
		__this->set_currentIngredient_13(((int32_t)((int32_t)L_18+(int32_t)1)));
		PlayMakerFSM_t3799847376 * L_19 = __this->get_myFsm_11();
		NullCheck(L_19);
		Fsm_t1527112426 * L_20 = PlayMakerFSM_get_Fsm_m886945091(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Fsm_Event_m4127177141(L_20, _stringLiteral2524, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_currentIngredient_13();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_22 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		Int32U5BU5DU5BU5D_t1820556512* L_23 = L_22->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_24 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_24);
		int32_t L_25 = L_24->get_menu_10();
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		int32_t L_26 = L_25;
		Int32U5BU5D_t3230847821* L_27 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		if ((!(((uint32_t)L_21) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))))))
		{
			goto IL_00a1;
		}
	}
	{
		Il2CppObject * L_28 = ScoreManager_Icing_Finish_m3471962216(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_28, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		goto IL_00d3;
	}

IL_00a6:
	{
		int32_t L_29 = __this->get_missCount_12();
		__this->set_missCount_12(((int32_t)((int32_t)L_29+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_30 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_30);
		SoundManage_efxBadOn_m2820389210(L_30, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_31 = __this->get_myFsm_11();
		NullCheck(L_31);
		Fsm_t1527112426 * L_32 = PlayMakerFSM_get_Fsm_m886945091(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Fsm_Event_m4127177141(L_32, _stringLiteral65509, /*hidden argument*/NULL);
	}

IL_00d3:
	{
		goto IL_0105;
	}

IL_00d8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_33 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_33);
		SoundManage_efxBadOn_m2820389210(L_33, /*hidden argument*/NULL);
		int32_t L_34 = __this->get_missCount_12();
		__this->set_missCount_12(((int32_t)((int32_t)L_34+(int32_t)1)));
		PlayMakerFSM_t3799847376 * L_35 = __this->get_myFsm_11();
		NullCheck(L_35);
		Fsm_t1527112426 * L_36 = PlayMakerFSM_get_Fsm_m886945091(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Fsm_Event_m4127177141(L_36, _stringLiteral65509, /*hidden argument*/NULL);
	}

IL_0105:
	{
		return;
	}
}
// System.Void ScoreManager_Icing::TimeCheckerStart()
extern "C"  void ScoreManager_Icing_TimeCheckerStart_m2412828135 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ScoreManager_Icing_TimerCheck_m3021594264(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Icing::TimerCheck()
extern Il2CppClass* U3CTimerCheckU3Ec__Iterator1C_t3854228761_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Icing_TimerCheck_m3021594264_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Icing_TimerCheck_m3021594264 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_TimerCheck_m3021594264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerCheckU3Ec__Iterator1C_t3854228761 * V_0 = NULL;
	{
		U3CTimerCheckU3Ec__Iterator1C_t3854228761 * L_0 = (U3CTimerCheckU3Ec__Iterator1C_t3854228761 *)il2cpp_codegen_object_new(U3CTimerCheckU3Ec__Iterator1C_t3854228761_il2cpp_TypeInfo_var);
		U3CTimerCheckU3Ec__Iterator1C__ctor_m557015458(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerCheckU3Ec__Iterator1C_t3854228761 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CTimerCheckU3Ec__Iterator1C_t3854228761 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator ScoreManager_Icing::TimeEnd()
extern Il2CppClass* U3CTimeEndU3Ec__Iterator1D_t664746339_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Icing_TimeEnd_m3972789051_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Icing_TimeEnd_m3972789051 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_TimeEnd_m3972789051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimeEndU3Ec__Iterator1D_t664746339 * V_0 = NULL;
	{
		U3CTimeEndU3Ec__Iterator1D_t664746339 * L_0 = (U3CTimeEndU3Ec__Iterator1D_t664746339 *)il2cpp_codegen_object_new(U3CTimeEndU3Ec__Iterator1D_t664746339_il2cpp_TypeInfo_var);
		U3CTimeEndU3Ec__Iterator1D__ctor_m2437225320(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeEndU3Ec__Iterator1D_t664746339 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTimeEndU3Ec__Iterator1D_t664746339 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Icing::conFirm()
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern Il2CppCodeGenString* _stringLiteral3531583;
extern Il2CppCodeGenString* _stringLiteral3084718;
extern const uint32_t ScoreManager_Icing_conFirm_m1670790197_MetadataUsageId;
extern "C"  void ScoreManager_Icing_conFirm_m1670790197 (ScoreManager_Icing_t3849909252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Icing_conFirm_m1670790197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (0.0f);
		V_1 = (0.0f);
		V_2 = (0.0f);
		StructforMinigame_t1286533533 * L_0 = __this->get_stuff_8();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_1 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_grade_5();
		NullCheck(L_0);
		StructforMinigame_set_Level_m547274884(L_0, L_2, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_3 = __this->get_stuff_8();
		int32_t L_4 = __this->get_missCount_12();
		NullCheck(L_3);
		StructforMinigame_set_Misscount_m2498396499(L_3, L_4, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_5 = __this->get_stuff_8();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_6;
		String_t* L_7 = DateTime_ToString_m3415116655((&V_3), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_5);
		StructforMinigame_set_RecordDate_m450818768(L_5, L_7, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_8 = __this->get_sw_14();
		NullCheck(L_8);
		Stopwatch_Stop_m2612884438(L_8, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_9 = __this->get_stuff_8();
		float L_10 = __this->get_ticks_4();
		NullCheck(L_9);
		StructforMinigame_set_Playtime_m3659740247(L_9, L_10, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_11 = __this->get_stuff_8();
		NullCheck(L_11);
		float L_12 = StructforMinigame_get_Playtime_m2221809428(L_11, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_13 = __this->get_cuff_9();
		NullCheck(L_13);
		float L_14 = ConstforMinigame_get_Besttime_m1687094942(L_13, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_15 = __this->get_cuff_9();
		NullCheck(L_15);
		float L_16 = ConstforMinigame_get_Limittime_m4054875741(L_15, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_17 = __this->get_cuff_9();
		NullCheck(L_17);
		float L_18 = ConstforMinigame_get_Besttime_m1687094942(L_17, /*hidden argument*/NULL);
		float L_19 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_12-(float)L_14))/(float)((float)((float)L_16-(float)L_18)))), /*hidden argument*/NULL);
		float L_20 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		StructforMinigame_t1286533533 * L_21 = __this->get_stuff_8();
		StructforMinigame_t1286533533 * L_22 = __this->get_stuff_8();
		NullCheck(L_22);
		int32_t L_23 = StructforMinigame_get_Misscount_m2827575940(L_22, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_24 = __this->get_cuff_9();
		NullCheck(L_24);
		float L_25 = ConstforMinigame_get_Errorpenalty_m3689665390(L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		StructforMinigame_set_ErrorRate_m2862618580(L_21, ((float)((float)(((float)((float)L_23)))*(float)L_25)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_26 = __this->get_stuff_8();
		NullCheck(L_26);
		float L_27 = StructforMinigame_get_ErrorRate_m2504854007(L_26, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_28 = __this->get_cuff_9();
		NullCheck(L_28);
		float L_29 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_28, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_30 = __this->get_cuff_9();
		NullCheck(L_30);
		float L_31 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_30, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_32 = __this->get_cuff_9();
		NullCheck(L_32);
		float L_33 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_32, /*hidden argument*/NULL);
		float L_34 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_27-(float)L_29))/(float)((float)((float)L_31-(float)L_33)))), /*hidden argument*/NULL);
		float L_35 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		float L_36 = V_0;
		float L_37 = L_36;
		Il2CppObject * L_38 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3531583, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		float L_40 = V_2;
		float L_41 = L_40;
		Il2CppObject * L_42 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_41);
		String_t* L_43 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3084718, L_42, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_44 = __this->get_stuff_8();
		float L_45 = V_0;
		float L_46 = V_2;
		NullCheck(L_44);
		StructforMinigame_set_Score_m1127316490(L_44, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_45+(float)L_46))*(float)(50.0f))))), /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_47 = __this->get_pop_10();
		NullCheck(L_47);
		PopupManagement_makeIt_m1215430652(L_47, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::.ctor()
extern "C"  void U3CFinishU3Ec__Iterator1B__ctor_m2451273267 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Icing/<Finish>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFinishU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2006702217 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Icing/<Finish>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFinishU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m389680669 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Icing/<Finish>c__Iterator1B::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4002771076;
extern Il2CppCodeGenString* _stringLiteral721162579;
extern const uint32_t U3CFinishU3Ec__Iterator1B_MoveNext_m1512860361_MetadataUsageId;
extern "C"  bool U3CFinishU3Ec__Iterator1B_MoveNext_m1512860361 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFinishU3Ec__Iterator1B_MoveNext_m1512860361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0068;
	}

IL_0021:
	{
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_006a;
	}

IL_003d:
	{
		GameObject_t3674682005 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral4002771076, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_FindChild_m2149912886(L_4, _stringLiteral721162579, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0068:
	{
		return (bool)0;
	}

IL_006a:
	{
		return (bool)1;
	}
	// Dead block : IL_006c: ldloc.1
}
// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::Dispose()
extern "C"  void U3CFinishU3Ec__Iterator1B_Dispose_m1929814448 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Icing/<Finish>c__Iterator1B::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFinishU3Ec__Iterator1B_Reset_m97706208_MetadataUsageId;
extern "C"  void U3CFinishU3Ec__Iterator1B_Reset_m97706208 (U3CFinishU3Ec__Iterator1B_t1048149352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFinishU3Ec__Iterator1B_Reset_m97706208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator1D__ctor_m2437225320 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Icing/<TimeEnd>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1383866154 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Icing/<TimeEnd>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m3549804222 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Icing/<TimeEnd>c__Iterator1D::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator1D_MoveNext_m301783692_MetadataUsageId;
extern "C"  bool U3CTimeEndU3Ec__Iterator1D_MoveNext_m301783692 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator1D_MoveNext_m301783692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
		if (L_1 == 2)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_00ea;
	}

IL_0025:
	{
		ScoreManager_Icing_t3849909252 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Stopwatch_t3420517611 * L_3 = L_2->get_sw_14();
		NullCheck(L_3);
		Stopwatch_Stop_m2612884438(L_3, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00ec;
	}

IL_0051:
	{
		ScoreManager_Icing_t3849909252 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		L_5->set_ticks_4((0.0f));
		ScoreManager_Icing_t3849909252 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		StructforMinigame_t1286533533 * L_7 = L_6->get_stuff_8();
		ScoreManager_Icing_t3849909252 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		ConstforMinigame_t2789090703 * L_9 = L_8->get_cuff_9();
		NullCheck(L_9);
		float L_10 = ConstforMinigame_get_Limittime_m4054875741(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		StructforMinigame_set_Playtime_m3659740247(L_7, L_10, /*hidden argument*/NULL);
		ScoreManager_Icing_t3849909252 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		StructforMinigame_t1286533533 * L_12 = L_11->get_stuff_8();
		NullCheck(L_12);
		StructforMinigame_set_ErrorRate_m2862618580(L_12, (100.0f), /*hidden argument*/NULL);
		ScoreManager_Icing_t3849909252 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		PopupManagement_t282433007 * L_14 = L_13->get_pop_10();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 4, /*hidden argument*/NULL);
		goto IL_00c8;
	}

IL_00ac:
	{
		WaitForSecondsRealtime_t3698499994 * L_15 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_15, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_15);
		__this->set_U24PC_0(2);
		goto IL_00ec;
	}

IL_00c8:
	{
		ScoreManager_Icing_t3849909252 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		PopupManagement_t282433007 * L_17 = L_16->get_pop_10();
		NullCheck(L_17);
		Object_t3071478659 * L_18 = PopupManagement_get_PopupObject_m1866634147(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00ac;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_00ea:
	{
		return (bool)0;
	}

IL_00ec:
	{
		return (bool)1;
	}
	// Dead block : IL_00ee: ldloc.1
}
// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator1D_Dispose_m1314639269 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Icing/<TimeEnd>c__Iterator1D::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator1D_Reset_m83658261_MetadataUsageId;
extern "C"  void U3CTimeEndU3Ec__Iterator1D_Reset_m83658261 (U3CTimeEndU3Ec__Iterator1D_t664746339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator1D_Reset_m83658261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator1C__ctor_m557015458 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Icing/<TimerCheck>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2896638458 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ScoreManager_Icing/<TimerCheck>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m1533045134 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean ScoreManager_Icing/<TimerCheck>c__Iterator1C::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator1C_MoveNext_m2019924730 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00d6;
	}

IL_0021:
	{
		ScoreManager_Icing_t3849909252 * L_2 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Icing_t3849909252 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		Stopwatch_t3420517611 * L_4 = L_3->get_sw_14();
		NullCheck(L_4);
		int64_t L_5 = Stopwatch_get_ElapsedMilliseconds_m23977474(L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_ticks_4(((float)((float)((float)((float)(((float)((float)L_5)))+(float)L_6))/(float)(1000.0f))));
		ScoreManager_Icing_t3849909252 * L_7 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		ConstforMinigame_t2789090703 * L_8 = L_7->get_cuff_9();
		NullCheck(L_8);
		float L_9 = ConstforMinigame_get_Limittime_m4054875741(L_8, /*hidden argument*/NULL);
		ScoreManager_Icing_t3849909252 * L_10 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		float L_11 = L_10->get_ticks_4();
		double L_12 = Math_Round_m2351753873(NULL /*static, unused*/, (((double)((double)L_11))), 0, /*hidden argument*/NULL);
		__this->set_U3CitU3E__0_0((((int32_t)((int32_t)((double)((double)(((double)((double)L_9)))-(double)L_12))))));
		ScoreManager_Icing_t3849909252 * L_13 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		Text_t9039225 * L_14 = L_13->get_timmer_5();
		int32_t* L_15 = __this->get_address_of_U3CitU3E__0_0();
		String_t* L_16 = Int32_ToString_m1286526384(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_16);
		int32_t L_17 = __this->get_U3CitU3E__0_0();
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_00b7;
		}
	}
	{
		ScoreManager_Icing_t3849909252 * L_18 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Icing_t3849909252 * L_19 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_19);
		Il2CppObject * L_20 = ScoreManager_Icing_TimeEnd_m3972789051(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		MonoBehaviour_StartCoroutine_m2135303124(L_18, L_20, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b7:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_00d8;
	}

IL_00ca:
	{
		goto IL_0021;
	}
	// Dead block : IL_00cf: ldarg.0

IL_00d6:
	{
		return (bool)0;
	}

IL_00d8:
	{
		return (bool)1;
	}
}
// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator1C_Dispose_m2614193503 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ScoreManager_Icing/<TimerCheck>c__Iterator1C::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerCheckU3Ec__Iterator1C_Reset_m2498415695_MetadataUsageId;
extern "C"  void U3CTimerCheckU3Ec__Iterator1C_Reset_m2498415695 (U3CTimerCheckU3Ec__Iterator1C_t3854228761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerCheckU3Ec__Iterator1C_Reset_m2498415695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Lobby::.ctor()
extern Il2CppClass* List_1_t969614734_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1907060817_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU2CU5D_t4054002953_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m292046996_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m514304035_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1060804404;
extern Il2CppCodeGenString* _stringLiteral861563040;
extern Il2CppCodeGenString* _stringLiteral1628079092;
extern Il2CppCodeGenString* _stringLiteral478160928;
extern Il2CppCodeGenString* _stringLiteral1681949595;
extern Il2CppCodeGenString* _stringLiteral1628103536;
extern Il2CppCodeGenString* _stringLiteral1461891784;
extern Il2CppCodeGenString* _stringLiteral424023892;
extern Il2CppCodeGenString* _stringLiteral1231155788;
extern Il2CppCodeGenString* _stringLiteral1392080347;
extern Il2CppCodeGenString* _stringLiteral1428640639;
extern Il2CppCodeGenString* _stringLiteral51635395;
extern Il2CppCodeGenString* _stringLiteral2100103722;
extern Il2CppCodeGenString* _stringLiteral1165099387;
extern Il2CppCodeGenString* _stringLiteral822438886;
extern Il2CppCodeGenString* _stringLiteral2738283809;
extern Il2CppCodeGenString* _stringLiteral528482671;
extern Il2CppCodeGenString* _stringLiteral1951653636;
extern Il2CppCodeGenString* _stringLiteral2777632814;
extern Il2CppCodeGenString* _stringLiteral1236943827;
extern Il2CppCodeGenString* _stringLiteral522014791;
extern Il2CppCodeGenString* _stringLiteral779360358;
extern Il2CppCodeGenString* _stringLiteral540610971;
extern Il2CppCodeGenString* _stringLiteral2674316395;
extern const uint32_t ScoreManager_Lobby__ctor_m989399433_MetadataUsageId;
extern "C"  void ScoreManager_Lobby__ctor_m989399433 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby__ctor_m989399433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_getMenu_7(((int32_t)18));
		List_1_t969614734 * L_0 = (List_1_t969614734 *)il2cpp_codegen_object_new(List_1_t969614734_il2cpp_TypeInfo_var);
		List_1__ctor_m292046996(L_0, /*hidden argument*/List_1__ctor_m292046996_MethodInfo_var);
		__this->set_bts_13(L_0);
		List_1_t1907060817 * L_1 = (List_1_t1907060817 *)il2cpp_codegen_object_new(List_1_t1907060817_il2cpp_TypeInfo_var);
		List_1__ctor_m514304035(L_1, /*hidden argument*/List_1__ctor_m514304035_MethodInfo_var);
		__this->set_imgs_14(L_1);
		StringU5BU5D_t4054002952* L_2 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral1060804404);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1060804404);
		StringU5BU5D_t4054002952* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, _stringLiteral861563040);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral861563040);
		StringU5BU5D_t4054002952* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral861563040);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral861563040);
		StringU5BU5D_t4054002952* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral1628079092);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1628079092);
		StringU5BU5D_t4054002952* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1628079092);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1628079092);
		StringU5BU5D_t4054002952* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, _stringLiteral478160928);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral478160928);
		StringU5BU5D_t4054002952* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 6);
		ArrayElementTypeCheck (L_8, _stringLiteral478160928);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral478160928);
		StringU5BU5D_t4054002952* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 7);
		ArrayElementTypeCheck (L_9, _stringLiteral1681949595);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1681949595);
		StringU5BU5D_t4054002952* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 8);
		ArrayElementTypeCheck (L_10, _stringLiteral1681949595);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral1681949595);
		StringU5BU5D_t4054002952* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)9));
		ArrayElementTypeCheck (L_11, _stringLiteral1628103536);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral1628103536);
		StringU5BU5D_t4054002952* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)10));
		ArrayElementTypeCheck (L_12, _stringLiteral1628103536);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral1628103536);
		StringU5BU5D_t4054002952* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)11));
		ArrayElementTypeCheck (L_13, _stringLiteral1461891784);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral1461891784);
		StringU5BU5D_t4054002952* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)12));
		ArrayElementTypeCheck (L_14, _stringLiteral1461891784);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral1461891784);
		StringU5BU5D_t4054002952* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)13));
		ArrayElementTypeCheck (L_15, _stringLiteral424023892);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral424023892);
		StringU5BU5D_t4054002952* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)14));
		ArrayElementTypeCheck (L_16, _stringLiteral1231155788);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral1231155788);
		StringU5BU5D_t4054002952* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)15));
		ArrayElementTypeCheck (L_17, _stringLiteral1392080347);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral1392080347);
		__this->set_menuStr_30(L_17);
		StringU5BU5D_t4054002952* L_18 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, _stringLiteral1428640639);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1428640639);
		StringU5BU5D_t4054002952* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, _stringLiteral51635395);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral51635395);
		__this->set_hiStr_31(L_19);
		il2cpp_array_size_t L_21[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)2 };
		StringU5BU2CU5D_t4054002953* L_20 = (StringU5BU2CU5D_t4054002953*)GenArrayNew(StringU5BU2CU5D_t4054002953_il2cpp_TypeInfo_var, L_21);
		StringU5BU2CU5D_t4054002953* L_22 = L_20;
		NullCheck(L_22);
		(L_22)->SetAt(0, 0, _stringLiteral2100103722);
		StringU5BU2CU5D_t4054002953* L_23 = L_22;
		NullCheck(L_23);
		(L_23)->SetAt(0, 1, _stringLiteral1165099387);
		StringU5BU2CU5D_t4054002953* L_24 = L_23;
		NullCheck(L_24);
		(L_24)->SetAt(1, 0, _stringLiteral822438886);
		StringU5BU2CU5D_t4054002953* L_25 = L_24;
		NullCheck(L_25);
		(L_25)->SetAt(1, 1, _stringLiteral2738283809);
		StringU5BU2CU5D_t4054002953* L_26 = L_25;
		NullCheck(L_26);
		(L_26)->SetAt(2, 0, _stringLiteral528482671);
		StringU5BU2CU5D_t4054002953* L_27 = L_26;
		NullCheck(L_27);
		(L_27)->SetAt(2, 1, _stringLiteral1951653636);
		StringU5BU2CU5D_t4054002953* L_28 = L_27;
		NullCheck(L_28);
		(L_28)->SetAt(3, 0, _stringLiteral2777632814);
		StringU5BU2CU5D_t4054002953* L_29 = L_28;
		NullCheck(L_29);
		(L_29)->SetAt(3, 1, _stringLiteral1236943827);
		StringU5BU2CU5D_t4054002953* L_30 = L_29;
		NullCheck(L_30);
		(L_30)->SetAt(4, 0, _stringLiteral522014791);
		StringU5BU2CU5D_t4054002953* L_31 = L_30;
		NullCheck(L_31);
		(L_31)->SetAt(4, 1, _stringLiteral779360358);
		StringU5BU2CU5D_t4054002953* L_32 = L_31;
		NullCheck(L_32);
		(L_32)->SetAt(5, 0, _stringLiteral540610971);
		StringU5BU2CU5D_t4054002953* L_33 = L_32;
		NullCheck(L_33);
		(L_33)->SetAt(5, 1, _stringLiteral2674316395);
		__this->set_creamText_32(L_33);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Lobby::Start()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisImage_t538875265_m1150406570_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4281375108_MethodInfo_var;
extern const MethodInfo* List_1_Add_m208664851_MethodInfo_var;
extern const uint32_t ScoreManager_Lobby_Start_m4231504521_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_Start_m4231504521 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_Start_m4231504521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ButtonU5BU5D_t3726781579* V_1 = NULL;
	ImageU5BU5D_t4039083868* V_2 = NULL;
	int32_t V_3 = 0;
	Scene_t1080795294  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Image_t538875265 * L_0 = __this->get_desk_12();
		NullCheck(L_0);
		Behaviour_set_enabled_m2046806933(L_0, (bool)1, /*hidden argument*/NULL);
		Scene_t1080795294  L_1 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_1;
		int32_t L_2 = Scene_get_buildIndex_m3533090789((&V_4), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_2-(int32_t)1)));
		int32_t L_3 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_4 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_stuff_5(L_4);
		int32_t L_5 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_6 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_8 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_5, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_6(L_8);
		PopupManagement_t282433007 * L_9 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_popup_8(L_9);
		PopupManagement_t282433007 * L_10 = __this->get_popup_8();
		NullCheck(L_10);
		PopupManagement_makeIt_m1215430652(L_10, 5, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_11 = __this->get_stuff_5();
		NullCheck(L_11);
		Stopwatch_t3420517611 * L_12 = StructforMinigame_get_Sw_m1790624722(L_11, /*hidden argument*/NULL);
		__this->set_sw_3(L_12);
		Stopwatch_t3420517611 * L_13 = __this->get_sw_3();
		NullCheck(L_13);
		Stopwatch_Reset_m2376504733(L_13, /*hidden argument*/NULL);
		__this->set_ticks_4((0.0f));
		__this->set_counterBars_29(0);
		StructforMinigame_t1286533533 * L_14 = __this->get_stuff_5();
		NullCheck(L_14);
		StructforMinigame_set_Misscount_m2498396499(L_14, 0, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00f8;
	}

IL_00a8:
	{
		GameObjectU5BU5D_t2662109048* L_15 = __this->get_MenuButton_23();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		GameObject_t3674682005 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		ButtonU5BU5D_t3726781579* L_19 = GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495(L_18, /*hidden argument*/GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495_MethodInfo_var);
		V_1 = L_19;
		GameObjectU5BU5D_t2662109048* L_20 = __this->get_MenuButton_23();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		GameObject_t3674682005 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		ImageU5BU5D_t4039083868* L_24 = GameObject_GetComponentsInChildren_TisImage_t538875265_m1150406570(L_23, /*hidden argument*/GameObject_GetComponentsInChildren_TisImage_t538875265_m1150406570_MethodInfo_var);
		V_2 = L_24;
		V_3 = 0;
		goto IL_00eb;
	}

IL_00cb:
	{
		List_1_t969614734 * L_25 = __this->get_bts_13();
		ButtonU5BU5D_t3726781579* L_26 = V_1;
		int32_t L_27 = V_3;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		Button_t3896396478 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_25);
		List_1_Add_m4281375108(L_25, L_29, /*hidden argument*/List_1_Add_m4281375108_MethodInfo_var);
		List_1_t1907060817 * L_30 = __this->get_imgs_14();
		ImageU5BU5D_t4039083868* L_31 = V_2;
		int32_t L_32 = V_3;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		Image_t538875265 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_30);
		List_1_Add_m208664851(L_30, L_34, /*hidden argument*/List_1_Add_m208664851_MethodInfo_var);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00eb:
	{
		int32_t L_36 = V_3;
		ButtonU5BU5D_t3726781579* L_37 = V_1;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_38 = V_0;
		V_0 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00f8:
	{
		int32_t L_39 = V_0;
		GameObjectU5BU5D_t2662109048* L_40 = __this->get_MenuButton_23();
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length)))))))
		{
			goto IL_00a8;
		}
	}
	{
		ScoreManager_Lobby_SetOnInit_m1996947960(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Lobby::TimerCheck()
extern Il2CppClass* U3CTimerCheckU3Ec__Iterator1E_t3376731881_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_TimerCheck_m3722550630_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Lobby_TimerCheck_m3722550630 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_TimerCheck_m3722550630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerCheckU3Ec__Iterator1E_t3376731881 * V_0 = NULL;
	{
		U3CTimerCheckU3Ec__Iterator1E_t3376731881 * L_0 = (U3CTimerCheckU3Ec__Iterator1E_t3376731881 *)il2cpp_codegen_object_new(U3CTimerCheckU3Ec__Iterator1E_t3376731881_il2cpp_TypeInfo_var);
		U3CTimerCheckU3Ec__Iterator1E__ctor_m310211026(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerCheckU3Ec__Iterator1E_t3376731881 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CTimerCheckU3Ec__Iterator1E_t3376731881 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator ScoreManager_Lobby::TimeEnd()
extern Il2CppClass* U3CTimeEndU3Ec__Iterator1F_t583418455_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_TimeEnd_m4145239853_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Lobby_TimeEnd_m4145239853 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_TimeEnd_m4145239853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimeEndU3Ec__Iterator1F_t583418455 * V_0 = NULL;
	{
		U3CTimeEndU3Ec__Iterator1F_t583418455 * L_0 = (U3CTimeEndU3Ec__Iterator1F_t583418455 *)il2cpp_codegen_object_new(U3CTimeEndU3Ec__Iterator1F_t583418455_il2cpp_TypeInfo_var);
		U3CTimeEndU3Ec__Iterator1F__ctor_m1401349364(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeEndU3Ec__Iterator1F_t583418455 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTimeEndU3Ec__Iterator1F_t583418455 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Lobby::makeActive()
extern "C"  void ScoreManager_Lobby_makeActive_m749326479 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		__this->set_counterBars_29(0);
		V_0 = 0;
		goto IL_002e;
	}

IL_000e:
	{
		GameObject_t3674682005 * L_0 = __this->get_Bars_15();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t1659122786 * L_3 = Transform_GetChild_m4040462992(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)1, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)5)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::backButton()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1199519538_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1768030745_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3158782031_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m207825188_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3307638489;
extern Il2CppCodeGenString* _stringLiteral1102060698;
extern Il2CppCodeGenString* _stringLiteral3069746827;
extern Il2CppCodeGenString* _stringLiteral1632924745;
extern Il2CppCodeGenString* _stringLiteral157937801;
extern Il2CppCodeGenString* _stringLiteral80818744;
extern const uint32_t ScoreManager_Lobby_backButton_m3489665972_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_backButton_m3489665972 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_backButton_m3489665972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_OffBgm_m2683352902(L_0, /*hidden argument*/NULL);
		SoundManage_t3403985716 * L_1 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_1);
		SoundManage_efxButtonOn_m2962933949(L_1, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_2 = __this->get_sw_3();
		NullCheck(L_2);
		Stopwatch_Stop_m2612884438(L_2, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_3 = __this->get_sw_3();
		NullCheck(L_3);
		Stopwatch_Reset_m2376504733(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1199519538_il2cpp_TypeInfo_var);
		GPGSMng_t946704145 * L_4 = Singleton_1_get_GetInstance_m3158782031(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3158782031_MethodInfo_var);
		NullCheck(L_4);
		bool L_5 = GPGSMng_get_bLogin_m2039479974(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0076;
		}
	}
	{
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral3307638489, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_6 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		int32_t L_7 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1102060698, /*hidden argument*/NULL);
		int32_t L_8 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral3069746827, /*hidden argument*/NULL);
		int32_t L_9 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral1632924745, /*hidden argument*/NULL);
		int32_t L_10 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral157937801, /*hidden argument*/NULL);
		NullCheck(L_6);
		NetworkMng_RequestSaveGameData_m2757540381(L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0076:
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral80818744, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Lobby::wait()
extern Il2CppClass* U3CwaitU3Ec__Iterator20_t1632431941_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_wait_m3214450424_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Lobby_wait_m3214450424 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_wait_m3214450424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CwaitU3Ec__Iterator20_t1632431941 * V_0 = NULL;
	{
		U3CwaitU3Ec__Iterator20_t1632431941 * L_0 = (U3CwaitU3Ec__Iterator20_t1632431941 *)il2cpp_codegen_object_new(U3CwaitU3Ec__Iterator20_t1632431941_il2cpp_TypeInfo_var);
		U3CwaitU3Ec__Iterator20__ctor_m3400299254(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CwaitU3Ec__Iterator20_t1632431941 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CwaitU3Ec__Iterator20_t1632431941 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Lobby::SetTextOrder(System.Int32,System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1440949141;
extern Il2CppCodeGenString* _stringLiteral3450149191;
extern Il2CppCodeGenString* _stringLiteral240283849;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral3315289454;
extern Il2CppCodeGenString* _stringLiteral3257043471;
extern Il2CppCodeGenString* _stringLiteral4036426862;
extern Il2CppCodeGenString* _stringLiteral1563823862;
extern Il2CppCodeGenString* _stringLiteral4165736930;
extern Il2CppCodeGenString* _stringLiteral50363005;
extern Il2CppCodeGenString* _stringLiteral3309607446;
extern Il2CppCodeGenString* _stringLiteral2128656406;
extern Il2CppCodeGenString* _stringLiteral3293506240;
extern Il2CppCodeGenString* _stringLiteral1356410070;
extern const uint32_t ScoreManager_Lobby_SetTextOrder_m502009003_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_SetTextOrder_m502009003 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ____randomMenu1, int32_t ____randomHI2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_SetTextOrder_m502009003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1440949141);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1440949141);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		int32_t L_2 = ____randomNum0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral3450149191);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3450149191);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int32_t L_7 = ____randomMenu1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral240283849);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral240283849);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		int32_t L_12 = ____randomHI2;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_16 = ____randomMenu1;
		if (!L_16)
		{
			goto IL_0241;
		}
	}
	{
		int32_t L_17 = ____randomMenu1;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)13))))
		{
			goto IL_0241;
		}
	}
	{
		int32_t L_18 = ____randomMenu1;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)14))))
		{
			goto IL_0241;
		}
	}
	{
		int32_t L_19 = ____randomMenu1;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)15))))
		{
			goto IL_0241;
		}
	}
	{
		int32_t L_20 = ____randomNum0;
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_00b1;
		}
	}
	{
		Text_t9039225 * L_21 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_22 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
		StringU5BU5D_t4054002952* L_24 = L_22;
		StringU5BU5D_t4054002952* L_25 = __this->get_menuStr_30();
		int32_t L_26 = ____randomMenu1;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		ArrayElementTypeCheck (L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_28);
		StringU5BU5D_t4054002952* L_29 = L_24;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 2);
		ArrayElementTypeCheck (L_29, _stringLiteral32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_30 = L_29;
		StringU5BU5D_t4054002952* L_31 = __this->get_hiStr_31();
		int32_t L_32 = ____randomHI2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		String_t* L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 3);
		ArrayElementTypeCheck (L_30, L_34);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_34);
		StringU5BU5D_t4054002952* L_35 = L_30;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 4);
		ArrayElementTypeCheck (L_35, _stringLiteral3315289454);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3315289454);
		String_t* L_36 = String_Concat_m21867311(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_36);
		goto IL_023c;
	}

IL_00b1:
	{
		int32_t L_37 = ____randomNum0;
		if ((!(((uint32_t)L_37) == ((uint32_t)2))))
		{
			goto IL_0101;
		}
	}
	{
		Text_t9039225 * L_38 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_39 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 0);
		ArrayElementTypeCheck (L_39, L_40);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_40);
		StringU5BU5D_t4054002952* L_41 = L_39;
		StringU5BU5D_t4054002952* L_42 = __this->get_menuStr_30();
		int32_t L_43 = ____randomMenu1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		String_t* L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_45);
		StringU5BU5D_t4054002952* L_46 = L_41;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 2);
		ArrayElementTypeCheck (L_46, _stringLiteral32);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_47 = L_46;
		StringU5BU5D_t4054002952* L_48 = __this->get_hiStr_31();
		int32_t L_49 = ____randomHI2;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		String_t* L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 3);
		ArrayElementTypeCheck (L_47, L_51);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_51);
		StringU5BU5D_t4054002952* L_52 = L_47;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 4);
		ArrayElementTypeCheck (L_52, _stringLiteral3257043471);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3257043471);
		String_t* L_53 = String_Concat_m21867311(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, L_53);
		goto IL_023c;
	}

IL_0101:
	{
		int32_t L_54 = ____randomNum0;
		if ((!(((uint32_t)L_54) == ((uint32_t)3))))
		{
			goto IL_0151;
		}
	}
	{
		Text_t9039225 * L_55 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_56 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 0);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_57);
		StringU5BU5D_t4054002952* L_58 = L_56;
		StringU5BU5D_t4054002952* L_59 = __this->get_menuStr_30();
		int32_t L_60 = ____randomMenu1;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		int32_t L_61 = L_60;
		String_t* L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 1);
		ArrayElementTypeCheck (L_58, L_62);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_62);
		StringU5BU5D_t4054002952* L_63 = L_58;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 2);
		ArrayElementTypeCheck (L_63, _stringLiteral32);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_64 = L_63;
		StringU5BU5D_t4054002952* L_65 = __this->get_hiStr_31();
		int32_t L_66 = ____randomHI2;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_66);
		int32_t L_67 = L_66;
		String_t* L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 3);
		ArrayElementTypeCheck (L_64, L_68);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_68);
		StringU5BU5D_t4054002952* L_69 = L_64;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 4);
		ArrayElementTypeCheck (L_69, _stringLiteral4036426862);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4036426862);
		String_t* L_70 = String_Concat_m21867311(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		NullCheck(L_55);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_55, L_70);
		goto IL_023c;
	}

IL_0151:
	{
		int32_t L_71 = ____randomNum0;
		if ((!(((uint32_t)L_71) == ((uint32_t)4))))
		{
			goto IL_01a1;
		}
	}
	{
		Text_t9039225 * L_72 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_73 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 0);
		ArrayElementTypeCheck (L_73, _stringLiteral1563823862);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1563823862);
		StringU5BU5D_t4054002952* L_74 = L_73;
		StringU5BU5D_t4054002952* L_75 = __this->get_menuStr_30();
		int32_t L_76 = ____randomMenu1;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
		int32_t L_77 = L_76;
		String_t* L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, 1);
		ArrayElementTypeCheck (L_74, L_78);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_78);
		StringU5BU5D_t4054002952* L_79 = L_74;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 2);
		ArrayElementTypeCheck (L_79, _stringLiteral32);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_80 = L_79;
		StringU5BU5D_t4054002952* L_81 = __this->get_hiStr_31();
		int32_t L_82 = ____randomHI2;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, L_82);
		int32_t L_83 = L_82;
		String_t* L_84 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 3);
		ArrayElementTypeCheck (L_80, L_84);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_84);
		StringU5BU5D_t4054002952* L_85 = L_80;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 4);
		ArrayElementTypeCheck (L_85, _stringLiteral4165736930);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4165736930);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_86 = String_Concat_m21867311(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		NullCheck(L_72);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_72, L_86);
		goto IL_023c;
	}

IL_01a1:
	{
		int32_t L_87 = ____randomNum0;
		if ((!(((uint32_t)L_87) == ((uint32_t)5))))
		{
			goto IL_01f1;
		}
	}
	{
		Text_t9039225 * L_88 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_89 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 0);
		ArrayElementTypeCheck (L_89, _stringLiteral50363005);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral50363005);
		StringU5BU5D_t4054002952* L_90 = L_89;
		StringU5BU5D_t4054002952* L_91 = __this->get_menuStr_30();
		int32_t L_92 = ____randomMenu1;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, L_92);
		int32_t L_93 = L_92;
		String_t* L_94 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 1);
		ArrayElementTypeCheck (L_90, L_94);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_94);
		StringU5BU5D_t4054002952* L_95 = L_90;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 2);
		ArrayElementTypeCheck (L_95, _stringLiteral32);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_96 = L_95;
		StringU5BU5D_t4054002952* L_97 = __this->get_hiStr_31();
		int32_t L_98 = ____randomHI2;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, L_98);
		int32_t L_99 = L_98;
		String_t* L_100 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, 3);
		ArrayElementTypeCheck (L_96, L_100);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_100);
		StringU5BU5D_t4054002952* L_101 = L_96;
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 4);
		ArrayElementTypeCheck (L_101, _stringLiteral3309607446);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3309607446);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_102 = String_Concat_m21867311(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		NullCheck(L_88);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_88, L_102);
		goto IL_023c;
	}

IL_01f1:
	{
		int32_t L_103 = ____randomNum0;
		if ((!(((uint32_t)L_103) == ((uint32_t)6))))
		{
			goto IL_023c;
		}
	}
	{
		Text_t9039225 * L_104 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_105 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_106 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 0);
		ArrayElementTypeCheck (L_105, L_106);
		(L_105)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_106);
		StringU5BU5D_t4054002952* L_107 = L_105;
		StringU5BU5D_t4054002952* L_108 = __this->get_menuStr_30();
		int32_t L_109 = ____randomMenu1;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		int32_t L_110 = L_109;
		String_t* L_111 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 1);
		ArrayElementTypeCheck (L_107, L_111);
		(L_107)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_111);
		StringU5BU5D_t4054002952* L_112 = L_107;
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 2);
		ArrayElementTypeCheck (L_112, _stringLiteral2128656406);
		(L_112)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2128656406);
		StringU5BU5D_t4054002952* L_113 = L_112;
		StringU5BU5D_t4054002952* L_114 = __this->get_hiStr_31();
		int32_t L_115 = ____randomHI2;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, L_115);
		int32_t L_116 = L_115;
		String_t* L_117 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116));
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, 3);
		ArrayElementTypeCheck (L_113, L_117);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_117);
		StringU5BU5D_t4054002952* L_118 = L_113;
		NullCheck(L_118);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_118, 4);
		ArrayElementTypeCheck (L_118, _stringLiteral3293506240);
		(L_118)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3293506240);
		String_t* L_119 = String_Concat_m21867311(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		NullCheck(L_104);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_104, L_119);
	}

IL_023c:
	{
		goto IL_0350;
	}

IL_0241:
	{
		int32_t L_120 = ____randomNum0;
		if ((!(((uint32_t)L_120) == ((uint32_t)1))))
		{
			goto IL_026f;
		}
	}
	{
		Text_t9039225 * L_121 = __this->get_MayItakeYourOrder_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t4054002952* L_123 = __this->get_menuStr_30();
		int32_t L_124 = ____randomMenu1;
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, L_124);
		int32_t L_125 = L_124;
		String_t* L_126 = (L_123)->GetAt(static_cast<il2cpp_array_size_t>(L_125));
		String_t* L_127 = String_Concat_m1825781833(NULL /*static, unused*/, L_122, L_126, _stringLiteral3315289454, /*hidden argument*/NULL);
		NullCheck(L_121);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_121, L_127);
		goto IL_0350;
	}

IL_026f:
	{
		int32_t L_128 = ____randomNum0;
		if ((!(((uint32_t)L_128) == ((uint32_t)2))))
		{
			goto IL_029d;
		}
	}
	{
		Text_t9039225 * L_129 = __this->get_MayItakeYourOrder_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_130 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t4054002952* L_131 = __this->get_menuStr_30();
		int32_t L_132 = ____randomMenu1;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = L_132;
		String_t* L_134 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		String_t* L_135 = String_Concat_m1825781833(NULL /*static, unused*/, L_130, L_134, _stringLiteral3257043471, /*hidden argument*/NULL);
		NullCheck(L_129);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_129, L_135);
		goto IL_0350;
	}

IL_029d:
	{
		int32_t L_136 = ____randomNum0;
		if ((!(((uint32_t)L_136) == ((uint32_t)3))))
		{
			goto IL_02cb;
		}
	}
	{
		Text_t9039225 * L_137 = __this->get_MayItakeYourOrder_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_138 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t4054002952* L_139 = __this->get_menuStr_30();
		int32_t L_140 = ____randomMenu1;
		NullCheck(L_139);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_139, L_140);
		int32_t L_141 = L_140;
		String_t* L_142 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		String_t* L_143 = String_Concat_m1825781833(NULL /*static, unused*/, L_138, L_142, _stringLiteral4036426862, /*hidden argument*/NULL);
		NullCheck(L_137);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_137, L_143);
		goto IL_0350;
	}

IL_02cb:
	{
		int32_t L_144 = ____randomNum0;
		if ((!(((uint32_t)L_144) == ((uint32_t)4))))
		{
			goto IL_02f9;
		}
	}
	{
		Text_t9039225 * L_145 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_146 = __this->get_menuStr_30();
		int32_t L_147 = ____randomMenu1;
		NullCheck(L_146);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_146, L_147);
		int32_t L_148 = L_147;
		String_t* L_149 = (L_146)->GetAt(static_cast<il2cpp_array_size_t>(L_148));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_150 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1563823862, L_149, _stringLiteral4165736930, /*hidden argument*/NULL);
		NullCheck(L_145);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_145, L_150);
		goto IL_0350;
	}

IL_02f9:
	{
		int32_t L_151 = ____randomNum0;
		if ((!(((uint32_t)L_151) == ((uint32_t)5))))
		{
			goto IL_0327;
		}
	}
	{
		Text_t9039225 * L_152 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_153 = __this->get_menuStr_30();
		int32_t L_154 = ____randomMenu1;
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, L_154);
		int32_t L_155 = L_154;
		String_t* L_156 = (L_153)->GetAt(static_cast<il2cpp_array_size_t>(L_155));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_157 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral50363005, L_156, _stringLiteral3309607446, /*hidden argument*/NULL);
		NullCheck(L_152);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_152, L_157);
		goto IL_0350;
	}

IL_0327:
	{
		int32_t L_158 = ____randomNum0;
		if ((!(((uint32_t)L_158) == ((uint32_t)6))))
		{
			goto IL_0350;
		}
	}
	{
		Text_t9039225 * L_159 = __this->get_MayItakeYourOrder_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_160 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t4054002952* L_161 = __this->get_menuStr_30();
		int32_t L_162 = ____randomMenu1;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, L_162);
		int32_t L_163 = L_162;
		String_t* L_164 = (L_161)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		String_t* L_165 = String_Concat_m1825781833(NULL /*static, unused*/, L_160, L_164, _stringLiteral1356410070, /*hidden argument*/NULL);
		NullCheck(L_159);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_159, L_165);
	}

IL_0350:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::SetTextWrongOrder(System.Int32,System.Int32,System.Int32)
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2576418317;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral3144064296;
extern Il2CppCodeGenString* _stringLiteral1632916;
extern Il2CppCodeGenString* _stringLiteral1472036;
extern Il2CppCodeGenString* _stringLiteral1738497201;
extern Il2CppCodeGenString* _stringLiteral4075380099;
extern Il2CppCodeGenString* _stringLiteral1340442853;
extern Il2CppCodeGenString* _stringLiteral3877174107;
extern Il2CppCodeGenString* _stringLiteral3659724535;
extern Il2CppCodeGenString* _stringLiteral2902252077;
extern const uint32_t ScoreManager_Lobby_SetTextWrongOrder_m4236314504_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_SetTextWrongOrder_m4236314504 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ____randomMenu1, int32_t ____randomHI2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_SetTextWrongOrder_m4236314504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ____randomMenu1;
		if (!L_0)
		{
			goto IL_0196;
		}
	}
	{
		int32_t L_1 = ____randomMenu1;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)13))))
		{
			goto IL_0196;
		}
	}
	{
		int32_t L_2 = ____randomMenu1;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)14))))
		{
			goto IL_0196;
		}
	}
	{
		int32_t L_3 = ____randomMenu1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)15))))
		{
			goto IL_0196;
		}
	}
	{
		int32_t L_4 = ____randomNum0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		Text_t9039225 * L_5 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_6 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral2576418317);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2576418317);
		StringU5BU5D_t4054002952* L_7 = L_6;
		StringU5BU5D_t4054002952* L_8 = __this->get_menuStr_30();
		int32_t L_9 = ____randomMenu1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_11);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_11);
		StringU5BU5D_t4054002952* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral32);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral32);
		StringU5BU5D_t4054002952* L_13 = L_12;
		StringU5BU5D_t4054002952* L_14 = __this->get_hiStr_31();
		int32_t L_15 = ____randomHI2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		String_t* L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_17);
		StringU5BU5D_t4054002952* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 4);
		ArrayElementTypeCheck (L_18, _stringLiteral3144064296);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3144064296);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_19);
		goto IL_0191;
	}

IL_006e:
	{
		int32_t L_20 = ____randomNum0;
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_00be;
		}
	}
	{
		Text_t9039225 * L_21 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_22 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		ArrayElementTypeCheck (L_22, _stringLiteral1632916);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1632916);
		StringU5BU5D_t4054002952* L_23 = L_22;
		StringU5BU5D_t4054002952* L_24 = __this->get_menuStr_30();
		int32_t L_25 = ____randomMenu1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		String_t* L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		ArrayElementTypeCheck (L_23, L_27);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_27);
		StringU5BU5D_t4054002952* L_28 = L_23;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
		ArrayElementTypeCheck (L_28, _stringLiteral1472036);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1472036);
		StringU5BU5D_t4054002952* L_29 = L_28;
		StringU5BU5D_t4054002952* L_30 = __this->get_hiStr_31();
		int32_t L_31 = ____randomHI2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		int32_t L_32 = L_31;
		String_t* L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		ArrayElementTypeCheck (L_29, L_33);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_33);
		StringU5BU5D_t4054002952* L_34 = L_29;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 4);
		ArrayElementTypeCheck (L_34, _stringLiteral1738497201);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1738497201);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m21867311(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_35);
		goto IL_0191;
	}

IL_00be:
	{
		int32_t L_36 = ____randomNum0;
		if ((!(((uint32_t)L_36) == ((uint32_t)3))))
		{
			goto IL_00f4;
		}
	}
	{
		Text_t9039225 * L_37 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_38 = __this->get_menuStr_30();
		int32_t L_39 = ____randomMenu1;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		String_t* L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		StringU5BU5D_t4054002952* L_42 = __this->get_hiStr_31();
		int32_t L_43 = ____randomHI2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		String_t* L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m2933632197(NULL /*static, unused*/, L_41, _stringLiteral32, L_45, _stringLiteral4075380099, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_46);
		goto IL_0191;
	}

IL_00f4:
	{
		int32_t L_47 = ____randomNum0;
		if ((!(((uint32_t)L_47) == ((uint32_t)4))))
		{
			goto IL_012a;
		}
	}
	{
		Text_t9039225 * L_48 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_49 = __this->get_menuStr_30();
		int32_t L_50 = ____randomMenu1;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		String_t* L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		StringU5BU5D_t4054002952* L_53 = __this->get_hiStr_31();
		int32_t L_54 = ____randomHI2;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		String_t* L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m2933632197(NULL /*static, unused*/, L_52, _stringLiteral32, L_56, _stringLiteral1340442853, /*hidden argument*/NULL);
		NullCheck(L_48);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_48, L_57);
		goto IL_0191;
	}

IL_012a:
	{
		int32_t L_58 = ____randomNum0;
		if ((!(((uint32_t)L_58) == ((uint32_t)5))))
		{
			goto IL_0160;
		}
	}
	{
		Text_t9039225 * L_59 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_60 = __this->get_menuStr_30();
		int32_t L_61 = ____randomMenu1;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, L_61);
		int32_t L_62 = L_61;
		String_t* L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		StringU5BU5D_t4054002952* L_64 = __this->get_hiStr_31();
		int32_t L_65 = ____randomHI2;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		int32_t L_66 = L_65;
		String_t* L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = String_Concat_m2933632197(NULL /*static, unused*/, L_63, _stringLiteral32, L_67, _stringLiteral3877174107, /*hidden argument*/NULL);
		NullCheck(L_59);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_59, L_68);
		goto IL_0191;
	}

IL_0160:
	{
		int32_t L_69 = ____randomNum0;
		if ((!(((uint32_t)L_69) == ((uint32_t)6))))
		{
			goto IL_0191;
		}
	}
	{
		Text_t9039225 * L_70 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_71 = __this->get_menuStr_30();
		int32_t L_72 = ____randomMenu1;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		int32_t L_73 = L_72;
		String_t* L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		StringU5BU5D_t4054002952* L_75 = __this->get_hiStr_31();
		int32_t L_76 = ____randomHI2;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
		int32_t L_77 = L_76;
		String_t* L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_79 = String_Concat_m2933632197(NULL /*static, unused*/, L_74, _stringLiteral32, L_78, _stringLiteral3659724535, /*hidden argument*/NULL);
		NullCheck(L_70);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_70, L_79);
	}

IL_0191:
	{
		goto IL_0296;
	}

IL_0196:
	{
		int32_t L_80 = ____randomNum0;
		if ((!(((uint32_t)L_80) == ((uint32_t)1))))
		{
			goto IL_01c4;
		}
	}
	{
		Text_t9039225 * L_81 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_82 = __this->get_menuStr_30();
		int32_t L_83 = ____randomMenu1;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, L_83);
		int32_t L_84 = L_83;
		String_t* L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_86 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2576418317, L_85, _stringLiteral3144064296, /*hidden argument*/NULL);
		NullCheck(L_81);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_81, L_86);
		goto IL_0296;
	}

IL_01c4:
	{
		int32_t L_87 = ____randomNum0;
		if ((!(((uint32_t)L_87) == ((uint32_t)2))))
		{
			goto IL_01f2;
		}
	}
	{
		Text_t9039225 * L_88 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_89 = __this->get_menuStr_30();
		int32_t L_90 = ____randomMenu1;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		int32_t L_91 = L_90;
		String_t* L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_93 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1632916, L_92, _stringLiteral2902252077, /*hidden argument*/NULL);
		NullCheck(L_88);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_88, L_93);
		goto IL_0296;
	}

IL_01f2:
	{
		int32_t L_94 = ____randomNum0;
		if ((!(((uint32_t)L_94) == ((uint32_t)3))))
		{
			goto IL_0220;
		}
	}
	{
		Text_t9039225 * L_95 = __this->get_MayItakeYourOrder_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t4054002952* L_97 = __this->get_menuStr_30();
		int32_t L_98 = ____randomMenu1;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, L_98);
		int32_t L_99 = L_98;
		String_t* L_100 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		String_t* L_101 = String_Concat_m1825781833(NULL /*static, unused*/, L_96, L_100, _stringLiteral4075380099, /*hidden argument*/NULL);
		NullCheck(L_95);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_95, L_101);
		goto IL_0296;
	}

IL_0220:
	{
		int32_t L_102 = ____randomNum0;
		if ((!(((uint32_t)L_102) == ((uint32_t)4))))
		{
			goto IL_0249;
		}
	}
	{
		Text_t9039225 * L_103 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_104 = __this->get_menuStr_30();
		int32_t L_105 = ____randomMenu1;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = L_105;
		String_t* L_107 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_108 = String_Concat_m138640077(NULL /*static, unused*/, L_107, _stringLiteral1340442853, /*hidden argument*/NULL);
		NullCheck(L_103);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_103, L_108);
		goto IL_0296;
	}

IL_0249:
	{
		int32_t L_109 = ____randomNum0;
		if ((!(((uint32_t)L_109) == ((uint32_t)5))))
		{
			goto IL_0272;
		}
	}
	{
		Text_t9039225 * L_110 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_111 = __this->get_menuStr_30();
		int32_t L_112 = ____randomMenu1;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		String_t* L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_115 = String_Concat_m138640077(NULL /*static, unused*/, L_114, _stringLiteral3877174107, /*hidden argument*/NULL);
		NullCheck(L_110);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_110, L_115);
		goto IL_0296;
	}

IL_0272:
	{
		int32_t L_116 = ____randomNum0;
		if ((!(((uint32_t)L_116) == ((uint32_t)6))))
		{
			goto IL_0296;
		}
	}
	{
		Text_t9039225 * L_117 = __this->get_MayItakeYourOrder_10();
		StringU5BU5D_t4054002952* L_118 = __this->get_menuStr_30();
		int32_t L_119 = ____randomMenu1;
		NullCheck(L_118);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_118, L_119);
		int32_t L_120 = L_119;
		String_t* L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = String_Concat_m138640077(NULL /*static, unused*/, L_121, _stringLiteral3659724535, /*hidden argument*/NULL);
		NullCheck(L_117);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_117, L_122);
	}

IL_0296:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::SetOnInit()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2045202944_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2819912587_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m533925194_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral157975376;
extern const uint32_t ScoreManager_Lobby_SetOnInit_m1996947960_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_SetOnInit_m1996947960 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_SetOnInit_m1996947960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Sprite_t3199167241 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		float L_0 = Random_Range_m3362417303(NULL /*static, unused*/, (1.0f), (7.0f), /*hidden argument*/NULL);
		double L_1 = floor((((double)((double)L_0))));
		__this->set_randomNum_25((((int32_t)((int32_t)L_1))));
		int32_t L_2 = __this->get_randomNum_25();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral157975376, L_4, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_6 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_5, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_0 = L_6;
		Image_t538875265 * L_7 = __this->get_desk_12();
		Sprite_t3199167241 * L_8 = V_0;
		NullCheck(L_7);
		Image_set_sprite_m572551402(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_9 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_grade_5();
		V_4 = L_10;
		int32_t L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 1)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 2)
		{
			goto IL_018c;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)1)) == 3)
		{
			goto IL_021c;
		}
	}
	{
		goto IL_0267;
	}

IL_006d:
	{
		V_1 = 5;
		goto IL_00a0;
	}

IL_0074:
	{
		List_1_t1907060817 * L_12 = __this->get_imgs_14();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		Image_t538875265 * L_14 = List_1_get_Item_m2045202944(L_12, L_13, /*hidden argument*/List_1_get_Item_m2045202944_MethodInfo_var);
		Color_t4194546905  L_15 = Color_get_gray_m3805362451(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_14, L_15);
		List_1_t969614734 * L_16 = __this->get_bts_13();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		Button_t3896396478 * L_18 = List_1_get_Item_m2819912587(L_16, L_17, /*hidden argument*/List_1_get_Item_m2819912587_MethodInfo_var);
		NullCheck(L_18);
		Behaviour_set_enabled_m2046806933(L_18, (bool)0, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_20 = V_1;
		List_1_t969614734 * L_21 = __this->get_bts_13();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Count_m533925194(L_21, /*hidden argument*/List_1_get_Count_m533925194_MethodInfo_var);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0074;
		}
	}
	{
		float L_23 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (5.0f), /*hidden argument*/NULL);
		double L_24 = floor((((double)((double)L_23))));
		__this->set_randomMenu_26((((int32_t)((int32_t)L_24))));
		int32_t L_25 = __this->get_randomMenu_26();
		int32_t L_26 = ScoreManager_Lobby_HiIndex_m47683643(__this, L_25, /*hidden argument*/NULL);
		__this->set_randomHI_27(L_26);
		int32_t L_27 = __this->get_randomNum_25();
		int32_t L_28 = __this->get_randomMenu_26();
		int32_t L_29 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextOrder_m502009003(__this, L_27, L_28, L_29, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_00fc:
	{
		V_2 = ((int32_t)9);
		goto IL_0130;
	}

IL_0104:
	{
		List_1_t1907060817 * L_30 = __this->get_imgs_14();
		int32_t L_31 = V_2;
		NullCheck(L_30);
		Image_t538875265 * L_32 = List_1_get_Item_m2045202944(L_30, L_31, /*hidden argument*/List_1_get_Item_m2045202944_MethodInfo_var);
		Color_t4194546905  L_33 = Color_get_gray_m3805362451(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_32, L_33);
		List_1_t969614734 * L_34 = __this->get_bts_13();
		int32_t L_35 = V_2;
		NullCheck(L_34);
		Button_t3896396478 * L_36 = List_1_get_Item_m2819912587(L_34, L_35, /*hidden argument*/List_1_get_Item_m2819912587_MethodInfo_var);
		NullCheck(L_36);
		Behaviour_set_enabled_m2046806933(L_36, (bool)0, /*hidden argument*/NULL);
		int32_t L_37 = V_2;
		V_2 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_0130:
	{
		int32_t L_38 = V_2;
		List_1_t969614734 * L_39 = __this->get_bts_13();
		NullCheck(L_39);
		int32_t L_40 = List_1_get_Count_m533925194(L_39, /*hidden argument*/List_1_get_Count_m533925194_MethodInfo_var);
		if ((((int32_t)L_38) < ((int32_t)L_40)))
		{
			goto IL_0104;
		}
	}
	{
		float L_41 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (9.0f), /*hidden argument*/NULL);
		double L_42 = floor((((double)((double)L_41))));
		__this->set_randomMenu_26((((int32_t)((int32_t)L_42))));
		int32_t L_43 = __this->get_randomMenu_26();
		int32_t L_44 = ScoreManager_Lobby_HiIndex_m47683643(__this, L_43, /*hidden argument*/NULL);
		__this->set_randomHI_27(L_44);
		int32_t L_45 = __this->get_randomNum_25();
		int32_t L_46 = __this->get_randomMenu_26();
		int32_t L_47 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextOrder_m502009003(__this, L_45, L_46, L_47, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_018c:
	{
		V_3 = ((int32_t)13);
		goto IL_01c0;
	}

IL_0194:
	{
		List_1_t1907060817 * L_48 = __this->get_imgs_14();
		int32_t L_49 = V_3;
		NullCheck(L_48);
		Image_t538875265 * L_50 = List_1_get_Item_m2045202944(L_48, L_49, /*hidden argument*/List_1_get_Item_m2045202944_MethodInfo_var);
		Color_t4194546905  L_51 = Color_get_gray_m3805362451(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_50);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_50, L_51);
		List_1_t969614734 * L_52 = __this->get_bts_13();
		int32_t L_53 = V_3;
		NullCheck(L_52);
		Button_t3896396478 * L_54 = List_1_get_Item_m2819912587(L_52, L_53, /*hidden argument*/List_1_get_Item_m2819912587_MethodInfo_var);
		NullCheck(L_54);
		Behaviour_set_enabled_m2046806933(L_54, (bool)0, /*hidden argument*/NULL);
		int32_t L_55 = V_3;
		V_3 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_01c0:
	{
		int32_t L_56 = V_3;
		List_1_t969614734 * L_57 = __this->get_bts_13();
		NullCheck(L_57);
		int32_t L_58 = List_1_get_Count_m533925194(L_57, /*hidden argument*/List_1_get_Count_m533925194_MethodInfo_var);
		if ((((int32_t)L_56) < ((int32_t)L_58)))
		{
			goto IL_0194;
		}
	}
	{
		float L_59 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (13.0f), /*hidden argument*/NULL);
		double L_60 = floor((((double)((double)L_59))));
		__this->set_randomMenu_26((((int32_t)((int32_t)L_60))));
		int32_t L_61 = __this->get_randomMenu_26();
		int32_t L_62 = ScoreManager_Lobby_HiIndex_m47683643(__this, L_61, /*hidden argument*/NULL);
		__this->set_randomHI_27(L_62);
		int32_t L_63 = __this->get_randomNum_25();
		int32_t L_64 = __this->get_randomMenu_26();
		int32_t L_65 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextOrder_m502009003(__this, L_63, L_64, L_65, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_021c:
	{
		float L_66 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (16.0f), /*hidden argument*/NULL);
		double L_67 = floor((((double)((double)L_66))));
		__this->set_randomMenu_26((((int32_t)((int32_t)L_67))));
		int32_t L_68 = __this->get_randomMenu_26();
		int32_t L_69 = ScoreManager_Lobby_HiIndex_m47683643(__this, L_68, /*hidden argument*/NULL);
		__this->set_randomHI_27(L_69);
		int32_t L_70 = __this->get_randomNum_25();
		int32_t L_71 = __this->get_randomMenu_26();
		int32_t L_72 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextOrder_m502009003(__this, L_70, L_71, L_72, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_0267:
	{
		float L_73 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (16.0f), /*hidden argument*/NULL);
		double L_74 = floor((((double)((double)L_73))));
		__this->set_randomMenu_26((((int32_t)((int32_t)L_74))));
		int32_t L_75 = __this->get_randomMenu_26();
		int32_t L_76 = ScoreManager_Lobby_HiIndex_m47683643(__this, L_75, /*hidden argument*/NULL);
		__this->set_randomHI_27(L_76);
		int32_t L_77 = __this->get_randomNum_25();
		int32_t L_78 = __this->get_randomMenu_26();
		int32_t L_79 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextOrder_m502009003(__this, L_77, L_78, L_79, /*hidden argument*/NULL);
		goto IL_02b2;
	}

IL_02b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_80 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		int32_t L_81 = __this->get_randomMenu_26();
		NullCheck(L_80);
		L_80->set_menu_10(L_81);
		MainUserInfo_t2526075922 * L_82 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		int32_t L_83 = __this->get_randomCream_28();
		NullCheck(L_82);
		L_82->set_cream_11(L_83);
		MainUserInfo_t2526075922 * L_84 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		int32_t L_85 = __this->get_randomNum_25();
		NullCheck(L_84);
		L_84->set_guestId_9(L_85);
		MainUserInfo_t2526075922 * L_86 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_86);
		L_86->set_cream_11(1);
		return;
	}
}
// System.Void ScoreManager_Lobby::getOrder(UnityEngine.UI.Button)
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_getOrder_m3323546474_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_getOrder_m3323546474 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_getOrder_m3323546474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorBlock_t508458230  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		Button_t3896396478 * L_1 = ___bt0;
		NullCheck(L_1);
		ColorBlock_t508458230  L_2 = Selectable_get_colors_m2926475394(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t4194546905  L_3 = ColorBlock_get_normalColor_m1021113593((&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_4 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Color_op_Equality_m4163276884(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Button_t3896396478 * L_6 = ___bt0;
		ScoreManager_Lobby_buttonWhite_m2268534975(__this, L_6, /*hidden argument*/NULL);
		__this->set_getMenu_7(((int32_t)18));
		goto IL_0059;
	}

IL_003b:
	{
		ScoreManager_Lobby_buttonsWhite_m2723922851(__this, /*hidden argument*/NULL);
		Button_t3896396478 * L_7 = ___bt0;
		ScoreManager_Lobby_buttonRed_m836565591(__this, L_7, /*hidden argument*/NULL);
		Button_t3896396478 * L_8 = ___bt0;
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_10 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_getMenu_7(L_10);
	}

IL_0059:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::getCreamOrder(UnityEngine.UI.Button)
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_getCreamOrder_m467325460_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_getCreamOrder_m467325460 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_getCreamOrder_m467325460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorBlock_t508458230  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		Button_t3896396478 * L_1 = ___bt0;
		NullCheck(L_1);
		ColorBlock_t508458230  L_2 = Selectable_get_colors_m2926475394(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t4194546905  L_3 = ColorBlock_get_normalColor_m1021113593((&V_0), /*hidden argument*/NULL);
		Color_t4194546905  L_4 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Color_op_Equality_m4163276884(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Button_t3896396478 * L_6 = ___bt0;
		ScoreManager_Lobby_buttonWhite_m2268534975(__this, L_6, /*hidden argument*/NULL);
		__this->set_getMenu_7(((int32_t)18));
		goto IL_0059;
	}

IL_003b:
	{
		ScoreManager_Lobby_buttonsWhite2_m2837239377(__this, /*hidden argument*/NULL);
		Button_t3896396478 * L_7 = ___bt0;
		ScoreManager_Lobby_buttonRed_m836565591(__this, L_7, /*hidden argument*/NULL);
		Button_t3896396478 * L_8 = ___bt0;
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_10 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_getMenu_7(L_10);
	}

IL_0059:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::buttonsWhite()
extern const MethodInfo* List_1_get_Item_m2819912587_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m533925194_MethodInfo_var;
extern const uint32_t ScoreManager_Lobby_buttonsWhite_m2723922851_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_buttonsWhite_m2723922851 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_buttonsWhite_m2723922851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0007:
	{
		List_1_t969614734 * L_0 = __this->get_bts_13();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Button_t3896396478 * L_2 = List_1_get_Item_m2819912587(L_0, L_1, /*hidden argument*/List_1_get_Item_m2819912587_MethodInfo_var);
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m1239363704(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		List_1_t969614734 * L_4 = __this->get_bts_13();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Button_t3896396478 * L_6 = List_1_get_Item_m2819912587(L_4, L_5, /*hidden argument*/List_1_get_Item_m2819912587_MethodInfo_var);
		ScoreManager_Lobby_buttonWhite_m2268534975(__this, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		List_1_t969614734 * L_9 = __this->get_bts_13();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m533925194(L_9, /*hidden argument*/List_1_get_Count_m533925194_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0007;
		}
	}
	{
		__this->set_getMenu_7(((int32_t)18));
		return;
	}
}
// System.Void ScoreManager_Lobby::buttonsWhite2()
extern const MethodInfo* GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495_MethodInfo_var;
extern const uint32_t ScoreManager_Lobby_buttonsWhite2_m2837239377_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_buttonsWhite2_m2837239377 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_buttonsWhite2_m2837239377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ButtonU5BU5D_t3726781579* V_0 = NULL;
	int32_t V_1 = 0;
	{
		GameObject_t3674682005 * L_0 = __this->get_creamButton_24();
		NullCheck(L_0);
		ButtonU5BU5D_t3726781579* L_1 = GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495(L_0, /*hidden argument*/GameObject_GetComponentsInChildren_TisButton_t3896396478_m2193291495_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_002d;
	}

IL_0013:
	{
		ButtonU5BU5D_t3726781579* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Button_t3896396478 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		bool L_6 = Behaviour_get_enabled_m1239363704(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		ButtonU5BU5D_t3726781579* L_7 = V_0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		Button_t3896396478 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ScoreManager_Lobby_buttonWhite_m2268534975(__this, L_10, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_12 = V_1;
		ButtonU5BU5D_t3726781579* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_getMenu_7(((int32_t)18));
		return;
	}
}
// System.Void ScoreManager_Lobby::buttonWhite(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_buttonWhite_m2268534975 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method)
{
	ColorBlock_t508458230  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Button_t3896396478 * L_0 = ___bt0;
		NullCheck(L_0);
		ColorBlock_t508458230  L_1 = Selectable_get_colors_m2926475394(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Color_t4194546905  L_2 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorBlock_set_normalColor_m2914277480((&V_0), L_2, /*hidden argument*/NULL);
		Color_t4194546905  L_3 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorBlock_set_highlightedColor_m4179336310((&V_0), L_3, /*hidden argument*/NULL);
		Button_t3896396478 * L_4 = ___bt0;
		ColorBlock_t508458230  L_5 = V_0;
		NullCheck(L_4);
		Selectable_set_colors_m2365118697(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Lobby::buttonRed(UnityEngine.UI.Button)
extern "C"  void ScoreManager_Lobby_buttonRed_m836565591 (ScoreManager_Lobby_t3853030226 * __this, Button_t3896396478 * ___bt0, const MethodInfo* method)
{
	ColorBlock_t508458230  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Button_t3896396478 * L_0 = ___bt0;
		NullCheck(L_0);
		ColorBlock_t508458230  L_1 = Selectable_get_colors_m2926475394(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Color_t4194546905  L_2 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorBlock_set_normalColor_m2914277480((&V_0), L_2, /*hidden argument*/NULL);
		Color_t4194546905  L_3 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorBlock_set_highlightedColor_m4179336310((&V_0), L_3, /*hidden argument*/NULL);
		Button_t3896396478 * L_4 = ___bt0;
		ColorBlock_t508458230  L_5 = V_0;
		NullCheck(L_4);
		Selectable_set_colors_m2365118697(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Lobby::confirm()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral157975376;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral2394495;
extern Il2CppCodeGenString* _stringLiteral2034678003;
extern Il2CppCodeGenString* _stringLiteral65372258;
extern Il2CppCodeGenString* _stringLiteral892020605;
extern const uint32_t ScoreManager_Lobby_confirm_m2759373831_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_confirm_m2759373831 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_confirm_m2759373831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Sprite_t3199167241 * V_0 = NULL;
	Sprite_t3199167241 * V_1 = NULL;
	Sprite_t3199167241 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_getMenu_7();
		if ((((int32_t)L_1) == ((int32_t)((int32_t)16))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_2 = __this->get_getMenu_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_00b9;
		}
	}

IL_0024:
	{
		int32_t L_3 = __this->get_getMenu_7();
		int32_t L_4 = __this->get_randomCream_28();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)((int32_t)L_4+(int32_t)((int32_t)16)))))))
		{
			goto IL_0043;
		}
	}
	{
		ScoreManager_Lobby_RightOrder_m2078416173(__this, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_0043:
	{
		int32_t L_5 = __this->get_randomNum_25();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral157975376, L_7, _stringLiteral50, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_9 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_8, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_0 = L_9;
		Image_t538875265 * L_10 = __this->get_desk_12();
		Sprite_t3199167241 * L_11 = V_0;
		NullCheck(L_10);
		Image_set_sprite_m572551402(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_randomNum_25();
		int32_t L_13 = __this->get_randomCream_28();
		ScoreManager_Lobby_CreamText_m1005494064(__this, L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_14 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_14);
		SoundManage_efxBadOn_m2820389210(L_14, /*hidden argument*/NULL);
		LobbyTalkBoxAnimation_t432405659 * L_15 = __this->get_lobby_ani_18();
		NullCheck(L_15);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_15, /*hidden argument*/NULL);
		LobbyDeskAnimation_t4061140181 * L_16 = __this->get_lobby_desk_ani_19();
		NullCheck(L_16);
		LobbyDeskAnimation_DeskCry_m566826005(L_16, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_17 = __this->get_stuff_5();
		StructforMinigame_t1286533533 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = StructforMinigame_get_Misscount_m2827575940(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		StructforMinigame_set_Misscount_m2498396499(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_00b4:
	{
		goto IL_02c8;
	}

IL_00b9:
	{
		int32_t L_20 = __this->get_getMenu_7();
		int32_t L_21 = __this->get_randomMenu_26();
		if ((!(((uint32_t)L_20) == ((uint32_t)L_21))))
		{
			goto IL_01c8;
		}
	}
	{
		int32_t L_22 = __this->get_getMenu_7();
		if ((((int32_t)L_22) == ((int32_t)((int32_t)9))))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_23 = __this->get_getMenu_7();
		if ((((int32_t)L_23) == ((int32_t)((int32_t)10))))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_24 = __this->get_getMenu_7();
		if ((((int32_t)L_24) == ((int32_t)((int32_t)11))))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_25 = __this->get_getMenu_7();
		if ((((int32_t)L_25) == ((int32_t)((int32_t)12))))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_26 = __this->get_getMenu_7();
		if ((((int32_t)L_26) == ((int32_t)5)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_27 = __this->get_getMenu_7();
		if ((((int32_t)L_27) == ((int32_t)6)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_28 = __this->get_getMenu_7();
		if ((((int32_t)L_28) == ((int32_t)7)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_29 = __this->get_getMenu_7();
		if ((!(((uint32_t)L_29) == ((uint32_t)8))))
		{
			goto IL_01bd;
		}
	}

IL_012e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_30 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_30);
		SoundManage_efxOkOn_m3935479783(L_30, /*hidden argument*/NULL);
		float L_31 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (2.0f), /*hidden argument*/NULL);
		double L_32 = floor((((double)((double)L_31))));
		__this->set_randomCream_28((((int32_t)((int32_t)L_32))));
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_33 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		int32_t L_34 = __this->get_randomCream_28();
		NullCheck(L_33);
		L_33->set_cream_11(L_34);
		int32_t L_35 = __this->get_randomNum_25();
		int32_t L_36 = __this->get_randomCream_28();
		ScoreManager_Lobby_CreamText_m1005494064(__this, L_35, L_36, /*hidden argument*/NULL);
		GameObjectU5BU5D_t2662109048* L_37 = __this->get_MenuButton_23();
		int32_t L_38 = __this->get_currentMenuPage_20();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		GameObject_t3674682005 * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		GameObject_SetActive_m3538205401(L_40, (bool)0, /*hidden argument*/NULL);
		Button_t3896396478 * L_41 = __this->get_prevButton_21();
		NullCheck(L_41);
		Selectable_set_interactable_m2686686419(L_41, (bool)0, /*hidden argument*/NULL);
		Button_t3896396478 * L_42 = __this->get_nextButton_22();
		NullCheck(L_42);
		Selectable_set_interactable_m2686686419(L_42, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_43 = __this->get_creamButton_24();
		NullCheck(L_43);
		GameObject_SetActive_m3538205401(L_43, (bool)1, /*hidden argument*/NULL);
		LobbyTalkBoxAnimation_t432405659 * L_44 = __this->get_lobby_ani_18();
		NullCheck(L_44);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_44, /*hidden argument*/NULL);
		goto IL_01c3;
	}

IL_01bd:
	{
		ScoreManager_Lobby_RightOrder_m2078416173(__this, /*hidden argument*/NULL);
	}

IL_01c3:
	{
		goto IL_02c8;
	}

IL_01c8:
	{
		int32_t L_45 = __this->get_getMenu_7();
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_0251;
		}
	}
	{
		int32_t L_46 = __this->get_randomNum_25();
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral157975376, L_48, _stringLiteral50, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_50 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_49, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_1 = L_50;
		Image_t538875265 * L_51 = __this->get_desk_12();
		Sprite_t3199167241 * L_52 = V_1;
		NullCheck(L_51);
		Image_set_sprite_m572551402(L_51, L_52, /*hidden argument*/NULL);
		int32_t L_53 = __this->get_randomNum_25();
		int32_t L_54 = __this->get_randomMenu_26();
		int32_t L_55 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextWrongOrder_m4236314504(__this, L_53, L_54, L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_56 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_56);
		SoundManage_efxBadOn_m2820389210(L_56, /*hidden argument*/NULL);
		LobbyTalkBoxAnimation_t432405659 * L_57 = __this->get_lobby_ani_18();
		NullCheck(L_57);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_57, /*hidden argument*/NULL);
		LobbyDeskAnimation_t4061140181 * L_58 = __this->get_lobby_desk_ani_19();
		NullCheck(L_58);
		LobbyDeskAnimation_DeskCry_m566826005(L_58, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_59 = __this->get_stuff_5();
		StructforMinigame_t1286533533 * L_60 = L_59;
		NullCheck(L_60);
		int32_t L_61 = StructforMinigame_get_Misscount_m2827575940(L_60, /*hidden argument*/NULL);
		NullCheck(L_60);
		StructforMinigame_set_Misscount_m2498396499(L_60, ((int32_t)((int32_t)L_61+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_02c8;
	}

IL_0251:
	{
		int32_t L_62 = __this->get_randomNum_25();
		int32_t L_63 = L_62;
		Il2CppObject * L_64 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_63);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_65 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral157975376, L_64, _stringLiteral50, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_66 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_65, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_2 = L_66;
		Image_t538875265 * L_67 = __this->get_desk_12();
		Sprite_t3199167241 * L_68 = V_2;
		NullCheck(L_67);
		Image_set_sprite_m572551402(L_67, L_68, /*hidden argument*/NULL);
		int32_t L_69 = __this->get_randomNum_25();
		int32_t L_70 = __this->get_randomMenu_26();
		int32_t L_71 = __this->get_randomHI_27();
		ScoreManager_Lobby_SetTextWrongOrder_m4236314504(__this, L_69, L_70, L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_72 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_72);
		SoundManage_efxBadOn_m2820389210(L_72, /*hidden argument*/NULL);
		LobbyTalkBoxAnimation_t432405659 * L_73 = __this->get_lobby_ani_18();
		NullCheck(L_73);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_73, /*hidden argument*/NULL);
		LobbyDeskAnimation_t4061140181 * L_74 = __this->get_lobby_desk_ani_19();
		NullCheck(L_74);
		LobbyDeskAnimation_DeskCry_m566826005(L_74, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_75 = __this->get_stuff_5();
		StructforMinigame_t1286533533 * L_76 = L_75;
		NullCheck(L_76);
		int32_t L_77 = StructforMinigame_get_Misscount_m2827575940(L_76, /*hidden argument*/NULL);
		NullCheck(L_76);
		StructforMinigame_set_Misscount_m2498396499(L_76, ((int32_t)((int32_t)L_77+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_02c8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_78 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_78);
		int32_t L_79 = L_78->get_menu_10();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral2394495, L_79, /*hidden argument*/NULL);
		MainUserInfo_t2526075922 * L_80 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_80);
		int32_t L_81 = L_80->get_guestId_9();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral2034678003, L_81, /*hidden argument*/NULL);
		MainUserInfo_t2526075922 * L_82 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_82);
		int32_t L_83 = L_82->get_cream_11();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral65372258, L_83, /*hidden argument*/NULL);
		MainUserInfo_t2526075922 * L_84 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_84);
		int32_t L_85 = L_84->get_menu_10();
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, _stringLiteral892020605, ((int32_t)((int32_t)L_85+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Lobby::RightOrder()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral157975376;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern Il2CppCodeGenString* _stringLiteral3672633968;
extern Il2CppCodeGenString* _stringLiteral3382361343;
extern Il2CppCodeGenString* _stringLiteral3382363265;
extern Il2CppCodeGenString* _stringLiteral3382805325;
extern Il2CppCodeGenString* _stringLiteral3382358460;
extern Il2CppCodeGenString* _stringLiteral3179078025;
extern const uint32_t ScoreManager_Lobby_RightOrder_m2078416173_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_RightOrder_m2078416173 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_RightOrder_m2078416173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Sprite_t3199167241 * V_2 = NULL;
	int32_t V_3 = 0;
	DateTime_t4283661327  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxOkOn_m3935479783(L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_isOkDone_33();
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		__this->set_isOkDone_33((bool)1);
		V_0 = (0.0f);
		V_1 = (0.0f);
		int32_t L_2 = __this->get_randomNum_25();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral157975376, L_4, _stringLiteral51, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_6 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_5, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_2 = L_6;
		StructforMinigame_t1286533533 * L_7 = __this->get_stuff_5();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_gameId_6();
		NullCheck(L_7);
		StructforMinigame_set_Gameid_m1686107441(L_7, L_9, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_10 = __this->get_stuff_5();
		MainUserInfo_t2526075922 * L_11 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_11);
		int32_t L_12 = L_11->get_grade_5();
		NullCheck(L_10);
		StructforMinigame_set_Level_m547274884(L_10, L_12, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_13 = __this->get_stuff_5();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_14 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_14;
		String_t* L_15 = DateTime_ToString_m3415116655((&V_4), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_13);
		StructforMinigame_set_RecordDate_m450818768(L_13, L_15, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_16 = __this->get_sw_3();
		NullCheck(L_16);
		Stopwatch_Stop_m2612884438(L_16, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_17 = __this->get_stuff_5();
		float L_18 = __this->get_ticks_4();
		NullCheck(L_17);
		StructforMinigame_set_Playtime_m3659740247(L_17, L_18, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_19 = __this->get_stuff_5();
		NullCheck(L_19);
		float L_20 = StructforMinigame_get_Playtime_m2221809428(L_19, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_21 = __this->get_cuff_6();
		NullCheck(L_21);
		float L_22 = ConstforMinigame_get_Besttime_m1687094942(L_21, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_23 = __this->get_cuff_6();
		NullCheck(L_23);
		float L_24 = ConstforMinigame_get_Limittime_m4054875741(L_23, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_25 = __this->get_cuff_6();
		NullCheck(L_25);
		float L_26 = ConstforMinigame_get_Besttime_m1687094942(L_25, /*hidden argument*/NULL);
		float L_27 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_20-(float)L_22))/(float)((float)((float)L_24-(float)L_26)))), /*hidden argument*/NULL);
		float L_28 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		StructforMinigame_t1286533533 * L_29 = __this->get_stuff_5();
		StructforMinigame_t1286533533 * L_30 = __this->get_stuff_5();
		NullCheck(L_30);
		int32_t L_31 = StructforMinigame_get_Misscount_m2827575940(L_30, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_32 = __this->get_cuff_6();
		NullCheck(L_32);
		float L_33 = ConstforMinigame_get_Errorpenalty_m3689665390(L_32, /*hidden argument*/NULL);
		NullCheck(L_29);
		StructforMinigame_set_ErrorRate_m2862618580(L_29, ((float)((float)(((float)((float)L_31)))*(float)L_33)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_34 = __this->get_stuff_5();
		NullCheck(L_34);
		float L_35 = StructforMinigame_get_ErrorRate_m2504854007(L_34, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_36 = __this->get_cuff_6();
		NullCheck(L_36);
		float L_37 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_36, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_38 = __this->get_cuff_6();
		NullCheck(L_38);
		float L_39 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_38, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_40 = __this->get_cuff_6();
		NullCheck(L_40);
		float L_41 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_40, /*hidden argument*/NULL);
		float L_42 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_35-(float)L_37))/(float)((float)((float)L_39-(float)L_41)))), /*hidden argument*/NULL);
		float L_43 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_42, /*hidden argument*/NULL);
		V_1 = L_43;
		StructforMinigame_t1286533533 * L_44 = __this->get_stuff_5();
		NullCheck(L_44);
		int32_t L_45 = StructforMinigame_get_Misscount_m2827575940(L_44, /*hidden argument*/NULL);
		int32_t L_46 = L_45;
		Il2CppObject * L_47 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_46);
		String_t* L_48 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3672633968, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_49 = __this->get_cuff_6();
		NullCheck(L_49);
		float L_50 = ConstforMinigame_get_Errorpenalty_m3689665390(L_49, /*hidden argument*/NULL);
		float L_51 = L_50;
		Il2CppObject * L_52 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_51);
		String_t* L_53 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3382361343, L_52, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_54 = __this->get_stuff_5();
		NullCheck(L_54);
		float L_55 = StructforMinigame_get_ErrorRate_m2504854007(L_54, /*hidden argument*/NULL);
		float L_56 = L_55;
		Il2CppObject * L_57 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_56);
		String_t* L_58 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3382363265, L_57, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		float L_59 = V_0;
		float L_60 = L_59;
		Il2CppObject * L_61 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_60);
		String_t* L_62 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3382805325, L_61, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		float L_63 = V_1;
		float L_64 = L_63;
		Il2CppObject * L_65 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_64);
		String_t* L_66 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3382358460, L_65, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_67 = __this->get_stuff_5();
		float L_68 = V_0;
		float L_69 = V_1;
		NullCheck(L_67);
		StructforMinigame_set_Score_m1127316490(L_67, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_68+(float)L_69))*(float)(50.0f))))), /*hidden argument*/NULL);
		Image_t538875265 * L_70 = __this->get_desk_12();
		Sprite_t3199167241 * L_71 = V_2;
		NullCheck(L_70);
		Image_set_sprite_m572551402(L_70, L_71, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_72 = __this->get_stuff_5();
		NullCheck(L_72);
		float L_73 = StructforMinigame_get_Score_m239363585(L_72, /*hidden argument*/NULL);
		int32_t L_74 = ScoreManager_Lobby_RightText_m4200486377(__this, L_73, /*hidden argument*/NULL);
		V_3 = L_74;
		Text_t9039225 * L_75 = __this->get_MayItakeYourOrder_10();
		NullCheck(L_75);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral3179078025);
		LobbyTalkBoxAnimation_t432405659 * L_76 = __this->get_lobby_ani_18();
		NullCheck(L_76);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_76, /*hidden argument*/NULL);
		Il2CppObject * L_77 = ScoreManager_Lobby_wait_m3214450424(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_77, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ScoreManager_Lobby::RightText(System.Single)
extern "C"  int32_t ScoreManager_Lobby_RightText_m4200486377 (ScoreManager_Lobby_t3853030226 * __this, float ___cs0, const MethodInfo* method)
{
	{
		float L_0 = ___cs0;
		if ((!(((float)L_0) < ((float)(60.0f)))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float L_1 = ___cs0;
		if ((!(((float)L_1) < ((float)(70.0f)))))
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		float L_2 = ___cs0;
		if ((!(((float)L_2) < ((float)(80.0f)))))
		{
			goto IL_0027;
		}
	}
	{
		return 2;
	}

IL_0027:
	{
		float L_3 = ___cs0;
		if ((!(((float)L_3) < ((float)(90.0f)))))
		{
			goto IL_0034;
		}
	}
	{
		return 3;
	}

IL_0034:
	{
		return 4;
	}
}
// System.Int32 ScoreManager_Lobby::HiIndex(System.Int32)
extern "C"  int32_t ScoreManager_Lobby_HiIndex_m47683643 (ScoreManager_Lobby_t3853030226 * __this, int32_t ___num0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___num0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
		if (L_1 == 2)
		{
			goto IL_0051;
		}
		if (L_1 == 3)
		{
			goto IL_0053;
		}
		if (L_1 == 4)
		{
			goto IL_0055;
		}
		if (L_1 == 5)
		{
			goto IL_0057;
		}
		if (L_1 == 6)
		{
			goto IL_0059;
		}
		if (L_1 == 7)
		{
			goto IL_005b;
		}
		if (L_1 == 8)
		{
			goto IL_005d;
		}
		if (L_1 == 9)
		{
			goto IL_005f;
		}
		if (L_1 == 10)
		{
			goto IL_0061;
		}
		if (L_1 == 11)
		{
			goto IL_0063;
		}
		if (L_1 == 12)
		{
			goto IL_0065;
		}
		if (L_1 == 13)
		{
			goto IL_0067;
		}
		if (L_1 == 14)
		{
			goto IL_0069;
		}
		if (L_1 == 15)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_006d;
	}

IL_004d:
	{
		return 0;
	}

IL_004f:
	{
		return 0;
	}

IL_0051:
	{
		return 1;
	}

IL_0053:
	{
		return 0;
	}

IL_0055:
	{
		return 1;
	}

IL_0057:
	{
		return 0;
	}

IL_0059:
	{
		return 1;
	}

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		return 1;
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		return 1;
	}

IL_0063:
	{
		return 0;
	}

IL_0065:
	{
		return 1;
	}

IL_0067:
	{
		return 0;
	}

IL_0069:
	{
		return 0;
	}

IL_006b:
	{
		return 0;
	}

IL_006d:
	{
		return 0;
	}
}
// System.Void ScoreManager_Lobby::CreamText(System.Int32,System.Int32)
extern "C"  void ScoreManager_Lobby_CreamText_m1005494064 (ScoreManager_Lobby_t3853030226 * __this, int32_t ____randomNum0, int32_t ___num1, const MethodInfo* method)
{
	{
		Text_t9039225 * L_0 = __this->get_MayItakeYourOrder_10();
		StringU5BU2CU5D_t4054002953* L_1 = __this->get_creamText_32();
		int32_t L_2 = ____randomNum0;
		int32_t L_3 = ___num1;
		NullCheck(L_1);
		String_t* L_4 = (L_1)->GetAt(((int32_t)((int32_t)L_2-(int32_t)1)), L_3);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void ScoreManager_Lobby::ReadyAndStart()
extern "C"  void ScoreManager_Lobby_ReadyAndStart_m3132264629 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ScoreManager_Lobby_RS_m4060323140(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Lobby::RS()
extern Il2CppClass* U3CRSU3Ec__Iterator21_t4268541650_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_RS_m4060323140_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Lobby_RS_m4060323140 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_RS_m4060323140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CRSU3Ec__Iterator21_t4268541650 * V_0 = NULL;
	{
		U3CRSU3Ec__Iterator21_t4268541650 * L_0 = (U3CRSU3Ec__Iterator21_t4268541650 *)il2cpp_codegen_object_new(U3CRSU3Ec__Iterator21_t4268541650_il2cpp_TypeInfo_var);
		U3CRSU3Ec__Iterator21__ctor_m2449600521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRSU3Ec__Iterator21_t4268541650 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CRSU3Ec__Iterator21_t4268541650 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Lobby::TalkBoxShowAndDown()
extern "C"  void ScoreManager_Lobby_TalkBoxShowAndDown_m2134674488 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::prevButtonClick()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_prevButtonClick_m2819073994_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_prevButtonClick_m2819073994 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_prevButtonClick_m2819073994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_currentMenuPage_20();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_2 = __this->get_MenuButton_23();
		int32_t L_3 = __this->get_currentMenuPage_20();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		GameObject_t3674682005 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_currentMenuPage_20();
		__this->set_currentMenuPage_20(((int32_t)((int32_t)L_6-(int32_t)1)));
		GameObjectU5BU5D_t2662109048* L_7 = __this->get_MenuButton_23();
		int32_t L_8 = __this->get_currentMenuPage_20();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		GameObject_t3674682005 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)1, /*hidden argument*/NULL);
		Button_t3896396478 * L_11 = __this->get_nextButton_22();
		NullCheck(L_11);
		Selectable_set_interactable_m2686686419(L_11, (bool)1, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_currentMenuPage_20();
		if (L_12)
		{
			goto IL_006d;
		}
	}
	{
		Button_t3896396478 * L_13 = __this->get_prevButton_21();
		NullCheck(L_13);
		Selectable_set_interactable_m2686686419(L_13, (bool)0, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby::nextButtonClick()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Lobby_nextButtonClick_m2044262154_MetadataUsageId;
extern "C"  void ScoreManager_Lobby_nextButtonClick_m2044262154 (ScoreManager_Lobby_t3853030226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Lobby_nextButtonClick_m2044262154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_currentMenuPage_20();
		GameObjectU5BU5D_t2662109048* L_2 = __this->get_MenuButton_23();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1)))))
		{
			goto IL_0080;
		}
	}
	{
		GameObjectU5BU5D_t2662109048* L_3 = __this->get_MenuButton_23();
		int32_t L_4 = __this->get_currentMenuPage_20();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		GameObject_t3674682005 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_currentMenuPage_20();
		__this->set_currentMenuPage_20(((int32_t)((int32_t)L_7+(int32_t)1)));
		GameObjectU5BU5D_t2662109048* L_8 = __this->get_MenuButton_23();
		int32_t L_9 = __this->get_currentMenuPage_20();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		GameObject_t3674682005 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)1, /*hidden argument*/NULL);
		Button_t3896396478 * L_12 = __this->get_prevButton_21();
		NullCheck(L_12);
		Selectable_set_interactable_m2686686419(L_12, (bool)1, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_currentMenuPage_20();
		GameObjectU5BU5D_t2662109048* L_14 = __this->get_MenuButton_23();
		NullCheck(L_14);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))-(int32_t)1))))))
		{
			goto IL_0080;
		}
	}
	{
		Button_t3896396478 * L_15 = __this->get_nextButton_22();
		NullCheck(L_15);
		Selectable_set_interactable_m2686686419(L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void ScoreManager_Lobby/<RS>c__Iterator21::.ctor()
extern "C"  void U3CRSU3Ec__Iterator21__ctor_m2449600521 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Lobby/<RS>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRSU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m416652019 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Lobby/<RS>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRSU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3498697863 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Lobby/<RS>c__Iterator21::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern const uint32_t U3CRSU3Ec__Iterator21_MoveNext_m3154842419_MetadataUsageId;
extern "C"  bool U3CRSU3Ec__Iterator21_MoveNext_m3154842419 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRSU3Ec__Iterator21_MoveNext_m3154842419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_005a;
		}
		if (L_1 == 2)
		{
			goto IL_0098;
		}
		if (L_1 == 3)
		{
			goto IL_00c5;
		}
		if (L_1 == 4)
		{
			goto IL_00f7;
		}
	}
	{
		goto IL_013a;
	}

IL_002d:
	{
		ScoreManager_Lobby_t3853030226 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get__Ready_16();
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)1, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_4 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_4, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_013c;
	}

IL_005a:
	{
		ScoreManager_Lobby_t3853030226 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = L_5->get__Ready_16();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)0, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = L_7->get__Start_17();
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)1, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_9 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_9, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		__this->set_U24PC_0(2);
		goto IL_013c;
	}

IL_0098:
	{
		ScoreManager_Lobby_t3853030226 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = L_10->get__Start_17();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_12 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_12, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_12);
		__this->set_U24PC_0(3);
		goto IL_013c;
	}

IL_00c5:
	{
		ScoreManager_Lobby_t3853030226 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		Image_t538875265 * L_14 = L_13->get_desk_12();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_SetActive_m3538205401(L_15, (bool)1, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_16 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_16, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_16);
		__this->set_U24PC_0(4);
		goto IL_013c;
	}

IL_00f7:
	{
		ScoreManager_Lobby_t3853030226 * L_17 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_17);
		LobbyTalkBoxAnimation_t432405659 * L_18 = L_17->get_lobby_ani_18();
		NullCheck(L_18);
		LobbyTalkBoxAnimation_ShowmetheCoffeSizeUp_m2834643661(L_18, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_19 = __this->get_U3CU3Ef__this_2();
		ScoreManager_Lobby_t3853030226 * L_20 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_20);
		Il2CppObject * L_21 = ScoreManager_Lobby_TimerCheck_m3722550630(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		MonoBehaviour_StartCoroutine_m2135303124(L_19, L_21, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_22 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_22);
		StructforMinigame_t1286533533 * L_23 = L_22->get_stuff_5();
		NullCheck(L_23);
		Stopwatch_t3420517611 * L_24 = StructforMinigame_get_Sw_m1790624722(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Stopwatch_Start_m3677209584(L_24, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_013a:
	{
		return (bool)0;
	}

IL_013c:
	{
		return (bool)1;
	}
	// Dead block : IL_013e: ldloc.1
}
// System.Void ScoreManager_Lobby/<RS>c__Iterator21::Dispose()
extern "C"  void U3CRSU3Ec__Iterator21_Dispose_m322305542 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Lobby/<RS>c__Iterator21::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CRSU3Ec__Iterator21_Reset_m96033462_MetadataUsageId;
extern "C"  void U3CRSU3Ec__Iterator21_Reset_m96033462 (U3CRSU3Ec__Iterator21_t4268541650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRSU3Ec__Iterator21_Reset_m96033462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator1F__ctor_m1401349364 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Lobby/<TimeEnd>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561960734 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Lobby/<TimeEnd>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m739980466 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Lobby/<TimeEnd>c__Iterator1F::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator1F_MoveNext_m4156167552_MetadataUsageId;
extern "C"  bool U3CTimeEndU3Ec__Iterator1F_MoveNext_m4156167552 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator1F_MoveNext_m4156167552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
		if (L_1 == 2)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_00f5;
	}

IL_0025:
	{
		ScoreManager_Lobby_t3853030226 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Stopwatch_t3420517611 * L_3 = L_2->get_sw_3();
		NullCheck(L_3);
		Stopwatch_Stop_m2612884438(L_3, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00f7;
	}

IL_0051:
	{
		ScoreManager_Lobby_t3853030226 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		ScoreManager_Lobby_buttonsWhite_m2723922851(L_5, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		L_6->set_ticks_4((0.0f));
		ScoreManager_Lobby_t3853030226 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		StructforMinigame_t1286533533 * L_8 = L_7->get_stuff_5();
		ScoreManager_Lobby_t3853030226 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		ConstforMinigame_t2789090703 * L_10 = L_9->get_cuff_6();
		NullCheck(L_10);
		float L_11 = ConstforMinigame_get_Limittime_m4054875741(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		StructforMinigame_set_Playtime_m3659740247(L_8, L_11, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_12 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_12);
		StructforMinigame_t1286533533 * L_13 = L_12->get_stuff_5();
		NullCheck(L_13);
		StructforMinigame_set_ErrorRate_m2862618580(L_13, (100.0f), /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		PopupManagement_t282433007 * L_15 = L_14->get_popup_8();
		NullCheck(L_15);
		PopupManagement_makeIt_m1215430652(L_15, 4, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_00b7:
	{
		WaitForSecondsRealtime_t3698499994 * L_16 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_16, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_16);
		__this->set_U24PC_0(2);
		goto IL_00f7;
	}

IL_00d3:
	{
		ScoreManager_Lobby_t3853030226 * L_17 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_17);
		PopupManagement_t282433007 * L_18 = L_17->get_popup_8();
		NullCheck(L_18);
		Object_t3071478659 * L_19 = PopupManagement_get_PopupObject_m1866634147(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_19, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00b7;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_00f5:
	{
		return (bool)0;
	}

IL_00f7:
	{
		return (bool)1;
	}
	// Dead block : IL_00f9: ldloc.1
}
// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator1F_Dispose_m2270258225 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Lobby/<TimeEnd>c__Iterator1F::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator1F_Reset_m3342749601_MetadataUsageId;
extern "C"  void U3CTimeEndU3Ec__Iterator1F_Reset_m3342749601 (U3CTimeEndU3Ec__Iterator1F_t583418455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator1F_Reset_m3342749601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator1E__ctor_m310211026 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Lobby/<TimerCheck>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2460934090 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ScoreManager_Lobby/<TimerCheck>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3114121054 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean ScoreManager_Lobby/<TimerCheck>c__Iterator1E::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator1E_MoveNext_m2453101770 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00d6;
	}

IL_0021:
	{
		ScoreManager_Lobby_t3853030226 * L_2 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Lobby_t3853030226 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		Stopwatch_t3420517611 * L_4 = L_3->get_sw_3();
		NullCheck(L_4);
		int64_t L_5 = Stopwatch_get_ElapsedMilliseconds_m23977474(L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_ticks_4(((float)((float)((float)((float)(((float)((float)L_5)))+(float)L_6))/(float)(1000.0f))));
		ScoreManager_Lobby_t3853030226 * L_7 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		ConstforMinigame_t2789090703 * L_8 = L_7->get_cuff_6();
		NullCheck(L_8);
		float L_9 = ConstforMinigame_get_Limittime_m4054875741(L_8, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_10 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		float L_11 = L_10->get_ticks_4();
		double L_12 = Math_Round_m2351753873(NULL /*static, unused*/, (((double)((double)L_11))), 0, /*hidden argument*/NULL);
		__this->set_U3CitU3E__0_0((((int32_t)((int32_t)((double)((double)(((double)((double)L_9)))-(double)L_12))))));
		ScoreManager_Lobby_t3853030226 * L_13 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		Text_t9039225 * L_14 = L_13->get_timmer_9();
		int32_t* L_15 = __this->get_address_of_U3CitU3E__0_0();
		String_t* L_16 = Int32_ToString_m1286526384(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_16);
		int32_t L_17 = __this->get_U3CitU3E__0_0();
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_00b7;
		}
	}
	{
		ScoreManager_Lobby_t3853030226 * L_18 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Lobby_t3853030226 * L_19 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_19);
		Il2CppObject * L_20 = ScoreManager_Lobby_TimeEnd_m4145239853(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		MonoBehaviour_StartCoroutine_m2135303124(L_18, L_20, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b7:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_00d8;
	}

IL_00ca:
	{
		goto IL_0021;
	}
	// Dead block : IL_00cf: ldarg.0

IL_00d6:
	{
		return (bool)0;
	}

IL_00d8:
	{
		return (bool)1;
	}
}
// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator1E_Dispose_m1658335631 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerCheckU3Ec__Iterator1E_Reset_m2251611263_MetadataUsageId;
extern "C"  void U3CTimerCheckU3Ec__Iterator1E_Reset_m2251611263 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerCheckU3Ec__Iterator1E_Reset_m2251611263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Lobby/<wait>c__Iterator20::.ctor()
extern "C"  void U3CwaitU3Ec__Iterator20__ctor_m3400299254 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Lobby/<wait>c__Iterator20::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2750974182 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Lobby/<wait>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m2838829178 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Lobby/<wait>c__Iterator20::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CwaitU3Ec__Iterator20_MoveNext_m2549231654_MetadataUsageId;
extern "C"  bool U3CwaitU3Ec__Iterator20_MoveNext_m2549231654 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CwaitU3Ec__Iterator20_MoveNext_m2549231654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
		if (L_1 == 2)
		{
			goto IL_009e;
		}
	}
	{
		goto IL_00c0;
	}

IL_0025:
	{
		ScoreManager_Lobby_t3853030226 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Stopwatch_t3420517611 * L_3 = L_2->get_sw_3();
		NullCheck(L_3);
		Stopwatch_Stop_m2612884438(L_3, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00c2;
	}

IL_0051:
	{
		ScoreManager_Lobby_t3853030226 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		ScoreManager_Lobby_buttonsWhite_m2723922851(L_5, /*hidden argument*/NULL);
		ScoreManager_Lobby_t3853030226 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		L_6->set_ticks_4((0.0f));
		ScoreManager_Lobby_t3853030226 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		PopupManagement_t282433007 * L_8 = L_7->get_popup_8();
		NullCheck(L_8);
		PopupManagement_makeIt_m1215430652(L_8, 4, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_0082:
	{
		WaitForSecondsRealtime_t3698499994 * L_9 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_9, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		__this->set_U24PC_0(2);
		goto IL_00c2;
	}

IL_009e:
	{
		ScoreManager_Lobby_t3853030226 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		PopupManagement_t282433007 * L_11 = L_10->get_popup_8();
		NullCheck(L_11);
		Object_t3071478659 * L_12 = PopupManagement_get_PopupObject_m1866634147(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0082;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_00c0:
	{
		return (bool)0;
	}

IL_00c2:
	{
		return (bool)1;
	}
	// Dead block : IL_00c4: ldloc.1
}
// System.Void ScoreManager_Lobby/<wait>c__Iterator20::Dispose()
extern "C"  void U3CwaitU3Ec__Iterator20_Dispose_m3410721203 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Lobby/<wait>c__Iterator20::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CwaitU3Ec__Iterator20_Reset_m1046732195_MetadataUsageId;
extern "C"  void U3CwaitU3Ec__Iterator20_Reset_m1046732195 (U3CwaitU3Ec__Iterator20_t1632431941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CwaitU3Ec__Iterator20_Reset_m1046732195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Temping::.ctor()
extern "C"  void ScoreManager_Temping__ctor_m599960017 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::Start()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral1320885114;
extern Il2CppCodeGenString* _stringLiteral305279620;
extern Il2CppCodeGenString* _stringLiteral2193171;
extern const uint32_t ScoreManager_Temping_Start_m3842065105_MetadataUsageId;
extern "C"  void ScoreManager_Temping_Start_m3842065105 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_Start_m3842065105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Scene_t1080795294  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Scene_t1080795294  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_2), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Scene_t1080795294  L_2 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_2;
		String_t* L_3 = Scene_get_name_m894591657((&V_3), /*hidden argument*/NULL);
		__this->set_nowScene_3(L_3);
		int32_t L_4 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_5 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_stuff_4(L_5);
		int32_t L_6 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_7 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_9 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_6, ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_5(L_9);
		StructforMinigame_t1286533533 * L_10 = __this->get_stuff_4();
		NullCheck(L_10);
		Stopwatch_t3420517611 * L_11 = StructforMinigame_get_Sw_m1790624722(L_10, /*hidden argument*/NULL);
		__this->set_sw_6(L_11);
		Stopwatch_t3420517611 * L_12 = __this->get_sw_6();
		NullCheck(L_12);
		Stopwatch_Reset_m2376504733(L_12, /*hidden argument*/NULL);
		__this->set_ticks_10((0.0f));
		PopupManagement_t282433007 * L_13 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_pop_7(L_13);
		PopupManagement_t282433007 * L_14 = __this->get_pop_7();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_15 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Int32U5BU5DU5BU5D_t1820556512* L_16 = L_15->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_17 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Int32U5BU5DU5BU5D_t1820556512* L_18 = L_17->get_extractionRecipe_8();
		MainUserInfo_t2526075922 * L_19 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_19);
		int32_t L_20 = L_19->get_menu_10();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		int32_t L_21 = L_20;
		Int32U5BU5D_t3230847821* L_22 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		int32_t L_23 = 0;
		int32_t L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_24-(int32_t)1)));
		int32_t L_25 = ((int32_t)((int32_t)L_24-(int32_t)1));
		Int32U5BU5D_t3230847821* L_26 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		int32_t L_27 = 0;
		int32_t L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_0 = (((float)((float)L_28)));
		RecipeList_t1641733996 * L_29 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		Int32U5BU5DU5BU5D_t1820556512* L_30 = L_29->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_31 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Int32U5BU5DU5BU5D_t1820556512* L_32 = L_31->get_extractionRecipe_8();
		MainUserInfo_t2526075922 * L_33 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_33);
		int32_t L_34 = L_33->get_menu_10();
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_34);
		int32_t L_35 = L_34;
		Int32U5BU5D_t3230847821* L_36 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		int32_t L_37 = 0;
		int32_t L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)L_38-(int32_t)1)));
		int32_t L_39 = ((int32_t)((int32_t)L_38-(int32_t)1));
		Int32U5BU5D_t3230847821* L_40 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 1);
		int32_t L_41 = 1;
		int32_t L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_1 = (((float)((float)L_42)));
		float L_43 = V_0;
		float L_44 = V_1;
		float L_45 = Random_Range_m3362417303(NULL /*static, unused*/, L_43, ((float)((float)L_44+(float)(1.0f))), /*hidden argument*/NULL);
		double L_46 = floor((((double)((double)L_45))));
		__this->set_tempingLimitAmount_11((((float)((float)L_46))));
		GameObject_t3674682005 * L_47 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_t1659122786 * L_48 = GameObject_get_transform_m1278640159(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_t1659122786 * L_49 = Transform_FindChild_m2149912886(L_48, _stringLiteral1320885114, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t1659122786 * L_50 = Component_get_transform_m4257140443(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Transform_FindChild_m2149912886(L_50, _stringLiteral305279620, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t1659122786 * L_52 = Component_get_transform_m4257140443(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = Transform_FindChild_m2149912886(L_52, _stringLiteral2193171, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_t1659122786 * L_54 = Component_get_transform_m4257140443(L_53, /*hidden argument*/NULL);
		Vector3_t4282066566  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m2926210380(&L_55, (0.0f), (0.16f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localPosition_m3504330903(L_54, L_55, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::ReadyAndStart()
extern "C"  void ScoreManager_Temping_ReadyAndStart_m1125869821 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ScoreManager_Temping_REandSt_m4176850764(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Temping::REandSt()
extern Il2CppClass* U3CREandStU3Ec__Iterator22_t438786049_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Temping_REandSt_m4176850764_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Temping_REandSt_m4176850764 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_REandSt_m4176850764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CREandStU3Ec__Iterator22_t438786049 * V_0 = NULL;
	{
		U3CREandStU3Ec__Iterator22_t438786049 * L_0 = (U3CREandStU3Ec__Iterator22_t438786049 *)il2cpp_codegen_object_new(U3CREandStU3Ec__Iterator22_t438786049_il2cpp_TypeInfo_var);
		U3CREandStU3Ec__Iterator22__ctor_m3813340298(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CREandStU3Ec__Iterator22_t438786049 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CREandStU3Ec__Iterator22_t438786049 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator ScoreManager_Temping::TimerCheck()
extern Il2CppClass* U3CTimerCheckU3Ec__Iterator23_t859433390_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Temping_TimerCheck_m1109030942_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Temping_TimerCheck_m1109030942 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_TimerCheck_m1109030942_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerCheckU3Ec__Iterator23_t859433390 * V_0 = NULL;
	{
		U3CTimerCheckU3Ec__Iterator23_t859433390 * L_0 = (U3CTimerCheckU3Ec__Iterator23_t859433390 *)il2cpp_codegen_object_new(U3CTimerCheckU3Ec__Iterator23_t859433390_il2cpp_TypeInfo_var);
		U3CTimerCheckU3Ec__Iterator23__ctor_m1392936621(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerCheckU3Ec__Iterator23_t859433390 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CTimerCheckU3Ec__Iterator23_t859433390 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator ScoreManager_Temping::TimeEnd()
extern Il2CppClass* U3CTimeEndU3Ec__Iterator24_t3910199980_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Temping_TimeEnd_m653211509_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Temping_TimeEnd_m653211509 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_TimeEnd_m653211509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimeEndU3Ec__Iterator24_t3910199980 * V_0 = NULL;
	{
		U3CTimeEndU3Ec__Iterator24_t3910199980 * L_0 = (U3CTimeEndU3Ec__Iterator24_t3910199980 *)il2cpp_codegen_object_new(U3CTimeEndU3Ec__Iterator24_t3910199980_il2cpp_TypeInfo_var);
		U3CTimeEndU3Ec__Iterator24__ctor_m3658756671(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeEndU3Ec__Iterator24_t3910199980 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTimeEndU3Ec__Iterator24_t3910199980 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Temping::Confirm()
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern const uint32_t ScoreManager_Temping_Confirm_m4174217775_MetadataUsageId;
extern "C"  void ScoreManager_Temping_Confirm_m4174217775 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_Confirm_m4174217775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (0.0f);
		V_1 = (0.0f);
		V_2 = (0.0f);
		StructforMinigame_t1286533533 * L_0 = __this->get_stuff_4();
		int32_t L_1 = __this->get_nowNum_2();
		NullCheck(L_0);
		StructforMinigame_set_Gameid_m1686107441(L_0, ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_2 = __this->get_stuff_4();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_3 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_grade_5();
		NullCheck(L_2);
		StructforMinigame_set_Level_m547274884(L_2, L_4, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_5 = __this->get_stuff_4();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_6;
		String_t* L_7 = DateTime_ToString_m3415116655((&V_3), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_5);
		StructforMinigame_set_RecordDate_m450818768(L_5, L_7, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_8 = __this->get_stuff_4();
		float L_9 = __this->get_ticks_10();
		NullCheck(L_8);
		StructforMinigame_set_Playtime_m3659740247(L_8, L_9, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_10 = __this->get_stuff_4();
		NullCheck(L_10);
		float L_11 = StructforMinigame_get_Playtime_m2221809428(L_10, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_12 = __this->get_cuff_5();
		NullCheck(L_12);
		float L_13 = ConstforMinigame_get_Besttime_m1687094942(L_12, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_14 = __this->get_cuff_5();
		NullCheck(L_14);
		float L_15 = ConstforMinigame_get_Limittime_m4054875741(L_14, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_16 = __this->get_cuff_5();
		NullCheck(L_16);
		float L_17 = ConstforMinigame_get_Besttime_m1687094942(L_16, /*hidden argument*/NULL);
		float L_18 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_11-(float)L_13))/(float)((float)((float)L_15-(float)L_17)))), /*hidden argument*/NULL);
		float L_19 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		StructforMinigame_t1286533533 * L_20 = __this->get_stuff_4();
		float L_21 = __this->get_tempingAmount_12();
		float L_22 = __this->get_tempingLimitAmount_11();
		float L_23 = __this->get_tempingLimitAmount_11();
		float L_24 = fabsf(((float)((float)((float)((float)((float)((float)L_21-(float)L_22))/(float)L_23))*(float)(100.0f))));
		NullCheck(L_20);
		StructforMinigame_set_ErrorRate_m2862618580(L_20, L_24, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_25 = __this->get_stuff_4();
		NullCheck(L_25);
		float L_26 = StructforMinigame_get_ErrorRate_m2504854007(L_25, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_27 = __this->get_cuff_5();
		NullCheck(L_27);
		float L_28 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_27, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_29 = __this->get_cuff_5();
		NullCheck(L_29);
		float L_30 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_29, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_31 = __this->get_cuff_5();
		NullCheck(L_31);
		float L_32 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_31, /*hidden argument*/NULL);
		float L_33 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_26-(float)L_28))/(float)((float)((float)L_30-(float)L_32)))), /*hidden argument*/NULL);
		float L_34 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		StructforMinigame_t1286533533 * L_35 = __this->get_stuff_4();
		float L_36 = V_0;
		float L_37 = V_1;
		NullCheck(L_35);
		StructforMinigame_set_Score_m1127316490(L_35, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_36+(float)L_37))*(float)(50.0f))))), /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_38 = __this->get_pop_7();
		NullCheck(L_38);
		PopupManagement_makeIt_m1215430652(L_38, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::EndOfTemping()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var;
extern const MethodInfo* Resources_LoadAll_TisSprite_t3199167241_m634693541_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral721162579;
extern Il2CppCodeGenString* _stringLiteral2117;
extern Il2CppCodeGenString* _stringLiteral1581134692;
extern Il2CppCodeGenString* _stringLiteral1725724898;
extern Il2CppCodeGenString* _stringLiteral3420;
extern Il2CppCodeGenString* _stringLiteral1725724899;
extern Il2CppCodeGenString* _stringLiteral2329455453;
extern Il2CppCodeGenString* _stringLiteral2166079755;
extern Il2CppCodeGenString* _stringLiteral323663835;
extern Il2CppCodeGenString* _stringLiteral39144429;
extern const uint32_t ScoreManager_Temping_EndOfTemping_m311035759_MetadataUsageId;
extern "C"  void ScoreManager_Temping_EndOfTemping_m311035759 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_EndOfTemping_m311035759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		Stopwatch_t3420517611 * L_0 = __this->get_sw_6();
		NullCheck(L_0);
		Stopwatch_Stop_m2612884438(L_0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Transform_FindChild_m2149912886(L_2, _stringLiteral721162579, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t3674682005 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_FindChild_m2149912886(L_6, _stringLiteral2117, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Transform_FindChild_m2149912886(L_8, _stringLiteral1581134692, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = Transform_FindChild_m2149912886(L_10, _stringLiteral1725724898, /*hidden argument*/NULL);
		NullCheck(L_11);
		Text_t9039225 * L_12 = Component_GetComponent_TisText_t9039225_m1610753993(L_11, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		float L_14 = __this->get_tempingAmount_12();
		int32_t L_15 = (((int32_t)((int32_t)L_14)));
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = String_Concat_m2809334143(NULL /*static, unused*/, L_13, L_16, _stringLiteral3420, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		GameObject_t3674682005 * L_18 = V_0;
		NullCheck(L_18);
		Transform_t1659122786 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = Transform_FindChild_m2149912886(L_19, _stringLiteral2117, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t1659122786 * L_21 = Component_get_transform_m4257140443(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = Transform_FindChild_m2149912886(L_21, _stringLiteral1581134692, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = Component_get_transform_m4257140443(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Transform_FindChild_m2149912886(L_23, _stringLiteral1725724899, /*hidden argument*/NULL);
		NullCheck(L_24);
		Text_t9039225 * L_25 = Component_GetComponent_TisText_t9039225_m1610753993(L_24, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		float L_27 = __this->get_tempingAmount_12();
		int32_t L_28 = (((int32_t)((int32_t)L_27)));
		Il2CppObject * L_29 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_28);
		String_t* L_30 = String_Concat_m2809334143(NULL /*static, unused*/, L_26, L_29, _stringLiteral3420, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_30);
		GameObject_t3674682005 * L_31 = V_0;
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = GameObject_get_transform_m1278640159(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = Transform_FindChild_m2149912886(L_32, _stringLiteral2117, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = Component_get_transform_m4257140443(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = Transform_FindChild_m2149912886(L_34, _stringLiteral1581134692, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = Component_get_transform_m4257140443(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t1659122786 * L_37 = Transform_FindChild_m2149912886(L_36, _stringLiteral2329455453, /*hidden argument*/NULL);
		NullCheck(L_37);
		Text_t9039225 * L_38 = Component_GetComponent_TisText_t9039225_m1610753993(L_37, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		String_t* L_39 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		float L_40 = __this->get_tempingLimitAmount_11();
		int32_t L_41 = (((int32_t)((int32_t)L_40)));
		Il2CppObject * L_42 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_41);
		String_t* L_43 = String_Concat_m2809334143(NULL /*static, unused*/, L_39, L_42, _stringLiteral3420, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, L_43);
		float L_44 = __this->get_tempingAmount_12();
		float L_45 = __this->get_tempingLimitAmount_11();
		float L_46 = __this->get_tempingLimitAmount_11();
		float L_47 = fabsf(((float)((float)((float)((float)((float)((float)L_44-(float)L_45))/(float)L_46))*(float)(100.0f))));
		V_1 = ((float)((float)(100.0f)-(float)L_47));
		SpriteU5BU5D_t2761310900* L_48 = Resources_LoadAll_TisSprite_t3199167241_m634693541(NULL /*static, unused*/, _stringLiteral2166079755, /*hidden argument*/Resources_LoadAll_TisSprite_t3199167241_m634693541_MethodInfo_var);
		__this->set_res_9(L_48);
		GameObject_t3674682005 * L_49 = V_0;
		NullCheck(L_49);
		Transform_t1659122786 * L_50 = GameObject_get_transform_m1278640159(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Transform_FindChild_m2149912886(L_50, _stringLiteral2117, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t1659122786 * L_52 = Component_get_transform_m4257140443(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = Transform_FindChild_m2149912886(L_52, _stringLiteral1581134692, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_t1659122786 * L_54 = Component_get_transform_m4257140443(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t1659122786 * L_55 = Transform_FindChild_m2149912886(L_54, _stringLiteral323663835, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t1659122786 * L_56 = Component_get_transform_m4257140443(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_t1659122786 * L_57 = Transform_FindChild_m2149912886(L_56, _stringLiteral39144429, /*hidden argument*/NULL);
		NullCheck(L_57);
		Image_t538875265 * L_58 = Component_GetComponent_TisImage_t538875265_m3706520426(L_57, /*hidden argument*/Component_GetComponent_TisImage_t538875265_m3706520426_MethodInfo_var);
		SpriteU5BU5D_t2761310900* L_59 = __this->get_res_9();
		float L_60 = V_1;
		int32_t L_61 = ScoreManager_Temping_SwitchErrorNum_m387527416(__this, L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_61);
		int32_t L_62 = L_61;
		Sprite_t3199167241 * L_63 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_58);
		Image_set_sprite_m572551402(L_58, L_63, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_64 = V_0;
		NullCheck(L_64);
		GameObject_SetActive_m3538205401(L_64, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::ButtonPassClick()
extern Il2CppCodeGenString* _stringLiteral3268531790;
extern Il2CppCodeGenString* _stringLiteral2501482401;
extern Il2CppCodeGenString* _stringLiteral4107675593;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral721162579;
extern Il2CppCodeGenString* _stringLiteral773600752;
extern Il2CppCodeGenString* _stringLiteral2949902318;
extern Il2CppCodeGenString* _stringLiteral2320122506;
extern const uint32_t ScoreManager_Temping_ButtonPassClick_m1576181012_MetadataUsageId;
extern "C"  void ScoreManager_Temping_ButtonPassClick_m1576181012 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_ButtonPassClick_m1576181012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Transform_FindChild_m2149912886(L_0, _stringLiteral3268531790, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Transform_FindChild_m2149912886(L_2, _stringLiteral2501482401, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Transform_FindChild_m2149912886(L_5, _stringLiteral3268531790, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Transform_FindChild_m2149912886(L_7, _stringLiteral4107675593, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = Transform_FindChild_m2149912886(L_11, _stringLiteral721162579, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = Component_get_gameObject_m1170635899(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_SetActive_m3538205401(L_13, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_14 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral773600752, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = GameObject_get_transform_m1278640159(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = Transform_FindChild_m2149912886(L_15, _stringLiteral2949902318, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = Component_get_transform_m4257140443(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t1659122786 * L_18 = Transform_FindChild_m2149912886(L_17, _stringLiteral2320122506, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_t3674682005 * L_19 = Component_get_gameObject_m1170635899(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SetActive_m3538205401(L_19, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::ButtonRetryClick()
extern const MethodInfo* GameObject_GetComponent_TisTempingPointer_t2678149871_m846601486_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral721162579;
extern Il2CppCodeGenString* _stringLiteral80998175;
extern Il2CppCodeGenString* _stringLiteral815429745;
extern Il2CppCodeGenString* _stringLiteral1320885114;
extern Il2CppCodeGenString* _stringLiteral773600752;
extern const uint32_t ScoreManager_Temping_ButtonRetryClick_m2574026341_MetadataUsageId;
extern "C"  void ScoreManager_Temping_ButtonRetryClick_m2574026341 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Temping_ButtonRetryClick_m2574026341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TempingPointer_t2678149871 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_FindChild_m2149912886(L_1, _stringLiteral721162579, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m3538205401(L_3, (bool)0, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_4 = __this->get_sw_6();
		NullCheck(L_4);
		Stopwatch_Start_m3677209584(L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_5 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = GameObject_get_transform_m1278640159(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_FindChild_m2149912886(L_6, _stringLiteral80998175, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Transform_FindChild_m2149912886(L_8, _stringLiteral815429745, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_t3674682005 * L_10 = Component_get_gameObject_m1170635899(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = Transform_FindChild_m2149912886(L_12, _stringLiteral1320885114, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = Component_get_gameObject_m1170635899(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SetActive_m3538205401(L_14, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_15 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral773600752, /*hidden argument*/NULL);
		NullCheck(L_15);
		TempingPointer_t2678149871 * L_16 = GameObject_GetComponent_TisTempingPointer_t2678149871_m846601486(L_15, /*hidden argument*/GameObject_GetComponent_TisTempingPointer_t2678149871_m846601486_MethodInfo_var);
		V_0 = L_16;
		TempingPointer_t2678149871 * L_17 = V_0;
		NullCheck(L_17);
		TempingPointer_Init_m1667348040(L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Temping::EndOfExtracting()
extern "C"  void ScoreManager_Temping_EndOfExtracting_m1906868770 (ScoreManager_Temping_t3029572618 * __this, const MethodInfo* method)
{
	{
		ScoreManager_Temping_Confirm_m4174217775(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ScoreManager_Temping::SwitchErrorNum(System.Single)
extern "C"  int32_t ScoreManager_Temping_SwitchErrorNum_m387527416 (ScoreManager_Temping_t3029572618 * __this, float ___errorRate0, const MethodInfo* method)
{
	{
		float L_0 = ___errorRate0;
		if ((!(((float)L_0) >= ((float)(95.0f)))))
		{
			goto IL_000d;
		}
	}
	{
		return 4;
	}

IL_000d:
	{
		float L_1 = ___errorRate0;
		if ((!(((float)L_1) >= ((float)(90.0f)))))
		{
			goto IL_001a;
		}
	}
	{
		return 3;
	}

IL_001a:
	{
		float L_2 = ___errorRate0;
		if ((!(((float)L_2) >= ((float)(70.0f)))))
		{
			goto IL_0027;
		}
	}
	{
		return 2;
	}

IL_0027:
	{
		float L_3 = ___errorRate0;
		if ((!(((float)L_3) >= ((float)(60.0f)))))
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}

IL_0034:
	{
		return 0;
	}
}
// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::.ctor()
extern "C"  void U3CREandStU3Ec__Iterator22__ctor_m3813340298 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Temping/<REandSt>c__Iterator22::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CREandStU3Ec__Iterator22_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2859661448 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ScoreManager_Temping/<REandSt>c__Iterator22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CREandStU3Ec__Iterator22_System_Collections_IEnumerator_get_Current_m142019612 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean ScoreManager_Temping/<REandSt>c__Iterator22::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral3754286315;
extern Il2CppCodeGenString* _stringLiteral77848963;
extern Il2CppCodeGenString* _stringLiteral79219778;
extern Il2CppCodeGenString* _stringLiteral846339116;
extern Il2CppCodeGenString* _stringLiteral80998175;
extern const uint32_t U3CREandStU3Ec__Iterator22_MoveNext_m419205930_MetadataUsageId;
extern "C"  bool U3CREandStU3Ec__Iterator22_MoveNext_m419205930 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CREandStU3Ec__Iterator22_MoveNext_m419205930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
		if (L_1 == 2)
		{
			goto IL_00a0;
		}
		if (L_1 == 3)
		{
			goto IL_0107;
		}
	}
	{
		goto IL_019d;
	}

IL_0029:
	{
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (0.5f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_019f;
	}

IL_0045:
	{
		GameObject_t3674682005 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_FindChild_m2149912886(L_4, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_FindChild_m2149912886(L_6, _stringLiteral77848963, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		__this->set_U3C_objU3E__0_0(L_8);
		GameObject_t3674682005 * L_9 = __this->get_U3C_objU3E__0_0();
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)1, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_10 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_10, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_10);
		__this->set_U24PC_1(2);
		goto IL_019f;
	}

IL_00a0:
	{
		GameObject_t3674682005 * L_11 = __this->get_U3C_objU3E__0_0();
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_12 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = GameObject_get_transform_m1278640159(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t1659122786 * L_14 = Transform_FindChild_m2149912886(L_13, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = Component_get_transform_m4257140443(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = Transform_FindChild_m2149912886(L_15, _stringLiteral79219778, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = Component_get_gameObject_m1170635899(L_16, /*hidden argument*/NULL);
		__this->set_U3C_objU3E__0_0(L_17);
		GameObject_t3674682005 * L_18 = __this->get_U3C_objU3E__0_0();
		NullCheck(L_18);
		GameObject_SetActive_m3538205401(L_18, (bool)1, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_19 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_19, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_19);
		__this->set_U24PC_1(3);
		goto IL_019f;
	}

IL_0107:
	{
		GameObject_t3674682005 * L_20 = __this->get_U3C_objU3E__0_0();
		NullCheck(L_20);
		GameObject_SetActive_m3538205401(L_20, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_21 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = Transform_FindChild_m2149912886(L_22, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Component_get_transform_m4257140443(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = Transform_FindChild_m2149912886(L_24, _stringLiteral846339116, /*hidden argument*/NULL);
		NullCheck(L_25);
		GameObject_t3674682005 * L_26 = Component_get_gameObject_m1170635899(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_SetActive_m3538205401(L_26, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_27 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_t1659122786 * L_28 = GameObject_get_transform_m1278640159(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = Transform_FindChild_m2149912886(L_28, _stringLiteral80998175, /*hidden argument*/NULL);
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = Component_get_gameObject_m1170635899(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		GameObject_SetActive_m3538205401(L_30, (bool)1, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_31 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Temping_t3029572618 * L_32 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_32);
		Il2CppObject * L_33 = ScoreManager_Temping_TimerCheck_m1109030942(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		MonoBehaviour_StartCoroutine_m2135303124(L_31, L_33, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_34 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_34);
		Stopwatch_t3420517611 * L_35 = L_34->get_sw_6();
		NullCheck(L_35);
		Stopwatch_Start_m3677209584(L_35, /*hidden argument*/NULL);
		goto IL_019d;
	}
	// Dead block : IL_0196: ldarg.0

IL_019d:
	{
		return (bool)0;
	}

IL_019f:
	{
		return (bool)1;
	}
}
// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::Dispose()
extern "C"  void U3CREandStU3Ec__Iterator22_Dispose_m911205959 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ScoreManager_Temping/<REandSt>c__Iterator22::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CREandStU3Ec__Iterator22_Reset_m1459773239_MetadataUsageId;
extern "C"  void U3CREandStU3Ec__Iterator22_Reset_m1459773239 (U3CREandStU3Ec__Iterator22_t438786049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CREandStU3Ec__Iterator22_Reset_m1459773239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator24__ctor_m3658756671 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Temping/<TimeEnd>c__Iterator24::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator24_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3789971955 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Temping/<TimeEnd>c__Iterator24::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator24_System_Collections_IEnumerator_get_Current_m1242082183 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Temping/<TimeEnd>c__Iterator24::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator24_MoveNext_m3718282581_MetadataUsageId;
extern "C"  bool U3CTimeEndU3Ec__Iterator24_MoveNext_m3718282581 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator24_MoveNext_m3718282581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
		if (L_1 == 2)
		{
			goto IL_00b3;
		}
	}
	{
		goto IL_00d5;
	}

IL_0025:
	{
		ScoreManager_Temping_t3029572618 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Stopwatch_t3420517611 * L_3 = L_2->get_sw_6();
		NullCheck(L_3);
		Stopwatch_Stop_m2612884438(L_3, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00d7;
	}

IL_0051:
	{
		ScoreManager_Temping_t3029572618 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		L_5->set_ticks_10((0.0f));
		ScoreManager_Temping_t3029572618 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		StructforMinigame_t1286533533 * L_7 = L_6->get_stuff_4();
		ScoreManager_Temping_t3029572618 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		ConstforMinigame_t2789090703 * L_9 = L_8->get_cuff_5();
		NullCheck(L_9);
		float L_10 = ConstforMinigame_get_Limittime_m4054875741(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		StructforMinigame_set_Playtime_m3659740247(L_7, L_10, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		PopupManagement_t282433007 * L_12 = L_11->get_pop_7();
		NullCheck(L_12);
		PopupManagement_makeIt_m1215430652(L_12, 4, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0097:
	{
		WaitForSecondsRealtime_t3698499994 * L_13 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_13, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_13);
		__this->set_U24PC_0(2);
		goto IL_00d7;
	}

IL_00b3:
	{
		ScoreManager_Temping_t3029572618 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		PopupManagement_t282433007 * L_15 = L_14->get_pop_7();
		NullCheck(L_15);
		Object_t3071478659 * L_16 = PopupManagement_get_PopupObject_m1866634147(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_16, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0097;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_00d5:
	{
		return (bool)0;
	}

IL_00d7:
	{
		return (bool)1;
	}
	// Dead block : IL_00d9: ldloc.1
}
// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator24_Dispose_m2680195772 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Temping/<TimeEnd>c__Iterator24::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator24_Reset_m1305189612_MetadataUsageId;
extern "C"  void U3CTimeEndU3Ec__Iterator24_Reset_m1305189612 (U3CTimeEndU3Ec__Iterator24_t3910199980 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator24_Reset_m1305189612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator23__ctor_m1392936621 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Temping/<TimerCheck>c__Iterator23::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator23_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m402183311 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ScoreManager_Temping/<TimerCheck>c__Iterator23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator23_System_Collections_IEnumerator_get_Current_m539743779 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean ScoreManager_Temping/<TimerCheck>c__Iterator23::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator23_MoveNext_m4060313871 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00d6;
	}

IL_0021:
	{
		ScoreManager_Temping_t3029572618 * L_2 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Temping_t3029572618 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		Stopwatch_t3420517611 * L_4 = L_3->get_sw_6();
		NullCheck(L_4);
		int64_t L_5 = Stopwatch_get_ElapsedMilliseconds_m23977474(L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_ticks_10(((float)((float)((float)((float)(((float)((float)L_5)))+(float)L_6))/(float)(1000.0f))));
		ScoreManager_Temping_t3029572618 * L_7 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		ConstforMinigame_t2789090703 * L_8 = L_7->get_cuff_5();
		NullCheck(L_8);
		float L_9 = ConstforMinigame_get_Limittime_m4054875741(L_8, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_10 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		float L_11 = L_10->get_ticks_10();
		double L_12 = Math_Round_m2351753873(NULL /*static, unused*/, (((double)((double)L_11))), 0, /*hidden argument*/NULL);
		__this->set_U3CitU3E__0_0((((int32_t)((int32_t)((double)((double)(((double)((double)L_9)))-(double)L_12))))));
		ScoreManager_Temping_t3029572618 * L_13 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		Text_t9039225 * L_14 = L_13->get_timmer_8();
		int32_t* L_15 = __this->get_address_of_U3CitU3E__0_0();
		String_t* L_16 = Int32_ToString_m1286526384(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_16);
		int32_t L_17 = __this->get_U3CitU3E__0_0();
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_00b7;
		}
	}
	{
		ScoreManager_Temping_t3029572618 * L_18 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Temping_t3029572618 * L_19 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_19);
		Il2CppObject * L_20 = ScoreManager_Temping_TimeEnd_m653211509(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		MonoBehaviour_StartCoroutine_m2135303124(L_18, L_20, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b7:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_00d8;
	}

IL_00ca:
	{
		goto IL_0021;
	}
	// Dead block : IL_00cf: ldarg.0

IL_00d6:
	{
		return (bool)0;
	}

IL_00d8:
	{
		return (bool)1;
	}
}
// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator23_Dispose_m2775546794 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ScoreManager_Temping/<TimerCheck>c__Iterator23::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerCheckU3Ec__Iterator23_Reset_m3334336858_MetadataUsageId;
extern "C"  void U3CTimerCheckU3Ec__Iterator23_Reset_m3334336858 (U3CTimerCheckU3Ec__Iterator23_t859433390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerCheckU3Ec__Iterator23_Reset_m3334336858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Variation::.ctor()
extern "C"  void ScoreManager_Variation__ctor_m1171556844 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation::Start()
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t ScoreManager_Variation_Start_m118694636_MetadataUsageId;
extern "C"  void ScoreManager_Variation_Start_m118694636 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_Start_m118694636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1080795294  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Scene_t1080795294  L_2 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = Scene_get_name_m894591657((&V_1), /*hidden argument*/NULL);
		__this->set_nowScene_3(L_3);
		PopupManagement_t282433007 * L_4 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_pop_11(L_4);
		int32_t L_5 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_6 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_stuff_9(L_6);
		int32_t L_7 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_10 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_7, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_10(L_10);
		StructforMinigame_t1286533533 * L_11 = __this->get_stuff_9();
		NullCheck(L_11);
		Stopwatch_t3420517611 * L_12 = StructforMinigame_get_Sw_m1790624722(L_11, /*hidden argument*/NULL);
		__this->set_sw_15(L_12);
		Stopwatch_t3420517611 * L_13 = __this->get_sw_15();
		NullCheck(L_13);
		Stopwatch_Reset_m2376504733(L_13, /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_14 = __this->get_pop_11();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Variation::CheckIngredient()
extern Il2CppClass* U3CCheckIngredientU3Ec__Iterator25_t55506035_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Variation_CheckIngredient_m4221245755_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Variation_CheckIngredient_m4221245755 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_CheckIngredient_m4221245755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckIngredientU3Ec__Iterator25_t55506035 * V_0 = NULL;
	{
		U3CCheckIngredientU3Ec__Iterator25_t55506035 * L_0 = (U3CCheckIngredientU3Ec__Iterator25_t55506035 *)il2cpp_codegen_object_new(U3CCheckIngredientU3Ec__Iterator25_t55506035_il2cpp_TypeInfo_var);
		U3CCheckIngredientU3Ec__Iterator25__ctor_m1382725208(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckIngredientU3Ec__Iterator25_t55506035 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCheckIngredientU3Ec__Iterator25_t55506035 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Variation::buttonOkClickSound()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Variation_buttonOkClickSound_m2844652525_MetadataUsageId;
extern "C"  void ScoreManager_Variation_buttonOkClickSound_m2844652525 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_buttonOkClickSound_m2844652525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation::startVariation()
extern "C"  void ScoreManager_Variation_startVariation_m258681993 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_ready_7();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation::RestartThisGame()
extern "C"  void ScoreManager_Variation_RestartThisGame_m551128809 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_nowScene_3();
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation::ButtonClickEvent(PlayMakerFSM)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1008886500;
extern const uint32_t ScoreManager_Variation_ButtonClickEvent_m3233447116_MetadataUsageId;
extern "C"  void ScoreManager_Variation_ButtonClickEvent_m3233447116 (ScoreManager_Variation_t1665153935 * __this, PlayMakerFSM_t3799847376 * ____myFsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_ButtonClickEvent_m3233447116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = ____myFsm0;
		__this->set_myFsm_12(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1008886500, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation::isRightSelect(System.Int32)
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2524;
extern Il2CppCodeGenString* _stringLiteral65509;
extern const uint32_t ScoreManager_Variation_isRightSelect_m275584681_MetadataUsageId;
extern "C"  void ScoreManager_Variation_isRightSelect_m275584681 (ScoreManager_Variation_t1665153935 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_isRightSelect_m275584681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_0 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_cream_11();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = 1;
		goto IL_001a;
	}

IL_0018:
	{
		V_0 = 0;
	}

IL_001a:
	{
		int32_t L_2 = __this->get_currentIngredient_14();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_3 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Int32U5BU5DU5BU5D_t1820556512* L_4 = L_3->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_5 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_menu_10();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
		int32_t L_7 = L_6;
		Int32U5BU5D_t3230847821* L_8 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		int32_t L_9 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))+(int32_t)L_9)))))
		{
			goto IL_01fe;
		}
	}
	{
		int32_t L_10 = __this->get_currentIngredient_14();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_11 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Int32U5BU5DU5BU5D_t1820556512* L_12 = L_11->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_13 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = L_13->get_menu_10();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		int32_t L_15 = L_14;
		Int32U5BU5D_t3230847821* L_16 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		if ((!(((uint32_t)L_10) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))))))
		{
			goto IL_00db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_17 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_cream_11();
		if (L_18)
		{
			goto IL_00db;
		}
	}
	{
		int32_t L_19 = ___num0;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)41)))))
		{
			goto IL_00db;
		}
	}
	{
		ShowRecipePre_t977589144 * L_20 = __this->get_recipePre_6();
		NullCheck(L_20);
		ShowRecipePre_RightChoice_m3387411854(L_20, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_currentIngredient_14();
		__this->set_currentIngredient_14(((int32_t)((int32_t)L_21+(int32_t)1)));
		PlayMakerFSM_t3799847376 * L_22 = __this->get_myFsm_12();
		NullCheck(L_22);
		Fsm_t1527112426 * L_23 = PlayMakerFSM_get_Fsm_m886945091(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Fsm_Event_m4127177141(L_23, _stringLiteral2524, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_currentIngredient_14();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_25 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		Int32U5BU5DU5BU5D_t1820556512* L_26 = L_25->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_27 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_27);
		int32_t L_28 = L_27->get_menu_10();
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_28);
		int32_t L_29 = L_28;
		Int32U5BU5D_t3230847821* L_30 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_30);
		int32_t L_31 = V_0;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))+(int32_t)L_31))))))
		{
			goto IL_00d6;
		}
	}
	{
		Il2CppObject * L_32 = ScoreManager_Variation_CheckIngredient_m4221245755(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_32, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		goto IL_01f9;
	}

IL_00db:
	{
		int32_t L_33 = __this->get_currentIngredient_14();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_34 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		Int32U5BU5DU5BU5D_t1820556512* L_35 = L_34->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_36 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_36);
		int32_t L_37 = L_36->get_menu_10();
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_37);
		int32_t L_38 = L_37;
		Int32U5BU5D_t3230847821* L_39 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_39);
		if ((!(((uint32_t)L_33) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length))))))))
		{
			goto IL_0146;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_40 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_40);
		int32_t L_41 = L_40->get_cream_11();
		if (L_41)
		{
			goto IL_0146;
		}
	}
	{
		int32_t L_42 = ___num0;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)41))))
		{
			goto IL_0146;
		}
	}
	{
		int32_t L_43 = __this->get_missCount_13();
		__this->set_missCount_13(((int32_t)((int32_t)L_43+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_44 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_44);
		SoundManage_efxBadOn_m2820389210(L_44, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_45 = __this->get_myFsm_12();
		NullCheck(L_45);
		Fsm_t1527112426 * L_46 = PlayMakerFSM_get_Fsm_m886945091(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Fsm_Event_m4127177141(L_46, _stringLiteral65509, /*hidden argument*/NULL);
		goto IL_01f9;
	}

IL_0146:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_47 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		Int32U5BU5DU5BU5D_t1820556512* L_48 = L_47->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_49 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_49);
		int32_t L_50 = L_49->get_menu_10();
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_50);
		int32_t L_51 = L_50;
		Int32U5BU5D_t3230847821* L_52 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		int32_t L_53 = __this->get_currentIngredient_14();
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = L_53;
		int32_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		int32_t L_56 = ___num0;
		if ((!(((uint32_t)L_55) == ((uint32_t)L_56))))
		{
			goto IL_01cc;
		}
	}
	{
		ShowRecipePre_t977589144 * L_57 = __this->get_recipePre_6();
		NullCheck(L_57);
		ShowRecipePre_RightChoice_m3387411854(L_57, /*hidden argument*/NULL);
		int32_t L_58 = __this->get_currentIngredient_14();
		__this->set_currentIngredient_14(((int32_t)((int32_t)L_58+(int32_t)1)));
		PlayMakerFSM_t3799847376 * L_59 = __this->get_myFsm_12();
		NullCheck(L_59);
		Fsm_t1527112426 * L_60 = PlayMakerFSM_get_Fsm_m886945091(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Fsm_Event_m4127177141(L_60, _stringLiteral2524, /*hidden argument*/NULL);
		int32_t L_61 = __this->get_currentIngredient_14();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_62 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_62);
		Int32U5BU5DU5BU5D_t1820556512* L_63 = L_62->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_64 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_64);
		int32_t L_65 = L_64->get_menu_10();
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_65);
		int32_t L_66 = L_65;
		Int32U5BU5D_t3230847821* L_67 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		NullCheck(L_67);
		int32_t L_68 = V_0;
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_67)->max_length))))+(int32_t)L_68))))))
		{
			goto IL_01c7;
		}
	}
	{
		Il2CppObject * L_69 = ScoreManager_Variation_CheckIngredient_m4221245755(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_69, /*hidden argument*/NULL);
	}

IL_01c7:
	{
		goto IL_01f9;
	}

IL_01cc:
	{
		int32_t L_70 = __this->get_missCount_13();
		__this->set_missCount_13(((int32_t)((int32_t)L_70+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_71 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_71);
		SoundManage_efxBadOn_m2820389210(L_71, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_72 = __this->get_myFsm_12();
		NullCheck(L_72);
		Fsm_t1527112426 * L_73 = PlayMakerFSM_get_Fsm_m886945091(L_72, /*hidden argument*/NULL);
		NullCheck(L_73);
		Fsm_Event_m4127177141(L_73, _stringLiteral65509, /*hidden argument*/NULL);
	}

IL_01f9:
	{
		goto IL_022b;
	}

IL_01fe:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_74 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_74);
		SoundManage_efxBadOn_m2820389210(L_74, /*hidden argument*/NULL);
		int32_t L_75 = __this->get_missCount_13();
		__this->set_missCount_13(((int32_t)((int32_t)L_75+(int32_t)1)));
		PlayMakerFSM_t3799847376 * L_76 = __this->get_myFsm_12();
		NullCheck(L_76);
		Fsm_t1527112426 * L_77 = PlayMakerFSM_get_Fsm_m886945091(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Fsm_Event_m4127177141(L_77, _stringLiteral65509, /*hidden argument*/NULL);
	}

IL_022b:
	{
		return;
	}
}
// System.Void ScoreManager_Variation::TimeCheckerStart()
extern "C"  void ScoreManager_Variation_TimeCheckerStart_m1950168562 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ScoreManager_Variation_TimerCheck_m557678307(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ScoreManager_Variation::TimerCheck()
extern Il2CppClass* U3CTimerCheckU3Ec__Iterator26_t3278601462_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Variation_TimerCheck_m557678307_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Variation_TimerCheck_m557678307 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_TimerCheck_m557678307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimerCheckU3Ec__Iterator26_t3278601462 * V_0 = NULL;
	{
		U3CTimerCheckU3Ec__Iterator26_t3278601462 * L_0 = (U3CTimerCheckU3Ec__Iterator26_t3278601462 *)il2cpp_codegen_object_new(U3CTimerCheckU3Ec__Iterator26_t3278601462_il2cpp_TypeInfo_var);
		U3CTimerCheckU3Ec__Iterator26__ctor_m3907616357(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimerCheckU3Ec__Iterator26_t3278601462 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CTimerCheckU3Ec__Iterator26_t3278601462 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator ScoreManager_Variation::TimeEnd()
extern Il2CppClass* U3CTimeEndU3Ec__Iterator27_t1567663498_il2cpp_TypeInfo_var;
extern const uint32_t ScoreManager_Variation_TimeEnd_m2239639248_MetadataUsageId;
extern "C"  Il2CppObject * ScoreManager_Variation_TimeEnd_m2239639248 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_TimeEnd_m2239639248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTimeEndU3Ec__Iterator27_t1567663498 * V_0 = NULL;
	{
		U3CTimeEndU3Ec__Iterator27_t1567663498 * L_0 = (U3CTimeEndU3Ec__Iterator27_t1567663498 *)il2cpp_codegen_object_new(U3CTimeEndU3Ec__Iterator27_t1567663498_il2cpp_TypeInfo_var);
		U3CTimeEndU3Ec__Iterator27__ctor_m3270293281(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTimeEndU3Ec__Iterator27_t1567663498 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTimeEndU3Ec__Iterator27_t1567663498 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScoreManager_Variation::conFirm()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern Il2CppCodeGenString* _stringLiteral3531583;
extern Il2CppCodeGenString* _stringLiteral3084718;
extern const uint32_t ScoreManager_Variation_conFirm_m802853834_MetadataUsageId;
extern "C"  void ScoreManager_Variation_conFirm_m802853834 (ScoreManager_Variation_t1665153935 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManager_Variation_conFirm_m802853834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		V_0 = (0.0f);
		V_1 = (0.0f);
		V_2 = (0.0f);
		StructforMinigame_t1286533533 * L_1 = __this->get_stuff_9();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_2 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_grade_5();
		NullCheck(L_1);
		StructforMinigame_set_Level_m547274884(L_1, L_3, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_4 = __this->get_stuff_9();
		int32_t L_5 = __this->get_missCount_13();
		NullCheck(L_4);
		StructforMinigame_set_Misscount_m2498396499(L_4, L_5, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_6 = __this->get_stuff_9();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_7 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_7;
		String_t* L_8 = DateTime_ToString_m3415116655((&V_3), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_6);
		StructforMinigame_set_RecordDate_m450818768(L_6, L_8, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_9 = __this->get_sw_15();
		NullCheck(L_9);
		Stopwatch_Stop_m2612884438(L_9, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_10 = __this->get_stuff_9();
		float L_11 = __this->get_ticks_4();
		NullCheck(L_10);
		StructforMinigame_set_Playtime_m3659740247(L_10, L_11, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_12 = __this->get_stuff_9();
		NullCheck(L_12);
		float L_13 = StructforMinigame_get_Playtime_m2221809428(L_12, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_14 = __this->get_cuff_10();
		NullCheck(L_14);
		float L_15 = ConstforMinigame_get_Besttime_m1687094942(L_14, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_16 = __this->get_cuff_10();
		NullCheck(L_16);
		float L_17 = ConstforMinigame_get_Limittime_m4054875741(L_16, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_18 = __this->get_cuff_10();
		NullCheck(L_18);
		float L_19 = ConstforMinigame_get_Besttime_m1687094942(L_18, /*hidden argument*/NULL);
		float L_20 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_13-(float)L_15))/(float)((float)((float)L_17-(float)L_19)))), /*hidden argument*/NULL);
		float L_21 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		StructforMinigame_t1286533533 * L_22 = __this->get_stuff_9();
		StructforMinigame_t1286533533 * L_23 = __this->get_stuff_9();
		NullCheck(L_23);
		int32_t L_24 = StructforMinigame_get_Misscount_m2827575940(L_23, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_25 = __this->get_cuff_10();
		NullCheck(L_25);
		float L_26 = ConstforMinigame_get_Errorpenalty_m3689665390(L_25, /*hidden argument*/NULL);
		NullCheck(L_22);
		StructforMinigame_set_ErrorRate_m2862618580(L_22, ((float)((float)(((float)((float)L_24)))*(float)L_26)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_27 = __this->get_stuff_9();
		NullCheck(L_27);
		float L_28 = StructforMinigame_get_ErrorRate_m2504854007(L_27, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_29 = __this->get_cuff_10();
		NullCheck(L_29);
		float L_30 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_29, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_31 = __this->get_cuff_10();
		NullCheck(L_31);
		float L_32 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_31, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_33 = __this->get_cuff_10();
		NullCheck(L_33);
		float L_34 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_33, /*hidden argument*/NULL);
		float L_35 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_28-(float)L_30))/(float)((float)((float)L_32-(float)L_34)))), /*hidden argument*/NULL);
		float L_36 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_35, /*hidden argument*/NULL);
		V_2 = L_36;
		float L_37 = V_0;
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3531583, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		float L_41 = V_2;
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_42);
		String_t* L_44 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3084718, L_43, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_45 = __this->get_stuff_9();
		float L_46 = V_0;
		float L_47 = V_2;
		NullCheck(L_45);
		StructforMinigame_set_Score_m1127316490(L_45, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_46+(float)L_47))*(float)(50.0f))))), /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_48 = __this->get_pop_11();
		NullCheck(L_48);
		PopupManagement_makeIt_m1215430652(L_48, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::.ctor()
extern "C"  void U3CCheckIngredientU3Ec__Iterator25__ctor_m1382725208 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Variation/<CheckIngredient>c__Iterator25::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckIngredientU3Ec__Iterator25_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1049711034 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Variation/<CheckIngredient>c__Iterator25::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckIngredientU3Ec__Iterator25_System_Collections_IEnumerator_get_Current_m1325533518 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Variation/<CheckIngredient>c__Iterator25::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckIngredientU3Ec__Iterator25_MoveNext_m3381774236_MetadataUsageId;
extern "C"  bool U3CCheckIngredientU3Ec__Iterator25_MoveNext_m3381774236 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckIngredientU3Ec__Iterator25_MoveNext_m3381774236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	{
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (5.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0057;
	}

IL_003d:
	{
		ScoreManager_Variation_t1665153935 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = L_3->get_buttonOk_8();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0055:
	{
		return (bool)0;
	}

IL_0057:
	{
		return (bool)1;
	}
	// Dead block : IL_0059: ldloc.1
}
// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::Dispose()
extern "C"  void U3CCheckIngredientU3Ec__Iterator25_Dispose_m1552313493 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Variation/<CheckIngredient>c__Iterator25::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckIngredientU3Ec__Iterator25_Reset_m3324125445_MetadataUsageId;
extern "C"  void U3CCheckIngredientU3Ec__Iterator25_Reset_m3324125445 (U3CCheckIngredientU3Ec__Iterator25_t55506035 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckIngredientU3Ec__Iterator25_Reset_m3324125445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::.ctor()
extern "C"  void U3CTimeEndU3Ec__Iterator27__ctor_m3270293281 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Variation/<TimeEnd>c__Iterator27::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4252178321 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ScoreManager_Variation/<TimeEnd>c__Iterator27::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimeEndU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m2017484069 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ScoreManager_Variation/<TimeEnd>c__Iterator27::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator27_MoveNext_m4184191539_MetadataUsageId;
extern "C"  bool U3CTimeEndU3Ec__Iterator27_MoveNext_m4184191539 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator27_MoveNext_m4184191539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0051;
		}
		if (L_1 == 2)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_00ea;
	}

IL_0025:
	{
		ScoreManager_Variation_t1665153935 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Stopwatch_t3420517611 * L_3 = L_2->get_sw_15();
		NullCheck(L_3);
		Stopwatch_Stop_m2612884438(L_3, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_4 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_4, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_00ec;
	}

IL_0051:
	{
		ScoreManager_Variation_t1665153935 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		L_5->set_ticks_4((0.0f));
		ScoreManager_Variation_t1665153935 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		StructforMinigame_t1286533533 * L_7 = L_6->get_stuff_9();
		ScoreManager_Variation_t1665153935 * L_8 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_8);
		ConstforMinigame_t2789090703 * L_9 = L_8->get_cuff_10();
		NullCheck(L_9);
		float L_10 = ConstforMinigame_get_Limittime_m4054875741(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		StructforMinigame_set_Playtime_m3659740247(L_7, L_10, /*hidden argument*/NULL);
		ScoreManager_Variation_t1665153935 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		StructforMinigame_t1286533533 * L_12 = L_11->get_stuff_9();
		NullCheck(L_12);
		StructforMinigame_set_ErrorRate_m2862618580(L_12, (100.0f), /*hidden argument*/NULL);
		ScoreManager_Variation_t1665153935 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		PopupManagement_t282433007 * L_14 = L_13->get_pop_11();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 4, /*hidden argument*/NULL);
		goto IL_00c8;
	}

IL_00ac:
	{
		WaitForSecondsRealtime_t3698499994 * L_15 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_15, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_15);
		__this->set_U24PC_0(2);
		goto IL_00ec;
	}

IL_00c8:
	{
		ScoreManager_Variation_t1665153935 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		PopupManagement_t282433007 * L_17 = L_16->get_pop_11();
		NullCheck(L_17);
		Object_t3071478659 * L_18 = PopupManagement_get_PopupObject_m1866634147(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_18, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00ac;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_00ea:
	{
		return (bool)0;
	}

IL_00ec:
	{
		return (bool)1;
	}
	// Dead block : IL_00ee: ldloc.1
}
// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::Dispose()
extern "C"  void U3CTimeEndU3Ec__Iterator27_Dispose_m3029032734 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ScoreManager_Variation/<TimeEnd>c__Iterator27::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimeEndU3Ec__Iterator27_Reset_m916726222_MetadataUsageId;
extern "C"  void U3CTimeEndU3Ec__Iterator27_Reset_m916726222 (U3CTimeEndU3Ec__Iterator27_t1567663498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimeEndU3Ec__Iterator27_Reset_m916726222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator26__ctor_m3907616357 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScoreManager_Variation/<TimerCheck>c__Iterator26::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2946221719 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ScoreManager_Variation/<TimerCheck>c__Iterator26::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m247715883 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean ScoreManager_Variation/<TimerCheck>c__Iterator26::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator26_MoveNext_m3271174487 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00d6;
	}

IL_0021:
	{
		ScoreManager_Variation_t1665153935 * L_2 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Variation_t1665153935 * L_3 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_3);
		Stopwatch_t3420517611 * L_4 = L_3->get_sw_15();
		NullCheck(L_4);
		int64_t L_5 = Stopwatch_get_ElapsedMilliseconds_m23977474(L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_ticks_4(((float)((float)((float)((float)(((float)((float)L_5)))+(float)L_6))/(float)(1000.0f))));
		ScoreManager_Variation_t1665153935 * L_7 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_7);
		ConstforMinigame_t2789090703 * L_8 = L_7->get_cuff_10();
		NullCheck(L_8);
		float L_9 = ConstforMinigame_get_Limittime_m4054875741(L_8, /*hidden argument*/NULL);
		ScoreManager_Variation_t1665153935 * L_10 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		float L_11 = L_10->get_ticks_4();
		double L_12 = Math_Round_m2351753873(NULL /*static, unused*/, (((double)((double)L_11))), 0, /*hidden argument*/NULL);
		__this->set_U3CitU3E__0_0((((int32_t)((int32_t)((double)((double)(((double)((double)L_9)))-(double)L_12))))));
		ScoreManager_Variation_t1665153935 * L_13 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		Text_t9039225 * L_14 = L_13->get_timmer_5();
		int32_t* L_15 = __this->get_address_of_U3CitU3E__0_0();
		String_t* L_16 = Int32_ToString_m1286526384(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_16);
		int32_t L_17 = __this->get_U3CitU3E__0_0();
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_00b7;
		}
	}
	{
		ScoreManager_Variation_t1665153935 * L_18 = __this->get_U3CU3Ef__this_3();
		ScoreManager_Variation_t1665153935 * L_19 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_19);
		Il2CppObject * L_20 = ScoreManager_Variation_TimeEnd_m2239639248(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		MonoBehaviour_StartCoroutine_m2135303124(L_18, L_20, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b7:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_00d8;
	}

IL_00ca:
	{
		goto IL_0021;
	}
	// Dead block : IL_00cf: ldarg.0

IL_00d6:
	{
		return (bool)0;
	}

IL_00d8:
	{
		return (bool)1;
	}
}
// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator26_Dispose_m1316185442 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void ScoreManager_Variation/<TimerCheck>c__Iterator26::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTimerCheckU3Ec__Iterator26_Reset_m1554049298_MetadataUsageId;
extern "C"  void U3CTimerCheckU3Ec__Iterator26_Reset_m1554049298 (U3CTimerCheckU3Ec__Iterator26_t3278601462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTimerCheckU3Ec__Iterator26_Reset_m1554049298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ScoreManagerMiniGame::.ctor()
extern "C"  void ScoreManagerMiniGame__ctor_m2886143031 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManagerMiniGame::Start()
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3395374200;
extern const uint32_t ScoreManagerMiniGame_Start_m1833280823_MetadataUsageId;
extern "C"  void ScoreManagerMiniGame_Start_m1833280823 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManagerMiniGame_Start_m1833280823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1080795294  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Scene_t1080795294  L_2 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = Scene_get_name_m894591657((&V_1), /*hidden argument*/NULL);
		__this->set_nowScene_3(L_3);
		PopupManagement_t282433007 * L_4 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_pop_8(L_4);
		int32_t L_5 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_6 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_stuff_6(L_6);
		int32_t L_7 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_10 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_7, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_7(L_10);
		StructforMinigame_t1286533533 * L_11 = __this->get_stuff_6();
		NullCheck(L_11);
		Stopwatch_t3420517611 * L_12 = StructforMinigame_get_Sw_m1790624722(L_11, /*hidden argument*/NULL);
		__this->set_sw_9(L_12);
		Stopwatch_t3420517611 * L_13 = __this->get_sw_9();
		NullCheck(L_13);
		Stopwatch_Reset_m2376504733(L_13, /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_14 = __this->get_pop_8();
		NullCheck(L_14);
		PopupManagement_makeIt_m1215430652(L_14, 5, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_15 = __this->get_cuff_7();
		NullCheck(L_15);
		float L_16 = ConstforMinigame_get_Limittime_m4054875741(L_15, /*hidden argument*/NULL);
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, _stringLiteral3395374200, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManagerMiniGame::StartMinigame()
extern "C"  void ScoreManagerMiniGame_StartMinigame_m3099216352 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method)
{
	{
		GameObject_t3674682005 * L_0 = __this->get_ready_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreManagerMiniGame::conFirm()
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2532443403;
extern Il2CppCodeGenString* _stringLiteral2246125721;
extern Il2CppCodeGenString* _stringLiteral1349114208;
extern Il2CppCodeGenString* _stringLiteral3531583;
extern Il2CppCodeGenString* _stringLiteral3084718;
extern const uint32_t ScoreManagerMiniGame_conFirm_m3547705173_MetadataUsageId;
extern "C"  void ScoreManagerMiniGame_conFirm_m3547705173 (ScoreManagerMiniGame_t3434204900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreManagerMiniGame_conFirm_m3547705173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	DateTime_t4283661327  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_0 = (0.0f);
		V_1 = (0.0f);
		V_2 = (0.0f);
		float L_0 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral2532443403, /*hidden argument*/NULL);
		V_3 = L_0;
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, _stringLiteral2246125721, /*hidden argument*/NULL);
		V_4 = L_1;
		StructforMinigame_t1286533533 * L_2 = __this->get_stuff_6();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_3 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_grade_5();
		NullCheck(L_2);
		StructforMinigame_set_Level_m547274884(L_2, L_4, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_5 = __this->get_stuff_6();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_6;
		String_t* L_7 = DateTime_ToString_m3415116655((&V_5), _stringLiteral1349114208, /*hidden argument*/NULL);
		NullCheck(L_5);
		StructforMinigame_set_RecordDate_m450818768(L_5, L_7, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_8 = __this->get_sw_9();
		NullCheck(L_8);
		Stopwatch_Stop_m2612884438(L_8, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_9 = __this->get_sw_9();
		NullCheck(L_9);
		Stopwatch_Reset_m2376504733(L_9, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_10 = __this->get_stuff_6();
		ConstforMinigame_t2789090703 * L_11 = __this->get_cuff_7();
		NullCheck(L_11);
		float L_12 = ConstforMinigame_get_Limittime_m4054875741(L_11, /*hidden argument*/NULL);
		float L_13 = V_3;
		NullCheck(L_10);
		StructforMinigame_set_Playtime_m3659740247(L_10, ((float)((float)L_12-(float)L_13)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_14 = __this->get_stuff_6();
		int32_t L_15 = V_4;
		NullCheck(L_14);
		StructforMinigame_set_Misscount_m2498396499(L_14, L_15, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_16 = __this->get_stuff_6();
		NullCheck(L_16);
		float L_17 = StructforMinigame_get_Playtime_m2221809428(L_16, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_18 = __this->get_cuff_7();
		NullCheck(L_18);
		float L_19 = ConstforMinigame_get_Besttime_m1687094942(L_18, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_20 = __this->get_cuff_7();
		NullCheck(L_20);
		float L_21 = ConstforMinigame_get_Limittime_m4054875741(L_20, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_22 = __this->get_cuff_7();
		NullCheck(L_22);
		float L_23 = ConstforMinigame_get_Besttime_m1687094942(L_22, /*hidden argument*/NULL);
		float L_24 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_17-(float)L_19))/(float)((float)((float)L_21-(float)L_23)))), /*hidden argument*/NULL);
		float L_25 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		StructforMinigame_t1286533533 * L_26 = __this->get_stuff_6();
		StructforMinigame_t1286533533 * L_27 = __this->get_stuff_6();
		NullCheck(L_27);
		int32_t L_28 = StructforMinigame_get_Misscount_m2827575940(L_27, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_29 = __this->get_cuff_7();
		NullCheck(L_29);
		float L_30 = ConstforMinigame_get_Errorpenalty_m3689665390(L_29, /*hidden argument*/NULL);
		NullCheck(L_26);
		StructforMinigame_set_ErrorRate_m2862618580(L_26, ((float)((float)(((float)((float)L_28)))*(float)L_30)), /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_31 = __this->get_stuff_6();
		NullCheck(L_31);
		float L_32 = StructforMinigame_get_ErrorRate_m2504854007(L_31, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_33 = __this->get_cuff_7();
		NullCheck(L_33);
		float L_34 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_33, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_35 = __this->get_cuff_7();
		NullCheck(L_35);
		float L_36 = ConstforMinigame_get_LimitErrorPercent_m472954317(L_35, /*hidden argument*/NULL);
		ConstforMinigame_t2789090703 * L_37 = __this->get_cuff_7();
		NullCheck(L_37);
		float L_38 = ConstforMinigame_get_BestErrorPercent_m3513216270(L_37, /*hidden argument*/NULL);
		float L_39 = Math_Min_m2866037191(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_32-(float)L_34))/(float)((float)((float)L_36-(float)L_38)))), /*hidden argument*/NULL);
		float L_40 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		float L_41 = V_0;
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_42);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3531583, L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		float L_45 = V_2;
		float L_46 = L_45;
		Il2CppObject * L_47 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_46);
		String_t* L_48 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3084718, L_47, /*hidden argument*/NULL);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		StructforMinigame_t1286533533 * L_49 = __this->get_stuff_6();
		float L_50 = V_0;
		float L_51 = V_2;
		NullCheck(L_49);
		StructforMinigame_set_Score_m1127316490(L_49, ((float)((float)(100.0f)-(float)((float)((float)((float)((float)L_50+(float)L_51))*(float)(50.0f))))), /*hidden argument*/NULL);
		PopupManagement_t282433007 * L_52 = __this->get_pop_8();
		NullCheck(L_52);
		PopupManagement_makeIt_m1215430652(L_52, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeekBarCtrl::.ctor()
extern "C"  void SeekBarCtrl__ctor_m3817456325 (SeekBarCtrl_t3766823782 * __this, const MethodInfo* method)
{
	{
		__this->set_m_fDragTime_4((0.2f));
		__this->set_m_bActiveDrag_5((bool)1);
		__this->set_m_bUpdate_6((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeekBarCtrl::Start()
extern "C"  void SeekBarCtrl_Start_m2764594117 (SeekBarCtrl_t3766823782 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SeekBarCtrl::Update()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t SeekBarCtrl_Update_m4103891176_MetadataUsageId;
extern "C"  void SeekBarCtrl_Update_m4103891176 (SeekBarCtrl_t3766823782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeekBarCtrl_Update_m4103891176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_bActiveDrag_5();
		if (L_0)
		{
			goto IL_0040;
		}
	}
	{
		float L_1 = __this->get_m_fDeltaTime_7();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_fDeltaTime_7(((float)((float)L_1+(float)L_2)));
		float L_3 = __this->get_m_fDeltaTime_7();
		float L_4 = __this->get_m_fDragTime_4();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0040;
		}
	}
	{
		__this->set_m_bActiveDrag_5((bool)1);
		__this->set_m_fDeltaTime_7((0.0f));
	}

IL_0040:
	{
		bool L_5 = __this->get_m_bUpdate_6();
		if (L_5)
		{
			goto IL_004c;
		}
	}
	{
		return;
	}

IL_004c:
	{
		MediaPlayerCtrl_t3572035536 * L_6 = __this->get_m_srcVideo_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0084;
		}
	}
	{
		Slider_t79469677 * L_8 = __this->get_m_srcSlider_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0084;
		}
	}
	{
		Slider_t79469677 * L_10 = __this->get_m_srcSlider_3();
		MediaPlayerCtrl_t3572035536 * L_11 = __this->get_m_srcVideo_2();
		NullCheck(L_11);
		float L_12 = MediaPlayerCtrl_GetSeekBarValue_m112776625(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_10, L_12);
	}

IL_0084:
	{
		return;
	}
}
// System.Void SeekBarCtrl::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3854121280;
extern const uint32_t SeekBarCtrl_OnPointerEnter_m4286266789_MetadataUsageId;
extern "C"  void SeekBarCtrl_OnPointerEnter_m4286266789 (SeekBarCtrl_t3766823782 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeekBarCtrl_OnPointerEnter_m4286266789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3854121280, /*hidden argument*/NULL);
		__this->set_m_bUpdate_6((bool)0);
		return;
	}
}
// System.Void SeekBarCtrl::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3311202878;
extern const uint32_t SeekBarCtrl_OnPointerExit_m3395613663_MetadataUsageId;
extern "C"  void SeekBarCtrl_OnPointerExit_m3395613663 (SeekBarCtrl_t3766823782 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeekBarCtrl_OnPointerExit_m3395613663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3311202878, /*hidden argument*/NULL);
		__this->set_m_bUpdate_6((bool)1);
		return;
	}
}
// System.Void SeekBarCtrl::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SeekBarCtrl_OnPointerDown_m3002531291 (SeekBarCtrl_t3766823782 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SeekBarCtrl::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void SeekBarCtrl_OnPointerUp_m1304296002 (SeekBarCtrl_t3766823782 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get_m_srcVideo_2();
		Slider_t79469677 * L_1 = __this->get_m_srcSlider_3();
		NullCheck(L_1);
		float L_2 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		NullCheck(L_0);
		MediaPlayerCtrl_SetSeekBarValue_m3613810010(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SeekBarCtrl::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral313845447;
extern const uint32_t SeekBarCtrl_OnDrag_m91731052_MetadataUsageId;
extern "C"  void SeekBarCtrl_OnDrag_m91731052 (SeekBarCtrl_t3766823782 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SeekBarCtrl_OnDrag_m91731052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PointerEventData_t1848751023 * L_0 = ___eventData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral313845447, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_m_bActiveDrag_5();
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Slider_t79469677 * L_3 = __this->get_m_srcSlider_3();
		NullCheck(L_3);
		float L_4 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_3);
		__this->set_m_fLastValue_8(L_4);
		return;
	}

IL_002d:
	{
		MediaPlayerCtrl_t3572035536 * L_5 = __this->get_m_srcVideo_2();
		Slider_t79469677 * L_6 = __this->get_m_srcSlider_3();
		NullCheck(L_6);
		float L_7 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_6);
		NullCheck(L_5);
		MediaPlayerCtrl_SetSeekBarValue_m3613810010(L_5, L_7, /*hidden argument*/NULL);
		Slider_t79469677 * L_8 = __this->get_m_srcSlider_3();
		NullCheck(L_8);
		float L_9 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_8);
		__this->set_m_fLastSetValue_9(L_9);
		__this->set_m_bActiveDrag_5((bool)0);
		return;
	}
}
// System.Void Serving::.ctor()
extern Il2CppClass* StringU5BU2CU5D_t4054002953_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral727411022;
extern Il2CppCodeGenString* _stringLiteral2338853595;
extern Il2CppCodeGenString* _stringLiteral3424007847;
extern Il2CppCodeGenString* _stringLiteral481830216;
extern Il2CppCodeGenString* _stringLiteral1170610445;
extern Il2CppCodeGenString* _stringLiteral1335944469;
extern Il2CppCodeGenString* _stringLiteral406171825;
extern Il2CppCodeGenString* _stringLiteral1611520770;
extern Il2CppCodeGenString* _stringLiteral1006892483;
extern Il2CppCodeGenString* _stringLiteral2740677666;
extern Il2CppCodeGenString* _stringLiteral2603559543;
extern Il2CppCodeGenString* _stringLiteral1186794132;
extern Il2CppCodeGenString* _stringLiteral3381037407;
extern Il2CppCodeGenString* _stringLiteral376833047;
extern Il2CppCodeGenString* _stringLiteral2803072367;
extern Il2CppCodeGenString* _stringLiteral2816769186;
extern Il2CppCodeGenString* _stringLiteral4197631154;
extern Il2CppCodeGenString* _stringLiteral397093700;
extern Il2CppCodeGenString* _stringLiteral2524909272;
extern Il2CppCodeGenString* _stringLiteral486286111;
extern Il2CppCodeGenString* _stringLiteral2220559825;
extern Il2CppCodeGenString* _stringLiteral2960857132;
extern Il2CppCodeGenString* _stringLiteral3511644575;
extern Il2CppCodeGenString* _stringLiteral4212609090;
extern Il2CppCodeGenString* _stringLiteral3417861239;
extern Il2CppCodeGenString* _stringLiteral1038843969;
extern Il2CppCodeGenString* _stringLiteral801969667;
extern Il2CppCodeGenString* _stringLiteral1158570308;
extern Il2CppCodeGenString* _stringLiteral2924350511;
extern Il2CppCodeGenString* _stringLiteral2899455758;
extern const uint32_t Serving__ctor_m953940223_MetadataUsageId;
extern "C"  void Serving__ctor_m953940223 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving__ctor_m953940223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)5 };
		StringU5BU2CU5D_t4054002953* L_0 = (StringU5BU2CU5D_t4054002953*)GenArrayNew(StringU5BU2CU5D_t4054002953_il2cpp_TypeInfo_var, L_1);
		StringU5BU2CU5D_t4054002953* L_2 = L_0;
		NullCheck(L_2);
		(L_2)->SetAt(0, 0, _stringLiteral727411022);
		StringU5BU2CU5D_t4054002953* L_3 = L_2;
		NullCheck(L_3);
		(L_3)->SetAt(0, 1, _stringLiteral2338853595);
		StringU5BU2CU5D_t4054002953* L_4 = L_3;
		NullCheck(L_4);
		(L_4)->SetAt(0, 2, _stringLiteral3424007847);
		StringU5BU2CU5D_t4054002953* L_5 = L_4;
		NullCheck(L_5);
		(L_5)->SetAt(0, 3, _stringLiteral481830216);
		StringU5BU2CU5D_t4054002953* L_6 = L_5;
		NullCheck(L_6);
		(L_6)->SetAt(0, 4, _stringLiteral1170610445);
		StringU5BU2CU5D_t4054002953* L_7 = L_6;
		NullCheck(L_7);
		(L_7)->SetAt(1, 0, _stringLiteral1335944469);
		StringU5BU2CU5D_t4054002953* L_8 = L_7;
		NullCheck(L_8);
		(L_8)->SetAt(1, 1, _stringLiteral406171825);
		StringU5BU2CU5D_t4054002953* L_9 = L_8;
		NullCheck(L_9);
		(L_9)->SetAt(1, 2, _stringLiteral1611520770);
		StringU5BU2CU5D_t4054002953* L_10 = L_9;
		NullCheck(L_10);
		(L_10)->SetAt(1, 3, _stringLiteral1006892483);
		StringU5BU2CU5D_t4054002953* L_11 = L_10;
		NullCheck(L_11);
		(L_11)->SetAt(1, 4, _stringLiteral2740677666);
		StringU5BU2CU5D_t4054002953* L_12 = L_11;
		NullCheck(L_12);
		(L_12)->SetAt(2, 0, _stringLiteral2603559543);
		StringU5BU2CU5D_t4054002953* L_13 = L_12;
		NullCheck(L_13);
		(L_13)->SetAt(2, 1, _stringLiteral1186794132);
		StringU5BU2CU5D_t4054002953* L_14 = L_13;
		NullCheck(L_14);
		(L_14)->SetAt(2, 2, _stringLiteral3381037407);
		StringU5BU2CU5D_t4054002953* L_15 = L_14;
		NullCheck(L_15);
		(L_15)->SetAt(2, 3, _stringLiteral376833047);
		StringU5BU2CU5D_t4054002953* L_16 = L_15;
		NullCheck(L_16);
		(L_16)->SetAt(2, 4, _stringLiteral2803072367);
		StringU5BU2CU5D_t4054002953* L_17 = L_16;
		NullCheck(L_17);
		(L_17)->SetAt(3, 0, _stringLiteral2816769186);
		StringU5BU2CU5D_t4054002953* L_18 = L_17;
		NullCheck(L_18);
		(L_18)->SetAt(3, 1, _stringLiteral4197631154);
		StringU5BU2CU5D_t4054002953* L_19 = L_18;
		NullCheck(L_19);
		(L_19)->SetAt(3, 2, _stringLiteral397093700);
		StringU5BU2CU5D_t4054002953* L_20 = L_19;
		NullCheck(L_20);
		(L_20)->SetAt(3, 3, _stringLiteral2524909272);
		StringU5BU2CU5D_t4054002953* L_21 = L_20;
		NullCheck(L_21);
		(L_21)->SetAt(3, 4, _stringLiteral486286111);
		StringU5BU2CU5D_t4054002953* L_22 = L_21;
		NullCheck(L_22);
		(L_22)->SetAt(4, 0, _stringLiteral2220559825);
		StringU5BU2CU5D_t4054002953* L_23 = L_22;
		NullCheck(L_23);
		(L_23)->SetAt(4, 1, _stringLiteral2960857132);
		StringU5BU2CU5D_t4054002953* L_24 = L_23;
		NullCheck(L_24);
		(L_24)->SetAt(4, 2, _stringLiteral3511644575);
		StringU5BU2CU5D_t4054002953* L_25 = L_24;
		NullCheck(L_25);
		(L_25)->SetAt(4, 3, _stringLiteral4212609090);
		StringU5BU2CU5D_t4054002953* L_26 = L_25;
		NullCheck(L_26);
		(L_26)->SetAt(4, 4, _stringLiteral3417861239);
		StringU5BU2CU5D_t4054002953* L_27 = L_26;
		NullCheck(L_27);
		(L_27)->SetAt(5, 0, _stringLiteral1038843969);
		StringU5BU2CU5D_t4054002953* L_28 = L_27;
		NullCheck(L_28);
		(L_28)->SetAt(5, 1, _stringLiteral801969667);
		StringU5BU2CU5D_t4054002953* L_29 = L_28;
		NullCheck(L_29);
		(L_29)->SetAt(5, 2, _stringLiteral1158570308);
		StringU5BU2CU5D_t4054002953* L_30 = L_29;
		NullCheck(L_30);
		(L_30)->SetAt(5, 3, _stringLiteral2924350511);
		StringU5BU2CU5D_t4054002953* L_31 = L_30;
		NullCheck(L_31);
		(L_31)->SetAt(5, 4, _stringLiteral2899455758);
		__this->set_rightText_2(L_31);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Serving::Start()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1768030745_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m207825188_MethodInfo_var;
extern const uint32_t Serving_Start_m4196045311_MetadataUsageId;
extern "C"  void Serving_Start_m4196045311 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_Start_m4196045311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Sprite_t3199167241 * V_0 = NULL;
	Scene_t1080795294  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_OffBgm_m2683352902(L_0, /*hidden argument*/NULL);
		Scene_t1080795294  L_1 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = Scene_get_buildIndex_m3533090789((&V_1), /*hidden argument*/NULL);
		__this->set_nowNum_20(((int32_t)((int32_t)L_2-(int32_t)1)));
		PopupManagement_t282433007 * L_3 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		__this->set_popup_3(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_4 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_cream_11();
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		GameObject_t3674682005 * L_6 = __this->get_CreamImg1_9();
		NullCheck(L_6);
		GameObject_SetActive_m3538205401(L_6, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_7 = __this->get_CreamImg2_10();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_8 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringU5BU5D_t4054002952* L_9 = L_8->get_coffeeImgList_3();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_10 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_menu_10();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		Sprite_t3199167241 * L_14 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_13, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_0 = L_14;
		Image_t538875265 * L_15 = __this->get_CoffeeImage1_7();
		Sprite_t3199167241 * L_16 = V_0;
		NullCheck(L_15);
		Image_set_sprite_m572551402(L_15, L_16, /*hidden argument*/NULL);
		Image_t538875265 * L_17 = __this->get_CoffeeImage2_8();
		Sprite_t3199167241 * L_18 = V_0;
		NullCheck(L_17);
		Image_set_sprite_m572551402(L_17, L_18, /*hidden argument*/NULL);
		Image_t538875265 * L_19 = __this->get_CoffeeImage1_7();
		NullCheck(L_19);
		Behaviour_set_enabled_m2046806933(L_19, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_20 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		NullCheck(L_20);
		ResultData_t1421128071 * L_21 = NetworkMng_ReadSaveData_m43045332(L_20, /*hidden argument*/NULL);
		Serving_OnAllResult_m3087523573(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Serving::onStart()
extern Il2CppClass* U3ConStartU3Ec__Iterator28_t3665343203_il2cpp_TypeInfo_var;
extern const uint32_t Serving_onStart_m3673233544_MetadataUsageId;
extern "C"  Il2CppObject * Serving_onStart_m3673233544 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_onStart_m3673233544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3ConStartU3Ec__Iterator28_t3665343203 * V_0 = NULL;
	{
		U3ConStartU3Ec__Iterator28_t3665343203 * L_0 = (U3ConStartU3Ec__Iterator28_t3665343203 *)il2cpp_codegen_object_new(U3ConStartU3Ec__Iterator28_t3665343203_il2cpp_TypeInfo_var);
		U3ConStartU3Ec__Iterator28__ctor_m2034858264(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3ConStartU3Ec__Iterator28_t3665343203 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3ConStartU3Ec__Iterator28_t3665343203 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 Serving::RightText(System.Single)
extern "C"  int32_t Serving_RightText_m270046167 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method)
{
	{
		float L_0 = ___cs0;
		if ((!(((float)L_0) < ((float)(60.0f)))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float L_1 = ___cs0;
		if ((!(((float)L_1) < ((float)(70.0f)))))
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		float L_2 = ___cs0;
		if ((!(((float)L_2) < ((float)(80.0f)))))
		{
			goto IL_0027;
		}
	}
	{
		return 2;
	}

IL_0027:
	{
		float L_3 = ___cs0;
		if ((!(((float)L_3) < ((float)(90.0f)))))
		{
			goto IL_0034;
		}
	}
	{
		return 3;
	}

IL_0034:
	{
		return 4;
	}
}
// System.String Serving::afterText(System.Int32,System.Int32)
extern "C"  String_t* Serving_afterText_m3826407805 (Serving_t3648806892 * __this, int32_t ____randomNum0, int32_t ___num1, const MethodInfo* method)
{
	{
		StringU5BU2CU5D_t4054002953* L_0 = __this->get_rightText_2();
		int32_t L_1 = ____randomNum0;
		int32_t L_2 = ___num1;
		NullCheck(L_0);
		String_t* L_3 = (L_0)->GetAt(((int32_t)((int32_t)L_1-(int32_t)1)), L_2);
		return L_3;
	}
}
// System.Void Serving::OnAllResult(ResultData)
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2921734612_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3801208417_MethodInfo_var;
extern const uint32_t Serving_OnAllResult_m3087523573_MetadataUsageId;
extern "C"  void Serving_OnAllResult_m3087523573 (Serving_t3648806892 * __this, ResultData_t1421128071 * ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_OnAllResult_m3087523573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0092;
	}

IL_0007:
	{
		ResultData_t1421128071 * L_0 = ___info0;
		NullCheck(L_0);
		List_1_t199460084 * L_1 = L_0->get_resultData_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		ResultSaveData_t3126241828 * L_3 = List_1_get_Item_m2921734612(L_1, L_2, /*hidden argument*/List_1_get_Item_m2921734612_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_prctc_grade_1();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_5 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_grade_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_008e;
		}
	}
	{
		ResultData_t1421128071 * L_7 = ___info0;
		NullCheck(L_7);
		List_1_t199460084 * L_8 = L_7->get_resultData_0();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		ResultSaveData_t3126241828 * L_10 = List_1_get_Item_m2921734612(L_8, L_9, /*hidden argument*/List_1_get_Item_m2921734612_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_prctc_guest_th_3();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_12 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_guestTh_7();
		if ((!(((uint32_t)L_11) == ((uint32_t)L_13))))
		{
			goto IL_008e;
		}
	}
	{
		ResultData_t1421128071 * L_14 = ___info0;
		NullCheck(L_14);
		List_1_t199460084 * L_15 = L_14->get_resultData_0();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		ResultSaveData_t3126241828 * L_17 = List_1_get_Item_m2921734612(L_15, L_16, /*hidden argument*/List_1_get_Item_m2921734612_MethodInfo_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_prctc_game_id_2();
		if ((((int32_t)L_18) > ((int32_t)6)))
		{
			goto IL_008e;
		}
	}
	{
		float L_19 = __this->get_sum_11();
		ResultData_t1421128071 * L_20 = ___info0;
		NullCheck(L_20);
		List_1_t199460084 * L_21 = L_20->get_resultData_0();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		ResultSaveData_t3126241828 * L_23 = List_1_get_Item_m2921734612(L_21, L_22, /*hidden argument*/List_1_get_Item_m2921734612_MethodInfo_var);
		NullCheck(L_23);
		float L_24 = L_23->get_prctc_score_4();
		__this->set_sum_11(((float)((float)L_19+(float)L_24)));
		float L_25 = __this->get_cnt_12();
		__this->set_cnt_12(((float)((float)L_25+(float)(1.0f))));
	}

IL_008e:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_0092:
	{
		int32_t L_27 = V_0;
		ResultData_t1421128071 * L_28 = ___info0;
		NullCheck(L_28);
		List_1_t199460084 * L_29 = L_28->get_resultData_0();
		NullCheck(L_29);
		int32_t L_30 = List_1_get_Count_m3801208417(L_29, /*hidden argument*/List_1_get_Count_m3801208417_MethodInfo_var);
		if ((((int32_t)L_27) < ((int32_t)L_30)))
		{
			goto IL_0007;
		}
	}
	{
		float L_31 = __this->get_sum_11();
		float L_32 = __this->get_cnt_12();
		__this->set_sum_11(((float)((float)L_31/(float)L_32)));
		Il2CppObject * L_33 = Serving_ShowData_m444028324(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Serving::OnAllResultFail()
extern "C"  void Serving_OnAllResultFail_m3844952474 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator Serving::ShowData()
extern Il2CppClass* U3CShowDataU3Ec__Iterator29_t2136060378_il2cpp_TypeInfo_var;
extern const uint32_t Serving_ShowData_m444028324_MetadataUsageId;
extern "C"  Il2CppObject * Serving_ShowData_m444028324 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_ShowData_m444028324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowDataU3Ec__Iterator29_t2136060378 * V_0 = NULL;
	{
		U3CShowDataU3Ec__Iterator29_t2136060378 * L_0 = (U3CShowDataU3Ec__Iterator29_t2136060378 *)il2cpp_codegen_object_new(U3CShowDataU3Ec__Iterator29_t2136060378_il2cpp_TypeInfo_var);
		U3CShowDataU3Ec__Iterator29__ctor_m4016880337(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowDataU3Ec__Iterator29_t2136060378 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CShowDataU3Ec__Iterator29_t2136060378 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator Serving::ShowmetheCoffeSizeUpCo()
extern Il2CppClass* U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680_il2cpp_TypeInfo_var;
extern const uint32_t Serving_ShowmetheCoffeSizeUpCo_m2400252450_MetadataUsageId;
extern "C"  Il2CppObject * Serving_ShowmetheCoffeSizeUpCo_m2400252450 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_ShowmetheCoffeSizeUpCo_m2400252450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * V_0 = NULL;
	{
		U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * L_0 = (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 *)il2cpp_codegen_object_new(U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680_il2cpp_TypeInfo_var);
		U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A__ctor_m1583786699(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * L_2 = V_0;
		return L_2;
	}
}
// System.String Serving::stringSwitch(System.Single)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3291433554;
extern Il2CppCodeGenString* _stringLiteral268395270;
extern Il2CppCodeGenString* _stringLiteral602066437;
extern Il2CppCodeGenString* _stringLiteral828100009;
extern Il2CppCodeGenString* _stringLiteral926411057;
extern Il2CppCodeGenString* _stringLiteral4232486887;
extern Il2CppCodeGenString* _stringLiteral1622298065;
extern Il2CppCodeGenString* _stringLiteral788269977;
extern Il2CppCodeGenString* _stringLiteral1123092200;
extern const uint32_t Serving_stringSwitch_m3488818404_MetadataUsageId;
extern "C"  String_t* Serving_stringSwitch_m3488818404 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_stringSwitch_m3488818404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___cs0;
		if ((!(((float)L_0) < ((float)(60.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return _stringLiteral3291433554;
	}

IL_0011:
	{
		float L_1 = ___cs0;
		if ((!(((float)L_1) < ((float)(67.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return _stringLiteral268395270;
	}

IL_0022:
	{
		float L_2 = ___cs0;
		if ((!(((float)L_2) < ((float)(70.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		return _stringLiteral602066437;
	}

IL_0033:
	{
		float L_3 = ___cs0;
		if ((!(((float)L_3) < ((float)(77.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		return _stringLiteral828100009;
	}

IL_0044:
	{
		float L_4 = ___cs0;
		if ((!(((float)L_4) < ((float)(80.0f)))))
		{
			goto IL_0055;
		}
	}
	{
		return _stringLiteral926411057;
	}

IL_0055:
	{
		float L_5 = ___cs0;
		if ((!(((float)L_5) < ((float)(87.0f)))))
		{
			goto IL_0066;
		}
	}
	{
		return _stringLiteral4232486887;
	}

IL_0066:
	{
		float L_6 = ___cs0;
		if ((!(((float)L_6) < ((float)(90.0f)))))
		{
			goto IL_0077;
		}
	}
	{
		return _stringLiteral1622298065;
	}

IL_0077:
	{
		float L_7 = ___cs0;
		if ((!(((float)L_7) < ((float)(97.0f)))))
		{
			goto IL_0088;
		}
	}
	{
		return _stringLiteral788269977;
	}

IL_0088:
	{
		float L_8 = ___cs0;
		if ((!(((float)L_8) <= ((float)(100.0f)))))
		{
			goto IL_0099;
		}
	}
	{
		return _stringLiteral1123092200;
	}

IL_0099:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_9;
	}
}
// System.String Serving::ImageSwitch(System.Single)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral51;
extern const uint32_t Serving_ImageSwitch_m88420956_MetadataUsageId;
extern "C"  String_t* Serving_ImageSwitch_m88420956 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_ImageSwitch_m88420956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___cs0;
		if ((!(((float)L_0) < ((float)(60.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return _stringLiteral50;
	}

IL_0011:
	{
		float L_1 = ___cs0;
		if ((!(((float)L_1) < ((float)(87.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_2;
	}

IL_0022:
	{
		float L_3 = ___cs0;
		if ((!(((float)L_3) <= ((float)(100.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		return _stringLiteral51;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
// System.Void Serving::csSwitch(System.Single)
extern "C"  void Serving_csSwitch_m1901630274 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method)
{
	{
		float L_0 = ___cs0;
		if ((!(((float)L_0) < ((float)(60.0f)))))
		{
			goto IL_0018;
		}
	}
	{
		__this->set_reverse_21(((int32_t)12));
		goto IL_012a;
	}

IL_0018:
	{
		float L_1 = ___cs0;
		if ((!(((float)L_1) < ((float)(63.0f)))))
		{
			goto IL_0030;
		}
	}
	{
		__this->set_reverse_21(((int32_t)11));
		goto IL_012a;
	}

IL_0030:
	{
		float L_2 = ___cs0;
		if ((!(((float)L_2) < ((float)(67.0f)))))
		{
			goto IL_0048;
		}
	}
	{
		__this->set_reverse_21(((int32_t)9));
		goto IL_012a;
	}

IL_0048:
	{
		float L_3 = ___cs0;
		if ((!(((float)L_3) < ((float)(70.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		__this->set_reverse_21(((int32_t)10));
		goto IL_012a;
	}

IL_0060:
	{
		float L_4 = ___cs0;
		if ((!(((float)L_4) < ((float)(73.0f)))))
		{
			goto IL_0077;
		}
	}
	{
		__this->set_reverse_21(8);
		goto IL_012a;
	}

IL_0077:
	{
		float L_5 = ___cs0;
		if ((!(((float)L_5) < ((float)(77.0f)))))
		{
			goto IL_008e;
		}
	}
	{
		__this->set_reverse_21(6);
		goto IL_012a;
	}

IL_008e:
	{
		float L_6 = ___cs0;
		if ((!(((float)L_6) < ((float)(80.0f)))))
		{
			goto IL_00a5;
		}
	}
	{
		__this->set_reverse_21(7);
		goto IL_012a;
	}

IL_00a5:
	{
		float L_7 = ___cs0;
		if ((!(((float)L_7) < ((float)(83.0f)))))
		{
			goto IL_00bc;
		}
	}
	{
		__this->set_reverse_21(5);
		goto IL_012a;
	}

IL_00bc:
	{
		float L_8 = ___cs0;
		if ((!(((float)L_8) < ((float)(87.0f)))))
		{
			goto IL_00d3;
		}
	}
	{
		__this->set_reverse_21(3);
		goto IL_012a;
	}

IL_00d3:
	{
		float L_9 = ___cs0;
		if ((!(((float)L_9) < ((float)(90.0f)))))
		{
			goto IL_00ea;
		}
	}
	{
		__this->set_reverse_21(4);
		goto IL_012a;
	}

IL_00ea:
	{
		float L_10 = ___cs0;
		if ((!(((float)L_10) < ((float)(94.0f)))))
		{
			goto IL_0101;
		}
	}
	{
		__this->set_reverse_21(2);
		goto IL_012a;
	}

IL_0101:
	{
		float L_11 = ___cs0;
		if ((!(((float)L_11) < ((float)(97.0f)))))
		{
			goto IL_0118;
		}
	}
	{
		__this->set_reverse_21(0);
		goto IL_012a;
	}

IL_0118:
	{
		float L_12 = ___cs0;
		if ((!(((float)L_12) <= ((float)(100.0f)))))
		{
			goto IL_012a;
		}
	}
	{
		__this->set_reverse_21(1);
	}

IL_012a:
	{
		return;
	}
}
// System.Void Serving::juSwitch(System.Single)
extern "C"  void Serving_juSwitch_m2243901767 (Serving_t3648806892 * __this, float ___ju0, const MethodInfo* method)
{
	{
		float L_0 = ___ju0;
		if ((!(((float)L_0) <= ((float)(5.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		__this->set_reverse_21(4);
		goto IL_006e;
	}

IL_0017:
	{
		float L_1 = ___ju0;
		if ((!(((float)L_1) <= ((float)(12.0f)))))
		{
			goto IL_002e;
		}
	}
	{
		__this->set_reverse_21(3);
		goto IL_006e;
	}

IL_002e:
	{
		float L_2 = ___ju0;
		if ((!(((float)L_2) <= ((float)(25.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		__this->set_reverse_21(2);
		goto IL_006e;
	}

IL_0045:
	{
		float L_3 = ___ju0;
		if ((!(((float)L_3) <= ((float)(40.0f)))))
		{
			goto IL_005c;
		}
	}
	{
		__this->set_reverse_21(1);
		goto IL_006e;
	}

IL_005c:
	{
		float L_4 = ___ju0;
		if ((!(((float)L_4) <= ((float)(60.0f)))))
		{
			goto IL_006e;
		}
	}
	{
		__this->set_reverse_21(0);
	}

IL_006e:
	{
		return;
	}
}
// System.Void Serving::OnDestroy()
extern "C"  void Serving_OnDestroy_m2549823096 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Serving::OkClick()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t1768030745_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2764559344_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m207825188_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3657169165_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t Serving_OkClick_m2819261737_MetadataUsageId;
extern "C"  void Serving_OkClick_m2819261737 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_OkClick_m2819261737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_1 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		NullCheck(L_1);
		NetworkMng_RequestSaveGameDataInLocal_m2383761503(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2764559344_il2cpp_TypeInfo_var);
		GameStepManager_t2511743951 * L_2 = Singleton_1_get_GetInstance_m3657169165(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3657169165_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_3 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_gameId_6();
		NullCheck(L_2);
		String_t* L_5 = GameStepManager_GetNextScene_m3994849017(L_2, L_4, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Serving::okNet()
extern Il2CppClass* U3CokNetU3Ec__Iterator2B_t599730315_il2cpp_TypeInfo_var;
extern const uint32_t Serving_okNet_m3019028070_MetadataUsageId;
extern "C"  Il2CppObject * Serving_okNet_m3019028070 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_okNet_m3019028070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CokNetU3Ec__Iterator2B_t599730315 * V_0 = NULL;
	{
		U3CokNetU3Ec__Iterator2B_t599730315 * L_0 = (U3CokNetU3Ec__Iterator2B_t599730315 *)il2cpp_codegen_object_new(U3CokNetU3Ec__Iterator2B_t599730315_il2cpp_TypeInfo_var);
		U3CokNetU3Ec__Iterator2B__ctor_m3561948784(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CokNetU3Ec__Iterator2B_t599730315 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CokNetU3Ec__Iterator2B_t599730315 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Serving::OnSave()
extern Il2CppClass* Singleton_1_t2764559344_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3657169165_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t Serving_OnSave_m1400634465_MetadataUsageId;
extern "C"  void Serving_OnSave_m1400634465 (Serving_t3648806892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serving_OnSave_m1400634465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2764559344_il2cpp_TypeInfo_var);
		GameStepManager_t2511743951 * L_0 = Singleton_1_get_GetInstance_m3657169165(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3657169165_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_1 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_gameId_6();
		NullCheck(L_0);
		String_t* L_3 = GameStepManager_GetNextScene_m3994849017(L_0, L_2, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_okClick_22((bool)0);
		return;
	}
}
// System.Void Serving/<okNet>c__Iterator2B::.ctor()
extern "C"  void U3CokNetU3Ec__Iterator2B__ctor_m3561948784 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Serving/<okNet>c__Iterator2B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator2B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2761915372 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Serving/<okNet>c__Iterator2B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CokNetU3Ec__Iterator2B_System_Collections_IEnumerator_get_Current_m993974144 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Serving/<okNet>c__Iterator2B::MoveNext()
extern Il2CppClass* Singleton_1_t1768030745_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2764559344_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m207825188_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3657169165_MethodInfo_var;
extern const uint32_t U3CokNetU3Ec__Iterator2B_MoveNext_m3027829612_MetadataUsageId;
extern "C"  bool U3CokNetU3Ec__Iterator2B_MoveNext_m3027829612 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CokNetU3Ec__Iterator2B_MoveNext_m3027829612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00a5;
		}
	}
	{
		goto IL_00b1;
	}

IL_0021:
	{
		Serving_t3648806892 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_okClick_22();
		if (L_3)
		{
			goto IL_008d;
		}
	}
	{
		Serving_t3648806892 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		L_4->set_okClick_22((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1768030745_il2cpp_TypeInfo_var);
		NetworkMng_t1515215352 * L_5 = Singleton_1_get_GetInstance_m207825188(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m207825188_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_6 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_gameId_6();
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_trialTh_8();
		MainUserInfo_t2526075922 * L_10 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_grade_5();
		MainUserInfo_t2526075922 * L_12 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_guestTh_7();
		NullCheck(L_5);
		NetworkMng_RequestSaveGameData_m2757540381(L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2764559344_il2cpp_TypeInfo_var);
		GameStepManager_t2511743951 * L_14 = Singleton_1_get_GetInstance_m3657169165(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3657169165_MethodInfo_var);
		MainUserInfo_t2526075922 * L_15 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_15);
		int32_t L_16 = L_15->get_gameId_6();
		NullCheck(L_14);
		String_t* L_17 = GameStepManager_GetNextScene_m3994849017(L_14, L_16, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_008d:
	{
		goto IL_00b1;
	}

IL_0092:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_00b3;
	}

IL_00a5:
	{
		goto IL_0021;
	}
	// Dead block : IL_00aa: ldarg.0

IL_00b1:
	{
		return (bool)0;
	}

IL_00b3:
	{
		return (bool)1;
	}
}
// System.Void Serving/<okNet>c__Iterator2B::Dispose()
extern "C"  void U3CokNetU3Ec__Iterator2B_Dispose_m4137096877 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Serving/<okNet>c__Iterator2B::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CokNetU3Ec__Iterator2B_Reset_m1208381725_MetadataUsageId;
extern "C"  void U3CokNetU3Ec__Iterator2B_Reset_m1208381725 (U3CokNetU3Ec__Iterator2B_t599730315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CokNetU3Ec__Iterator2B_Reset_m1208381725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Serving/<onStart>c__Iterator28::.ctor()
extern "C"  void U3ConStartU3Ec__Iterator28__ctor_m2034858264 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Serving/<onStart>c__Iterator28::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ConStartU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2816612612 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Serving/<onStart>c__Iterator28::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ConStartU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2781783192 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean Serving/<onStart>c__Iterator28::MoveNext()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern Il2CppClass* GradeDataInfo_t1480749135_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* Resources_LoadAll_TisSprite_t3199167241_m634693541_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3648806892;
extern Il2CppCodeGenString* _stringLiteral109264530;
extern const uint32_t U3ConStartU3Ec__Iterator28_MoveNext_m3212694212_MetadataUsageId;
extern "C"  bool U3ConStartU3Ec__Iterator28_MoveNext_m3212694212 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3ConStartU3Ec__Iterator28_MoveNext_m3212694212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0133;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3648806892, /*hidden argument*/NULL);
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (4.0f), /*hidden argument*/NULL);
		__this->set_U24current_3(L_2);
		__this->set_U24PC_2(1);
		goto IL_0135;
	}

IL_0047:
	{
		Serving_t3648806892 * L_3 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = L_3->get_popServing_15();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GradeDataInfo_t1480749135_il2cpp_TypeInfo_var);
		GradeDataInfo_t1480749135 * L_5 = GradeDataInfo_GetInstance_m2964107273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GradeDataU5BU5D_t1462969052* L_6 = L_5->get_grade_data_0();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_7 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_grade_5();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_8-(int32_t)1)));
		int32_t L_9 = ((int32_t)((int32_t)L_8-(int32_t)1));
		GradeData_t483491841 * L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		String_t* L_11 = L_10->get_teacher_name_2();
		Sprite_t3199167241 * L_12 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_11, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		__this->set_U3CsourU3E__0_0(L_12);
		Serving_t3648806892 * L_13 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_13);
		Image_t538875265 * L_14 = L_13->get__teacher_16();
		Sprite_t3199167241 * L_15 = __this->get_U3CsourU3E__0_0();
		NullCheck(L_14);
		Image_set_sprite_m572551402(L_14, L_15, /*hidden argument*/NULL);
		SpriteU5BU5D_t2761310900* L_16 = Resources_LoadAll_TisSprite_t3199167241_m634693541(NULL /*static, unused*/, _stringLiteral109264530, /*hidden argument*/Resources_LoadAll_TisSprite_t3199167241_m634693541_MethodInfo_var);
		__this->set_U3CsourceU3E__1_1(L_16);
		Serving_t3648806892 * L_17 = __this->get_U3CU3Ef__this_4();
		Serving_t3648806892 * L_18 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_18);
		float L_19 = L_18->get_sum_11();
		NullCheck(L_17);
		Serving_csSwitch_m1901630274(L_17, L_19, /*hidden argument*/NULL);
		Serving_t3648806892 * L_20 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_20);
		Image_t538875265 * L_21 = L_20->get__scoreAlphabet_17();
		SpriteU5BU5D_t2761310900* L_22 = __this->get_U3CsourceU3E__1_1();
		Serving_t3648806892 * L_23 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_reverse_21();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_24);
		int32_t L_25 = L_24;
		Sprite_t3199167241 * L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_21);
		Image_set_sprite_m572551402(L_21, L_26, /*hidden argument*/NULL);
		Serving_t3648806892 * L_27 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_27);
		Text_t9039225 * L_28 = L_27->get__greatText_18();
		Serving_t3648806892 * L_29 = __this->get_U3CU3Ef__this_4();
		Serving_t3648806892 * L_30 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_30);
		float L_31 = L_30->get_sum_11();
		NullCheck(L_29);
		String_t* L_32 = Serving_stringSwitch_m3488818404(L_29, L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_28, L_32);
		Serving_t3648806892 * L_33 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_33);
		Text_t9039225 * L_34 = L_33->get_coffeeText_19();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_35 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		MenuList_t3755595453 * L_36 = L_35->get_recipeList_5();
		MainUserInfo_t2526075922 * L_37 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_37);
		int32_t L_38 = L_37->get_menu_10();
		NullCheck(L_36);
		String_t* L_39 = MenuList_GetMenuInPopup_m1960107628(L_36, L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, L_39);
		__this->set_U24PC_2((-1));
	}

IL_0133:
	{
		return (bool)0;
	}

IL_0135:
	{
		return (bool)1;
	}
	// Dead block : IL_0137: ldloc.1
}
// System.Void Serving/<onStart>c__Iterator28::Dispose()
extern "C"  void U3ConStartU3Ec__Iterator28_Dispose_m1186955093 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Serving/<onStart>c__Iterator28::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3ConStartU3Ec__Iterator28_Reset_m3976258501_MetadataUsageId;
extern "C"  void U3ConStartU3Ec__Iterator28_Reset_m3976258501 (U3ConStartU3Ec__Iterator28_t3665343203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3ConStartU3Ec__Iterator28_Reset_m3976258501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Serving/<ShowData>c__Iterator29::.ctor()
extern "C"  void U3CShowDataU3Ec__Iterator29__ctor_m4016880337 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Serving/<ShowData>c__Iterator29::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowDataU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m106551585 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Serving/<ShowData>c__Iterator29::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowDataU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m2877046453 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean Serving/<ShowData>c__Iterator29::MoveNext()
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral157975376;
extern Il2CppCodeGenString* _stringLiteral1196198895;
extern const uint32_t U3CShowDataU3Ec__Iterator29_MoveNext_m1926243715_MetadataUsageId;
extern "C"  bool U3CShowDataU3Ec__Iterator29_MoveNext_m1926243715 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowDataU3Ec__Iterator29_MoveNext_m1926243715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_01b1;
		}
	}
	{
		goto IL_01eb;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_2 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_guestId_9();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		Serving_t3648806892 * L_6 = __this->get_U3CU3Ef__this_5();
		Serving_t3648806892 * L_7 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		float L_8 = L_7->get_sum_11();
		NullCheck(L_6);
		String_t* L_9 = Serving_ImageSwitch_m88420956(L_6, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral157975376, L_5, L_9, /*hidden argument*/NULL);
		Sprite_t3199167241 * L_11 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_10, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		__this->set_U3CsourceU3E__0_0(L_11);
		Serving_t3648806892 * L_12 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_12);
		Image_t538875265 * L_13 = L_12->get_desk_6();
		Sprite_t3199167241 * L_14 = __this->get_U3CsourceU3E__0_0();
		NullCheck(L_13);
		Image_set_sprite_m572551402(L_13, L_14, /*hidden argument*/NULL);
		Serving_t3648806892 * L_15 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_15);
		Image_t538875265 * L_16 = L_15->get_desk_6();
		NullCheck(L_16);
		Behaviour_set_enabled_m2046806933(L_16, (bool)1, /*hidden argument*/NULL);
		Serving_t3648806892 * L_17 = __this->get_U3CU3Ef__this_5();
		Serving_t3648806892 * L_18 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_18);
		float L_19 = L_18->get_sum_11();
		NullCheck(L_17);
		int32_t L_20 = Serving_RightText_m270046167(L_17, L_19, /*hidden argument*/NULL);
		__this->set_U3CtempNumU3E__1_1(L_20);
		Serving_t3648806892 * L_21 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_21);
		float L_22 = L_21->get_sum_11();
		float L_23 = L_22;
		Il2CppObject * L_24 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_23);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1196198895, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Serving_t3648806892 * L_26 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_26);
		Text_t9039225 * L_27 = L_26->get_MayItakeYourOrder_4();
		Serving_t3648806892 * L_28 = __this->get_U3CU3Ef__this_5();
		MainUserInfo_t2526075922 * L_29 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_29);
		int32_t L_30 = L_29->get_guestId_9();
		int32_t L_31 = __this->get_U3CtempNumU3E__1_1();
		NullCheck(L_28);
		String_t* L_32 = Serving_afterText_m3826407805(L_28, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_27);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_27, L_32);
	}

IL_00e8:
	{
		Serving_t3648806892 * L_33 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = L_33->get_imgTrans_13();
		Serving_t3648806892 * L_35 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = L_35->get_imgTrans_13();
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_position_m2211398607(L_36, /*hidden argument*/NULL);
		Serving_t3648806892 * L_38 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_38);
		Transform_t1659122786 * L_39 = L_38->get_pos2_14();
		NullCheck(L_39);
		Vector3_t4282066566  L_40 = Transform_get_position_m2211398607(L_39, /*hidden argument*/NULL);
		float L_41 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_42 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_37, L_40, ((float)((float)(5.0f)*(float)L_41)), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_m3111394108(L_34, L_42, /*hidden argument*/NULL);
		float L_43 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtempU3E__2_2(((float)((float)(1.5f)*(float)L_43)));
		Serving_t3648806892 * L_44 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_44);
		Transform_t1659122786 * L_45 = L_44->get_imgTrans_13();
		Transform_t1659122786 * L_46 = L_45;
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_localScale_m3886572677(L_46, /*hidden argument*/NULL);
		float L_48 = __this->get_U3CtempU3E__2_2();
		float L_49 = __this->get_U3CtempU3E__2_2();
		Vector3_t4282066566  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m2926210380(&L_50, L_48, L_49, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_51 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_47, L_50, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_localScale_m310756934(L_46, L_51, /*hidden argument*/NULL);
		Serving_t3648806892 * L_52 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = L_52->get_imgTrans_13();
		NullCheck(L_53);
		Vector3_t4282066566  L_54 = Transform_get_position_m2211398607(L_53, /*hidden argument*/NULL);
		Serving_t3648806892 * L_55 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_55);
		Transform_t1659122786 * L_56 = L_55->get_pos2_14();
		NullCheck(L_56);
		Vector3_t4282066566  L_57 = Transform_get_position_m2211398607(L_56, /*hidden argument*/NULL);
		float L_58 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_54, L_57, /*hidden argument*/NULL);
		if ((!(((float)L_58) < ((float)(0.1f)))))
		{
			goto IL_019e;
		}
	}
	{
		goto IL_01b6;
	}

IL_019e:
	{
		__this->set_U24current_4(NULL);
		__this->set_U24PC_3(1);
		goto IL_01ed;
	}

IL_01b1:
	{
		goto IL_00e8;
	}

IL_01b6:
	{
		Serving_t3648806892 * L_59 = __this->get_U3CU3Ef__this_5();
		Serving_t3648806892 * L_60 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_60);
		Il2CppObject * L_61 = Serving_ShowmetheCoffeSizeUpCo_m2400252450(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		MonoBehaviour_StartCoroutine_m2135303124(L_59, L_61, /*hidden argument*/NULL);
		Serving_t3648806892 * L_62 = __this->get_U3CU3Ef__this_5();
		Serving_t3648806892 * L_63 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_63);
		Il2CppObject * L_64 = Serving_onStart_m3673233544(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		MonoBehaviour_StartCoroutine_m2135303124(L_62, L_64, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_01eb:
	{
		return (bool)0;
	}

IL_01ed:
	{
		return (bool)1;
	}
	// Dead block : IL_01ef: ldloc.1
}
// System.Void Serving/<ShowData>c__Iterator29::Dispose()
extern "C"  void U3CShowDataU3Ec__Iterator29_Dispose_m3239655118 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Serving/<ShowData>c__Iterator29::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowDataU3Ec__Iterator29_Reset_m1663313278_MetadataUsageId;
extern "C"  void U3CShowDataU3Ec__Iterator29_Reset_m1663313278 (U3CShowDataU3Ec__Iterator29_t2136060378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowDataU3Ec__Iterator29_Reset_m1663313278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::.ctor()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A__ctor_m1583786699 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2332891687 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_System_Collections_IEnumerator_get_Current_m3488500667 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::MoveNext()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63463647;
extern const uint32_t U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_MoveNext_m3994035785_MetadataUsageId;
extern "C"  bool U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_MoveNext_m3994035785 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_MoveNext_m3994035785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_0021:
	{
		Serving_t3648806892 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_ShowmetheCoffee_5();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = GameObject_get_gameObject_m1966529385(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2926210380(&L_6, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m310756934(L_5, L_6, /*hidden argument*/NULL);
		Serving_t3648806892 * L_7 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = L_7->get_ShowmetheCoffee_5();
		NullCheck(L_8);
		GameObject_t3674682005 * L_9 = GameObject_get_gameObject_m1966529385(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m3538205401(L_9, (bool)1, /*hidden argument*/NULL);
		__this->set_U3ClimitSizeU3E__0_0((0.0f));
	}

IL_0070:
	{
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtempU3E__1_1(((float)((float)(5.0f)*(float)L_10)));
		float L_11 = __this->get_U3ClimitSizeU3E__0_0();
		float L_12 = __this->get_U3CtempU3E__1_1();
		__this->set_U3ClimitSizeU3E__0_0(((float)((float)L_11+(float)L_12)));
		Serving_t3648806892 * L_13 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = L_13->get_ShowmetheCoffee_5();
		NullCheck(L_14);
		GameObject_t3674682005 * L_15 = GameObject_get_gameObject_m1966529385(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		Transform_t1659122786 * L_17 = L_16;
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_localScale_m3886572677(L_17, /*hidden argument*/NULL);
		float L_19 = __this->get_U3CtempU3E__1_1();
		float L_20 = __this->get_U3CtempU3E__1_1();
		Vector3_t4282066566  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2926210380(&L_21, L_19, L_20, (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_22 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localScale_m310756934(L_17, L_22, /*hidden argument*/NULL);
		float L_23 = __this->get_U3ClimitSizeU3E__0_0();
		if ((!(((float)L_23) > ((float)(1.5f)))))
		{
			goto IL_00ee;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral63463647, /*hidden argument*/NULL);
		goto IL_0106;
	}

IL_00ee:
	{
		__this->set_U24current_3(NULL);
		__this->set_U24PC_2(1);
		goto IL_010f;
	}

IL_0101:
	{
		goto IL_0070;
	}

IL_0106:
	{
		__this->set_U24PC_2((-1));
	}

IL_010d:
	{
		return (bool)0;
	}

IL_010f:
	{
		return (bool)1;
	}
	// Dead block : IL_0111: ldloc.1
}
// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::Dispose()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Dispose_m1498878024 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Reset_m3525186936_MetadataUsageId;
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Reset_m3525186936 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Reset_m3525186936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ShowRecipe::.ctor()
extern "C"  void ShowRecipe__ctor_m3357232624 (ShowRecipe_t2733639435 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowRecipe::Start()
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3209242635;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t ShowRecipe_Start_m2304370416_MetadataUsageId;
extern "C"  void ShowRecipe_Start_m2304370416 (ShowRecipe_t2733639435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipe_Start_m2304370416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t9039225 * L_0 = __this->get_menuTitle_2();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_1 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		MenuList_t3755595453 * L_2 = L_1->get_recipeList_5();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_3 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_menu_10();
		NullCheck(L_2);
		String_t* L_5 = MenuList_GetMenuName_m2677322154(L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		Text_t9039225 * L_6 = __this->get_recipe_3();
		RecipeList_t1641733996 * L_7 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Ingredient_t1787055601 * L_8 = L_7->get_ingredient_4();
		RecipeList_t1641733996 * L_9 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Int32U5BU5DU5BU5D_t1820556512* L_10 = L_9->get_coffeeBeanRecipe_7();
		MainUserInfo_t2526075922 * L_11 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_11);
		int32_t L_12 = L_11->get_menu_10();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_12);
		int32_t L_13 = L_12;
		Int32U5BU5D_t3230847821* L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		int32_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_8);
		String_t* L_17 = Ingredient_GetIngredientByNum_m930423446(L_8, L_16, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_17);
		Text_t9039225 * L_18 = __this->get_goalPoint_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		float L_20 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3209242635, /*hidden argument*/NULL);
		float L_21 = L_20;
		Il2CppObject * L_22 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m2809334143(NULL /*static, unused*/, L_19, L_22, _stringLiteral103, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		return;
	}
}
// System.Void ShowRecipePre::.ctor()
extern Il2CppClass* List_1_t1375417109_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t ShowRecipePre__ctor_m2362317011_MetadataUsageId;
extern "C"  void ShowRecipePre__ctor_m2362317011 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre__ctor_m2362317011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1375417109 * L_0 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_menuList_10(L_0);
		List_1_t1375417109 * L_1 = (List_1_t1375417109 *)il2cpp_codegen_object_new(List_1_t1375417109_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_1, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_unitList_11(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowRecipePre::Start()
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4975193_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2493766289;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral70470760;
extern Il2CppCodeGenString* _stringLiteral392418414;
extern Il2CppCodeGenString* _stringLiteral3209242635;
extern Il2CppCodeGenString* _stringLiteral103;
extern const uint32_t ShowRecipePre_Start_m1309454803_MetadataUsageId;
extern "C"  void ShowRecipePre_Start_m1309454803 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre_Start_m1309454803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	int32_t V_14 = 0;
	Sprite_t3199167241 * V_15 = NULL;
	Scene_t1080795294  V_16;
	memset(&V_16, 0, sizeof(V_16));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_16 = L_0;
		String_t* L_1 = Scene_get_name_m894591657((&V_16), /*hidden argument*/NULL);
		__this->set_nowScene_13(L_1);
		Text_t9039225 * L_2 = __this->get_menuName_2();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_3 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		MenuList_t3755595453 * L_4 = L_3->get_recipeList_5();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_5 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_menu_10();
		NullCheck(L_4);
		String_t* L_7 = MenuList_GetMenuName_m2677322154(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_7);
		MainUserInfo_t2526075922 * L_8 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_cream_11();
		if (L_9)
		{
			goto IL_0053;
		}
	}
	{
		Image_t538875265 * L_10 = __this->get_creamImg_5();
		NullCheck(L_10);
		Behaviour_set_enabled_m2046806933(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_0053:
	{
		String_t* L_11 = __this->get_nowScene_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral2493766289, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0281;
		}
	}
	{
		V_0 = 0;
		goto IL_017f;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_13 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Int32U5BU5DU5BU5D_t1820556512* L_14 = L_13->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_15 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		Int32U5BU5DU5BU5D_t1820556512* L_16 = L_15->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_17 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_menu_10();
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_18);
		int32_t L_19 = L_18;
		Int32U5BU5D_t3230847821* L_20 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		int32_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_23-(int32_t)1)));
		int32_t L_24 = ((int32_t)((int32_t)L_23-(int32_t)1));
		Int32U5BU5D_t3230847821* L_25 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		int32_t L_26 = 0;
		int32_t L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		V_1 = (((float)((float)L_27)));
		RecipeList_t1641733996 * L_28 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Int32U5BU5DU5BU5D_t1820556512* L_29 = L_28->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_30 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		Int32U5BU5DU5BU5D_t1820556512* L_31 = L_30->get_variationRecipe_11();
		MainUserInfo_t2526075922 * L_32 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_32);
		int32_t L_33 = L_32->get_menu_10();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_33);
		int32_t L_34 = L_33;
		Int32U5BU5D_t3230847821* L_35 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		int32_t L_36 = V_0;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)L_38-(int32_t)1)));
		int32_t L_39 = ((int32_t)((int32_t)L_38-(int32_t)1));
		Int32U5BU5D_t3230847821* L_40 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 1);
		int32_t L_41 = 1;
		int32_t L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_2 = (((float)((float)L_42)));
		ObjectU5BU5D_t1108656482* L_43 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		ArrayElementTypeCheck (L_43, L_44);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_44);
		ObjectU5BU5D_t1108656482* L_45 = L_43;
		float L_46 = V_1;
		float L_47 = V_2;
		float L_48 = Random_Range_m3362417303(NULL /*static, unused*/, L_46, ((float)((float)L_47+(float)(1.0f))), /*hidden argument*/NULL);
		double L_49 = floor((((double)((double)L_48))));
		float L_50 = (((float)((float)L_49)));
		Il2CppObject * L_51 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		ArrayElementTypeCheck (L_45, L_51);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_51);
		ObjectU5BU5D_t1108656482* L_52 = L_45;
		String_t* L_53 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 2);
		ArrayElementTypeCheck (L_52, L_53);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_53);
		ObjectU5BU5D_t1108656482* L_54 = L_52;
		RecipeList_t1641733996 * L_55 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		Ingredient_t1787055601 * L_56 = L_55->get_ingredient_4();
		RecipeList_t1641733996 * L_57 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		Int32U5BU5DU5BU5D_t1820556512* L_58 = L_57->get_variationRecipe_11();
		MainUserInfo_t2526075922 * L_59 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_59);
		int32_t L_60 = L_59->get_menu_10();
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_60);
		int32_t L_61 = L_60;
		Int32U5BU5D_t3230847821* L_62 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		int32_t L_63 = V_0;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		int32_t L_64 = L_63;
		int32_t L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_56);
		String_t* L_66 = Ingredient_GetIngredientUnit_m1214740577(L_56, L_65, /*hidden argument*/NULL);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 3);
		ArrayElementTypeCheck (L_54, L_66);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_66);
		String_t* L_67 = String_Concat_m3016520001(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		V_3 = L_67;
		RecipeList_t1641733996 * L_68 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_68);
		Ingredient_t1787055601 * L_69 = L_68->get_ingredient_4();
		RecipeList_t1641733996 * L_70 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_70);
		Int32U5BU5DU5BU5D_t1820556512* L_71 = L_70->get_variationRecipe_11();
		MainUserInfo_t2526075922 * L_72 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_72);
		int32_t L_73 = L_72->get_menu_10();
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_73);
		int32_t L_74 = L_73;
		Int32U5BU5D_t3230847821* L_75 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		int32_t L_76 = V_0;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
		int32_t L_77 = L_76;
		int32_t L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		NullCheck(L_69);
		String_t* L_79 = Ingredient_GetIngredientByNum_m930423446(L_69, L_78, /*hidden argument*/NULL);
		V_4 = L_79;
		List_1_t1375417109 * L_80 = __this->get_menuList_10();
		String_t* L_81 = V_4;
		NullCheck(L_80);
		List_1_Add_m4975193(L_80, L_81, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_82 = __this->get_unitList_11();
		String_t* L_83 = V_3;
		NullCheck(L_82);
		List_1_Add_m4975193(L_82, L_83, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		TextU5BU5D_t3798907012* L_84 = __this->get_recipes_3();
		int32_t L_85 = V_0;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, L_85);
		int32_t L_86 = L_85;
		Text_t9039225 * L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		String_t* L_88 = V_4;
		String_t* L_89 = V_3;
		String_t* L_90 = String_Concat_m1825781833(NULL /*static, unused*/, L_88, _stringLiteral32, L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_87, L_90);
		int32_t L_91 = V_0;
		V_0 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_92 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_93 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_93);
		Int32U5BU5DU5BU5D_t1820556512* L_94 = L_93->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_95 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_95);
		int32_t L_96 = L_95->get_menu_10();
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_96);
		int32_t L_97 = L_96;
		Int32U5BU5D_t3230847821* L_98 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		NullCheck(L_98);
		if ((((int32_t)L_92) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_98)->max_length)))))))
		{
			goto IL_006f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_99 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_99);
		int32_t L_100 = L_99->get_cream_11();
		if (L_100)
		{
			goto IL_027c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_101 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_101);
		Int32U5BU5DU5BU5D_t1820556512* L_102 = L_101->get_randomValueByRecipe_12();
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, ((int32_t)40));
		int32_t L_103 = ((int32_t)40);
		Int32U5BU5D_t3230847821* L_104 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, 0);
		int32_t L_105 = 0;
		int32_t L_106 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		V_5 = (((float)((float)L_106)));
		RecipeList_t1641733996 * L_107 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_107);
		Int32U5BU5DU5BU5D_t1820556512* L_108 = L_107->get_randomValueByRecipe_12();
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, ((int32_t)40));
		int32_t L_109 = ((int32_t)40);
		Int32U5BU5D_t3230847821* L_110 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_109));
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, 1);
		int32_t L_111 = 1;
		int32_t L_112 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		V_6 = (((float)((float)L_112)));
		ObjectU5BU5D_t1108656482* L_113 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_114 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, 0);
		ArrayElementTypeCheck (L_113, L_114);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_114);
		ObjectU5BU5D_t1108656482* L_115 = L_113;
		float L_116 = V_5;
		float L_117 = V_6;
		float L_118 = Random_Range_m3362417303(NULL /*static, unused*/, L_116, ((float)((float)L_117+(float)(1.0f))), /*hidden argument*/NULL);
		double L_119 = floor((((double)((double)L_118))));
		float L_120 = (((float)((float)L_119)));
		Il2CppObject * L_121 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_120);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 1);
		ArrayElementTypeCheck (L_115, L_121);
		(L_115)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_121);
		ObjectU5BU5D_t1108656482* L_122 = L_115;
		String_t* L_123 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, 2);
		ArrayElementTypeCheck (L_122, L_123);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_123);
		ObjectU5BU5D_t1108656482* L_124 = L_122;
		RecipeList_t1641733996 * L_125 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_125);
		Ingredient_t1787055601 * L_126 = L_125->get_ingredient_4();
		NullCheck(L_126);
		String_t* L_127 = Ingredient_GetIngredientUnit_m1214740577(L_126, ((int32_t)41), /*hidden argument*/NULL);
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, 3);
		ArrayElementTypeCheck (L_124, L_127);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_127);
		String_t* L_128 = String_Concat_m3016520001(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		V_7 = L_128;
		RecipeList_t1641733996 * L_129 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_129);
		Ingredient_t1787055601 * L_130 = L_129->get_ingredient_4();
		NullCheck(L_130);
		String_t* L_131 = Ingredient_GetIngredientByNum_m930423446(L_130, ((int32_t)41), /*hidden argument*/NULL);
		V_8 = L_131;
		List_1_t1375417109 * L_132 = __this->get_menuList_10();
		String_t* L_133 = V_8;
		NullCheck(L_132);
		List_1_Add_m4975193(L_132, L_133, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_134 = __this->get_unitList_11();
		String_t* L_135 = V_7;
		NullCheck(L_134);
		List_1_Add_m4975193(L_134, L_135, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		TextU5BU5D_t3798907012* L_136 = __this->get_recipes_3();
		RecipeList_t1641733996 * L_137 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_137);
		Int32U5BU5DU5BU5D_t1820556512* L_138 = L_137->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_139 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_139);
		int32_t L_140 = L_139->get_menu_10();
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, L_140);
		int32_t L_141 = L_140;
		Int32U5BU5D_t3230847821* L_142 = (L_138)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		NullCheck(L_142);
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, (((int32_t)((int32_t)(((Il2CppArray *)L_142)->max_length)))));
		int32_t L_143 = (((int32_t)((int32_t)(((Il2CppArray *)L_142)->max_length))));
		Text_t9039225 * L_144 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		String_t* L_145 = V_8;
		String_t* L_146 = V_7;
		String_t* L_147 = String_Concat_m1825781833(NULL /*static, unused*/, L_145, _stringLiteral32, L_146, /*hidden argument*/NULL);
		NullCheck(L_144);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_144, L_147);
	}

IL_027c:
	{
		goto IL_0485;
	}

IL_0281:
	{
		String_t* L_148 = __this->get_nowScene_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_149 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_148, _stringLiteral70470760, /*hidden argument*/NULL);
		if (!L_149)
		{
			goto IL_03df;
		}
	}
	{
		V_9 = 0;
		goto IL_03bc;
	}

IL_029e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_150 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_150);
		Int32U5BU5DU5BU5D_t1820556512* L_151 = L_150->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_152 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_152);
		Int32U5BU5DU5BU5D_t1820556512* L_153 = L_152->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_154 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_154);
		int32_t L_155 = L_154->get_menu_10();
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, L_155);
		int32_t L_156 = L_155;
		Int32U5BU5D_t3230847821* L_157 = (L_153)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		int32_t L_158 = V_9;
		NullCheck(L_157);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_157, L_158);
		int32_t L_159 = L_158;
		int32_t L_160 = (L_157)->GetAt(static_cast<il2cpp_array_size_t>(L_159));
		NullCheck(L_151);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_151, ((int32_t)((int32_t)L_160-(int32_t)1)));
		int32_t L_161 = ((int32_t)((int32_t)L_160-(int32_t)1));
		Int32U5BU5D_t3230847821* L_162 = (L_151)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		NullCheck(L_162);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_162, 0);
		int32_t L_163 = 0;
		int32_t L_164 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		V_10 = (((float)((float)L_164)));
		RecipeList_t1641733996 * L_165 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_165);
		Int32U5BU5DU5BU5D_t1820556512* L_166 = L_165->get_randomValueByRecipe_12();
		RecipeList_t1641733996 * L_167 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_167);
		Int32U5BU5DU5BU5D_t1820556512* L_168 = L_167->get_iceMixerRecipe_10();
		MainUserInfo_t2526075922 * L_169 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_169);
		int32_t L_170 = L_169->get_menu_10();
		NullCheck(L_168);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_168, L_170);
		int32_t L_171 = L_170;
		Int32U5BU5D_t3230847821* L_172 = (L_168)->GetAt(static_cast<il2cpp_array_size_t>(L_171));
		int32_t L_173 = V_9;
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, L_173);
		int32_t L_174 = L_173;
		int32_t L_175 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		NullCheck(L_166);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_166, ((int32_t)((int32_t)L_175-(int32_t)1)));
		int32_t L_176 = ((int32_t)((int32_t)L_175-(int32_t)1));
		Int32U5BU5D_t3230847821* L_177 = (L_166)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		NullCheck(L_177);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_177, 1);
		int32_t L_178 = 1;
		int32_t L_179 = (L_177)->GetAt(static_cast<il2cpp_array_size_t>(L_178));
		V_11 = (((float)((float)L_179)));
		ObjectU5BU5D_t1108656482* L_180 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_181 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_180);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_180, 0);
		ArrayElementTypeCheck (L_180, L_181);
		(L_180)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_181);
		ObjectU5BU5D_t1108656482* L_182 = L_180;
		float L_183 = V_10;
		float L_184 = V_11;
		float L_185 = Random_Range_m3362417303(NULL /*static, unused*/, L_183, ((float)((float)L_184+(float)(1.0f))), /*hidden argument*/NULL);
		double L_186 = floor((((double)((double)L_185))));
		float L_187 = (((float)((float)L_186)));
		Il2CppObject * L_188 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_187);
		NullCheck(L_182);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_182, 1);
		ArrayElementTypeCheck (L_182, L_188);
		(L_182)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_188);
		ObjectU5BU5D_t1108656482* L_189 = L_182;
		String_t* L_190 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, 2);
		ArrayElementTypeCheck (L_189, L_190);
		(L_189)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_190);
		ObjectU5BU5D_t1108656482* L_191 = L_189;
		RecipeList_t1641733996 * L_192 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_192);
		Ingredient_t1787055601 * L_193 = L_192->get_ingredient_4();
		RecipeList_t1641733996 * L_194 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_194);
		Int32U5BU5DU5BU5D_t1820556512* L_195 = L_194->get_iceMixerRecipe_10();
		MainUserInfo_t2526075922 * L_196 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_196);
		int32_t L_197 = L_196->get_menu_10();
		NullCheck(L_195);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_195, L_197);
		int32_t L_198 = L_197;
		Int32U5BU5D_t3230847821* L_199 = (L_195)->GetAt(static_cast<il2cpp_array_size_t>(L_198));
		int32_t L_200 = V_9;
		NullCheck(L_199);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_199, L_200);
		int32_t L_201 = L_200;
		int32_t L_202 = (L_199)->GetAt(static_cast<il2cpp_array_size_t>(L_201));
		NullCheck(L_193);
		String_t* L_203 = Ingredient_GetIngredientUnit_m1214740577(L_193, L_202, /*hidden argument*/NULL);
		NullCheck(L_191);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_191, 3);
		ArrayElementTypeCheck (L_191, L_203);
		(L_191)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_203);
		String_t* L_204 = String_Concat_m3016520001(NULL /*static, unused*/, L_191, /*hidden argument*/NULL);
		V_12 = L_204;
		RecipeList_t1641733996 * L_205 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_205);
		Ingredient_t1787055601 * L_206 = L_205->get_ingredient_4();
		RecipeList_t1641733996 * L_207 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_207);
		Int32U5BU5DU5BU5D_t1820556512* L_208 = L_207->get_iceMixerRecipe_10();
		MainUserInfo_t2526075922 * L_209 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_209);
		int32_t L_210 = L_209->get_menu_10();
		NullCheck(L_208);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_208, L_210);
		int32_t L_211 = L_210;
		Int32U5BU5D_t3230847821* L_212 = (L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		int32_t L_213 = V_9;
		NullCheck(L_212);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_212, L_213);
		int32_t L_214 = L_213;
		int32_t L_215 = (L_212)->GetAt(static_cast<il2cpp_array_size_t>(L_214));
		NullCheck(L_206);
		String_t* L_216 = Ingredient_GetIngredientByNum_m930423446(L_206, L_215, /*hidden argument*/NULL);
		V_13 = L_216;
		List_1_t1375417109 * L_217 = __this->get_menuList_10();
		String_t* L_218 = V_13;
		NullCheck(L_217);
		List_1_Add_m4975193(L_217, L_218, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		List_1_t1375417109 * L_219 = __this->get_unitList_11();
		String_t* L_220 = V_12;
		NullCheck(L_219);
		List_1_Add_m4975193(L_219, L_220, /*hidden argument*/List_1_Add_m4975193_MethodInfo_var);
		TextU5BU5D_t3798907012* L_221 = __this->get_recipes_3();
		int32_t L_222 = V_9;
		NullCheck(L_221);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_221, L_222);
		int32_t L_223 = L_222;
		Text_t9039225 * L_224 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		String_t* L_225 = V_13;
		String_t* L_226 = V_12;
		String_t* L_227 = String_Concat_m1825781833(NULL /*static, unused*/, L_225, _stringLiteral32, L_226, /*hidden argument*/NULL);
		NullCheck(L_224);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_224, L_227);
		int32_t L_228 = V_9;
		V_9 = ((int32_t)((int32_t)L_228+(int32_t)1));
	}

IL_03bc:
	{
		int32_t L_229 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_230 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_230);
		Int32U5BU5DU5BU5D_t1820556512* L_231 = L_230->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_232 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_232);
		int32_t L_233 = L_232->get_menu_10();
		NullCheck(L_231);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_231, L_233);
		int32_t L_234 = L_233;
		Int32U5BU5D_t3230847821* L_235 = (L_231)->GetAt(static_cast<il2cpp_array_size_t>(L_234));
		NullCheck(L_235);
		if ((((int32_t)L_229) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_235)->max_length)))))))
		{
			goto IL_029e;
		}
	}
	{
		goto IL_0485;
	}

IL_03df:
	{
		String_t* L_236 = __this->get_nowScene_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_237 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_236, _stringLiteral392418414, /*hidden argument*/NULL);
		if (!L_237)
		{
			goto IL_0485;
		}
	}
	{
		V_14 = 0;
		goto IL_0467;
	}

IL_03fc:
	{
		TextU5BU5D_t3798907012* L_238 = __this->get_recipes_3();
		int32_t L_239 = V_14;
		NullCheck(L_238);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_238, L_239);
		int32_t L_240 = L_239;
		Text_t9039225 * L_241 = (L_238)->GetAt(static_cast<il2cpp_array_size_t>(L_240));
		ObjectU5BU5D_t1108656482* L_242 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_243 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_243);
		Ingredient_t1787055601 * L_244 = L_243->get_ingredient_4();
		RecipeList_t1641733996 * L_245 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_245);
		Int32U5BU5DU5BU5D_t1820556512* L_246 = L_245->get_coffeeBeanRecipe_7();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_247 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_247);
		int32_t L_248 = L_247->get_menu_10();
		NullCheck(L_246);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_246, L_248);
		int32_t L_249 = L_248;
		Int32U5BU5D_t3230847821* L_250 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_249));
		int32_t L_251 = V_14;
		NullCheck(L_250);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_250, L_251);
		int32_t L_252 = L_251;
		int32_t L_253 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252));
		NullCheck(L_244);
		String_t* L_254 = Ingredient_GetIngredientByNum_m930423446(L_244, L_253, /*hidden argument*/NULL);
		NullCheck(L_242);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_242, 0);
		ArrayElementTypeCheck (L_242, L_254);
		(L_242)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_254);
		ObjectU5BU5D_t1108656482* L_255 = L_242;
		NullCheck(L_255);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_255, 1);
		ArrayElementTypeCheck (L_255, _stringLiteral32);
		(L_255)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral32);
		ObjectU5BU5D_t1108656482* L_256 = L_255;
		float L_257 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, _stringLiteral3209242635, /*hidden argument*/NULL);
		float L_258 = L_257;
		Il2CppObject * L_259 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_258);
		NullCheck(L_256);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_256, 2);
		ArrayElementTypeCheck (L_256, L_259);
		(L_256)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_259);
		ObjectU5BU5D_t1108656482* L_260 = L_256;
		NullCheck(L_260);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_260, 3);
		ArrayElementTypeCheck (L_260, _stringLiteral103);
		(L_260)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral103);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_261 = String_Concat_m3016520001(NULL /*static, unused*/, L_260, /*hidden argument*/NULL);
		NullCheck(L_241);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_241, L_261);
		int32_t L_262 = V_14;
		V_14 = ((int32_t)((int32_t)L_262+(int32_t)1));
	}

IL_0467:
	{
		int32_t L_263 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_264 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_264);
		Int32U5BU5DU5BU5D_t1820556512* L_265 = L_264->get_coffeeBeanRecipe_7();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_266 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_266);
		int32_t L_267 = L_266->get_menu_10();
		NullCheck(L_265);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_265, L_267);
		int32_t L_268 = L_267;
		Int32U5BU5D_t3230847821* L_269 = (L_265)->GetAt(static_cast<il2cpp_array_size_t>(L_268));
		NullCheck(L_269);
		if ((((int32_t)L_263) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_269)->max_length)))))))
		{
			goto IL_03fc;
		}
	}

IL_0485:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_270 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_270);
		StringU5BU5D_t4054002952* L_271 = L_270->get_coffeeImgList_3();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_272 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_272);
		int32_t L_273 = L_272->get_menu_10();
		NullCheck(L_271);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_271, L_273);
		int32_t L_274 = L_273;
		String_t* L_275 = (L_271)->GetAt(static_cast<il2cpp_array_size_t>(L_274));
		Sprite_t3199167241 * L_276 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_275, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_15 = L_276;
		Image_t538875265 * L_277 = __this->get_coffeeImg_4();
		Sprite_t3199167241 * L_278 = V_15;
		NullCheck(L_277);
		Image_set_sprite_m572551402(L_277, L_278, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowRecipePre::OnEnable()
extern "C"  void ShowRecipePre_OnEnable_m523540947 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ShowRecipePre_enableCoru_m1233405435(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ShowRecipePre::enableCoru()
extern Il2CppClass* U3CenableCoruU3Ec__Iterator2C_t1488953467_il2cpp_TypeInfo_var;
extern const uint32_t ShowRecipePre_enableCoru_m1233405435_MetadataUsageId;
extern "C"  Il2CppObject * ShowRecipePre_enableCoru_m1233405435 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre_enableCoru_m1233405435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CenableCoruU3Ec__Iterator2C_t1488953467 * V_0 = NULL;
	{
		U3CenableCoruU3Ec__Iterator2C_t1488953467 * L_0 = (U3CenableCoruU3Ec__Iterator2C_t1488953467 *)il2cpp_codegen_object_new(U3CenableCoruU3Ec__Iterator2C_t1488953467_il2cpp_TypeInfo_var);
		U3CenableCoruU3Ec__Iterator2C__ctor_m2108770640(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CenableCoruU3Ec__Iterator2C_t1488953467 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CenableCoruU3Ec__Iterator2C_t1488953467 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ShowRecipePre::RightChoice()
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t ShowRecipePre_RightChoice_m3387411854_MetadataUsageId;
extern "C"  void ShowRecipePre_RightChoice_m3387411854 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre_RightChoice_m3387411854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ImageU5BU5D_t4039083868* L_0 = __this->get_checks_8();
		int32_t L_1 = __this->get_currentIngredient_12();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Image_t538875265 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, (bool)1, /*hidden argument*/NULL);
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_4 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_cream_11();
		if (L_5)
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 1;
		goto IL_002d;
	}

IL_002b:
	{
		V_0 = 0;
	}

IL_002d:
	{
		int32_t L_6 = __this->get_currentIngredient_12();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_7 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Int32U5BU5DU5BU5D_t1820556512* L_8 = L_7->get_variationRecipe_11();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_9 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_menu_10();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
		int32_t L_11 = L_10;
		Int32U5BU5D_t3230847821* L_12 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		int32_t L_13 = V_0;
		if ((((int32_t)L_6) >= ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))-(int32_t)1))+(int32_t)L_13)))))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_14 = __this->get_currentIngredient_12();
		__this->set_currentIngredient_12(((int32_t)((int32_t)L_14+(int32_t)1)));
		MonoBehaviour_StopAllCoroutines_m1437893335(__this, /*hidden argument*/NULL);
		Il2CppObject * L_15 = ShowRecipePre_FocusDown_m3625254227(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_15, /*hidden argument*/NULL);
		goto IL_008a;
	}

IL_0079:
	{
		Transform_t1659122786 * L_16 = __this->get_imgTrans_7();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = Component_get_gameObject_m1170635899(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_SetActive_m3538205401(L_17, (bool)0, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void ShowRecipePre::IcingRightChoice()
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t ShowRecipePre_IcingRightChoice_m3568626182_MetadataUsageId;
extern "C"  void ShowRecipePre_IcingRightChoice_m3568626182 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre_IcingRightChoice_m3568626182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ImageU5BU5D_t4039083868* L_0 = __this->get_checks_8();
		int32_t L_1 = __this->get_currentIngredient_12();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		Image_t538875265 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, (bool)1, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_currentIngredient_12();
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_5 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Int32U5BU5DU5BU5D_t1820556512* L_6 = L_5->get_iceMixerRecipe_10();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_7 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_menu_10();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
		int32_t L_9 = L_8;
		Int32U5BU5D_t3230847821* L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		if ((((int32_t)L_4) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)1)))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_11 = __this->get_currentIngredient_12();
		__this->set_currentIngredient_12(((int32_t)((int32_t)L_11+(int32_t)1)));
		MonoBehaviour_StopAllCoroutines_m1437893335(__this, /*hidden argument*/NULL);
		Il2CppObject * L_12 = ShowRecipePre_FocusDown_m3625254227(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_12, /*hidden argument*/NULL);
		goto IL_006e;
	}

IL_005d:
	{
		Transform_t1659122786 * L_13 = __this->get_imgTrans_7();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = Component_get_gameObject_m1170635899(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SetActive_m3538205401(L_14, (bool)0, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Collections.IEnumerator ShowRecipePre::FocusDown()
extern Il2CppClass* U3CFocusDownU3Ec__Iterator2D_t659480186_il2cpp_TypeInfo_var;
extern const uint32_t ShowRecipePre_FocusDown_m3625254227_MetadataUsageId;
extern "C"  Il2CppObject * ShowRecipePre_FocusDown_m3625254227 (ShowRecipePre_t977589144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowRecipePre_FocusDown_m3625254227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFocusDownU3Ec__Iterator2D_t659480186 * V_0 = NULL;
	{
		U3CFocusDownU3Ec__Iterator2D_t659480186 * L_0 = (U3CFocusDownU3Ec__Iterator2D_t659480186 *)il2cpp_codegen_object_new(U3CFocusDownU3Ec__Iterator2D_t659480186_il2cpp_TypeInfo_var);
		U3CFocusDownU3Ec__Iterator2D__ctor_m1503383393(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFocusDownU3Ec__Iterator2D_t659480186 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CFocusDownU3Ec__Iterator2D_t659480186 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::.ctor()
extern "C"  void U3CenableCoruU3Ec__Iterator2C__ctor_m2108770640 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ShowRecipePre/<enableCoru>c__Iterator2C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CenableCoruU3Ec__Iterator2C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2201829634 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ShowRecipePre/<enableCoru>c__Iterator2C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CenableCoruU3Ec__Iterator2C_System_Collections_IEnumerator_get_Current_m2227538070 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ShowRecipePre/<enableCoru>c__Iterator2C::MoveNext()
extern Il2CppClass* WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var;
extern const uint32_t U3CenableCoruU3Ec__Iterator2C_MoveNext_m4084401828_MetadataUsageId;
extern "C"  bool U3CenableCoruU3Ec__Iterator2C_MoveNext_m4084401828 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CenableCoruU3Ec__Iterator2C_MoveNext_m4084401828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0054;
	}

IL_0021:
	{
		WaitForSecondsRealtime_t3698499994 * L_2 = (WaitForSecondsRealtime_t3698499994 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t3698499994_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m1777059252(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0056;
	}

IL_003d:
	{
		ShowRecipePre_t977589144 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		RecipeUpDown_t1716098347 * L_4 = L_3->get_UpDown_9();
		NullCheck(L_4);
		RecipeUpDown_powerUp_m229524046(L_4, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0054:
	{
		return (bool)0;
	}

IL_0056:
	{
		return (bool)1;
	}
	// Dead block : IL_0058: ldloc.1
}
// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::Dispose()
extern "C"  void U3CenableCoruU3Ec__Iterator2C_Dispose_m3497271693 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ShowRecipePre/<enableCoru>c__Iterator2C::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CenableCoruU3Ec__Iterator2C_Reset_m4050170877_MetadataUsageId;
extern "C"  void U3CenableCoruU3Ec__Iterator2C_Reset_m4050170877 (U3CenableCoruU3Ec__Iterator2C_t1488953467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CenableCoruU3Ec__Iterator2C_Reset_m4050170877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::.ctor()
extern "C"  void U3CFocusDownU3Ec__Iterator2D__ctor_m1503383393 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ShowRecipePre/<FocusDown>c__Iterator2D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFocusDownU3Ec__Iterator2D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3613956443 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object ShowRecipePre/<FocusDown>c__Iterator2D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFocusDownU3Ec__Iterator2D_System_Collections_IEnumerator_get_Current_m1903604463 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean ShowRecipePre/<FocusDown>c__Iterator2D::MoveNext()
extern "C"  bool U3CFocusDownU3Ec__Iterator2D_MoveNext_m413530075 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_00cc;
	}

IL_0021:
	{
		ShowRecipePre_t977589144 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = L_2->get_imgTrans_7();
		ShowRecipePre_t977589144 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = L_4->get_imgTrans_7();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		ShowRecipePre_t977589144 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		TransformU5BU5D_t3792884695* L_8 = L_7->get_recipeTrans_6();
		ShowRecipePre_t977589144 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_currentIngredient_12();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
		int32_t L_11 = L_10;
		Transform_t1659122786 * L_12 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Vector3_t4282066566  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_6, L_13, ((float)((float)(5.0f)*(float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_15, /*hidden argument*/NULL);
		ShowRecipePre_t977589144 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = L_16->get_imgTrans_7();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		ShowRecipePre_t977589144 * L_19 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_19);
		TransformU5BU5D_t3792884695* L_20 = L_19->get_recipeTrans_6();
		ShowRecipePre_t977589144 * L_21 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_currentIngredient_12();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_22);
		int32_t L_23 = L_22;
		Transform_t1659122786 * L_24 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		float L_26 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_18, L_25, /*hidden argument*/NULL);
		if ((!(((float)L_26) < ((float)(0.1f)))))
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00c5;
	}

IL_00ad:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_00ce;
	}

IL_00c0:
	{
		goto IL_0021;
	}

IL_00c5:
	{
		__this->set_U24PC_0((-1));
	}

IL_00cc:
	{
		return (bool)0;
	}

IL_00ce:
	{
		return (bool)1;
	}
	// Dead block : IL_00d0: ldloc.1
}
// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::Dispose()
extern "C"  void U3CFocusDownU3Ec__Iterator2D_Dispose_m1540712286 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void ShowRecipePre/<FocusDown>c__Iterator2D::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFocusDownU3Ec__Iterator2D_Reset_m3444783630_MetadataUsageId;
extern "C"  void U3CFocusDownU3Ec__Iterator2D_Reset_m3444783630 (U3CFocusDownU3Ec__Iterator2D_t659480186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFocusDownU3Ec__Iterator2D_Reset_m3444783630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SoundManage::.ctor()
extern "C"  void SoundManage__ctor_m2177573559 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::.cctor()
extern "C"  void SoundManage__cctor_m2598174678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SoundManage::Awake()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const uint32_t SoundManage_Awake_m2415178778_MetadataUsageId;
extern "C"  void SoundManage_Awake_m2415178778 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManage_Awake_m2415178778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->set_instance_4(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_2 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::efxButtonOn()
extern "C"  void SoundManage_efxButtonOn_m2962933949 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1740077639 * L_0 = __this->get_efxSource_2();
		AudioClip_t794140988 * L_1 = __this->get__efxBtn_14();
		NullCheck(L_0);
		AudioSource_PlayOneShot_m823779350(L_0, L_1, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::efxOkOn()
extern "C"  void SoundManage_efxOkOn_m3935479783 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1740077639 * L_0 = __this->get_efxSource_2();
		AudioClip_t794140988 * L_1 = __this->get__efxOk_15();
		NullCheck(L_0);
		AudioSource_PlayOneShot_m823779350(L_0, L_1, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::efxBadOn()
extern "C"  void SoundManage_efxBadOn_m2820389210 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1740077639 * L_0 = __this->get_efxSource_2();
		AudioClip_t794140988 * L_1 = __this->get__efxBad_16();
		NullCheck(L_0);
		AudioSource_PlayOneShot_m823779350(L_0, L_1, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::PlayBgm(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral80818744;
extern Il2CppCodeGenString* _stringLiteral73591734;
extern Il2CppCodeGenString* _stringLiteral392418414;
extern Il2CppCodeGenString* _stringLiteral2493766289;
extern Il2CppCodeGenString* _stringLiteral2999403785;
extern Il2CppCodeGenString* _stringLiteral236546926;
extern Il2CppCodeGenString* _stringLiteral70470760;
extern Il2CppCodeGenString* _stringLiteral47513267;
extern Il2CppCodeGenString* _stringLiteral1862981704;
extern Il2CppCodeGenString* _stringLiteral4148943661;
extern Il2CppCodeGenString* _stringLiteral2957617669;
extern const uint32_t SoundManage_PlayBgm_m180107993_MetadataUsageId;
extern "C"  void SoundManage_PlayBgm_m180107993 (SoundManage_t3403985716 * __this, String_t* ___sceneName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManage_PlayBgm_m180107993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, _stringLiteral80818744, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		AudioClip_t794140988 * L_2 = __this->get__bgmTitle_5();
		__this->set__bgm_13(L_2);
		goto IL_0145;
	}

IL_0021:
	{
		String_t* L_3 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, _stringLiteral73591734, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		AudioClip_t794140988 * L_5 = __this->get__bgmLobby_6();
		__this->set__bgm_13(L_5);
		goto IL_0145;
	}

IL_0042:
	{
		String_t* L_6 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_6, _stringLiteral392418414, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		AudioClip_t794140988 * L_8 = __this->get__bgmGrinding_7();
		__this->set__bgm_13(L_8);
		goto IL_0145;
	}

IL_0063:
	{
		String_t* L_9 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral2493766289, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0084;
		}
	}
	{
		AudioClip_t794140988 * L_11 = __this->get__bgmVariaion_8();
		__this->set__bgm_13(L_11);
		goto IL_0145;
	}

IL_0084:
	{
		String_t* L_12 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_12, _stringLiteral2999403785, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00a5;
		}
	}
	{
		AudioClip_t794140988 * L_14 = __this->get__bgmMinigame_12();
		__this->set__bgm_13(L_14);
		goto IL_0145;
	}

IL_00a5:
	{
		String_t* L_15 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_15, _stringLiteral236546926, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c6;
		}
	}
	{
		AudioClip_t794140988 * L_17 = __this->get__bgmTemping_9();
		__this->set__bgm_13(L_17);
		goto IL_0145;
	}

IL_00c6:
	{
		String_t* L_18 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral70470760, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e7;
		}
	}
	{
		AudioClip_t794140988 * L_20 = __this->get__bgmIcing_10();
		__this->set__bgm_13(L_20);
		goto IL_0145;
	}

IL_00e7:
	{
		String_t* L_21 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_21, _stringLiteral47513267, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0127;
		}
	}
	{
		String_t* L_23 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_23, _stringLiteral1862981704, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0127;
		}
	}
	{
		String_t* L_25 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_25, _stringLiteral4148943661, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0127;
		}
	}
	{
		String_t* L_27 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_27, _stringLiteral2957617669, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0145;
		}
	}

IL_0127:
	{
		bool L_29 = __this->get_isPlaying_17();
		if (L_29)
		{
			goto IL_0145;
		}
	}
	{
		__this->set_isPlaying_17((bool)1);
		AudioClip_t794140988 * L_30 = __this->get__bgmLearn_11();
		__this->set__bgm_13(L_30);
	}

IL_0145:
	{
		AudioSource_t1740077639 * L_31 = __this->get_musicSource_3();
		AudioClip_t794140988 * L_32 = __this->get__bgm_13();
		NullCheck(L_31);
		AudioSource_set_clip_m19502010(L_31, L_32, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_33 = __this->get_musicSource_3();
		NullCheck(L_33);
		Behaviour_set_enabled_m2046806933(L_33, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManage::OffBgm()
extern "C"  void SoundManage_OffBgm_m2683352902 (SoundManage_t3403985716 * __this, const MethodInfo* method)
{
	{
		__this->set_isPlaying_17((bool)0);
		AudioSource_t1740077639 * L_0 = __this->get_musicSource_3();
		NullCheck(L_0);
		AudioSource_set_clip_m19502010(L_0, (AudioClip_t794140988 *)NULL, /*hidden argument*/NULL);
		AudioSource_t1740077639 * L_1 = __this->get_musicSource_3();
		NullCheck(L_1);
		Behaviour_set_enabled_m2046806933(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SphereMirror::.ctor()
extern "C"  void SphereMirror__ctor_m2674768399 (SphereMirror_t167396876 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SphereMirror::Start()
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3839065225_m3816897097_MethodInfo_var;
extern const uint32_t SphereMirror_Start_m1621906191_MetadataUsageId;
extern "C"  void SphereMirror_Start_m1621906191 (SphereMirror_t167396876 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SphereMirror_Start_m1621906191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2U5BU5D_t4024180168* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MeshFilter_t3839065225 * L_1 = Component_GetComponent_TisMeshFilter_t3839065225_m3816897097(L_0, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3839065225_m3816897097_MethodInfo_var);
		NullCheck(L_1);
		Mesh_t4241756145 * L_2 = MeshFilter_get_mesh_m484001117(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector2U5BU5D_t4024180168* L_3 = Mesh_get_uv_m558008935(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0050;
	}

IL_001d:
	{
		Vector2U5BU5D_t4024180168* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Vector2U5BU5D_t4024180168* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		float L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_x_1();
		Vector2U5BU5D_t4024180168* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		float L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_y_2();
		Vector2_t4282066565  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m1517109030(&L_12, ((float)((float)(1.0f)-(float)L_8)), L_11, /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_12;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_14 = V_1;
		Vector2U5BU5D_t4024180168* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_001d;
		}
	}
	{
		Transform_t1659122786 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		MeshFilter_t3839065225 * L_17 = Component_GetComponent_TisMeshFilter_t3839065225_m3816897097(L_16, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3839065225_m3816897097_MethodInfo_var);
		NullCheck(L_17);
		Mesh_t4241756145 * L_18 = MeshFilter_get_mesh_m484001117(L_17, /*hidden argument*/NULL);
		Vector2U5BU5D_t4024180168* L_19 = V_0;
		NullCheck(L_18);
		Mesh_set_uv_m498907190(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SphereMirror::Update()
extern "C"  void SphereMirror_Update_m3040303838 (SphereMirror_t167396876 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void start_mini_game::.ctor()
extern "C"  void start_mini_game__ctor_m2312171726 (start_mini_game_t3518442301 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void start_mini_game::Start()
extern Il2CppClass* PopupManagement_t282433007_il2cpp_TypeInfo_var;
extern const uint32_t start_mini_game_Start_m1259309518_MetadataUsageId;
extern "C"  void start_mini_game_Start_m1259309518 (start_mini_game_t3518442301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (start_mini_game_Start_m1259309518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PopupManagement_t282433007 * V_0 = NULL;
	PlayMakerFSM_t3799847376 * V_1 = NULL;
	{
		PopupManagement_t282433007 * L_0 = ((PopupManagement_t282433007_StaticFields*)PopupManagement_t282433007_il2cpp_TypeInfo_var->static_fields)->get_p_2();
		V_0 = L_0;
		PopupManagement_t282433007 * L_1 = V_0;
		NullCheck(L_1);
		PopupManagement_makeIt_m1215430652(L_1, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void start_mini_game::Update()
extern "C"  void start_mini_game_Update_m389741567 (start_mini_game_t3518442301 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void start_mini_game::onStart()
extern const MethodInfo* GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2996680944;
extern Il2CppCodeGenString* _stringLiteral64212328;
extern const uint32_t start_mini_game_onStart_m2876431439_MetadataUsageId;
extern "C"  void start_mini_game_onStart_m2876431439 (start_mini_game_t3518442301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (start_mini_game_onStart_m2876431439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayMakerFSM_t3799847376 * V_0 = NULL;
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2996680944, /*hidden argument*/NULL);
		NullCheck(L_0);
		PlayMakerFSM_t3799847376 * L_1 = GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayMakerFSM_t3799847376_m3107677963_MethodInfo_var);
		V_0 = L_1;
		PlayMakerFSM_t3799847376 * L_2 = V_0;
		NullCheck(L_2);
		Fsm_t1527112426 * L_3 = PlayMakerFSM_get_Fsm_m886945091(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Fsm_Event_m4127177141(L_3, _stringLiteral64212328, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartFirst::.ctor()
extern "C"  void StartFirst__ctor_m2925777101 (StartFirst_t396912526 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_3((3.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartFirst::Start()
extern "C"  void StartFirst_Start_m1872914893 (StartFirst_t396912526 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = StartFirst_BlackBlink_m3299090428(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator StartFirst::BlackBlink()
extern Il2CppClass* U3CBlackBlinkU3Ec__Iterator2E_t2642540702_il2cpp_TypeInfo_var;
extern const uint32_t StartFirst_BlackBlink_m3299090428_MetadataUsageId;
extern "C"  Il2CppObject * StartFirst_BlackBlink_m3299090428 (StartFirst_t396912526 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartFirst_BlackBlink_m3299090428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * V_0 = NULL;
	{
		U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * L_0 = (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 *)il2cpp_codegen_object_new(U3CBlackBlinkU3Ec__Iterator2E_t2642540702_il2cpp_TypeInfo_var);
		U3CBlackBlinkU3Ec__Iterator2E__ctor_m3552183229(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * L_2 = V_0;
		return L_2;
	}
}
// System.Void StartFirst/<BlackBlink>c__Iterator2E::.ctor()
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E__ctor_m3552183229 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object StartFirst/<BlackBlink>c__Iterator2E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlackBlinkU3Ec__Iterator2E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3755503551 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object StartFirst/<BlackBlink>c__Iterator2E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlackBlinkU3Ec__Iterator2E_System_Collections_IEnumerator_get_Current_m1367855443 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean StartFirst/<BlackBlink>c__Iterator2E::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral80818744;
extern const uint32_t U3CBlackBlinkU3Ec__Iterator2E_MoveNext_m2286416639_MetadataUsageId;
extern "C"  bool U3CBlackBlinkU3Ec__Iterator2E_MoveNext_m2286416639 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlackBlinkU3Ec__Iterator2E_MoveNext_m2286416639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_00ef;
		}
		if (L_1 == 2)
		{
			goto IL_0110;
		}
		if (L_1 == 3)
		{
			goto IL_019a;
		}
	}
	{
		goto IL_01b0;
	}

IL_0029:
	{
		__this->set_U3CstartTimeU3E__0_0((3.0f));
		StartFirst_t396912526 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_Blink_2();
		NullCheck(L_3);
		Image_t538875265 * L_4 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		Color_t4194546905  L_5 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
		StartFirst_t396912526 * L_6 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = L_6->get_Blink_2();
		NullCheck(L_7);
		GameObject_t3674682005 * L_8 = GameObject_get_gameObject_m1966529385(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m3538205401(L_8, (bool)1, /*hidden argument*/NULL);
	}

IL_0064:
	{
		float L_9 = __this->get_U3CstartTimeU3E__0_0();
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		StartFirst_t396912526 * L_11 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_11);
		float L_12 = L_11->get_speed_3();
		__this->set_U3CstartTimeU3E__0_0(((float)((float)L_9-(float)((float)((float)L_10*(float)L_12)))));
		StartFirst_t396912526 * L_13 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_13);
		GameObject_t3674682005 * L_14 = L_13->get_Blink_2();
		NullCheck(L_14);
		Image_t538875265 * L_15 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_14, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		StartFirst_t396912526 * L_16 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_16);
		GameObject_t3674682005 * L_17 = L_16->get_Blink_2();
		NullCheck(L_17);
		Image_t538875265 * L_18 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_17, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_18);
		Color_t4194546905  L_19 = VirtFuncInvoker0< Color_t4194546905  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_18);
		Color_t4194546905  L_20 = Color_get_clear_m2578346879(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		StartFirst_t396912526 * L_22 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_22);
		float L_23 = L_22->get_speed_3();
		Color_t4194546905  L_24 = Color_LerpUnclamped_m1638053670(NULL /*static, unused*/, L_19, L_20, ((float)((float)L_21*(float)L_23)), /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_15, L_24);
		float L_25 = __this->get_U3CstartTimeU3E__0_0();
		if ((!(((float)L_25) <= ((float)(0.0f)))))
		{
			goto IL_00dc;
		}
	}
	{
		goto IL_00f4;
	}

IL_00dc:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_01b2;
	}

IL_00ef:
	{
		goto IL_0064;
	}

IL_00f4:
	{
		WaitForSeconds_t1615819279 * L_26 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_26, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_26);
		__this->set_U24PC_1(2);
		goto IL_01b2;
	}

IL_0110:
	{
		__this->set_U3CstartTimeU3E__0_0((3.0f));
	}

IL_011b:
	{
		float L_27 = __this->get_U3CstartTimeU3E__0_0();
		float L_28 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E__0_0(((float)((float)L_27-(float)L_28)));
		StartFirst_t396912526 * L_29 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_29);
		GameObject_t3674682005 * L_30 = L_29->get_Blink_2();
		NullCheck(L_30);
		Image_t538875265 * L_31 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_30, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		StartFirst_t396912526 * L_32 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_32);
		GameObject_t3674682005 * L_33 = L_32->get_Blink_2();
		NullCheck(L_33);
		Image_t538875265 * L_34 = GameObject_GetComponent_TisImage_t538875265_m2140199269(L_33, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m2140199269_MethodInfo_var);
		NullCheck(L_34);
		Color_t4194546905  L_35 = VirtFuncInvoker0< Color_t4194546905  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_34);
		Color_t4194546905  L_36 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_37 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		StartFirst_t396912526 * L_38 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_38);
		float L_39 = L_38->get_speed_3();
		Color_t4194546905  L_40 = Color_LerpUnclamped_m1638053670(NULL /*static, unused*/, L_35, L_36, ((float)((float)L_37*(float)L_39)), /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker1< Color_t4194546905  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_31, L_40);
		float L_41 = __this->get_U3CstartTimeU3E__0_0();
		if ((!(((float)L_41) <= ((float)(0.0f)))))
		{
			goto IL_0187;
		}
	}
	{
		goto IL_019f;
	}

IL_0187:
	{
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(3);
		goto IL_01b2;
	}

IL_019a:
	{
		goto IL_011b;
	}

IL_019f:
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral80818744, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_01b0:
	{
		return (bool)0;
	}

IL_01b2:
	{
		return (bool)1;
	}
	// Dead block : IL_01b4: ldloc.1
}
// System.Void StartFirst/<BlackBlink>c__Iterator2E::Dispose()
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E_Dispose_m3342333114 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void StartFirst/<BlackBlink>c__Iterator2E::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CBlackBlinkU3Ec__Iterator2E_Reset_m1198616170_MetadataUsageId;
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E_Reset_m1198616170 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CBlackBlinkU3Ec__Iterator2E_Reset_m1198616170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void StartImageBGMS::.ctor()
extern "C"  void StartImageBGMS__ctor_m1151217335 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartImageBGMS::Start()
extern "C"  void StartImageBGMS_Start_m98355127 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method)
{
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m894591657((&V_0), /*hidden argument*/NULL);
		__this->set_nowScene_2(L_1);
		return;
	}
}
// System.Void StartImageBGMS::OnDisable()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t StartImageBGMS_OnDisable_m1710313374_MetadataUsageId;
extern "C"  void StartImageBGMS_OnDisable_m1710313374 (StartImageBGMS_t2059898468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartImageBGMS_OnDisable_m1710313374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		String_t* L_1 = __this->get_nowScene_2();
		NullCheck(L_0);
		SoundManage_PlayBgm_m180107993(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartImageDisapear::.ctor()
extern "C"  void StartImageDisapear__ctor_m1986209417 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartImageDisapear::Awake()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern const uint32_t StartImageDisapear_Awake_m2223814636_MetadataUsageId;
extern "C"  void StartImageDisapear_Awake_m2223814636 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartImageDisapear_Awake_m2223814636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t1080795294  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		Scene_t1080795294  L_2 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = Scene_get_name_m894591657((&V_1), /*hidden argument*/NULL);
		__this->set_nowScene_3(L_3);
		int32_t L_4 = __this->get_nowNum_2();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_5 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_stuff_4(L_5);
		StructforMinigame_t1286533533 * L_6 = __this->get_stuff_4();
		NullCheck(L_6);
		Stopwatch_t3420517611 * L_7 = StructforMinigame_get_Sw_m1790624722(L_6, /*hidden argument*/NULL);
		__this->set_sw_5(L_7);
		return;
	}
}
// System.Void StartImageDisapear::OnDisable()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisScoreManager_Icing_t3849909252_m2889672921_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisScoreManager_Variation_t1665153935_m389509742_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral70470760;
extern Il2CppCodeGenString* _stringLiteral773600752;
extern Il2CppCodeGenString* _stringLiteral2493766289;
extern const uint32_t StartImageDisapear_OnDisable_m3119648368_MetadataUsageId;
extern "C"  void StartImageDisapear_OnDisable_m3119648368 (StartImageDisapear_t2918831698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartImageDisapear_OnDisable_m3119648368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stopwatch_t3420517611 * L_0 = __this->get_sw_5();
		NullCheck(L_0);
		Stopwatch_Start_m3677209584(L_0, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_nowScene_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral70470760, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t3674682005 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral773600752, /*hidden argument*/NULL);
		NullCheck(L_3);
		ScoreManager_Icing_t3849909252 * L_4 = GameObject_GetComponent_TisScoreManager_Icing_t3849909252_m2889672921(L_3, /*hidden argument*/GameObject_GetComponent_TisScoreManager_Icing_t3849909252_m2889672921_MethodInfo_var);
		NullCheck(L_4);
		ScoreManager_Icing_TimeCheckerStart_m2412828135(L_4, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0039:
	{
		String_t* L_5 = __this->get_nowScene_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, _stringLiteral2493766289, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		GameObject_t3674682005 * L_7 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral773600752, /*hidden argument*/NULL);
		NullCheck(L_7);
		ScoreManager_Variation_t1665153935 * L_8 = GameObject_GetComponent_TisScoreManager_Variation_t1665153935_m389509742(L_7, /*hidden argument*/GameObject_GetComponent_TisScoreManager_Variation_t1665153935_m389509742_MethodInfo_var);
		NullCheck(L_8);
		ScoreManager_Variation_TimeCheckerStart_m1950168562(L_8, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void StructforMinigame::.ctor()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern const uint32_t StructforMinigame__ctor_m2741910638_MetadataUsageId;
extern "C"  void StructforMinigame__ctor_m2741910638 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame__ctor_m2741910638_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_001e;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_0 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_1 = V_0;
		int32_t L_2 = V_0;
		StructforMinigame_t1286533533 * L_3 = (StructforMinigame_t1286533533 *)il2cpp_codegen_object_new(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame__ctor_m919518399(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (StructforMinigame_t1286533533 *)L_3);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)20))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void StructforMinigame::.ctor(System.Int32)
extern Il2CppClass* Stopwatch_t3420517611_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3921354019;
extern const uint32_t StructforMinigame__ctor_m919518399_MetadataUsageId;
extern "C"  void StructforMinigame__ctor_m919518399 (StructforMinigame_t1286533533 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame__ctor_m919518399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Stopwatch_t3420517611 * L_0 = (Stopwatch_t3420517611 *)il2cpp_codegen_object_new(Stopwatch_t3420517611_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m435104496(L_0, /*hidden argument*/NULL);
		__this->set_sw_1(L_0);
		__this->set_userId_2(_stringLiteral3921354019);
		__this->set_gameId_3(1);
		__this->set_level_4(1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_recordDate_5(L_1);
		__this->set_score_6((0.0f));
		__this->set_playtime_7((0.0f));
		__this->set_misscount_8(0);
		__this->set_errorRate_9((0.0f));
		return;
	}
}
// System.Void StructforMinigame::.cctor()
extern Il2CppClass* StructforMinigameU5BU5D_t1843399504_il2cpp_TypeInfo_var;
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern const uint32_t StructforMinigame__cctor_m2912754943_MetadataUsageId;
extern "C"  void StructforMinigame__cctor_m2912754943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame__cctor_m2912754943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->set_stuff_0(((StructforMinigameU5BU5D_t1843399504*)SZArrayNew(StructforMinigameU5BU5D_t1843399504_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20))));
		return;
	}
}
// System.String StructforMinigame::get_Userid()
extern "C"  String_t* StructforMinigame_get_Userid_m3023313986 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_userId_2();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Userid(System.String)
extern "C"  void StructforMinigame_set_Userid_m3465234537 (StructforMinigame_t1286533533 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_userId_2(L_0);
		return;
	}
}
// System.Int32 StructforMinigame::get_Gameid()
extern "C"  int32_t StructforMinigame_get_Gameid_m349399966 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_gameId_3();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Gameid(System.Int32)
extern "C"  void StructforMinigame_set_Gameid_m1686107441 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = Math_Max_m1309380475(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		__this->set_gameId_3(L_1);
		return;
	}
}
// System.Int32 StructforMinigame::get_Level()
extern "C"  int32_t StructforMinigame_get_Level_m2077766709 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_level_4();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Level(System.Int32)
extern "C"  void StructforMinigame_set_Level_m547274884 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = Math_Max_m1309380475(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		__this->set_level_4(L_1);
		return;
	}
}
// System.String StructforMinigame::get_RecordDate()
extern "C"  String_t* StructforMinigame_get_RecordDate_m24046075 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_recordDate_5();
		return L_0;
	}
}
// System.Void StructforMinigame::set_RecordDate(System.String)
extern "C"  void StructforMinigame_set_RecordDate_m450818768 (StructforMinigame_t1286533533 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_recordDate_5(L_0);
		return;
	}
}
// System.Single StructforMinigame::get_Score()
extern "C"  float StructforMinigame_get_Score_m239363585 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_score_6();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Score(System.Single)
extern "C"  void StructforMinigame_set_Score_m1127316490 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_0, /*hidden argument*/NULL);
		__this->set_score_6(L_1);
		return;
	}
}
// System.Single StructforMinigame::get_Playtime()
extern "C"  float StructforMinigame_get_Playtime_m2221809428 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_playtime_7();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Playtime(System.Single)
extern "C"  void StructforMinigame_set_Playtime_m3659740247 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_0, /*hidden argument*/NULL);
		__this->set_playtime_7(L_1);
		return;
	}
}
// System.Int32 StructforMinigame::get_Misscount()
extern "C"  int32_t StructforMinigame_get_Misscount_m2827575940 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_misscount_8();
		return L_0;
	}
}
// System.Void StructforMinigame::set_Misscount(System.Int32)
extern "C"  void StructforMinigame_set_Misscount_m2498396499 (StructforMinigame_t1286533533 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = Math_Max_m1309380475(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		__this->set_misscount_8(L_1);
		return;
	}
}
// System.Single StructforMinigame::get_ErrorRate()
extern "C"  float StructforMinigame_get_ErrorRate_m2504854007 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_errorRate_9();
		return L_0;
	}
}
// System.Void StructforMinigame::set_ErrorRate(System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t StructforMinigame_set_ErrorRate_m2862618580_MetadataUsageId;
extern "C"  void StructforMinigame_set_ErrorRate_m2862618580 (StructforMinigame_t1286533533 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame_set_ErrorRate_m2862618580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Min_m2322067385(NULL /*static, unused*/, (100.0f), L_0, /*hidden argument*/NULL);
		float L_2 = Math_Max_m172798965(NULL /*static, unused*/, (0.0f), L_1, /*hidden argument*/NULL);
		__this->set_errorRate_9(L_2);
		return;
	}
}
// System.Diagnostics.Stopwatch StructforMinigame::get_Sw()
extern "C"  Stopwatch_t3420517611 * StructforMinigame_get_Sw_m1790624722 (StructforMinigame_t1286533533 * __this, const MethodInfo* method)
{
	{
		Stopwatch_t3420517611 * L_0 = __this->get_sw_1();
		return L_0;
	}
}
// StructforMinigame StructforMinigame::getSingleton(System.Int32)
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern const uint32_t StructforMinigame_getSingleton_m51070278_MetadataUsageId;
extern "C"  StructforMinigame_t1286533533 * StructforMinigame_getSingleton_m51070278 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame_getSingleton_m51070278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)90)))))
		{
			goto IL_000b;
		}
	}
	{
		___index0 = 4;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_1 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		StructforMinigame_t1286533533 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_5 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_6 = ___index0;
		int32_t L_7 = ___index0;
		StructforMinigame_t1286533533 * L_8 = (StructforMinigame_t1286533533 *)il2cpp_codegen_object_new(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame__ctor_m919518399(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (StructforMinigame_t1286533533 *)L_8);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_9 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_10 = ___index0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		StructforMinigame_t1286533533 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		return L_12;
	}
}
// StructforMinigame[] StructforMinigame::getSingleton()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern const uint32_t StructforMinigame_getSingleton_m919431639_MetadataUsageId;
extern "C"  StructforMinigameU5BU5D_t1843399504* StructforMinigame_getSingleton_m919431639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StructforMinigame_getSingleton_m919431639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_0 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		StructforMinigame_t1286533533 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_4 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		int32_t L_5 = V_0;
		int32_t L_6 = V_0;
		StructforMinigame_t1286533533 * L_7 = (StructforMinigame_t1286533533 *)il2cpp_codegen_object_new(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame__ctor_m919518399(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (StructforMinigame_t1286533533 *)L_7);
	}

IL_0020:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)20))))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigameU5BU5D_t1843399504* L_10 = ((StructforMinigame_t1286533533_StaticFields*)StructforMinigame_t1286533533_il2cpp_TypeInfo_var->static_fields)->get_stuff_0();
		return L_10;
	}
}
// System.Void TempingLastObj::.ctor()
extern "C"  void TempingLastObj__ctor_m3727658696 (TempingLastObj_t3031672627 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingLastObj::OnEnable()
extern const MethodInfo* GameObject_GetComponent_TisScoreManager_Temping_t3029572618_m1205013395_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral773600752;
extern const uint32_t TempingLastObj_OnEnable_m2077385662_MetadataUsageId;
extern "C"  void TempingLastObj_OnEnable_m2077385662 (TempingLastObj_t3031672627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingLastObj_OnEnable_m2077385662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral773600752, /*hidden argument*/NULL);
		NullCheck(L_0);
		ScoreManager_Temping_t3029572618 * L_1 = GameObject_GetComponent_TisScoreManager_Temping_t3029572618_m1205013395(L_0, /*hidden argument*/GameObject_GetComponent_TisScoreManager_Temping_t3029572618_m1205013395_MethodInfo_var);
		NullCheck(L_1);
		ScoreManager_Temping_EndOfExtracting_m1906868770(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingPointer::.ctor()
extern "C"  void TempingPointer__ctor_m1650546316 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_12((0.1f));
		__this->set_speedDown_13((0.3f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingPointer::Start()
extern const MethodInfo* Component_GetComponent_TisScoreManager_Temping_t3029572618_m1898272059_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral1320885114;
extern Il2CppCodeGenString* _stringLiteral2247099273;
extern const uint32_t TempingPointer_Start_m597684108_MetadataUsageId;
extern "C"  void TempingPointer_Start_m597684108 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_Start_m597684108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_FirstGaugeTrans_11(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ScoreManager_Temping_t3029572618 * L_2 = Component_GetComponent_TisScoreManager_Temping_t3029572618_m1898272059(L_1, /*hidden argument*/Component_GetComponent_TisScoreManager_Temping_t3029572618_m1898272059_MethodInfo_var);
		__this->set__tempingMng_9(L_2);
		GameObject_t3674682005 * L_3 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_FindChild_m2149912886(L_4, _stringLiteral1320885114, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_FindChild_m2149912886(L_6, _stringLiteral2247099273, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		__this->set_gaugePoint_10(L_8);
		Transform_t1659122786 * L_9 = __this->get_gaugePoint_10();
		NullCheck(L_9);
		Vector3_t4282066566  L_10 = Transform_get_position_m2211398607(L_9, /*hidden argument*/NULL);
		__this->set_FirstGaugeTrans_11(L_10);
		return;
	}
}
// System.Void TempingPointer::FirstSelectButton()
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral3754286315;
extern Il2CppCodeGenString* _stringLiteral846339116;
extern Il2CppCodeGenString* _stringLiteral80998175;
extern Il2CppCodeGenString* _stringLiteral70760763;
extern const uint32_t TempingPointer_FirstSelectButton_m102904008_MetadataUsageId;
extern "C"  void TempingPointer_FirstSelectButton_m102904008 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_FirstSelectButton_m102904008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_FindChild_m2149912886(L_1, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_FindChild_m2149912886(L_3, _stringLiteral846339116, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Transform_FindChild_m2149912886(L_7, _stringLiteral80998175, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = Component_get_transform_m4257140443(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = Transform_FindChild_m2149912886(L_9, _stringLiteral70760763, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m3538205401(L_11, (bool)0, /*hidden argument*/NULL);
		Il2CppObject * L_12 = TempingPointer_MainCameraMove_m2703110703(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator TempingPointer::MainCameraMove()
extern Il2CppClass* U3CMainCameraMoveU3Ec__Iterator2F_t3998083026_il2cpp_TypeInfo_var;
extern const uint32_t TempingPointer_MainCameraMove_m2703110703_MetadataUsageId;
extern "C"  Il2CppObject * TempingPointer_MainCameraMove_m2703110703 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_MainCameraMove_m2703110703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * V_0 = NULL;
	{
		U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * L_0 = (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 *)il2cpp_codegen_object_new(U3CMainCameraMoveU3Ec__Iterator2F_t3998083026_il2cpp_TypeInfo_var);
		U3CMainCameraMoveU3Ec__Iterator2F__ctor_m1971214089(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * L_2 = V_0;
		return L_2;
	}
}
// System.Void TempingPointer::Init()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern const uint32_t TempingPointer_Init_m1667348040_MetadataUsageId;
extern "C"  void TempingPointer_Init_m1667348040 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_Init_m1667348040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ScoreManager_Temping_t3029572618 * L_0 = __this->get__tempingMng_9();
		NullCheck(L_0);
		L_0->set_tempingAmount_12((0.0f));
		Transform_t1659122786 * L_1 = __this->get_gaugePoint_10();
		Vector3_t4282066566  L_2 = __this->get_FirstGaugeTrans_11();
		NullCheck(L_1);
		Transform_set_position_m3111394108(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_3 = __this->get_Temper_8();
		Transform_t1659122786 * L_4 = __this->get__filterTarget_6();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveTo_m3275864871(NULL /*static, unused*/, L_3, L_5, (3.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingPointer::TempingMouseDown()
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral3754286315;
extern Il2CppCodeGenString* _stringLiteral1932202358;
extern const uint32_t TempingPointer_TempingMouseDown_m2262648689_MetadataUsageId;
extern "C"  void TempingPointer_TempingMouseDown_m2262648689 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_TempingMouseDown_m2262648689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopAllCoroutines_m1437893335(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_FindChild_m2149912886(L_1, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_FindChild_m2149912886(L_3, _stringLiteral1932202358, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_6 = TempingPointer_TempingSlideUp_m2215103486(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator TempingPointer::TempingSlideUp()
extern Il2CppClass* U3CTempingSlideUpU3Ec__Iterator30_t3867951914_il2cpp_TypeInfo_var;
extern const uint32_t TempingPointer_TempingSlideUp_m2215103486_MetadataUsageId;
extern "C"  Il2CppObject * TempingPointer_TempingSlideUp_m2215103486 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_TempingSlideUp_m2215103486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * V_0 = NULL;
	{
		U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * L_0 = (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 *)il2cpp_codegen_object_new(U3CTempingSlideUpU3Ec__Iterator30_t3867951914_il2cpp_TypeInfo_var);
		U3CTempingSlideUpU3Ec__Iterator30__ctor_m4151656113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator TempingPointer::TempingSlideDown()
extern Il2CppClass* U3CTempingSlideDownU3Ec__Iterator31_t3382326578_il2cpp_TypeInfo_var;
extern const uint32_t TempingPointer_TempingSlideDown_m2220440517_MetadataUsageId;
extern "C"  Il2CppObject * TempingPointer_TempingSlideDown_m2220440517 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_TempingSlideDown_m2220440517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * V_0 = NULL;
	{
		U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * L_0 = (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 *)il2cpp_codegen_object_new(U3CTempingSlideDownU3Ec__Iterator31_t3382326578_il2cpp_TypeInfo_var);
		U3CTempingSlideDownU3Ec__Iterator31__ctor_m1655238569(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * L_2 = V_0;
		return L_2;
	}
}
// System.Void TempingPointer::TempingMouseUp()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral3754286315;
extern Il2CppCodeGenString* _stringLiteral1932202358;
extern Il2CppCodeGenString* _stringLiteral1562924924;
extern Il2CppCodeGenString* _stringLiteral1320885114;
extern Il2CppCodeGenString* _stringLiteral721162579;
extern Il2CppCodeGenString* _stringLiteral80998175;
extern Il2CppCodeGenString* _stringLiteral815429745;
extern const uint32_t TempingPointer_TempingMouseUp_m624087722_MetadataUsageId;
extern "C"  void TempingPointer_TempingMouseUp_m624087722 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_TempingMouseUp_m624087722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopAllCoroutines_m1437893335(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = GameObject_get_transform_m1278640159(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_FindChild_m2149912886(L_1, _stringLiteral3754286315, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t1659122786 * L_4 = Transform_FindChild_m2149912886(L_3, _stringLiteral1932202358, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_6 = __this->get__tempingMng_9();
		NullCheck(L_6);
		float L_7 = L_6->get_tempingAmount_12();
		ScoreManager_Temping_t3029572618 * L_8 = __this->get__tempingMng_9();
		NullCheck(L_8);
		float L_9 = L_8->get_tempingLimitAmount_11();
		ScoreManager_Temping_t3029572618 * L_10 = __this->get__tempingMng_9();
		NullCheck(L_10);
		float L_11 = L_10->get_tempingLimitAmount_11();
		float L_12 = fabsf(((float)((float)((float)((float)((float)((float)L_7-(float)L_9))/(float)L_11))*(float)(100.0f))));
		float L_13 = L_12;
		Il2CppObject * L_14 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1562924924, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_16 = __this->get__tempingMng_9();
		NullCheck(L_16);
		float L_17 = L_16->get_tempingAmount_12();
		ScoreManager_Temping_t3029572618 * L_18 = __this->get__tempingMng_9();
		NullCheck(L_18);
		float L_19 = L_18->get_tempingLimitAmount_11();
		ScoreManager_Temping_t3029572618 * L_20 = __this->get__tempingMng_9();
		NullCheck(L_20);
		float L_21 = L_20->get_tempingLimitAmount_11();
		float L_22 = fabsf(((float)((float)((float)((float)((float)((float)L_17-(float)L_19))/(float)L_21))*(float)(100.0f))));
		if ((!(((float)((float)((float)(100.0f)-(float)L_22))) < ((float)(60.0f)))))
		{
			goto IL_00cb;
		}
	}
	{
		Il2CppObject * L_23 = TempingPointer_TempingSlideDown_m2220440517(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_23, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00cb:
	{
		GameObject_t3674682005 * L_24 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t1659122786 * L_25 = GameObject_get_transform_m1278640159(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = Transform_FindChild_m2149912886(L_25, _stringLiteral1320885114, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_t3674682005 * L_27 = Component_get_gameObject_m1170635899(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		GameObject_SetActive_m3538205401(L_27, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_28 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_t1659122786 * L_29 = GameObject_get_transform_m1278640159(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = Transform_FindChild_m2149912886(L_29, _stringLiteral721162579, /*hidden argument*/NULL);
		NullCheck(L_30);
		GameObject_t3674682005 * L_31 = Component_get_gameObject_m1170635899(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		GameObject_SetActive_m3538205401(L_31, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_32 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t1659122786 * L_33 = GameObject_get_transform_m1278640159(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_t1659122786 * L_34 = Transform_FindChild_m2149912886(L_33, _stringLiteral80998175, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = Component_get_transform_m4257140443(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t1659122786 * L_36 = Transform_FindChild_m2149912886(L_35, _stringLiteral815429745, /*hidden argument*/NULL);
		NullCheck(L_36);
		GameObject_t3674682005 * L_37 = Component_get_gameObject_m1170635899(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		GameObject_SetActive_m3538205401(L_37, (bool)0, /*hidden argument*/NULL);
		ScoreManager_Temping_t3029572618 * L_38 = __this->get__tempingMng_9();
		NullCheck(L_38);
		ScoreManager_Temping_EndOfTemping_m311035759(L_38, /*hidden argument*/NULL);
	}

IL_0151:
	{
		return;
	}
}
// System.Void TempingPointer::Temping1Button()
extern Il2CppCodeGenString* _stringLiteral1990582217;
extern const uint32_t TempingPointer_Temping1Button_m2914373293_MetadataUsageId;
extern "C"  void TempingPointer_Temping1Button_m2914373293 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_Temping1Button_m2914373293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayMakerFSM_t3799847376 * L_0 = __this->get__myFsm_3();
		NullCheck(L_0);
		Fsm_t1527112426 * L_1 = PlayMakerFSM_get_Fsm_m886945091(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_Event_m4127177141(L_1, _stringLiteral1990582217, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingPointer::Temping2Button()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1990582218;
extern const uint32_t TempingPointer_Temping2Button_m1106918830_MetadataUsageId;
extern "C"  void TempingPointer_Temping2Button_m1106918830 (TempingPointer_t2678149871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TempingPointer_Temping2Button_m1106918830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral1990582218, /*hidden argument*/NULL);
		PlayMakerFSM_t3799847376 * L_0 = __this->get__myFsm_3();
		NullCheck(L_0);
		Fsm_t1527112426 * L_1 = PlayMakerFSM_get_Fsm_m886945091(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_Event_m4127177141(L_1, _stringLiteral1990582218, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::.ctor()
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F__ctor_m1971214089 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object TempingPointer/<MainCameraMove>c__Iterator2F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMainCameraMoveU3Ec__Iterator2F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1138446067 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object TempingPointer/<MainCameraMove>c__Iterator2F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMainCameraMoveU3Ec__Iterator2F_System_Collections_IEnumerator_get_Current_m91723911 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean TempingPointer/<MainCameraMove>c__Iterator2F::MoveNext()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern Il2CppCodeGenString* _stringLiteral122;
extern Il2CppCodeGenString* _stringLiteral342389008;
extern Il2CppCodeGenString* _stringLiteral3507899432;
extern Il2CppCodeGenString* _stringLiteral3192295205;
extern Il2CppCodeGenString* _stringLiteral109641799;
extern Il2CppCodeGenString* _stringLiteral3494502884;
extern Il2CppCodeGenString* _stringLiteral80998175;
extern Il2CppCodeGenString* _stringLiteral815429745;
extern Il2CppCodeGenString* _stringLiteral1320885114;
extern const uint32_t U3CMainCameraMoveU3Ec__Iterator2F_MoveNext_m3077134387_MetadataUsageId;
extern "C"  bool U3CMainCameraMoveU3Ec__Iterator2F_MoveNext_m3077134387 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMainCameraMoveU3Ec__Iterator2F_MoveNext_m3077134387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t4282066566  V_3;
	memset(&V_3, 0, sizeof(V_3));
	bool V_4 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0143;
		}
	}
	{
		goto IL_01a1;
	}

IL_0021:
	{
		TempingPointer_t2678149871 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		Camera_t2727095145 * L_3 = L_2->get__camera_2();
		NullCheck(L_3);
		GameObject_t3674682005 * L_4 = Component_get_gameObject_m1170635899(L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t1108656482* L_5 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12)));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, _stringLiteral120);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral120);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		TempingPointer_t2678149871 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = L_7->get__cameraPos_4();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_x_1();
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_12);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_12);
		ObjectU5BU5D_t1108656482* L_13 = L_6;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral121);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral121);
		ObjectU5BU5D_t1108656482* L_14 = L_13;
		TempingPointer_t2678149871 * L_15 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_15);
		Transform_t1659122786 * L_16 = L_15->get__cameraPos_4();
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_localPosition_m668140784(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = (&V_2)->get_y_2();
		float L_19 = L_18;
		Il2CppObject * L_20 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_20);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_20);
		ObjectU5BU5D_t1108656482* L_21 = L_14;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		ArrayElementTypeCheck (L_21, _stringLiteral122);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral122);
		ObjectU5BU5D_t1108656482* L_22 = L_21;
		TempingPointer_t2678149871 * L_23 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = L_23->get__cameraPos_4();
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localPosition_m668140784(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_z_3();
		float L_27 = L_26;
		Il2CppObject * L_28 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 5);
		ArrayElementTypeCheck (L_22, L_28);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_28);
		ObjectU5BU5D_t1108656482* L_29 = L_22;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 6);
		ArrayElementTypeCheck (L_29, _stringLiteral342389008);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral342389008);
		ObjectU5BU5D_t1108656482* L_30 = L_29;
		TempingPointer_t2678149871 * L_31 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_31);
		Transform_t1659122786 * L_32 = L_31->get__lookTarget_5();
		NullCheck(L_32);
		Vector3_t4282066566  L_33 = Transform_get_position_m2211398607(L_32, /*hidden argument*/NULL);
		Vector3_t4282066566  L_34 = L_33;
		Il2CppObject * L_35 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 7);
		ArrayElementTypeCheck (L_30, L_35);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_35);
		ObjectU5BU5D_t1108656482* L_36 = L_30;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 8);
		ArrayElementTypeCheck (L_36, _stringLiteral3507899432);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral3507899432);
		ObjectU5BU5D_t1108656482* L_37 = L_36;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)9));
		ArrayElementTypeCheck (L_37, _stringLiteral3192295205);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral3192295205);
		ObjectU5BU5D_t1108656482* L_38 = L_37;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)10));
		ArrayElementTypeCheck (L_38, _stringLiteral109641799);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral109641799);
		ObjectU5BU5D_t1108656482* L_39 = L_38;
		float L_40 = (2.0f);
		Il2CppObject * L_41 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)11));
		ArrayElementTypeCheck (L_39, L_41);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_41);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		Hashtable_t1407064410 * L_42 = iTween_Hash_m2099495140(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		iTween_MoveTo_m569418479(NULL /*static, unused*/, L_4, L_42, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_43 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_43);
		GameObject_t3674682005 * L_44 = L_43->get_Temper_8();
		TempingPointer_t2678149871 * L_45 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_45);
		Transform_t1659122786 * L_46 = L_45->get__filterTarget_6();
		NullCheck(L_46);
		Vector3_t4282066566  L_47 = Transform_get_position_m2211398607(L_46, /*hidden argument*/NULL);
		iTween_MoveTo_m3275864871(NULL /*static, unused*/, L_44, L_47, (3.0f), /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_48 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_48, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_48);
		__this->set_U24PC_0(1);
		goto IL_01a3;
	}

IL_0143:
	{
		GameObject_t3674682005 * L_49 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t1659122786 * L_50 = GameObject_get_transform_m1278640159(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Transform_t1659122786 * L_51 = Transform_FindChild_m2149912886(L_50, _stringLiteral80998175, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t1659122786 * L_52 = Component_get_transform_m4257140443(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t1659122786 * L_53 = Transform_FindChild_m2149912886(L_52, _stringLiteral815429745, /*hidden argument*/NULL);
		NullCheck(L_53);
		GameObject_t3674682005 * L_54 = Component_get_gameObject_m1170635899(L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		GameObject_SetActive_m3538205401(L_54, (bool)1, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_55 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3494502884, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t1659122786 * L_56 = GameObject_get_transform_m1278640159(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_t1659122786 * L_57 = Transform_FindChild_m2149912886(L_56, _stringLiteral1320885114, /*hidden argument*/NULL);
		NullCheck(L_57);
		GameObject_t3674682005 * L_58 = Component_get_gameObject_m1170635899(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		GameObject_SetActive_m3538205401(L_58, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_01a1:
	{
		return (bool)0;
	}

IL_01a3:
	{
		return (bool)1;
	}
	// Dead block : IL_01a5: ldloc.s V_4
}
// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::Dispose()
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F_Dispose_m154445062 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void TempingPointer/<MainCameraMove>c__Iterator2F::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMainCameraMoveU3Ec__Iterator2F_Reset_m3912614326_MetadataUsageId;
extern "C"  void U3CMainCameraMoveU3Ec__Iterator2F_Reset_m3912614326 (U3CMainCameraMoveU3Ec__Iterator2F_t3998083026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMainCameraMoveU3Ec__Iterator2F_Reset_m3912614326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::.ctor()
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31__ctor_m1655238569 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object TempingPointer/<TempingSlideDown>c__Iterator31::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideDownU3Ec__Iterator31_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1746407699 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object TempingPointer/<TempingSlideDown>c__Iterator31::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideDownU3Ec__Iterator31_System_Collections_IEnumerator_get_Current_m3938228903 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean TempingPointer/<TempingSlideDown>c__Iterator31::MoveNext()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t U3CTempingSlideDownU3Ec__Iterator31_MoveNext_m1457168019_MetadataUsageId;
extern "C"  bool U3CTempingSlideDownU3Ec__Iterator31_MoveNext_m1457168019 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTempingSlideDownU3Ec__Iterator31_MoveNext_m1457168019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_013a;
		}
	}
	{
		goto IL_0146;
	}

IL_0021:
	{
		TempingPointer_t2678149871 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_Temper_8();
		TempingPointer_t2678149871 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = L_4->get__filterTarget_6();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveTo_m3275864871(NULL /*static, unused*/, L_3, L_6, (3.0f), /*hidden argument*/NULL);
	}

IL_0046:
	{
		TempingPointer_t2678149871 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = L_7->get_gaugePoint_10();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		if ((!(((float)L_10) <= ((float)(0.0f)))))
		{
			goto IL_009c;
		}
	}
	{
		TempingPointer_t2678149871 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		ScoreManager_Temping_t3029572618 * L_12 = L_11->get__tempingMng_9();
		NullCheck(L_12);
		float L_13 = L_12->get_tempingAmount_12();
		if ((!(((float)L_13) < ((float)(0.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		TempingPointer_t2678149871 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		ScoreManager_Temping_t3029572618 * L_15 = L_14->get__tempingMng_9();
		NullCheck(L_15);
		L_15->set_tempingAmount_12((0.0f));
	}

IL_0097:
	{
		goto IL_0146;
	}

IL_009c:
	{
		TempingPointer_t2678149871 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = L_16->get_gaugePoint_10();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_localPosition_m668140784(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_y_2();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_22 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = L_22->get_gaugePoint_10();
		Transform_t1659122786 * L_24 = L_23;
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localPosition_m668140784(L_24, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_26 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_26);
		float L_27 = L_26->get_speedDown_13();
		float L_28 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2926210380(&L_29, (0.0f), ((float)((float)L_27*(float)L_28)), (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m3504330903(L_24, L_30, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_31 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_31);
		ScoreManager_Temping_t3029572618 * L_32 = L_31->get__tempingMng_9();
		ScoreManager_Temping_t3029572618 * L_33 = L_32;
		NullCheck(L_33);
		float L_34 = L_33->get_tempingAmount_12();
		TempingPointer_t2678149871 * L_35 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_35);
		float L_36 = L_35->get_speedDown_13();
		float L_37 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_tempingAmount_12(((float)((float)L_34-(float)((float)((float)((float)((float)L_36*(float)L_37))*(float)(200.0f))))));
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_0148;
	}

IL_013a:
	{
		goto IL_0046;
	}
	// Dead block : IL_013f: ldarg.0

IL_0146:
	{
		return (bool)0;
	}

IL_0148:
	{
		return (bool)1;
	}
}
// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::Dispose()
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31_Dispose_m1444648358 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void TempingPointer/<TempingSlideDown>c__Iterator31::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTempingSlideDownU3Ec__Iterator31_Reset_m3596638806_MetadataUsageId;
extern "C"  void U3CTempingSlideDownU3Ec__Iterator31_Reset_m3596638806 (U3CTempingSlideDownU3Ec__Iterator31_t3382326578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTempingSlideDownU3Ec__Iterator31_Reset_m3596638806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::.ctor()
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30__ctor_m4151656113 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object TempingPointer/<TempingSlideUp>c__Iterator30::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideUpU3Ec__Iterator30_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15851595 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object TempingPointer/<TempingSlideUp>c__Iterator30::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTempingSlideUpU3Ec__Iterator30_System_Collections_IEnumerator_get_Current_m248240607 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean TempingPointer/<TempingSlideUp>c__Iterator30::MoveNext()
extern Il2CppClass* iTween_t3087282050_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const uint32_t U3CTempingSlideUpU3Ec__Iterator30_MoveNext_m3540086667_MetadataUsageId;
extern "C"  bool U3CTempingSlideUpU3Ec__Iterator30_MoveNext_m3540086667 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTempingSlideUpU3Ec__Iterator30_MoveNext_m3540086667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t4282066566  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_013a;
		}
	}
	{
		goto IL_0146;
	}

IL_0021:
	{
		TempingPointer_t2678149871 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t3674682005 * L_3 = L_2->get_Temper_8();
		TempingPointer_t2678149871 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = L_4->get__filterDown_7();
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_position_m2211398607(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t3087282050_il2cpp_TypeInfo_var);
		iTween_MoveTo_m3275864871(NULL /*static, unused*/, L_3, L_6, (3.0f), /*hidden argument*/NULL);
	}

IL_0046:
	{
		TempingPointer_t2678149871 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = L_7->get_gaugePoint_10();
		NullCheck(L_8);
		Vector3_t4282066566  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		if ((!(((float)L_10) >= ((float)(0.5f)))))
		{
			goto IL_009c;
		}
	}
	{
		TempingPointer_t2678149871 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		ScoreManager_Temping_t3029572618 * L_12 = L_11->get__tempingMng_9();
		NullCheck(L_12);
		float L_13 = L_12->get_tempingAmount_12();
		if ((!(((float)L_13) > ((float)(100.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		TempingPointer_t2678149871 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		ScoreManager_Temping_t3029572618 * L_15 = L_14->get__tempingMng_9();
		NullCheck(L_15);
		L_15->set_tempingAmount_12((100.0f));
	}

IL_0097:
	{
		goto IL_0146;
	}

IL_009c:
	{
		TempingPointer_t2678149871 * L_16 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_16);
		Transform_t1659122786 * L_17 = L_16->get_gaugePoint_10();
		NullCheck(L_17);
		Vector3_t4282066566  L_18 = Transform_get_localPosition_m668140784(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_y_2();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_22 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_22);
		Transform_t1659122786 * L_23 = L_22->get_gaugePoint_10();
		Transform_t1659122786 * L_24 = L_23;
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_localPosition_m668140784(L_24, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_26 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_26);
		float L_27 = L_26->get_speed_12();
		float L_28 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2926210380(&L_29, (0.0f), ((float)((float)L_27*(float)L_28)), (0.0f), /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m3504330903(L_24, L_30, /*hidden argument*/NULL);
		TempingPointer_t2678149871 * L_31 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_31);
		ScoreManager_Temping_t3029572618 * L_32 = L_31->get__tempingMng_9();
		ScoreManager_Temping_t3029572618 * L_33 = L_32;
		NullCheck(L_33);
		float L_34 = L_33->get_tempingAmount_12();
		TempingPointer_t2678149871 * L_35 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_35);
		float L_36 = L_35->get_speed_12();
		float L_37 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_tempingAmount_12(((float)((float)L_34+(float)((float)((float)((float)((float)L_36*(float)L_37))*(float)(200.0f))))));
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_0148;
	}

IL_013a:
	{
		goto IL_0046;
	}
	// Dead block : IL_013f: ldarg.0

IL_0146:
	{
		return (bool)0;
	}

IL_0148:
	{
		return (bool)1;
	}
}
// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::Dispose()
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30_Dispose_m3910156974 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void TempingPointer/<TempingSlideUp>c__Iterator30::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CTempingSlideUpU3Ec__Iterator30_Reset_m1798089054_MetadataUsageId;
extern "C"  void U3CTempingSlideUpU3Ec__Iterator30_Reset_m1798089054 (U3CTempingSlideUpU3Ec__Iterator30_t3867951914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CTempingSlideUpU3Ec__Iterator30_Reset_m1798089054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TimerPref::.ctor()
extern "C"  void TimerPref__ctor_m799065987 (TimerPref_t2057114344 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerPref::Start()
extern Il2CppClass* StructforMinigame_t1286533533_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstforMinigame_t2789090703_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const uint32_t TimerPref_Start_m4041171075_MetadataUsageId;
extern "C"  void TimerPref_Start_m4041171075 (TimerPref_t2057114344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimerPref_Start_m4041171075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1080795294  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1080795294  L_0 = SceneManager_GetActiveScene_m3062973092(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3533090789((&V_0), /*hidden argument*/NULL);
		__this->set_nowNum_4(((int32_t)((int32_t)L_1-(int32_t)1)));
		int32_t L_2 = __this->get_nowNum_4();
		IL2CPP_RUNTIME_CLASS_INIT(StructforMinigame_t1286533533_il2cpp_TypeInfo_var);
		StructforMinigame_t1286533533 * L_3 = StructforMinigame_getSingleton_m51070278(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_stuff_6(L_3);
		int32_t L_4 = __this->get_nowNum_4();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_5 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_grade_5();
		IL2CPP_RUNTIME_CLASS_INIT(ConstforMinigame_t2789090703_il2cpp_TypeInfo_var);
		ConstforMinigame_t2789090703 * L_7 = ConstforMinigame_getSingleton_m1109573803(NULL /*static, unused*/, L_4, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_cuff_7(L_7);
		Text_t9039225 * L_8 = __this->get_timerText_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007e;
		}
	}
	{
		Text_t9039225 * L_10 = __this->get_timerText_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ConstforMinigame_t2789090703 * L_12 = __this->get_cuff_7();
		NullCheck(L_12);
		float L_13 = ConstforMinigame_get_Limittime_m4054875741(L_12, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_14);
		String_t* L_16 = String_Concat_m389863537(NULL /*static, unused*/, L_11, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_16);
		goto IL_00b4;
	}

IL_007e:
	{
		TextMesh_t2567681854 * L_17 = __this->get_timerTextMesh_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_17, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b4;
		}
	}
	{
		TextMesh_t2567681854 * L_19 = __this->get_timerTextMesh_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ConstforMinigame_t2789090703 * L_21 = __this->get_cuff_7();
		NullCheck(L_21);
		float L_22 = ConstforMinigame_get_Limittime_m4054875741(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Il2CppObject * L_24 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_23);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, L_20, L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		TextMesh_set_text_m3628430759(L_19, L_25, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		Image_t538875265 * L_26 = __this->get_img_8();
		ConstforMinigame_t2789090703 * L_27 = __this->get_cuff_7();
		NullCheck(L_27);
		float L_28 = ConstforMinigame_get_Limittime_m4054875741(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		Image_set_fillAmount_m1583793743(L_26, ((float)((float)(((float)((float)((float)((float)(180.0f)-(float)L_28)))))*(float)(0.0055f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerPref::Update()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t TimerPref_Update_m728103914_MetadataUsageId;
extern "C"  void TimerPref_Update_m728103914 (TimerPref_t2057114344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TimerPref_Update_m728103914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TextMesh_t2567681854 * L_0 = __this->get_timerTextMesh_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		Image_t538875265 * L_2 = __this->get_img_8();
		NullCheck(L_2);
		float L_3 = Image_get_fillAmount_m3193252212(L_2, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_3)))) <= ((double)(0.99)))))
		{
			goto IL_007f;
		}
	}
	{
		Text_t9039225 * L_4 = __this->get_timerText_2();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(73 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		if (L_5)
		{
			goto IL_0055;
		}
	}
	{
		Text_t9039225 * L_6 = __this->get_timerText_2();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(73 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007f;
		}
	}

IL_0055:
	{
		Text_t9039225 * L_10 = __this->get_timerText_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(73 /* System.String UnityEngine.UI.Text::get_text() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_12 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Image_t538875265 * L_13 = __this->get_img_8();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		Image_set_fillAmount_m1583793743(L_13, ((float)((float)(((float)((float)((int32_t)((int32_t)((int32_t)180)-(int32_t)L_14)))))*(float)(0.0055f))), /*hidden argument*/NULL);
	}

IL_007f:
	{
		goto IL_00f2;
	}

IL_0084:
	{
		Image_t538875265 * L_15 = __this->get_img_8();
		NullCheck(L_15);
		float L_16 = Image_get_fillAmount_m3193252212(L_15, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_16)))) <= ((double)(0.99)))))
		{
			goto IL_00f2;
		}
	}
	{
		TextMesh_t2567681854 * L_17 = __this->get_timerTextMesh_3();
		NullCheck(L_17);
		String_t* L_18 = TextMesh_get_text_m2892967978(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00c8;
		}
	}
	{
		TextMesh_t2567681854 * L_19 = __this->get_timerTextMesh_3();
		NullCheck(L_19);
		String_t* L_20 = TextMesh_get_text_m2892967978(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_22 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00f2;
		}
	}

IL_00c8:
	{
		TextMesh_t2567681854 * L_23 = __this->get_timerTextMesh_3();
		NullCheck(L_23);
		String_t* L_24 = TextMesh_get_text_m2892967978(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_25 = Convert_ToInt32_m4085381007(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		Image_t538875265 * L_26 = __this->get_img_8();
		int32_t L_27 = V_1;
		NullCheck(L_26);
		Image_set_fillAmount_m1583793743(L_26, ((float)((float)(((float)((float)((int32_t)((int32_t)((int32_t)180)-(int32_t)L_27)))))*(float)(0.0055f))), /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void TrackMouseMovement::.ctor()
extern "C"  void TrackMouseMovement__ctor_m2636792242 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackMouseMovement::Start()
extern "C"  void TrackMouseMovement_Start_m1583930034 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TrackMouseMovement::Update()
extern "C"  void TrackMouseMovement_Update_m1863042971 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UserExperience::.ctor()
extern "C"  void UserExperience__ctor_m1985297542 (UserExperience_t3961908149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserId::.ctor()
extern "C"  void UserId__ctor_m24616181 (UserId_t2542803558 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserInfo::.ctor()
extern "C"  void UserInfo__ctor_m3194749826 (UserInfo_t4092807993 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserInfoCall::.ctor()
extern "C"  void UserInfoCall__ctor_m1362750276 (UserInfoCall_t3868783927 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UserInfoType::.ctor()
extern "C"  void UserInfoType__ctor_m2250608104 (UserInfoType_t3869313555 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariationResultWindow::.ctor()
extern "C"  void VariationResultWindow__ctor_m2133979499 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariationResultWindow::OnEnable()
extern Il2CppClass* RecipeList_t1641733996_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_1_t2778891315_il2cpp_TypeInfo_var;
extern Il2CppClass* MenuList_t3755595453_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_GetInstance_m3214810238_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3285151025_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m935595982_MethodInfo_var;
extern const uint32_t VariationResultWindow_OnEnable_m1348917819_MetadataUsageId;
extern "C"  void VariationResultWindow_OnEnable_m1348917819 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VariationResultWindow_OnEnable_m1348917819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Sprite_t3199167241 * V_0 = NULL;
	MenuList_t3755595453 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RecipeList_t1641733996_il2cpp_TypeInfo_var);
		RecipeList_t1641733996 * L_0 = RecipeList_GetInstance_m3683561895(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringU5BU5D_t4054002952* L_1 = L_0->get_coffeeImgList_3();
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_2 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_menu_10();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_3);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		Sprite_t3199167241 * L_6 = Resources_Load_TisSprite_t3199167241_m3887230130(NULL /*static, unused*/, L_5, /*hidden argument*/Resources_Load_TisSprite_t3199167241_m3887230130_MethodInfo_var);
		V_0 = L_6;
		Image_t538875265 * L_7 = __this->get_coffeImg_2();
		Sprite_t3199167241 * L_8 = V_0;
		NullCheck(L_7);
		Image_set_sprite_m572551402(L_7, L_8, /*hidden argument*/NULL);
		MenuList_t3755595453 * L_9 = (MenuList_t3755595453 *)il2cpp_codegen_object_new(MenuList_t3755595453_il2cpp_TypeInfo_var);
		MenuList__ctor_m2878352510(L_9, /*hidden argument*/NULL);
		V_1 = L_9;
		MainUserInfo_t2526075922 * L_10 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_cream_11();
		if (L_11)
		{
			goto IL_0048;
		}
	}
	{
		Image_t538875265 * L_12 = __this->get_CreamImg_3();
		NullCheck(L_12);
		Behaviour_set_enabled_m2046806933(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_0048:
	{
		Text_t9039225 * L_13 = __this->get_menuText_4();
		MenuList_t3755595453 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t2778891315_il2cpp_TypeInfo_var);
		MainUserInfo_t2526075922 * L_15 = Singleton_1_get_GetInstance_m3214810238(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_GetInstance_m3214810238_MethodInfo_var);
		NullCheck(L_15);
		int32_t L_16 = L_15->get_menu_10();
		NullCheck(L_14);
		String_t* L_17 = MenuList_GetMenuInPopup_m1960107628(L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_17);
		V_2 = 0;
		goto IL_00aa;
	}

IL_006a:
	{
		TextU5BU5D_t3798907012* L_18 = __this->get_menuTextList_7();
		int32_t L_19 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		Text_t9039225 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		ShowRecipePre_t977589144 * L_22 = __this->get__showRecipePre_5();
		NullCheck(L_22);
		List_1_t1375417109 * L_23 = L_22->get_menuList_10();
		int32_t L_24 = V_2;
		NullCheck(L_23);
		String_t* L_25 = List_1_get_Item_m3285151025(L_23, L_24, /*hidden argument*/List_1_get_Item_m3285151025_MethodInfo_var);
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_25);
		TextU5BU5D_t3798907012* L_26 = __this->get_unitTextList_8();
		int32_t L_27 = V_2;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		Text_t9039225 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		ShowRecipePre_t977589144 * L_30 = __this->get__showRecipePre_5();
		NullCheck(L_30);
		List_1_t1375417109 * L_31 = L_30->get_unitList_11();
		int32_t L_32 = V_2;
		NullCheck(L_31);
		String_t* L_33 = List_1_get_Item_m3285151025(L_31, L_32, /*hidden argument*/List_1_get_Item_m3285151025_MethodInfo_var);
		NullCheck(L_29);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, L_33);
		int32_t L_34 = V_2;
		V_2 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_35 = V_2;
		ShowRecipePre_t977589144 * L_36 = __this->get__showRecipePre_5();
		NullCheck(L_36);
		List_1_t1375417109 * L_37 = L_36->get_menuList_10();
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m935595982(L_37, /*hidden argument*/List_1_get_Count_m935595982_MethodInfo_var);
		if ((((int32_t)L_35) < ((int32_t)L_38)))
		{
			goto IL_006a;
		}
	}
	{
		return;
	}
}
// System.Void VariationResultWindow::Update()
extern "C"  void VariationResultWindow_Update_m3455717122 (VariationResultWindow_t1568540672 * __this, const MethodInfo* method)
{
	{
		ShowRecipePre_t977589144 * L_0 = __this->get__showRecipePre_5();
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = L_0->get_imgTrans_7();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = GameObject_get_active_m1960165469(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		Button_t3896396478 * L_4 = __this->get__btn_6();
		NullCheck(L_4);
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m3538205401(L_5, (bool)0, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0030:
	{
		Button_t3896396478 * L_6 = __this->get__btn_6();
		NullCheck(L_6);
		GameObject_t3674682005 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::.ctor()
extern "C"  void YoutubeEasyMovieTexture__ctor_m4288615259 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::Start()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeEasyMovieTexture_Start_m3235753051_MetadataUsageId;
extern "C"  void YoutubeEasyMovieTexture_Start_m3235753051 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeEasyMovieTexture_Start_m3235753051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_OffBgm_m2683352902(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::BackButtonClick()
extern Il2CppCodeGenString* _stringLiteral47513267;
extern const uint32_t YoutubeEasyMovieTexture_BackButtonClick_m2711418056_MetadataUsageId;
extern "C"  void YoutubeEasyMovieTexture_BackButtonClick_m2711418056 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeEasyMovieTexture_BackButtonClick_m2711418056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral47513267, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::ButtonClickYoutube(System.String)
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppClass* YoutubeVideo_t2077346616_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeEasyMovieTexture_ButtonClickYoutube_m692045996_MetadataUsageId;
extern "C"  void YoutubeEasyMovieTexture_ButtonClickYoutube_m692045996 (YoutubeEasyMovieTexture_t1195908624 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeEasyMovieTexture_ButtonClickYoutube_m692045996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_LearnPop_4();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_Player_2();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)1, /*hidden argument*/NULL);
		MediaPlayerCtrl_t3572035536 * L_3 = __this->get__movie_3();
		YoutubeVideo_t2077346616 * L_4 = ((YoutubeVideo_t2077346616_StaticFields*)YoutubeVideo_t2077346616_il2cpp_TypeInfo_var->static_fields)->get_Instance_2();
		String_t* L_5 = ___str0;
		NullCheck(L_4);
		String_t* L_6 = YoutubeVideo_RequestVideo_m2780138341(L_4, L_5, ((int32_t)360), /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_m_strFileName_2(L_6);
		MediaPlayerCtrl_t3572035536 * L_7 = __this->get__movie_3();
		NullCheck(L_7);
		MediaPlayerCtrl_Play_m4037048957(L_7, /*hidden argument*/NULL);
		Il2CppObject * L_8 = YoutubeEasyMovieTexture_CheckPlay_m3316543517(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::StopButton()
extern "C"  void YoutubeEasyMovieTexture_StopButton_m3077138781 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get__movie_3();
		NullCheck(L_0);
		MediaPlayerCtrl_Stop_m4130733003(L_0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = __this->get_Player_2();
		NullCheck(L_1);
		GameObject_SetActive_m3538205401(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_2 = __this->get_LearnPop_4();
		NullCheck(L_2);
		GameObject_SetActive_m3538205401(L_2, (bool)1, /*hidden argument*/NULL);
		Slider_t79469677 * L_3 = __this->get__slider_5();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_3, (0.0f));
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::PlayOrPause()
extern "C"  void YoutubeEasyMovieTexture_PlayOrPause_m1579295416 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get__movie_3();
		NullCheck(L_0);
		int32_t L_1 = MediaPlayerCtrl_GetCurrentState_m372257026(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0021;
		}
	}
	{
		MediaPlayerCtrl_t3572035536 * L_2 = __this->get__movie_3();
		NullCheck(L_2);
		MediaPlayerCtrl_Pause_m297895727(L_2, /*hidden argument*/NULL);
		goto IL_002c;
	}

IL_0021:
	{
		MediaPlayerCtrl_t3572035536 * L_3 = __this->get__movie_3();
		NullCheck(L_3);
		MediaPlayerCtrl_Play_m4037048957(L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::MouseDown()
extern "C"  void YoutubeEasyMovieTexture_MouseDown_m1542773152 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get__movie_3();
		NullCheck(L_0);
		MediaPlayerCtrl_Pause_m297895727(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeEasyMovieTexture::MouseUp()
extern "C"  void YoutubeEasyMovieTexture_MouseUp_m2719425689 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get__movie_3();
		Slider_t79469677 * L_1 = __this->get__slider_5();
		NullCheck(L_1);
		float L_2 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		NullCheck(L_0);
		MediaPlayerCtrl_SeekTo_m1961002989(L_0, (((int32_t)((int32_t)L_2))), /*hidden argument*/NULL);
		MediaPlayerCtrl_t3572035536 * L_3 = __this->get__movie_3();
		NullCheck(L_3);
		MediaPlayerCtrl_Play_m4037048957(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator YoutubeEasyMovieTexture::CheckPlay()
extern Il2CppClass* U3CCheckPlayU3Ec__Iterator33_t4170198610_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeEasyMovieTexture_CheckPlay_m3316543517_MetadataUsageId;
extern "C"  Il2CppObject * YoutubeEasyMovieTexture_CheckPlay_m3316543517 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeEasyMovieTexture_CheckPlay_m3316543517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckPlayU3Ec__Iterator33_t4170198610 * V_0 = NULL;
	{
		U3CCheckPlayU3Ec__Iterator33_t4170198610 * L_0 = (U3CCheckPlayU3Ec__Iterator33_t4170198610 *)il2cpp_codegen_object_new(U3CCheckPlayU3Ec__Iterator33_t4170198610_il2cpp_TypeInfo_var);
		U3CCheckPlayU3Ec__Iterator33__ctor_m3454047369(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckPlayU3Ec__Iterator33_t4170198610 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCheckPlayU3Ec__Iterator33_t4170198610 * L_2 = V_0;
		return L_2;
	}
}
// System.Void YoutubeEasyMovieTexture::FixedUpdate()
extern "C"  void YoutubeEasyMovieTexture_FixedUpdate_m1185267862 (YoutubeEasyMovieTexture_t1195908624 * __this, const MethodInfo* method)
{
	{
		MediaPlayerCtrl_t3572035536 * L_0 = __this->get__movie_3();
		NullCheck(L_0);
		int32_t L_1 = MediaPlayerCtrl_GetCurrentState_m372257026(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0028;
		}
	}
	{
		Slider_t79469677 * L_2 = __this->get__slider_5();
		MediaPlayerCtrl_t3572035536 * L_3 = __this->get__movie_3();
		NullCheck(L_3);
		int32_t L_4 = MediaPlayerCtrl_GetSeekPosition_m2843612094(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, (((float)((float)L_4))));
	}

IL_0028:
	{
		return;
	}
}
// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::.ctor()
extern "C"  void U3CCheckPlayU3Ec__Iterator33__ctor_m3454047369 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckPlayU3Ec__Iterator33_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2143567603 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckPlayU3Ec__Iterator33_System_Collections_IEnumerator_get_Current_m2436925575 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::MoveNext()
extern "C"  bool U3CCheckPlayU3Ec__Iterator33_MoveNext_m2964566195 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00a6;
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		YoutubeEasyMovieTexture_t1195908624 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		MediaPlayerCtrl_t3572035536 * L_3 = L_2->get__movie_3();
		NullCheck(L_3);
		int32_t L_4 = MediaPlayerCtrl_GetCurrentState_m372257026(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)3))))
		{
			goto IL_0093;
		}
	}
	{
		YoutubeEasyMovieTexture_t1195908624 * L_5 = __this->get_U3CU3Ef__this_2();
		YoutubeEasyMovieTexture_t1195908624 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		MediaPlayerCtrl_t3572035536 * L_7 = L_6->get__movie_3();
		NullCheck(L_7);
		int32_t L_8 = MediaPlayerCtrl_GetDuration_m3691995857(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_totalLength_7(L_8);
		YoutubeEasyMovieTexture_t1195908624 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		Slider_t79469677 * L_10 = L_9->get__slider_5();
		YoutubeEasyMovieTexture_t1195908624 * L_11 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_totalLength_7();
		NullCheck(L_10);
		Slider_set_maxValue_m4261736743(L_10, (((float)((float)L_12))), /*hidden argument*/NULL);
		YoutubeEasyMovieTexture_t1195908624 * L_13 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_13);
		Text_t9039225 * L_14 = L_13->get__text_6();
		YoutubeEasyMovieTexture_t1195908624 * L_15 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_15);
		int32_t* L_16 = L_15->get_address_of_totalLength_7();
		String_t* L_17 = Int32_ToString_m1286526384(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_17);
		goto IL_00b2;
	}

IL_0093:
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(1);
		goto IL_00b4;
	}

IL_00a6:
	{
		goto IL_0021;
	}
	// Dead block : IL_00ab: ldarg.0

IL_00b2:
	{
		return (bool)0;
	}

IL_00b4:
	{
		return (bool)1;
	}
}
// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::Dispose()
extern "C"  void U3CCheckPlayU3Ec__Iterator33_Dispose_m3523052166 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void YoutubeEasyMovieTexture/<CheckPlay>c__Iterator33::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckPlayU3Ec__Iterator33_Reset_m1100480310_MetadataUsageId;
extern "C"  void U3CCheckPlayU3Ec__Iterator33_Reset_m1100480310 (U3CCheckPlayU3Ec__Iterator33_t4170198610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckPlayU3Ec__Iterator33_Reset_m1100480310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void YoutubeStrim::.ctor()
extern "C"  void YoutubeStrim__ctor_m3835307273 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeStrim::Start()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeStrim_Start_m2782445065_MetadataUsageId;
extern "C"  void YoutubeStrim_Start_m2782445065 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeStrim_Start_m2782445065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_OffBgm_m2683352902(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeStrim::BackButtonClick()
extern Il2CppClass* SoundManage_t3403985716_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47513267;
extern const uint32_t YoutubeStrim_BackButtonClick_m740836598_MetadataUsageId;
extern "C"  void YoutubeStrim_BackButtonClick_m740836598 (YoutubeStrim_t2074917330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeStrim_BackButtonClick_m740836598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManage_t3403985716_il2cpp_TypeInfo_var);
		SoundManage_t3403985716 * L_0 = ((SoundManage_t3403985716_StaticFields*)SoundManage_t3403985716_il2cpp_TypeInfo_var->static_fields)->get_instance_4();
		NullCheck(L_0);
		SoundManage_efxButtonOn_m2962933949(L_0, /*hidden argument*/NULL);
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral47513267, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeVideo::.ctor()
extern "C"  void YoutubeVideo__ctor_m1943750243 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void YoutubeVideo::Awake()
extern Il2CppClass* YoutubeVideo_t2077346616_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeVideo_Awake_m2181355462_MetadataUsageId;
extern "C"  void YoutubeVideo_Awake_m2181355462 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeVideo_Awake_m2181355462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((YoutubeVideo_t2077346616_StaticFields*)YoutubeVideo_t2077346616_il2cpp_TypeInfo_var->static_fields)->set_Instance_2(__this);
		return;
	}
}
// System.String YoutubeVideo::RequestVideo(System.String,System.Int32)
extern Il2CppClass* RemoteCertificateValidationCallback_t1894914657_il2cpp_TypeInfo_var;
extern Il2CppClass* ServicePointManager_t165502476_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1105416852_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4011336240_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern const MethodInfo* YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3871108831;
extern Il2CppCodeGenString* _stringLiteral2802109582;
extern const uint32_t YoutubeVideo_RequestVideo_m2780138341_MetadataUsageId;
extern "C"  String_t* YoutubeVideo_RequestVideo_m2780138341 (YoutubeVideo_t2077346616 * __this, String_t* ___urlOrId0, int32_t ___quality1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeVideo_RequestVideo_m2780138341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t1116831938 * V_0 = NULL;
	bool V_1 = false;
	Il2CppObject* V_2 = NULL;
	VideoInfo_t2099471191 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603_MethodInfo_var);
		RemoteCertificateValidationCallback_t1894914657 * L_1 = (RemoteCertificateValidationCallback_t1894914657 *)il2cpp_codegen_object_new(RemoteCertificateValidationCallback_t1894914657_il2cpp_TypeInfo_var);
		RemoteCertificateValidationCallback__ctor_m1684204841(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ServicePointManager_t165502476_il2cpp_TypeInfo_var);
		ServicePointManager_set_ServerCertificateValidationCallback_m1255728468(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___urlOrId0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		bool L_3 = Uri_TryCreate_m1126538084(NULL /*static, unused*/, L_2, 1, (&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0049;
		}
	}
	{
		Uri_t1116831938 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = Uri_get_Scheme_m2606456870(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		String_t* L_6 = ((Uri_t1116831938_StaticFields*)Uri_t1116831938_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeHttp_22();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0046;
		}
	}
	{
		Uri_t1116831938 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = Uri_get_Scheme_m2606456870(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		String_t* L_10 = ((Uri_t1116831938_StaticFields*)Uri_t1116831938_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeHttps_23();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_0047;
	}

IL_0046:
	{
		G_B4_0 = 1;
	}

IL_0047:
	{
		G_B6_0 = G_B4_0;
		goto IL_004a;
	}

IL_0049:
	{
		G_B6_0 = 0;
	}

IL_004a:
	{
		V_1 = (bool)G_B6_0;
		bool L_12 = V_1;
		if (L_12)
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_13 = ___urlOrId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3871108831, L_13, /*hidden argument*/NULL);
		___urlOrId0 = L_14;
	}

IL_005e:
	{
		String_t* L_15 = ___urlOrId0;
		Il2CppObject* L_16 = DownloadUrlResolver_GetDownloadUrls_m3448163278(NULL /*static, unused*/, L_15, (bool)0, /*hidden argument*/NULL);
		V_2 = L_16;
		V_3 = (VideoInfo_t2099471191 *)NULL;
		Il2CppObject* L_17 = V_2;
		NullCheck(L_17);
		Il2CppObject* L_18 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<YoutubeExtractor.VideoInfo>::GetEnumerator() */, IEnumerable_1_t1105416852_il2cpp_TypeInfo_var, L_17);
		V_4 = L_18;
		goto IL_00a6;
	}

IL_0075:
	{
		Il2CppObject* L_19 = V_4;
		NullCheck(L_19);
		VideoInfo_t2099471191 * L_20 = InterfaceFuncInvoker0< VideoInfo_t2099471191 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<YoutubeExtractor.VideoInfo>::get_Current() */, IEnumerator_1_t4011336240_il2cpp_TypeInfo_var, L_19);
		NullCheck(L_20);
		int32_t L_21 = VideoInfo_get_VideoType_m2599923248(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00a6;
		}
	}
	{
		Il2CppObject* L_22 = V_4;
		NullCheck(L_22);
		VideoInfo_t2099471191 * L_23 = InterfaceFuncInvoker0< VideoInfo_t2099471191 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<YoutubeExtractor.VideoInfo>::get_Current() */, IEnumerator_1_t4011336240_il2cpp_TypeInfo_var, L_22);
		NullCheck(L_23);
		int32_t L_24 = VideoInfo_get_Resolution_m3843029452(L_23, /*hidden argument*/NULL);
		int32_t L_25 = ___quality1;
		if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
		{
			goto IL_00a6;
		}
	}
	{
		Il2CppObject* L_26 = V_4;
		NullCheck(L_26);
		VideoInfo_t2099471191 * L_27 = InterfaceFuncInvoker0< VideoInfo_t2099471191 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<YoutubeExtractor.VideoInfo>::get_Current() */, IEnumerator_1_t4011336240_il2cpp_TypeInfo_var, L_26);
		V_3 = L_27;
		goto IL_00b2;
	}

IL_00a6:
	{
		Il2CppObject* L_28 = V_4;
		NullCheck(L_28);
		bool L_29 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_28);
		if (L_29)
		{
			goto IL_0075;
		}
	}

IL_00b2:
	{
		VideoInfo_t2099471191 * L_30 = V_3;
		NullCheck(L_30);
		bool L_31 = VideoInfo_get_RequiresDecryption_m4226650415(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00c3;
		}
	}
	{
		VideoInfo_t2099471191 * L_32 = V_3;
		DownloadUrlResolver_DecryptDownloadUrl_m1994299011(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		VideoInfo_t2099471191 * L_33 = V_3;
		NullCheck(L_33);
		String_t* L_34 = VideoInfo_get_DownloadUrl_m2189851804(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2802109582, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		VideoInfo_t2099471191 * L_36 = V_3;
		NullCheck(L_36);
		String_t* L_37 = VideoInfo_get_DownloadUrl_m2189851804(L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// System.Boolean YoutubeVideo::MyRemoteCertificateValidationCallback(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern Il2CppClass* X509Certificate2_t160474609_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603_MetadataUsageId;
extern "C"  bool YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603 (YoutubeVideo_t2077346616 * __this, Il2CppObject * ___sender0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeVideo_MyRemoteCertificateValidationCallback_m949573603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___sslPolicyErrors3;
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		V_1 = 0;
		goto IL_007c;
	}

IL_0010:
	{
		X509Chain_t1111884825 * L_1 = ___chain2;
		NullCheck(L_1);
		X509ChainStatusU5BU5D_t2899776074* L_2 = X509Chain_get_ChainStatus_m3128438893(L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = X509ChainStatus_get_Status_m805216413(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)64))))
		{
			goto IL_0078;
		}
	}
	{
		X509Chain_t1111884825 * L_5 = ___chain2;
		NullCheck(L_5);
		X509ChainPolicy_t676713451 * L_6 = X509Chain_get_ChainPolicy_m3141824143(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509ChainPolicy_set_RevocationFlag_m1110915148(L_6, 1, /*hidden argument*/NULL);
		X509Chain_t1111884825 * L_7 = ___chain2;
		NullCheck(L_7);
		X509ChainPolicy_t676713451 * L_8 = X509Chain_get_ChainPolicy_m3141824143(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		X509ChainPolicy_set_RevocationMode_m3262748012(L_8, 1, /*hidden argument*/NULL);
		X509Chain_t1111884825 * L_9 = ___chain2;
		NullCheck(L_9);
		X509ChainPolicy_t676713451 * L_10 = X509Chain_get_ChainPolicy_m3141824143(L_9, /*hidden argument*/NULL);
		TimeSpan_t413522987  L_11;
		memset(&L_11, 0, sizeof(L_11));
		TimeSpan__ctor_m4160332047(&L_11, 0, 1, 0, /*hidden argument*/NULL);
		NullCheck(L_10);
		X509ChainPolicy_set_UrlRetrievalTimeout_m2952957641(L_10, L_11, /*hidden argument*/NULL);
		X509Chain_t1111884825 * L_12 = ___chain2;
		NullCheck(L_12);
		X509ChainPolicy_t676713451 * L_13 = X509Chain_get_ChainPolicy_m3141824143(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		X509ChainPolicy_set_VerificationFlags_m1465140514(L_13, ((int32_t)4095), /*hidden argument*/NULL);
		X509Chain_t1111884825 * L_14 = ___chain2;
		X509Certificate_t3076817455 * L_15 = ___certificate1;
		NullCheck(L_14);
		bool L_16 = X509Chain_Build_m2593695788(L_14, ((X509Certificate2_t160474609 *)CastclassClass(L_15, X509Certificate2_t160474609_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_16;
		bool L_17 = V_2;
		if (L_17)
		{
			goto IL_0078;
		}
	}
	{
		V_0 = (bool)0;
	}

IL_0078:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_19 = V_1;
		X509Chain_t1111884825 * L_20 = ___chain2;
		NullCheck(L_20);
		X509ChainStatusU5BU5D_t2899776074* L_21 = X509Chain_get_ChainStatus_m3128438893(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0010;
		}
	}

IL_008a:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void YoutubeVideo::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern const uint32_t YoutubeVideo_OnGUI_m1439148893_MetadataUsageId;
extern "C"  void YoutubeVideo_OnGUI_m1439148893 (YoutubeVideo_t2077346616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YoutubeVideo_OnGUI_m1439148893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_set_depth_m4181267379(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		bool L_0 = __this->get_drawBackground_3();
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m3291325233(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		Texture2D_t3884108195 * L_4 = __this->get_backgroundImage_4();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m418809280(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
