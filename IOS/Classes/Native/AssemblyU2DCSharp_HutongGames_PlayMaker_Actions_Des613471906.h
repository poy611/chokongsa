﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyObjects
struct  DestroyObjects_t613471906  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.DestroyObjects::gameObjects
	FsmArray_t2129666875 * ___gameObjects_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DestroyObjects::delay
	FsmFloat_t2134102846 * ___delay_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroyObjects::detachChildren
	FsmBool_t1075959796 * ___detachChildren_13;

public:
	inline static int32_t get_offset_of_gameObjects_11() { return static_cast<int32_t>(offsetof(DestroyObjects_t613471906, ___gameObjects_11)); }
	inline FsmArray_t2129666875 * get_gameObjects_11() const { return ___gameObjects_11; }
	inline FsmArray_t2129666875 ** get_address_of_gameObjects_11() { return &___gameObjects_11; }
	inline void set_gameObjects_11(FsmArray_t2129666875 * value)
	{
		___gameObjects_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjects_11, value);
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(DestroyObjects_t613471906, ___delay_12)); }
	inline FsmFloat_t2134102846 * get_delay_12() const { return ___delay_12; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(FsmFloat_t2134102846 * value)
	{
		___delay_12 = value;
		Il2CppCodeGenWriteBarrier(&___delay_12, value);
	}

	inline static int32_t get_offset_of_detachChildren_13() { return static_cast<int32_t>(offsetof(DestroyObjects_t613471906, ___detachChildren_13)); }
	inline FsmBool_t1075959796 * get_detachChildren_13() const { return ___detachChildren_13; }
	inline FsmBool_t1075959796 ** get_address_of_detachChildren_13() { return &___detachChildren_13; }
	inline void set_detachChildren_13(FsmBool_t1075959796 * value)
	{
		___detachChildren_13 = value;
		Il2CppCodeGenWriteBarrier(&___detachChildren_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
