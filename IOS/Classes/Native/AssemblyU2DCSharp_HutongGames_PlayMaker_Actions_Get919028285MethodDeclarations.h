﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetStringRight
struct GetStringRight_t919028285;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetStringRight::.ctor()
extern "C"  void GetStringRight__ctor_m2505439945 (GetStringRight_t919028285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::Reset()
extern "C"  void GetStringRight_Reset_m151872886 (GetStringRight_t919028285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnEnter()
extern "C"  void GetStringRight_OnEnter_m1937829984 (GetStringRight_t919028285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnUpdate()
extern "C"  void GetStringRight_OnUpdate_m3371714019 (GetStringRight_t919028285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::DoGetStringRight()
extern "C"  void GetStringRight_DoGetStringRight_m4250366747 (GetStringRight_t919028285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
