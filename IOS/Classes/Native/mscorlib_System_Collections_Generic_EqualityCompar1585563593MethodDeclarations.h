﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct DefaultComparer_t1585563593;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor()
extern "C"  void DefaultComparer__ctor_m2700042476_gshared (DefaultComparer_t1585563593 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2700042476(__this, method) ((  void (*) (DefaultComparer_t1585563593 *, const MethodInfo*))DefaultComparer__ctor_m2700042476_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2161838911_gshared (DefaultComparer_t1585563593 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2161838911(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1585563593 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2161838911_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1998743741_gshared (DefaultComparer_t1585563593 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1998743741(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1585563593 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1998743741_gshared)(__this, ___x0, ___y1, method)
