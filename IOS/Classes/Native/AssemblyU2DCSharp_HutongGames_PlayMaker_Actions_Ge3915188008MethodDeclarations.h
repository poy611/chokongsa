﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
struct GetAnimatorHumanScale_t3915188008;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::.ctor()
extern "C"  void GetAnimatorHumanScale__ctor_m3985010446 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::Reset()
extern "C"  void GetAnimatorHumanScale_Reset_m1631443387 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::OnEnter()
extern "C"  void GetAnimatorHumanScale_OnEnter_m2170906469 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::DoGetHumanScale()
extern "C"  void GetAnimatorHumanScale_DoGetHumanScale_m527444404 (GetAnimatorHumanScale_t3915188008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
