﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrackMouseMovement
struct TrackMouseMovement_t1693767945;

#include "codegen/il2cpp-codegen.h"

// System.Void TrackMouseMovement::.ctor()
extern "C"  void TrackMouseMovement__ctor_m2636792242 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackMouseMovement::Start()
extern "C"  void TrackMouseMovement_Start_m1583930034 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackMouseMovement::Update()
extern "C"  void TrackMouseMovement_Update_m1863042971 (TrackMouseMovement_t1693767945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
