﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2825067351.h"
#include "UnityEngine_UnityEngine_WrapMode1491636113.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode23503924.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimationSettings
struct  AnimationSettings_t3134957589  : public BaseAnimationAction_t2825067351
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimationSettings::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimationSettings::animName
	FsmString_t952858651 * ___animName_14;
	// UnityEngine.WrapMode HutongGames.PlayMaker.Actions.AnimationSettings::wrapMode
	int32_t ___wrapMode_15;
	// UnityEngine.AnimationBlendMode HutongGames.PlayMaker.Actions.AnimationSettings::blendMode
	int32_t ___blendMode_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimationSettings::speed
	FsmFloat_t2134102846 * ___speed_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimationSettings::layer
	FsmInt_t1596138449 * ___layer_18;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___gameObject_13)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_animName_14() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___animName_14)); }
	inline FsmString_t952858651 * get_animName_14() const { return ___animName_14; }
	inline FsmString_t952858651 ** get_address_of_animName_14() { return &___animName_14; }
	inline void set_animName_14(FsmString_t952858651 * value)
	{
		___animName_14 = value;
		Il2CppCodeGenWriteBarrier(&___animName_14, value);
	}

	inline static int32_t get_offset_of_wrapMode_15() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___wrapMode_15)); }
	inline int32_t get_wrapMode_15() const { return ___wrapMode_15; }
	inline int32_t* get_address_of_wrapMode_15() { return &___wrapMode_15; }
	inline void set_wrapMode_15(int32_t value)
	{
		___wrapMode_15 = value;
	}

	inline static int32_t get_offset_of_blendMode_16() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___blendMode_16)); }
	inline int32_t get_blendMode_16() const { return ___blendMode_16; }
	inline int32_t* get_address_of_blendMode_16() { return &___blendMode_16; }
	inline void set_blendMode_16(int32_t value)
	{
		___blendMode_16 = value;
	}

	inline static int32_t get_offset_of_speed_17() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___speed_17)); }
	inline FsmFloat_t2134102846 * get_speed_17() const { return ___speed_17; }
	inline FsmFloat_t2134102846 ** get_address_of_speed_17() { return &___speed_17; }
	inline void set_speed_17(FsmFloat_t2134102846 * value)
	{
		___speed_17 = value;
		Il2CppCodeGenWriteBarrier(&___speed_17, value);
	}

	inline static int32_t get_offset_of_layer_18() { return static_cast<int32_t>(offsetof(AnimationSettings_t3134957589, ___layer_18)); }
	inline FsmInt_t1596138449 * get_layer_18() const { return ___layer_18; }
	inline FsmInt_t1596138449 ** get_address_of_layer_18() { return &___layer_18; }
	inline void set_layer_18(FsmInt_t1596138449 * value)
	{
		___layer_18 = value;
		Il2CppCodeGenWriteBarrier(&___layer_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
