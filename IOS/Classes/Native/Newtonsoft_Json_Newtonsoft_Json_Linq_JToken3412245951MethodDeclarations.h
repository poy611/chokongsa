﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2418191612;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1069135460;
// System.Uri
struct Uri_t1116831938;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.Linq.JsonLoadSettings
struct JsonLoadSettings_t1368013569;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JEnumerable_1_213262205.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting732683613.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen2946749061.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen1237964965.h"
#include "mscorlib_System_Nullable_1_gen108794446.h"
#include "mscorlib_System_Nullable_1_gen2946736183.h"
#include "mscorlib_System_Nullable_1_gen1245896300.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Nullable_1_gen1237965118.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Nullable_1_gen108794504.h"
#include "mscorlib_System_Nullable_1_gen108794599.h"
#include "mscorlib_System_Guid2862754429.h"
#include "mscorlib_System_Nullable_1_gen2946880952.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader816925123.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonLoadSetti1368013569.h"

// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::get_Parent()
extern "C"  JContainer_t3364442311 * JToken_get_Parent_m3280818908 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Parent(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JToken_set_Parent_m2373136271 (JToken_t3412245951 * __this, JContainer_t3364442311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Root()
extern "C"  JToken_t3412245951 * JToken_get_Root_m1391718988 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Next()
extern "C"  JToken_t3412245951 * JToken_get_Next_m1268235293 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Next(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_set_Next_m3522615534 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Previous()
extern "C"  JToken_t3412245951 * JToken_get_Previous_m2584829601 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Previous(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_set_Previous_m1842699114 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::get_Path()
extern "C"  String_t* JToken_get_Path_m1566172343 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::.ctor()
extern "C"  void JToken__ctor_m3098100152 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::AncestorsAndSelf()
extern "C"  Il2CppObject* JToken_AncestorsAndSelf_m2628712603 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::GetAncestors(System.Boolean)
extern "C"  Il2CppObject* JToken_GetAncestors_m1146784249 (JToken_t3412245951 * __this, bool ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JToken_get_Item_m876095607 (JToken_t3412245951 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_First()
extern "C"  JToken_t3412245951 * JToken_get_First_m2259512296 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Last()
extern "C"  JToken_t3412245951 * JToken_get_Last_m1207133952 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Children()
extern "C"  JEnumerable_1_t213262205  JToken_Children_m4304224 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::Remove()
extern "C"  void JToken_Remove_m1443267920 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::Replace(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_Replace_m1846142112 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ToString()
extern "C"  String_t* JToken_ToString_m870427701 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ToString(Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JToken_ToString_m1181508034 (JToken_t3412245951 * __this, int32_t ___formatting0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JToken::EnsureValue(Newtonsoft.Json.Linq.JToken)
extern "C"  JValue_t3413677367 * JToken_EnsureValue_m2515155694 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::GetType(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JToken_GetType_m1927077025 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JTokenType[],System.Boolean)
extern "C"  bool JToken_ValidateToken_m881793582 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, JTokenTypeU5BU5D_t1069135460* ___validTypes1, bool ___nullable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_op_Explicit_m3590141016 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  DateTimeOffset_t3884714306  JToken_op_Explicit_m1286780860 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t560925241  JToken_op_Explicit_m2258764953 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int64_t JToken_op_Explicit_m3477684125 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t72820554  JToken_op_Explicit_m3719586750 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t3968840829  JToken_op_Explicit_m2018825963 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t2038477154  JToken_op_Explicit_m3412424016 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t3952353088  JToken_op_Explicit_m1587076552 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Char> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t2946749061  JToken_op_Explicit_m1243200931 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JToken_op_Explicit_m2182199934 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int16_t JToken_op_Explicit_m170597828 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint16_t JToken_op_Explicit_m3823811789 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Il2CppChar JToken_op_Explicit_m2135983044 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint8_t JToken_op_Explicit_m3204264950 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int8_t JToken_op_Explicit_m895269451 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237965023  JToken_op_Explicit_m3453346931 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237964965  JToken_op_Explicit_m1223223789 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794446  JToken_op_Explicit_m1133997818 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Byte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t2946736183  JToken_op_Explicit_m201649 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.SByte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1245896300  JToken_op_Explicit_m2213207622 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  DateTime_t4283661327  JToken_op_Explicit_m3027353801 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237965118  JToken_op_Explicit_m663683892 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t81078199  JToken_op_Explicit_m4170770801 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Decimal_t1954350631  JToken_op_Explicit_m4181545153 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794504  JToken_op_Explicit_m3364120960 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794599  JToken_op_Explicit_m574457921 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  double JToken_op_Explicit_m2730048575 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  float JToken_op_Explicit_m1982109558 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JToken_op_Explicit_m1902702719 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint32_t JToken_op_Explicit_m1540446599 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint64_t JToken_op_Explicit_m2835930790 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Guid_t2862754429  JToken_op_Explicit_m2862677175 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Guid> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t2946880952  JToken_op_Explicit_m2295882512 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  TimeSpan_t413522987  JToken_op_Explicit_m3024337893 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.TimeSpan> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t497649510  JToken_op_Explicit_m3626093602 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Uri_t1116831938 * JToken_op_Explicit_m3721317148 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.DateTime)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m1336026772 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.String)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2928834142 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JToken_System_Collections_IEnumerable_GetEnumerator_m2914219059 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m115364802 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReader Newtonsoft.Json.Linq.JToken::CreateReader()
extern "C"  JsonReader_t816925123 * JToken_CreateReader_m1032825912 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type)
extern "C"  Il2CppObject * JToken_ToObject_m124813768 (JToken_t3412245951 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type,Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * JToken_ToObject_m1662963055 (JToken_t3412245951 * __this, Type_t * ___objectType0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JToken_ReadFrom_m1833695777 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern "C"  JToken_t3412245951 * JToken_ReadFrom_m4225406595 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, JsonLoadSettings_t1368013569 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(Newtonsoft.Json.IJsonLineInfo,Newtonsoft.Json.Linq.JsonLoadSettings)
extern "C"  void JToken_SetLineInfo_m3378944156 (JToken_t3412245951 * __this, Il2CppObject * ___lineInfo0, JsonLoadSettings_t1368013569 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(System.Int32,System.Int32)
extern "C"  void JToken_SetLineInfo_m970747148 (JToken_t3412245951 * __this, int32_t ___lineNumber0, int32_t ___linePosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern "C"  bool JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m3368444332 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern "C"  int32_t JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m2173314942 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern "C"  int32_t JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m2976145502 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::System.ICloneable.Clone()
extern "C"  Il2CppObject * JToken_System_ICloneable_Clone_m129198439 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::DeepClone()
extern "C"  JToken_t3412245951 * JToken_DeepClone_m3348727986 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::AddAnnotation(System.Object)
extern "C"  void JToken_AddAnnotation_m3348138702 (JToken_t3412245951 * __this, Il2CppObject * ___annotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::.cctor()
extern "C"  void JToken__cctor_m1069727989 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
