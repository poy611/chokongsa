﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response419677757.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonSt671203459.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus427705392.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3670871388.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSou3931761423.h"

// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.Native.ConversionUtils::ConvertResponseStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern "C"  int32_t ConversionUtils_ConvertResponseStatus_m2566392437 (Il2CppObject * __this /* static, unused */, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.CommonStatusCodes GooglePlayGames.Native.ConversionUtils::ConvertResponseStatusToCommonStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern "C"  int32_t ConversionUtils_ConvertResponseStatusToCommonStatus_m3380200457 (Il2CppObject * __this /* static, unused */, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.UIStatus GooglePlayGames.Native.ConversionUtils::ConvertUIStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C"  int32_t ConversionUtils_ConvertUIStatus_m3767976840 (Il2CppObject * __this /* static, unused */, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/DataSource GooglePlayGames.Native.ConversionUtils::AsDataSource(GooglePlayGames.BasicApi.DataSource)
extern "C"  int32_t ConversionUtils_AsDataSource_m57437478 (Il2CppObject * __this /* static, unused */, int32_t ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
