﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C"  void HandleRef__ctor_m3670859955 (HandleRef_t1780819301 * __this, Il2CppObject * ___wrapper0, IntPtr_t ___handle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C"  IntPtr_t HandleRef_get_Handle_m2336437435 (HandleRef_t1780819301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::ToIntPtr(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t HandleRef_ToIntPtr_m4006420033 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
