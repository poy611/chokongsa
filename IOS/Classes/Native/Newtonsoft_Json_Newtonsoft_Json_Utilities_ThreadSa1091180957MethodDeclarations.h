﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ThreadSa2874570697MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m3084732379(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t1091180957 *, Func_2_t2696141581 *, const MethodInfo*))ThreadSafeStore_2__ctor_m2250270936_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>::Get(TKey)
#define ThreadSafeStore_2_Get_m2410823809(__this, ___key0, method) ((  BidirectionalDictionary_2_t157076046 * (*) (ThreadSafeStore_2_t1091180957 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_Get_m4035272722_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1909500593(__this, ___key0, method) ((  BidirectionalDictionary_2_t157076046 * (*) (ThreadSafeStore_2_t1091180957 *, Type_t *, const MethodInfo*))ThreadSafeStore_2_AddValue_m2055708096_gshared)(__this, ___key0, method)
