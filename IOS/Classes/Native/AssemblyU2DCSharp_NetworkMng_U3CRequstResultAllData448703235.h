﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UserInfoCall
struct UserInfoCall_t3868783927;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// NetworkMng
struct NetworkMng_t1515215352;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15
struct  U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235  : public Il2CppObject
{
public:
	// UserInfoCall NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::<user>__0
	UserInfoCall_t3868783927 * ___U3CuserU3E__0_0;
	// System.Int32 NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::startNum
	int32_t ___startNum_1;
	// System.Int32 NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::endNum
	int32_t ___endNum_2;
	// System.String NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::<str>__1
	String_t* ___U3CstrU3E__1_3;
	// System.Int32 NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::$PC
	int32_t ___U24PC_4;
	// System.Object NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::$current
	Il2CppObject * ___U24current_5;
	// System.Int32 NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::<$>startNum
	int32_t ___U3CU24U3EstartNum_6;
	// System.Int32 NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::<$>endNum
	int32_t ___U3CU24U3EendNum_7;
	// NetworkMng NetworkMng/<RequstResultAllDataCouroutine>c__Iterator15::<>f__this
	NetworkMng_t1515215352 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CuserU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U3CuserU3E__0_0)); }
	inline UserInfoCall_t3868783927 * get_U3CuserU3E__0_0() const { return ___U3CuserU3E__0_0; }
	inline UserInfoCall_t3868783927 ** get_address_of_U3CuserU3E__0_0() { return &___U3CuserU3E__0_0; }
	inline void set_U3CuserU3E__0_0(UserInfoCall_t3868783927 * value)
	{
		___U3CuserU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuserU3E__0_0, value);
	}

	inline static int32_t get_offset_of_startNum_1() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___startNum_1)); }
	inline int32_t get_startNum_1() const { return ___startNum_1; }
	inline int32_t* get_address_of_startNum_1() { return &___startNum_1; }
	inline void set_startNum_1(int32_t value)
	{
		___startNum_1 = value;
	}

	inline static int32_t get_offset_of_endNum_2() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___endNum_2)); }
	inline int32_t get_endNum_2() const { return ___endNum_2; }
	inline int32_t* get_address_of_endNum_2() { return &___endNum_2; }
	inline void set_endNum_2(int32_t value)
	{
		___endNum_2 = value;
	}

	inline static int32_t get_offset_of_U3CstrU3E__1_3() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U3CstrU3E__1_3)); }
	inline String_t* get_U3CstrU3E__1_3() const { return ___U3CstrU3E__1_3; }
	inline String_t** get_address_of_U3CstrU3E__1_3() { return &___U3CstrU3E__1_3; }
	inline void set_U3CstrU3E__1_3(String_t* value)
	{
		___U3CstrU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EstartNum_6() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U3CU24U3EstartNum_6)); }
	inline int32_t get_U3CU24U3EstartNum_6() const { return ___U3CU24U3EstartNum_6; }
	inline int32_t* get_address_of_U3CU24U3EstartNum_6() { return &___U3CU24U3EstartNum_6; }
	inline void set_U3CU24U3EstartNum_6(int32_t value)
	{
		___U3CU24U3EstartNum_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EendNum_7() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U3CU24U3EendNum_7)); }
	inline int32_t get_U3CU24U3EendNum_7() const { return ___U3CU24U3EendNum_7; }
	inline int32_t* get_address_of_U3CU24U3EendNum_7() { return &___U3CU24U3EendNum_7; }
	inline void set_U3CU24U3EendNum_7(int32_t value)
	{
		___U3CU24U3EendNum_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CRequstResultAllDataCouroutineU3Ec__Iterator15_t448703235, ___U3CU3Ef__this_8)); }
	inline NetworkMng_t1515215352 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline NetworkMng_t1515215352 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(NetworkMng_t1515215352 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
