﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Serving
struct Serving_t3648806892;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// ResultData
struct ResultData_t1421128071;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultData1421128071.h"

// System.Void Serving::.ctor()
extern "C"  void Serving__ctor_m953940223 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::Start()
extern "C"  void Serving_Start_m4196045311 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Serving::onStart()
extern "C"  Il2CppObject * Serving_onStart_m3673233544 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Serving::RightText(System.Single)
extern "C"  int32_t Serving_RightText_m270046167 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Serving::afterText(System.Int32,System.Int32)
extern "C"  String_t* Serving_afterText_m3826407805 (Serving_t3648806892 * __this, int32_t ____randomNum0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::OnAllResult(ResultData)
extern "C"  void Serving_OnAllResult_m3087523573 (Serving_t3648806892 * __this, ResultData_t1421128071 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::OnAllResultFail()
extern "C"  void Serving_OnAllResultFail_m3844952474 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Serving::ShowData()
extern "C"  Il2CppObject * Serving_ShowData_m444028324 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Serving::ShowmetheCoffeSizeUpCo()
extern "C"  Il2CppObject * Serving_ShowmetheCoffeSizeUpCo_m2400252450 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Serving::stringSwitch(System.Single)
extern "C"  String_t* Serving_stringSwitch_m3488818404 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Serving::ImageSwitch(System.Single)
extern "C"  String_t* Serving_ImageSwitch_m88420956 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::csSwitch(System.Single)
extern "C"  void Serving_csSwitch_m1901630274 (Serving_t3648806892 * __this, float ___cs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::juSwitch(System.Single)
extern "C"  void Serving_juSwitch_m2243901767 (Serving_t3648806892 * __this, float ___ju0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::OnDestroy()
extern "C"  void Serving_OnDestroy_m2549823096 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::OkClick()
extern "C"  void Serving_OkClick_m2819261737 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Serving::okNet()
extern "C"  Il2CppObject * Serving_okNet_m3019028070 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving::OnSave()
extern "C"  void Serving_OnSave_m1400634465 (Serving_t3648806892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
