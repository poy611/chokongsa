﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GradeData[]
struct GradeDataU5BU5D_t1462969052;
// GradeDataInfo
struct GradeDataInfo_t1480749135;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GradeDataInfo
struct  GradeDataInfo_t1480749135  : public Il2CppObject
{
public:
	// GradeData[] GradeDataInfo::grade_data
	GradeDataU5BU5D_t1462969052* ___grade_data_0;

public:
	inline static int32_t get_offset_of_grade_data_0() { return static_cast<int32_t>(offsetof(GradeDataInfo_t1480749135, ___grade_data_0)); }
	inline GradeDataU5BU5D_t1462969052* get_grade_data_0() const { return ___grade_data_0; }
	inline GradeDataU5BU5D_t1462969052** get_address_of_grade_data_0() { return &___grade_data_0; }
	inline void set_grade_data_0(GradeDataU5BU5D_t1462969052* value)
	{
		___grade_data_0 = value;
		Il2CppCodeGenWriteBarrier(&___grade_data_0, value);
	}
};

struct GradeDataInfo_t1480749135_StaticFields
{
public:
	// GradeDataInfo GradeDataInfo::instance
	GradeDataInfo_t1480749135 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(GradeDataInfo_t1480749135_StaticFields, ___instance_1)); }
	inline GradeDataInfo_t1480749135 * get_instance_1() const { return ___instance_1; }
	inline GradeDataInfo_t1480749135 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(GradeDataInfo_t1480749135 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
