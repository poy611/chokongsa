﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RotateCamera
struct CFX_Demo_RotateCamera_t3034654674;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RotateCamera::.ctor()
extern "C"  void CFX_Demo_RotateCamera__ctor_m270564825 (CFX_Demo_RotateCamera_t3034654674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RotateCamera::.cctor()
extern "C"  void CFX_Demo_RotateCamera__cctor_m3610446068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RotateCamera::Update()
extern "C"  void CFX_Demo_RotateCamera_Update_m1524437076 (CFX_Demo_RotateCamera_t3034654674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
