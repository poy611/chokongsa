﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections
struct NetworkGetMaximumConnections_t1349448613;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::.ctor()
extern "C"  void NetworkGetMaximumConnections__ctor_m4245935201 (NetworkGetMaximumConnections_t1349448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::Reset()
extern "C"  void NetworkGetMaximumConnections_Reset_m1892368142 (NetworkGetMaximumConnections_t1349448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::OnEnter()
extern "C"  void NetworkGetMaximumConnections_OnEnter_m3811492856 (NetworkGetMaximumConnections_t1349448613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
