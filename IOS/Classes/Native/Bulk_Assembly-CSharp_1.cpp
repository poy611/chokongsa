﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
struct ShowAllUICallback_t766686189;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
struct OnAuthActionStartedCallback_t2378408244;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback
struct OnLogCallback_t508427221;
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
struct OnAuthActionFinishedCallback_t1172336457;
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
struct OnTurnBasedMatchEventCallback_t2064424764;
// GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback
struct OnQuestCompletedCallback_t2055592770;
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
struct OnMultiplayerInvitationEventCallback_t3993607552;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
struct FetchAllCallback_t213806343;
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
struct FetchCallback_t1493526500;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback
struct FlushCallback_t3126222379;
// GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback
struct FetchServerAuthCodeCallback_t1082452761;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback
struct FetchAllCallback_t3766146474;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
struct FetchScoreSummaryCallback_t1723193589;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
struct ShowAllUICallback_t3664260959;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback
struct FetchScorePageCallback_t1423453514;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback
struct FetchAllScoreSummariesCallback_t1010253660;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
struct ShowUICallback_t1832555764;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback
struct FetchCallback_t2463332897;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback
struct FetchListCallback_t2067692889;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback
struct FetchCallback_t1233224091;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
struct FetchSelfCallback_t2479296007;
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
struct FetchListCallback_t3150025178;
// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
struct AcceptCallback_t1408209808;
// GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback
struct QuestUICallback_t3097297816;
// GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback
struct ClaimMilestoneCallback_t1411568668;
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback
struct FetchCallback_t1576861340;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
struct OnParticipantStatusChangedCallback_t2709967058;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
struct OnP2PDisconnectedCallback_t401589328;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
struct OnDataReceivedCallback_t176853902;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
struct OnRoomStatusChangedCallback_t1130665710;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
struct OnP2PConnectedCallback_t507387838;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
struct OnRoomConnectedSetChangedCallback_t3651478055;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback
struct RealTimeRoomCallback_t3285480827;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback
struct LeaveRoomCallback_t3947593351;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback
struct WaitingRoomUICallback_t206291377;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback
struct PlayerSelectUICallback_t2633669638;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback
struct SendReliableMessageCallback_t2145488650;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback
struct FetchInvitationsCallback_t1518378837;
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback
struct RoomInboxUICallback_t3659854740;
// GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback
struct FetchAllCallback_t677738899;
// GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback
struct SnapshotSelectUICallback_t2531066752;
// GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback
struct ReadCallback_t1569945378;
// GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback
struct CommitCallback_t1623255043;
// GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback
struct OpenCallback_t1210339286;
// GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback
struct FetchForPlayerCallback_t1995639285;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
struct PlayerSelectUICallback_t4277124957;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback
struct MultiplayerStatusCallback_t994271114;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback
struct MatchInboxUICallback_t3070391457;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
struct TurnBasedMatchCallback_t3800683771;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback
struct TurnBasedMatchesCallback_t1856996905;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_A766686189.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_A766686189MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Void2863195528.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Bu44137017.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Bu44137017MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2378408244.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_B508427221.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3195991383.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1172336457.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2064424764.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2055592770.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3993607552.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1172336457MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1624738888.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_C803408059.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2378408244MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_B508427221MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3993607552MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2613789975.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2055592770MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2064424764MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3746182413.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3746182413MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_C803408059MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1223161653.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1223161653MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1491989755.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1491989755MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1924926087.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1924926087MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2173406145.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2173406145MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4152150264.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4152150264MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4260498101.h"
#include "mscorlib_System_Boolean476798718.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_E824422131.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_E824422131MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3670871388.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_E213806343.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1493526500.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_E213806343MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1493526500MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_G615344560.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_G615344560MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3126222379.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1082452761.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1082452761MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3126222379MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4128599633.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4128599633MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1480377078.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1480377078MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1565207195.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1565207195MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4142477704.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3186999280.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3186999280MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3766146474.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2241332029.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3906033828.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1723193589.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4146228892.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3664260959.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1423453514.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1010253660.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1832555764.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2463332897.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3766146474MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1010253660MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2463332897MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1423453514MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1723193589MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3664260959MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1832555764MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2018903281.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2018903281MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2590224534.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1278486393.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1278486393MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4163934672.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2938557483.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3140053955.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3140053955MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_P923675361.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_P923675361MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1516569834.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1516569834MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2067692889.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1233224091.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2479296007.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1233224091MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2067692889MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2479296007MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3817316700.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3817316700MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4163202880.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4163202880MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2963515846.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3723028395.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3723028395MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3150025178.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1408209808.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3097297816.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1411568668.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1576861340.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1408209808MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1411568668MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1576861340MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3150025178MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3097297816MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2071018926.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2071018926MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3579266924.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2183308367.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2183308367MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2709967058.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R401589328.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R176853902.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1130665710.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R507387838.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3651478055.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R176853902MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R507387838MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R401589328MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2709967058MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3651478055MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1130665710MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2890974046.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2890974046MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3285480827.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3947593351.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R206291377.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2633669638.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2145488650.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1518378837.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3659854740.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1518378837MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3947593351MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2633669638MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3285480827MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3659854740MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2145488650MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R206291377MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2571637830.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2571637830MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3582821007.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R483779176.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_R483779176MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1368061809.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1368061809MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4164523248.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4164523248MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1240214303.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1240214303MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3038614516.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3038614516MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3900181421.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3900181421MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2815248615.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2815248615MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S677738899.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2531066752.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1569945378.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1623255043.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3763226879.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1210339286.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1623255043MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S677738899MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1210339286MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1569945378MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2531066752MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1550549971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1550549971MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2949661859.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2949661859MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1969466582.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1969466582MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3013934414.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3013934414MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1995639285.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1995639285MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1016946674.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1016946674MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1471843510.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1471843510MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S719253683.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S719253683MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S469824154.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S469824154MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1534922096.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1534922096MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S351539360.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_S351539360MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2425532716.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2425532716MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2576600719.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2576600719MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1179182268.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1179182268MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1448608642.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1448608642MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1480171629.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1480171629MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T340021551.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T340021551MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2980502528.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2252410001.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2252410001MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T506248488.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T506248488MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3921466855.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3921466855MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4277124957.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_T994271114.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3070391457.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3800683771.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1856996905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ShowAllUICallback__ctor_m85166612 (ShowAllUICallback_t766686189 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C"  void ShowAllUICallback_Invoke_m1030609045 (ShowAllUICallback_t766686189 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ShowAllUICallback_Invoke_m1030609045((ShowAllUICallback_t766686189 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ShowAllUICallback_t766686189 (ShowAllUICallback_t766686189 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t3557407943_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t ShowAllUICallback_BeginInvoke_m328741922_MetadataUsageId;
extern "C"  Il2CppObject * ShowAllUICallback_BeginInvoke_m328741922 (ShowAllUICallback_t766686189 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowAllUICallback_BeginInvoke_m328741922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t3557407943_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void ShowAllUICallback_EndInvoke_m3041338916 (ShowAllUICallback_t766686189 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnAuthActionStarted(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnAuthActionStarted(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback,System.IntPtr)
extern "C"  void Builder_GameServices_Builder_SetOnAuthActionStarted_m4187361799 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnAuthActionStartedCallback_t2378408244 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnAuthActionStarted)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL GameServices_Builder_AddOauthScope(void*, char*);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_AddOauthScope(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void Builder_GameServices_Builder_AddOauthScope_m2987098391 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___scope1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___scope1' to native representation
	char* ____scope1_marshaled = NULL;
	____scope1_marshaled = il2cpp_codegen_marshal_string(___scope1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_AddOauthScope)(____self0_marshaled, ____scope1_marshaled);

	// Marshaling cleanup of parameter '___scope1' native representation
	il2cpp_codegen_marshal_free(____scope1_marshaled);
	____scope1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetLogging(void*, Il2CppMethodPointer, intptr_t, int32_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetLogging(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback,System.IntPtr,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C"  void Builder_GameServices_Builder_SetLogging_m2472601294 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnLogCallback_t508427221 * ___callback1, IntPtr_t ___callback_arg2, int32_t ___min_level3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t, int32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetLogging)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()), ___min_level3);

}
extern "C" intptr_t DEFAULT_CALL GameServices_Builder_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Construct()
extern "C"  IntPtr_t Builder_GameServices_Builder_Construct_m4217393154 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(GameServices_Builder_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL GameServices_Builder_EnableSnapshots(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_EnableSnapshots(System.Runtime.InteropServices.HandleRef)
extern "C"  void Builder_GameServices_Builder_EnableSnapshots_m465060425 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_EnableSnapshots)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL GameServices_Builder_RequireGooglePlus(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_RequireGooglePlus(System.Runtime.InteropServices.HandleRef)
extern "C"  void Builder_GameServices_Builder_RequireGooglePlus_m896466261 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_RequireGooglePlus)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnLog(void*, Il2CppMethodPointer, intptr_t, int32_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnLog(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback,System.IntPtr,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C"  void Builder_GameServices_Builder_SetOnLog_m3808896840 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnLogCallback_t508427221 * ___callback1, IntPtr_t ___callback_arg2, int32_t ___min_level3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t, int32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnLog)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()), ___min_level3);

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetDefaultOnLog(void*, int32_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetDefaultOnLog(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/LogLevel)
extern "C"  void Builder_GameServices_Builder_SetDefaultOnLog_m152428 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___min_level1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetDefaultOnLog)(____self0_marshaled, ___min_level1);

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnAuthActionFinished(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnAuthActionFinished(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback,System.IntPtr)
extern "C"  void Builder_GameServices_Builder_SetOnAuthActionFinished_m2576883581 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnAuthActionFinishedCallback_t1172336457 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnAuthActionFinished)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnTurnBasedMatchEvent(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnTurnBasedMatchEvent(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback,System.IntPtr)
extern "C"  void Builder_GameServices_Builder_SetOnTurnBasedMatchEvent_m3955139847 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnTurnBasedMatchEventCallback_t2064424764 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnTurnBasedMatchEvent)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnQuestCompleted(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnQuestCompleted(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback,System.IntPtr)
extern "C"  void Builder_GameServices_Builder_SetOnQuestCompleted_m2611432715 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnQuestCompletedCallback_t2055592770 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnQuestCompleted)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL GameServices_Builder_SetOnMultiplayerInvitationEvent(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_SetOnMultiplayerInvitationEvent(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback,System.IntPtr)
extern "C"  void Builder_GameServices_Builder_SetOnMultiplayerInvitationEvent_m4175376015 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnMultiplayerInvitationEventCallback_t3993607552 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_SetOnMultiplayerInvitationEvent)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" intptr_t DEFAULT_CALL GameServices_Builder_Create(void*, intptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Create(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  IntPtr_t Builder_GameServices_Builder_Create_m2942984894 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___platform1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(GameServices_Builder_Create)(____self0_marshaled, reinterpret_cast<intptr_t>(___platform1.get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL GameServices_Builder_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Builder::GameServices_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Builder_GameServices_Builder_Dispose_m4050568892 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Builder_Dispose)(____self0_marshaled);

}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAuthActionFinishedCallback__ctor_m4069508976 (OnAuthActionFinishedCallback_t1172336457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr)
extern "C"  void OnAuthActionFinishedCallback_Invoke_m1598155865 (OnAuthActionFinishedCallback_t1172336457 * __this, int32_t ___arg00, int32_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnAuthActionFinishedCallback_Invoke_m1598155865((OnAuthActionFinishedCallback_t1172336457 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, int32_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, int32_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnAuthActionFinishedCallback_t1172336457 (OnAuthActionFinishedCallback_t1172336457 * __this, int32_t ___arg00, int32_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, ___arg11, reinterpret_cast<intptr_t>(___arg22.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* AuthOperation_t1624738888_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthStatus_t803408059_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnAuthActionFinishedCallback_BeginInvoke_m2438696022_MetadataUsageId;
extern "C"  Il2CppObject * OnAuthActionFinishedCallback_BeginInvoke_m2438696022 (OnAuthActionFinishedCallback_t1172336457 * __this, int32_t ___arg00, int32_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnAuthActionFinishedCallback_BeginInvoke_m2438696022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(AuthOperation_t1624738888_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(AuthStatus_t803408059_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnAuthActionFinishedCallback_EndInvoke_m2840621440 (OnAuthActionFinishedCallback_t1172336457 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAuthActionStartedCallback__ctor_m1826211083 (OnAuthActionStartedCallback_t2378408244 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr)
extern "C"  void OnAuthActionStartedCallback_Invoke_m1811538739 (OnAuthActionStartedCallback_t2378408244 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnAuthActionStartedCallback_Invoke_m1811538739((OnAuthActionStartedCallback_t2378408244 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnAuthActionStartedCallback_t2378408244 (OnAuthActionStartedCallback_t2378408244 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* AuthOperation_t1624738888_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnAuthActionStartedCallback_BeginInvoke_m3632451220_MetadataUsageId;
extern "C"  Il2CppObject * OnAuthActionStartedCallback_BeginInvoke_m3632451220 (OnAuthActionStartedCallback_t2378408244 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnAuthActionStartedCallback_BeginInvoke_m3632451220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AuthOperation_t1624738888_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnAuthActionStartedCallback_EndInvoke_m1398240411 (OnAuthActionStartedCallback_t2378408244 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLogCallback__ctor_m1297460524 (OnLogCallback_t508427221 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr)
extern "C"  void OnLogCallback_Invoke_m763351961 (OnLogCallback_t508427221 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnLogCallback_Invoke_m763351961((OnLogCallback_t508427221 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnLogCallback_t508427221 (OnLogCallback_t508427221 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___arg11' to native representation
	char* ____arg11_marshaled = NULL;
	____arg11_marshaled = il2cpp_codegen_marshal_string(___arg11);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, ____arg11_marshaled, reinterpret_cast<intptr_t>(___arg22.get_m_value_0()));

	// Marshaling cleanup of parameter '___arg11' native representation
	il2cpp_codegen_marshal_free(____arg11_marshaled);
	____arg11_marshaled = NULL;

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/LogLevel,System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* LogLevel_t3195991383_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnLogCallback_BeginInvoke_m4129920006_MetadataUsageId;
extern "C"  Il2CppObject * OnLogCallback_BeginInvoke_m4129920006 (OnLogCallback_t508427221 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnLogCallback_BeginInvoke_m4129920006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(LogLevel_t3195991383_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = ___arg11;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnLogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnLogCallback_EndInvoke_m394695996 (OnLogCallback_t508427221 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnMultiplayerInvitationEventCallback__ctor_m239009319 (OnMultiplayerInvitationEventCallback_t3993607552 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C"  void OnMultiplayerInvitationEventCallback_Invoke_m1142193506 (OnMultiplayerInvitationEventCallback_t3993607552 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnMultiplayerInvitationEventCallback_Invoke_m1142193506((OnMultiplayerInvitationEventCallback_t3993607552 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnMultiplayerInvitationEventCallback_t3993607552 (OnMultiplayerInvitationEventCallback_t3993607552 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*, intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___arg11' to native representation
	char* ____arg11_marshaled = NULL;
	____arg11_marshaled = il2cpp_codegen_marshal_string(___arg11);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, ____arg11_marshaled, reinterpret_cast<intptr_t>(___arg22.get_m_value_0()), reinterpret_cast<intptr_t>(___arg33.get_m_value_0()));

	// Marshaling cleanup of parameter '___arg11' native representation
	il2cpp_codegen_marshal_free(____arg11_marshaled);
	____arg11_marshaled = NULL;

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnMultiplayerInvitationEventCallback_BeginInvoke_m3175198395_MetadataUsageId;
extern "C"  Il2CppObject * OnMultiplayerInvitationEventCallback_BeginInvoke_m3175198395 (OnMultiplayerInvitationEventCallback_t3993607552 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnMultiplayerInvitationEventCallback_BeginInvoke_m3175198395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = ___arg11;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	__d_args[3] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg33);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnMultiplayerInvitationEventCallback_EndInvoke_m3768425399 (OnMultiplayerInvitationEventCallback_t3993607552 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnQuestCompletedCallback__ctor_m2413043433 (OnQuestCompletedCallback_t2055592770 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void OnQuestCompletedCallback_Invoke_m3086866559 (OnQuestCompletedCallback_t2055592770 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnQuestCompletedCallback_Invoke_m3086866559((OnQuestCompletedCallback_t2055592770 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnQuestCompletedCallback_t2055592770 (OnQuestCompletedCallback_t2055592770 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnQuestCompletedCallback_BeginInvoke_m2180135428_MetadataUsageId;
extern "C"  Il2CppObject * OnQuestCompletedCallback_BeginInvoke_m2180135428 (OnQuestCompletedCallback_t2055592770 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnQuestCompletedCallback_BeginInvoke_m2180135428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnQuestCompletedCallback_EndInvoke_m1238723961 (OnQuestCompletedCallback_t2055592770 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnTurnBasedMatchEventCallback__ctor_m3417684243 (OnTurnBasedMatchEventCallback_t2064424764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C"  void OnTurnBasedMatchEventCallback_Invoke_m227604302 (OnTurnBasedMatchEventCallback_t2064424764 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnTurnBasedMatchEventCallback_Invoke_m227604302((OnTurnBasedMatchEventCallback_t2064424764 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnTurnBasedMatchEventCallback_t2064424764 (OnTurnBasedMatchEventCallback_t2064424764 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*, intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___arg11' to native representation
	char* ____arg11_marshaled = NULL;
	____arg11_marshaled = il2cpp_codegen_marshal_string(___arg11);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, ____arg11_marshaled, reinterpret_cast<intptr_t>(___arg22.get_m_value_0()), reinterpret_cast<intptr_t>(___arg33.get_m_value_0()));

	// Marshaling cleanup of parameter '___arg11' native representation
	il2cpp_codegen_marshal_free(____arg11_marshaled);
	____arg11_marshaled = NULL;

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnTurnBasedMatchEventCallback_BeginInvoke_m466466847_MetadataUsageId;
extern "C"  Il2CppObject * OnTurnBasedMatchEventCallback_BeginInvoke_m466466847 (OnTurnBasedMatchEventCallback_t2064424764 * __this, int32_t ___arg00, String_t* ___arg11, IntPtr_t ___arg22, IntPtr_t ___arg33, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnTurnBasedMatchEventCallback_BeginInvoke_m466466847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(MultiplayerEvent_t2613789975_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = ___arg11;
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	__d_args[3] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg33);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnTurnBasedMatchEventCallback_EndInvoke_m1478405283 (OnTurnBasedMatchEventCallback_t2064424764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" uint64_t DEFAULT_CALL Event_Count(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.Event::Event_Count(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Event_Event_Count_m177444275 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Count)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Event_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Event_Event_Description_m1854641761 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Event_ImageUrl(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_ImageUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Event_Event_ImageUrl_m3523972717 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Event_ImageUrl)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL Event_Visibility(void*);
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility GooglePlayGames.Native.Cwrapper.Event::Event_Visibility(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t Event_Event_Visibility_m2720180936 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Visibility)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Event_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Event_Event_Id_m2278290822 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL Event_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Event::Event_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Event_Event_Valid_m3420675960 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL Event_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Event::Event_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Event_Event_Dispose_m4253961895 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Event_Dispose)(____self0_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Event_Copy(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Copy(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Event_Event_Copy_m3143938388 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Copy)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Event_Name(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Event::Event_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Event_Event_Name_m1171463574 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Event_Name)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL EventManager_FetchAll(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback,System.IntPtr)
extern "C"  void EventManager_EventManager_FetchAll_m3820558856 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchAllCallback_t213806343 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EventManager_FetchAll)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL EventManager_Fetch(void*, int32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback,System.IntPtr)
extern "C"  void EventManager_EventManager_Fetch_m494543538 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___event_id2, FetchCallback_t1493526500 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___event_id2' to native representation
	char* ____event_id2_marshaled = NULL;
	____event_id2_marshaled = il2cpp_codegen_marshal_string(___event_id2);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EventManager_Fetch)(____self0_marshaled, ___data_source1, ____event_id2_marshaled, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___event_id2' native representation
	il2cpp_codegen_marshal_free(____event_id2_marshaled);
	____event_id2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EventManager_Increment(void*, char*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Increment(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C"  void EventManager_EventManager_Increment_m410793165 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___event_id1, uint32_t ___steps2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___event_id1' to native representation
	char* ____event_id1_marshaled = NULL;
	____event_id1_marshaled = il2cpp_codegen_marshal_string(___event_id1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EventManager_Increment)(____self0_marshaled, ____event_id1_marshaled, ___steps2);

	// Marshaling cleanup of parameter '___event_id1' native representation
	il2cpp_codegen_marshal_free(____event_id1_marshaled);
	____event_id1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EventManager_FetchAllResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void EventManager_EventManager_FetchAllResponse_Dispose_m3264865846 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EventManager_FetchAllResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL EventManager_FetchAllResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t EventManager_EventManager_FetchAllResponse_GetStatus_m1536464950 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EventManager_FetchAllResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL EventManager_FetchAllResponse_GetData(void*, intptr_t*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetData(System.Runtime.InteropServices.HandleRef,System.IntPtr[],System.UIntPtr)
extern "C"  UIntPtr_t  EventManager_EventManager_FetchAllResponse_GetData_m3903605334 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtrU5BU5D_t3228729122* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	intptr_t* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_array<intptr_t>((Il2CppCodeGenArray*)___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(EventManager_FetchAllResponse_GetData)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL EventManager_FetchResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void EventManager_EventManager_FetchResponse_Dispose_m3403259209 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EventManager_FetchResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL EventManager_FetchResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t EventManager_EventManager_FetchResponse_GetStatus_m2055738491 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EventManager_FetchResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL EventManager_FetchResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t EventManager_EventManager_FetchResponse_GetData_m2454104383 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(EventManager_FetchResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchAllCallback__ctor_m2962576734 (FetchAllCallback_t213806343 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchAllCallback_Invoke_m2942529706 (FetchAllCallback_t213806343 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchAllCallback_Invoke_m2942529706((FetchAllCallback_t213806343 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchAllCallback_t213806343 (FetchAllCallback_t213806343 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllCallback_BeginInvoke_m3634183863_MetadataUsageId;
extern "C"  Il2CppObject * FetchAllCallback_BeginInvoke_m3634183863 (FetchAllCallback_t213806343 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllCallback_BeginInvoke_m3634183863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchAllCallback_EndInvoke_m1815488110 (FetchAllCallback_t213806343 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchCallback__ctor_m1001377931 (FetchCallback_t1493526500 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchCallback_Invoke_m2274908957 (FetchCallback_t1493526500 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchCallback_Invoke_m2274908957((FetchCallback_t1493526500 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchCallback_t1493526500 (FetchCallback_t1493526500 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchCallback_BeginInvoke_m2850894754_MetadataUsageId;
extern "C"  Il2CppObject * FetchCallback_BeginInvoke_m2850894754 (FetchCallback_t1493526500 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchCallback_BeginInvoke_m2850894754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchCallback_EndInvoke_m2583642139 (FetchCallback_t1493526500 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL GameServices_Flush(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Flush(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback,System.IntPtr)
extern "C"  void GameServices_GameServices_Flush_m3275566173 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, FlushCallback_t3126222379 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Flush)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL GameServices_FetchServerAuthCode(void*, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_FetchServerAuthCode(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback,System.IntPtr)
extern "C"  void GameServices_GameServices_FetchServerAuthCode_m2919126661 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___server_client_id1, FetchServerAuthCodeCallback_t1082452761 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___server_client_id1' to native representation
	char* ____server_client_id1_marshaled = NULL;
	____server_client_id1_marshaled = il2cpp_codegen_marshal_string(___server_client_id1);

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_FetchServerAuthCode)(____self0_marshaled, ____server_client_id1_marshaled, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

	// Marshaling cleanup of parameter '___server_client_id1' native representation
	il2cpp_codegen_marshal_free(____server_client_id1_marshaled);
	____server_client_id1_marshaled = NULL;

}
extern "C" int8_t DEFAULT_CALL GameServices_IsAuthorized(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.GameServices::GameServices_IsAuthorized(System.Runtime.InteropServices.HandleRef)
extern "C"  bool GameServices_GameServices_IsAuthorized_m4258229637 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(GameServices_IsAuthorized)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL GameServices_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void GameServices_GameServices_Dispose_m2353921991 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_Dispose)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL GameServices_SignOut(void*);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_SignOut(System.Runtime.InteropServices.HandleRef)
extern "C"  void GameServices_GameServices_SignOut_m827006841 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_SignOut)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL GameServices_StartAuthorizationUI(void*);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_StartAuthorizationUI(System.Runtime.InteropServices.HandleRef)
extern "C"  void GameServices_GameServices_StartAuthorizationUI_m1226951135 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_StartAuthorizationUI)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL GameServices_FetchServerAuthCodeResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.GameServices::GameServices_FetchServerAuthCodeResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void GameServices_GameServices_FetchServerAuthCodeResponse_Dispose_m1129664315 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(GameServices_FetchServerAuthCodeResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL GameServices_FetchServerAuthCodeResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.GameServices::GameServices_FetchServerAuthCodeResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t GameServices_GameServices_FetchServerAuthCodeResponse_GetStatus_m766129709 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GameServices_FetchServerAuthCodeResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL GameServices_FetchServerAuthCodeResponse_GetCode(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.GameServices::GameServices_FetchServerAuthCodeResponse_GetCode(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  GameServices_GameServices_FetchServerAuthCodeResponse_GetCode_m1905169286 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(GameServices_FetchServerAuthCodeResponse_GetCode)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchServerAuthCodeCallback__ctor_m2142037824 (FetchServerAuthCodeCallback_t1082452761 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchServerAuthCodeCallback_Invoke_m3275627272 (FetchServerAuthCodeCallback_t1082452761 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchServerAuthCodeCallback_Invoke_m3275627272((FetchServerAuthCodeCallback_t1082452761 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchServerAuthCodeCallback_t1082452761 (FetchServerAuthCodeCallback_t1082452761 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchServerAuthCodeCallback_BeginInvoke_m3593913229_MetadataUsageId;
extern "C"  Il2CppObject * FetchServerAuthCodeCallback_BeginInvoke_m3593913229 (FetchServerAuthCodeCallback_t1082452761 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchServerAuthCodeCallback_BeginInvoke_m3593913229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FetchServerAuthCodeCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchServerAuthCodeCallback_EndInvoke_m655751504 (FetchServerAuthCodeCallback_t1082452761 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FlushCallback__ctor_m3989401426 (FlushCallback_t3126222379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus,System.IntPtr)
extern "C"  void FlushCallback_Invoke_m2939810879 (FlushCallback_t3126222379 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FlushCallback_Invoke_m2939810879((FlushCallback_t3126222379 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FlushCallback_t3126222379 (FlushCallback_t3126222379 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/FlushStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* FlushStatus_t1223161653_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FlushCallback_BeginInvoke_m793985810_MetadataUsageId;
extern "C"  Il2CppObject * FlushCallback_BeginInvoke_m793985810 (FlushCallback_t3126222379 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FlushCallback_BeginInvoke_m793985810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FlushStatus_t1223161653_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.GameServices/FlushCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FlushCallback_EndInvoke_m4039989858 (FlushCallback_t3126222379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL InternalHooks_ConfigureForUnityPlugin(void*);
// System.Void GooglePlayGames.Native.Cwrapper.InternalHooks::InternalHooks_ConfigureForUnityPlugin(System.Runtime.InteropServices.HandleRef)
extern "C"  void InternalHooks_InternalHooks_ConfigureForUnityPlugin_m4202235629 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___builder0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___builder0' to native representation
	void* ____builder0_marshaled = NULL;
	____builder0_marshaled = ___builder0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(InternalHooks_ConfigureForUnityPlugin)(____builder0_marshaled);

}
extern "C" intptr_t DEFAULT_CALL IosPlatformConfiguration_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Construct()
extern "C"  IntPtr_t IosPlatformConfiguration_IosPlatformConfiguration_Construct_m1863237347 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(IosPlatformConfiguration_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL IosPlatformConfiguration_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void IosPlatformConfiguration_IosPlatformConfiguration_Dispose_m1495653331 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IosPlatformConfiguration_Dispose)(____self0_marshaled);

}
extern "C" int8_t DEFAULT_CALL IosPlatformConfiguration_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool IosPlatformConfiguration_IosPlatformConfiguration_Valid_m3397918884 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(IosPlatformConfiguration_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL IosPlatformConfiguration_SetClientID(void*, char*);
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_SetClientID(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void IosPlatformConfiguration_IosPlatformConfiguration_SetClientID_m1483856728 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___client_id1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___client_id1' to native representation
	char* ____client_id1_marshaled = NULL;
	____client_id1_marshaled = il2cpp_codegen_marshal_string(___client_id1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IosPlatformConfiguration_SetClientID)(____self0_marshaled, ____client_id1_marshaled);

	// Marshaling cleanup of parameter '___client_id1' native representation
	il2cpp_codegen_marshal_free(____client_id1_marshaled);
	____client_id1_marshaled = NULL;

}
extern "C" uintptr_t DEFAULT_CALL Leaderboard_Name(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Leaderboard_Leaderboard_Name_m2322020470 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Leaderboard_Name)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Leaderboard_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Leaderboard_Leaderboard_Id_m4268312678 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Leaderboard_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Leaderboard_IconUrl(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_IconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Leaderboard_Leaderboard_IconUrl_m2471574023 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Leaderboard_IconUrl)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Leaderboard_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Leaderboard_Leaderboard_Dispose_m3257982343 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Leaderboard_Dispose)(____self0_marshaled);

}
extern "C" int8_t DEFAULT_CALL Leaderboard_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Leaderboard_Leaderboard_Valid_m2726869080 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Leaderboard_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL Leaderboard_Order(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Order(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t Leaderboard_Leaderboard_Order_m3850414315 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Leaderboard_Order)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchAll(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchAll_m3086784703 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchAllCallback_t3766146474 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAll)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchScoreSummary(void*, int32_t, char*, int32_t, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummary(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchScoreSummary_m4023621912 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___leaderboard_id2, int32_t ___time_span3, int32_t ___collection4, FetchScoreSummaryCallback_t1723193589 * ___callback5, IntPtr_t ___callback_arg6, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, int32_t, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id2' to native representation
	char* ____leaderboard_id2_marshaled = NULL;
	____leaderboard_id2_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id2);

	// Marshaling of parameter '___callback5' to native representation
	Il2CppMethodPointer ____callback5_marshaled = NULL;
	____callback5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScoreSummary)(____self0_marshaled, ___data_source1, ____leaderboard_id2_marshaled, ___time_span3, ___collection4, ____callback5_marshaled, reinterpret_cast<intptr_t>(___callback_arg6.get_m_value_0()));

	// Marshaling cleanup of parameter '___leaderboard_id2' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id2_marshaled);
	____leaderboard_id2_marshaled = NULL;

}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_ScorePageToken(void*, char*, int32_t, int32_t, int32_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ScorePageToken(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_ScorePageToken_m4032945966 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___leaderboard_id1, int32_t ___start2, int32_t ___time_span3, int32_t ___collection4, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, int32_t, int32_t, int32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id1' to native representation
	char* ____leaderboard_id1_marshaled = NULL;
	____leaderboard_id1_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id1);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_ScorePageToken)(____self0_marshaled, ____leaderboard_id1_marshaled, ___start2, ___time_span3, ___collection4);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	// Marshaling cleanup of parameter '___leaderboard_id1' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id1_marshaled);
	____leaderboard_id1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL LeaderboardManager_ShowAllUI(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_ShowAllUI_m866732149 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ShowAllUICallback_t3664260959 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_ShowAllUI)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchScorePage(void*, int32_t, intptr_t, uint32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePage(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.IntPtr,System.UInt32,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchScorePage_m697795265 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, IntPtr_t ___token2, uint32_t ___max_results3, FetchScorePageCallback_t1423453514 * ___callback4, IntPtr_t ___callback_arg5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, intptr_t, uint32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback4' to native representation
	Il2CppMethodPointer ____callback4_marshaled = NULL;
	____callback4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback4));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScorePage)(____self0_marshaled, ___data_source1, reinterpret_cast<intptr_t>(___token2.get_m_value_0()), ___max_results3, ____callback4_marshaled, reinterpret_cast<intptr_t>(___callback_arg5.get_m_value_0()));

}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchAllScoreSummaries(void*, int32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummaries(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchAllScoreSummaries_m3895156183 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___leaderboard_id2, FetchAllScoreSummariesCallback_t1010253660 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id2' to native representation
	char* ____leaderboard_id2_marshaled = NULL;
	____leaderboard_id2_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id2);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllScoreSummaries)(____self0_marshaled, ___data_source1, ____leaderboard_id2_marshaled, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___leaderboard_id2' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id2_marshaled);
	____leaderboard_id2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL LeaderboardManager_ShowUI(void*, char*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowUI(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_ShowUI_m2384274998 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___leaderboard_id1, int32_t ___time_span2, ShowUICallback_t1832555764 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id1' to native representation
	char* ____leaderboard_id1_marshaled = NULL;
	____leaderboard_id1_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id1);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_ShowUI)(____self0_marshaled, ____leaderboard_id1_marshaled, ___time_span2, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___leaderboard_id1' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id1_marshaled);
	____leaderboard_id1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL LeaderboardManager_Fetch(void*, int32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback,System.IntPtr)
extern "C"  void LeaderboardManager_LeaderboardManager_Fetch_m2620800335 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___leaderboard_id2, FetchCallback_t2463332897 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id2' to native representation
	char* ____leaderboard_id2_marshaled = NULL;
	____leaderboard_id2_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id2);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_Fetch)(____self0_marshaled, ___data_source1, ____leaderboard_id2_marshaled, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___leaderboard_id2' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id2_marshaled);
	____leaderboard_id2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL LeaderboardManager_SubmitScore(void*, char*, uint64_t, char*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_SubmitScore(System.Runtime.InteropServices.HandleRef,System.String,System.UInt64,System.String)
extern "C"  void LeaderboardManager_LeaderboardManager_SubmitScore_m1200233903 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___leaderboard_id1, uint64_t ___score2, String_t* ___metadata3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint64_t, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___leaderboard_id1' to native representation
	char* ____leaderboard_id1_marshaled = NULL;
	____leaderboard_id1_marshaled = il2cpp_codegen_marshal_string(___leaderboard_id1);

	// Marshaling of parameter '___metadata3' to native representation
	char* ____metadata3_marshaled = NULL;
	____metadata3_marshaled = il2cpp_codegen_marshal_string(___metadata3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_SubmitScore)(____self0_marshaled, ____leaderboard_id1_marshaled, ___score2, ____metadata3_marshaled);

	// Marshaling cleanup of parameter '___leaderboard_id1' native representation
	il2cpp_codegen_marshal_free(____leaderboard_id1_marshaled);
	____leaderboard_id1_marshaled = NULL;

	// Marshaling cleanup of parameter '___metadata3' native representation
	il2cpp_codegen_marshal_free(____metadata3_marshaled);
	____metadata3_marshaled = NULL;

}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchResponse_Dispose_m1843189891 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL LeaderboardManager_FetchResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t LeaderboardManager_LeaderboardManager_FetchResponse_GetStatus_m2392634293 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_FetchResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_FetchResponse_GetData_m1969339641 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchAllResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchAllResponse_Dispose_m3080923324 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetStatus_m660731580 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetData_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_Length_m110941772 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllResponse_GetData_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_FetchAllResponse_GetData_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_GetElement_m3369118872 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllResponse_GetData_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchScorePageResponse_Dispose_m3108568668 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScorePageResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetStatus_m4272660252 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScorePageResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_FetchScorePageResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetData_m4006917896 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScorePageResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_Dispose_m571805271 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScoreSummaryResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetStatus_m2272677641 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScoreSummaryResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_FetchScoreSummaryResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetData_m3459320653 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchScoreSummaryResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_Dispose_m1318596590 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllScoreSummariesResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus_m45206830 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length_m564118746 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement_m2741674506 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchAllCallback__ctor_m375551617 (FetchAllCallback_t3766146474 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchAllCallback_Invoke_m54162407 (FetchAllCallback_t3766146474 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchAllCallback_Invoke_m54162407((FetchAllCallback_t3766146474 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchAllCallback_t3766146474 (FetchAllCallback_t3766146474 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllCallback_BeginInvoke_m2893188596_MetadataUsageId;
extern "C"  Il2CppObject * FetchAllCallback_BeginInvoke_m2893188596 (FetchAllCallback_t3766146474 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllCallback_BeginInvoke_m2893188596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchAllCallback_EndInvoke_m3719254289 (FetchAllCallback_t3766146474 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchAllScoreSummariesCallback__ctor_m1723753779 (FetchAllScoreSummariesCallback_t1010253660 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchAllScoreSummariesCallback_Invoke_m3193723765 (FetchAllScoreSummariesCallback_t1010253660 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchAllScoreSummariesCallback_Invoke_m3193723765((FetchAllScoreSummariesCallback_t1010253660 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchAllScoreSummariesCallback_t1010253660 (FetchAllScoreSummariesCallback_t1010253660 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllScoreSummariesCallback_BeginInvoke_m3036928130_MetadataUsageId;
extern "C"  Il2CppObject * FetchAllScoreSummariesCallback_BeginInvoke_m3036928130 (FetchAllScoreSummariesCallback_t1010253660 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllScoreSummariesCallback_BeginInvoke_m3036928130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchAllScoreSummariesCallback_EndInvoke_m295856323 (FetchAllScoreSummariesCallback_t1010253660 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchCallback__ctor_m4183554632 (FetchCallback_t2463332897 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchCallback_Invoke_m2138138880 (FetchCallback_t2463332897 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchCallback_Invoke_m2138138880((FetchCallback_t2463332897 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchCallback_t2463332897 (FetchCallback_t2463332897 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchCallback_BeginInvoke_m2404087173_MetadataUsageId;
extern "C"  Il2CppObject * FetchCallback_BeginInvoke_m2404087173 (FetchCallback_t2463332897 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchCallback_BeginInvoke_m2404087173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchCallback_EndInvoke_m3004682328 (FetchCallback_t2463332897 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchScorePageCallback__ctor_m3167423521 (FetchScorePageCallback_t1423453514 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchScorePageCallback_Invoke_m702845511 (FetchScorePageCallback_t1423453514 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchScorePageCallback_Invoke_m702845511((FetchScorePageCallback_t1423453514 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchScorePageCallback_t1423453514 (FetchScorePageCallback_t1423453514 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchScorePageCallback_BeginInvoke_m3377207636_MetadataUsageId;
extern "C"  Il2CppObject * FetchScorePageCallback_BeginInvoke_m3377207636 (FetchScorePageCallback_t1423453514 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchScorePageCallback_BeginInvoke_m3377207636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchScorePageCallback_EndInvoke_m906213041 (FetchScorePageCallback_t1423453514 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchScoreSummaryCallback__ctor_m1247634204 (FetchScoreSummaryCallback_t1723193589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchScoreSummaryCallback_Invoke_m1318918828 (FetchScoreSummaryCallback_t1723193589 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchScoreSummaryCallback_Invoke_m1318918828((FetchScoreSummaryCallback_t1723193589 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchScoreSummaryCallback_t1723193589 (FetchScoreSummaryCallback_t1723193589 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchScoreSummaryCallback_BeginInvoke_m4182798641_MetadataUsageId;
extern "C"  Il2CppObject * FetchScoreSummaryCallback_BeginInvoke_m4182798641 (FetchScoreSummaryCallback_t1723193589 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchScoreSummaryCallback_BeginInvoke_m4182798641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchScoreSummaryCallback_EndInvoke_m3845571372 (FetchScoreSummaryCallback_t1723193589 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ShowAllUICallback__ctor_m2661528710 (ShowAllUICallback_t3664260959 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C"  void ShowAllUICallback_Invoke_m4121529223 (ShowAllUICallback_t3664260959 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ShowAllUICallback_Invoke_m4121529223((ShowAllUICallback_t3664260959 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ShowAllUICallback_t3664260959 (ShowAllUICallback_t3664260959 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t3557407943_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t ShowAllUICallback_BeginInvoke_m35818772_MetadataUsageId;
extern "C"  Il2CppObject * ShowAllUICallback_BeginInvoke_m35818772 (ShowAllUICallback_t3664260959 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowAllUICallback_BeginInvoke_m35818772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t3557407943_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void ShowAllUICallback_EndInvoke_m4284635542 (ShowAllUICallback_t3664260959 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ShowUICallback__ctor_m1164443339 (ShowUICallback_t1832555764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C"  void ShowUICallback_Invoke_m1583916300 (ShowUICallback_t1832555764 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ShowUICallback_Invoke_m1583916300((ShowUICallback_t1832555764 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ShowUICallback_t1832555764 (ShowUICallback_t1832555764 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t3557407943_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t ShowUICallback_BeginInvoke_m1886219025_MetadataUsageId;
extern "C"  Il2CppObject * ShowUICallback_BeginInvoke_m1886219025 (ShowUICallback_t1832555764 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowUICallback_BeginInvoke_m1886219025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UIStatus_t3557407943_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void ShowUICallback_EndInvoke_m1099468379 (ShowUICallback_t1832555764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" uint32_t DEFAULT_CALL MultiplayerInvitation_AutomatchingSlotsAvailable(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerInvitation_MultiplayerInvitation_AutomatchingSlotsAvailable_m1782766457 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_AutomatchingSlotsAvailable)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL MultiplayerInvitation_InvitingParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_InvitingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t MultiplayerInvitation_MultiplayerInvitation_InvitingParticipant_m1805269738 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_InvitingParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL MultiplayerInvitation_Variant(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerInvitation_MultiplayerInvitation_Variant_m98103368 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Variant)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL MultiplayerInvitation_CreationTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t MultiplayerInvitation_MultiplayerInvitation_CreationTime_m2074854660 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_CreationTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL MultiplayerInvitation_Participants_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Participants_Length_m3870351221 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Participants_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL MultiplayerInvitation_Participants_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t MultiplayerInvitation_MultiplayerInvitation_Participants_GetElement_m153709327 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Participants_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL MultiplayerInvitation_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerInvitation_MultiplayerInvitation_Valid_m903083864 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL MultiplayerInvitation_Type(void*);
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Type(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerInvitation_MultiplayerInvitation_Type_m8447793 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Type)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL MultiplayerInvitation_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Id_m2447988070 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL MultiplayerInvitation_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MultiplayerInvitation_MultiplayerInvitation_Dispose_m1530150535 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(MultiplayerInvitation_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL MultiplayerParticipant_Status(void*);
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerParticipant_MultiplayerParticipant_Status_m4290365423 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_Status)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL MultiplayerParticipant_MatchRank(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchRank(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t MultiplayerParticipant_MultiplayerParticipant_MatchRank_m1891349798 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_MatchRank)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL MultiplayerParticipant_IsConnectedToRoom(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_IsConnectedToRoom(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_IsConnectedToRoom_m2181604547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_IsConnectedToRoom)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL MultiplayerParticipant_DisplayName(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_DisplayName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_DisplayName_m1235596894 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_DisplayName)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL MultiplayerParticipant_HasPlayer(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasPlayer(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_HasPlayer_m2153678729 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_HasPlayer)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL MultiplayerParticipant_AvatarUrl(void*, int32_t, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_AvatarUrl_m2286680377 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___resolution1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg2' to native representation
	char* ____out_arg2_marshaled = NULL;
	____out_arg2_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg2);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_AvatarUrl)(____self0_marshaled, ___resolution1, ____out_arg2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size3.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg2, ____out_arg2_marshaled);

	// Marshaling cleanup of parameter '___out_arg2' native representation
	il2cpp_codegen_marshal_free(____out_arg2_marshaled);
	____out_arg2_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL MultiplayerParticipant_MatchResult(void*);
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_MatchResult(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t MultiplayerParticipant_MultiplayerParticipant_MatchResult_m3302970052 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_MatchResult)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL MultiplayerParticipant_Player(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Player(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t MultiplayerParticipant_MultiplayerParticipant_Player_m3369922382 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_Player)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL MultiplayerParticipant_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void MultiplayerParticipant_MultiplayerParticipant_Dispose_m4157444057 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_Dispose)(____self0_marshaled);

}
extern "C" int8_t DEFAULT_CALL MultiplayerParticipant_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_Valid_m3857586218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL MultiplayerParticipant_HasMatchResult(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_HasMatchResult(System.Runtime.InteropServices.HandleRef)
extern "C"  bool MultiplayerParticipant_MultiplayerParticipant_HasMatchResult_m3021916118 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_HasMatchResult)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL MultiplayerParticipant_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerParticipant::MultiplayerParticipant_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  MultiplayerParticipant_MultiplayerParticipant_Id_m3448685496 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(MultiplayerParticipant_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL ParticipantResults_WithResult(void*, char*, uint32_t, int32_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_WithResult(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32,GooglePlayGames.Native.Cwrapper.Types/MatchResult)
extern "C"  IntPtr_t ParticipantResults_ParticipantResults_WithResult_m3749793133 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, uint32_t ___placing2, int32_t ___result3, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uint32_t, int32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___participant_id1' to native representation
	char* ____participant_id1_marshaled = NULL;
	____participant_id1_marshaled = il2cpp_codegen_marshal_string(___participant_id1);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ParticipantResults_WithResult)(____self0_marshaled, ____participant_id1_marshaled, ___placing2, ___result3);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	// Marshaling cleanup of parameter '___participant_id1' native representation
	il2cpp_codegen_marshal_free(____participant_id1_marshaled);
	____participant_id1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL ParticipantResults_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ParticipantResults_ParticipantResults_Valid_m1338331198 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ParticipantResults_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL ParticipantResults_MatchResultForParticipant(void*, char*);
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_MatchResultForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  int32_t ParticipantResults_ParticipantResults_MatchResultForParticipant_m421575006 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___participant_id1' to native representation
	char* ____participant_id1_marshaled = NULL;
	____participant_id1_marshaled = il2cpp_codegen_marshal_string(___participant_id1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ParticipantResults_MatchResultForParticipant)(____self0_marshaled, ____participant_id1_marshaled);

	// Marshaling cleanup of parameter '___participant_id1' native representation
	il2cpp_codegen_marshal_free(____participant_id1_marshaled);
	____participant_id1_marshaled = NULL;

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL ParticipantResults_PlaceForParticipant(void*, char*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_PlaceForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  uint32_t ParticipantResults_ParticipantResults_PlaceForParticipant_m3371389750 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___participant_id1' to native representation
	char* ____participant_id1_marshaled = NULL;
	____participant_id1_marshaled = il2cpp_codegen_marshal_string(___participant_id1);

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(ParticipantResults_PlaceForParticipant)(____self0_marshaled, ____participant_id1_marshaled);

	// Marshaling cleanup of parameter '___participant_id1' native representation
	il2cpp_codegen_marshal_free(____participant_id1_marshaled);
	____participant_id1_marshaled = NULL;

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ParticipantResults_HasResultsForParticipant(void*, char*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_HasResultsForParticipant(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  bool ParticipantResults_ParticipantResults_HasResultsForParticipant_m638434428 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___participant_id1, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___participant_id1' to native representation
	char* ____participant_id1_marshaled = NULL;
	____participant_id1_marshaled = il2cpp_codegen_marshal_string(___participant_id1);

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ParticipantResults_HasResultsForParticipant)(____self0_marshaled, ____participant_id1_marshaled);

	// Marshaling cleanup of parameter '___participant_id1' native representation
	il2cpp_codegen_marshal_free(____participant_id1_marshaled);
	____participant_id1_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL ParticipantResults_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.ParticipantResults::ParticipantResults_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ParticipantResults_ParticipantResults_Dispose_m3239255789 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ParticipantResults_Dispose)(____self0_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Player_CurrentLevel(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_CurrentLevel(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Player_Player_CurrentLevel_m1264167784 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_CurrentLevel)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Player_Name(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Name_m332392472 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_Name)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Player_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Player::Player_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Player_Player_Dispose_m2925546665 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Player_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL Player_AvatarUrl(void*, int32_t, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_AvatarUrl_m901019241 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___resolution1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg2' to native representation
	char* ____out_arg2_marshaled = NULL;
	____out_arg2_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg2);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_AvatarUrl)(____self0_marshaled, ___resolution1, ____out_arg2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size3.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg2, ____out_arg2_marshaled);

	// Marshaling cleanup of parameter '___out_arg2' native representation
	il2cpp_codegen_marshal_free(____out_arg2_marshaled);
	____out_arg2_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint64_t DEFAULT_CALL Player_LastLevelUpTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_LastLevelUpTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Player_Player_LastLevelUpTime_m996014492 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(Player_LastLevelUpTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Player_Title(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Title(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Title_m1522031299 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_Title)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint64_t DEFAULT_CALL Player_CurrentXP(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_CurrentXP(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Player_Player_CurrentXP_m17556439 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(Player_CurrentXP)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL Player_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Player_Player_Valid_m170236154 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Player_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL Player_HasLevelInfo(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_HasLevelInfo(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Player_Player_HasLevelInfo_m3132517014 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Player_HasLevelInfo)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL Player_NextLevel(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_NextLevel(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Player_Player_NextLevel_m3927512976 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_NextLevel)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Player_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Player_Player_Id_m605911176 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Player_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL PlayerManager_FetchInvitable(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchInvitable(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C"  void PlayerManager_PlayerManager_FetchInvitable_m2282165339 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchListCallback_t2067692889 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchInvitable)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL PlayerManager_FetchConnected(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchConnected(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C"  void PlayerManager_PlayerManager_FetchConnected_m1675562728 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchListCallback_t2067692889 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchConnected)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL PlayerManager_Fetch(void*, int32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback,System.IntPtr)
extern "C"  void PlayerManager_PlayerManager_Fetch_m347110307 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___player_id2, FetchCallback_t1233224091 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___player_id2' to native representation
	char* ____player_id2_marshaled = NULL;
	____player_id2_marshaled = il2cpp_codegen_marshal_string(___player_id2);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_Fetch)(____self0_marshaled, ___data_source1, ____player_id2_marshaled, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___player_id2' native representation
	il2cpp_codegen_marshal_free(____player_id2_marshaled);
	____player_id2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL PlayerManager_FetchRecentlyPlayed(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchRecentlyPlayed(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C"  void PlayerManager_PlayerManager_FetchRecentlyPlayed_m1891255950 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchListCallback_t2067692889 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchRecentlyPlayed)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL PlayerManager_FetchSelf(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelf(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback,System.IntPtr)
extern "C"  void PlayerManager_PlayerManager_FetchSelf_m1180265743 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchSelfCallback_t2479296007 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchSelf)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL PlayerManager_FetchSelfResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerManager_PlayerManager_FetchSelfResponse_Dispose_m624931599 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchSelfResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL PlayerManager_FetchSelfResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerManager_PlayerManager_FetchSelfResponse_GetStatus_m2310467073 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchSelfResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL PlayerManager_FetchSelfResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t PlayerManager_PlayerManager_FetchSelfResponse_GetData_m79348549 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchSelfResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL PlayerManager_FetchResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerManager_PlayerManager_FetchResponse_Dispose_m3241435299 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL PlayerManager_FetchResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerManager_PlayerManager_FetchResponse_GetStatus_m4206879509 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL PlayerManager_FetchResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t PlayerManager_PlayerManager_FetchResponse_GetData_m3490935641 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL PlayerManager_FetchListResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerManager_PlayerManager_FetchListResponse_Dispose_m359459297 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerManager_FetchListResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL PlayerManager_FetchListResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerManager_PlayerManager_FetchListResponse_GetStatus_m594655315 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchListResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL PlayerManager_FetchListResponse_GetData_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  PlayerManager_PlayerManager_FetchListResponse_GetData_Length_m2043206263 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchListResponse_GetData_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL PlayerManager_FetchListResponse_GetData_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t PlayerManager_PlayerManager_FetchListResponse_GetData_GetElement_m823786567 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerManager_FetchListResponse_GetData_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchCallback__ctor_m4142880242 (FetchCallback_t1233224091 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchCallback_Invoke_m877232790 (FetchCallback_t1233224091 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchCallback_Invoke_m877232790((FetchCallback_t1233224091 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchCallback_t1233224091 (FetchCallback_t1233224091 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchCallback_BeginInvoke_m2131412387_MetadataUsageId;
extern "C"  Il2CppObject * FetchCallback_BeginInvoke_m2131412387 (FetchCallback_t1233224091 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchCallback_BeginInvoke_m2131412387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchCallback_EndInvoke_m4180644098 (FetchCallback_t1233224091 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchListCallback__ctor_m1788214960 (FetchListCallback_t2067692889 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchListCallback_Invoke_m897053080 (FetchListCallback_t2067692889 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchListCallback_Invoke_m897053080((FetchListCallback_t2067692889 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchListCallback_t2067692889 (FetchListCallback_t2067692889 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchListCallback_BeginInvoke_m2181376165_MetadataUsageId;
extern "C"  Il2CppObject * FetchListCallback_BeginInvoke_m2181376165 (FetchListCallback_t2067692889 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchListCallback_BeginInvoke_m2181376165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchListCallback_EndInvoke_m563366080 (FetchListCallback_t2067692889 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchSelfCallback__ctor_m2720551006 (FetchSelfCallback_t2479296007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchSelfCallback_Invoke_m4029666730 (FetchSelfCallback_t2479296007 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchSelfCallback_Invoke_m4029666730((FetchSelfCallback_t2479296007 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchSelfCallback_t2479296007 (FetchSelfCallback_t2479296007 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchSelfCallback_BeginInvoke_m2202800823_MetadataUsageId;
extern "C"  Il2CppObject * FetchSelfCallback_BeginInvoke_m2202800823 (FetchSelfCallback_t2479296007 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchSelfCallback_BeginInvoke_m2202800823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchSelfCallback_EndInvoke_m4236157294 (FetchSelfCallback_t2479296007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" int8_t DEFAULT_CALL PlayerStats_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_Valid_m1111996920 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL PlayerStats_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerStats_PlayerStats_Dispose_m1849030439 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PlayerStats_Dispose)(____self0_marshaled);

}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasAverageSessionLength(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasAverageSessionLength(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasAverageSessionLength_m4033552885 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasAverageSessionLength)(____self0_marshaled);

	return returnValue;
}
extern "C" float DEFAULT_CALL PlayerStats_AverageSessionLength(void*);
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_AverageSessionLength(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_AverageSessionLength_m4087324967 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_AverageSessionLength)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasChurnProbability(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasChurnProbability(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasChurnProbability_m2631311647 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasChurnProbability)(____self0_marshaled);

	return returnValue;
}
extern "C" float DEFAULT_CALL PlayerStats_ChurnProbability(void*);
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_ChurnProbability(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_ChurnProbability_m2206107985 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_ChurnProbability)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasDaysSinceLastPlayed(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasDaysSinceLastPlayed(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasDaysSinceLastPlayed_m2056999314 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasDaysSinceLastPlayed)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL PlayerStats_DaysSinceLastPlayed(void*);
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_DaysSinceLastPlayed(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_DaysSinceLastPlayed_m123098082 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_DaysSinceLastPlayed)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasNumberOfPurchases(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasNumberOfPurchases(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasNumberOfPurchases_m3293499896 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasNumberOfPurchases)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL PlayerStats_NumberOfPurchases(void*);
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_NumberOfPurchases(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_NumberOfPurchases_m3246794824 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_NumberOfPurchases)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasNumberOfSessions(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasNumberOfSessions(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasNumberOfSessions_m3953189427 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasNumberOfSessions)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL PlayerStats_NumberOfSessions(void*);
// System.Int32 GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_NumberOfSessions(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t PlayerStats_PlayerStats_NumberOfSessions_m3120398819 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_NumberOfSessions)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasSessionPercentile(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasSessionPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasSessionPercentile_m887392441 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasSessionPercentile)(____self0_marshaled);

	return returnValue;
}
extern "C" float DEFAULT_CALL PlayerStats_SessionPercentile(void*);
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_SessionPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_SessionPercentile_m590980807 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_SessionPercentile)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL PlayerStats_HasSpendPercentile(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_HasSpendPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  bool PlayerStats_PlayerStats_HasSpendPercentile_m704060801 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_HasSpendPercentile)(____self0_marshaled);

	return returnValue;
}
extern "C" float DEFAULT_CALL PlayerStats_SpendPercentile(void*);
// System.Single GooglePlayGames.Native.Cwrapper.PlayerStats::PlayerStats_SpendPercentile(System.Runtime.InteropServices.HandleRef)
extern "C"  float PlayerStats_PlayerStats_SpendPercentile_m1937270543 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(PlayerStats_SpendPercentile)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Quest_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Quest_Quest_Description_m1218041697 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Quest_BannerUrl(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_BannerUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Quest_Quest_BannerUrl_m1767168218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_BannerUrl)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int64_t DEFAULT_CALL Quest_ExpirationTime(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_ExpirationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t Quest_Quest_ExpirationTime_m2144424225 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_ExpirationTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Quest_IconUrl(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_IconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Quest_Quest_IconUrl_m3649339879 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_IconUrl)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL Quest_State(void*);
// GooglePlayGames.Native.Cwrapper.Types/QuestState GooglePlayGames.Native.Cwrapper.Quest::Quest_State(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t Quest_Quest_State_m4267024976 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_State)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL Quest_Copy(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Copy(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Quest_Quest_Copy_m3713593940 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_Copy)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL Quest_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Quest::Quest_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Quest_Quest_Valid_m3900128888 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int64_t DEFAULT_CALL Quest_StartTime(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_StartTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t Quest_Quest_StartTime_m1080319206 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_StartTime)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL Quest_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Quest::Quest_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Quest_Quest_Dispose_m1151757735 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Quest_Dispose)(____self0_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Quest_CurrentMilestone(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_CurrentMilestone(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t Quest_Quest_CurrentMilestone_m1682520790 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_CurrentMilestone)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int64_t DEFAULT_CALL Quest_AcceptedTime(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.Quest::Quest_AcceptedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t Quest_Quest_AcceptedTime_m1008898521 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_AcceptedTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL Quest_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Quest_Quest_Id_m1971360902 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL Quest_Name(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Quest::Quest_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Quest_Quest_Name_m2564553878 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Quest_Name)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestManager_FetchList(void*, int32_t, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchList(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Int32,GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_FetchList_m1714230423 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, int32_t ___fetch_flags2, FetchListCallback_t3150025178 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_FetchList)(____self0_marshaled, ___data_source1, ___fetch_flags2, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

}
extern "C" void DEFAULT_CALL QuestManager_Accept(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_Accept(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_Accept_m131569118 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___quest1, AcceptCallback_t1408209808 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_Accept)(____self0_marshaled, reinterpret_cast<intptr_t>(___quest1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL QuestManager_ShowAllUI(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_ShowAllUI_m2755700006 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, QuestUICallback_t3097297816 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_ShowAllUI)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL QuestManager_ShowUI(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ShowUI(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_ShowUI_m3545983711 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___quest1, QuestUICallback_t3097297816 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_ShowUI)(____self0_marshaled, reinterpret_cast<intptr_t>(___quest1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL QuestManager_ClaimMilestone(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestone(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_ClaimMilestone_m3309348190 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___milestone1, ClaimMilestoneCallback_t1411568668 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_ClaimMilestone)(____self0_marshaled, reinterpret_cast<intptr_t>(___milestone1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL QuestManager_Fetch(void*, int32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback,System.IntPtr)
extern "C"  void QuestManager_QuestManager_Fetch_m1591477354 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___quest_id2, FetchCallback_t1576861340 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___quest_id2' to native representation
	char* ____quest_id2_marshaled = NULL;
	____quest_id2_marshaled = il2cpp_codegen_marshal_string(___quest_id2);

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_Fetch)(____self0_marshaled, ___data_source1, ____quest_id2_marshaled, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

	// Marshaling cleanup of parameter '___quest_id2' native representation
	il2cpp_codegen_marshal_free(____quest_id2_marshaled);
	____quest_id2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL QuestManager_FetchResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestManager_QuestManager_FetchResponse_Dispose_m1919085753 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_FetchResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL QuestManager_FetchResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestManager_QuestManager_FetchResponse_GetStatus_m1694189547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_FetchResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_FetchResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_FetchResponse_GetData_m969930927 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_FetchResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestManager_FetchListResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestManager_QuestManager_FetchListResponse_Dispose_m900430583 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_FetchListResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL QuestManager_FetchListResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestManager_QuestManager_FetchListResponse_GetStatus_m2233647657 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_FetchListResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL QuestManager_FetchListResponse_GetData_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  QuestManager_QuestManager_FetchListResponse_GetData_Length_m389718689 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_FetchListResponse_GetData_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_FetchListResponse_GetData_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t QuestManager_QuestManager_FetchListResponse_GetData_GetElement_m3759343389 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_FetchListResponse_GetData_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestManager_AcceptResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestManager_QuestManager_AcceptResponse_Dispose_m2030488071 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_AcceptResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL QuestManager_AcceptResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestManager_QuestManager_AcceptResponse_GetStatus_m4203580184 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_AcceptResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_AcceptResponse_GetAcceptedQuest(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_GetAcceptedQuest(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_AcceptResponse_GetAcceptedQuest_m66525390 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_AcceptResponse_GetAcceptedQuest)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestManager_ClaimMilestoneResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestManager_QuestManager_ClaimMilestoneResponse_Dispose_m1713644179 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_ClaimMilestoneResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestManager_QuestManager_ClaimMilestoneResponse_GetStatus_m1176571952 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_ClaimMilestoneResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_ClaimMilestoneResponse_GetClaimedMilestone_m3806592426 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_ClaimMilestoneResponse_GetClaimedMilestone)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetQuest(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetQuest(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_ClaimMilestoneResponse_GetQuest_m3869556841 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_ClaimMilestoneResponse_GetQuest)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestManager_QuestUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestManager_QuestManager_QuestUIResponse_Dispose_m4213761525 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestManager_QuestUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL QuestManager_QuestUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestManager_QuestManager_QuestUIResponse_GetStatus_m1191355540 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_QuestUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_QuestUIResponse_GetAcceptedQuest(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetAcceptedQuest(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_QuestUIResponse_GetAcceptedQuest_m3700411734 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_QuestUIResponse_GetAcceptedQuest)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL QuestManager_QuestUIResponse_GetMilestoneToClaim(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetMilestoneToClaim(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestManager_QuestManager_QuestUIResponse_GetMilestoneToClaim_m1142301362 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestManager_QuestUIResponse_GetMilestoneToClaim)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AcceptCallback__ctor_m3607515495 (AcceptCallback_t1408209808 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void AcceptCallback_Invoke_m1460794817 (AcceptCallback_t1408209808 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AcceptCallback_Invoke_m1460794817((AcceptCallback_t1408209808 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AcceptCallback_t1408209808 (AcceptCallback_t1408209808 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t AcceptCallback_BeginInvoke_m3423302094_MetadataUsageId;
extern "C"  Il2CppObject * AcceptCallback_BeginInvoke_m3423302094 (AcceptCallback_t1408209808 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AcceptCallback_BeginInvoke_m3423302094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AcceptCallback_EndInvoke_m2391838455 (AcceptCallback_t1408209808 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ClaimMilestoneCallback__ctor_m1193255923 (ClaimMilestoneCallback_t1411568668 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void ClaimMilestoneCallback_Invoke_m3928159413 (ClaimMilestoneCallback_t1411568668 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ClaimMilestoneCallback_Invoke_m3928159413((ClaimMilestoneCallback_t1411568668 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ClaimMilestoneCallback_t1411568668 (ClaimMilestoneCallback_t1411568668 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t ClaimMilestoneCallback_BeginInvoke_m3384649922_MetadataUsageId;
extern "C"  Il2CppObject * ClaimMilestoneCallback_BeginInvoke_m3384649922 (ClaimMilestoneCallback_t1411568668 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ClaimMilestoneCallback_BeginInvoke_m3384649922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ClaimMilestoneCallback_EndInvoke_m200345475 (ClaimMilestoneCallback_t1411568668 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchCallback__ctor_m3350489923 (FetchCallback_t1576861340 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchCallback_Invoke_m2082936677 (FetchCallback_t1576861340 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchCallback_Invoke_m2082936677((FetchCallback_t1576861340 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchCallback_t1576861340 (FetchCallback_t1576861340 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchCallback_BeginInvoke_m2770077674_MetadataUsageId;
extern "C"  Il2CppObject * FetchCallback_BeginInvoke_m2770077674 (FetchCallback_t1576861340 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchCallback_BeginInvoke_m2770077674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchCallback_EndInvoke_m3727554259 (FetchCallback_t1576861340 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchListCallback__ctor_m2101216129 (FetchListCallback_t3150025178 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchListCallback_Invoke_m2010154727 (FetchListCallback_t3150025178 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchListCallback_Invoke_m2010154727((FetchListCallback_t3150025178 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchListCallback_t3150025178 (FetchListCallback_t3150025178 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchListCallback_BeginInvoke_m3717066604_MetadataUsageId;
extern "C"  Il2CppObject * FetchListCallback_BeginInvoke_m3717066604 (FetchListCallback_t3150025178 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchListCallback_BeginInvoke_m3717066604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchListCallback_EndInvoke_m4065943057 (FetchListCallback_t3150025178 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void QuestUICallback__ctor_m1330513983 (QuestUICallback_t3097297816 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void QuestUICallback_Invoke_m3888191977 (QuestUICallback_t3097297816 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		QuestUICallback_Invoke_m3888191977((QuestUICallback_t3097297816 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_QuestUICallback_t3097297816 (QuestUICallback_t3097297816 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t QuestUICallback_BeginInvoke_m2533697390_MetadataUsageId;
extern "C"  Il2CppObject * QuestUICallback_BeginInvoke_m2533697390 (QuestUICallback_t3097297816 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QuestUICallback_BeginInvoke_m2533697390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void QuestUICallback_EndInvoke_m4231360975 (QuestUICallback_t3097297816 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" uintptr_t DEFAULT_CALL QuestMilestone_EventId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_EventId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_EventId_m2634184396 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_EventId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint64_t DEFAULT_CALL QuestMilestone_CurrentCount(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CurrentCount(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t QuestMilestone_QuestMilestone_CurrentCount_m2780596114 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_CurrentCount)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL QuestMilestone_Copy(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Copy(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t QuestMilestone_QuestMilestone_Copy_m2920690680 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_Copy)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL QuestMilestone_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void QuestMilestone_QuestMilestone_Dispose_m3246914755 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(QuestMilestone_Dispose)(____self0_marshaled);

}
extern "C" uint64_t DEFAULT_CALL QuestMilestone_TargetCount(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_TargetCount(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t QuestMilestone_QuestMilestone_TargetCount_m2990276734 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_TargetCount)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL QuestMilestone_QuestId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_QuestId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_QuestId_m2078821764 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_QuestId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL QuestMilestone_CompletionRewardData(void*, uint8_t*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CompletionRewardData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_CompletionRewardData_m640186953 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	uint8_t* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_CompletionRewardData)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	if (____out_arg1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_array_out<uint8_t>(____out_arg1_marshaled, ___out_arg1);
	}

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL QuestMilestone_State(void*);
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_State(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t QuestMilestone_QuestMilestone_State_m409042012 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_State)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL QuestMilestone_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool QuestMilestone_QuestMilestone_Valid_m279486356 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL QuestMilestone_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  QuestMilestone_QuestMilestone_Id_m327259042 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(QuestMilestone_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m2774103205 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnParticipantStatusChangedCallback_t2709967058 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" intptr_t DEFAULT_CALL RealTimeEventListenerHelper_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Construct()
extern "C"  IntPtr_t RealTimeEventListenerHelper_RealTimeEventListenerHelper_Construct_m1869499991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback_m2344363819 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnP2PDisconnectedCallback_t401589328 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnDataReceivedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnDataReceivedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnDataReceivedCallback_m2919741101 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnDataReceivedCallback_t176853902 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnDataReceivedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m2861445547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnRoomStatusChangedCallback_t1130665710 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnP2PConnectedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PConnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PConnectedCallback_m3416472141 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnP2PConnectedCallback_t507387838 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnP2PConnectedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback,System.IntPtr)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m4025949195 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, OnRoomConnectedSetChangedCallback_t3651478055 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeEventListenerHelper_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeEventListenerHelper_RealTimeEventListenerHelper_Dispose_m239894535 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeEventListenerHelper_Dispose)(____self0_marshaled);

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDataReceivedCallback__ctor_m3018045237 (OnDataReceivedCallback_t176853902 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr)
extern "C"  void OnDataReceivedCallback_Invoke_m2778949787 (OnDataReceivedCallback_t176853902 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, UIntPtr_t  ___arg33, bool ___arg44, IntPtr_t ___arg55, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDataReceivedCallback_Invoke_m2778949787((OnDataReceivedCallback_t176853902 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, ___arg44, ___arg55, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, UIntPtr_t  ___arg33, bool ___arg44, IntPtr_t ___arg55, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33, ___arg44, ___arg55,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, UIntPtr_t  ___arg33, bool ___arg44, IntPtr_t ___arg55, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22, ___arg33, ___arg44, ___arg55,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnDataReceivedCallback_t176853902 (OnDataReceivedCallback_t176853902 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, UIntPtr_t  ___arg33, bool ___arg44, IntPtr_t ___arg55, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t, intptr_t, uintptr_t, int8_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()), reinterpret_cast<intptr_t>(___arg22.get_m_value_0()), static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___arg33.get__pointer_1())), ___arg44, reinterpret_cast<intptr_t>(___arg55.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t OnDataReceivedCallback_BeginInvoke_m2856867600_MetadataUsageId;
extern "C"  Il2CppObject * OnDataReceivedCallback_BeginInvoke_m2856867600 (OnDataReceivedCallback_t176853902 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, UIntPtr_t  ___arg33, bool ___arg44, IntPtr_t ___arg55, AsyncCallback_t1369114871 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnDataReceivedCallback_BeginInvoke_m2856867600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[7] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	__d_args[3] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___arg33);
	__d_args[4] = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &___arg44);
	__d_args[5] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg55);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback6, (Il2CppObject*)___object7);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnDataReceivedCallback_EndInvoke_m1052052421 (OnDataReceivedCallback_t176853902 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnP2PConnectedCallback__ctor_m2704862053 (OnP2PConnectedCallback_t507387838 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void OnP2PConnectedCallback_Invoke_m496854993 (OnP2PConnectedCallback_t507387838 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnP2PConnectedCallback_Invoke_m496854993((OnP2PConnectedCallback_t507387838 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnP2PConnectedCallback_t507387838 (OnP2PConnectedCallback_t507387838 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()), reinterpret_cast<intptr_t>(___arg22.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnP2PConnectedCallback_BeginInvoke_m2750434106_MetadataUsageId;
extern "C"  Il2CppObject * OnP2PConnectedCallback_BeginInvoke_m2750434106 (OnP2PConnectedCallback_t507387838 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnP2PConnectedCallback_BeginInvoke_m2750434106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnP2PConnectedCallback_EndInvoke_m1504434165 (OnP2PConnectedCallback_t507387838 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnP2PDisconnectedCallback__ctor_m1184347175 (OnP2PDisconnectedCallback_t401589328 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void OnP2PDisconnectedCallback_Invoke_m912245455 (OnP2PDisconnectedCallback_t401589328 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnP2PDisconnectedCallback_Invoke_m912245455((OnP2PDisconnectedCallback_t401589328 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnP2PDisconnectedCallback_t401589328 (OnP2PDisconnectedCallback_t401589328 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()), reinterpret_cast<intptr_t>(___arg22.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnP2PDisconnectedCallback_BeginInvoke_m3196088512_MetadataUsageId;
extern "C"  Il2CppObject * OnP2PDisconnectedCallback_BeginInvoke_m3196088512 (OnP2PDisconnectedCallback_t401589328 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnP2PDisconnectedCallback_BeginInvoke_m3196088512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnP2PDisconnectedCallback_EndInvoke_m1572950455 (OnP2PDisconnectedCallback_t401589328 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnParticipantStatusChangedCallback__ctor_m2799928953 (OnParticipantStatusChangedCallback_t2709967058 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C"  void OnParticipantStatusChangedCallback_Invoke_m3211706941 (OnParticipantStatusChangedCallback_t2709967058 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnParticipantStatusChangedCallback_Invoke_m3211706941((OnParticipantStatusChangedCallback_t2709967058 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnParticipantStatusChangedCallback_t2709967058 (OnParticipantStatusChangedCallback_t2709967058 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()), reinterpret_cast<intptr_t>(___arg22.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnParticipantStatusChangedCallback_BeginInvoke_m3705352102_MetadataUsageId;
extern "C"  Il2CppObject * OnParticipantStatusChangedCallback_BeginInvoke_m3705352102 (OnParticipantStatusChangedCallback_t2709967058 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, IntPtr_t ___arg22, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnParticipantStatusChangedCallback_BeginInvoke_m3705352102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg22);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnParticipantStatusChangedCallback_EndInvoke_m3882294537 (OnParticipantStatusChangedCallback_t2709967058 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRoomConnectedSetChangedCallback__ctor_m2174851710 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void OnRoomConnectedSetChangedCallback_Invoke_m4292857738 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnRoomConnectedSetChangedCallback_Invoke_m4292857738((OnRoomConnectedSetChangedCallback_t3651478055 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnRoomConnectedSetChangedCallback_t3651478055 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnRoomConnectedSetChangedCallback_BeginInvoke_m4259196311_MetadataUsageId;
extern "C"  Il2CppObject * OnRoomConnectedSetChangedCallback_BeginInvoke_m4259196311 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnRoomConnectedSetChangedCallback_BeginInvoke_m4259196311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnRoomConnectedSetChangedCallback_EndInvoke_m2540829582 (OnRoomConnectedSetChangedCallback_t3651478055 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRoomStatusChangedCallback__ctor_m3342395845 (OnRoomStatusChangedCallback_t1130665710 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void OnRoomStatusChangedCallback_Invoke_m1832020259 (OnRoomStatusChangedCallback_t1130665710 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnRoomStatusChangedCallback_Invoke_m1832020259((OnRoomStatusChangedCallback_t1130665710 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OnRoomStatusChangedCallback_t1130665710 (OnRoomStatusChangedCallback_t1130665710 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OnRoomStatusChangedCallback_BeginInvoke_m1781574960_MetadataUsageId;
extern "C"  Il2CppObject * OnRoomStatusChangedCallback_BeginInvoke_m1781574960 (OnRoomStatusChangedCallback_t1130665710 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnRoomStatusChangedCallback_BeginInvoke_m1781574960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnRoomStatusChangedCallback_EndInvoke_m3777627733 (OnRoomStatusChangedCallback_t1130665710 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_CreateRealTimeRoom(void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_CreateRealTimeRoom(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_CreateRealTimeRoom_m2316364501 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___config1, IntPtr_t ___helper2, RealTimeRoomCallback_t3285480827 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_CreateRealTimeRoom)(____self0_marshaled, reinterpret_cast<intptr_t>(___config1.get_m_value_0()), reinterpret_cast<intptr_t>(___helper2.get_m_value_0()), ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_LeaveRoom(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_LeaveRoom(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_LeaveRoom_m585026857 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___room1, LeaveRoomCallback_t3947593351 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_LeaveRoom)(____self0_marshaled, reinterpret_cast<intptr_t>(___room1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_SendUnreliableMessage(void*, intptr_t, intptr_t*, uintptr_t, uint8_t*, uintptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendUnreliableMessage(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr[],System.UIntPtr,System.Byte[],System.UIntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendUnreliableMessage_m3812210231 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___room1, IntPtrU5BU5D_t3228729122* ___participants2, UIntPtr_t  ___participants_size3, ByteU5BU5D_t4260760469* ___data4, UIntPtr_t  ___data_size5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t*, uintptr_t, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___participants2' to native representation
	intptr_t* ____participants2_marshaled = NULL;
	____participants2_marshaled = il2cpp_codegen_marshal_array<intptr_t>((Il2CppCodeGenArray*)___participants2);

	// Marshaling of parameter '___data4' to native representation
	uint8_t* ____data4_marshaled = NULL;
	____data4_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_SendUnreliableMessage)(____self0_marshaled, reinterpret_cast<intptr_t>(___room1.get_m_value_0()), ____participants2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___participants_size3.get__pointer_1())), ____data4_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___data_size5.get__pointer_1())));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_ShowWaitingRoomUI(void*, intptr_t, uint32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowWaitingRoomUI(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.UInt32,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowWaitingRoomUI_m3577746302 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___room1, uint32_t ___min_participants_to_start2, WaitingRoomUICallback_t206291377 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, uint32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_ShowWaitingRoomUI)(____self0_marshaled, reinterpret_cast<intptr_t>(___room1.get_m_value_0()), ___min_participants_to_start2, ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_ShowPlayerSelectUI(void*, uint32_t, uint32_t, int8_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowPlayerSelectUI(System.Runtime.InteropServices.HandleRef,System.UInt32,System.UInt32,System.Boolean,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowPlayerSelectUI_m3976382691 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_players1, uint32_t ___maximum_players2, bool ___allow_automatch3, PlayerSelectUICallback_t2633669638 * ___callback4, IntPtr_t ___callback_arg5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t, uint32_t, int8_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback4' to native representation
	Il2CppMethodPointer ____callback4_marshaled = NULL;
	____callback4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback4));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_ShowPlayerSelectUI)(____self0_marshaled, ___minimum_players1, ___maximum_players2, ___allow_automatch3, ____callback4_marshaled, reinterpret_cast<intptr_t>(___callback_arg5.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_DismissInvitation(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_DismissInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_DismissInvitation_m677533589 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_DismissInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_DeclineInvitation(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_DeclineInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_DeclineInvitation_m2699115777 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_DeclineInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_SendReliableMessage(void*, intptr_t, intptr_t, uint8_t*, uintptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendReliableMessage(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.Byte[],System.UIntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendReliableMessage_m3417015793 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___room1, IntPtr_t ___participant2, ByteU5BU5D_t4260760469* ___data3, UIntPtr_t  ___data_size4, SendReliableMessageCallback_t2145488650 * ___callback5, IntPtr_t ___callback_arg6, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, uint8_t*, uintptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	____data3_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data3);

	// Marshaling of parameter '___callback5' to native representation
	Il2CppMethodPointer ____callback5_marshaled = NULL;
	____callback5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_SendReliableMessage)(____self0_marshaled, reinterpret_cast<intptr_t>(___room1.get_m_value_0()), reinterpret_cast<intptr_t>(___participant2.get_m_value_0()), ____data3_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___data_size4.get__pointer_1())), ____callback5_marshaled, reinterpret_cast<intptr_t>(___callback_arg6.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_AcceptInvitation(void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_AcceptInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_AcceptInvitation_m2298327156 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, IntPtr_t ___helper2, RealTimeRoomCallback_t3285480827 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_AcceptInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()), reinterpret_cast<intptr_t>(___helper2.get_m_value_0()), ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitations(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitations(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitations_m3427673821 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, FetchInvitationsCallback_t1518378837 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_FetchInvitations)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_SendUnreliableMessageToOthers(void*, intptr_t, uint8_t*, uintptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendUnreliableMessageToOthers(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendUnreliableMessageToOthers_m1764273218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___room1, ByteU5BU5D_t4260760469* ___data2, UIntPtr_t  ___data_size3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___data2' to native representation
	uint8_t* ____data2_marshaled = NULL;
	____data2_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_SendUnreliableMessageToOthers)(____self0_marshaled, reinterpret_cast<intptr_t>(___room1.get_m_value_0()), ____data2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___data_size3.get__pointer_1())));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_ShowRoomInboxUI(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowRoomInboxUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback,System.IntPtr)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowRoomInboxUI_m4162965050 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, RoomInboxUICallback_t3659854740 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_ShowRoomInboxUI)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose_m2471590943 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus_m3770438144 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom_m1954218364 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose_m4003242276 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus_m1024620547 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation_m2624710537 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose_m2884528257 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus_m2818535072 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom_m3859300840 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose_m3411527289 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus_m972465209 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length_m1965771187 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement_m3992910475 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchInvitationsCallback__ctor_m3201750700 (FetchInvitationsCallback_t1518378837 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchInvitationsCallback_Invoke_m1766988060 (FetchInvitationsCallback_t1518378837 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchInvitationsCallback_Invoke_m1766988060((FetchInvitationsCallback_t1518378837 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchInvitationsCallback_t1518378837 (FetchInvitationsCallback_t1518378837 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchInvitationsCallback_BeginInvoke_m655528489_MetadataUsageId;
extern "C"  Il2CppObject * FetchInvitationsCallback_BeginInvoke_m655528489 (FetchInvitationsCallback_t1518378837 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchInvitationsCallback_BeginInvoke_m655528489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchInvitationsCallback_EndInvoke_m3742924476 (FetchInvitationsCallback_t1518378837 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LeaveRoomCallback__ctor_m1232807854 (LeaveRoomCallback_t3947593351 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr)
extern "C"  void LeaveRoomCallback_Invoke_m678727298 (LeaveRoomCallback_t3947593351 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LeaveRoomCallback_Invoke_m678727298((LeaveRoomCallback_t3947593351 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LeaveRoomCallback_t3947593351 (LeaveRoomCallback_t3947593351 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* ResponseStatus_t4049911828_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t LeaveRoomCallback_BeginInvoke_m3176011977_MetadataUsageId;
extern "C"  Il2CppObject * LeaveRoomCallback_BeginInvoke_m3176011977 (LeaveRoomCallback_t3947593351 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LeaveRoomCallback_BeginInvoke_m3176011977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ResponseStatus_t4049911828_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LeaveRoomCallback_EndInvoke_m2801901758 (LeaveRoomCallback_t3947593351 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PlayerSelectUICallback__ctor_m1064711389 (PlayerSelectUICallback_t2633669638 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void PlayerSelectUICallback_Invoke_m4238246155 (PlayerSelectUICallback_t2633669638 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PlayerSelectUICallback_Invoke_m4238246155((PlayerSelectUICallback_t2633669638 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PlayerSelectUICallback_t2633669638 (PlayerSelectUICallback_t2633669638 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayerSelectUICallback_BeginInvoke_m2505453592_MetadataUsageId;
extern "C"  Il2CppObject * PlayerSelectUICallback_BeginInvoke_m2505453592 (PlayerSelectUICallback_t2633669638 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerSelectUICallback_BeginInvoke_m2505453592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void PlayerSelectUICallback_EndInvoke_m3208158573 (PlayerSelectUICallback_t2633669638 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RealTimeRoomCallback__ctor_m1736891858 (RealTimeRoomCallback_t3285480827 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void RealTimeRoomCallback_Invoke_m3601004214 (RealTimeRoomCallback_t3285480827 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RealTimeRoomCallback_Invoke_m3601004214((RealTimeRoomCallback_t3285480827 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RealTimeRoomCallback_t3285480827 (RealTimeRoomCallback_t3285480827 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t RealTimeRoomCallback_BeginInvoke_m2909843395_MetadataUsageId;
extern "C"  Il2CppObject * RealTimeRoomCallback_BeginInvoke_m2909843395 (RealTimeRoomCallback_t3285480827 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RealTimeRoomCallback_BeginInvoke_m2909843395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RealTimeRoomCallback_EndInvoke_m3951429858 (RealTimeRoomCallback_t3285480827 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RoomInboxUICallback__ctor_m2326642747 (RoomInboxUICallback_t3659854740 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void RoomInboxUICallback_Invoke_m408445293 (RoomInboxUICallback_t3659854740 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RoomInboxUICallback_Invoke_m408445293((RoomInboxUICallback_t3659854740 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RoomInboxUICallback_t3659854740 (RoomInboxUICallback_t3659854740 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t RoomInboxUICallback_BeginInvoke_m1937167346_MetadataUsageId;
extern "C"  Il2CppObject * RoomInboxUICallback_BeginInvoke_m1937167346 (RoomInboxUICallback_t3659854740 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RoomInboxUICallback_BeginInvoke_m1937167346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void RoomInboxUICallback_EndInvoke_m3276310475 (RoomInboxUICallback_t3659854740 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SendReliableMessageCallback__ctor_m4033671345 (SendReliableMessageCallback_t2145488650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern "C"  void SendReliableMessageCallback_Invoke_m2265256170 (SendReliableMessageCallback_t2145488650 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SendReliableMessageCallback_Invoke_m2265256170((SendReliableMessageCallback_t2145488650 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_SendReliableMessageCallback_t2145488650 (SendReliableMessageCallback_t2145488650 * __this, int32_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___arg00, reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t SendReliableMessageCallback_BeginInvoke_m1297468265_MetadataUsageId;
extern "C"  Il2CppObject * SendReliableMessageCallback_BeginInvoke_m1297468265 (SendReliableMessageCallback_t2145488650 * __this, int32_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendReliableMessageCallback_BeginInvoke_m1297468265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(MultiplayerStatus_t2675372491_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SendReliableMessageCallback_EndInvoke_m1861116737 (SendReliableMessageCallback_t2145488650 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void WaitingRoomUICallback__ctor_m2094066136 (WaitingRoomUICallback_t206291377 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void WaitingRoomUICallback_Invoke_m1788504944 (WaitingRoomUICallback_t206291377 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WaitingRoomUICallback_Invoke_m1788504944((WaitingRoomUICallback_t206291377 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_WaitingRoomUICallback_t206291377 (WaitingRoomUICallback_t206291377 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t WaitingRoomUICallback_BeginInvoke_m583520245_MetadataUsageId;
extern "C"  Il2CppObject * WaitingRoomUICallback_BeginInvoke_m583520245 (WaitingRoomUICallback_t206291377 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaitingRoomUICallback_BeginInvoke_m583520245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void WaitingRoomUICallback_EndInvoke_m2635270120 (WaitingRoomUICallback_t206291377 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" int32_t DEFAULT_CALL RealTimeRoom_Status(void*);
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t RealTimeRoom_RealTimeRoom_Status_m2039121804 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Status)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL RealTimeRoom_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Description_m2559466261 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL RealTimeRoom_Variant(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoom_RealTimeRoom_Variant_m3087259124 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Variant)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL RealTimeRoom_CreationTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t RealTimeRoom_RealTimeRoom_CreationTime_m1329026200 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_CreationTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL RealTimeRoom_Participants_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Participants_Length_m3618104417 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Participants_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL RealTimeRoom_Participants_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t RealTimeRoom_RealTimeRoom_Participants_GetElement_m3658866019 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Participants_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL RealTimeRoom_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool RealTimeRoom_RealTimeRoom_Valid_m135601220 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL RealTimeRoom_RemainingAutomatchingSlots(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_RemainingAutomatchingSlots(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoom_RealTimeRoom_RemainingAutomatchingSlots_m940488768 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_RemainingAutomatchingSlots)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL RealTimeRoom_AutomatchWaitEstimate(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_AutomatchWaitEstimate(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t RealTimeRoom_RealTimeRoom_AutomatchWaitEstimate_m4268584131 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_AutomatchWaitEstimate)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL RealTimeRoom_CreatingParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeRoom_RealTimeRoom_CreatingParticipant_m596834755 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_CreatingParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL RealTimeRoom_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoom_RealTimeRoom_Id_m1805342290 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoom_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeRoom_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoom_RealTimeRoom_Dispose_m875074419 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoom_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL RealTimeRoomConfig_PlayerIdsToInvite_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_Length_m3300603846 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_PlayerIdsToInvite_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL RealTimeRoomConfig_PlayerIdsToInvite_GetElement(void*, uintptr_t, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_GetElement_m2082844962 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg2' to native representation
	char* ____out_arg2_marshaled = NULL;
	____out_arg2_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg2);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_PlayerIdsToInvite_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())), ____out_arg2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size3.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg2, ____out_arg2_marshaled);

	// Marshaling cleanup of parameter '___out_arg2' native representation
	il2cpp_codegen_marshal_free(____out_arg2_marshaled);
	____out_arg2_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL RealTimeRoomConfig_Variant(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_Variant_m669807416 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Variant)(____self0_marshaled);

	return returnValue;
}
extern "C" int64_t DEFAULT_CALL RealTimeRoomConfig_ExclusiveBitMask(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t RealTimeRoomConfig_RealTimeRoomConfig_ExclusiveBitMask_m3216536272 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_ExclusiveBitMask)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL RealTimeRoomConfig_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool RealTimeRoomConfig_RealTimeRoomConfig_Valid_m3078683912 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL RealTimeRoomConfig_MaximumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MaximumAutomatchingPlayers_m1498051663 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_MaximumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL RealTimeRoomConfig_MinimumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MinimumAutomatchingPlayers_m293273889 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_MinimumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoomConfig_RealTimeRoomConfig_Dispose_m680968503 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Dispose)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse_m1108133859 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___response1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse)(____self0_marshaled, reinterpret_cast<intptr_t>(___response1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_SetVariant(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetVariant_m3493815174 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___variant1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_SetVariant)(____self0_marshaled, ___variant1);

}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_AddPlayerToInvite(void*, char*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_AddPlayerToInvite_m2908520879 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___player_id1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___player_id1' to native representation
	char* ____player_id1_marshaled = NULL;
	____player_id1_marshaled = il2cpp_codegen_marshal_string(___player_id1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_AddPlayerToInvite)(____self0_marshaled, ____player_id1_marshaled);

	// Marshaling cleanup of parameter '___player_id1' native representation
	il2cpp_codegen_marshal_free(____player_id1_marshaled);
	____player_id1_marshaled = NULL;

}
extern "C" intptr_t DEFAULT_CALL RealTimeRoomConfig_Builder_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Construct()
extern "C"  IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Construct_m2185716274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_SetExclusiveBitMask(void*, uint64_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetExclusiveBitMask_m4001679775 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___exclusive_bit_mask1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_SetExclusiveBitMask)(____self0_marshaled, ___exclusive_bit_mask1);

}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers_m4197123561 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___maximum_automatching_players1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers)(____self0_marshaled, ___maximum_automatching_players1);

}
extern "C" intptr_t DEFAULT_CALL RealTimeRoomConfig_Builder_Create(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Create_m1280644352 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_Create)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers_m1383316283 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_automatching_players1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers)(____self0_marshaled, ___minimum_automatching_players1);

}
extern "C" void DEFAULT_CALL RealTimeRoomConfig_Builder_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Dispose_m3244885484 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(RealTimeRoomConfig_Builder_Dispose)(____self0_marshaled);

}
extern "C" uint64_t DEFAULT_CALL Score_Value(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Value(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Score_Score_Value_m4082652309 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(Score_Value)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL Score_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.Score::Score_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool Score_Score_Valid_m2871425144 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(Score_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL Score_Rank(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Rank(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t Score_Score_Rank_m204128196 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(Score_Rank)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL Score_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.Score::Score_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void Score_Score_Dispose_m409937831 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Score_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL Score_Metadata(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Score::Score_Metadata(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  Score_Score_Metadata_m1330209394 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(Score_Metadata)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL ScorePage_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_Dispose_m427433031 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ScorePage_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL ScorePage_TimeSpan(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_TimeSpan_m3308009877 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_TimeSpan)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL ScorePage_LeaderboardId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScorePage_ScorePage_LeaderboardId_m191657541 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_LeaderboardId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL ScorePage_Collection(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Collection(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_Collection_m3994076963 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Collection)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL ScorePage_Start(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Start(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScorePage_ScorePage_Start_m84508075 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Start)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ScorePage_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_Valid_m3695976728 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ScorePage_HasPreviousScorePage(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasPreviousScorePage(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_HasPreviousScorePage_m3906038640 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_HasPreviousScorePage)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ScorePage_HasNextScorePage(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasNextScorePage(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_HasNextScorePage_m3899782644 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_HasNextScorePage)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL ScorePage_PreviousScorePageToken(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_PreviousScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_PreviousScorePageToken_m223637390 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_PreviousScorePageToken)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL ScorePage_NextScorePageToken(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_NextScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_NextScorePageToken_m1126495242 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_NextScorePageToken)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL ScorePage_Entries_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  ScorePage_ScorePage_Entries_Length_m1701400801 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entries_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL ScorePage_Entries_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t ScorePage_ScorePage_Entries_GetElement_m658925533 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entries_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL ScorePage_Entry_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_Entry_Dispose_m536074778 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ScorePage_Entry_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL ScorePage_Entry_PlayerId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_PlayerId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScorePage_ScorePage_Entry_PlayerId_m2556016984 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entry_PlayerId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint64_t DEFAULT_CALL ScorePage_Entry_LastModified(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModified(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScorePage_ScorePage_Entry_LastModified_m483083204 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entry_LastModified)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL ScorePage_Entry_Score(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Score(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScorePage_ScorePage_Entry_Score_m161561410 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entry_Score)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL ScorePage_Entry_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_Entry_Valid_m595583147 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entry_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL ScorePage_Entry_LastModifiedTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScorePage_ScorePage_Entry_LastModifiedTime_m1928048497 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_Entry_LastModifiedTime)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ScorePage_ScorePageToken_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScorePage_ScorePage_ScorePageToken_Valid_m3846258869 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScorePage_ScorePageToken_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL ScorePage_ScorePageToken_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScorePage_ScorePage_ScorePageToken_Dispose_m3670275724 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ScorePage_ScorePageToken_Dispose)(____self0_marshaled);

}
extern "C" uint64_t DEFAULT_CALL ScoreSummary_ApproximateNumberOfScores(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_ApproximateNumberOfScores(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t ScoreSummary_ScoreSummary_ApproximateNumberOfScores_m681694017 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_ApproximateNumberOfScores)(____self0_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL ScoreSummary_TimeSpan(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScoreSummary_ScoreSummary_TimeSpan_m1941048333 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_TimeSpan)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL ScoreSummary_LeaderboardId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  ScoreSummary_ScoreSummary_LeaderboardId_m1270467645 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_LeaderboardId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL ScoreSummary_Collection(void*);
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Collection(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t ScoreSummary_ScoreSummary_Collection_m942568027 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_Collection)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL ScoreSummary_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool ScoreSummary_ScoreSummary_Valid_m3692325664 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL ScoreSummary_CurrentPlayerScore(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_CurrentPlayerScore(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t ScoreSummary_ScoreSummary_CurrentPlayerScore_m2788018287 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(ScoreSummary_CurrentPlayerScore)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL ScoreSummary_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void ScoreSummary_ScoreSummary_Dispose_m93297487 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(ScoreSummary_Dispose)(____self0_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Sentinels_AutomatchingParticipant();
// System.IntPtr GooglePlayGames.Native.Cwrapper.Sentinels::Sentinels_AutomatchingParticipant()
extern "C"  IntPtr_t Sentinels_Sentinels_AutomatchingParticipant_m2704397095 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Sentinels_AutomatchingParticipant)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotManager_FetchAll(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_FetchAll_m854890322 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchAllCallback_t677738899 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_FetchAll)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL SnapshotManager_ShowSelectUIOperation(void*, int8_t, int8_t, uint32_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ShowSelectUIOperation(System.Runtime.InteropServices.HandleRef,System.Boolean,System.Boolean,System.UInt32,System.String,GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_ShowSelectUIOperation_m596028194 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, bool ___allow_create1, bool ___allow_delete2, uint32_t ___max_snapshots3, String_t* ___title4, SnapshotSelectUICallback_t2531066752 * ___callback5, IntPtr_t ___callback_arg6, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int8_t, int8_t, uint32_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___title4' to native representation
	char* ____title4_marshaled = NULL;
	____title4_marshaled = il2cpp_codegen_marshal_string(___title4);

	// Marshaling of parameter '___callback5' to native representation
	Il2CppMethodPointer ____callback5_marshaled = NULL;
	____callback5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_ShowSelectUIOperation)(____self0_marshaled, ___allow_create1, ___allow_delete2, ___max_snapshots3, ____title4_marshaled, ____callback5_marshaled, reinterpret_cast<intptr_t>(___callback_arg6.get_m_value_0()));

	// Marshaling cleanup of parameter '___title4' native representation
	il2cpp_codegen_marshal_free(____title4_marshaled);
	____title4_marshaled = NULL;

}
extern "C" void DEFAULT_CALL SnapshotManager_Read(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Read(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_Read_m3479849658 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___snapshot_metadata1, ReadCallback_t1569945378 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_Read)(____self0_marshaled, reinterpret_cast<intptr_t>(___snapshot_metadata1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL SnapshotManager_Commit(void*, intptr_t, intptr_t, uint8_t*, uintptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Commit(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.Byte[],System.UIntPtr,GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_Commit_m1395112938 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___snapshot_metadata1, IntPtr_t ___metadata_change2, ByteU5BU5D_t4260760469* ___data3, UIntPtr_t  ___data_size4, CommitCallback_t1623255043 * ___callback5, IntPtr_t ___callback_arg6, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, uint8_t*, uintptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___data3' to native representation
	uint8_t* ____data3_marshaled = NULL;
	____data3_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data3);

	// Marshaling of parameter '___callback5' to native representation
	Il2CppMethodPointer ____callback5_marshaled = NULL;
	____callback5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_Commit)(____self0_marshaled, reinterpret_cast<intptr_t>(___snapshot_metadata1.get_m_value_0()), reinterpret_cast<intptr_t>(___metadata_change2.get_m_value_0()), ____data3_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___data_size4.get__pointer_1())), ____callback5_marshaled, reinterpret_cast<intptr_t>(___callback_arg6.get_m_value_0()));

}
extern "C" void DEFAULT_CALL SnapshotManager_Open(void*, int32_t, char*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Open(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy,GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_Open_m2153573951 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, String_t* ___file_name2, int32_t ___conflict_policy3, OpenCallback_t1210339286 * ___callback4, IntPtr_t ___callback_arg5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___file_name2' to native representation
	char* ____file_name2_marshaled = NULL;
	____file_name2_marshaled = il2cpp_codegen_marshal_string(___file_name2);

	// Marshaling of parameter '___callback4' to native representation
	Il2CppMethodPointer ____callback4_marshaled = NULL;
	____callback4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback4));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_Open)(____self0_marshaled, ___data_source1, ____file_name2_marshaled, ___conflict_policy3, ____callback4_marshaled, reinterpret_cast<intptr_t>(___callback_arg5.get_m_value_0()));

	// Marshaling cleanup of parameter '___file_name2' native representation
	il2cpp_codegen_marshal_free(____file_name2_marshaled);
	____file_name2_marshaled = NULL;

}
extern "C" void DEFAULT_CALL SnapshotManager_ResolveConflict(void*, intptr_t, intptr_t, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ResolveConflict(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.String,GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_ResolveConflict_m1052197715 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___snapshot_metadata1, IntPtr_t ___metadata_change2, String_t* ___conflict_id3, CommitCallback_t1623255043 * ___callback4, IntPtr_t ___callback_arg5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___conflict_id3' to native representation
	char* ____conflict_id3_marshaled = NULL;
	____conflict_id3_marshaled = il2cpp_codegen_marshal_string(___conflict_id3);

	// Marshaling of parameter '___callback4' to native representation
	Il2CppMethodPointer ____callback4_marshaled = NULL;
	____callback4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback4));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_ResolveConflict)(____self0_marshaled, reinterpret_cast<intptr_t>(___snapshot_metadata1.get_m_value_0()), reinterpret_cast<intptr_t>(___metadata_change2.get_m_value_0()), ____conflict_id3_marshaled, ____callback4_marshaled, reinterpret_cast<intptr_t>(___callback_arg5.get_m_value_0()));

	// Marshaling cleanup of parameter '___conflict_id3' native representation
	il2cpp_codegen_marshal_free(____conflict_id3_marshaled);
	____conflict_id3_marshaled = NULL;

}
extern "C" void DEFAULT_CALL SnapshotManager_Delete(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Delete(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void SnapshotManager_SnapshotManager_Delete_m1855694989 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___snapshot_metadata1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_Delete)(____self0_marshaled, reinterpret_cast<intptr_t>(___snapshot_metadata1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL SnapshotManager_FetchAllResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotManager_SnapshotManager_FetchAllResponse_Dispose_m3323893884 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_FetchAllResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL SnapshotManager_FetchAllResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t SnapshotManager_SnapshotManager_FetchAllResponse_GetStatus_m279620540 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_FetchAllResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotManager_FetchAllResponse_GetData_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  SnapshotManager_SnapshotManager_FetchAllResponse_GetData_Length_m2754352268 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_FetchAllResponse_GetData_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_FetchAllResponse_GetData_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_FetchAllResponse_GetData_GetElement_m2092587160 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_FetchAllResponse_GetData_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotManager_OpenResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotManager_SnapshotManager_OpenResponse_Dispose_m2186024831 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL SnapshotManager_OpenResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t SnapshotManager_SnapshotManager_OpenResponse_GetStatus_m356190316 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetData_m2569209387 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotManager_SnapshotManager_OpenResponse_GetConflictId_m3386434098 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_GetConflictId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictOriginal(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictOriginal(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetConflictOriginal_m661418628 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_GetConflictOriginal)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictUnmerged(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictUnmerged(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetConflictUnmerged_m4226482872 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_OpenResponse_GetConflictUnmerged)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotManager_CommitResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotManager_SnapshotManager_CommitResponse_Dispose_m173916844 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_CommitResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL SnapshotManager_CommitResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t SnapshotManager_SnapshotManager_CommitResponse_GetStatus_m3492871084 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_CommitResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_CommitResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_CommitResponse_GetData_m901309080 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_CommitResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotManager_ReadResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotManager_SnapshotManager_ReadResponse_Dispose_m2094027723 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_ReadResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL SnapshotManager_ReadResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t SnapshotManager_SnapshotManager_ReadResponse_GetStatus_m4089324235 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_ReadResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotManager_ReadResponse_GetData(void*, uint8_t*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_GetData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotManager_SnapshotManager_ReadResponse_GetData_m2577370730 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	uint8_t* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_ReadResponse_GetData)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	if (____out_arg1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_array_out<uint8_t>(____out_arg1_marshaled, ___out_arg1);
	}

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_Dispose_m3717262889 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotManager_SnapshotSelectUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetStatus_m1545112284 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_SnapshotSelectUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetData_m2615366741 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotManager_SnapshotSelectUIResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CommitCallback__ctor_m1437284650 (CommitCallback_t1623255043 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void CommitCallback_Invoke_m2903115358 (CommitCallback_t1623255043 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CommitCallback_Invoke_m2903115358((CommitCallback_t1623255043 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_CommitCallback_t1623255043 (CommitCallback_t1623255043 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CommitCallback_BeginInvoke_m2039507939_MetadataUsageId;
extern "C"  Il2CppObject * CommitCallback_BeginInvoke_m2039507939 (CommitCallback_t1623255043 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CommitCallback_BeginInvoke_m2039507939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CommitCallback_EndInvoke_m2081280058 (CommitCallback_t1623255043 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchAllCallback__ctor_m3335956666 (FetchAllCallback_t677738899 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchAllCallback_Invoke_m1632405710 (FetchAllCallback_t677738899 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchAllCallback_Invoke_m1632405710((FetchAllCallback_t677738899 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchAllCallback_t677738899 (FetchAllCallback_t677738899 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchAllCallback_BeginInvoke_m3359518547_MetadataUsageId;
extern "C"  Il2CppObject * FetchAllCallback_BeginInvoke_m3359518547 (FetchAllCallback_t677738899 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchAllCallback_BeginInvoke_m3359518547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchAllCallback_EndInvoke_m3003090890 (FetchAllCallback_t677738899 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OpenCallback__ctor_m2093256061 (OpenCallback_t1210339286 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void OpenCallback_Invoke_m1763392619 (OpenCallback_t1210339286 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OpenCallback_Invoke_m1763392619((OpenCallback_t1210339286 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_OpenCallback_t1210339286 (OpenCallback_t1210339286 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OpenCallback_BeginInvoke_m3849002992_MetadataUsageId;
extern "C"  Il2CppObject * OpenCallback_BeginInvoke_m3849002992 (OpenCallback_t1210339286 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenCallback_BeginInvoke_m3849002992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OpenCallback_EndInvoke_m902579725 (OpenCallback_t1210339286 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadCallback__ctor_m767491785 (ReadCallback_t1569945378 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void ReadCallback_Invoke_m3614373023 (ReadCallback_t1569945378 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReadCallback_Invoke_m3614373023((ReadCallback_t1569945378 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ReadCallback_t1569945378 (ReadCallback_t1569945378 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t ReadCallback_BeginInvoke_m4096758820_MetadataUsageId;
extern "C"  Il2CppObject * ReadCallback_BeginInvoke_m4096758820 (ReadCallback_t1569945378 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadCallback_BeginInvoke_m4096758820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ReadCallback_EndInvoke_m149634393 (ReadCallback_t1569945378 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SnapshotSelectUICallback__ctor_m777984039 (SnapshotSelectUICallback_t2531066752 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void SnapshotSelectUICallback_Invoke_m3939632897 (SnapshotSelectUICallback_t2531066752 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SnapshotSelectUICallback_Invoke_m3939632897((SnapshotSelectUICallback_t2531066752 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_SnapshotSelectUICallback_t2531066752 (SnapshotSelectUICallback_t2531066752 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t SnapshotSelectUICallback_BeginInvoke_m2114814598_MetadataUsageId;
extern "C"  Il2CppObject * SnapshotSelectUICallback_BeginInvoke_m2114814598 (SnapshotSelectUICallback_t2531066752 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SnapshotSelectUICallback_BeginInvoke_m2114814598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C"  void SnapshotSelectUICallback_EndInvoke_m4175082935 (SnapshotSelectUICallback_t2531066752 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" void DEFAULT_CALL SnapshotMetadata_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadata_SnapshotMetadata_Dispose_m84177869 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadata_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL SnapshotMetadata_CoverImageURL(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_CoverImageURL(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_CoverImageURL_m2203124652 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_CoverImageURL)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotMetadata_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_Description_m3412118779 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL SnapshotMetadata_IsOpen(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_IsOpen(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadata_SnapshotMetadata_IsOpen_m1972405166 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_IsOpen)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotMetadata_FileName(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_FileName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadata_SnapshotMetadata_FileName_m4109404960 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_FileName)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL SnapshotMetadata_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadata_SnapshotMetadata_Valid_m811751198 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" int64_t DEFAULT_CALL SnapshotMetadata_PlayedTime(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t SnapshotMetadata_SnapshotMetadata_PlayedTime_m3644402463 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_PlayedTime)(____self0_marshaled);

	return returnValue;
}
extern "C" int64_t DEFAULT_CALL SnapshotMetadata_LastModifiedTime(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t SnapshotMetadata_SnapshotMetadata_LastModifiedTime_m3105604107 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadata_LastModifiedTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL SnapshotMetadataChange_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  SnapshotMetadataChange_SnapshotMetadataChange_Description_m3578059163 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL SnapshotMetadataChange_Image(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Image(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotMetadataChange_SnapshotMetadataChange_Image_m1929964574 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Image)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL SnapshotMetadataChange_PlayedTimeIsChanged(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTimeIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_PlayedTimeIsChanged_m1083010508 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_PlayedTimeIsChanged)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL SnapshotMetadataChange_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_Valid_m3444210814 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL SnapshotMetadataChange_PlayedTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t SnapshotMetadataChange_SnapshotMetadataChange_PlayedTime_m2411950354 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_PlayedTime)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL SnapshotMetadataChange_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadataChange_SnapshotMetadataChange_Dispose_m2040672045 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Dispose)(____self0_marshaled);

}
extern "C" int8_t DEFAULT_CALL SnapshotMetadataChange_ImageIsChanged(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_ImageIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_ImageIsChanged_m2404129033 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_ImageIsChanged)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL SnapshotMetadataChange_DescriptionIsChanged(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_DescriptionIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C"  bool SnapshotMetadataChange_SnapshotMetadataChange_DescriptionIsChanged_m2503180232 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_DescriptionIsChanged)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL SnapshotMetadataChange_Builder_SetDescription(void*, char*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetDescription_m3675148389 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___description1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___description1' to native representation
	char* ____description1_marshaled = NULL;
	____description1_marshaled = il2cpp_codegen_marshal_string(___description1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_SetDescription)(____self0_marshaled, ____description1_marshaled);

	// Marshaling cleanup of parameter '___description1' native representation
	il2cpp_codegen_marshal_free(____description1_marshaled);
	____description1_marshaled = NULL;

}
extern "C" intptr_t DEFAULT_CALL SnapshotMetadataChange_Builder_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Construct()
extern "C"  IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Construct_m2235991890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotMetadataChange_Builder_SetPlayedTime(void*, uint64_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetPlayedTime(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetPlayedTime_m1518428836 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___played_time1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_SetPlayedTime)(____self0_marshaled, ___played_time1);

}
extern "C" void DEFAULT_CALL SnapshotMetadataChange_Builder_SetCoverImageFromPngData(void*, uint8_t*, uintptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetCoverImageFromPngData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetCoverImageFromPngData_m841557980 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___png_data1, UIntPtr_t  ___png_data_size2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___png_data1' to native representation
	uint8_t* ____png_data1_marshaled = NULL;
	____png_data1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___png_data1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_SetCoverImageFromPngData)(____self0_marshaled, ____png_data1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___png_data_size2.get__pointer_1())));

}
extern "C" intptr_t DEFAULT_CALL SnapshotMetadataChange_Builder_Create(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Create_m2813109216 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_Create)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL SnapshotMetadataChange_Builder_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Dispose_m2684579340 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SnapshotMetadataChange_Builder_Dispose)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL StatsManager_FetchForPlayer(void*, int32_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayer(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback,System.IntPtr)
extern "C"  void StatsManager_StatsManager_FetchForPlayer_m163136903 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchForPlayerCallback_t1995639285 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StatsManager_FetchForPlayer)(____self0_marshaled, ___data_source1, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL StatsManager_FetchForPlayerResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void StatsManager_StatsManager_FetchForPlayerResponse_Dispose_m2997693225 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StatsManager_FetchForPlayerResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL StatsManager_FetchForPlayerResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t StatsManager_StatsManager_FetchForPlayerResponse_GetStatus_m333399849 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(StatsManager_FetchForPlayerResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL StatsManager_FetchForPlayerResponse_GetData(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t StatsManager_StatsManager_FetchForPlayerResponse_GetData_m3796184149 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(StatsManager_FetchForPlayerResponse_GetData)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void FetchForPlayerCallback__ctor_m2851840332 (FetchForPlayerCallback_t1995639285 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C"  void FetchForPlayerCallback_Invoke_m3804668540 (FetchForPlayerCallback_t1995639285 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FetchForPlayerCallback_Invoke_m3804668540((FetchForPlayerCallback_t1995639285 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FetchForPlayerCallback_t1995639285 (FetchForPlayerCallback_t1995639285 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t, intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>(___arg00.get_m_value_0()), reinterpret_cast<intptr_t>(___arg11.get_m_value_0()));

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FetchForPlayerCallback_BeginInvoke_m2968634249_MetadataUsageId;
extern "C"  Il2CppObject * FetchForPlayerCallback_BeginInvoke_m2968634249 (FetchForPlayerCallback_t1995639285 * __this, IntPtr_t ___arg00, IntPtr_t ___arg11, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FetchForPlayerCallback_BeginInvoke_m2968634249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback::EndInvoke(System.IAsyncResult)
extern "C"  void FetchForPlayerCallback_EndInvoke_m1149999452 (FetchForPlayerCallback_t1995639285 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatch_AutomatchingSlotsAvailable(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_AutomatchingSlotsAvailable_m2902189243 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_AutomatchingSlotsAvailable)(____self0_marshaled);

	return returnValue;
}
extern "C" uint64_t DEFAULT_CALL TurnBasedMatch_CreationTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t TurnBasedMatch_TurnBasedMatch_CreationTime_m4226460678 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_CreationTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_Participants_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Participants_Length_m2778357555 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Participants_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_Participants_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_Participants_GetElement_m3392342993 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Participants_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatch_Version(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Version(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Version_m2006707929 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Version)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_ParticipantResults(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_ParticipantResults(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_ParticipantResults_m1708809668 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_ParticipantResults)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int32_t DEFAULT_CALL TurnBasedMatch_Status(void*);
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMatch_TurnBasedMatch_Status_m3735410325 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Status)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_Description(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Description_m409076355 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Description)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_PendingParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PendingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_PendingParticipant_m259424861 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_PendingParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatch_Variant(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Variant_m425458246 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Variant)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL TurnBasedMatch_HasPreviousMatchData(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasPreviousMatchData(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasPreviousMatchData_m3805740416 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_HasPreviousMatchData)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_Data(void*, uint8_t*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Data(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Data_m352361334 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	uint8_t* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Data)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	if (____out_arg1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_array_out<uint8_t>(____out_arg1_marshaled, ___out_arg1);
	}

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_LastUpdatingParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_LastUpdatingParticipant_m2087426610 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_LastUpdatingParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" int8_t DEFAULT_CALL TurnBasedMatch_HasData(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasData(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasData_m2996543742 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_HasData)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_SuggestedNextParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_SuggestedNextParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_SuggestedNextParticipant_m1768475230 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_SuggestedNextParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_PreviousMatchData(void*, uint8_t*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PreviousMatchData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_PreviousMatchData_m1923701096 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, ByteU5BU5D_t4260760469* ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	uint8_t* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_PreviousMatchData)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	if (____out_arg1_marshaled != NULL)
	{
		il2cpp_codegen_marshal_array_out<uint8_t>(____out_arg1_marshaled, ___out_arg1);
	}

	return _returnValue_unmarshaled;
}
extern "C" uint64_t DEFAULT_CALL TurnBasedMatch_LastUpdateTime(void*);
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdateTime(System.Runtime.InteropServices.HandleRef)
extern "C"  uint64_t TurnBasedMatch_TurnBasedMatch_LastUpdateTime_m571059494 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint64_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_LastUpdateTime)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_RematchId(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_RematchId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_RematchId_m861563570 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_RematchId)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatch_Number(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Number(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatch_TurnBasedMatch_Number_m1393226596 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Number)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL TurnBasedMatch_HasRematchId(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasRematchId(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_HasRematchId_m1583527829 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_HasRematchId)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL TurnBasedMatch_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatch_TurnBasedMatch_Valid_m1987500182 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatch_CreatingParticipant(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatch_TurnBasedMatch_CreatingParticipant_m2180183573 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_CreatingParticipant)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatch_Id(void*, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Id_m2011101988 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, StringBuilder_t243639308 * ___out_arg1, UIntPtr_t  ___out_size2, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg1' to native representation
	char* ____out_arg1_marshaled = NULL;
	____out_arg1_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg1);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Id)(____self0_marshaled, ____out_arg1_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size2.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg1' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg1, ____out_arg1_marshaled);

	// Marshaling cleanup of parameter '___out_arg1' native representation
	il2cpp_codegen_marshal_free(____out_arg1_marshaled);
	____out_arg1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMatch_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatch_TurnBasedMatch_Dispose_m3970694469 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatch_Dispose)(____self0_marshaled);

}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatchConfig_PlayerIdsToInvite_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_Length_m353120500 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_PlayerIdsToInvite_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(void*, uintptr_t, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_GetElement_m3736469840 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg2' to native representation
	char* ____out_arg2_marshaled = NULL;
	____out_arg2_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg2);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_PlayerIdsToInvite_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())), ____out_arg2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size3.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg2, ____out_arg2_marshaled);

	// Marshaling cleanup of parameter '___out_arg2' native representation
	il2cpp_codegen_marshal_free(____out_arg2_marshaled);
	____out_arg2_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatchConfig_Variant(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_Variant_m4247830218 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Variant)(____self0_marshaled);

	return returnValue;
}
extern "C" int64_t DEFAULT_CALL TurnBasedMatchConfig_ExclusiveBitMask(void*);
// System.Int64 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C"  int64_t TurnBasedMatchConfig_TurnBasedMatchConfig_ExclusiveBitMask_m3723863422 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int64_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_ExclusiveBitMask)(____self0_marshaled);

	return returnValue;
}
extern "C" int8_t DEFAULT_CALL TurnBasedMatchConfig_Valid(void*);
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C"  bool TurnBasedMatchConfig_TurnBasedMatchConfig_Valid_m3020583066 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int8_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Valid)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatchConfig_MaximumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MaximumAutomatchingPlayers_m3651177341 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_MaximumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMatchConfig_MinimumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MinimumAutomatchingPlayers_m2446399567 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_MinimumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfig_TurnBasedMatchConfig_Dispose_m2127172169 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Dispose)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse_m3393489795 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___response1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse)(____self0_marshaled, reinterpret_cast<intptr_t>(___response1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetVariant(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetVariant_m4181722918 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___variant1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_SetVariant)(____self0_marshaled, ___variant1);

}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_AddPlayerToInvite(void*, char*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_AddPlayerToInvite_m2963966479 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___player_id1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___player_id1' to native representation
	char* ____player_id1_marshaled = NULL;
	____player_id1_marshaled = il2cpp_codegen_marshal_string(___player_id1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_AddPlayerToInvite)(____self0_marshaled, ____player_id1_marshaled);

	// Marshaling cleanup of parameter '___player_id1' native representation
	il2cpp_codegen_marshal_free(____player_id1_marshaled);
	____player_id1_marshaled = NULL;

}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatchConfig_Builder_Construct();
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Construct()
extern "C"  IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Construct_m2904048914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_Construct)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetExclusiveBitMask(void*, uint64_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetExclusiveBitMask_m1450326527 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint64_t ___exclusive_bit_mask1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_SetExclusiveBitMask)(____self0_marshaled, ___exclusive_bit_mask1);

}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers_m30375497 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___maximum_automatching_players1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers)(____self0_marshaled, ___maximum_automatching_players1);

}
extern "C" intptr_t DEFAULT_CALL TurnBasedMatchConfig_Builder_Create(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Create_m3121774112 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_Create)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(void*, uint32_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers_m1511535515 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_automatching_players1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers)(____self0_marshaled, ___minimum_automatching_players1);

}
extern "C" void DEFAULT_CALL TurnBasedMatchConfig_Builder_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Dispose_m1368534604 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMatchConfig_Builder_Dispose)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_ShowPlayerSelectUI(void*, uint32_t, uint32_t, int8_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowPlayerSelectUI(System.Runtime.InteropServices.HandleRef,System.UInt32,System.UInt32,System.Boolean,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowPlayerSelectUI_m3419203364 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, uint32_t ___minimum_players1, uint32_t ___maximum_players2, bool ___allow_automatch3, PlayerSelectUICallback_t4277124957 * ___callback4, IntPtr_t ___callback_arg5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t, uint32_t, int8_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback4' to native representation
	Il2CppMethodPointer ____callback4_marshaled = NULL;
	____callback4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback4));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_ShowPlayerSelectUI)(____self0_marshaled, ___minimum_players1, ___maximum_players2, ___allow_automatch3, ____callback4_marshaled, reinterpret_cast<intptr_t>(___callback_arg5.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_CancelMatch(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CancelMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CancelMatch_m3919581137 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, MultiplayerStatusCallback_t994271114 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_CancelMatch)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_DismissMatch(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissMatch_m3038299453 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_DismissMatch)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_ShowMatchInboxUI(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowMatchInboxUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowMatchInboxUI_m2424059789 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, MatchInboxUICallback_t3070391457 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_ShowMatchInboxUI)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_SynchronizeData(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_SynchronizeData(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_SynchronizeData_m3324325442 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_SynchronizeData)(____self0_marshaled);

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_Rematch(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_Rematch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_Rematch_m1408642129 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, TurnBasedMatchCallback_t3800683771 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_Rematch)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_DismissInvitation(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissInvitation_m763348857 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_DismissInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_FetchMatch(void*, char*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatch(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatch_m2638862366 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, String_t* ___match_id1, TurnBasedMatchCallback_t3800683771 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___match_id1' to native representation
	char* ____match_id1_marshaled = NULL;
	____match_id1_marshaled = il2cpp_codegen_marshal_string(___match_id1);

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_FetchMatch)(____self0_marshaled, ____match_id1_marshaled, ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

	// Marshaling cleanup of parameter '___match_id1' native representation
	il2cpp_codegen_marshal_free(____match_id1_marshaled);
	____match_id1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_DeclineInvitation(void*, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DeclineInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DeclineInvitation_m2784931045 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_DeclineInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(void*, intptr_t, uint8_t*, uintptr_t, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FinishMatchDuringMyTurn_m2572900911 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, ByteU5BU5D_t4260760469* ___match_data2, UIntPtr_t  ___match_data_size3, IntPtr_t ___results4, TurnBasedMatchCallback_t3800683771 * ___callback5, IntPtr_t ___callback_arg6, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, uint8_t*, uintptr_t, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___match_data2' to native representation
	uint8_t* ____match_data2_marshaled = NULL;
	____match_data2_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___match_data2);

	// Marshaling of parameter '___callback5' to native representation
	Il2CppMethodPointer ____callback5_marshaled = NULL;
	____callback5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback5));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_FinishMatchDuringMyTurn)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____match_data2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___match_data_size3.get__pointer_1())), reinterpret_cast<intptr_t>(___results4.get_m_value_0()), ____callback5_marshaled, reinterpret_cast<intptr_t>(___callback_arg6.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_FetchMatches(void*, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatches(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatches_m3205772926 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, TurnBasedMatchesCallback_t1856996905 * ___callback1, IntPtr_t ___callback_arg2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback1' to native representation
	Il2CppMethodPointer ____callback1_marshaled = NULL;
	____callback1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback1));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_FetchMatches)(____self0_marshaled, ____callback1_marshaled, reinterpret_cast<intptr_t>(___callback_arg2.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_CreateTurnBasedMatch(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CreateTurnBasedMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CreateTurnBasedMatch_m4112807276 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___config1, TurnBasedMatchCallback_t3800683771 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_CreateTurnBasedMatch)(____self0_marshaled, reinterpret_cast<intptr_t>(___config1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_AcceptInvitation(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_AcceptInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_AcceptInvitation_m3355169462 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___invitation1, TurnBasedMatchCallback_t3800683771 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_AcceptInvitation)(____self0_marshaled, reinterpret_cast<intptr_t>(___invitation1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_TakeMyTurn(void*, intptr_t, uint8_t*, uintptr_t, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TakeMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TakeMyTurn_m1018634945 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, ByteU5BU5D_t4260760469* ___match_data2, UIntPtr_t  ___match_data_size3, IntPtr_t ___results4, IntPtr_t ___next_participant5, TurnBasedMatchCallback_t3800683771 * ___callback6, IntPtr_t ___callback_arg7, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, uint8_t*, uintptr_t, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___match_data2' to native representation
	uint8_t* ____match_data2_marshaled = NULL;
	____match_data2_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___match_data2);

	// Marshaling of parameter '___callback6' to native representation
	Il2CppMethodPointer ____callback6_marshaled = NULL;
	____callback6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback6));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TakeMyTurn)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____match_data2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___match_data_size3.get__pointer_1())), reinterpret_cast<intptr_t>(___results4.get_m_value_0()), reinterpret_cast<intptr_t>(___next_participant5.get_m_value_0()), ____callback6_marshaled, reinterpret_cast<intptr_t>(___callback_arg7.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_ConfirmPendingCompletion(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ConfirmPendingCompletion(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ConfirmPendingCompletion_m3075365316 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, TurnBasedMatchCallback_t3800683771 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_ConfirmPendingCompletion)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn_m2337901432 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, IntPtr_t ___next_participant2, MultiplayerStatusCallback_t994271114 * ___callback3, IntPtr_t ___callback_arg4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback3' to native representation
	Il2CppMethodPointer ____callback3_marshaled = NULL;
	____callback3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback3));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), reinterpret_cast<intptr_t>(___next_participant2.get_m_value_0()), ____callback3_marshaled, reinterpret_cast<intptr_t>(___callback_arg4.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(void*, intptr_t, Il2CppMethodPointer, intptr_t);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn_m1374199278 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, IntPtr_t ___match1, MultiplayerStatusCallback_t994271114 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t, Il2CppMethodPointer, intptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn)(____self0_marshaled, reinterpret_cast<intptr_t>(___match1.get_m_value_0()), ____callback2_marshaled, reinterpret_cast<intptr_t>(___callback_arg3.get_m_value_0()));

}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose_m1721808164 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus_m2848141765 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch_m557914715 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose_m4225394450 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus_m3228912179 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length_m1819430028 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement_m1846925266 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length_m3099920668 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement_m149252610 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length_m3060513636 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement_m979554880 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length_m42170552 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(void*, uintptr_t);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement_m759843948 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose_m4276676362 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus_m1413420669 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(void*);
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch_m2844274293 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch)(____self0_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(void*);
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose_m1144619782 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose)(____self0_marshaled);

}
extern "C" int32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(void*);
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus_m1965162745 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus)(____self0_marshaled);

	return returnValue;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(void*);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(System.Runtime.InteropServices.HandleRef)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length_m2892168899 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length)(____self0_marshaled);

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	return _returnValue_unmarshaled;
}
extern "C" uintptr_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(void*, uintptr_t, char*, uintptr_t);
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement_m3618641567 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, UIntPtr_t  ___index1, StringBuilder_t243639308 * ___out_arg2, UIntPtr_t  ___out_size3, const MethodInfo* method)
{
	typedef uintptr_t (DEFAULT_CALL *PInvokeFunc) (void*, uintptr_t, char*, uintptr_t);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___out_arg2' to native representation
	char* ____out_arg2_marshaled = NULL;
	____out_arg2_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg2);

	// Native function invocation
	uintptr_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement)(____self0_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___index1.get__pointer_1())), ____out_arg2_marshaled, static_cast<uintptr_t>(reinterpret_cast<intptr_t>(___out_size3.get__pointer_1())));

	// Marshaling of return value back from native representation
	UIntPtr_t  _returnValue_unmarshaled;
	_returnValue_unmarshaled.set__pointer_1(reinterpret_cast<void*>((uintptr_t)returnValue));

	// Marshaling of parameter '___out_arg2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg2, ____out_arg2_marshaled);

	// Marshaling cleanup of parameter '___out_arg2' native representation
	il2cpp_codegen_marshal_free(____out_arg2_marshaled);
	____out_arg2_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers_m3176157530 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
extern "C" uint32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(void*);
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C"  uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers_m85968008 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___self0' to native representation
	void* ____self0_marshaled = NULL;
	____self0_marshaled = ___self0.get_handle_1().get_m_value_0();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers)(____self0_marshaled);

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
