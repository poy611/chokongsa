﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t2948332186;

#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3227540113.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t624170136  : public JsonContainerContract_t3227540113
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_t2948332186 * ___U3CISerializableCreatorU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t624170136, ___U3CISerializableCreatorU3Ek__BackingField_27)); }
	inline ObjectConstructor_1_t2948332186 * get_U3CISerializableCreatorU3Ek__BackingField_27() const { return ___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline ObjectConstructor_1_t2948332186 ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_27() { return &___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_27(ObjectConstructor_1_t2948332186 * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CISerializableCreatorU3Ek__BackingField_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
