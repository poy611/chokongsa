﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Qu1884049229.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
struct  GetQuaternionEulerAngles_t4150772157  : public QuaternionBaseAction_t1884049229
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::quaternion
	FsmQuaternion_t3871136040 * ___quaternion_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::eulerAngles
	FsmVector3_t533912882 * ___eulerAngles_14;

public:
	inline static int32_t get_offset_of_quaternion_13() { return static_cast<int32_t>(offsetof(GetQuaternionEulerAngles_t4150772157, ___quaternion_13)); }
	inline FsmQuaternion_t3871136040 * get_quaternion_13() const { return ___quaternion_13; }
	inline FsmQuaternion_t3871136040 ** get_address_of_quaternion_13() { return &___quaternion_13; }
	inline void set_quaternion_13(FsmQuaternion_t3871136040 * value)
	{
		___quaternion_13 = value;
		Il2CppCodeGenWriteBarrier(&___quaternion_13, value);
	}

	inline static int32_t get_offset_of_eulerAngles_14() { return static_cast<int32_t>(offsetof(GetQuaternionEulerAngles_t4150772157, ___eulerAngles_14)); }
	inline FsmVector3_t533912882 * get_eulerAngles_14() const { return ___eulerAngles_14; }
	inline FsmVector3_t533912882 ** get_address_of_eulerAngles_14() { return &___eulerAngles_14; }
	inline void set_eulerAngles_14(FsmVector3_t533912882 * value)
	{
		___eulerAngles_14 = value;
		Il2CppCodeGenWriteBarrier(&___eulerAngles_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
