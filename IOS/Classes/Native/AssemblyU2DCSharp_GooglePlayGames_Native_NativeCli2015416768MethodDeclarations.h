﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53
struct U3CLoadUsersU3Ec__AnonStorey53_t2015416768;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53::.ctor()
extern "C"  void U3CLoadUsersU3Ec__AnonStorey53__ctor_m1047014315 (U3CLoadUsersU3Ec__AnonStorey53_t2015416768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<LoadUsers>c__AnonStorey52/<LoadUsers>c__AnonStorey53::<>m__39()
extern "C"  void U3CLoadUsersU3Ec__AnonStorey53_U3CU3Em__39_m3466517658 (U3CLoadUsersU3Ec__AnonStorey53_t2015416768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
