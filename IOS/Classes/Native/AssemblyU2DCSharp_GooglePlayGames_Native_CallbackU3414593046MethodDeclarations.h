﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m3400352285_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m3400352285(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey43_3__ctor_m3400352285_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3/<ToOnGameThread>c__AnonStorey43`3<System.Object,System.Object,System.Object>::<>m__15()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m1576478410_gshared (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m1576478410(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey43_3_t3414593046 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey43_3_U3CU3Em__15_m1576478410_gshared)(__this, method)
