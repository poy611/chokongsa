﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919299614.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m964065322_gshared (Enumerator_t919299614 * __this, Dictionary_2_t304363560 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m964065322(__this, ___host0, method) ((  void (*) (Enumerator_t919299614 *, Dictionary_2_t304363560 *, const MethodInfo*))Enumerator__ctor_m964065322_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2046383927_gshared (Enumerator_t919299614 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2046383927(__this, method) ((  Il2CppObject * (*) (Enumerator_t919299614 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2046383927_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1554059851_gshared (Enumerator_t919299614 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1554059851(__this, method) ((  void (*) (Enumerator_t919299614 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1554059851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Dispose()
extern "C"  void Enumerator_Dispose_m797834572_gshared (Enumerator_t919299614 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m797834572(__this, method) ((  void (*) (Enumerator_t919299614 *, const MethodInfo*))Enumerator_Dispose_m797834572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2616197303_gshared (Enumerator_t919299614 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2616197303(__this, method) ((  bool (*) (Enumerator_t919299614 *, const MethodInfo*))Enumerator_MoveNext_m2616197303_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2685884093_gshared (Enumerator_t919299614 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2685884093(__this, method) ((  Il2CppObject * (*) (Enumerator_t919299614 *, const MethodInfo*))Enumerator_get_Current_m2685884093_gshared)(__this, method)
