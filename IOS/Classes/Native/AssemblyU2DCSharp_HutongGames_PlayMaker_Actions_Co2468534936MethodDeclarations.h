﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertStringToInt
struct ConvertStringToInt_t2468534936;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::.ctor()
extern "C"  void ConvertStringToInt__ctor_m526240974 (ConvertStringToInt_t2468534936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::Reset()
extern "C"  void ConvertStringToInt_Reset_m2467641211 (ConvertStringToInt_t2468534936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::OnEnter()
extern "C"  void ConvertStringToInt_OnEnter_m2598130981 (ConvertStringToInt_t2468534936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::OnUpdate()
extern "C"  void ConvertStringToInt_OnUpdate_m2366208446 (ConvertStringToInt_t2468534936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::DoConvertStringToInt()
extern "C"  void ConvertStringToInt_DoConvertStringToInt_m3604946961 (ConvertStringToInt_t2468534936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
