﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLayer
struct GetLayer_t1705980995;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLayer::.ctor()
extern "C"  void GetLayer__ctor_m2066637187 (GetLayer_t1705980995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::Reset()
extern "C"  void GetLayer_Reset_m4008037424 (GetLayer_t1705980995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnEnter()
extern "C"  void GetLayer_OnEnter_m1155174554 (GetLayer_t1705980995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnUpdate()
extern "C"  void GetLayer_OnUpdate_m584232169 (GetLayer_t1705980995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::DoGetLayer()
extern "C"  void GetLayer_DoGetLayer_m1176715047 (GetLayer_t1705980995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
