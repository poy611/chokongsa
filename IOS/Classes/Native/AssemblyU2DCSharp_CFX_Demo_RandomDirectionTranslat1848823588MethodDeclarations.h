﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CFX_Demo_RandomDirectionTranslate
struct CFX_Demo_RandomDirectionTranslate_t1848823588;

#include "codegen/il2cpp-codegen.h"

// System.Void CFX_Demo_RandomDirectionTranslate::.ctor()
extern "C"  void CFX_Demo_RandomDirectionTranslate__ctor_m3386300103 (CFX_Demo_RandomDirectionTranslate_t1848823588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDirectionTranslate::Start()
extern "C"  void CFX_Demo_RandomDirectionTranslate_Start_m2333437895 (CFX_Demo_RandomDirectionTranslate_t1848823588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CFX_Demo_RandomDirectionTranslate::Update()
extern "C"  void CFX_Demo_RandomDirectionTranslate_Update_m3622950182 (CFX_Demo_RandomDirectionTranslate_t1848823588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
