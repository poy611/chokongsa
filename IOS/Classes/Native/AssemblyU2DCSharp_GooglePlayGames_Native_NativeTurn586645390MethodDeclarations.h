﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<HandleMatchEvent>c__AnonStorey91
struct U3CHandleMatchEventU3Ec__AnonStorey91_t586645390;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<HandleMatchEvent>c__AnonStorey91::.ctor()
extern "C"  void U3CHandleMatchEventU3Ec__AnonStorey91__ctor_m2564629917 (U3CHandleMatchEventU3Ec__AnonStorey91_t586645390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<HandleMatchEvent>c__AnonStorey91::<>m__80()
extern "C"  void U3CHandleMatchEventU3Ec__AnonStorey91_U3CU3Em__80_m1606370846 (U3CHandleMatchEventU3Ec__AnonStorey91_t586645390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
