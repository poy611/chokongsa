﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateTimeZoneHandli2945560484.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRe1751946808.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_DateTime1855814826.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateFormatHandling4014082626.h"
#include "mscorlib_System_Nullable_1_gen497649510.h"

// System.Void Newtonsoft.Json.Utilities.DateTimeUtils::.cctor()
extern "C"  void DateTimeUtils__cctor_m2452056586 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.Utilities.DateTimeUtils::GetUtcOffset(System.DateTime)
extern "C"  TimeSpan_t413522987  DateTimeUtils_GetUtcOffset_m4006481587 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::EnsureDateTime(System.DateTime,Newtonsoft.Json.DateTimeZoneHandling)
extern "C"  DateTime_t4283661327  DateTimeUtils_EnsureDateTime_m1344423374 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, int32_t ___timeZone1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToLocalTime(System.DateTime)
extern "C"  DateTime_t4283661327  DateTimeUtils_SwitchToLocalTime_m908630869 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToUtcTime(System.DateTime)
extern "C"  DateTime_t4283661327  DateTimeUtils_SwitchToUtcTime_m3302658108 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ToUniversalTicks(System.DateTime)
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m3967438966 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t DateTimeUtils_ToUniversalTicks_m3754172620 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m5533746 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.Boolean)
extern "C"  int64_t DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m204305441 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, bool ___convertToUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern "C"  int64_t DateTimeUtils_UniversialTicksToJavaScriptTicks_m1375243002 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern "C"  DateTime_t4283661327  DateTimeUtils_ConvertJavaScriptTicksToDateTime_m19551558 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeIso(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern "C"  bool DateTimeUtils_TryParseDateTimeIso_m3453085353 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___text0, int32_t ___dateTimeZoneHandling1, DateTime_t4283661327 * ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetIso(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern "C"  bool DateTimeUtils_TryParseDateTimeOffsetIso_m3093606438 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___text0, DateTimeOffset_t3884714306 * ___dt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::CreateDateTime(Newtonsoft.Json.Utilities.DateTimeParser)
extern "C"  DateTime_t4283661327  DateTimeUtils_CreateDateTime_m3068611660 (Il2CppObject * __this /* static, unused */, DateTimeParser_t1855814826  ___dateTimeParser0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern "C"  bool DateTimeUtils_TryParseDateTime_m258163454 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___s0, int32_t ___dateTimeZoneHandling1, String_t* ___dateFormatString2, CultureInfo_t1065375142 * ___culture3, DateTime_t4283661327 * ___dt4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern "C"  bool DateTimeUtils_TryParseDateTime_m3556531373 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___dateTimeZoneHandling1, String_t* ___dateFormatString2, CultureInfo_t1065375142 * ___culture3, DateTime_t4283661327 * ___dt4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(Newtonsoft.Json.Utilities.StringReference,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern "C"  bool DateTimeUtils_TryParseDateTimeOffset_m347265447 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___s0, String_t* ___dateFormatString1, CultureInfo_t1065375142 * ___culture2, DateTimeOffset_t3884714306 * ___dt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern "C"  bool DateTimeUtils_TryParseDateTimeOffset_m3791122264 (Il2CppObject * __this /* static, unused */, String_t* ___s0, String_t* ___dateFormatString1, CultureInfo_t1065375142 * ___culture2, DateTimeOffset_t3884714306 * ___dt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseMicrosoftDate(Newtonsoft.Json.Utilities.StringReference,System.Int64&,System.TimeSpan&,System.DateTimeKind&)
extern "C"  bool DateTimeUtils_TryParseMicrosoftDate_m420572110 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___text0, int64_t* ___ticks1, TimeSpan_t413522987 * ___offset2, int32_t* ___kind3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeMicrosoft(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern "C"  bool DateTimeUtils_TryParseDateTimeMicrosoft_m2872118432 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___text0, int32_t ___dateTimeZoneHandling1, DateTime_t4283661327 * ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeExact(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern "C"  bool DateTimeUtils_TryParseDateTimeExact_m4235041370 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___dateTimeZoneHandling1, String_t* ___dateFormatString2, CultureInfo_t1065375142 * ___culture3, DateTime_t4283661327 * ___dt4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetMicrosoft(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern "C"  bool DateTimeUtils_TryParseDateTimeOffsetMicrosoft_m1332365135 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___text0, DateTimeOffset_t3884714306 * ___dt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetExact(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern "C"  bool DateTimeUtils_TryParseDateTimeOffsetExact_m2570338193 (Il2CppObject * __this /* static, unused */, String_t* ___text0, String_t* ___dateFormatString1, CultureInfo_t1065375142 * ___culture2, DateTimeOffset_t3884714306 * ___dt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryReadOffset(Newtonsoft.Json.Utilities.StringReference,System.Int32,System.TimeSpan&)
extern "C"  bool DateTimeUtils_TryReadOffset_m3195603777 (Il2CppObject * __this /* static, unused */, StringReference_t1751946808  ___offsetText0, int32_t ___startIndex1, TimeSpan_t413522987 * ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.IO.TextWriter,System.DateTime,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern "C"  void DateTimeUtils_WriteDateTimeString_m309914248 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, DateTime_t4283661327  ___value1, int32_t ___format2, String_t* ___formatString3, CultureInfo_t1065375142 * ___culture4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.Char[],System.Int32,System.DateTime,System.Nullable`1<System.TimeSpan>,System.DateTimeKind,Newtonsoft.Json.DateFormatHandling)
extern "C"  int32_t DateTimeUtils_WriteDateTimeString_m1176889044 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, DateTime_t4283661327  ___value2, Nullable_1_t497649510  ___offset3, int32_t ___kind4, int32_t ___format5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDefaultIsoDate(System.Char[],System.Int32,System.DateTime)
extern "C"  int32_t DateTimeUtils_WriteDefaultIsoDate_m969637092 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, DateTime_t4283661327  ___dt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DateTimeUtils::CopyIntToCharArray(System.Char[],System.Int32,System.Int32,System.Int32)
extern "C"  void DateTimeUtils_CopyIntToCharArray_m505226763 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, int32_t ___value2, int32_t ___digits3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffset(System.Char[],System.Int32,System.TimeSpan,Newtonsoft.Json.DateFormatHandling)
extern "C"  int32_t DateTimeUtils_WriteDateTimeOffset_m446939395 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3324145743* ___chars0, int32_t ___start1, TimeSpan_t413522987  ___offset2, int32_t ___format3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffsetString(System.IO.TextWriter,System.DateTimeOffset,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern "C"  void DateTimeUtils_WriteDateTimeOffsetString_m3233307048 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, DateTimeOffset_t3884714306  ___value1, int32_t ___format2, String_t* ___formatString3, CultureInfo_t1065375142 * ___culture4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DateTimeUtils::GetDateValues(System.DateTime,System.Int32&,System.Int32&,System.Int32&)
extern "C"  void DateTimeUtils_GetDateValues_m3266590032 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___td0, int32_t* ___year1, int32_t* ___month2, int32_t* ___day3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
