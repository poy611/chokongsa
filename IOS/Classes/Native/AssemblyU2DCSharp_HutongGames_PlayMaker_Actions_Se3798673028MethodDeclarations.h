﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMass
struct SetMass_t3798673028;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMass::.ctor()
extern "C"  void SetMass__ctor_m3167718706 (SetMass_t3798673028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::Reset()
extern "C"  void SetMass_Reset_m814151647 (SetMass_t3798673028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::OnEnter()
extern "C"  void SetMass_OnEnter_m2732559497 (SetMass_t3798673028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::DoSetMass()
extern "C"  void SetMass_DoSetMass_m1119298427 (SetMass_t3798673028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
