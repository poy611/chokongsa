﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t1083620227;
// System.Collections.Generic.IEnumerable`1<System.IntPtr>
struct IEnumerable_1_t3016347632;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t1627299724;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.IntPtr>
struct ICollection_1_t610024662;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>
struct ReadOnlyCollection_1_t1272512211;
// System.IntPtr[]
struct IntPtrU5BU5D_t3228729122;
// System.Predicate`1<System.IntPtr>
struct Predicate_1_t3621458854;
// System.Collections.Generic.IComparer`1<System.IntPtr>
struct IComparer_1_t2290448717;
// System.Comparison`1<System.IntPtr>
struct Comparison_1_t2726763158;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1103292997.h"

// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor()
extern "C"  void List_1__ctor_m1031710804_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1__ctor_m1031710804(__this, method) ((  void (*) (List_1_t1083620227 *, const MethodInfo*))List_1__ctor_m1031710804_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1690359723_gshared (List_1_t1083620227 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1690359723(__this, ___collection0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1690359723_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1179549605_gshared (List_1_t1083620227 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1179549605(__this, ___capacity0, method) ((  void (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1__ctor_m1179549605_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.cctor()
extern "C"  void List_1__cctor_m1436167641_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1436167641(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1436167641_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.IntPtr>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m662701222_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m662701222(__this, method) ((  Il2CppObject* (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m662701222_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1686571888_gshared (List_1_t1083620227 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1686571888(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1083620227 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1686571888_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m159407211_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m159407211(__this, method) ((  Il2CppObject * (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m159407211_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1601723494_gshared (List_1_t1083620227 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1601723494(__this, ___item0, method) ((  int32_t (*) (List_1_t1083620227 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1601723494_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1238956838_gshared (List_1_t1083620227 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1238956838(__this, ___item0, method) ((  bool (*) (List_1_t1083620227 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1238956838_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2831675326_gshared (List_1_t1083620227 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2831675326(__this, ___item0, method) ((  int32_t (*) (List_1_t1083620227 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2831675326_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1613927145_gshared (List_1_t1083620227 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1613927145(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1083620227 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1613927145_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m4257189791_gshared (List_1_t1083620227 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m4257189791(__this, ___item0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m4257189791_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3501831079_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3501831079(__this, method) ((  bool (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3501831079_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3104162422_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3104162422(__this, method) ((  bool (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3104162422_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1280624866_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1280624866(__this, method) ((  Il2CppObject * (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1280624866_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1886224149_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1886224149(__this, method) ((  bool (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1886224149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3147254084_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3147254084(__this, method) ((  bool (*) (List_1_t1083620227 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3147254084_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3385461225_gshared (List_1_t1083620227 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3385461225(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3385461225_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1114744960_gshared (List_1_t1083620227 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1114744960(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1083620227 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1114744960_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Add(T)
extern "C"  void List_1_Add_m1270349227_gshared (List_1_t1083620227 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define List_1_Add_m1270349227(__this, ___item0, method) ((  void (*) (List_1_t1083620227 *, IntPtr_t, const MethodInfo*))List_1_Add_m1270349227_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2327226470_gshared (List_1_t1083620227 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2327226470(__this, ___newCount0, method) ((  void (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2327226470_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1027760065_gshared (List_1_t1083620227 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1027760065(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1083620227 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1027760065_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m385607524_gshared (List_1_t1083620227 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m385607524(__this, ___collection0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m385607524_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3941547044_gshared (List_1_t1083620227 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3941547044(__this, ___enumerable0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3941547044_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3065883635_gshared (List_1_t1083620227 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3065883635(__this, ___collection0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3065883635_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.IntPtr>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1272512211 * List_1_AsReadOnly_m1052186838_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1052186838(__this, method) ((  ReadOnlyCollection_1_t1272512211 * (*) (List_1_t1083620227 *, const MethodInfo*))List_1_AsReadOnly_m1052186838_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Clear()
extern "C"  void List_1_Clear_m2732811391_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_Clear_m2732811391(__this, method) ((  void (*) (List_1_t1083620227 *, const MethodInfo*))List_1_Clear_m2732811391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::Contains(T)
extern "C"  bool List_1_Contains_m2255538733_gshared (List_1_t1083620227 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2255538733(__this, ___item0, method) ((  bool (*) (List_1_t1083620227 *, IntPtr_t, const MethodInfo*))List_1_Contains_m2255538733_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1086256508_gshared (List_1_t1083620227 * __this, IntPtrU5BU5D_t3228729122* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1086256508(__this, ___array0, method) ((  void (*) (List_1_t1083620227 *, IntPtrU5BU5D_t3228729122*, const MethodInfo*))List_1_CopyTo_m1086256508_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4236767451_gshared (List_1_t1083620227 * __this, IntPtrU5BU5D_t3228729122* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4236767451(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1083620227 *, IntPtrU5BU5D_t3228729122*, int32_t, const MethodInfo*))List_1_CopyTo_m4236767451_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.IntPtr>::Find(System.Predicate`1<T>)
extern "C"  IntPtr_t List_1_Find_m3557741869_gshared (List_1_t1083620227 * __this, Predicate_1_t3621458854 * ___match0, const MethodInfo* method);
#define List_1_Find_m3557741869(__this, ___match0, method) ((  IntPtr_t (*) (List_1_t1083620227 *, Predicate_1_t3621458854 *, const MethodInfo*))List_1_Find_m3557741869_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1853617256_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3621458854 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1853617256(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3621458854 *, const MethodInfo*))List_1_CheckMatch_m1853617256_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2130267021_gshared (List_1_t1083620227 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3621458854 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2130267021(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1083620227 *, int32_t, int32_t, Predicate_1_t3621458854 *, const MethodInfo*))List_1_GetIndex_m2130267021_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.IntPtr>::GetEnumerator()
extern "C"  Enumerator_t1103292997  List_1_GetEnumerator_m470573290_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m470573290(__this, method) ((  Enumerator_t1103292997  (*) (List_1_t1083620227 *, const MethodInfo*))List_1_GetEnumerator_m470573290_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2798406047_gshared (List_1_t1083620227 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2798406047(__this, ___item0, method) ((  int32_t (*) (List_1_t1083620227 *, IntPtr_t, const MethodInfo*))List_1_IndexOf_m2798406047_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4034575410_gshared (List_1_t1083620227 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4034575410(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1083620227 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4034575410_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3983134635_gshared (List_1_t1083620227 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3983134635(__this, ___index0, method) ((  void (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3983134635_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4254237202_gshared (List_1_t1083620227 * __this, int32_t ___index0, IntPtr_t ___item1, const MethodInfo* method);
#define List_1_Insert_m4254237202(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1083620227 *, int32_t, IntPtr_t, const MethodInfo*))List_1_Insert_m4254237202_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1728311559_gshared (List_1_t1083620227 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1728311559(__this, ___collection0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1728311559_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::Remove(T)
extern "C"  bool List_1_Remove_m1673277800_gshared (List_1_t1083620227 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1673277800(__this, ___item0, method) ((  bool (*) (List_1_t1083620227 *, IntPtr_t, const MethodInfo*))List_1_Remove_m1673277800_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3160965154_gshared (List_1_t1083620227 * __this, Predicate_1_t3621458854 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3160965154(__this, ___match0, method) ((  int32_t (*) (List_1_t1083620227 *, Predicate_1_t3621458854 *, const MethodInfo*))List_1_RemoveAll_m3160965154_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2128090072_gshared (List_1_t1083620227 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2128090072(__this, ___index0, method) ((  void (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2128090072_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1531002875_gshared (List_1_t1083620227 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1531002875(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1083620227 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1531002875_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Reverse()
extern "C"  void List_1_Reverse_m3669415828_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_Reverse_m3669415828(__this, method) ((  void (*) (List_1_t1083620227 *, const MethodInfo*))List_1_Reverse_m3669415828_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Sort()
extern "C"  void List_1_Sort_m1796321422_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_Sort_m1796321422(__this, method) ((  void (*) (List_1_t1083620227 *, const MethodInfo*))List_1_Sort_m1796321422_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3126660374_gshared (List_1_t1083620227 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3126660374(__this, ___comparer0, method) ((  void (*) (List_1_t1083620227 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3126660374_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1119272545_gshared (List_1_t1083620227 * __this, Comparison_1_t2726763158 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1119272545(__this, ___comparison0, method) ((  void (*) (List_1_t1083620227 *, Comparison_1_t2726763158 *, const MethodInfo*))List_1_Sort_m1119272545_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.IntPtr>::ToArray()
extern "C"  IntPtrU5BU5D_t3228729122* List_1_ToArray_m3458922643_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_ToArray_m3458922643(__this, method) ((  IntPtrU5BU5D_t3228729122* (*) (List_1_t1083620227 *, const MethodInfo*))List_1_ToArray_m3458922643_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2733747623_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2733747623(__this, method) ((  void (*) (List_1_t1083620227 *, const MethodInfo*))List_1_TrimExcess_m2733747623_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1314937423_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1314937423(__this, method) ((  int32_t (*) (List_1_t1083620227 *, const MethodInfo*))List_1_get_Capacity_m1314937423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3832382584_gshared (List_1_t1083620227 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3832382584(__this, ___value0, method) ((  void (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3832382584_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::get_Count()
extern "C"  int32_t List_1_get_Count_m668549052_gshared (List_1_t1083620227 * __this, const MethodInfo* method);
#define List_1_get_Count_m668549052(__this, method) ((  int32_t (*) (List_1_t1083620227 *, const MethodInfo*))List_1_get_Count_m668549052_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.IntPtr>::get_Item(System.Int32)
extern "C"  IntPtr_t List_1_get_Item_m3598474588_gshared (List_1_t1083620227 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3598474588(__this, ___index0, method) ((  IntPtr_t (*) (List_1_t1083620227 *, int32_t, const MethodInfo*))List_1_get_Item_m3598474588_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1322681961_gshared (List_1_t1083620227 * __this, int32_t ___index0, IntPtr_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1322681961(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1083620227 *, int32_t, IntPtr_t, const MethodInfo*))List_1_set_Item_m1322681961_gshared)(__this, ___index0, ___value1, method)
