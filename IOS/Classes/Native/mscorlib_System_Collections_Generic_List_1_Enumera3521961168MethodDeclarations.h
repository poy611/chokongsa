﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m681996699(__this, ___l0, method) ((  void (*) (Enumerator_t3521961168 *, List_1_t3502288398 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1080610263(__this, method) ((  void (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4221635277(__this, method) ((  Il2CppObject * (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::Dispose()
#define Enumerator_Dispose_m661182016(__this, method) ((  void (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::VerifyState()
#define Enumerator_VerifyState_m1741124985(__this, method) ((  void (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::MoveNext()
#define Enumerator_MoveNext_m2182004978(__this, method) ((  bool (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmFloat>::get_Current()
#define Enumerator_get_Current_m232212010(__this, method) ((  FsmFloat_t2134102846 * (*) (Enumerator_t3521961168 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
