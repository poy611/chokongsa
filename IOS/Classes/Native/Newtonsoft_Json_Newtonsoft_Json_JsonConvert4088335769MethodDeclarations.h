﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`1<Newtonsoft.Json.JsonSerializerSettings>
struct Func_1_t3714567099;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatFormatHandlin3887485542.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "Newtonsoft_Json_Newtonsoft_Json_StringEscapeHandli1042460335.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializerSett2589405525.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer251850770.h"

// System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::get_DefaultSettings()
extern "C"  Func_1_t3714567099 * JsonConvert_get_DefaultSettings_m101884946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern "C"  String_t* JsonConvert_ToString_m833059226 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern "C"  String_t* JsonConvert_ToString_m4128171418 (Il2CppObject * __this /* static, unused */, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Single,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern "C"  String_t* JsonConvert_ToString_m2869870837 (Il2CppObject * __this /* static, unused */, float ___value0, int32_t ___floatFormatHandling1, Il2CppChar ___quoteChar2, bool ___nullable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureFloatFormat(System.Double,System.String,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern "C"  String_t* JsonConvert_EnsureFloatFormat_m2505010495 (Il2CppObject * __this /* static, unused */, double ___value0, String_t* ___text1, int32_t ___floatFormatHandling2, Il2CppChar ___quoteChar3, bool ___nullable4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Double,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern "C"  String_t* JsonConvert_ToString_m4195151998 (Il2CppObject * __this /* static, unused */, double ___value0, int32_t ___floatFormatHandling1, Il2CppChar ___quoteChar2, bool ___nullable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m2448359443 (Il2CppObject * __this /* static, unused */, double ___value0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m1477636055 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern "C"  String_t* JsonConvert_ToString_m3687495569 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern "C"  String_t* JsonConvert_ToString_m297159295 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern "C"  String_t* JsonConvert_ToString_m3457127062 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimiter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char,Newtonsoft.Json.StringEscapeHandling)
extern "C"  String_t* JsonConvert_ToString_m637019812 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimiter1, int32_t ___stringEscapeHandling2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object)
extern "C"  String_t* JsonConvert_SerializeObject_m683678730 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  String_t* JsonConvert_SerializeObject_m3279387511 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObjectInternal(System.Object,System.Type,Newtonsoft.Json.JsonSerializer)
extern "C"  String_t* JsonConvert_SerializeObjectInternal_m441509879 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, JsonSerializer_t251850770 * ___jsonSerializer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m1360968954 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern "C"  void JsonConvert__cctor_m2189751715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
