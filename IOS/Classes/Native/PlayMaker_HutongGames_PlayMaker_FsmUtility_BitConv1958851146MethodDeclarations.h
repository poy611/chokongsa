﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Int32 HutongGames.PlayMaker.FsmUtility/BitConverter::ToInt32(System.Byte[],System.Int32)
extern "C"  int32_t BitConverter_ToInt32_m2157419792 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmUtility/BitConverter::ToSingle(System.Byte[],System.Int32)
extern "C"  float BitConverter_ToSingle_m2374191178 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmUtility/BitConverter::ToBoolean(System.Byte[],System.Int32)
extern "C"  bool BitConverter_ToBoolean_m2411990032 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility/BitConverter::GetBytes(System.Boolean)
extern "C"  ByteU5BU5D_t4260760469* BitConverter_GetBytes_m152845383 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility/BitConverter::GetBytes(System.Int32)
extern "C"  ByteU5BU5D_t4260760469* BitConverter_GetBytes_m999731233 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility/BitConverter::GetBytes(System.Single)
extern "C"  ByteU5BU5D_t4260760469* BitConverter_GetBytes_m1064919259 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
