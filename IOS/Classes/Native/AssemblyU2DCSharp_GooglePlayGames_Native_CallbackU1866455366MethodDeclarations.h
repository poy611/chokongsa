﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3E_1__ctor_m2917705613_gshared (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey3E_1__ctor_m2917705613(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey3E_1__ctor_m2917705613_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey3E`1<System.Object>::<>m__E(T)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277_gshared (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277(__this, ___val0, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey3E_1_t1866455366 *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey3E_1_U3CU3Em__E_m1610079277_gshared)(__this, ___val0, method)
