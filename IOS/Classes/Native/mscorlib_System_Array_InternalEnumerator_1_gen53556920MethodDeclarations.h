﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen53556920.h"
#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1358800949_gshared (InternalEnumerator_1_t53556920 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1358800949(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t53556920 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1358800949_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315(__this, method) ((  void (*) (InternalEnumerator_1_t53556920 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3781226315_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t53556920 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1082149367_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m665911372_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m665911372(__this, method) ((  void (*) (InternalEnumerator_1_t53556920 *, const MethodInfo*))InternalEnumerator_1_Dispose_m665911372_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2455145271_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2455145271(__this, method) ((  bool (*) (InternalEnumerator_1_t53556920 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2455145271_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::get_Current()
extern "C"  Point_t1271214244  InternalEnumerator_1_get_Current_m4220172348_gshared (InternalEnumerator_1_t53556920 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4220172348(__this, method) ((  Point_t1271214244  (*) (InternalEnumerator_1_t53556920 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4220172348_gshared)(__this, method)
