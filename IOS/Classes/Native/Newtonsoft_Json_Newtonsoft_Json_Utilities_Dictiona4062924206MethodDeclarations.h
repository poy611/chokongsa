﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3856534026;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Dictiona4062924206.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
extern "C"  void DictionaryEnumerator_2__ctor_m3301157341_gshared (DictionaryEnumerator_2_t4062924206 * __this, Il2CppObject* ___e0, const MethodInfo* method);
#define DictionaryEnumerator_2__ctor_m3301157341(__this, ___e0, method) ((  void (*) (DictionaryEnumerator_2_t4062924206 *, Il2CppObject*, const MethodInfo*))DictionaryEnumerator_2__ctor_m3301157341_gshared)(__this, ___e0, method)
// System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  DictionaryEnumerator_2_get_Entry_m2420324981_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Entry_m2420324981(__this, method) ((  DictionaryEntry_t1751606614  (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_get_Entry_m2420324981_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Key_m3310238376_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Key_m3310238376(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_get_Key_m3310238376_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Value_m3911716666_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Value_m3911716666(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_get_Value_m3911716666_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * DictionaryEnumerator_2_get_Current_m2488022850_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_get_Current_m2488022850(__this, method) ((  Il2CppObject * (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_get_Current_m2488022850_gshared)(__this, method)
// System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool DictionaryEnumerator_2_MoveNext_m1748601087_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_MoveNext_m1748601087(__this, method) ((  bool (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_MoveNext_m1748601087_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2/DictionaryEnumerator`2<System.Object,System.Object,System.Object,System.Object>::Reset()
extern "C"  void DictionaryEnumerator_2_Reset_m1189624908_gshared (DictionaryEnumerator_2_t4062924206 * __this, const MethodInfo* method);
#define DictionaryEnumerator_2_Reset_m1189624908(__this, method) ((  void (*) (DictionaryEnumerator_2_t4062924206 *, const MethodInfo*))DictionaryEnumerator_2_Reset_m1189624908_gshared)(__this, method)
