﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmGameObject
struct GetFsmGameObject_t4125302563;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::.ctor()
extern "C"  void GetFsmGameObject__ctor_m1546853027 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::Reset()
extern "C"  void GetFsmGameObject_Reset_m3488253264 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnEnter()
extern "C"  void GetFsmGameObject_OnEnter_m4153770426 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnUpdate()
extern "C"  void GetFsmGameObject_OnUpdate_m3346390985 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::DoGetFsmGameObject()
extern "C"  void GetFsmGameObject_DoGetFsmGameObject_m3089964775 (GetFsmGameObject_t4125302563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
