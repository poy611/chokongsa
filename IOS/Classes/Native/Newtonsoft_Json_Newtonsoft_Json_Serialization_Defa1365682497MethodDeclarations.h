﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1
struct U3CU3Ec__DisplayClass34_1_t1365682497;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass34_1__ctor_m3300929375 (U3CU3Ec__DisplayClass34_1_t1365682497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_1::<SetExtensionDataDelegates>b__0(System.Object,System.String,System.Object)
extern "C"  void U3CU3Ec__DisplayClass34_1_U3CSetExtensionDataDelegatesU3Eb__0_m2632274016 (U3CU3Ec__DisplayClass34_1_t1365682497 * __this, Il2CppObject * ___o0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
