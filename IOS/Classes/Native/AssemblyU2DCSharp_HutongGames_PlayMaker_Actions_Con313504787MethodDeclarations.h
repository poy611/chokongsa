﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToColor
struct ConvertBoolToColor_t313504787;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::.ctor()
extern "C"  void ConvertBoolToColor__ctor_m4252921267 (ConvertBoolToColor_t313504787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::Reset()
extern "C"  void ConvertBoolToColor_Reset_m1899354208 (ConvertBoolToColor_t313504787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::OnEnter()
extern "C"  void ConvertBoolToColor_OnEnter_m1935167690 (ConvertBoolToColor_t313504787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::OnUpdate()
extern "C"  void ConvertBoolToColor_OnUpdate_m3289182905 (ConvertBoolToColor_t313504787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::DoConvertBoolToColor()
extern "C"  void ConvertBoolToColor_DoConvertBoolToColor_m3064094535 (ConvertBoolToColor_t313504787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
