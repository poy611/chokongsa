﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultAllDataUpper
struct ResultAllDataUpper_t3242865492;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultAllDataUpper::.ctor()
extern "C"  void ResultAllDataUpper__ctor_m3015592903 (ResultAllDataUpper_t3242865492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
