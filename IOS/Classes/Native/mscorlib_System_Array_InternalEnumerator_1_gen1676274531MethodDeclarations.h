﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1676274531.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22893931855.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m270858264_gshared (InternalEnumerator_1_t1676274531 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m270858264(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1676274531 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m270858264_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304(__this, method) ((  void (*) (InternalEnumerator_1_t1676274531 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3229767304_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1676274531 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1180034292_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m272449519_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m272449519(__this, method) ((  void (*) (InternalEnumerator_1_t1676274531 *, const MethodInfo*))InternalEnumerator_1_Dispose_m272449519_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2936217332_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2936217332(__this, method) ((  bool (*) (InternalEnumerator_1_t1676274531 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2936217332_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Serialization.ResolverContractKey,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2893931855  InternalEnumerator_1_get_Current_m3404344799_gshared (InternalEnumerator_1_t1676274531 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3404344799(__this, method) ((  KeyValuePair_2_t2893931855  (*) (InternalEnumerator_1_t1676274531 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3404344799_gshared)(__this, method)
