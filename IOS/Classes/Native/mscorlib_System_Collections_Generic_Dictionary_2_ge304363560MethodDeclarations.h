﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t304363560;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct IDictionary_2_t4177204201;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>[]
struct KeyValuePair_2U5BU5D_t3777292111;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>>
struct IEnumerator_1_t2115009315;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct KeyCollection_t1931123011;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct ValueCollection_t3299936569;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_203144266.h"
#include "mscorlib_System_Array1146569071.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2429291660.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1621686952.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor()
extern "C"  void Dictionary_2__ctor_m2102355510_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2102355510(__this, method) ((  void (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2__ctor_m2102355510_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3323784621_gshared (Dictionary_2_t304363560 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3323784621(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3323784621_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4268801314_gshared (Dictionary_2_t304363560 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4268801314(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4268801314_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2576841351_gshared (Dictionary_2_t304363560 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2576841351(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t304363560 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2576841351_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2912826395_gshared (Dictionary_2_t304363560 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2912826395(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2912826395_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1468828630_gshared (Dictionary_2_t304363560 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1468828630(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t304363560 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1468828630_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m668445303_gshared (Dictionary_2_t304363560 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m668445303(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t304363560 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m668445303_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768766232_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768766232(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3768766232_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m459503996_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m459503996(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m459503996_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m683033134_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m683033134(__this, method) ((  bool (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m683033134_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4164310738_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4164310738(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4164310738_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1803927223_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1803927223(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1803927223_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3889424698_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3889424698(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3889424698_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2521171708_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2521171708(__this, ___key0, method) ((  bool (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2521171708_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2825029365_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2825029365(__this, ___key0, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2825029365_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2598726104_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2598726104(__this, method) ((  bool (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2598726104_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3823885444_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3823885444(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3823885444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3018384412_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3018384412(__this, method) ((  bool (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3018384412_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3837949195_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2_t203144266  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3837949195(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t304363560 *, KeyValuePair_2_t203144266 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3837949195_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2277031063_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2_t203144266  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2277031063(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t304363560 *, KeyValuePair_2_t203144266 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2277031063_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2053003311_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2U5BU5D_t3777292111* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2053003311(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t304363560 *, KeyValuePair_2U5BU5D_t3777292111*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2053003311_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3467320700_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2_t203144266  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3467320700(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t304363560 *, KeyValuePair_2_t203144266 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3467320700_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1108826062_gshared (Dictionary_2_t304363560 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1108826062(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1108826062_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3507557065_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3507557065(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3507557065_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4160682694_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4160682694(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4160682694_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2947021025_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2947021025(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2947021025_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2859703198_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2859703198(__this, method) ((  int32_t (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_get_Count_m2859703198_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m2389274765_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2389274765(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m2389274765_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3349251446_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3349251446(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m3349251446_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2578943406_gshared (Dictionary_2_t304363560 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2578943406(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t304363560 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2578943406_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m132720585_gshared (Dictionary_2_t304363560 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m132720585(__this, ___size0, method) ((  void (*) (Dictionary_2_t304363560 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m132720585_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1109536773_gshared (Dictionary_2_t304363560 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1109536773(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1109536773_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t203144266  Dictionary_2_make_pair_m2783637585_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2783637585(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t203144266  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m2783637585_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m2304707717_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2304707717(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m2304707717_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1568523909_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1568523909(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1568523909_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3325515818_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2U5BU5D_t3777292111* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3325515818(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t304363560 *, KeyValuePair_2U5BU5D_t3777292111*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3325515818_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Resize()
extern "C"  void Dictionary_2_Resize_m806308034_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m806308034(__this, method) ((  void (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_Resize_m806308034_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1109945151_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1109945151(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_Add_m1109945151_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Clear()
extern "C"  void Dictionary_2_Clear_m3803456097_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3803456097(__this, method) ((  void (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_Clear_m3803456097_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4039029127_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4039029127(__this, ___key0, method) ((  bool (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m4039029127_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m466086407_gshared (Dictionary_2_t304363560 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m466086407(__this, ___value0, method) ((  bool (*) (Dictionary_2_t304363560 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m466086407_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1050577620_gshared (Dictionary_2_t304363560 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1050577620(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t304363560 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1050577620_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1619706896_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1619706896(__this, ___sender0, method) ((  void (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1619706896_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m192704265_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m192704265(__this, ___key0, method) ((  bool (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m192704265_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3810562784_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3810562784(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t304363560 *, Il2CppObject *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m3810562784_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Keys()
extern "C"  KeyCollection_t1931123011 * Dictionary_2_get_Keys_m2222255135_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2222255135(__this, method) ((  KeyCollection_t1931123011 * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_get_Keys_m2222255135_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::get_Values()
extern "C"  ValueCollection_t3299936569 * Dictionary_2_get_Values_m3157106015_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3157106015(__this, method) ((  ValueCollection_t3299936569 * (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_get_Values_m3157106015_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m1754566624_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1754566624(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1754566624_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m4175805344_gshared (Dictionary_2_t304363560 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4175805344(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t304363560 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4175805344_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m778097900_gshared (Dictionary_2_t304363560 * __this, KeyValuePair_2_t203144266  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m778097900(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t304363560 *, KeyValuePair_2_t203144266 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m778097900_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::GetEnumerator()
extern "C"  Enumerator_t1621686952  Dictionary_2_GetEnumerator_m876665723_gshared (Dictionary_2_t304363560 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m876665723(__this, method) ((  Enumerator_t1621686952  (*) (Dictionary_2_t304363560 *, const MethodInfo*))Dictionary_2_GetEnumerator_m876665723_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Utilities.PrimitiveTypeCode>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m2203705610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2203705610(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2203705610_gshared)(__this /* static, unused */, ___key0, ___value1, method)
