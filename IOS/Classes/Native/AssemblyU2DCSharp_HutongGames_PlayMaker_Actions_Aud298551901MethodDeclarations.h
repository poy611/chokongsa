﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AudioMute
struct AudioMute_t298551901;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AudioMute::.ctor()
extern "C"  void AudioMute__ctor_m1166726009 (AudioMute_t298551901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioMute::Reset()
extern "C"  void AudioMute_Reset_m3108126246 (AudioMute_t298551901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioMute::OnEnter()
extern "C"  void AudioMute_OnEnter_m3923926288 (AudioMute_t298551901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
