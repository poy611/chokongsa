﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkInitializeServer
struct  NetworkInitializeServer_t4294133583  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkInitializeServer::connections
	FsmInt_t1596138449 * ___connections_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkInitializeServer::listenPort
	FsmInt_t1596138449 * ___listenPort_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkInitializeServer::incomingPassword
	FsmString_t952858651 * ___incomingPassword_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkInitializeServer::useNAT
	FsmBool_t1075959796 * ___useNAT_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkInitializeServer::useSecurityLayer
	FsmBool_t1075959796 * ___useSecurityLayer_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkInitializeServer::runInBackground
	FsmBool_t1075959796 * ___runInBackground_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkInitializeServer::errorEvent
	FsmEvent_t2133468028 * ___errorEvent_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkInitializeServer::errorString
	FsmString_t952858651 * ___errorString_18;

public:
	inline static int32_t get_offset_of_connections_11() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___connections_11)); }
	inline FsmInt_t1596138449 * get_connections_11() const { return ___connections_11; }
	inline FsmInt_t1596138449 ** get_address_of_connections_11() { return &___connections_11; }
	inline void set_connections_11(FsmInt_t1596138449 * value)
	{
		___connections_11 = value;
		Il2CppCodeGenWriteBarrier(&___connections_11, value);
	}

	inline static int32_t get_offset_of_listenPort_12() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___listenPort_12)); }
	inline FsmInt_t1596138449 * get_listenPort_12() const { return ___listenPort_12; }
	inline FsmInt_t1596138449 ** get_address_of_listenPort_12() { return &___listenPort_12; }
	inline void set_listenPort_12(FsmInt_t1596138449 * value)
	{
		___listenPort_12 = value;
		Il2CppCodeGenWriteBarrier(&___listenPort_12, value);
	}

	inline static int32_t get_offset_of_incomingPassword_13() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___incomingPassword_13)); }
	inline FsmString_t952858651 * get_incomingPassword_13() const { return ___incomingPassword_13; }
	inline FsmString_t952858651 ** get_address_of_incomingPassword_13() { return &___incomingPassword_13; }
	inline void set_incomingPassword_13(FsmString_t952858651 * value)
	{
		___incomingPassword_13 = value;
		Il2CppCodeGenWriteBarrier(&___incomingPassword_13, value);
	}

	inline static int32_t get_offset_of_useNAT_14() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___useNAT_14)); }
	inline FsmBool_t1075959796 * get_useNAT_14() const { return ___useNAT_14; }
	inline FsmBool_t1075959796 ** get_address_of_useNAT_14() { return &___useNAT_14; }
	inline void set_useNAT_14(FsmBool_t1075959796 * value)
	{
		___useNAT_14 = value;
		Il2CppCodeGenWriteBarrier(&___useNAT_14, value);
	}

	inline static int32_t get_offset_of_useSecurityLayer_15() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___useSecurityLayer_15)); }
	inline FsmBool_t1075959796 * get_useSecurityLayer_15() const { return ___useSecurityLayer_15; }
	inline FsmBool_t1075959796 ** get_address_of_useSecurityLayer_15() { return &___useSecurityLayer_15; }
	inline void set_useSecurityLayer_15(FsmBool_t1075959796 * value)
	{
		___useSecurityLayer_15 = value;
		Il2CppCodeGenWriteBarrier(&___useSecurityLayer_15, value);
	}

	inline static int32_t get_offset_of_runInBackground_16() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___runInBackground_16)); }
	inline FsmBool_t1075959796 * get_runInBackground_16() const { return ___runInBackground_16; }
	inline FsmBool_t1075959796 ** get_address_of_runInBackground_16() { return &___runInBackground_16; }
	inline void set_runInBackground_16(FsmBool_t1075959796 * value)
	{
		___runInBackground_16 = value;
		Il2CppCodeGenWriteBarrier(&___runInBackground_16, value);
	}

	inline static int32_t get_offset_of_errorEvent_17() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___errorEvent_17)); }
	inline FsmEvent_t2133468028 * get_errorEvent_17() const { return ___errorEvent_17; }
	inline FsmEvent_t2133468028 ** get_address_of_errorEvent_17() { return &___errorEvent_17; }
	inline void set_errorEvent_17(FsmEvent_t2133468028 * value)
	{
		___errorEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___errorEvent_17, value);
	}

	inline static int32_t get_offset_of_errorString_18() { return static_cast<int32_t>(offsetof(NetworkInitializeServer_t4294133583, ___errorString_18)); }
	inline FsmString_t952858651 * get_errorString_18() const { return ___errorString_18; }
	inline FsmString_t952858651 ** get_address_of_errorString_18() { return &___errorString_18; }
	inline void set_errorString_18(FsmString_t952858651 * value)
	{
		___errorString_18 = value;
		Il2CppCodeGenWriteBarrier(&___errorString_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
