﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3571553821(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1245559798 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m420802513_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::Invoke(T)
#define Func_2_Invoke_m97192484(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t1245559798 *, Type_t *, const MethodInfo*))Func_2_Invoke_m1009799647_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2475911639(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1245559798 *, Type_t *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m2046933040(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t1245559798 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
