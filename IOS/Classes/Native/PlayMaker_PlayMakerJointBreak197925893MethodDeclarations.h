﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerJointBreak
struct PlayMakerJointBreak_t197925893;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerJointBreak::OnJointBreak(System.Single)
extern "C"  void PlayMakerJointBreak_OnJointBreak_m105815447 (PlayMakerJointBreak_t197925893 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerJointBreak::.ctor()
extern "C"  void PlayMakerJointBreak__ctor_m133126660 (PlayMakerJointBreak_t197925893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
