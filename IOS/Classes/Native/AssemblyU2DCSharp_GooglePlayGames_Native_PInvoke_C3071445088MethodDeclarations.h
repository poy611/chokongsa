﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>
struct U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9C_2__ctor_m2655981944_gshared (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 * __this, const MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey9C_2__ctor_m2655981944(__this, method) ((  void (*) (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 *, const MethodInfo*))U3CToIntPtrU3Ec__AnonStorey9C_2__ctor_m2655981944_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey9C`2<System.Object,System.Object>::<>m__9C(T,System.IntPtr)
extern "C"  void U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793_gshared (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 * __this, Il2CppObject * ___param10, IntPtr_t ___param21, const MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793(__this, ___param10, ___param21, method) ((  void (*) (U3CToIntPtrU3Ec__AnonStorey9C_2_t3071445088 *, Il2CppObject *, IntPtr_t, const MethodInfo*))U3CToIntPtrU3Ec__AnonStorey9C_2_U3CU3Em__9C_m3148021793_gshared)(__this, ___param10, ___param21, method)
