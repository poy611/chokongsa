﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>
struct U3CU3Ec__DisplayClass4_0_1_t3696729747;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0_1__ctor_m726501149_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass4_0_1__ctor_m726501149(__this, method) ((  void (*) (U3CU3Ec__DisplayClass4_0_1_t3696729747 *, const MethodInfo*))U3CU3Ec__DisplayClass4_0_1__ctor_m726501149_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::<CreateMethodCall>b__0(T,System.Object[])
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__0_m923796313_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method);
#define U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__0_m923796313(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CU3Ec__DisplayClass4_0_1_t3696729747 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, const MethodInfo*))U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__0_m923796313_gshared)(__this, ___o0, ___a1, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<>c__DisplayClass4_0`1<System.Object>::<CreateMethodCall>b__1(T,System.Object[])
extern "C"  Il2CppObject * U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__1_m514972088_gshared (U3CU3Ec__DisplayClass4_0_1_t3696729747 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method);
#define U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__1_m514972088(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CU3Ec__DisplayClass4_0_1_t3696729747 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, const MethodInfo*))U3CU3Ec__DisplayClass4_0_1_U3CCreateMethodCallU3Eb__1_m514972088_gshared)(__this, ___o0, ___a1, method)
