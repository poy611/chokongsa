﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2166990872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3484314264.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3455248874_gshared (Enumerator_t3484314264 * __this, Dictionary_2_t2166990872 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3455248874(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3484314264 *, Dictionary_2_t2166990872 *, const MethodInfo*))Enumerator__ctor_m3455248874_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3267178999_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3267178999(__this, method) ((  Il2CppObject * (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3267178999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m692652171_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m692652171(__this, method) ((  void (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m692652171_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m948350932_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m948350932(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m948350932_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3926317459_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3926317459(__this, method) ((  Il2CppObject * (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3926317459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3258228581_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3258228581(__this, method) ((  Il2CppObject * (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3258228581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2518547447_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2518547447(__this, method) ((  bool (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_MoveNext_m2518547447_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t2065771578  Enumerator_get_Current_m3624402649_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3624402649(__this, method) ((  KeyValuePair_2_t2065771578  (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_get_Current_m3624402649_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3221742212_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3221742212(__this, method) ((  Il2CppObject * (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_get_CurrentKey_m3221742212_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m3627513384_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3627513384(__this, method) ((  float (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_get_CurrentValue_m3627513384_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m2659337532_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2659337532(__this, method) ((  void (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_Reset_m2659337532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3674454085_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3674454085(__this, method) ((  void (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_VerifyState_m3674454085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3432130157_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3432130157(__this, method) ((  void (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_VerifyCurrent_m3432130157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m2641256204_gshared (Enumerator_t3484314264 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2641256204(__this, method) ((  void (*) (Enumerator_t3484314264 *, const MethodInfo*))Enumerator_Dispose_m2641256204_gshared)(__this, method)
