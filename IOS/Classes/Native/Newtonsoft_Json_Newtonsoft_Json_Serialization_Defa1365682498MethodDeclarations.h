﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_2
struct U3CU3Ec__DisplayClass34_2_t1365682498;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t950614638;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_2::.ctor()
extern "C"  void U3CU3Ec__DisplayClass34_2__ctor_m3104415870 (U3CU3Ec__DisplayClass34_2_t1365682498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass34_2::<SetExtensionDataDelegates>b__1(System.Object)
extern "C"  Il2CppObject* U3CU3Ec__DisplayClass34_2_U3CSetExtensionDataDelegatesU3Eb__1_m253595547 (U3CU3Ec__DisplayClass34_2_t1365682498 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
