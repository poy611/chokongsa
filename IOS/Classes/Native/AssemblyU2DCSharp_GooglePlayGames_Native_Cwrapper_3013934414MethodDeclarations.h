﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback
struct FetchForPlayerCallback_t1995639285;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3670871388.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_1995639285.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_4049911828.h"

// System.Void GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayer(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.StatsManager/FetchForPlayerCallback,System.IntPtr)
extern "C"  void StatsManager_StatsManager_FetchForPlayer_m163136903 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, int32_t ___data_source1, FetchForPlayerCallback_t1995639285 * ___callback2, IntPtr_t ___callback_arg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void StatsManager_StatsManager_FetchForPlayerResponse_Dispose_m2997693225 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t StatsManager_StatsManager_FetchForPlayerResponse_GetStatus_m333399849 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.StatsManager::StatsManager_FetchForPlayerResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t StatsManager_StatsManager_FetchForPlayerResponse_GetData_m3796184149 (Il2CppObject * __this /* static, unused */, HandleRef_t1780819301  ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
