﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.Nearby.DummyNearbyConnectionClient
struct  DummyNearbyConnectionClient_t2104311078  : public Il2CppObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
