﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey98
struct U3CLeaveDuringTurnU3Ec__AnonStorey98_t1470863565;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t3337232325;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_M3337232325.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey98::.ctor()
extern "C"  void U3CLeaveDuringTurnU3Ec__AnonStorey98__ctor_m1655950446 (U3CLeaveDuringTurnU3Ec__AnonStorey98_t1470863565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey98::<>m__87(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CLeaveDuringTurnU3Ec__AnonStorey98_U3CU3Em__87_m322392355 (U3CLeaveDuringTurnU3Ec__AnonStorey98_t1470863565 * __this, MultiplayerParticipant_t3337232325 * ___pendingParticipant0, NativeTurnBasedMatch_t302853426 * ___foundMatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey98::<>m__91(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C"  void U3CLeaveDuringTurnU3Ec__AnonStorey98_U3CU3Em__91_m3680270680 (U3CLeaveDuringTurnU3Ec__AnonStorey98_t1470863565 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
