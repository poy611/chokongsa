﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVertexPosition
struct GetVertexPosition_t1771096113;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::.ctor()
extern "C"  void GetVertexPosition__ctor_m1741323301 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::Reset()
extern "C"  void GetVertexPosition_Reset_m3682723538 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnEnter()
extern "C"  void GetVertexPosition_OnEnter_m2061142716 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnUpdate()
extern "C"  void GetVertexPosition_OnUpdate_m2899441415 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::DoGetVertexPosition()
extern "C"  void GetVertexPosition_DoGetVertexPosition_m3709014619 (GetVertexPosition_t1771096113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
