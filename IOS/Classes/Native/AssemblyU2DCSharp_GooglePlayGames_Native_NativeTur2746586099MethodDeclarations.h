﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey99
struct U3CCancelU3Ec__AnonStorey99_t2746586099;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Na302853426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey99::.ctor()
extern "C"  void U3CCancelU3Ec__AnonStorey99__ctor_m72529112 (U3CCancelU3Ec__AnonStorey99_t2746586099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey99::<>m__88(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C"  void U3CCancelU3Ec__AnonStorey99_U3CU3Em__88_m2071235400 (U3CCancelU3Ec__AnonStorey99_t2746586099 * __this, NativeTurnBasedMatch_t302853426 * ___foundMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey99::<>m__92(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C"  void U3CCancelU3Ec__AnonStorey99_U3CU3Em__92_m2581001165 (U3CCancelU3Ec__AnonStorey99_t2746586099 * __this, int32_t ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
