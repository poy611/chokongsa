﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime
struct SetAnimatorPlayBackTime_t698453439;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::.ctor()
extern "C"  void SetAnimatorPlayBackTime__ctor_m2524661847 (SetAnimatorPlayBackTime_t698453439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::Reset()
extern "C"  void SetAnimatorPlayBackTime_Reset_m171094788 (SetAnimatorPlayBackTime_t698453439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::OnEnter()
extern "C"  void SetAnimatorPlayBackTime_OnEnter_m3230208622 (SetAnimatorPlayBackTime_t698453439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::OnUpdate()
extern "C"  void SetAnimatorPlayBackTime_OnUpdate_m485778837 (SetAnimatorPlayBackTime_t698453439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::DoPlaybackTime()
extern "C"  void SetAnimatorPlayBackTime_DoPlaybackTime_m1144880640 (SetAnimatorPlayBackTime_t698453439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
