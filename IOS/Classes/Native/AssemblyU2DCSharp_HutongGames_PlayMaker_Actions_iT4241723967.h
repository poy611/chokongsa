﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenScaleUpdate
struct  iTweenScaleUpdate_t4241723967  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenScaleUpdate::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenScaleUpdate::transformScale
	FsmGameObject_t1697147867 * ___transformScale_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenScaleUpdate::vectorScale
	FsmVector3_t533912882 * ___vectorScale_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleUpdate::time
	FsmFloat_t2134102846 * ___time_14;
	// System.Collections.Hashtable HutongGames.PlayMaker.Actions.iTweenScaleUpdate::hash
	Hashtable_t1407064410 * ___hash_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.iTweenScaleUpdate::go
	GameObject_t3674682005 * ___go_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_transformScale_12() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___transformScale_12)); }
	inline FsmGameObject_t1697147867 * get_transformScale_12() const { return ___transformScale_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_transformScale_12() { return &___transformScale_12; }
	inline void set_transformScale_12(FsmGameObject_t1697147867 * value)
	{
		___transformScale_12 = value;
		Il2CppCodeGenWriteBarrier(&___transformScale_12, value);
	}

	inline static int32_t get_offset_of_vectorScale_13() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___vectorScale_13)); }
	inline FsmVector3_t533912882 * get_vectorScale_13() const { return ___vectorScale_13; }
	inline FsmVector3_t533912882 ** get_address_of_vectorScale_13() { return &___vectorScale_13; }
	inline void set_vectorScale_13(FsmVector3_t533912882 * value)
	{
		___vectorScale_13 = value;
		Il2CppCodeGenWriteBarrier(&___vectorScale_13, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___time_14)); }
	inline FsmFloat_t2134102846 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2134102846 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2134102846 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier(&___time_14, value);
	}

	inline static int32_t get_offset_of_hash_15() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___hash_15)); }
	inline Hashtable_t1407064410 * get_hash_15() const { return ___hash_15; }
	inline Hashtable_t1407064410 ** get_address_of_hash_15() { return &___hash_15; }
	inline void set_hash_15(Hashtable_t1407064410 * value)
	{
		___hash_15 = value;
		Il2CppCodeGenWriteBarrier(&___hash_15, value);
	}

	inline static int32_t get_offset_of_go_16() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t4241723967, ___go_16)); }
	inline GameObject_t3674682005 * get_go_16() const { return ___go_16; }
	inline GameObject_t3674682005 ** get_address_of_go_16() { return &___go_16; }
	inline void set_go_16(GameObject_t3674682005 * value)
	{
		___go_16 = value;
		Il2CppCodeGenWriteBarrier(&___go_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
