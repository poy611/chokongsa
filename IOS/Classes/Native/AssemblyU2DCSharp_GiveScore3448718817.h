﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// StructforMinigame[]
struct StructforMinigameU5BU5D_t1843399504;
// PopupManagement
struct PopupManagement_t282433007;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GiveScore
struct  GiveScore_t3448718817  : public MonoBehaviour_t667441552
{
public:
	// System.Diagnostics.Stopwatch GiveScore::sw
	Stopwatch_t3420517611 * ___sw_2;
	// System.Int32 GiveScore::nowNum
	int32_t ___nowNum_3;
	// System.Single GiveScore::ticks
	float ___ticks_4;
	// StructforMinigame[] GiveScore::stuff
	StructforMinigameU5BU5D_t1843399504* ___stuff_5;
	// PopupManagement GiveScore::popup
	PopupManagement_t282433007 * ___popup_6;

public:
	inline static int32_t get_offset_of_sw_2() { return static_cast<int32_t>(offsetof(GiveScore_t3448718817, ___sw_2)); }
	inline Stopwatch_t3420517611 * get_sw_2() const { return ___sw_2; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_2() { return &___sw_2; }
	inline void set_sw_2(Stopwatch_t3420517611 * value)
	{
		___sw_2 = value;
		Il2CppCodeGenWriteBarrier(&___sw_2, value);
	}

	inline static int32_t get_offset_of_nowNum_3() { return static_cast<int32_t>(offsetof(GiveScore_t3448718817, ___nowNum_3)); }
	inline int32_t get_nowNum_3() const { return ___nowNum_3; }
	inline int32_t* get_address_of_nowNum_3() { return &___nowNum_3; }
	inline void set_nowNum_3(int32_t value)
	{
		___nowNum_3 = value;
	}

	inline static int32_t get_offset_of_ticks_4() { return static_cast<int32_t>(offsetof(GiveScore_t3448718817, ___ticks_4)); }
	inline float get_ticks_4() const { return ___ticks_4; }
	inline float* get_address_of_ticks_4() { return &___ticks_4; }
	inline void set_ticks_4(float value)
	{
		___ticks_4 = value;
	}

	inline static int32_t get_offset_of_stuff_5() { return static_cast<int32_t>(offsetof(GiveScore_t3448718817, ___stuff_5)); }
	inline StructforMinigameU5BU5D_t1843399504* get_stuff_5() const { return ___stuff_5; }
	inline StructforMinigameU5BU5D_t1843399504** get_address_of_stuff_5() { return &___stuff_5; }
	inline void set_stuff_5(StructforMinigameU5BU5D_t1843399504* value)
	{
		___stuff_5 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_5, value);
	}

	inline static int32_t get_offset_of_popup_6() { return static_cast<int32_t>(offsetof(GiveScore_t3448718817, ___popup_6)); }
	inline PopupManagement_t282433007 * get_popup_6() const { return ___popup_6; }
	inline PopupManagement_t282433007 ** get_address_of_popup_6() { return &___popup_6; }
	inline void set_popup_6(PopupManagement_t282433007 * value)
	{
		___popup_6 = value;
		Il2CppCodeGenWriteBarrier(&___popup_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
