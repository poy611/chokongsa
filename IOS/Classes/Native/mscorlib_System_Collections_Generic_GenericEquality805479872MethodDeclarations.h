﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>
struct GenericEqualityComparer_1_t805479872;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m317513656_gshared (GenericEqualityComparer_1_t805479872 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m317513656(__this, method) ((  void (*) (GenericEqualityComparer_1_t805479872 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m317513656_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m4213324347_gshared (GenericEqualityComparer_1_t805479872 * __this, TypeConvertKey_t866134174  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m4213324347(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t805479872 *, TypeConvertKey_t866134174 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m4213324347_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m1926478029_gshared (GenericEqualityComparer_1_t805479872 * __this, TypeConvertKey_t866134174  ___x0, TypeConvertKey_t866134174  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m1926478029(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t805479872 *, TypeConvertKey_t866134174 , TypeConvertKey_t866134174 , const MethodInfo*))GenericEqualityComparer_1_Equals_m1926478029_gshared)(__this, ___x0, ___y1, method)
