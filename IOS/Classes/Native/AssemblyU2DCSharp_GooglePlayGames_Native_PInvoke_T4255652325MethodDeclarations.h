﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t4255652325;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::.ctor(System.IntPtr)
extern "C"  void TurnBasedMatchResponse__ctor_m3633152958 (TurnBasedMatchResponse_t4255652325 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::ResponseStatus()
extern "C"  int32_t TurnBasedMatchResponse_ResponseStatus_m2592316137 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::RequestSucceeded()
extern "C"  bool TurnBasedMatchResponse_RequestSucceeded_m627747956 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::Match()
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchResponse_Match_m2858914191 (TurnBasedMatchResponse_t4255652325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchResponse_CallDispose_m283130290 (TurnBasedMatchResponse_t4255652325 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::FromPointer(System.IntPtr)
extern "C"  TurnBasedMatchResponse_t4255652325 * TurnBasedMatchResponse_FromPointer_m863377605 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
