﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CameraFadeIn
struct CameraFadeIn_t2254266990;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::.ctor()
extern "C"  void CameraFadeIn__ctor_m2325620664 (CameraFadeIn_t2254266990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::Reset()
extern "C"  void CameraFadeIn_Reset_m4267020901 (CameraFadeIn_t2254266990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnEnter()
extern "C"  void CameraFadeIn_OnEnter_m930192783 (CameraFadeIn_t2254266990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnUpdate()
extern "C"  void CameraFadeIn_OnUpdate_m2199731860 (CameraFadeIn_t2254266990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnGUI()
extern "C"  void CameraFadeIn_OnGUI_m1821019314 (CameraFadeIn_t2254266990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
