﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m304723487(__this, ___l0, method) ((  void (*) (Enumerator_t2464284800 *, List_1_t2444612030 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3225798355(__this, method) ((  void (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2025564095(__this, method) ((  Il2CppObject * (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::Dispose()
#define Enumerator_Dispose_m2949807044(__this, method) ((  void (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::VerifyState()
#define Enumerator_VerifyState_m2954541309(__this, method) ((  void (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::MoveNext()
#define Enumerator_MoveNext_m2119401332(__this, method) ((  bool (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::get_Current()
#define Enumerator_get_Current_m2595564828(__this, method) ((  FsmRect_t1076426478 * (*) (Enumerator_t2464284800 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
