﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F
struct U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607;
// GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse
struct FetchForPlayerResponse_t1267220047;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_S1267220047.h"

// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F::.ctor()
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey4F__ctor_m457940924 (U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<GetPlayerStats>c__AnonStorey4F::<>m__26(GooglePlayGames.Native.PInvoke.StatsManager/FetchForPlayerResponse)
extern "C"  void U3CGetPlayerStatsU3Ec__AnonStorey4F_U3CU3Em__26_m626914394 (U3CGetPlayerStatsU3Ec__AnonStorey4F_t1415230607 * __this, FetchForPlayerResponse_t1267220047 * ___playerStatsResponse0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
