﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenToWorldPoint
struct  ScreenToWorldPoint_t174806253  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenVector
	FsmVector3_t533912882 * ___screenVector_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenX
	FsmFloat_t2134102846 * ___screenX_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenY
	FsmFloat_t2134102846 * ___screenY_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenZ
	FsmFloat_t2134102846 * ___screenZ_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenToWorldPoint::normalized
	FsmBool_t1075959796 * ___normalized_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldVector
	FsmVector3_t533912882 * ___storeWorldVector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldX
	FsmFloat_t2134102846 * ___storeWorldX_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldY
	FsmFloat_t2134102846 * ___storeWorldY_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldZ
	FsmFloat_t2134102846 * ___storeWorldZ_19;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenToWorldPoint::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_screenVector_11() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenVector_11)); }
	inline FsmVector3_t533912882 * get_screenVector_11() const { return ___screenVector_11; }
	inline FsmVector3_t533912882 ** get_address_of_screenVector_11() { return &___screenVector_11; }
	inline void set_screenVector_11(FsmVector3_t533912882 * value)
	{
		___screenVector_11 = value;
		Il2CppCodeGenWriteBarrier(&___screenVector_11, value);
	}

	inline static int32_t get_offset_of_screenX_12() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenX_12)); }
	inline FsmFloat_t2134102846 * get_screenX_12() const { return ___screenX_12; }
	inline FsmFloat_t2134102846 ** get_address_of_screenX_12() { return &___screenX_12; }
	inline void set_screenX_12(FsmFloat_t2134102846 * value)
	{
		___screenX_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenX_12, value);
	}

	inline static int32_t get_offset_of_screenY_13() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenY_13)); }
	inline FsmFloat_t2134102846 * get_screenY_13() const { return ___screenY_13; }
	inline FsmFloat_t2134102846 ** get_address_of_screenY_13() { return &___screenY_13; }
	inline void set_screenY_13(FsmFloat_t2134102846 * value)
	{
		___screenY_13 = value;
		Il2CppCodeGenWriteBarrier(&___screenY_13, value);
	}

	inline static int32_t get_offset_of_screenZ_14() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___screenZ_14)); }
	inline FsmFloat_t2134102846 * get_screenZ_14() const { return ___screenZ_14; }
	inline FsmFloat_t2134102846 ** get_address_of_screenZ_14() { return &___screenZ_14; }
	inline void set_screenZ_14(FsmFloat_t2134102846 * value)
	{
		___screenZ_14 = value;
		Il2CppCodeGenWriteBarrier(&___screenZ_14, value);
	}

	inline static int32_t get_offset_of_normalized_15() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___normalized_15)); }
	inline FsmBool_t1075959796 * get_normalized_15() const { return ___normalized_15; }
	inline FsmBool_t1075959796 ** get_address_of_normalized_15() { return &___normalized_15; }
	inline void set_normalized_15(FsmBool_t1075959796 * value)
	{
		___normalized_15 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_15, value);
	}

	inline static int32_t get_offset_of_storeWorldVector_16() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldVector_16)); }
	inline FsmVector3_t533912882 * get_storeWorldVector_16() const { return ___storeWorldVector_16; }
	inline FsmVector3_t533912882 ** get_address_of_storeWorldVector_16() { return &___storeWorldVector_16; }
	inline void set_storeWorldVector_16(FsmVector3_t533912882 * value)
	{
		___storeWorldVector_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldVector_16, value);
	}

	inline static int32_t get_offset_of_storeWorldX_17() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldX_17)); }
	inline FsmFloat_t2134102846 * get_storeWorldX_17() const { return ___storeWorldX_17; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldX_17() { return &___storeWorldX_17; }
	inline void set_storeWorldX_17(FsmFloat_t2134102846 * value)
	{
		___storeWorldX_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldX_17, value);
	}

	inline static int32_t get_offset_of_storeWorldY_18() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldY_18)); }
	inline FsmFloat_t2134102846 * get_storeWorldY_18() const { return ___storeWorldY_18; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldY_18() { return &___storeWorldY_18; }
	inline void set_storeWorldY_18(FsmFloat_t2134102846 * value)
	{
		___storeWorldY_18 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldY_18, value);
	}

	inline static int32_t get_offset_of_storeWorldZ_19() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___storeWorldZ_19)); }
	inline FsmFloat_t2134102846 * get_storeWorldZ_19() const { return ___storeWorldZ_19; }
	inline FsmFloat_t2134102846 ** get_address_of_storeWorldZ_19() { return &___storeWorldZ_19; }
	inline void set_storeWorldZ_19(FsmFloat_t2134102846 * value)
	{
		___storeWorldZ_19 = value;
		Il2CppCodeGenWriteBarrier(&___storeWorldZ_19, value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t174806253, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
