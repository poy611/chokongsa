﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutRepeatButton
struct GUILayoutRepeatButton_t3980277856;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::.ctor()
extern "C"  void GUILayoutRepeatButton__ctor_m897391830 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::Reset()
extern "C"  void GUILayoutRepeatButton_Reset_m2838792067 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::OnGUI()
extern "C"  void GUILayoutRepeatButton_OnGUI_m392790480 (GUILayoutRepeatButton_t3980277856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
