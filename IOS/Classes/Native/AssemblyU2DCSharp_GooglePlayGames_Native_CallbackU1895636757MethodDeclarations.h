﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m3213873058_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m3213873058(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey41_2__ctor_m3213873058_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey40`2/<ToOnGameThread>c__AnonStorey41`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::<>m__14()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2758566734_gshared (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2758566734(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey41_2_t1895636757 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey41_2_U3CU3Em__14_m2758566734_gshared)(__this, method)
