﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;
// System.Object
struct Il2CppObject;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t107909964;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8
struct  U3CCheckIfAliveU3Ec__Iterator8_t161768900  : public Il2CppObject
{
public:
	// UnityEngine.ParticleSystem CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::<ps>__0
	ParticleSystem_t381473177 * ___U3CpsU3E__0_0;
	// System.Int32 CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::$PC
	int32_t ___U24PC_1;
	// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::$current
	Il2CppObject * ___U24current_2;
	// CFX_AutoDestructShuriken CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator8::<>f__this
	CFX_AutoDestructShuriken_t107909964 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CpsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator8_t161768900, ___U3CpsU3E__0_0)); }
	inline ParticleSystem_t381473177 * get_U3CpsU3E__0_0() const { return ___U3CpsU3E__0_0; }
	inline ParticleSystem_t381473177 ** get_address_of_U3CpsU3E__0_0() { return &___U3CpsU3E__0_0; }
	inline void set_U3CpsU3E__0_0(ParticleSystem_t381473177 * value)
	{
		___U3CpsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator8_t161768900, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator8_t161768900, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator8_t161768900, ___U3CU3Ef__this_3)); }
	inline CFX_AutoDestructShuriken_t107909964 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline CFX_AutoDestructShuriken_t107909964 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(CFX_AutoDestructShuriken_t107909964 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
