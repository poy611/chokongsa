﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t9039225;
// ShowRecipePre
struct ShowRecipePre_t977589144;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// PopupManagement
struct PopupManagement_t282433007;
// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Variation
struct  ScoreManager_Variation_t1665153935  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreManager_Variation::nowNum
	int32_t ___nowNum_2;
	// System.String ScoreManager_Variation::nowScene
	String_t* ___nowScene_3;
	// System.Single ScoreManager_Variation::ticks
	float ___ticks_4;
	// UnityEngine.UI.Text ScoreManager_Variation::timmer
	Text_t9039225 * ___timmer_5;
	// ShowRecipePre ScoreManager_Variation::recipePre
	ShowRecipePre_t977589144 * ___recipePre_6;
	// UnityEngine.GameObject ScoreManager_Variation::ready
	GameObject_t3674682005 * ___ready_7;
	// UnityEngine.GameObject ScoreManager_Variation::buttonOk
	GameObject_t3674682005 * ___buttonOk_8;
	// StructforMinigame ScoreManager_Variation::stuff
	StructforMinigame_t1286533533 * ___stuff_9;
	// ConstforMinigame ScoreManager_Variation::cuff
	ConstforMinigame_t2789090703 * ___cuff_10;
	// PopupManagement ScoreManager_Variation::pop
	PopupManagement_t282433007 * ___pop_11;
	// PlayMakerFSM ScoreManager_Variation::myFsm
	PlayMakerFSM_t3799847376 * ___myFsm_12;
	// System.Int32 ScoreManager_Variation::missCount
	int32_t ___missCount_13;
	// System.Int32 ScoreManager_Variation::currentIngredient
	int32_t ___currentIngredient_14;
	// System.Diagnostics.Stopwatch ScoreManager_Variation::sw
	Stopwatch_t3420517611 * ___sw_15;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_nowScene_3() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___nowScene_3)); }
	inline String_t* get_nowScene_3() const { return ___nowScene_3; }
	inline String_t** get_address_of_nowScene_3() { return &___nowScene_3; }
	inline void set_nowScene_3(String_t* value)
	{
		___nowScene_3 = value;
		Il2CppCodeGenWriteBarrier(&___nowScene_3, value);
	}

	inline static int32_t get_offset_of_ticks_4() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___ticks_4)); }
	inline float get_ticks_4() const { return ___ticks_4; }
	inline float* get_address_of_ticks_4() { return &___ticks_4; }
	inline void set_ticks_4(float value)
	{
		___ticks_4 = value;
	}

	inline static int32_t get_offset_of_timmer_5() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___timmer_5)); }
	inline Text_t9039225 * get_timmer_5() const { return ___timmer_5; }
	inline Text_t9039225 ** get_address_of_timmer_5() { return &___timmer_5; }
	inline void set_timmer_5(Text_t9039225 * value)
	{
		___timmer_5 = value;
		Il2CppCodeGenWriteBarrier(&___timmer_5, value);
	}

	inline static int32_t get_offset_of_recipePre_6() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___recipePre_6)); }
	inline ShowRecipePre_t977589144 * get_recipePre_6() const { return ___recipePre_6; }
	inline ShowRecipePre_t977589144 ** get_address_of_recipePre_6() { return &___recipePre_6; }
	inline void set_recipePre_6(ShowRecipePre_t977589144 * value)
	{
		___recipePre_6 = value;
		Il2CppCodeGenWriteBarrier(&___recipePre_6, value);
	}

	inline static int32_t get_offset_of_ready_7() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___ready_7)); }
	inline GameObject_t3674682005 * get_ready_7() const { return ___ready_7; }
	inline GameObject_t3674682005 ** get_address_of_ready_7() { return &___ready_7; }
	inline void set_ready_7(GameObject_t3674682005 * value)
	{
		___ready_7 = value;
		Il2CppCodeGenWriteBarrier(&___ready_7, value);
	}

	inline static int32_t get_offset_of_buttonOk_8() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___buttonOk_8)); }
	inline GameObject_t3674682005 * get_buttonOk_8() const { return ___buttonOk_8; }
	inline GameObject_t3674682005 ** get_address_of_buttonOk_8() { return &___buttonOk_8; }
	inline void set_buttonOk_8(GameObject_t3674682005 * value)
	{
		___buttonOk_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonOk_8, value);
	}

	inline static int32_t get_offset_of_stuff_9() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___stuff_9)); }
	inline StructforMinigame_t1286533533 * get_stuff_9() const { return ___stuff_9; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_9() { return &___stuff_9; }
	inline void set_stuff_9(StructforMinigame_t1286533533 * value)
	{
		___stuff_9 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_9, value);
	}

	inline static int32_t get_offset_of_cuff_10() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___cuff_10)); }
	inline ConstforMinigame_t2789090703 * get_cuff_10() const { return ___cuff_10; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_10() { return &___cuff_10; }
	inline void set_cuff_10(ConstforMinigame_t2789090703 * value)
	{
		___cuff_10 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_10, value);
	}

	inline static int32_t get_offset_of_pop_11() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___pop_11)); }
	inline PopupManagement_t282433007 * get_pop_11() const { return ___pop_11; }
	inline PopupManagement_t282433007 ** get_address_of_pop_11() { return &___pop_11; }
	inline void set_pop_11(PopupManagement_t282433007 * value)
	{
		___pop_11 = value;
		Il2CppCodeGenWriteBarrier(&___pop_11, value);
	}

	inline static int32_t get_offset_of_myFsm_12() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___myFsm_12)); }
	inline PlayMakerFSM_t3799847376 * get_myFsm_12() const { return ___myFsm_12; }
	inline PlayMakerFSM_t3799847376 ** get_address_of_myFsm_12() { return &___myFsm_12; }
	inline void set_myFsm_12(PlayMakerFSM_t3799847376 * value)
	{
		___myFsm_12 = value;
		Il2CppCodeGenWriteBarrier(&___myFsm_12, value);
	}

	inline static int32_t get_offset_of_missCount_13() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___missCount_13)); }
	inline int32_t get_missCount_13() const { return ___missCount_13; }
	inline int32_t* get_address_of_missCount_13() { return &___missCount_13; }
	inline void set_missCount_13(int32_t value)
	{
		___missCount_13 = value;
	}

	inline static int32_t get_offset_of_currentIngredient_14() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___currentIngredient_14)); }
	inline int32_t get_currentIngredient_14() const { return ___currentIngredient_14; }
	inline int32_t* get_address_of_currentIngredient_14() { return &___currentIngredient_14; }
	inline void set_currentIngredient_14(int32_t value)
	{
		___currentIngredient_14 = value;
	}

	inline static int32_t get_offset_of_sw_15() { return static_cast<int32_t>(offsetof(ScoreManager_Variation_t1665153935, ___sw_15)); }
	inline Stopwatch_t3420517611 * get_sw_15() const { return ___sw_15; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_15() { return &___sw_15; }
	inline void set_sw_15(Stopwatch_t3420517611 * value)
	{
		___sw_15 = value;
		Il2CppCodeGenWriteBarrier(&___sw_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
