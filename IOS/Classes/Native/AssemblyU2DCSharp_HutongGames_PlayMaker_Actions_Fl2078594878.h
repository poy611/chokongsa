﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSwitch
struct  FloatSwitch_t2078594878  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSwitch::floatVariable
	FsmFloat_t2134102846 * ___floatVariable_11;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.FloatSwitch::lessThan
	FsmFloatU5BU5D_t2945380875* ___lessThan_12;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.FloatSwitch::sendEvent
	FsmEventU5BU5D_t2862142229* ___sendEvent_13;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSwitch::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_floatVariable_11() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___floatVariable_11)); }
	inline FsmFloat_t2134102846 * get_floatVariable_11() const { return ___floatVariable_11; }
	inline FsmFloat_t2134102846 ** get_address_of_floatVariable_11() { return &___floatVariable_11; }
	inline void set_floatVariable_11(FsmFloat_t2134102846 * value)
	{
		___floatVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_11, value);
	}

	inline static int32_t get_offset_of_lessThan_12() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___lessThan_12)); }
	inline FsmFloatU5BU5D_t2945380875* get_lessThan_12() const { return ___lessThan_12; }
	inline FsmFloatU5BU5D_t2945380875** get_address_of_lessThan_12() { return &___lessThan_12; }
	inline void set_lessThan_12(FsmFloatU5BU5D_t2945380875* value)
	{
		___lessThan_12 = value;
		Il2CppCodeGenWriteBarrier(&___lessThan_12, value);
	}

	inline static int32_t get_offset_of_sendEvent_13() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___sendEvent_13)); }
	inline FsmEventU5BU5D_t2862142229* get_sendEvent_13() const { return ___sendEvent_13; }
	inline FsmEventU5BU5D_t2862142229** get_address_of_sendEvent_13() { return &___sendEvent_13; }
	inline void set_sendEvent_13(FsmEventU5BU5D_t2862142229* value)
	{
		___sendEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(FloatSwitch_t2078594878, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
