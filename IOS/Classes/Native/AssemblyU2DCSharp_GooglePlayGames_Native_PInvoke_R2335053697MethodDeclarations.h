﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStoreyA7
struct U3CToCallbackPointerU3Ec__AnonStoreyA7_t2335053697;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"

// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStoreyA7::.ctor()
extern "C"  void U3CToCallbackPointerU3Ec__AnonStoreyA7__ctor_m436083466 (U3CToCallbackPointerU3Ec__AnonStoreyA7_t2335053697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStoreyA7::<>m__CE(System.IntPtr)
extern "C"  void U3CToCallbackPointerU3Ec__AnonStoreyA7_U3CU3Em__CE_m4270645055 (U3CToCallbackPointerU3Ec__AnonStoreyA7_t2335053697 * __this, IntPtr_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
