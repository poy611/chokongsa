﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t3460122515;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct IEnumerable_1_t2417134198;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct IEnumerable_1_t3603766383;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t3411188537;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t302853426;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_2675372491.h"
#include "mscorlib_System_UIntPtr3365854250.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::.ctor(System.IntPtr)
extern "C"  void TurnBasedMatchesResponse__ctor_m1747058924 (TurnBasedMatchesResponse_t3460122515 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void TurnBasedMatchesResponse_CallDispose_m1111792196 (TurnBasedMatchesResponse_t3460122515 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Status()
extern "C"  int32_t TurnBasedMatchesResponse_Status_m2039759030 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Invitations()
extern "C"  Il2CppObject* TurnBasedMatchesResponse_Invitations_m4255789128 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::InvitationCount()
extern "C"  int32_t TurnBasedMatchesResponse_InvitationCount_m1396940782 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::MyTurnMatches()
extern "C"  Il2CppObject* TurnBasedMatchesResponse_MyTurnMatches_m3551672251 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::MyTurnMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_MyTurnMatchesCount_m706481007 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::TheirTurnMatches()
extern "C"  Il2CppObject* TurnBasedMatchesResponse_TheirTurnMatches_m2855471021 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::TheirTurnMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_TheirTurnMatchesCount_m4021892267 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CompletedMatches()
extern "C"  Il2CppObject* TurnBasedMatchesResponse_CompletedMatches_m3154803929 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CompletedMatchesCount()
extern "C"  int32_t TurnBasedMatchesResponse_CompletedMatchesCount_m1582832383 (TurnBasedMatchesResponse_t3460122515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::FromPointer(System.IntPtr)
extern "C"  TurnBasedMatchesResponse_t3460122515 * TurnBasedMatchesResponse_FromPointer_m2393966405 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<Invitations>m__D4(System.UIntPtr)
extern "C"  MultiplayerInvitation_t3411188537 * TurnBasedMatchesResponse_U3CInvitationsU3Em__D4_m2353585873 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<MyTurnMatches>m__D5(System.UIntPtr)
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CMyTurnMatchesU3Em__D5_m3680258111 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<TheirTurnMatches>m__D6(System.UIntPtr)
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CTheirTurnMatchesU3Em__D6_m2126903584 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<CompletedMatches>m__D7(System.UIntPtr)
extern "C"  NativeTurnBasedMatch_t302853426 * TurnBasedMatchesResponse_U3CCompletedMatchesU3Em__D7_m2722430541 (TurnBasedMatchesResponse_t3460122515 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
