﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3891715294.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li814405322.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2175399139_gshared (InternalEnumerator_1_t3891715294 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2175399139(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3891715294 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2175399139_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269(__this, method) ((  void (*) (InternalEnumerator_1_t3891715294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m886626269_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3891715294 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3969638921_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3422989946_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3422989946(__this, method) ((  void (*) (InternalEnumerator_1_t3891715294 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3422989946_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3932259529_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3932259529(__this, method) ((  bool (*) (InternalEnumerator_1_t3891715294 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3932259529_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Char>>::get_Current()
extern "C"  Link_t814405322  InternalEnumerator_1_get_Current_m3975311466_gshared (InternalEnumerator_1_t3891715294 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3975311466(__this, method) ((  Link_t814405322  (*) (InternalEnumerator_1_t3891715294 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3975311466_gshared)(__this, method)
