﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RunFSM
struct RunFSM_t3316021917;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RunFSM::.ctor()
extern "C"  void RunFSM__ctor_m4009215593 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::Reset()
extern "C"  void RunFSM_Reset_m1655648534 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::Awake()
extern "C"  void RunFSM_Awake_m4246820812 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::OnEnter()
extern "C"  void RunFSM_OnEnter_m3957216256 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSM::CheckIfFinished()
extern "C"  void RunFSM_CheckIfFinished_m3021569566 (RunFSM_t3316021917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
