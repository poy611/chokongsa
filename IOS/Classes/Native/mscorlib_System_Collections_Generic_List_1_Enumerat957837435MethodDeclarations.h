﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t938164665;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957837435.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3119637849_gshared (Enumerator_t957837435 * __this, List_1_t938164665 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3119637849(__this, ___l0, method) ((  void (*) (Enumerator_t957837435 *, List_1_t938164665 *, const MethodInfo*))Enumerator__ctor_m3119637849_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1612151641_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1612151641(__this, method) ((  void (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1612151641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3556448271(__this, method) ((  Il2CppObject * (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3556448271_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::Dispose()
extern "C"  void Enumerator_Dispose_m1969658238_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1969658238(__this, method) ((  void (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_Dispose_m1969658238_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::VerifyState()
extern "C"  void Enumerator_VerifyState_m781543863_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m781543863(__this, method) ((  void (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_VerifyState_m781543863_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2587609597_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2587609597(__this, method) ((  bool (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_MoveNext_m2587609597_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Newtonsoft.Json.JsonPosition>::get_Current()
extern "C"  JsonPosition_t3864946409  Enumerator_get_Current_m957964607_gshared (Enumerator_t957837435 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m957964607(__this, method) ((  JsonPosition_t3864946409  (*) (Enumerator_t957837435 *, const MethodInfo*))Enumerator_get_Current_m957964607_gshared)(__this, method)
