﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En567139676.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23443564286.h"
#include "UnityEngine_UnityEngine_RaycastHit2D1374744384.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3269824848_gshared (Enumerator_t567139676 * __this, Dictionary_2_t3544783580 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3269824848(__this, ___dictionary0, method) ((  void (*) (Enumerator_t567139676 *, Dictionary_2_t3544783580 *, const MethodInfo*))Enumerator__ctor_m3269824848_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3227461403_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3227461403(__this, method) ((  Il2CppObject * (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3227461403_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3877227877_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3877227877(__this, method) ((  void (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3877227877_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4254063836_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4254063836(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4254063836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m838689719_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m838689719(__this, method) ((  Il2CppObject * (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m838689719_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3870371977_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3870371977(__this, method) ((  Il2CppObject * (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3870371977_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3675212181_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3675212181(__this, method) ((  bool (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_MoveNext_m3675212181_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  KeyValuePair_2_t3443564286  Enumerator_get_Current_m3503833607_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3503833607(__this, method) ((  KeyValuePair_2_t3443564286  (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_get_Current_m3503833607_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1259765598_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1259765598(__this, method) ((  Il2CppObject * (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_get_CurrentKey_m1259765598_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_CurrentValue()
extern "C"  RaycastHit2D_t1374744384  Enumerator_get_CurrentValue_m2877790494_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2877790494(__this, method) ((  RaycastHit2D_t1374744384  (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_get_CurrentValue_m2877790494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Reset()
extern "C"  void Enumerator_Reset_m2383229858_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2383229858(__this, method) ((  void (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_Reset_m2383229858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2581578283_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2581578283(__this, method) ((  void (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_VerifyState_m2581578283_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1150504659_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1150504659(__this, method) ((  void (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_VerifyCurrent_m1150504659_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m3589753842_gshared (Enumerator_t567139676 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3589753842(__this, method) ((  void (*) (Enumerator_t567139676 *, const MethodInfo*))Enumerator_Dispose_m3589753842_gshared)(__this, method)
