﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>
struct DefaultComparer_t2312723076;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>::.ctor()
extern "C"  void DefaultComparer__ctor_m1868775986_gshared (DefaultComparer_t2312723076 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1868775986(__this, method) ((  void (*) (DefaultComparer_t2312723076 *, const MethodInfo*))DefaultComparer__ctor_m1868775986_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2246197889_gshared (DefaultComparer_t2312723076 * __this, JsonPosition_t3864946409  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2246197889(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2312723076 *, JsonPosition_t3864946409 , const MethodInfo*))DefaultComparer_GetHashCode_m2246197889_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Newtonsoft.Json.JsonPosition>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1385599559_gshared (DefaultComparer_t2312723076 * __this, JsonPosition_t3864946409  ___x0, JsonPosition_t3864946409  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1385599559(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2312723076 *, JsonPosition_t3864946409 , JsonPosition_t3864946409 , const MethodInfo*))DefaultComparer_Equals_m1385599559_gshared)(__this, ___x0, ___y1, method)
