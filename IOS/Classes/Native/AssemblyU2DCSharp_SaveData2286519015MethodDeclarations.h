﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveData
struct SaveData_t2286519015;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveData::.ctor()
extern "C"  void SaveData__ctor_m2081329044 (SaveData_t2286519015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
