﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3821466628MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1394861108(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2471845802 *, Dictionary_2_t845086351 *, const MethodInfo*))KeyCollection__ctor_m3137692258_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1623762082(__this, ___item0, method) ((  void (*) (KeyCollection_t2471845802 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3238035892_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3335752345(__this, method) ((  void (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4213408811_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m364928936(__this, ___item0, method) ((  bool (*) (KeyCollection_t2471845802 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2107760086_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2423298829(__this, ___item0, method) ((  bool (*) (KeyCollection_t2471845802 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2505559227_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m126153557(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2614311655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m277172619(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2471845802 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3194060829_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1191501254(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m250342616_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1390447881(__this, method) ((  bool (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1213937591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2484378875(__this, method) ((  bool (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1542859049_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2683723751(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m575153813_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1024400105(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2471845802 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3162820887_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::GetEnumerator()
#define KeyCollection_GetEnumerator_m511876684(__this, method) ((  Enumerator_t1460022405  (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_GetEnumerator_m3181750650_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>::get_Count()
#define KeyCollection_get_Count_m1368324545(__this, method) ((  int32_t (*) (KeyCollection_t2471845802 *, const MethodInfo*))KeyCollection_get_Count_m6474735_gshared)(__this, method)
