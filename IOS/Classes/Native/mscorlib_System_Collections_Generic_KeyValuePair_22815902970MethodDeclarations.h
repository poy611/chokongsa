﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22815902970.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertUt866134174.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2411535999_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2411535999(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2815902970 *, TypeConvertKey_t866134174 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2411535999_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Key()
extern "C"  TypeConvertKey_t866134174  KeyValuePair_2_get_Key_m1364625929_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1364625929(__this, method) ((  TypeConvertKey_t866134174  (*) (KeyValuePair_2_t2815902970 *, const MethodInfo*))KeyValuePair_2_get_Key_m1364625929_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1634396234_gshared (KeyValuePair_2_t2815902970 * __this, TypeConvertKey_t866134174  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1634396234(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2815902970 *, TypeConvertKey_t866134174 , const MethodInfo*))KeyValuePair_2_set_Key_m1634396234_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1894627949_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1894627949(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2815902970 *, const MethodInfo*))KeyValuePair_2_get_Value_m1894627949_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1512376138_gshared (KeyValuePair_2_t2815902970 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1512376138(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2815902970 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1512376138_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m52328446_gshared (KeyValuePair_2_t2815902970 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m52328446(__this, method) ((  String_t* (*) (KeyValuePair_2_t2815902970 *, const MethodInfo*))KeyValuePair_2_ToString_m52328446_gshared)(__this, method)
