﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1859153240.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23076810564.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m977979401_gshared (InternalEnumerator_1_t1859153240 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m977979401(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1859153240 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m977979401_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967(__this, method) ((  void (*) (InternalEnumerator_1_t1859153240 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1039070967_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1859153240 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3310022253_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3348633888_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3348633888(__this, method) ((  void (*) (InternalEnumerator_1_t1859153240 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3348633888_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2462826151_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2462826151(__this, method) ((  bool (*) (InternalEnumerator_1_t1859153240 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2462826151_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>::get_Current()
extern "C"  KeyValuePair_2_t3076810564  InternalEnumerator_1_get_Current_m1383438002_gshared (InternalEnumerator_1_t1859153240 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1383438002(__this, method) ((  KeyValuePair_2_t3076810564  (*) (InternalEnumerator_1_t1859153240 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1383438002_gshared)(__this, method)
