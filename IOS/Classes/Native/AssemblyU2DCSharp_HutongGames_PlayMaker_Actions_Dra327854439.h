﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914;
// UnityEngine.SpringJoint
struct SpringJoint_t558455091;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DragRigidBody
struct  DragRigidBody_t327854439  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DragRigidBody::spring
	FsmFloat_t2134102846 * ___spring_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DragRigidBody::damper
	FsmFloat_t2134102846 * ___damper_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DragRigidBody::drag
	FsmFloat_t2134102846 * ___drag_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DragRigidBody::angularDrag
	FsmFloat_t2134102846 * ___angularDrag_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DragRigidBody::distance
	FsmFloat_t2134102846 * ___distance_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DragRigidBody::attachToCenterOfMass
	FsmBool_t1075959796 * ___attachToCenterOfMass_16;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DragRigidBody::draggingPlaneTransform
	FsmOwnerDefault_t251897112 * ___draggingPlaneTransform_17;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.DragRigidBody::layerMask
	FsmIntU5BU5D_t1976821196* ___layerMask_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DragRigidBody::invertMask
	FsmBool_t1075959796 * ___invertMask_19;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.DragRigidBody::dragTaggedOnly
	FsmStringU5BU5D_t2523845914* ___dragTaggedOnly_20;
	// UnityEngine.SpringJoint HutongGames.PlayMaker.Actions.DragRigidBody::springJoint
	SpringJoint_t558455091 * ___springJoint_21;
	// System.Boolean HutongGames.PlayMaker.Actions.DragRigidBody::isDragging
	bool ___isDragging_22;
	// System.Single HutongGames.PlayMaker.Actions.DragRigidBody::oldDrag
	float ___oldDrag_23;
	// System.Single HutongGames.PlayMaker.Actions.DragRigidBody::oldAngularDrag
	float ___oldAngularDrag_24;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.DragRigidBody::_cam
	Camera_t2727095145 * ____cam_25;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.DragRigidBody::_goPlane
	GameObject_t3674682005 * ____goPlane_26;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.DragRigidBody::_dragStartPos
	Vector3_t4282066566  ____dragStartPos_27;
	// System.Single HutongGames.PlayMaker.Actions.DragRigidBody::dragDistance
	float ___dragDistance_28;

public:
	inline static int32_t get_offset_of_spring_11() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___spring_11)); }
	inline FsmFloat_t2134102846 * get_spring_11() const { return ___spring_11; }
	inline FsmFloat_t2134102846 ** get_address_of_spring_11() { return &___spring_11; }
	inline void set_spring_11(FsmFloat_t2134102846 * value)
	{
		___spring_11 = value;
		Il2CppCodeGenWriteBarrier(&___spring_11, value);
	}

	inline static int32_t get_offset_of_damper_12() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___damper_12)); }
	inline FsmFloat_t2134102846 * get_damper_12() const { return ___damper_12; }
	inline FsmFloat_t2134102846 ** get_address_of_damper_12() { return &___damper_12; }
	inline void set_damper_12(FsmFloat_t2134102846 * value)
	{
		___damper_12 = value;
		Il2CppCodeGenWriteBarrier(&___damper_12, value);
	}

	inline static int32_t get_offset_of_drag_13() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___drag_13)); }
	inline FsmFloat_t2134102846 * get_drag_13() const { return ___drag_13; }
	inline FsmFloat_t2134102846 ** get_address_of_drag_13() { return &___drag_13; }
	inline void set_drag_13(FsmFloat_t2134102846 * value)
	{
		___drag_13 = value;
		Il2CppCodeGenWriteBarrier(&___drag_13, value);
	}

	inline static int32_t get_offset_of_angularDrag_14() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___angularDrag_14)); }
	inline FsmFloat_t2134102846 * get_angularDrag_14() const { return ___angularDrag_14; }
	inline FsmFloat_t2134102846 ** get_address_of_angularDrag_14() { return &___angularDrag_14; }
	inline void set_angularDrag_14(FsmFloat_t2134102846 * value)
	{
		___angularDrag_14 = value;
		Il2CppCodeGenWriteBarrier(&___angularDrag_14, value);
	}

	inline static int32_t get_offset_of_distance_15() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___distance_15)); }
	inline FsmFloat_t2134102846 * get_distance_15() const { return ___distance_15; }
	inline FsmFloat_t2134102846 ** get_address_of_distance_15() { return &___distance_15; }
	inline void set_distance_15(FsmFloat_t2134102846 * value)
	{
		___distance_15 = value;
		Il2CppCodeGenWriteBarrier(&___distance_15, value);
	}

	inline static int32_t get_offset_of_attachToCenterOfMass_16() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___attachToCenterOfMass_16)); }
	inline FsmBool_t1075959796 * get_attachToCenterOfMass_16() const { return ___attachToCenterOfMass_16; }
	inline FsmBool_t1075959796 ** get_address_of_attachToCenterOfMass_16() { return &___attachToCenterOfMass_16; }
	inline void set_attachToCenterOfMass_16(FsmBool_t1075959796 * value)
	{
		___attachToCenterOfMass_16 = value;
		Il2CppCodeGenWriteBarrier(&___attachToCenterOfMass_16, value);
	}

	inline static int32_t get_offset_of_draggingPlaneTransform_17() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___draggingPlaneTransform_17)); }
	inline FsmOwnerDefault_t251897112 * get_draggingPlaneTransform_17() const { return ___draggingPlaneTransform_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_draggingPlaneTransform_17() { return &___draggingPlaneTransform_17; }
	inline void set_draggingPlaneTransform_17(FsmOwnerDefault_t251897112 * value)
	{
		___draggingPlaneTransform_17 = value;
		Il2CppCodeGenWriteBarrier(&___draggingPlaneTransform_17, value);
	}

	inline static int32_t get_offset_of_layerMask_18() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___layerMask_18)); }
	inline FsmIntU5BU5D_t1976821196* get_layerMask_18() const { return ___layerMask_18; }
	inline FsmIntU5BU5D_t1976821196** get_address_of_layerMask_18() { return &___layerMask_18; }
	inline void set_layerMask_18(FsmIntU5BU5D_t1976821196* value)
	{
		___layerMask_18 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_18, value);
	}

	inline static int32_t get_offset_of_invertMask_19() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___invertMask_19)); }
	inline FsmBool_t1075959796 * get_invertMask_19() const { return ___invertMask_19; }
	inline FsmBool_t1075959796 ** get_address_of_invertMask_19() { return &___invertMask_19; }
	inline void set_invertMask_19(FsmBool_t1075959796 * value)
	{
		___invertMask_19 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_19, value);
	}

	inline static int32_t get_offset_of_dragTaggedOnly_20() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___dragTaggedOnly_20)); }
	inline FsmStringU5BU5D_t2523845914* get_dragTaggedOnly_20() const { return ___dragTaggedOnly_20; }
	inline FsmStringU5BU5D_t2523845914** get_address_of_dragTaggedOnly_20() { return &___dragTaggedOnly_20; }
	inline void set_dragTaggedOnly_20(FsmStringU5BU5D_t2523845914* value)
	{
		___dragTaggedOnly_20 = value;
		Il2CppCodeGenWriteBarrier(&___dragTaggedOnly_20, value);
	}

	inline static int32_t get_offset_of_springJoint_21() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___springJoint_21)); }
	inline SpringJoint_t558455091 * get_springJoint_21() const { return ___springJoint_21; }
	inline SpringJoint_t558455091 ** get_address_of_springJoint_21() { return &___springJoint_21; }
	inline void set_springJoint_21(SpringJoint_t558455091 * value)
	{
		___springJoint_21 = value;
		Il2CppCodeGenWriteBarrier(&___springJoint_21, value);
	}

	inline static int32_t get_offset_of_isDragging_22() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___isDragging_22)); }
	inline bool get_isDragging_22() const { return ___isDragging_22; }
	inline bool* get_address_of_isDragging_22() { return &___isDragging_22; }
	inline void set_isDragging_22(bool value)
	{
		___isDragging_22 = value;
	}

	inline static int32_t get_offset_of_oldDrag_23() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___oldDrag_23)); }
	inline float get_oldDrag_23() const { return ___oldDrag_23; }
	inline float* get_address_of_oldDrag_23() { return &___oldDrag_23; }
	inline void set_oldDrag_23(float value)
	{
		___oldDrag_23 = value;
	}

	inline static int32_t get_offset_of_oldAngularDrag_24() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___oldAngularDrag_24)); }
	inline float get_oldAngularDrag_24() const { return ___oldAngularDrag_24; }
	inline float* get_address_of_oldAngularDrag_24() { return &___oldAngularDrag_24; }
	inline void set_oldAngularDrag_24(float value)
	{
		___oldAngularDrag_24 = value;
	}

	inline static int32_t get_offset_of__cam_25() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ____cam_25)); }
	inline Camera_t2727095145 * get__cam_25() const { return ____cam_25; }
	inline Camera_t2727095145 ** get_address_of__cam_25() { return &____cam_25; }
	inline void set__cam_25(Camera_t2727095145 * value)
	{
		____cam_25 = value;
		Il2CppCodeGenWriteBarrier(&____cam_25, value);
	}

	inline static int32_t get_offset_of__goPlane_26() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ____goPlane_26)); }
	inline GameObject_t3674682005 * get__goPlane_26() const { return ____goPlane_26; }
	inline GameObject_t3674682005 ** get_address_of__goPlane_26() { return &____goPlane_26; }
	inline void set__goPlane_26(GameObject_t3674682005 * value)
	{
		____goPlane_26 = value;
		Il2CppCodeGenWriteBarrier(&____goPlane_26, value);
	}

	inline static int32_t get_offset_of__dragStartPos_27() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ____dragStartPos_27)); }
	inline Vector3_t4282066566  get__dragStartPos_27() const { return ____dragStartPos_27; }
	inline Vector3_t4282066566 * get_address_of__dragStartPos_27() { return &____dragStartPos_27; }
	inline void set__dragStartPos_27(Vector3_t4282066566  value)
	{
		____dragStartPos_27 = value;
	}

	inline static int32_t get_offset_of_dragDistance_28() { return static_cast<int32_t>(offsetof(DragRigidBody_t327854439, ___dragDistance_28)); }
	inline float get_dragDistance_28() const { return ___dragDistance_28; }
	inline float* get_address_of_dragDistance_28() { return &___dragDistance_28; }
	inline void set_dragDistance_28(float value)
	{
		___dragDistance_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
