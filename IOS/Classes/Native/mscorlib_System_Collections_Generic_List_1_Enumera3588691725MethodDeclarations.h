﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m115691836(__this, ___l0, method) ((  void (*) (Enumerator_t3588691725 *, List_1_t3569018955 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m412282262(__this, method) ((  void (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m802233218(__this, method) ((  Il2CppObject * (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::Dispose()
#define Enumerator_Dispose_m2297228833(__this, method) ((  void (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::VerifyState()
#define Enumerator_VerifyState_m3083515098(__this, method) ((  void (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::MoveNext()
#define Enumerator_MoveNext_m4042779394(__this, method) ((  bool (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Invitation>::get_Current()
#define Enumerator_get_Current_m3032911121(__this, method) ((  Invitation_t2200833403 * (*) (Enumerator_t3588691725 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
