﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3029687007.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateTimeZoneHandli2945560484.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1567982783_gshared (Nullable_1_t3029687007 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1567982783(__this, ___value0, method) ((  void (*) (Nullable_1_t3029687007 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1567982783_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3011697484_gshared (Nullable_1_t3029687007 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3011697484(__this, method) ((  bool (*) (Nullable_1_t3029687007 *, const MethodInfo*))Nullable_1_get_HasValue_m3011697484_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3694802279_gshared (Nullable_1_t3029687007 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3694802279(__this, method) ((  int32_t (*) (Nullable_1_t3029687007 *, const MethodInfo*))Nullable_1_get_Value_m3694802279_gshared)(__this, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4103605557_gshared (Nullable_1_t3029687007 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4103605557(__this, ___other0, method) ((  bool (*) (Nullable_1_t3029687007 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m4103605557_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1959735402_gshared (Nullable_1_t3029687007 * __this, Nullable_1_t3029687007  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1959735402(__this, ___other0, method) ((  bool (*) (Nullable_1_t3029687007 *, Nullable_1_t3029687007 , const MethodInfo*))Nullable_1_Equals_m1959735402_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m886238605_gshared (Nullable_1_t3029687007 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m886238605(__this, method) ((  int32_t (*) (Nullable_1_t3029687007 *, const MethodInfo*))Nullable_1_GetHashCode_m886238605_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m281944307_gshared (Nullable_1_t3029687007 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m281944307(__this, method) ((  int32_t (*) (Nullable_1_t3029687007 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m281944307_gshared)(__this, method)
// T System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1869670108_gshared (Nullable_1_t3029687007 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1869670108(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3029687007 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m1869670108_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3206184427_gshared (Nullable_1_t3029687007 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3206184427(__this, method) ((  String_t* (*) (Nullable_1_t3029687007 *, const MethodInfo*))Nullable_1_ToString_m3206184427_gshared)(__this, method)
