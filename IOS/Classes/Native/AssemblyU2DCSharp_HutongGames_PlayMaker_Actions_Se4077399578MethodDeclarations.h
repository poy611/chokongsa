﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFogDensity
struct SetFogDensity_t4077399578;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::.ctor()
extern "C"  void SetFogDensity__ctor_m1239441500 (SetFogDensity_t4077399578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::Reset()
extern "C"  void SetFogDensity_Reset_m3180841737 (SetFogDensity_t4077399578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnEnter()
extern "C"  void SetFogDensity_OnEnter_m789069107 (SetFogDensity_t4077399578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnUpdate()
extern "C"  void SetFogDensity_OnUpdate_m2119865200 (SetFogDensity_t4077399578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::DoSetFogDensity()
extern "C"  void SetFogDensity_DoSetFogDensity_m2007988091 (SetFogDensity_t4077399578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
