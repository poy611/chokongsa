﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerControllerColliderHit
struct PlayMakerControllerColliderHit_t1717485139;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"

// System.Void PlayMakerControllerColliderHit::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void PlayMakerControllerColliderHit_OnControllerColliderHit_m3358584794 (PlayMakerControllerColliderHit_t1717485139 * __this, ControllerColliderHit_t2416790841 * ___hitCollider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerControllerColliderHit::.ctor()
extern "C"  void PlayMakerControllerColliderHit__ctor_m2054239210 (PlayMakerControllerColliderHit_t1717485139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
