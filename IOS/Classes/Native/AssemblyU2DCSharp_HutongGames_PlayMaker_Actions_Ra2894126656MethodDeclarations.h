﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomWait
struct RandomWait_t2894126656;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomWait::.ctor()
extern "C"  void RandomWait__ctor_m2819685926 (RandomWait_t2894126656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::Reset()
extern "C"  void RandomWait_Reset_m466118867 (RandomWait_t2894126656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::OnEnter()
extern "C"  void RandomWait_OnEnter_m3280507005 (RandomWait_t2894126656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::OnUpdate()
extern "C"  void RandomWait_OnUpdate_m2045028710 (RandomWait_t2894126656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
