﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookTo
struct iTweenLookTo_t440825252;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::.ctor()
extern "C"  void iTweenLookTo__ctor_m1203261762 (iTweenLookTo_t440825252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::Reset()
extern "C"  void iTweenLookTo_Reset_m3144661999 (iTweenLookTo_t440825252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnEnter()
extern "C"  void iTweenLookTo_OnEnter_m380079257 (iTweenLookTo_t440825252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnExit()
extern "C"  void iTweenLookTo_OnExit_m159727583 (iTweenLookTo_t440825252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::DoiTween()
extern "C"  void iTweenLookTo_DoiTween_m348687311 (iTweenLookTo_t440825252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
