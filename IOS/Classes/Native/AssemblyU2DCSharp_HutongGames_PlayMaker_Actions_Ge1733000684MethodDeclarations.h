﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetACosine
struct GetACosine_t1733000684;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetACosine::.ctor()
extern "C"  void GetACosine__ctor_m790708218 (GetACosine_t1733000684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetACosine::Reset()
extern "C"  void GetACosine_Reset_m2732108455 (GetACosine_t1733000684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetACosine::OnEnter()
extern "C"  void GetACosine_OnEnter_m3348082001 (GetACosine_t1733000684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetACosine::OnUpdate()
extern "C"  void GetACosine_OnUpdate_m4139853586 (GetACosine_t1733000684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetACosine::DoACosine()
extern "C"  void GetACosine_DoACosine_m4226920327 (GetACosine_t1733000684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
