﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m765033231(__this, ___l0, method) ((  void (*) (Enumerator_t1921771204 *, List_1_t1902098434 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2087365603(__this, method) ((  void (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2255845017(__this, method) ((  Il2CppObject * (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::Dispose()
#define Enumerator_Dispose_m789478580(__this, method) ((  void (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::VerifyState()
#define Enumerator_VerifyState_m1049412077(__this, method) ((  void (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::MoveNext()
#define Enumerator_MoveNext_m2005287358(__this, method) ((  bool (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector3>::get_Current()
#define Enumerator_get_Current_m1264797790(__this, method) ((  FsmVector3_t533912882 * (*) (Enumerator_t1921771204 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
