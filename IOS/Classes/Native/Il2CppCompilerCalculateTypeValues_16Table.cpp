﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_XPathExpression3588072235.h"
#include "System_Xml_System_Xml_XPath_XPathItem3597956134.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope1935109964.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1075073278.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera2530618692.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1273927208.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator1383168931.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe471241848.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType3637370479.h"
#include "System_Xml_System_Xml_XPath_XPathResultType516720010.h"
#include "System_Xml_System_Xml_XPath_XmlDataType545259441.h"
#include "System_Xml_System_Xml_Xsl_XsltContext894076946.h"
#include "System_Xml_System_Xml_ConformanceLevel233510445.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory809616844.h"
#include "System_Xml_Mono_Xml_DTDObjectModel3593115196.h"
#include "System_Xml_Mono_Xml_DictionaryBase2849977869.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat2174423114.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase1997767749.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollection918003794.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollectio2994484303.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection3329514151.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollection87515240.h"
#include "System_Xml_Mono_Xml_DTDContentModel647030886.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection194646180.h"
#include "System_Xml_Mono_Xml_DTDNode2039770680.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration41916820.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3691706913.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration1204852177.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2319669258.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration3913042473.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration2992004394.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCo541155938.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration149008292.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType4011965541.h"
#include "System_Xml_Mono_Xml_DTDOccurence81554249.h"
#include "System_Xml_System_Xml_DTDReader4151552713.h"
#include "System_Xml_System_Xml_EntityHandling2742017190.h"
#include "System_Xml_System_Xml_MonoFIXAttribute2749889992.h"
#include "System_Xml_System_Xml_NameTable2699772693.h"
#include "System_Xml_System_Xml_NameTable_Entry2866414864.h"
#include "System_Xml_System_Xml_NamespaceHandling1516402802.h"
#include "System_Xml_System_Xml_NewLineHandling53740107.h"
#include "System_Xml_System_Xml_ReadState352099245.h"
#include "System_Xml_System_Xml_WhitespaceHandling2567612992.h"
#include "System_Xml_System_Xml_WriteState1937244592.h"
#include "System_Xml_System_Xml_XmlAttribute6647939.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3012627841.h"
#include "System_Xml_System_Xml_XmlCDataSection68050113.h"
#include "System_Xml_System_Xml_XmlChar856576415.h"
#include "System_Xml_System_Xml_XmlCharacterData915341530.h"
#include "System_Xml_System_Xml_XmlComment2893623302.h"
#include "System_Xml_System_Xml_XmlConvert2894815066.h"
#include "System_Xml_System_Xml_XmlDeclaration1240444833.h"
#include "System_Xml_System_Xml_XmlDocument730752740.h"
#include "System_Xml_System_Xml_XmlDocumentFragment1144967508.h"
#include "System_Xml_System_Xml_XmlDocumentNavigator1264434787.h"
#include "System_Xml_System_Xml_XmlDocumentType838630462.h"
#include "System_Xml_System_Xml_XmlElement280387747.h"
#include "System_Xml_System_Xml_XmlEntity3759900140.h"
#include "System_Xml_System_Xml_XmlEntityReference2288721231.h"
#include "System_Xml_System_Xml_XmlException1475188278.h"
#include "System_Xml_System_Xml_XmlImplementation3716119739.h"
#include "System_Xml_System_Xml_XmlStreamReader837919148.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader2673413431.h"
#include "System_Xml_System_Xml_XmlInputStream2962968081.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList867247383.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList_XPathNode968381307.h"
#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"
#include "System_Xml_System_Xml_XmlNameEntry1203257998.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3943949348.h"
#include "System_Xml_System_Xml_XmlNameTable1216706026.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap977482698.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1467853467.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3658211563.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope1749213747.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1675009921.h"
#include "System_Xml_System_Xml_XmlNodeArrayList543782172.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction2010186255.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs1377283246.h"
#include "System_Xml_System_Xml_XmlNodeList991860617.h"
#include "System_Xml_System_Xml_XmlNodeListChildren3993110696.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerat3708952179.h"
#include "System_Xml_System_Xml_XmlNodeOrder444538963.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "System_Xml_System_Xml_XmlNotation1447424459.h"
#include "System_Xml_System_Xml_XmlOutputMethod1995898507.h"
#include "System_Xml_System_Xml_XmlParserContext1291067127.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem3307232354.h"
#include "System_Xml_System_Xml_XmlParserInput3438354706.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInput173147476.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction2161892066.h"
#include "System_Xml_System_Xml_XmlQualifiedName2133315502.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (XPathExpression_t3588072235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (XPathItem_t3597956134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (XPathNamespaceScope_t1935109964)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1602[4] = 
{
	XPathNamespaceScope_t1935109964::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (XPathNavigator_t1075073278), -1, sizeof(XPathNavigator_t1075073278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1603[2] = 
{
	XPathNavigator_t1075073278_StaticFields::get_offset_of_escape_text_chars_0(),
	XPathNavigator_t1075073278_StaticFields::get_offset_of_escape_attr_chars_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (EnumerableIterator_t2530618692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[3] = 
{
	EnumerableIterator_t2530618692::get_offset_of_source_1(),
	EnumerableIterator_t2530618692::get_offset_of_e_2(),
	EnumerableIterator_t2530618692::get_offset_of_pos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (U3CEnumerateChildrenU3Ec__Iterator0_t1273927208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[8] = 
{
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_n_0(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U3CnavU3E__0_1(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U3Cnav2U3E__1_2(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_type_3(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U24PC_4(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U24current_5(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U3CU24U3En_6(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1273927208::get_offset_of_U3CU24U3Etype_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (XPathNodeIterator_t1383168931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[1] = 
{
	XPathNodeIterator_t1383168931::get_offset_of__count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (U3CGetEnumeratorU3Ec__Iterator2_t471241848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[3] = 
{
	U3CGetEnumeratorU3Ec__Iterator2_t471241848::get_offset_of_U24PC_0(),
	U3CGetEnumeratorU3Ec__Iterator2_t471241848::get_offset_of_U24current_1(),
	U3CGetEnumeratorU3Ec__Iterator2_t471241848::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (XPathNodeType_t3637370479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[11] = 
{
	XPathNodeType_t3637370479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (XPathResultType_t516720010)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1609[8] = 
{
	XPathResultType_t516720010::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (XmlDataType_t545259441)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1610[3] = 
{
	XmlDataType_t545259441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (XsltContext_t894076946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (ConformanceLevel_t233510445)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1615[4] = 
{
	ConformanceLevel_t233510445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (DTDAutomataFactory_t809616844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[3] = 
{
	DTDAutomataFactory_t809616844::get_offset_of_root_0(),
	DTDAutomataFactory_t809616844::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t809616844::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (DTDObjectModel_t3593115196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[19] = 
{
	DTDObjectModel_t3593115196::get_offset_of_factory_0(),
	DTDObjectModel_t3593115196::get_offset_of_elementDecls_1(),
	DTDObjectModel_t3593115196::get_offset_of_attListDecls_2(),
	DTDObjectModel_t3593115196::get_offset_of_peDecls_3(),
	DTDObjectModel_t3593115196::get_offset_of_entityDecls_4(),
	DTDObjectModel_t3593115196::get_offset_of_notationDecls_5(),
	DTDObjectModel_t3593115196::get_offset_of_validationErrors_6(),
	DTDObjectModel_t3593115196::get_offset_of_resolver_7(),
	DTDObjectModel_t3593115196::get_offset_of_nameTable_8(),
	DTDObjectModel_t3593115196::get_offset_of_externalResources_9(),
	DTDObjectModel_t3593115196::get_offset_of_baseURI_10(),
	DTDObjectModel_t3593115196::get_offset_of_name_11(),
	DTDObjectModel_t3593115196::get_offset_of_publicId_12(),
	DTDObjectModel_t3593115196::get_offset_of_systemId_13(),
	DTDObjectModel_t3593115196::get_offset_of_intSubset_14(),
	DTDObjectModel_t3593115196::get_offset_of_intSubsetHasPERef_15(),
	DTDObjectModel_t3593115196::get_offset_of_isStandalone_16(),
	DTDObjectModel_t3593115196::get_offset_of_lineNumber_17(),
	DTDObjectModel_t3593115196::get_offset_of_linePosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (DictionaryBase_t2849977869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (U3CU3Ec__Iterator3_t2174423114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[5] = 
{
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t2174423114::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (DTDCollectionBase_t1997767749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[1] = 
{
	DTDCollectionBase_t1997767749::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (DTDElementDeclarationCollection_t918003794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (DTDAttListDeclarationCollection_t2994484303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (DTDEntityDeclarationCollection_t3329514151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (DTDNotationDeclarationCollection_t87515240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (DTDContentModel_t647030886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1625[6] = 
{
	DTDContentModel_t647030886::get_offset_of_root_5(),
	DTDContentModel_t647030886::get_offset_of_ownerElementName_6(),
	DTDContentModel_t647030886::get_offset_of_elementName_7(),
	DTDContentModel_t647030886::get_offset_of_orderType_8(),
	DTDContentModel_t647030886::get_offset_of_childModels_9(),
	DTDContentModel_t647030886::get_offset_of_occurence_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (DTDContentModelCollection_t194646180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	DTDContentModelCollection_t194646180::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (DTDNode_t2039770680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[5] = 
{
	DTDNode_t2039770680::get_offset_of_root_0(),
	DTDNode_t2039770680::get_offset_of_isInternalSubset_1(),
	DTDNode_t2039770680::get_offset_of_baseURI_2(),
	DTDNode_t2039770680::get_offset_of_lineNumber_3(),
	DTDNode_t2039770680::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (DTDElementDeclaration_t41916820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[6] = 
{
	DTDElementDeclaration_t41916820::get_offset_of_root_5(),
	DTDElementDeclaration_t41916820::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t41916820::get_offset_of_name_7(),
	DTDElementDeclaration_t41916820::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t41916820::get_offset_of_isAny_9(),
	DTDElementDeclaration_t41916820::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (DTDAttributeDefinition_t3691706913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[4] = 
{
	DTDAttributeDefinition_t3691706913::get_offset_of_name_5(),
	DTDAttributeDefinition_t3691706913::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3691706913::get_offset_of_unresolvedDefault_7(),
	DTDAttributeDefinition_t3691706913::get_offset_of_resolvedDefaultValue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (DTDAttListDeclaration_t1204852177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[3] = 
{
	DTDAttListDeclaration_t1204852177::get_offset_of_name_5(),
	DTDAttListDeclaration_t1204852177::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t1204852177::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (DTDEntityBase_t2319669258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[10] = 
{
	DTDEntityBase_t2319669258::get_offset_of_name_5(),
	DTDEntityBase_t2319669258::get_offset_of_publicId_6(),
	DTDEntityBase_t2319669258::get_offset_of_systemId_7(),
	DTDEntityBase_t2319669258::get_offset_of_literalValue_8(),
	DTDEntityBase_t2319669258::get_offset_of_replacementText_9(),
	DTDEntityBase_t2319669258::get_offset_of_uriString_10(),
	DTDEntityBase_t2319669258::get_offset_of_absUri_11(),
	DTDEntityBase_t2319669258::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2319669258::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2319669258::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (DTDEntityDeclaration_t3913042473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1632[6] = 
{
	DTDEntityDeclaration_t3913042473::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t3913042473::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t3913042473::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t3913042473::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t3913042473::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t3913042473::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (DTDNotationDeclaration_t2992004394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[5] = 
{
	DTDNotationDeclaration_t2992004394::get_offset_of_name_5(),
	DTDNotationDeclaration_t2992004394::get_offset_of_localName_6(),
	DTDNotationDeclaration_t2992004394::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t2992004394::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t2992004394::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (DTDParameterEntityDeclarationCollection_t541155938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[2] = 
{
	DTDParameterEntityDeclarationCollection_t541155938::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t541155938::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (DTDParameterEntityDeclaration_t149008292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (DTDContentOrderType_t4011965541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1636[4] = 
{
	DTDContentOrderType_t4011965541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (DTDOccurence_t81554249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1637[5] = 
{
	DTDOccurence_t81554249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (DTDReader_t4151552713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[14] = 
{
	DTDReader_t4151552713::get_offset_of_currentInput_0(),
	DTDReader_t4151552713::get_offset_of_parserInputStack_1(),
	DTDReader_t4151552713::get_offset_of_nameBuffer_2(),
	DTDReader_t4151552713::get_offset_of_nameLength_3(),
	DTDReader_t4151552713::get_offset_of_nameCapacity_4(),
	DTDReader_t4151552713::get_offset_of_valueBuffer_5(),
	DTDReader_t4151552713::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t4151552713::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t4151552713::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t4151552713::get_offset_of_normalization_9(),
	DTDReader_t4151552713::get_offset_of_processingInternalSubset_10(),
	DTDReader_t4151552713::get_offset_of_cachedPublicId_11(),
	DTDReader_t4151552713::get_offset_of_cachedSystemId_12(),
	DTDReader_t4151552713::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (EntityHandling_t2742017190)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1639[3] = 
{
	EntityHandling_t2742017190::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (MonoFIXAttribute_t2749889992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[1] = 
{
	MonoFIXAttribute_t2749889992::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (NameTable_t2699772693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[3] = 
{
	NameTable_t2699772693::get_offset_of_count_0(),
	NameTable_t2699772693::get_offset_of_buckets_1(),
	NameTable_t2699772693::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (Entry_t2866414864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[4] = 
{
	Entry_t2866414864::get_offset_of_str_0(),
	Entry_t2866414864::get_offset_of_hash_1(),
	Entry_t2866414864::get_offset_of_len_2(),
	Entry_t2866414864::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (NamespaceHandling_t1516402802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1647[3] = 
{
	NamespaceHandling_t1516402802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (NewLineHandling_t53740107)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1648[4] = 
{
	NewLineHandling_t53740107::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (ReadState_t352099245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1649[6] = 
{
	ReadState_t352099245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (WhitespaceHandling_t2567612992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1650[4] = 
{
	WhitespaceHandling_t2567612992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (WriteState_t1937244592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1651[8] = 
{
	WriteState_t1937244592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (XmlAttribute_t6647939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[4] = 
{
	XmlAttribute_t6647939::get_offset_of_name_5(),
	XmlAttribute_t6647939::get_offset_of_isDefault_6(),
	XmlAttribute_t6647939::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t6647939::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (XmlAttributeCollection_t3012627841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[2] = 
{
	XmlAttributeCollection_t3012627841::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3012627841::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (XmlCDataSection_t68050113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (XmlChar_t856576415), -1, sizeof(XmlChar_t856576415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[5] = 
{
	XmlChar_t856576415_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t856576415_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t856576415_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t856576415_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t856576415_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (XmlCharacterData_t915341530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	XmlCharacterData_t915341530::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (XmlComment_t2893623302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (XmlConvert_t2894815066), -1, sizeof(XmlConvert_t2894815066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1658[7] = 
{
	XmlConvert_t2894815066_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t2894815066_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t2894815066_StaticFields::get_offset_of__defaultStyle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (XmlDeclaration_t1240444833), -1, sizeof(XmlDeclaration_t1240444833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[4] = 
{
	XmlDeclaration_t1240444833::get_offset_of_encoding_6(),
	XmlDeclaration_t1240444833::get_offset_of_standalone_7(),
	XmlDeclaration_t1240444833::get_offset_of_version_8(),
	XmlDeclaration_t1240444833_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (XmlDocument_t730752740), -1, sizeof(XmlDocument_t730752740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[21] = 
{
	XmlDocument_t730752740_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t730752740::get_offset_of_optimal_create_element_6(),
	XmlDocument_t730752740::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t730752740::get_offset_of_nameTable_8(),
	XmlDocument_t730752740::get_offset_of_baseURI_9(),
	XmlDocument_t730752740::get_offset_of_implementation_10(),
	XmlDocument_t730752740::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t730752740::get_offset_of_resolver_12(),
	XmlDocument_t730752740::get_offset_of_idTable_13(),
	XmlDocument_t730752740::get_offset_of_nameCache_14(),
	XmlDocument_t730752740::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t730752740::get_offset_of_nsNodeXml_16(),
	XmlDocument_t730752740::get_offset_of_schemas_17(),
	XmlDocument_t730752740::get_offset_of_schemaInfo_18(),
	XmlDocument_t730752740::get_offset_of_loadMode_19(),
	XmlDocument_t730752740::get_offset_of_NodeChanged_20(),
	XmlDocument_t730752740::get_offset_of_NodeChanging_21(),
	XmlDocument_t730752740::get_offset_of_NodeInserted_22(),
	XmlDocument_t730752740::get_offset_of_NodeInserting_23(),
	XmlDocument_t730752740::get_offset_of_NodeRemoved_24(),
	XmlDocument_t730752740::get_offset_of_NodeRemoving_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (XmlDocumentFragment_t1144967508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[1] = 
{
	XmlDocumentFragment_t1144967508::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (XmlDocumentNavigator_t1264434787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[3] = 
{
	XmlDocumentNavigator_t1264434787::get_offset_of_node_2(),
	XmlDocumentNavigator_t1264434787::get_offset_of_nsNode_3(),
	XmlDocumentNavigator_t1264434787::get_offset_of_iteratedNsNames_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (XmlDocumentType_t838630462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[3] = 
{
	XmlDocumentType_t838630462::get_offset_of_entities_6(),
	XmlDocumentType_t838630462::get_offset_of_notations_7(),
	XmlDocumentType_t838630462::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (XmlElement_t280387747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[5] = 
{
	XmlElement_t280387747::get_offset_of_attributes_6(),
	XmlElement_t280387747::get_offset_of_name_7(),
	XmlElement_t280387747::get_offset_of_lastLinkedChild_8(),
	XmlElement_t280387747::get_offset_of_isNotEmpty_9(),
	XmlElement_t280387747::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (XmlEntity_t3759900140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[7] = 
{
	XmlEntity_t3759900140::get_offset_of_name_5(),
	XmlEntity_t3759900140::get_offset_of_NDATA_6(),
	XmlEntity_t3759900140::get_offset_of_publicId_7(),
	XmlEntity_t3759900140::get_offset_of_systemId_8(),
	XmlEntity_t3759900140::get_offset_of_baseUri_9(),
	XmlEntity_t3759900140::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t3759900140::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (XmlEntityReference_t2288721231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[2] = 
{
	XmlEntityReference_t2288721231::get_offset_of_entityName_6(),
	XmlEntityReference_t2288721231::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (XmlException_t1475188278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[5] = 
{
	XmlException_t1475188278::get_offset_of_lineNumber_11(),
	XmlException_t1475188278::get_offset_of_linePosition_12(),
	XmlException_t1475188278::get_offset_of_sourceUri_13(),
	XmlException_t1475188278::get_offset_of_res_14(),
	XmlException_t1475188278::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (XmlImplementation_t3716119739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[1] = 
{
	XmlImplementation_t3716119739::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (XmlStreamReader_t837919148), -1, sizeof(XmlStreamReader_t837919148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1669[2] = 
{
	XmlStreamReader_t837919148::get_offset_of_input_12(),
	XmlStreamReader_t837919148_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (NonBlockingStreamReader_t2673413431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[11] = 
{
	NonBlockingStreamReader_t2673413431::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t2673413431::get_offset_of_pos_4(),
	NonBlockingStreamReader_t2673413431::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t2673413431::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t2673413431::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t2673413431::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t2673413431::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t2673413431::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t2673413431::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (XmlInputStream_t2962968081), -1, sizeof(XmlInputStream_t2962968081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1671[7] = 
{
	XmlInputStream_t2962968081_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2962968081::get_offset_of_enc_2(),
	XmlInputStream_t2962968081::get_offset_of_stream_3(),
	XmlInputStream_t2962968081::get_offset_of_buffer_4(),
	XmlInputStream_t2962968081::get_offset_of_bufLength_5(),
	XmlInputStream_t2962968081::get_offset_of_bufPos_6(),
	XmlInputStream_t2962968081_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (XmlIteratorNodeList_t867247383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	XmlIteratorNodeList_t867247383::get_offset_of_source_0(),
	XmlIteratorNodeList_t867247383::get_offset_of_iterator_1(),
	XmlIteratorNodeList_t867247383::get_offset_of_list_2(),
	XmlIteratorNodeList_t867247383::get_offset_of_finished_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (XPathNodeIteratorNodeListIterator_t968381307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[2] = 
{
	XPathNodeIteratorNodeListIterator_t968381307::get_offset_of_iter_0(),
	XPathNodeIteratorNodeListIterator_t968381307::get_offset_of_source_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (XmlLinkedNode_t901819716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[1] = 
{
	XmlLinkedNode_t901819716::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (XmlNameEntry_t1203257998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[5] = 
{
	XmlNameEntry_t1203257998::get_offset_of_Prefix_0(),
	XmlNameEntry_t1203257998::get_offset_of_LocalName_1(),
	XmlNameEntry_t1203257998::get_offset_of_NS_2(),
	XmlNameEntry_t1203257998::get_offset_of_Hash_3(),
	XmlNameEntry_t1203257998::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (XmlNameEntryCache_t3943949348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[4] = 
{
	XmlNameEntryCache_t3943949348::get_offset_of_table_0(),
	XmlNameEntryCache_t3943949348::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3943949348::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3943949348::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (XmlNameTable_t1216706026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XmlNamedNodeMap_t977482698), -1, sizeof(XmlNamedNodeMap_t977482698_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1678[4] = 
{
	XmlNamedNodeMap_t977482698_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t977482698::get_offset_of_parent_1(),
	XmlNamedNodeMap_t977482698::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t977482698::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XmlNamespaceManager_t1467853467), -1, sizeof(XmlNamespaceManager_t1467853467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1679[9] = 
{
	XmlNamespaceManager_t1467853467::get_offset_of_decls_0(),
	XmlNamespaceManager_t1467853467::get_offset_of_declPos_1(),
	XmlNamespaceManager_t1467853467::get_offset_of_scopes_2(),
	XmlNamespaceManager_t1467853467::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t1467853467::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t1467853467::get_offset_of_count_5(),
	XmlNamespaceManager_t1467853467::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t1467853467::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t1467853467_StaticFields::get_offset_of_U3CU3Ef__switchU24map28_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (NsDecl_t3658211563)+ sizeof (Il2CppObject), sizeof(NsDecl_t3658211563_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1680[2] = 
{
	NsDecl_t3658211563::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3658211563::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (NsScope_t1749213747)+ sizeof (Il2CppObject), sizeof(NsScope_t1749213747_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1681[2] = 
{
	NsScope_t1749213747::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t1749213747::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XmlNode_t856910923), -1, sizeof(XmlNode_t856910923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1682[5] = 
{
	XmlNode_t856910923_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t856910923::get_offset_of_ownerDocument_1(),
	XmlNode_t856910923::get_offset_of_parentNode_2(),
	XmlNode_t856910923::get_offset_of_childNodes_3(),
	XmlNode_t856910923_StaticFields::get_offset_of_U3CU3Ef__switchU24map44_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (EmptyNodeList_t1675009921), -1, sizeof(EmptyNodeList_t1675009921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1683[1] = 
{
	EmptyNodeList_t1675009921_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XmlNodeArrayList_t543782172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[1] = 
{
	XmlNodeArrayList_t543782172::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (XmlNodeChangedAction_t2010186255)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[4] = 
{
	XmlNodeChangedAction_t2010186255::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (XmlNodeChangedEventArgs_t1377283246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[6] = 
{
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t1377283246::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XmlNodeList_t991860617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XmlNodeListChildren_t3993110696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[1] = 
{
	XmlNodeListChildren_t3993110696::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (Enumerator_t3708952179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[3] = 
{
	Enumerator_t3708952179::get_offset_of_parent_0(),
	Enumerator_t3708952179::get_offset_of_currentChild_1(),
	Enumerator_t3708952179::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XmlNodeOrder_t444538963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1690[5] = 
{
	XmlNodeOrder_t444538963::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XmlNodeType_t992114213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1691[19] = 
{
	XmlNodeType_t992114213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XmlNotation_t1447424459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[4] = 
{
	XmlNotation_t1447424459::get_offset_of_localName_5(),
	XmlNotation_t1447424459::get_offset_of_publicId_6(),
	XmlNotation_t1447424459::get_offset_of_systemId_7(),
	XmlNotation_t1447424459::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XmlOutputMethod_t1995898507)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1693[5] = 
{
	XmlOutputMethod_t1995898507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XmlParserContext_t1291067127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[13] = 
{
	XmlParserContext_t1291067127::get_offset_of_baseURI_0(),
	XmlParserContext_t1291067127::get_offset_of_docTypeName_1(),
	XmlParserContext_t1291067127::get_offset_of_encoding_2(),
	XmlParserContext_t1291067127::get_offset_of_internalSubset_3(),
	XmlParserContext_t1291067127::get_offset_of_namespaceManager_4(),
	XmlParserContext_t1291067127::get_offset_of_nameTable_5(),
	XmlParserContext_t1291067127::get_offset_of_publicID_6(),
	XmlParserContext_t1291067127::get_offset_of_systemID_7(),
	XmlParserContext_t1291067127::get_offset_of_xmlLang_8(),
	XmlParserContext_t1291067127::get_offset_of_xmlSpace_9(),
	XmlParserContext_t1291067127::get_offset_of_contextItems_10(),
	XmlParserContext_t1291067127::get_offset_of_contextItemCount_11(),
	XmlParserContext_t1291067127::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (ContextItem_t3307232354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[3] = 
{
	ContextItem_t3307232354::get_offset_of_BaseURI_0(),
	ContextItem_t3307232354::get_offset_of_XmlLang_1(),
	ContextItem_t3307232354::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (XmlParserInput_t3438354706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[5] = 
{
	XmlParserInput_t3438354706::get_offset_of_sourceStack_0(),
	XmlParserInput_t3438354706::get_offset_of_source_1(),
	XmlParserInput_t3438354706::get_offset_of_has_peek_2(),
	XmlParserInput_t3438354706::get_offset_of_peek_char_3(),
	XmlParserInput_t3438354706::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XmlParserInputSource_t173147476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[6] = 
{
	XmlParserInputSource_t173147476::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t173147476::get_offset_of_reader_1(),
	XmlParserInputSource_t173147476::get_offset_of_state_2(),
	XmlParserInputSource_t173147476::get_offset_of_isPE_3(),
	XmlParserInputSource_t173147476::get_offset_of_line_4(),
	XmlParserInputSource_t173147476::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (XmlProcessingInstruction_t2161892066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[2] = 
{
	XmlProcessingInstruction_t2161892066::get_offset_of_target_6(),
	XmlProcessingInstruction_t2161892066::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XmlQualifiedName_t2133315502), -1, sizeof(XmlQualifiedName_t2133315502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[4] = 
{
	XmlQualifiedName_t2133315502_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t2133315502::get_offset_of_name_1(),
	XmlQualifiedName_t2133315502::get_offset_of_ns_2(),
	XmlQualifiedName_t2133315502::get_offset_of_hash_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
