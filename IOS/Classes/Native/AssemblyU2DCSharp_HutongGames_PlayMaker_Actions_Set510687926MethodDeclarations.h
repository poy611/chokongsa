﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMass2d
struct SetMass2d_t510687926;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMass2d::.ctor()
extern "C"  void SetMass2d__ctor_m4177278784 (SetMass2d_t510687926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::Reset()
extern "C"  void SetMass2d_Reset_m1823711725 (SetMass2d_t510687926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::OnEnter()
extern "C"  void SetMass2d_OnEnter_m2257185559 (SetMass2d_t510687926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::DoSetMass()
extern "C"  void SetMass2d_DoSetMass_m3846444681 (SetMass2d_t510687926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
