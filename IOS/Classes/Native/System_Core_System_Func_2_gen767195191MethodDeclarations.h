﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2301125574MethodDeclarations.h"

// System.Void System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativePlayer>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2147412842(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t767195191 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m161237399_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativePlayer>::Invoke(T)
#define Func_2_Invoke_m1839868748(__this, ___arg10, method) ((  NativePlayer_t2636885988 * (*) (Func_2_t767195191 *, UIntPtr_t , const MethodInfo*))Func_2_Invoke_m527793903_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativePlayer>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4177414399(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t767195191 *, UIntPtr_t , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3185104162_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.NativePlayer>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1165385736(__this, ___result0, method) ((  NativePlayer_t2636885988 * (*) (Func_2_t767195191 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m858964037_gshared)(__this, ___result0, method)
