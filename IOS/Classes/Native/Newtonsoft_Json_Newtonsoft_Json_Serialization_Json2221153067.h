﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Func`1<System.Object>
struct Func_1_t1001010649;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t2221153067  : public Il2CppObject
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::converterType
	Type_t * ___converterType_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::defaultConstructor
	Func_1_t1001010649 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_converterType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2221153067, ___converterType_0)); }
	inline Type_t * get_converterType_0() const { return ___converterType_0; }
	inline Type_t ** get_address_of_converterType_0() { return &___converterType_0; }
	inline void set_converterType_0(Type_t * value)
	{
		___converterType_0 = value;
		Il2CppCodeGenWriteBarrier(&___converterType_0, value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2221153067, ___defaultConstructor_1)); }
	inline Func_1_t1001010649 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t1001010649 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t1001010649 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier(&___defaultConstructor_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
