﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t922401854;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3557407943.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef1780819301.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::.ctor(System.IntPtr)
extern "C"  void PlayerSelectUIResponse__ctor_m85247134 (PlayerSelectUIResponse_t922401854 * __this, IntPtr_t ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PlayerSelectUIResponse_System_Collections_IEnumerable_GetEnumerator_m277293355 (PlayerSelectUIResponse_t922401854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::Status()
extern "C"  int32_t PlayerSelectUIResponse_Status_m4163169002 (PlayerSelectUIResponse_t922401854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::PlayerIdAtIndex(System.UIntPtr)
extern "C"  String_t* PlayerSelectUIResponse_PlayerIdAtIndex_m4261867621 (PlayerSelectUIResponse_t922401854 * __this, UIntPtr_t  ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::GetEnumerator()
extern "C"  Il2CppObject* PlayerSelectUIResponse_GetEnumerator_m4164219974 (PlayerSelectUIResponse_t922401854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MinimumAutomatchingPlayers()
extern "C"  uint32_t PlayerSelectUIResponse_MinimumAutomatchingPlayers_m1771794379 (PlayerSelectUIResponse_t922401854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::MaximumAutomatchingPlayers()
extern "C"  uint32_t PlayerSelectUIResponse_MaximumAutomatchingPlayers_m2979014905 (PlayerSelectUIResponse_t922401854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C"  void PlayerSelectUIResponse_CallDispose_m1041011922 (PlayerSelectUIResponse_t922401854 * __this, HandleRef_t1780819301  ___selfPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse::FromPointer(System.IntPtr)
extern "C"  PlayerSelectUIResponse_t922401854 * PlayerSelectUIResponse_FromPointer_m2059843535 (Il2CppObject * __this /* static, unused */, IntPtr_t ___pointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
