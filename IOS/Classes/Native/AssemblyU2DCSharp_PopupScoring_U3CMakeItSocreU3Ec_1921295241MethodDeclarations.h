﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PopupScoring/<MakeItSocre>c__Iterator16
struct U3CMakeItSocreU3Ec__Iterator16_t1921295241;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PopupScoring/<MakeItSocre>c__Iterator16::.ctor()
extern "C"  void U3CMakeItSocreU3Ec__Iterator16__ctor_m3387151874 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopupScoring/<MakeItSocre>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeItSocreU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2728935312 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PopupScoring/<MakeItSocre>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeItSocreU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m686514980 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PopupScoring/<MakeItSocre>c__Iterator16::MoveNext()
extern "C"  bool U3CMakeItSocreU3Ec__Iterator16_MoveNext_m569762994 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScoring/<MakeItSocre>c__Iterator16::Dispose()
extern "C"  void U3CMakeItSocreU3Ec__Iterator16_Dispose_m3660990911 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PopupScoring/<MakeItSocre>c__Iterator16::Reset()
extern "C"  void U3CMakeItSocreU3Ec__Iterator16_Reset_m1033584815 (U3CMakeItSocreU3Ec__Iterator16_t1921295241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
