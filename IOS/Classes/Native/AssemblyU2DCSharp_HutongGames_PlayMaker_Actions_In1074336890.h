﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType930981288.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntInterpolate
struct  IntInterpolate_t1074336890  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.IntInterpolate::mode
	int32_t ___mode_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntInterpolate::fromInt
	FsmInt_t1596138449 * ___fromInt_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntInterpolate::toInt
	FsmInt_t1596138449 * ___toInt_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.IntInterpolate::time
	FsmFloat_t2134102846 * ___time_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntInterpolate::storeResult
	FsmInt_t1596138449 * ___storeResult_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntInterpolate::finishEvent
	FsmEvent_t2133468028 * ___finishEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.IntInterpolate::realTime
	bool ___realTime_17;
	// System.Single HutongGames.PlayMaker.Actions.IntInterpolate::startTime
	float ___startTime_18;
	// System.Single HutongGames.PlayMaker.Actions.IntInterpolate::currentTime
	float ___currentTime_19;

public:
	inline static int32_t get_offset_of_mode_11() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___mode_11)); }
	inline int32_t get_mode_11() const { return ___mode_11; }
	inline int32_t* get_address_of_mode_11() { return &___mode_11; }
	inline void set_mode_11(int32_t value)
	{
		___mode_11 = value;
	}

	inline static int32_t get_offset_of_fromInt_12() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___fromInt_12)); }
	inline FsmInt_t1596138449 * get_fromInt_12() const { return ___fromInt_12; }
	inline FsmInt_t1596138449 ** get_address_of_fromInt_12() { return &___fromInt_12; }
	inline void set_fromInt_12(FsmInt_t1596138449 * value)
	{
		___fromInt_12 = value;
		Il2CppCodeGenWriteBarrier(&___fromInt_12, value);
	}

	inline static int32_t get_offset_of_toInt_13() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___toInt_13)); }
	inline FsmInt_t1596138449 * get_toInt_13() const { return ___toInt_13; }
	inline FsmInt_t1596138449 ** get_address_of_toInt_13() { return &___toInt_13; }
	inline void set_toInt_13(FsmInt_t1596138449 * value)
	{
		___toInt_13 = value;
		Il2CppCodeGenWriteBarrier(&___toInt_13, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___time_14)); }
	inline FsmFloat_t2134102846 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2134102846 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2134102846 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier(&___time_14, value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___storeResult_15)); }
	inline FsmInt_t1596138449 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmInt_t1596138449 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmInt_t1596138449 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_15, value);
	}

	inline static int32_t get_offset_of_finishEvent_16() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___finishEvent_16)); }
	inline FsmEvent_t2133468028 * get_finishEvent_16() const { return ___finishEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_finishEvent_16() { return &___finishEvent_16; }
	inline void set_finishEvent_16(FsmEvent_t2133468028 * value)
	{
		___finishEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_16, value);
	}

	inline static int32_t get_offset_of_realTime_17() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___realTime_17)); }
	inline bool get_realTime_17() const { return ___realTime_17; }
	inline bool* get_address_of_realTime_17() { return &___realTime_17; }
	inline void set_realTime_17(bool value)
	{
		___realTime_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_currentTime_19() { return static_cast<int32_t>(offsetof(IntInterpolate_t1074336890, ___currentTime_19)); }
	inline float get_currentTime_19() const { return ___currentTime_19; }
	inline float* get_address_of_currentTime_19() { return &___currentTime_19; }
	inline void set_currentTime_19(float value)
	{
		___currentTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
