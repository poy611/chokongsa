﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties
struct  NetworkGetConnectedPlayerProperties_t4024683587  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::index
	FsmInt_t1596138449 * ___index_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::IpAddress
	FsmString_t952858651 * ___IpAddress_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::port
	FsmInt_t1596138449 * ___port_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::guid
	FsmString_t952858651 * ___guid_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::externalIPAddress
	FsmString_t952858651 * ___externalIPAddress_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::externalPort
	FsmInt_t1596138449 * ___externalPort_16;

public:
	inline static int32_t get_offset_of_index_11() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___index_11)); }
	inline FsmInt_t1596138449 * get_index_11() const { return ___index_11; }
	inline FsmInt_t1596138449 ** get_address_of_index_11() { return &___index_11; }
	inline void set_index_11(FsmInt_t1596138449 * value)
	{
		___index_11 = value;
		Il2CppCodeGenWriteBarrier(&___index_11, value);
	}

	inline static int32_t get_offset_of_IpAddress_12() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___IpAddress_12)); }
	inline FsmString_t952858651 * get_IpAddress_12() const { return ___IpAddress_12; }
	inline FsmString_t952858651 ** get_address_of_IpAddress_12() { return &___IpAddress_12; }
	inline void set_IpAddress_12(FsmString_t952858651 * value)
	{
		___IpAddress_12 = value;
		Il2CppCodeGenWriteBarrier(&___IpAddress_12, value);
	}

	inline static int32_t get_offset_of_port_13() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___port_13)); }
	inline FsmInt_t1596138449 * get_port_13() const { return ___port_13; }
	inline FsmInt_t1596138449 ** get_address_of_port_13() { return &___port_13; }
	inline void set_port_13(FsmInt_t1596138449 * value)
	{
		___port_13 = value;
		Il2CppCodeGenWriteBarrier(&___port_13, value);
	}

	inline static int32_t get_offset_of_guid_14() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___guid_14)); }
	inline FsmString_t952858651 * get_guid_14() const { return ___guid_14; }
	inline FsmString_t952858651 ** get_address_of_guid_14() { return &___guid_14; }
	inline void set_guid_14(FsmString_t952858651 * value)
	{
		___guid_14 = value;
		Il2CppCodeGenWriteBarrier(&___guid_14, value);
	}

	inline static int32_t get_offset_of_externalIPAddress_15() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___externalIPAddress_15)); }
	inline FsmString_t952858651 * get_externalIPAddress_15() const { return ___externalIPAddress_15; }
	inline FsmString_t952858651 ** get_address_of_externalIPAddress_15() { return &___externalIPAddress_15; }
	inline void set_externalIPAddress_15(FsmString_t952858651 * value)
	{
		___externalIPAddress_15 = value;
		Il2CppCodeGenWriteBarrier(&___externalIPAddress_15, value);
	}

	inline static int32_t get_offset_of_externalPort_16() { return static_cast<int32_t>(offsetof(NetworkGetConnectedPlayerProperties_t4024683587, ___externalPort_16)); }
	inline FsmInt_t1596138449 * get_externalPort_16() const { return ___externalPort_16; }
	inline FsmInt_t1596138449 ** get_address_of_externalPort_16() { return &___externalPort_16; }
	inline void set_externalPort_16(FsmInt_t1596138449 * value)
	{
		___externalPort_16 = value;
		Il2CppCodeGenWriteBarrier(&___externalPort_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
