﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.TextWriter
struct TextWriter_t2304124208;
// JsonFx.Json.JsonWriterSettings
struct JsonWriterSettings_t323204516;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonFx.Json.JsonWriter
struct  JsonWriter_t3589747297  : public Il2CppObject
{
public:
	// System.IO.TextWriter JsonFx.Json.JsonWriter::Writer
	TextWriter_t2304124208 * ___Writer_0;
	// JsonFx.Json.JsonWriterSettings JsonFx.Json.JsonWriter::settings
	JsonWriterSettings_t323204516 * ___settings_1;
	// System.Int32 JsonFx.Json.JsonWriter::depth
	int32_t ___depth_2;

public:
	inline static int32_t get_offset_of_Writer_0() { return static_cast<int32_t>(offsetof(JsonWriter_t3589747297, ___Writer_0)); }
	inline TextWriter_t2304124208 * get_Writer_0() const { return ___Writer_0; }
	inline TextWriter_t2304124208 ** get_address_of_Writer_0() { return &___Writer_0; }
	inline void set_Writer_0(TextWriter_t2304124208 * value)
	{
		___Writer_0 = value;
		Il2CppCodeGenWriteBarrier(&___Writer_0, value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(JsonWriter_t3589747297, ___settings_1)); }
	inline JsonWriterSettings_t323204516 * get_settings_1() const { return ___settings_1; }
	inline JsonWriterSettings_t323204516 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(JsonWriterSettings_t323204516 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier(&___settings_1, value);
	}

	inline static int32_t get_offset_of_depth_2() { return static_cast<int32_t>(offsetof(JsonWriter_t3589747297, ___depth_2)); }
	inline int32_t get_depth_2() const { return ___depth_2; }
	inline int32_t* get_address_of_depth_2() { return &___depth_2; }
	inline void set_depth_2(int32_t value)
	{
		___depth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
