﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2304636665MethodDeclarations.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3586507930(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t426447878 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2523847628_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>::Invoke(T)
#define Func_2_Invoke_m2652124792(__this, ___arg10, method) ((  ReadResponse_t2292627584 * (*) (Func_2_t426447878 *, IntPtr_t, const MethodInfo*))Func_2_Invoke_m1794082454_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1826193959(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t426447878 *, IntPtr_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2959052997_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m961366108(__this, ___result0, method) ((  ReadResponse_t2292627584 * (*) (Func_2_t426447878 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m558880510_gshared)(__this, ___result0, method)
