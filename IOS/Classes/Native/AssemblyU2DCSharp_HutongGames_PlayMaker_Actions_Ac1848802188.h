﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ActivateGameObject
struct  ActivateGameObject_t1848802188  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ActivateGameObject::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::activate
	FsmBool_t1075959796 * ___activate_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::recursive
	FsmBool_t1075959796 * ___recursive_13;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::resetOnExit
	bool ___resetOnExit_14;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ActivateGameObject::activatedGameObject
	GameObject_t3674682005 * ___activatedGameObject_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_activate_12() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___activate_12)); }
	inline FsmBool_t1075959796 * get_activate_12() const { return ___activate_12; }
	inline FsmBool_t1075959796 ** get_address_of_activate_12() { return &___activate_12; }
	inline void set_activate_12(FsmBool_t1075959796 * value)
	{
		___activate_12 = value;
		Il2CppCodeGenWriteBarrier(&___activate_12, value);
	}

	inline static int32_t get_offset_of_recursive_13() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___recursive_13)); }
	inline FsmBool_t1075959796 * get_recursive_13() const { return ___recursive_13; }
	inline FsmBool_t1075959796 ** get_address_of_recursive_13() { return &___recursive_13; }
	inline void set_recursive_13(FsmBool_t1075959796 * value)
	{
		___recursive_13 = value;
		Il2CppCodeGenWriteBarrier(&___recursive_13, value);
	}

	inline static int32_t get_offset_of_resetOnExit_14() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___resetOnExit_14)); }
	inline bool get_resetOnExit_14() const { return ___resetOnExit_14; }
	inline bool* get_address_of_resetOnExit_14() { return &___resetOnExit_14; }
	inline void set_resetOnExit_14(bool value)
	{
		___resetOnExit_14 = value;
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_activatedGameObject_16() { return static_cast<int32_t>(offsetof(ActivateGameObject_t1848802188, ___activatedGameObject_16)); }
	inline GameObject_t3674682005 * get_activatedGameObject_16() const { return ___activatedGameObject_16; }
	inline GameObject_t3674682005 ** get_address_of_activatedGameObject_16() { return &___activatedGameObject_16; }
	inline void set_activatedGameObject_16(GameObject_t3674682005 * value)
	{
		___activatedGameObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___activatedGameObject_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
