﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Sleep2d
struct Sleep2d_t3985951639;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Sleep2d::.ctor()
extern "C"  void Sleep2d__ctor_m2804084095 (Sleep2d_t3985951639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::Reset()
extern "C"  void Sleep2d_Reset_m450517036 (Sleep2d_t3985951639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::OnEnter()
extern "C"  void Sleep2d_OnEnter_m1172049302 (Sleep2d_t3985951639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::DoSleep()
extern "C"  void Sleep2d_DoSleep_m791058857 (Sleep2d_t3985951639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
