﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEventByName
struct  SendEventByName_t1556491042  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.SendEventByName::eventTarget
	FsmEventTarget_t1823904941 * ___eventTarget_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventByName::sendEvent
	FsmString_t952858651 * ___sendEvent_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEventByName::delay
	FsmFloat_t2134102846 * ___delay_13;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEventByName::everyFrame
	bool ___everyFrame_14;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEventByName::delayedEvent
	DelayedEvent_t1938906778 * ___delayedEvent_15;

public:
	inline static int32_t get_offset_of_eventTarget_11() { return static_cast<int32_t>(offsetof(SendEventByName_t1556491042, ___eventTarget_11)); }
	inline FsmEventTarget_t1823904941 * get_eventTarget_11() const { return ___eventTarget_11; }
	inline FsmEventTarget_t1823904941 ** get_address_of_eventTarget_11() { return &___eventTarget_11; }
	inline void set_eventTarget_11(FsmEventTarget_t1823904941 * value)
	{
		___eventTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_11, value);
	}

	inline static int32_t get_offset_of_sendEvent_12() { return static_cast<int32_t>(offsetof(SendEventByName_t1556491042, ___sendEvent_12)); }
	inline FsmString_t952858651 * get_sendEvent_12() const { return ___sendEvent_12; }
	inline FsmString_t952858651 ** get_address_of_sendEvent_12() { return &___sendEvent_12; }
	inline void set_sendEvent_12(FsmString_t952858651 * value)
	{
		___sendEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_12, value);
	}

	inline static int32_t get_offset_of_delay_13() { return static_cast<int32_t>(offsetof(SendEventByName_t1556491042, ___delay_13)); }
	inline FsmFloat_t2134102846 * get_delay_13() const { return ___delay_13; }
	inline FsmFloat_t2134102846 ** get_address_of_delay_13() { return &___delay_13; }
	inline void set_delay_13(FsmFloat_t2134102846 * value)
	{
		___delay_13 = value;
		Il2CppCodeGenWriteBarrier(&___delay_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(SendEventByName_t1556491042, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_delayedEvent_15() { return static_cast<int32_t>(offsetof(SendEventByName_t1556491042, ___delayedEvent_15)); }
	inline DelayedEvent_t1938906778 * get_delayedEvent_15() const { return ___delayedEvent_15; }
	inline DelayedEvent_t1938906778 ** get_address_of_delayedEvent_15() { return &___delayedEvent_15; }
	inline void set_delayedEvent_15(DelayedEvent_t1938906778 * value)
	{
		___delayedEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___delayedEvent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
