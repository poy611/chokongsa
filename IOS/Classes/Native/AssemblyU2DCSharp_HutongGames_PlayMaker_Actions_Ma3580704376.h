﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerRegisterHost
struct  MasterServerRegisterHost_t3580704376  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::gameTypeName
	FsmString_t952858651 * ___gameTypeName_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::gameName
	FsmString_t952858651 * ___gameName_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerRegisterHost::comment
	FsmString_t952858651 * ___comment_13;

public:
	inline static int32_t get_offset_of_gameTypeName_11() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___gameTypeName_11)); }
	inline FsmString_t952858651 * get_gameTypeName_11() const { return ___gameTypeName_11; }
	inline FsmString_t952858651 ** get_address_of_gameTypeName_11() { return &___gameTypeName_11; }
	inline void set_gameTypeName_11(FsmString_t952858651 * value)
	{
		___gameTypeName_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameTypeName_11, value);
	}

	inline static int32_t get_offset_of_gameName_12() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___gameName_12)); }
	inline FsmString_t952858651 * get_gameName_12() const { return ___gameName_12; }
	inline FsmString_t952858651 ** get_address_of_gameName_12() { return &___gameName_12; }
	inline void set_gameName_12(FsmString_t952858651 * value)
	{
		___gameName_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameName_12, value);
	}

	inline static int32_t get_offset_of_comment_13() { return static_cast<int32_t>(offsetof(MasterServerRegisterHost_t3580704376, ___comment_13)); }
	inline FsmString_t952858651 * get_comment_13() const { return ___comment_13; }
	inline FsmString_t952858651 ** get_address_of_comment_13() { return &___comment_13; }
	inline void set_comment_13(FsmString_t952858651 * value)
	{
		___comment_13 = value;
		Il2CppCodeGenWriteBarrier(&___comment_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
