﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet
struct SetAnimatorStabilizeFeet_t1599494748;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::.ctor()
extern "C"  void SetAnimatorStabilizeFeet__ctor_m4208121738 (SetAnimatorStabilizeFeet_t1599494748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::Reset()
extern "C"  void SetAnimatorStabilizeFeet_Reset_m1854554679 (SetAnimatorStabilizeFeet_t1599494748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::OnEnter()
extern "C"  void SetAnimatorStabilizeFeet_OnEnter_m1832493281 (SetAnimatorStabilizeFeet_t1599494748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::DoStabilizeFeet()
extern "C"  void SetAnimatorStabilizeFeet_DoStabilizeFeet_m2756905786 (SetAnimatorStabilizeFeet_t1599494748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
