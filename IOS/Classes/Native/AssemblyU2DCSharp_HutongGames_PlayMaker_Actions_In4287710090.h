﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In3286228119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntCompare2
struct  IntCompare2_t4287710090  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare2::integer1
	FsmInt_t1596138449 * ___integer1_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare2::integer2
	FsmInt_t1596138449 * ___integer2_12;
	// HutongGames.PlayMaker.Actions.IntCompare2/Operation HutongGames.PlayMaker.Actions.IntCompare2::operation
	int32_t ___operation_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IntCompare2::result
	FsmBool_t1075959796 * ___result_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare2::comparisonPassEvent
	FsmEvent_t2133468028 * ___comparisonPassEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare2::comparisonFailEvent
	FsmEvent_t2133468028 * ___comparisonFailEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.IntCompare2::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_integer1_11() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___integer1_11)); }
	inline FsmInt_t1596138449 * get_integer1_11() const { return ___integer1_11; }
	inline FsmInt_t1596138449 ** get_address_of_integer1_11() { return &___integer1_11; }
	inline void set_integer1_11(FsmInt_t1596138449 * value)
	{
		___integer1_11 = value;
		Il2CppCodeGenWriteBarrier(&___integer1_11, value);
	}

	inline static int32_t get_offset_of_integer2_12() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___integer2_12)); }
	inline FsmInt_t1596138449 * get_integer2_12() const { return ___integer2_12; }
	inline FsmInt_t1596138449 ** get_address_of_integer2_12() { return &___integer2_12; }
	inline void set_integer2_12(FsmInt_t1596138449 * value)
	{
		___integer2_12 = value;
		Il2CppCodeGenWriteBarrier(&___integer2_12, value);
	}

	inline static int32_t get_offset_of_operation_13() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___operation_13)); }
	inline int32_t get_operation_13() const { return ___operation_13; }
	inline int32_t* get_address_of_operation_13() { return &___operation_13; }
	inline void set_operation_13(int32_t value)
	{
		___operation_13 = value;
	}

	inline static int32_t get_offset_of_result_14() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___result_14)); }
	inline FsmBool_t1075959796 * get_result_14() const { return ___result_14; }
	inline FsmBool_t1075959796 ** get_address_of_result_14() { return &___result_14; }
	inline void set_result_14(FsmBool_t1075959796 * value)
	{
		___result_14 = value;
		Il2CppCodeGenWriteBarrier(&___result_14, value);
	}

	inline static int32_t get_offset_of_comparisonPassEvent_15() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___comparisonPassEvent_15)); }
	inline FsmEvent_t2133468028 * get_comparisonPassEvent_15() const { return ___comparisonPassEvent_15; }
	inline FsmEvent_t2133468028 ** get_address_of_comparisonPassEvent_15() { return &___comparisonPassEvent_15; }
	inline void set_comparisonPassEvent_15(FsmEvent_t2133468028 * value)
	{
		___comparisonPassEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___comparisonPassEvent_15, value);
	}

	inline static int32_t get_offset_of_comparisonFailEvent_16() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___comparisonFailEvent_16)); }
	inline FsmEvent_t2133468028 * get_comparisonFailEvent_16() const { return ___comparisonFailEvent_16; }
	inline FsmEvent_t2133468028 ** get_address_of_comparisonFailEvent_16() { return &___comparisonFailEvent_16; }
	inline void set_comparisonFailEvent_16(FsmEvent_t2133468028 * value)
	{
		___comparisonFailEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___comparisonFailEvent_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(IntCompare2_t4287710090, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
