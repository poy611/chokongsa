﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct ShimEnumerator_t4213034705;
// System.Collections.Generic.Dictionary`2<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t202289382;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m421453670_gshared (ShimEnumerator_t4213034705 * __this, Dictionary_2_t202289382 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m421453670(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4213034705 *, Dictionary_2_t202289382 *, const MethodInfo*))ShimEnumerator__ctor_m421453670_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m500446587_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m500446587(__this, method) ((  bool (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_MoveNext_m500446587_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2663309305_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2663309305(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_get_Entry_m2663309305_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m558292884_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m558292884(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_get_Key_m558292884_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m696985894_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m696985894(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_get_Value_m696985894_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1213236782_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1213236782(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_get_Current_m1213236782_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::Reset()
extern "C"  void ShimEnumerator_Reset_m4000825528_gshared (ShimEnumerator_t4213034705 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4000825528(__this, method) ((  void (*) (ShimEnumerator_t4213034705 *, const MethodInfo*))ShimEnumerator_Reset_m4000825528_gshared)(__this, method)
