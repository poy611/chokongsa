﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;

#include "codegen/il2cpp-codegen.h"

// System.Boolean System.Runtime.Serialization.DataContractAttribute::get_IsReference()
extern "C"  bool DataContractAttribute_get_IsReference_m1562831651 (DataContractAttribute_t2462274566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
