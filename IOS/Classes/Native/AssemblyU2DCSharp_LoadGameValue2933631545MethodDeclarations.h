﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadGameValue
struct LoadGameValue_t2933631545;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadGameValue::.ctor()
extern "C"  void LoadGameValue__ctor_m3887605586 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::Start()
extern "C"  void LoadGameValue_Start_m2834743378 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::Update()
extern "C"  void LoadGameValue_Update_m1983550971 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::PlusCoffee()
extern "C"  void LoadGameValue_PlusCoffee_m3533348728 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::MinusCoffee()
extern "C"  void LoadGameValue_MinusCoffee_m1119188620 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::SetNeed()
extern "C"  void LoadGameValue_SetNeed_m3741810376 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadGameValue::SetValue()
extern "C"  void LoadGameValue_SetValue_m2729579937 (LoadGameValue_t2933631545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
