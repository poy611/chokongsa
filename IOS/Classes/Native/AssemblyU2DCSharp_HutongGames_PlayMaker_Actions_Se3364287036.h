﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba2226508741.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmArrayItem
struct  SetFsmArrayItem_t3364287036  : public BaseFsmVariableIndexAction_t2226508741
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmArrayItem::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArrayItem::fsmName
	FsmString_t952858651 * ___fsmName_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArrayItem::variableName
	FsmString_t952858651 * ___variableName_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetFsmArrayItem::index
	FsmInt_t1596138449 * ___index_20;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.SetFsmArrayItem::value
	FsmVar_t1596150537 * ___value_21;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmArrayItem::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___gameObject_17)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_17, value);
	}

	inline static int32_t get_offset_of_fsmName_18() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___fsmName_18)); }
	inline FsmString_t952858651 * get_fsmName_18() const { return ___fsmName_18; }
	inline FsmString_t952858651 ** get_address_of_fsmName_18() { return &___fsmName_18; }
	inline void set_fsmName_18(FsmString_t952858651 * value)
	{
		___fsmName_18 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_18, value);
	}

	inline static int32_t get_offset_of_variableName_19() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___variableName_19)); }
	inline FsmString_t952858651 * get_variableName_19() const { return ___variableName_19; }
	inline FsmString_t952858651 ** get_address_of_variableName_19() { return &___variableName_19; }
	inline void set_variableName_19(FsmString_t952858651 * value)
	{
		___variableName_19 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_19, value);
	}

	inline static int32_t get_offset_of_index_20() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___index_20)); }
	inline FsmInt_t1596138449 * get_index_20() const { return ___index_20; }
	inline FsmInt_t1596138449 ** get_address_of_index_20() { return &___index_20; }
	inline void set_index_20(FsmInt_t1596138449 * value)
	{
		___index_20 = value;
		Il2CppCodeGenWriteBarrier(&___index_20, value);
	}

	inline static int32_t get_offset_of_value_21() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___value_21)); }
	inline FsmVar_t1596150537 * get_value_21() const { return ___value_21; }
	inline FsmVar_t1596150537 ** get_address_of_value_21() { return &___value_21; }
	inline void set_value_21(FsmVar_t1596150537 * value)
	{
		___value_21 = value;
		Il2CppCodeGenWriteBarrier(&___value_21, value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t3364287036, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
