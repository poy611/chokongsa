﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ApplicationRunInBackground
struct ApplicationRunInBackground_t3684091478;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::.ctor()
extern "C"  void ApplicationRunInBackground__ctor_m4008746704 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::Reset()
extern "C"  void ApplicationRunInBackground_Reset_m1655179645 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationRunInBackground::OnEnter()
extern "C"  void ApplicationRunInBackground_OnEnter_m3506613927 (ApplicationRunInBackground_t3684091478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
