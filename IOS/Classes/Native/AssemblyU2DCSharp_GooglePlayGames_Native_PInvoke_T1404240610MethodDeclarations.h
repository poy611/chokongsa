﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8
struct U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_UIntPtr3365854250.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8::.ctor()
extern "C"  void U3CPlayerIdAtIndexU3Ec__AnonStoreyA8__ctor_m1794160121 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStoreyA8::<>m__D8(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_U3CU3Em__D8_m12626581 (U3CPlayerIdAtIndexU3Ec__AnonStoreyA8_t1404240610 * __this, StringBuilder_t243639308 * ___out_string0, UIntPtr_t  ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
