﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LearnPage
struct LearnPage_t3558900435;

#include "codegen/il2cpp-codegen.h"

// System.Void LearnPage::.ctor()
extern "C"  void LearnPage__ctor_m4042592248 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::Start()
extern "C"  void LearnPage_Start_m2989730040 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::GoLearnMakeCoffee()
extern "C"  void LearnPage_GoLearnMakeCoffee_m2353181388 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::GoLearnCoffeeTool()
extern "C"  void LearnPage_GoLearnCoffeeTool_m3479117942 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::GoLearnCoffeePenut()
extern "C"  void LearnPage_GoLearnCoffeePenut_m936552508 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::GoLearnCalculation()
extern "C"  void LearnPage_GoLearnCalculation_m614727353 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LearnPage::BackButtonClick()
extern "C"  void LearnPage_BackButtonClick_m485069093 (LearnPage_t3558900435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
