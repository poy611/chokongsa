﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// PlayMakerFSM
struct PlayMakerFSM_t3799847376;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3211770239;
// HutongGames.PlayMaker.INameable
struct INameable_t2730839192;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t1024128046;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1596138449;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t3771611999;
// HutongGames.PlayMaker.ActionReport
struct ActionReport_t662142796;
// HutongGames.PlayMaker.FsmVarOverride
struct FsmVarOverride_t3235106805;
// HutongGames.PlayMaker.Fsm
struct Fsm_t1527112426;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t2685995989;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t3279845016;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t2786508133;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t1823904941;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t3927159007;
// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t964995201;
// HutongGames.PlayMaker.FsmString
struct FsmString_t952858651;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t821476169;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t1596150537;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t2129666875;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t1076048395;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t533912881;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t533912882;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t2131419205;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t1076426478;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t3871136040;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2366529033;
// HutongGames.PlayMaker.IFsmStateAction
struct IFsmStateAction_t3269097786;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1938906778;
// HutongGames.PlayMaker.FsmState
struct FsmState_t2146334067;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;
// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t2614866584;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t1596141350;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t924399665;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3073272573;

#include "mscorlib_System_Array1146569071.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions1271214244.h"
#include "PlayMaker_PlayMakerFSM3799847376.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat2134102846.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3211770239.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1596138449.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition3771611999.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport662142796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride3235106805.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm1527112426.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject1697147867.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault251897112.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve2685995989.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall3279845016.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl2786508133.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget1823904941.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty3927159007.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption964995201.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString952858651.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject821476169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar1596150537.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray2129666875.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum1076048395.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool1075959796.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector2533912881.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector3533912882.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor2131419205.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect1076426478.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion3871136040.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2672665179.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1938906778.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState2146334067.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent2133468028.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry2614866584.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog1596141350.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial924399665.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3073272573.h"

#pragma once
// HutongGames.Extensions.TextureExtensions/Point[]
struct PointU5BU5D_t178283597  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Point_t1271214244  m_Items[1];

public:
	inline Point_t1271214244  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Point_t1271214244 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Point_t1271214244  value)
	{
		m_Items[index] = value;
	}
};
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t191094001  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerFSM_t3799847376 * m_Items[1];

public:
	inline PlayMakerFSM_t3799847376 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayMakerFSM_t3799847376 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayMakerFSM_t3799847376 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t2945380875  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmFloat_t2134102846 * m_Items[1];

public:
	inline FsmFloat_t2134102846 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmFloat_t2134102846 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmFloat_t2134102846 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t2180779430  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NamedVariable_t3211770239 * m_Items[1];

public:
	inline NamedVariable_t3211770239 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NamedVariable_t3211770239 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NamedVariable_t3211770239 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.INameable[]
struct INameableU5BU5D_t2242950665  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.INamedVariable[]
struct INamedVariableU5BU5D_t2748317531  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t1976821196  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmInt_t1596138449 * m_Items[1];

public:
	inline FsmInt_t1596138449 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmInt_t1596138449 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmInt_t1596138449 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t818210886  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTransition_t3771611999 * m_Items[1];

public:
	inline FsmTransition_t3771611999 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTransition_t3771611999 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTransition_t3771611999 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.ActionReport[]
struct ActionReportU5BU5D_t33553925  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ActionReport_t662142796 * m_Items[1];

public:
	inline ActionReport_t662142796 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ActionReport_t662142796 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ActionReport_t662142796 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVarOverride[]
struct FsmVarOverrideU5BU5D_t3759009944  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVarOverride_t3235106805 * m_Items[1];

public:
	inline FsmVarOverride_t3235106805 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVarOverride_t3235106805 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVarOverride_t3235106805 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.Fsm[]
struct FsmU5BU5D_t3357880879  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Fsm_t1527112426 * m_Items[1];

public:
	inline Fsm_t1527112426 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Fsm_t1527112426 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Fsm_t1527112426 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t1706220122  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmGameObject_t1697147867 * m_Items[1];

public:
	inline FsmGameObject_t1697147867 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmGameObject_t1697147867 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmGameObject_t1697147867 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmOwnerDefault[]
struct FsmOwnerDefaultU5BU5D_t486281  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmOwnerDefault_t251897112 * m_Items[1];

public:
	inline FsmOwnerDefault_t251897112 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmOwnerDefault_t251897112 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmOwnerDefault_t251897112 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmAnimationCurve[]
struct FsmAnimationCurveU5BU5D_t2834032952  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmAnimationCurve_t2685995989 * m_Items[1];

public:
	inline FsmAnimationCurve_t2685995989 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmAnimationCurve_t2685995989 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmAnimationCurve_t2685995989 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FunctionCall[]
struct FunctionCallU5BU5D_t3031147529  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FunctionCall_t3279845016 * m_Items[1];

public:
	inline FunctionCall_t3279845016 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FunctionCall_t3279845016 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FunctionCall_t3279845016 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTemplateControl[]
struct FsmTemplateControlU5BU5D_t2045767528  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTemplateControl_t2786508133 * m_Items[1];

public:
	inline FsmTemplateControl_t2786508133 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTemplateControl_t2786508133 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTemplateControl_t2786508133 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEventTarget[]
struct FsmEventTargetU5BU5D_t2805508608  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEventTarget_t1823904941 * m_Items[1];

public:
	inline FsmEventTarget_t1823904941 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEventTarget_t1823904941 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEventTarget_t1823904941 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmProperty[]
struct FsmPropertyU5BU5D_t3532352710  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmProperty_t3927159007 * m_Items[1];

public:
	inline FsmProperty_t3927159007 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmProperty_t3927159007 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmProperty_t3927159007 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t3507996764  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LayoutOption_t964995201 * m_Items[1];

public:
	inline LayoutOption_t964995201 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LayoutOption_t964995201 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LayoutOption_t964995201 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2523845914  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmString_t952858651 * m_Items[1];

public:
	inline FsmString_t952858651 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmString_t952858651 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmString_t952858651 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t3873466740  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmObject_t821476169 * m_Items[1];

public:
	inline FsmObject_t821476169 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmObject_t821476169 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmObject_t821476169 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t3498949300  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVar_t1596150537 * m_Items[1];

public:
	inline FsmVar_t1596150537 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVar_t1596150537 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVar_t1596150537 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t1767340410  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmArray_t2129666875 * m_Items[1];

public:
	inline FsmArray_t2129666875 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmArray_t2129666875 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmArray_t2129666875 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t914670954  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEnum_t1076048395 * m_Items[1];

public:
	inline FsmEnum_t1076048395 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEnum_t1076048395 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEnum_t1076048395 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3689162173  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmBool_t1075959796 * m_Items[1];

public:
	inline FsmBool_t1075959796 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmBool_t1075959796 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmBool_t1075959796 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t120994540  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector2_t533912881 * m_Items[1];

public:
	inline FsmVector2_t533912881 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVector2_t533912881 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVector2_t533912881 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t607182279  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector3_t533912882 * m_Items[1];

public:
	inline FsmVector3_t533912882 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVector3_t533912882 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVector3_t533912882 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t530285832  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmColor_t2131419205 * m_Items[1];

public:
	inline FsmColor_t2131419205 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmColor_t2131419205 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmColor_t2131419205 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t4223261083  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmRect_t1076426478 * m_Items[1];

public:
	inline FsmRect_t1076426478 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmRect_t1076426478 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmRect_t1076426478 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t1443435833  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmQuaternion_t3871136040 * m_Items[1];

public:
	inline FsmQuaternion_t3871136040 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmQuaternion_t3871136040 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmQuaternion_t3871136040 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t3909450202  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t2476090292  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmStateAction_t2366529033 * m_Items[1];

public:
	inline FsmStateAction_t2366529033 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmStateAction_t2366529033 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmStateAction_t2366529033 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.IFsmStateAction[]
struct IFsmStateActionU5BU5D_t2889902239  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.DelayedEvent[]
struct DelayedEventU5BU5D_t362635711  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DelayedEvent_t1938906778 * m_Items[1];

public:
	inline DelayedEvent_t1938906778 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DelayedEvent_t1938906778 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DelayedEvent_t1938906778 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t2644459362  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmState_t2146334067 * m_Items[1];

public:
	inline FsmState_t2146334067 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmState_t2146334067 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmState_t2146334067 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2862142229  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEvent_t2133468028 * m_Items[1];

public:
	inline FsmEvent_t2133468028 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEvent_t2133468028 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEvent_t2133468028 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmLogEntry[]
struct FsmLogEntryU5BU5D_t1684404233  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmLogEntry_t2614866584 * m_Items[1];

public:
	inline FsmLogEntry_t2614866584 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmLogEntry_t2614866584 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmLogEntry_t2614866584 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmLog[]
struct FsmLogU5BU5D_t3658178947  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmLog_t1596141350 * m_Items[1];

public:
	inline FsmLog_t1596141350 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmLog_t1596141350 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmLog_t1596141350 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1409029100  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmMaterial_t924399665 * m_Items[1];

public:
	inline FsmMaterial_t924399665 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmMaterial_t924399665 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmMaterial_t924399665 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t997957744  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTexture_t3073272573 * m_Items[1];

public:
	inline FsmTexture_t3073272573 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTexture_t3073272573 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTexture_t3073272573 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
