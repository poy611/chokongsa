﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.JsonObjectAttribute
struct JsonObjectAttribute_t863918851;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerializatio1550301796.h"

// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::get_MemberSerialization()
extern "C"  int32_t JsonObjectAttribute_get_MemberSerialization_m984389181 (JsonObjectAttribute_t863918851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
