﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultSaveData
struct ResultSaveData_t3126241828;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// NetworkMng
struct NetworkMng_t1515215352;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkMng/<SaveResultDataCouroutine>c__Iterator14
struct  U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626  : public Il2CppObject
{
public:
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::nowNum
	int32_t ___nowNum_0;
	// ResultSaveData NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<info>__0
	ResultSaveData_t3126241828 * ___U3CinfoU3E__0_1;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::grade
	int32_t ___grade_2;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::gameId
	int32_t ___gameId_3;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::guestTh
	int32_t ___guestTh_4;
	// System.String NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<str>__1
	String_t* ___U3CstrU3E__1_5;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::$PC
	int32_t ___U24PC_6;
	// System.Object NetworkMng/<SaveResultDataCouroutine>c__Iterator14::$current
	Il2CppObject * ___U24current_7;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<$>nowNum
	int32_t ___U3CU24U3EnowNum_8;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<$>grade
	int32_t ___U3CU24U3Egrade_9;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<$>gameId
	int32_t ___U3CU24U3EgameId_10;
	// System.Int32 NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<$>guestTh
	int32_t ___U3CU24U3EguestTh_11;
	// NetworkMng NetworkMng/<SaveResultDataCouroutine>c__Iterator14::<>f__this
	NetworkMng_t1515215352 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_nowNum_0() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___nowNum_0)); }
	inline int32_t get_nowNum_0() const { return ___nowNum_0; }
	inline int32_t* get_address_of_nowNum_0() { return &___nowNum_0; }
	inline void set_nowNum_0(int32_t value)
	{
		___nowNum_0 = value;
	}

	inline static int32_t get_offset_of_U3CinfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CinfoU3E__0_1)); }
	inline ResultSaveData_t3126241828 * get_U3CinfoU3E__0_1() const { return ___U3CinfoU3E__0_1; }
	inline ResultSaveData_t3126241828 ** get_address_of_U3CinfoU3E__0_1() { return &___U3CinfoU3E__0_1; }
	inline void set_U3CinfoU3E__0_1(ResultSaveData_t3126241828 * value)
	{
		___U3CinfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinfoU3E__0_1, value);
	}

	inline static int32_t get_offset_of_grade_2() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___grade_2)); }
	inline int32_t get_grade_2() const { return ___grade_2; }
	inline int32_t* get_address_of_grade_2() { return &___grade_2; }
	inline void set_grade_2(int32_t value)
	{
		___grade_2 = value;
	}

	inline static int32_t get_offset_of_gameId_3() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___gameId_3)); }
	inline int32_t get_gameId_3() const { return ___gameId_3; }
	inline int32_t* get_address_of_gameId_3() { return &___gameId_3; }
	inline void set_gameId_3(int32_t value)
	{
		___gameId_3 = value;
	}

	inline static int32_t get_offset_of_guestTh_4() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___guestTh_4)); }
	inline int32_t get_guestTh_4() const { return ___guestTh_4; }
	inline int32_t* get_address_of_guestTh_4() { return &___guestTh_4; }
	inline void set_guestTh_4(int32_t value)
	{
		___guestTh_4 = value;
	}

	inline static int32_t get_offset_of_U3CstrU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CstrU3E__1_5)); }
	inline String_t* get_U3CstrU3E__1_5() const { return ___U3CstrU3E__1_5; }
	inline String_t** get_address_of_U3CstrU3E__1_5() { return &___U3CstrU3E__1_5; }
	inline void set_U3CstrU3E__1_5(String_t* value)
	{
		___U3CstrU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EnowNum_8() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CU24U3EnowNum_8)); }
	inline int32_t get_U3CU24U3EnowNum_8() const { return ___U3CU24U3EnowNum_8; }
	inline int32_t* get_address_of_U3CU24U3EnowNum_8() { return &___U3CU24U3EnowNum_8; }
	inline void set_U3CU24U3EnowNum_8(int32_t value)
	{
		___U3CU24U3EnowNum_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Egrade_9() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CU24U3Egrade_9)); }
	inline int32_t get_U3CU24U3Egrade_9() const { return ___U3CU24U3Egrade_9; }
	inline int32_t* get_address_of_U3CU24U3Egrade_9() { return &___U3CU24U3Egrade_9; }
	inline void set_U3CU24U3Egrade_9(int32_t value)
	{
		___U3CU24U3Egrade_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EgameId_10() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CU24U3EgameId_10)); }
	inline int32_t get_U3CU24U3EgameId_10() const { return ___U3CU24U3EgameId_10; }
	inline int32_t* get_address_of_U3CU24U3EgameId_10() { return &___U3CU24U3EgameId_10; }
	inline void set_U3CU24U3EgameId_10(int32_t value)
	{
		___U3CU24U3EgameId_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EguestTh_11() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CU24U3EguestTh_11)); }
	inline int32_t get_U3CU24U3EguestTh_11() const { return ___U3CU24U3EguestTh_11; }
	inline int32_t* get_address_of_U3CU24U3EguestTh_11() { return &___U3CU24U3EguestTh_11; }
	inline void set_U3CU24U3EguestTh_11(int32_t value)
	{
		___U3CU24U3EguestTh_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CSaveResultDataCouroutineU3Ec__Iterator14_t3546773626, ___U3CU3Ef__this_12)); }
	inline NetworkMng_t1515215352 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline NetworkMng_t1515215352 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(NetworkMng_t1515215352 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
