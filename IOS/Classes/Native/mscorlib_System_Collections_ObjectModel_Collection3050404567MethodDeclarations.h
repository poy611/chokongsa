﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>
struct Collection_1_t3050404567;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.JsonPosition[]
struct JsonPositionU5BU5D_t3412818772;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.JsonPosition>
struct IEnumerator_1_t1481844162;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonPosition>
struct IList_1_t2264626316;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition3864946409.h"

// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::.ctor()
extern "C"  void Collection_1__ctor_m3044728269_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3044728269(__this, method) ((  void (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1__ctor_m3044728269_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m971559882_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m971559882(__this, method) ((  bool (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m971559882_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1105486871_gshared (Collection_1_t3050404567 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1105486871(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3050404567 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1105486871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m355296358_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m355296358(__this, method) ((  Il2CppObject * (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m355296358_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1983155991_gshared (Collection_1_t3050404567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1983155991(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3050404567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1983155991_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3299367689_gshared (Collection_1_t3050404567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3299367689(__this, ___value0, method) ((  bool (*) (Collection_1_t3050404567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3299367689_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3420021231_gshared (Collection_1_t3050404567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3420021231(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3050404567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3420021231_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m4188810722_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m4188810722(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m4188810722_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m575181830_gshared (Collection_1_t3050404567 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m575181830(__this, ___value0, method) ((  void (*) (Collection_1_t3050404567 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m575181830_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1131230131_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1131230131(__this, method) ((  bool (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1131230131_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2967319589_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2967319589(__this, method) ((  Il2CppObject * (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2967319589_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m313476408_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m313476408(__this, method) ((  bool (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m313476408_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2126688961_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2126688961(__this, method) ((  bool (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2126688961_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m801452140_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m801452140(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3050404567 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m801452140_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1676699961_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1676699961(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1676699961_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::Add(T)
extern "C"  void Collection_1_Add_m1058190610_gshared (Collection_1_t3050404567 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1058190610(__this, ___item0, method) ((  void (*) (Collection_1_t3050404567 *, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_Add_m1058190610_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::Clear()
extern "C"  void Collection_1_Clear_m450861560_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_Clear_m450861560(__this, method) ((  void (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_Clear_m450861560_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3969221130_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3969221130(__this, method) ((  void (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_ClearItems_m3969221130_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::Contains(T)
extern "C"  bool Collection_1_Contains_m692583530_gshared (Collection_1_t3050404567 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m692583530(__this, ___item0, method) ((  bool (*) (Collection_1_t3050404567 *, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_Contains_m692583530_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m416300162_gshared (Collection_1_t3050404567 * __this, JsonPositionU5BU5D_t3412818772* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m416300162(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3050404567 *, JsonPositionU5BU5D_t3412818772*, int32_t, const MethodInfo*))Collection_1_CopyTo_m416300162_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2798454337_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2798454337(__this, method) ((  Il2CppObject* (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_GetEnumerator_m2798454337_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1966536910_gshared (Collection_1_t3050404567 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1966536910(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3050404567 *, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_IndexOf_m1966536910_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3740765049_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, JsonPosition_t3864946409  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3740765049(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_Insert_m3740765049_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2593805612_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, JsonPosition_t3864946409  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2593805612(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_InsertItem_m2593805612_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1176116280_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1176116280(__this, method) ((  Il2CppObject* (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_get_Items_m1176116280_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::Remove(T)
extern "C"  bool Collection_1_Remove_m2252656357_gshared (Collection_1_t3050404567 * __this, JsonPosition_t3864946409  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2252656357(__this, ___item0, method) ((  bool (*) (Collection_1_t3050404567 *, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_Remove_m2252656357_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1614617919_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1614617919(__this, ___index0, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1614617919_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4064251295_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4064251295(__this, ___index0, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4064251295_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m650409581_gshared (Collection_1_t3050404567 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m650409581(__this, method) ((  int32_t (*) (Collection_1_t3050404567 *, const MethodInfo*))Collection_1_get_Count_m650409581_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::get_Item(System.Int32)
extern "C"  JsonPosition_t3864946409  Collection_1_get_Item_m3550966821_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3550966821(__this, ___index0, method) ((  JsonPosition_t3864946409  (*) (Collection_1_t3050404567 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3550966821_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1797181968_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, JsonPosition_t3864946409  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1797181968(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_set_Item_m1797181968_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1316189289_gshared (Collection_1_t3050404567 * __this, int32_t ___index0, JsonPosition_t3864946409  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1316189289(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3050404567 *, int32_t, JsonPosition_t3864946409 , const MethodInfo*))Collection_1_SetItem_m1316189289_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4024882306_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4024882306(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4024882306_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::ConvertItem(System.Object)
extern "C"  JsonPosition_t3864946409  Collection_1_ConvertItem_m1611696260_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1611696260(__this /* static, unused */, ___item0, method) ((  JsonPosition_t3864946409  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1611696260_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m277630786_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m277630786(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m277630786_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1002515714_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1002515714(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1002515714_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonPosition>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1117391069_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1117391069(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1117391069_gshared)(__this /* static, unused */, ___list0, method)
