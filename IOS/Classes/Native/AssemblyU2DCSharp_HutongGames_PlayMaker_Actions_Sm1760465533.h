﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t251897112;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t1697147867;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2134102846;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothFollowAction
struct  SmoothFollowAction_t1760465533  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothFollowAction::gameObject
	FsmOwnerDefault_t251897112 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::targetObject
	FsmGameObject_t1697147867 * ___targetObject_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::distance
	FsmFloat_t2134102846 * ___distance_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::height
	FsmFloat_t2134102846 * ___height_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::heightDamping
	FsmFloat_t2134102846 * ___heightDamping_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::rotationDamping
	FsmFloat_t2134102846 * ___rotationDamping_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::cachedObject
	GameObject_t3674682005 * ___cachedObject_17;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::myTransform
	Transform_t1659122786 * ___myTransform_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::cachedTarget
	GameObject_t3674682005 * ___cachedTarget_19;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::targetTransform
	Transform_t1659122786 * ___targetTransform_20;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___gameObject_11)); }
	inline FsmOwnerDefault_t251897112 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t251897112 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t251897112 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_targetObject_12() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___targetObject_12)); }
	inline FsmGameObject_t1697147867 * get_targetObject_12() const { return ___targetObject_12; }
	inline FsmGameObject_t1697147867 ** get_address_of_targetObject_12() { return &___targetObject_12; }
	inline void set_targetObject_12(FsmGameObject_t1697147867 * value)
	{
		___targetObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_12, value);
	}

	inline static int32_t get_offset_of_distance_13() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___distance_13)); }
	inline FsmFloat_t2134102846 * get_distance_13() const { return ___distance_13; }
	inline FsmFloat_t2134102846 ** get_address_of_distance_13() { return &___distance_13; }
	inline void set_distance_13(FsmFloat_t2134102846 * value)
	{
		___distance_13 = value;
		Il2CppCodeGenWriteBarrier(&___distance_13, value);
	}

	inline static int32_t get_offset_of_height_14() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___height_14)); }
	inline FsmFloat_t2134102846 * get_height_14() const { return ___height_14; }
	inline FsmFloat_t2134102846 ** get_address_of_height_14() { return &___height_14; }
	inline void set_height_14(FsmFloat_t2134102846 * value)
	{
		___height_14 = value;
		Il2CppCodeGenWriteBarrier(&___height_14, value);
	}

	inline static int32_t get_offset_of_heightDamping_15() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___heightDamping_15)); }
	inline FsmFloat_t2134102846 * get_heightDamping_15() const { return ___heightDamping_15; }
	inline FsmFloat_t2134102846 ** get_address_of_heightDamping_15() { return &___heightDamping_15; }
	inline void set_heightDamping_15(FsmFloat_t2134102846 * value)
	{
		___heightDamping_15 = value;
		Il2CppCodeGenWriteBarrier(&___heightDamping_15, value);
	}

	inline static int32_t get_offset_of_rotationDamping_16() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___rotationDamping_16)); }
	inline FsmFloat_t2134102846 * get_rotationDamping_16() const { return ___rotationDamping_16; }
	inline FsmFloat_t2134102846 ** get_address_of_rotationDamping_16() { return &___rotationDamping_16; }
	inline void set_rotationDamping_16(FsmFloat_t2134102846 * value)
	{
		___rotationDamping_16 = value;
		Il2CppCodeGenWriteBarrier(&___rotationDamping_16, value);
	}

	inline static int32_t get_offset_of_cachedObject_17() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___cachedObject_17)); }
	inline GameObject_t3674682005 * get_cachedObject_17() const { return ___cachedObject_17; }
	inline GameObject_t3674682005 ** get_address_of_cachedObject_17() { return &___cachedObject_17; }
	inline void set_cachedObject_17(GameObject_t3674682005 * value)
	{
		___cachedObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___cachedObject_17, value);
	}

	inline static int32_t get_offset_of_myTransform_18() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___myTransform_18)); }
	inline Transform_t1659122786 * get_myTransform_18() const { return ___myTransform_18; }
	inline Transform_t1659122786 ** get_address_of_myTransform_18() { return &___myTransform_18; }
	inline void set_myTransform_18(Transform_t1659122786 * value)
	{
		___myTransform_18 = value;
		Il2CppCodeGenWriteBarrier(&___myTransform_18, value);
	}

	inline static int32_t get_offset_of_cachedTarget_19() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___cachedTarget_19)); }
	inline GameObject_t3674682005 * get_cachedTarget_19() const { return ___cachedTarget_19; }
	inline GameObject_t3674682005 ** get_address_of_cachedTarget_19() { return &___cachedTarget_19; }
	inline void set_cachedTarget_19(GameObject_t3674682005 * value)
	{
		___cachedTarget_19 = value;
		Il2CppCodeGenWriteBarrier(&___cachedTarget_19, value);
	}

	inline static int32_t get_offset_of_targetTransform_20() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t1760465533, ___targetTransform_20)); }
	inline Transform_t1659122786 * get_targetTransform_20() const { return ___targetTransform_20; }
	inline Transform_t1659122786 ** get_address_of_targetTransform_20() { return &___targetTransform_20; }
	inline void set_targetTransform_20(Transform_t1659122786 * value)
	{
		___targetTransform_20 = value;
		Il2CppCodeGenWriteBarrier(&___targetTransform_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
