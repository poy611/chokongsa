﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1920129602.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_3137786926.h"

// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2103005363_gshared (InternalEnumerator_1_t1920129602 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2103005363(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1920129602 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2103005363_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997(__this, method) ((  void (*) (InternalEnumerator_1_t1920129602 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2675689997_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1920129602 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1164641731_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2821688906_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2821688906(__this, method) ((  void (*) (InternalEnumerator_1_t1920129602 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2821688906_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1340035645_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1340035645(__this, method) ((  bool (*) (InternalEnumerator_1_t1920129602 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1340035645_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847028892_gshared (InternalEnumerator_1_t1920129602 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3847028892(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1920129602 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3847028892_gshared)(__this, method)
