﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t2133468028;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2366529033.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress
struct  NetworkHavePublicIpAddress_t2341933110  : public FsmStateAction_t2366529033
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::havePublicIpAddress
	FsmBool_t1075959796 * ___havePublicIpAddress_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::publicIpAddressFoundEvent
	FsmEvent_t2133468028 * ___publicIpAddressFoundEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkHavePublicIpAddress::publicIpAddressNotFoundEvent
	FsmEvent_t2133468028 * ___publicIpAddressNotFoundEvent_13;

public:
	inline static int32_t get_offset_of_havePublicIpAddress_11() { return static_cast<int32_t>(offsetof(NetworkHavePublicIpAddress_t2341933110, ___havePublicIpAddress_11)); }
	inline FsmBool_t1075959796 * get_havePublicIpAddress_11() const { return ___havePublicIpAddress_11; }
	inline FsmBool_t1075959796 ** get_address_of_havePublicIpAddress_11() { return &___havePublicIpAddress_11; }
	inline void set_havePublicIpAddress_11(FsmBool_t1075959796 * value)
	{
		___havePublicIpAddress_11 = value;
		Il2CppCodeGenWriteBarrier(&___havePublicIpAddress_11, value);
	}

	inline static int32_t get_offset_of_publicIpAddressFoundEvent_12() { return static_cast<int32_t>(offsetof(NetworkHavePublicIpAddress_t2341933110, ___publicIpAddressFoundEvent_12)); }
	inline FsmEvent_t2133468028 * get_publicIpAddressFoundEvent_12() const { return ___publicIpAddressFoundEvent_12; }
	inline FsmEvent_t2133468028 ** get_address_of_publicIpAddressFoundEvent_12() { return &___publicIpAddressFoundEvent_12; }
	inline void set_publicIpAddressFoundEvent_12(FsmEvent_t2133468028 * value)
	{
		___publicIpAddressFoundEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___publicIpAddressFoundEvent_12, value);
	}

	inline static int32_t get_offset_of_publicIpAddressNotFoundEvent_13() { return static_cast<int32_t>(offsetof(NetworkHavePublicIpAddress_t2341933110, ___publicIpAddressNotFoundEvent_13)); }
	inline FsmEvent_t2133468028 * get_publicIpAddressNotFoundEvent_13() const { return ___publicIpAddressNotFoundEvent_13; }
	inline FsmEvent_t2133468028 ** get_address_of_publicIpAddressNotFoundEvent_13() { return &___publicIpAddressNotFoundEvent_13; }
	inline void set_publicIpAddressNotFoundEvent_13(FsmEvent_t2133468028 * value)
	{
		___publicIpAddressNotFoundEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___publicIpAddressNotFoundEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
