﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76
struct U3CRoomConnectedU3Ec__AnonStorey76_t3218131764;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76::.ctor()
extern "C"  void U3CRoomConnectedU3Ec__AnonStorey76__ctor_m583780839 (U3CRoomConnectedU3Ec__AnonStorey76_t3218131764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey76::<>m__50()
extern "C"  void U3CRoomConnectedU3Ec__AnonStorey76_U3CU3Em__50_m680829643 (U3CRoomConnectedU3Ec__AnonStorey76_t3218131764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
