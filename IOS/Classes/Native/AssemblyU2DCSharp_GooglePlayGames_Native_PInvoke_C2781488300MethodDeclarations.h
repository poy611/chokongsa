﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2708837308_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2708837308(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2__ctor_m2708837308_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey9F`2/<AsOnGameThreadCallback>c__AnonStoreyA0`2<System.Object,System.Object>::<>m__A0()
extern "C"  void U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2750988244_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 * __this, const MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2750988244(__this, method) ((  void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_t2781488300 *, const MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStoreyA0_2_U3CU3Em__A0_m2750988244_gshared)(__this, method)
