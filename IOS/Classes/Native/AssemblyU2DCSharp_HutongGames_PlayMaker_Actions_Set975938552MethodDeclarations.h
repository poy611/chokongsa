﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIColor
struct SetGUIColor_t975938552;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::.ctor()
extern "C"  void SetGUIColor__ctor_m931675710 (SetGUIColor_t975938552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::Reset()
extern "C"  void SetGUIColor_Reset_m2873075947 (SetGUIColor_t975938552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::OnGUI()
extern "C"  void SetGUIColor_OnGUI_m427074360 (SetGUIColor_t975938552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
