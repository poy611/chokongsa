﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_3187252993.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::.ctor()
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m3294683583_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 * __this, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m3294683583(__this, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3__ctor_m3294683583_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey42`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,System.Object,System.Object>::<>m__12(T1,T2,T3)
extern "C"  void U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019_gshared (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 * __this, int32_t ___val10, Il2CppObject * ___val21, Il2CppObject * ___val32, const MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019(__this, ___val10, ___val21, ___val32, method) ((  void (*) (U3CToOnGameThreadU3Ec__AnonStorey42_3_t3078889654 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey42_3_U3CU3Em__12_m1271659019_gshared)(__this, ___val10, ___val21, ___val32, method)
