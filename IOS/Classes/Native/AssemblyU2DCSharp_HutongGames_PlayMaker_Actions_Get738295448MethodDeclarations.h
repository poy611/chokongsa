﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseY
struct GetMouseY_t738295448;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseY::.ctor()
extern "C"  void GetMouseY__ctor_m599028126 (GetMouseY_t738295448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::Reset()
extern "C"  void GetMouseY_Reset_m2540428363 (GetMouseY_t738295448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnEnter()
extern "C"  void GetMouseY_OnEnter_m3827107317 (GetMouseY_t738295448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnUpdate()
extern "C"  void GetMouseY_OnUpdate_m1809769198 (GetMouseY_t738295448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::DoGetMouseY()
extern "C"  void GetMouseY_DoGetMouseY_m1756108219 (GetMouseY_t738295448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
