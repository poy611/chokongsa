﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorIK
struct PlayMakerAnimatorIK_t2388642745;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerAnimatorIK::OnAnimatorIK(System.Int32)
extern "C"  void PlayMakerAnimatorIK_OnAnimatorIK_m4122536525 (PlayMakerAnimatorIK_t2388642745 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorIK::.ctor()
extern "C"  void PlayMakerAnimatorIK__ctor_m2998146256 (PlayMakerAnimatorIK_t2388642745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
