﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;
// StructforMinigame
struct StructforMinigame_t1286533533;
// ConstforMinigame
struct ConstforMinigame_t2789090703;
// PopupManagement
struct PopupManagement_t282433007;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t969614734;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t1907060817;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// LobbyTalkBoxAnimation
struct LobbyTalkBoxAnimation_t432405659;
// LobbyDeskAnimation
struct LobbyDeskAnimation_t4061140181;
// UnityEngine.UI.Button
struct Button_t3896396478;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String[,]
struct StringU5BU2CU5D_t4054002953;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_ScoreManager_Lobby_menu3396474364.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreManager_Lobby
struct  ScoreManager_Lobby_t3853030226  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreManager_Lobby::nowNum
	int32_t ___nowNum_2;
	// System.Diagnostics.Stopwatch ScoreManager_Lobby::sw
	Stopwatch_t3420517611 * ___sw_3;
	// System.Single ScoreManager_Lobby::ticks
	float ___ticks_4;
	// StructforMinigame ScoreManager_Lobby::stuff
	StructforMinigame_t1286533533 * ___stuff_5;
	// ConstforMinigame ScoreManager_Lobby::cuff
	ConstforMinigame_t2789090703 * ___cuff_6;
	// ScoreManager_Lobby/menu ScoreManager_Lobby::getMenu
	int32_t ___getMenu_7;
	// PopupManagement ScoreManager_Lobby::popup
	PopupManagement_t282433007 * ___popup_8;
	// UnityEngine.UI.Text ScoreManager_Lobby::timmer
	Text_t9039225 * ___timmer_9;
	// UnityEngine.UI.Text ScoreManager_Lobby::MayItakeYourOrder
	Text_t9039225 * ___MayItakeYourOrder_10;
	// UnityEngine.UI.Image ScoreManager_Lobby::ShowmetheCoffee
	Image_t538875265 * ___ShowmetheCoffee_11;
	// UnityEngine.UI.Image ScoreManager_Lobby::desk
	Image_t538875265 * ___desk_12;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> ScoreManager_Lobby::bts
	List_1_t969614734 * ___bts_13;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> ScoreManager_Lobby::imgs
	List_1_t1907060817 * ___imgs_14;
	// UnityEngine.GameObject ScoreManager_Lobby::Bars
	GameObject_t3674682005 * ___Bars_15;
	// UnityEngine.GameObject ScoreManager_Lobby::_Ready
	GameObject_t3674682005 * ____Ready_16;
	// UnityEngine.GameObject ScoreManager_Lobby::_Start
	GameObject_t3674682005 * ____Start_17;
	// LobbyTalkBoxAnimation ScoreManager_Lobby::lobby_ani
	LobbyTalkBoxAnimation_t432405659 * ___lobby_ani_18;
	// LobbyDeskAnimation ScoreManager_Lobby::lobby_desk_ani
	LobbyDeskAnimation_t4061140181 * ___lobby_desk_ani_19;
	// System.Int32 ScoreManager_Lobby::currentMenuPage
	int32_t ___currentMenuPage_20;
	// UnityEngine.UI.Button ScoreManager_Lobby::prevButton
	Button_t3896396478 * ___prevButton_21;
	// UnityEngine.UI.Button ScoreManager_Lobby::nextButton
	Button_t3896396478 * ___nextButton_22;
	// UnityEngine.GameObject[] ScoreManager_Lobby::MenuButton
	GameObjectU5BU5D_t2662109048* ___MenuButton_23;
	// UnityEngine.GameObject ScoreManager_Lobby::creamButton
	GameObject_t3674682005 * ___creamButton_24;
	// System.Int32 ScoreManager_Lobby::randomNum
	int32_t ___randomNum_25;
	// System.Int32 ScoreManager_Lobby::randomMenu
	int32_t ___randomMenu_26;
	// System.Int32 ScoreManager_Lobby::randomHI
	int32_t ___randomHI_27;
	// System.Int32 ScoreManager_Lobby::randomCream
	int32_t ___randomCream_28;
	// System.Int32 ScoreManager_Lobby::counterBars
	int32_t ___counterBars_29;
	// System.String[] ScoreManager_Lobby::menuStr
	StringU5BU5D_t4054002952* ___menuStr_30;
	// System.String[] ScoreManager_Lobby::hiStr
	StringU5BU5D_t4054002952* ___hiStr_31;
	// System.String[,] ScoreManager_Lobby::creamText
	StringU5BU2CU5D_t4054002953* ___creamText_32;
	// System.Boolean ScoreManager_Lobby::isOkDone
	bool ___isOkDone_33;

public:
	inline static int32_t get_offset_of_nowNum_2() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___nowNum_2)); }
	inline int32_t get_nowNum_2() const { return ___nowNum_2; }
	inline int32_t* get_address_of_nowNum_2() { return &___nowNum_2; }
	inline void set_nowNum_2(int32_t value)
	{
		___nowNum_2 = value;
	}

	inline static int32_t get_offset_of_sw_3() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___sw_3)); }
	inline Stopwatch_t3420517611 * get_sw_3() const { return ___sw_3; }
	inline Stopwatch_t3420517611 ** get_address_of_sw_3() { return &___sw_3; }
	inline void set_sw_3(Stopwatch_t3420517611 * value)
	{
		___sw_3 = value;
		Il2CppCodeGenWriteBarrier(&___sw_3, value);
	}

	inline static int32_t get_offset_of_ticks_4() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___ticks_4)); }
	inline float get_ticks_4() const { return ___ticks_4; }
	inline float* get_address_of_ticks_4() { return &___ticks_4; }
	inline void set_ticks_4(float value)
	{
		___ticks_4 = value;
	}

	inline static int32_t get_offset_of_stuff_5() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___stuff_5)); }
	inline StructforMinigame_t1286533533 * get_stuff_5() const { return ___stuff_5; }
	inline StructforMinigame_t1286533533 ** get_address_of_stuff_5() { return &___stuff_5; }
	inline void set_stuff_5(StructforMinigame_t1286533533 * value)
	{
		___stuff_5 = value;
		Il2CppCodeGenWriteBarrier(&___stuff_5, value);
	}

	inline static int32_t get_offset_of_cuff_6() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___cuff_6)); }
	inline ConstforMinigame_t2789090703 * get_cuff_6() const { return ___cuff_6; }
	inline ConstforMinigame_t2789090703 ** get_address_of_cuff_6() { return &___cuff_6; }
	inline void set_cuff_6(ConstforMinigame_t2789090703 * value)
	{
		___cuff_6 = value;
		Il2CppCodeGenWriteBarrier(&___cuff_6, value);
	}

	inline static int32_t get_offset_of_getMenu_7() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___getMenu_7)); }
	inline int32_t get_getMenu_7() const { return ___getMenu_7; }
	inline int32_t* get_address_of_getMenu_7() { return &___getMenu_7; }
	inline void set_getMenu_7(int32_t value)
	{
		___getMenu_7 = value;
	}

	inline static int32_t get_offset_of_popup_8() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___popup_8)); }
	inline PopupManagement_t282433007 * get_popup_8() const { return ___popup_8; }
	inline PopupManagement_t282433007 ** get_address_of_popup_8() { return &___popup_8; }
	inline void set_popup_8(PopupManagement_t282433007 * value)
	{
		___popup_8 = value;
		Il2CppCodeGenWriteBarrier(&___popup_8, value);
	}

	inline static int32_t get_offset_of_timmer_9() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___timmer_9)); }
	inline Text_t9039225 * get_timmer_9() const { return ___timmer_9; }
	inline Text_t9039225 ** get_address_of_timmer_9() { return &___timmer_9; }
	inline void set_timmer_9(Text_t9039225 * value)
	{
		___timmer_9 = value;
		Il2CppCodeGenWriteBarrier(&___timmer_9, value);
	}

	inline static int32_t get_offset_of_MayItakeYourOrder_10() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___MayItakeYourOrder_10)); }
	inline Text_t9039225 * get_MayItakeYourOrder_10() const { return ___MayItakeYourOrder_10; }
	inline Text_t9039225 ** get_address_of_MayItakeYourOrder_10() { return &___MayItakeYourOrder_10; }
	inline void set_MayItakeYourOrder_10(Text_t9039225 * value)
	{
		___MayItakeYourOrder_10 = value;
		Il2CppCodeGenWriteBarrier(&___MayItakeYourOrder_10, value);
	}

	inline static int32_t get_offset_of_ShowmetheCoffee_11() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___ShowmetheCoffee_11)); }
	inline Image_t538875265 * get_ShowmetheCoffee_11() const { return ___ShowmetheCoffee_11; }
	inline Image_t538875265 ** get_address_of_ShowmetheCoffee_11() { return &___ShowmetheCoffee_11; }
	inline void set_ShowmetheCoffee_11(Image_t538875265 * value)
	{
		___ShowmetheCoffee_11 = value;
		Il2CppCodeGenWriteBarrier(&___ShowmetheCoffee_11, value);
	}

	inline static int32_t get_offset_of_desk_12() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___desk_12)); }
	inline Image_t538875265 * get_desk_12() const { return ___desk_12; }
	inline Image_t538875265 ** get_address_of_desk_12() { return &___desk_12; }
	inline void set_desk_12(Image_t538875265 * value)
	{
		___desk_12 = value;
		Il2CppCodeGenWriteBarrier(&___desk_12, value);
	}

	inline static int32_t get_offset_of_bts_13() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___bts_13)); }
	inline List_1_t969614734 * get_bts_13() const { return ___bts_13; }
	inline List_1_t969614734 ** get_address_of_bts_13() { return &___bts_13; }
	inline void set_bts_13(List_1_t969614734 * value)
	{
		___bts_13 = value;
		Il2CppCodeGenWriteBarrier(&___bts_13, value);
	}

	inline static int32_t get_offset_of_imgs_14() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___imgs_14)); }
	inline List_1_t1907060817 * get_imgs_14() const { return ___imgs_14; }
	inline List_1_t1907060817 ** get_address_of_imgs_14() { return &___imgs_14; }
	inline void set_imgs_14(List_1_t1907060817 * value)
	{
		___imgs_14 = value;
		Il2CppCodeGenWriteBarrier(&___imgs_14, value);
	}

	inline static int32_t get_offset_of_Bars_15() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___Bars_15)); }
	inline GameObject_t3674682005 * get_Bars_15() const { return ___Bars_15; }
	inline GameObject_t3674682005 ** get_address_of_Bars_15() { return &___Bars_15; }
	inline void set_Bars_15(GameObject_t3674682005 * value)
	{
		___Bars_15 = value;
		Il2CppCodeGenWriteBarrier(&___Bars_15, value);
	}

	inline static int32_t get_offset_of__Ready_16() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ____Ready_16)); }
	inline GameObject_t3674682005 * get__Ready_16() const { return ____Ready_16; }
	inline GameObject_t3674682005 ** get_address_of__Ready_16() { return &____Ready_16; }
	inline void set__Ready_16(GameObject_t3674682005 * value)
	{
		____Ready_16 = value;
		Il2CppCodeGenWriteBarrier(&____Ready_16, value);
	}

	inline static int32_t get_offset_of__Start_17() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ____Start_17)); }
	inline GameObject_t3674682005 * get__Start_17() const { return ____Start_17; }
	inline GameObject_t3674682005 ** get_address_of__Start_17() { return &____Start_17; }
	inline void set__Start_17(GameObject_t3674682005 * value)
	{
		____Start_17 = value;
		Il2CppCodeGenWriteBarrier(&____Start_17, value);
	}

	inline static int32_t get_offset_of_lobby_ani_18() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___lobby_ani_18)); }
	inline LobbyTalkBoxAnimation_t432405659 * get_lobby_ani_18() const { return ___lobby_ani_18; }
	inline LobbyTalkBoxAnimation_t432405659 ** get_address_of_lobby_ani_18() { return &___lobby_ani_18; }
	inline void set_lobby_ani_18(LobbyTalkBoxAnimation_t432405659 * value)
	{
		___lobby_ani_18 = value;
		Il2CppCodeGenWriteBarrier(&___lobby_ani_18, value);
	}

	inline static int32_t get_offset_of_lobby_desk_ani_19() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___lobby_desk_ani_19)); }
	inline LobbyDeskAnimation_t4061140181 * get_lobby_desk_ani_19() const { return ___lobby_desk_ani_19; }
	inline LobbyDeskAnimation_t4061140181 ** get_address_of_lobby_desk_ani_19() { return &___lobby_desk_ani_19; }
	inline void set_lobby_desk_ani_19(LobbyDeskAnimation_t4061140181 * value)
	{
		___lobby_desk_ani_19 = value;
		Il2CppCodeGenWriteBarrier(&___lobby_desk_ani_19, value);
	}

	inline static int32_t get_offset_of_currentMenuPage_20() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___currentMenuPage_20)); }
	inline int32_t get_currentMenuPage_20() const { return ___currentMenuPage_20; }
	inline int32_t* get_address_of_currentMenuPage_20() { return &___currentMenuPage_20; }
	inline void set_currentMenuPage_20(int32_t value)
	{
		___currentMenuPage_20 = value;
	}

	inline static int32_t get_offset_of_prevButton_21() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___prevButton_21)); }
	inline Button_t3896396478 * get_prevButton_21() const { return ___prevButton_21; }
	inline Button_t3896396478 ** get_address_of_prevButton_21() { return &___prevButton_21; }
	inline void set_prevButton_21(Button_t3896396478 * value)
	{
		___prevButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___prevButton_21, value);
	}

	inline static int32_t get_offset_of_nextButton_22() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___nextButton_22)); }
	inline Button_t3896396478 * get_nextButton_22() const { return ___nextButton_22; }
	inline Button_t3896396478 ** get_address_of_nextButton_22() { return &___nextButton_22; }
	inline void set_nextButton_22(Button_t3896396478 * value)
	{
		___nextButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___nextButton_22, value);
	}

	inline static int32_t get_offset_of_MenuButton_23() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___MenuButton_23)); }
	inline GameObjectU5BU5D_t2662109048* get_MenuButton_23() const { return ___MenuButton_23; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_MenuButton_23() { return &___MenuButton_23; }
	inline void set_MenuButton_23(GameObjectU5BU5D_t2662109048* value)
	{
		___MenuButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___MenuButton_23, value);
	}

	inline static int32_t get_offset_of_creamButton_24() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___creamButton_24)); }
	inline GameObject_t3674682005 * get_creamButton_24() const { return ___creamButton_24; }
	inline GameObject_t3674682005 ** get_address_of_creamButton_24() { return &___creamButton_24; }
	inline void set_creamButton_24(GameObject_t3674682005 * value)
	{
		___creamButton_24 = value;
		Il2CppCodeGenWriteBarrier(&___creamButton_24, value);
	}

	inline static int32_t get_offset_of_randomNum_25() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___randomNum_25)); }
	inline int32_t get_randomNum_25() const { return ___randomNum_25; }
	inline int32_t* get_address_of_randomNum_25() { return &___randomNum_25; }
	inline void set_randomNum_25(int32_t value)
	{
		___randomNum_25 = value;
	}

	inline static int32_t get_offset_of_randomMenu_26() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___randomMenu_26)); }
	inline int32_t get_randomMenu_26() const { return ___randomMenu_26; }
	inline int32_t* get_address_of_randomMenu_26() { return &___randomMenu_26; }
	inline void set_randomMenu_26(int32_t value)
	{
		___randomMenu_26 = value;
	}

	inline static int32_t get_offset_of_randomHI_27() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___randomHI_27)); }
	inline int32_t get_randomHI_27() const { return ___randomHI_27; }
	inline int32_t* get_address_of_randomHI_27() { return &___randomHI_27; }
	inline void set_randomHI_27(int32_t value)
	{
		___randomHI_27 = value;
	}

	inline static int32_t get_offset_of_randomCream_28() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___randomCream_28)); }
	inline int32_t get_randomCream_28() const { return ___randomCream_28; }
	inline int32_t* get_address_of_randomCream_28() { return &___randomCream_28; }
	inline void set_randomCream_28(int32_t value)
	{
		___randomCream_28 = value;
	}

	inline static int32_t get_offset_of_counterBars_29() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___counterBars_29)); }
	inline int32_t get_counterBars_29() const { return ___counterBars_29; }
	inline int32_t* get_address_of_counterBars_29() { return &___counterBars_29; }
	inline void set_counterBars_29(int32_t value)
	{
		___counterBars_29 = value;
	}

	inline static int32_t get_offset_of_menuStr_30() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___menuStr_30)); }
	inline StringU5BU5D_t4054002952* get_menuStr_30() const { return ___menuStr_30; }
	inline StringU5BU5D_t4054002952** get_address_of_menuStr_30() { return &___menuStr_30; }
	inline void set_menuStr_30(StringU5BU5D_t4054002952* value)
	{
		___menuStr_30 = value;
		Il2CppCodeGenWriteBarrier(&___menuStr_30, value);
	}

	inline static int32_t get_offset_of_hiStr_31() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___hiStr_31)); }
	inline StringU5BU5D_t4054002952* get_hiStr_31() const { return ___hiStr_31; }
	inline StringU5BU5D_t4054002952** get_address_of_hiStr_31() { return &___hiStr_31; }
	inline void set_hiStr_31(StringU5BU5D_t4054002952* value)
	{
		___hiStr_31 = value;
		Il2CppCodeGenWriteBarrier(&___hiStr_31, value);
	}

	inline static int32_t get_offset_of_creamText_32() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___creamText_32)); }
	inline StringU5BU2CU5D_t4054002953* get_creamText_32() const { return ___creamText_32; }
	inline StringU5BU2CU5D_t4054002953** get_address_of_creamText_32() { return &___creamText_32; }
	inline void set_creamText_32(StringU5BU2CU5D_t4054002953* value)
	{
		___creamText_32 = value;
		Il2CppCodeGenWriteBarrier(&___creamText_32, value);
	}

	inline static int32_t get_offset_of_isOkDone_33() { return static_cast<int32_t>(offsetof(ScoreManager_Lobby_t3853030226, ___isOkDone_33)); }
	inline bool get_isOkDone_33() const { return ___isOkDone_33; }
	inline bool* get_address_of_isOkDone_33() { return &___isOkDone_33; }
	inline void set_isOkDone_33(bool value)
	{
		___isOkDone_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
