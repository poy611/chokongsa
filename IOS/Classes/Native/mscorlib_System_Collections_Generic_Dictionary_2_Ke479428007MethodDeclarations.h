﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1829048833MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1489612102(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t479428007 *, Dictionary_2_t3147635852 *, const MethodInfo*))KeyCollection__ctor_m1560804596_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1391065168(__this, ___item0, method) ((  void (*) (KeyCollection_t479428007 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3110605794_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1190261191(__this, method) ((  void (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1661323737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m95448634(__this, ___item0, method) ((  bool (*) (KeyCollection_t479428007 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m166641128_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3218548255(__this, ___item0, method) ((  bool (*) (KeyCollection_t479428007 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3978398029_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2716159683(__this, method) ((  Il2CppObject* (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m628159701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3872154553(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t479428007 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3507663051_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2475610868(__this, method) ((  Il2CppObject * (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3777142918_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m117915419(__this, method) ((  bool (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4109392713_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3384630669(__this, method) ((  bool (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3399826235_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m553116217(__this, method) ((  Il2CppObject * (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1842027111_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2953686011(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t479428007 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1199977385_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1425651742(__this, method) ((  Enumerator_t3762571906  (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_GetEnumerator_m1756222668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Count()
#define KeyCollection_get_Count_m491207635(__this, method) ((  int32_t (*) (KeyCollection_t479428007 *, const MethodInfo*))KeyCollection_get_Count_m720428929_gshared)(__this, method)
