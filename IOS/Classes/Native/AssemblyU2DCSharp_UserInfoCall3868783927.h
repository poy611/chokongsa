﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserInfoCall
struct  UserInfoCall_t3868783927  : public Il2CppObject
{
public:
	// System.String UserInfoCall::user_id
	String_t* ___user_id_0;
	// System.String UserInfoCall::start_num
	String_t* ___start_num_1;
	// System.String UserInfoCall::end_num
	String_t* ___end_num_2;

public:
	inline static int32_t get_offset_of_user_id_0() { return static_cast<int32_t>(offsetof(UserInfoCall_t3868783927, ___user_id_0)); }
	inline String_t* get_user_id_0() const { return ___user_id_0; }
	inline String_t** get_address_of_user_id_0() { return &___user_id_0; }
	inline void set_user_id_0(String_t* value)
	{
		___user_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_id_0, value);
	}

	inline static int32_t get_offset_of_start_num_1() { return static_cast<int32_t>(offsetof(UserInfoCall_t3868783927, ___start_num_1)); }
	inline String_t* get_start_num_1() const { return ___start_num_1; }
	inline String_t** get_address_of_start_num_1() { return &___start_num_1; }
	inline void set_start_num_1(String_t* value)
	{
		___start_num_1 = value;
		Il2CppCodeGenWriteBarrier(&___start_num_1, value);
	}

	inline static int32_t get_offset_of_end_num_2() { return static_cast<int32_t>(offsetof(UserInfoCall_t3868783927, ___end_num_2)); }
	inline String_t* get_end_num_2() const { return ___end_num_2; }
	inline String_t** get_address_of_end_num_2() { return &___end_num_2; }
	inline void set_end_num_2(String_t* value)
	{
		___end_num_2 = value;
		Il2CppCodeGenWriteBarrier(&___end_num_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
