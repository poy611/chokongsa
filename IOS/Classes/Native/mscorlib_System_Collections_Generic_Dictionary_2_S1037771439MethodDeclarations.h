﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>
struct ShimEnumerator_t1037771439;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.ReadType>
struct Dictionary_2_t1321993412;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1364344425_gshared (ShimEnumerator_t1037771439 * __this, Dictionary_2_t1321993412 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1364344425(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1037771439 *, Dictionary_2_t1321993412 *, const MethodInfo*))ShimEnumerator__ctor_m1364344425_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3968126556_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3968126556(__this, method) ((  bool (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_MoveNext_m3968126556_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3080888782_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3080888782(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_get_Entry_m3080888782_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m80274573_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m80274573(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_get_Key_m80274573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m882889695_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m882889695(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_get_Value_m882889695_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3773130407_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3773130407(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_get_Current_m3773130407_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Newtonsoft.Json.ReadType>::Reset()
extern "C"  void ShimEnumerator_Reset_m896748347_gshared (ShimEnumerator_t1037771439 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m896748347(__this, method) ((  void (*) (ShimEnumerator_t1037771439 *, const MethodInfo*))ShimEnumerator_Reset_m896748347_gshared)(__this, method)
