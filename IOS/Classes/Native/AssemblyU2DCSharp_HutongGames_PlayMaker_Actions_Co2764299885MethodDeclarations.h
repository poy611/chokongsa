﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Comment
struct Comment_t2764299885;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Comment::.ctor()
extern "C"  void Comment__ctor_m4096568169 (Comment_t2764299885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Comment::Reset()
extern "C"  void Comment_Reset_m1743001110 (Comment_t2764299885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Comment::OnEnter()
extern "C"  void Comment_OnEnter_m2003695872 (Comment_t2764299885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
