﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A
struct U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::.ctor()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A__ctor_m1583786699 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2332891687 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_System_Collections_IEnumerator_get_Current_m3488500667 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::MoveNext()
extern "C"  bool U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_MoveNext_m3994035785 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::Dispose()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Dispose_m1498878024 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Serving/<ShowmetheCoffeSizeUpCo>c__Iterator2A::Reset()
extern "C"  void U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_Reset_m3525186936 (U3CShowmetheCoffeSizeUpCoU3Ec__Iterator2A_t3955755680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
