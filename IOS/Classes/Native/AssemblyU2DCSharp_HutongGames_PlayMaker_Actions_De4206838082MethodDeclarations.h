﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugEnum
struct DebugEnum_t4206838082;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugEnum::.ctor()
extern "C"  void DebugEnum__ctor_m2048265780 (DebugEnum_t4206838082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugEnum::Reset()
extern "C"  void DebugEnum_Reset_m3989666017 (DebugEnum_t4206838082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugEnum::OnEnter()
extern "C"  void DebugEnum_OnEnter_m680121611 (DebugEnum_t4206838082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
