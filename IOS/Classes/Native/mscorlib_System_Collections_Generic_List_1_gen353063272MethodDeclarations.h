﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::.ctor()
#define List_1__ctor_m638064651(__this, method) ((  void (*) (List_1_t353063272 *, const MethodInfo*))List_1__ctor_m1457888136_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1354620009(__this, ___collection0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1384868247_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::.ctor(System.Int32)
#define List_1__ctor_m2641271463(__this, ___capacity0, method) ((  void (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::.cctor()
#define List_1__cctor_m1732042775(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1112417760(__this, method) ((  Il2CppObject* (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m809888686(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t353063272 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1917196477(__this, method) ((  Il2CppObject * (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2188870944(__this, ___item0, method) ((  int32_t (*) (List_1_t353063272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m592936864(__this, ___item0, method) ((  bool (*) (List_1_t353063272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1915759480(__this, ___item0, method) ((  int32_t (*) (List_1_t353063272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m354157547(__this, ___index0, ___item1, method) ((  void (*) (List_1_t353063272 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m537437021(__this, ___item0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1172334497(__this, method) ((  bool (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3271570364(__this, method) ((  bool (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4133019758(__this, method) ((  Il2CppObject * (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3663685391(__this, method) ((  bool (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1680570890(__this, method) ((  bool (*) (List_1_t353063272 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m52850293(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1656938754(__this, ___index0, ___value1, method) ((  void (*) (List_1_t353063272 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Add(T)
#define List_1_Add_m332425467(__this, ___item0, method) ((  void (*) (List_1_t353063272 *, FunctionCall_t3279845016 *, const MethodInfo*))List_1_Add_m1152248952_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2132051748(__this, ___newCount0, method) ((  void (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2684780099(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t353063272 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3531558434(__this, ___collection0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2792530658(__this, ___enumerable0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4022896245(__this, ___collection0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::AsReadOnly()
#define List_1_AsReadOnly_m48853872(__this, method) ((  ReadOnlyCollection_1_t541955256 * (*) (List_1_t353063272 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Clear()
#define List_1_Clear_m1495429761(__this, method) ((  void (*) (List_1_t353063272 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Contains(T)
#define List_1_Contains_m3694819699(__this, ___item0, method) ((  bool (*) (List_1_t353063272 *, FunctionCall_t3279845016 *, const MethodInfo*))List_1_Contains_m786374326_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CopyTo(T[])
#define List_1_CopyTo_m2229482110(__this, ___array0, method) ((  void (*) (List_1_t353063272 *, FunctionCallU5BU5D_t3031147529*, const MethodInfo*))List_1_CopyTo_m3016810556_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m799696665(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t353063272 *, FunctionCallU5BU5D_t3031147529*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m2094884237(__this, ___match0, method) ((  FunctionCall_t3279845016 * (*) (List_1_t353063272 *, Predicate_1_t2890901899 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2777129962(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2890901899 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2310907591(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t353063272 *, int32_t, int32_t, Predicate_1_t2890901899 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::GetEnumerator()
#define List_1_GetEnumerator_m2264582812(__this, method) ((  Enumerator_t372736042  (*) (List_1_t353063272 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::IndexOf(T)
#define List_1_IndexOf_m2985933221(__this, ___item0, method) ((  int32_t (*) (List_1_t353063272 *, FunctionCall_t3279845016 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1431966832(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t353063272 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m546063849(__this, ___index0, method) ((  void (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Insert(System.Int32,T)
#define List_1_Insert_m3736694736(__this, ___index0, ___item1, method) ((  void (*) (List_1_t353063272 *, int32_t, FunctionCall_t3279845016 *, const MethodInfo*))List_1_Insert_m3135247846_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1330159685(__this, ___collection0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Remove(T)
#define List_1_Remove_m2358573614(__this, ___item0, method) ((  bool (*) (List_1_t353063272 *, FunctionCall_t3279845016 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3893069736(__this, ___match0, method) ((  int32_t (*) (List_1_t353063272 *, Predicate_1_t2890901899 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1610547606(__this, ___index0, method) ((  void (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4092449316_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1359016377(__this, ___index0, ___count1, method) ((  void (*) (List_1_t353063272 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Reverse()
#define List_1_Reverse_m4251610390(__this, method) ((  void (*) (List_1_t353063272 *, const MethodInfo*))List_1_Reverse_m2062055293_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Sort()
#define List_1_Sort_m93837900(__this, method) ((  void (*) (List_1_t353063272 *, const MethodInfo*))List_1_Sort_m3444130117_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3778441496(__this, ___comparer0, method) ((  void (*) (List_1_t353063272 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2534925727(__this, ___comparison0, method) ((  void (*) (List_1_t353063272 *, Comparison_1_t1996206203 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::ToArray()
#define List_1_ToArray_m2043105647(__this, method) ((  FunctionCallU5BU5D_t3031147529* (*) (List_1_t353063272 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::TrimExcess()
#define List_1_TrimExcess_m3814002917(__this, method) ((  void (*) (List_1_t353063272 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::get_Capacity()
#define List_1_get_Capacity_m631920469(__this, method) ((  int32_t (*) (List_1_t353063272 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3637207862(__this, ___value0, method) ((  void (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::get_Count()
#define List_1_get_Count_m3913365473(__this, method) ((  int32_t (*) (List_1_t353063272 *, const MethodInfo*))List_1_get_Count_m2222238344_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::get_Item(System.Int32)
#define List_1_get_Item_m1794852308(__this, ___index0, method) ((  FunctionCall_t3279845016 * (*) (List_1_t353063272 *, int32_t, const MethodInfo*))List_1_get_Item_m850128002_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m2180578471(__this, ___index0, ___value1, method) ((  void (*) (List_1_t353063272 *, int32_t, FunctionCall_t3279845016 *, const MethodInfo*))List_1_set_Item_m2510829103_gshared)(__this, ___index0, ___value1, method)
