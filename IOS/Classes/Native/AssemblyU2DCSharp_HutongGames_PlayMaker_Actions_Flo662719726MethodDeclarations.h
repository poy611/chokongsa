﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatOperator
struct FloatOperator_t662719726;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatOperator::.ctor()
extern "C"  void FloatOperator__ctor_m2222698504 (FloatOperator_t662719726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::Reset()
extern "C"  void FloatOperator_Reset_m4164098741 (FloatOperator_t662719726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnEnter()
extern "C"  void FloatOperator_OnEnter_m806244831 (FloatOperator_t662719726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnUpdate()
extern "C"  void FloatOperator_OnUpdate_m2652312644 (FloatOperator_t662719726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::DoFloatOperator()
extern "C"  void FloatOperator_DoFloatOperator_m4708859 (FloatOperator_t662719726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
