﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_t4118229972;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JToken[]
struct JTokenU5BU5D_t2853253222;

#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty/JPropertyList::GetEnumerator()
extern "C"  Il2CppObject* JPropertyList_GetEnumerator_m168751194 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JProperty/JPropertyList::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JPropertyList_System_Collections_IEnumerable_GetEnumerator_m2030812309 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::Add(Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyList_Add_m673274489 (JPropertyList_t4118229972 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::Clear()
extern "C"  void JPropertyList_Clear_m2657932285 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty/JPropertyList::Contains(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JPropertyList_Contains_m1097332943 (JPropertyList_t4118229972 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern "C"  void JPropertyList_CopyTo_m1492218961 (JPropertyList_t4118229972 * __this, JTokenU5BU5D_t2853253222* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty/JPropertyList::Remove(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JPropertyList_Remove_m2400528842 (JPropertyList_t4118229972 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JProperty/JPropertyList::get_Count()
extern "C"  int32_t JPropertyList_get_Count_m1627443154 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty/JPropertyList::get_IsReadOnly()
extern "C"  bool JPropertyList_get_IsReadOnly_m3537619665 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JProperty/JPropertyList::IndexOf(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JPropertyList_IndexOf_m1727076309 (JPropertyList_t4118229972 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyList_Insert_m227792928 (JPropertyList_t4118229972 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::RemoveAt(System.Int32)
extern "C"  void JPropertyList_RemoveAt_m623769754 (JPropertyList_t4118229972 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList::get_Item(System.Int32)
extern "C"  JToken_t3412245951 * JPropertyList_get_Item_m1983260842 (JPropertyList_t4118229972 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JPropertyList_set_Item_m4078634295 (JPropertyList_t4118229972 * __this, int32_t ___index0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty/JPropertyList::.ctor()
extern "C"  void JPropertyList__ctor_m956831698 (JPropertyList_t4118229972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
