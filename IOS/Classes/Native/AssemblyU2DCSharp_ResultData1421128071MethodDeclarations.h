﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultData
struct ResultData_t1421128071;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultData::.ctor()
extern "C"  void ResultData__ctor_m3951603444 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
