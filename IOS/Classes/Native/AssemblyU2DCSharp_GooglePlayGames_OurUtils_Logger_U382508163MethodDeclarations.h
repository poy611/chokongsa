﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B
struct U3CwU3Ec__AnonStorey3B_t82508163;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B::.ctor()
extern "C"  void U3CwU3Ec__AnonStorey3B__ctor_m1802611320 (U3CwU3Ec__AnonStorey3B_t82508163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger/<w>c__AnonStorey3B::<>m__A()
extern "C"  void U3CwU3Ec__AnonStorey3B_U3CU3Em__A_m4277264066 (U3CwU3Ec__AnonStorey3B_t82508163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
