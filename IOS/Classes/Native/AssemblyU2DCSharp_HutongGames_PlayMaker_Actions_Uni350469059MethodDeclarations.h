﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.UnityAdsShowAd
struct UnityAdsShowAd_t350469059;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.UnityAdsShowAd::.ctor()
extern "C"  void UnityAdsShowAd__ctor_m1805834243 (UnityAdsShowAd_t350469059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UnityAdsShowAd::Reset()
extern "C"  void UnityAdsShowAd_Reset_m3747234480 (UnityAdsShowAd_t350469059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
