﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreManager_Lobby/<TimerCheck>c__Iterator1E
struct U3CTimerCheckU3Ec__Iterator1E_t3376731881;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::.ctor()
extern "C"  void U3CTimerCheckU3Ec__Iterator1E__ctor_m310211026 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<TimerCheck>c__Iterator1E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2460934090 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScoreManager_Lobby/<TimerCheck>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerCheckU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3114121054 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScoreManager_Lobby/<TimerCheck>c__Iterator1E::MoveNext()
extern "C"  bool U3CTimerCheckU3Ec__Iterator1E_MoveNext_m2453101770 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::Dispose()
extern "C"  void U3CTimerCheckU3Ec__Iterator1E_Dispose_m1658335631 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreManager_Lobby/<TimerCheck>c__Iterator1E::Reset()
extern "C"  void U3CTimerCheckU3Ec__Iterator1E_Reset_m2251611263 (U3CTimerCheckU3Ec__Iterator1E_t3376731881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
