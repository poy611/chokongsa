﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseX
struct GetMouseX_t738295447;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseX::.ctor()
extern "C"  void GetMouseX__ctor_m795541631 (GetMouseX_t738295447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::Reset()
extern "C"  void GetMouseX_Reset_m2736941868 (GetMouseX_t738295447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnEnter()
extern "C"  void GetMouseX_OnEnter_m3698024598 (GetMouseX_t738295447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnUpdate()
extern "C"  void GetMouseX_OnUpdate_m2103172205 (GetMouseX_t738295447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::DoGetMouseX()
extern "C"  void GetMouseX_DoGetMouseX_m2266641435 (GetMouseX_t738295447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
