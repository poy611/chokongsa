﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t1075959796;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ba4105009303.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel284580066.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugBool
struct  DebugBool_t4206749483  : public BaseLogAction_t4105009303
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugBool::logLevel
	int32_t ___logLevel_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DebugBool::boolVariable
	FsmBool_t1075959796 * ___boolVariable_13;

public:
	inline static int32_t get_offset_of_logLevel_12() { return static_cast<int32_t>(offsetof(DebugBool_t4206749483, ___logLevel_12)); }
	inline int32_t get_logLevel_12() const { return ___logLevel_12; }
	inline int32_t* get_address_of_logLevel_12() { return &___logLevel_12; }
	inline void set_logLevel_12(int32_t value)
	{
		___logLevel_12 = value;
	}

	inline static int32_t get_offset_of_boolVariable_13() { return static_cast<int32_t>(offsetof(DebugBool_t4206749483, ___boolVariable_13)); }
	inline FsmBool_t1075959796 * get_boolVariable_13() const { return ___boolVariable_13; }
	inline FsmBool_t1075959796 ** get_address_of_boolVariable_13() { return &___boolVariable_13; }
	inline void set_boolVariable_13(FsmBool_t1075959796 * value)
	{
		___boolVariable_13 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariable_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
