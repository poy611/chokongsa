﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntChanged
struct IntChanged_t778990893;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntChanged::.ctor()
extern "C"  void IntChanged__ctor_m822731225 (IntChanged_t778990893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::Reset()
extern "C"  void IntChanged_Reset_m2764131462 (IntChanged_t778990893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnEnter()
extern "C"  void IntChanged_OnEnter_m4057420656 (IntChanged_t778990893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnUpdate()
extern "C"  void IntChanged_OnUpdate_m359548115 (IntChanged_t778990893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
