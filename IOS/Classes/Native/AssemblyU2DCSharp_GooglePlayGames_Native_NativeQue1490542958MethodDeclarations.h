﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey5F
struct U3CFetchU3Ec__AnonStorey5F_t1490542958;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
struct FetchResponse_t3559403642;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Q3559403642.h"

// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey5F::.ctor()
extern "C"  void U3CFetchU3Ec__AnonStorey5F__ctor_m2569744829 (U3CFetchU3Ec__AnonStorey5F_t1490542958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey5F::<>m__3C(GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse)
extern "C"  void U3CFetchU3Ec__AnonStorey5F_U3CU3Em__3C_m3943772162 (U3CFetchU3Ec__AnonStorey5F_t1490542958 * __this, FetchResponse_t3559403642 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
