﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartFirst/<BlackBlink>c__Iterator2E
struct U3CBlackBlinkU3Ec__Iterator2E_t2642540702;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StartFirst/<BlackBlink>c__Iterator2E::.ctor()
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E__ctor_m3552183229 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StartFirst/<BlackBlink>c__Iterator2E::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlackBlinkU3Ec__Iterator2E_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3755503551 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StartFirst/<BlackBlink>c__Iterator2E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlackBlinkU3Ec__Iterator2E_System_Collections_IEnumerator_get_Current_m1367855443 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StartFirst/<BlackBlink>c__Iterator2E::MoveNext()
extern "C"  bool U3CBlackBlinkU3Ec__Iterator2E_MoveNext_m2286416639 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartFirst/<BlackBlink>c__Iterator2E::Dispose()
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E_Dispose_m3342333114 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartFirst/<BlackBlink>c__Iterator2E::Reset()
extern "C"  void U3CBlackBlinkU3Ec__Iterator2E_Reset_m1198616170 (U3CBlackBlinkU3Ec__Iterator2E_t2642540702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
