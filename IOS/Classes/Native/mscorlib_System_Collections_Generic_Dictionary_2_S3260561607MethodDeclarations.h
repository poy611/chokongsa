﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>
struct ShimEnumerator_t3260561607;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3544783580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2205601367_gshared (ShimEnumerator_t3260561607 * __this, Dictionary_2_t3544783580 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2205601367(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3260561607 *, Dictionary_2_t3544783580 *, const MethodInfo*))ShimEnumerator__ctor_m2205601367_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1162359982_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1162359982(__this, method) ((  bool (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_MoveNext_m1162359982_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2001470908_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2001470908(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_get_Entry_m2001470908_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1236691963_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1236691963(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_get_Key_m1236691963_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4098439117_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m4098439117(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_get_Value_m4098439117_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1539671829_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1539671829(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_get_Current_m1539671829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.RaycastHit2D>::Reset()
extern "C"  void ShimEnumerator_Reset_m3780630057_gshared (ShimEnumerator_t3260561607 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3780630057(__this, method) ((  void (*) (ShimEnumerator_t3260561607 *, const MethodInfo*))ShimEnumerator_Reset_m3780630057_gshared)(__this, method)
