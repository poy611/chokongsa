﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class YoutubeEasyMovieTexture : MonoBehaviour
{

	public GameObject Player;
	public MediaPlayerCtrl _movie;
	public GameObject LearnPop;
	public Slider _slider;
	public Text _text;
	int totalLength;
	float moveSpeed;
	void Start () {
		SoundManage.instance.OffBgm ();
	
	}
	public void BackButtonClick(){
		UnityEngine.SceneManagement.SceneManager.LoadScene ("LEARNPAGE");
	}
	public void ButtonClickYoutube(string str)
	{
		SoundManage.instance.efxButtonOn ();
		LearnPop.SetActive (false);
		Player.SetActive (true);
		_movie.m_strFileName = YoutubeVideo.Instance.RequestVideo(str,360);
		_movie.Play ();

		StartCoroutine (CheckPlay());
	
	}
	public void StopButton(){
		_movie.Stop ();
		Player.SetActive (false);
		LearnPop.SetActive (true);
		_slider.value = 0;
	}
	public void PlayOrPause(){
		if (_movie.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) {
			_movie.Pause ();
		} else {
			_movie.Play ();
		}
	}
	public void MouseDown(){
		_movie.Pause ();
	}
	public void MouseUp(){
		_movie.SeekTo ((int)_slider.value);
		_movie.Play ();
	}
	IEnumerator CheckPlay(){
		while (true) {
			if (_movie.GetCurrentState () == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) {
				totalLength = _movie.GetDuration ();
				_slider.maxValue = (float)totalLength;
				_text.text = totalLength.ToString ();
				yield break;
			}
			yield return null;
		}
	}
	void FixedUpdate(){
		if (_movie.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING) {
			_slider.value = _movie.GetSeekPosition();
		}
	}
}
