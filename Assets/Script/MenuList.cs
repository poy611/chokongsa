﻿using UnityEngine;
using System.Collections;

public class MenuList : MonoBehaviour {

	public string esspresso = "에스프레소";
	public string americano = "아메리카노";
	public string ice_americano = "아이스 아메리카노";
	public string cafelatte = "카페 라떼";
	public string ice_cafelatte = "아이스 카페라떼";
	public string vanillalatte = "바닐라 라떼";
	public string ice_vanillalatte = "아이스 바닐라 라떼";
	public string hazelnut = "헤이즐럿";
	public string ice_hazelnut = "아이스 헤이즐럿";
	public string cafemocca = "카페 모카";
	public string ice_cafemoca = "아이스 카페 모카";
	public string caramel = "카라멜 마끼아또";
	public string ice_caramel = "아이스 카라멜 마끼아또";
	public string caramelfrappuccino = "카라멜 프라푸치노";
	public string mochafrappuccino = "모카 프라푸치노";
	public string hazelnutfrappuccino = "헤이즐럿 프라푸치노";
	public string GetMenuName(int menuNum)
	{
		switch (menuNum+1)
		{
		case 1:
			return esspresso;
		case 2:
			return americano;
		case 3:
			return ice_americano;
		case 4:
			return cafelatte;
		case 5:
			return ice_cafelatte;
		case 6:
			return vanillalatte;
		case 7:
			return ice_vanillalatte;
		case 8:
			return hazelnut;
		case 9:
			return ice_hazelnut;
		case 10:
			return cafemocca;
		case 11:
			return ice_cafemoca;
		case 12:
			return caramel;
		case 13:
			return ice_caramel;
		case 14:
			return caramelfrappuccino;
		case 15:
			return mochafrappuccino;
		case 16:
			return hazelnutfrappuccino;
		}
		return "";
	}
	public string GetMenuInPopup(int menuNum){
		switch (menuNum+1)
		{
		case 1:
			return esspresso;
		case 2:
			return americano;
		case 3:
			return ice_americano;
		case 4:
			return cafelatte;
		case 5:
			return ice_cafelatte;
		case 6:
			return vanillalatte;
		case 7:
			return ice_vanillalatte;
		case 8:
			return hazelnut;
		case 9:
			return ice_hazelnut;
		case 10:
			return cafemocca;
		case 11:
			return ice_cafemoca;
		case 12:
			return caramel;
		case 13:
			return ice_caramel;
		case 14:
			return caramelfrappuccino;
		case 15:
			return mochafrappuccino;
		case 16:
			return hazelnutfrappuccino;
		}
		return "";
	}
}
