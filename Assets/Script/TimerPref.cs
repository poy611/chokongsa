﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
public class TimerPref : MonoBehaviour {

	public Text timerText;
	public TextMesh timerTextMesh;
	int nowNum;
	string nowScene;
	StructforMinigame stuff;
	ConstforMinigame cuff;
	public Image img;
	// Update is called once per frame
	void Start(){
		nowNum = SceneManager.GetActiveScene ().buildIndex - 1;
		stuff = StructforMinigame.getSingleton(nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum,MainUserInfo.GetInstance.grade-1);
		if (timerText != null) {
			timerText.text = "" + cuff.Limittime;
		} else if (timerTextMesh != null) {
			timerTextMesh.text = "" + cuff.Limittime;
		}
		img.fillAmount = (float)(180 - cuff.Limittime) * 0.0055f;
	
		 
	}
	void Update(){
		if (timerTextMesh == null) {
			if (img.fillAmount <= 0.99) {
				if (timerText.text != null || timerText.text != "") {
					int nowTime = Convert.ToInt32 (timerText.text);
					img.fillAmount = (float)(180 - nowTime) * 0.0055f;
				}
			}
		} else {
			if (img.fillAmount <= 0.99) {
				if (timerTextMesh.text != null || timerTextMesh.text != "") {
					int nowTime = Convert.ToInt32 (timerTextMesh.text);
					img.fillAmount = (float)(180 - nowTime) * 0.0055f;
				}
			}
		}
	}

}
