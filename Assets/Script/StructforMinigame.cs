﻿using UnityEngine;
using System;
using System.Collections;
using System.Diagnostics;

static class SFM
{
	public const int NUM = 20;
}
public class StructforMinigame {
	private static StructforMinigame[] stuff = new StructforMinigame[SFM.NUM];
	Stopwatch sw;

	string userId;
	int gameId;
	int level;
	string recordDate;
	float score;
	float playtime;
	int misscount;
	float errorRate;


	private StructforMinigame()
	{
		for (int i = 0; i < SFM.NUM; i++) {
			stuff [i] = new StructforMinigame (i);
		}
	}
	private StructforMinigame (int i)
	{
		sw = new Stopwatch ();
		userId = "abcdisnothing1234"; //null
		gameId = 1;
		level=1;
		recordDate="";
		score=0.0f;
		playtime=0f;
		misscount=0;
		errorRate=0f;

	}


	public string Userid{
		get{
			return userId;
		}
		set{
			userId = value;
		}
	}
	public int Gameid{
		get{
			return gameId;
		}
		set{
			gameId	= Math.Max (0, value);

			/*
			if (value > 0 && value < 1000)
				gameId = value;
			else
				gameId = 0;
			*/
		}
	}
	public int Level {
		get {
			return level;
		}
		set {
			level	= Math.Max (1, value);

			/*
			if (value > 0 && value < 6)
				level = value;
			else
				level = 1;
			*/
		}
	}
	public string RecordDate{
		get{
			return recordDate;
		}
		set{
			recordDate = value;
		}
	}
	public float Score{
		get{
			return score;
		}
		set{
			score	= Math.Max (0, value);


			/*
			if (value >= 0 && value <=100)
				score = value;
			else
				score= -1;
			*/
		}
	}
	public float Playtime{
		get{
			return playtime;
		}
		set{
			playtime	= Math.Max (0, value);


			/*
			if (value >= 0 && value <=999)
				playtime = value;
			else
				playtime = -1;
			*/

		}
	}

	public int Misscount {
		get {
			return misscount;
		}
		set {
			misscount	= Math.Max (0, value);


			/*
			if (value > -1 && value < 1000)
				misscount = value;
			else
				misscount = -1;
			*/
		}
	}
	public float ErrorRate{
		get{
			return errorRate;
		}
		set{
			errorRate	= Math.Max (0, Mathf.Min (100, value));

/*
 			if (value >= 0 && value <=100)
				errorRate = value;
			else
				errorRate = -1;
*/			
		}
	}

	public Stopwatch Sw{
		get{
			return sw;
		}
	}
	public static StructforMinigame getSingleton(int index)
	{
		/*for (int i = 0; i < 5; i++) {
			if (stuff [i] == null)
				stuff [i] = new StructforMinigame (i);
			}*/
		if (index == 90)
			index = 4;
		if (stuff [index] == null)
			stuff [index] = new StructforMinigame (index);

		return stuff[index];
	}
	public static StructforMinigame[] getSingleton()
	{
		for (int i = 0; i < SFM.NUM; i++) {
			if (stuff [i] == null)
				stuff [i] = new StructforMinigame (i);
			}

		return stuff;
	}
}
