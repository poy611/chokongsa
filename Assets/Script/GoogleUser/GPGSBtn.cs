﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GPGSBtn : MonoBehaviour
{
	public Text Login_Label = null;
	public Text User_Label = null;
	public RawImage User_Texture = null;
	public Texture2D _img;
	private bool isSetting=false;
	private bool autoLogin=false;
	public GameObject bt;
	public GameObject loginBt;
	public GameObject exitBt;
	public Text tempText;
	void Awake()
	{
		if (!PlayerPrefs.HasKey ("LOGINLOG")) {
			PlayerPrefs.SetInt ("LOGINLOG",0);
		}
		if(PlayerPrefs.GetInt ("BACKBUTTON")!=1){
			GPGSMng.GetInstance.InitializeGPGS (); // 초기화
			SetOnInit ();
		}
		PlayerPrefs.SetInt ("BACKBUTTON", 0);
		Debug.Log ("GPGSBtn Awake()");
		StartCoroutine (LoginChecker ());

	}
	void SetOnInit(){
		if (PlayerPrefs.GetInt ("GOOGLELOGINMNG") == 1) {
			ClickEvent ();
		}
	}
	IEnumerator LoginChecker(){
		while (true) {
			if (PlayerPrefs.GetInt ("LOGINLOG") == 0) {
				bt.SetActive (false);
				loginBt.SetActive (true);
			}
			else {
				bt.SetActive (true);
				loginBt.SetActive (false);
				exitBt.SetActive (true);
				if (GPGSMng.GetInstance.bLogin == false) {
					Login_Label.text = "Login";
					User_Label.text = "Guest";
					User_Texture.texture = _img;
				} else {
					Login_Label.text = "Logout";
					SettingUser ();
				}
			}
			yield return new WaitForSeconds (0.5f);
		}
	}

	IEnumerator LoginCheckerNet()
	{
		while (true) {
			if (GPGSMng.GetInstance.bLogin == false) {
				
			} else {
				MainUserInfo.GetInstance.userId = GPGSMng.GetInstance.GetIDGPGS();
				MainUserInfo.GetInstance.userName= GPGSMng.GetInstance.GetNameGPGS();
				NetworkMng.GetInstance.RequestLogin(GPGSMng.GetInstance.GetIDGPGS(),GPGSMng.GetInstance.GetNameGPGS()); // 서버에 저장
				PlayerPrefs.SetInt("GOOGLELOGINMNG",1);
				GPGSMng.GetInstance.bLogin=true;
				PlayerPrefs.SetString ("USER_ID",MainUserInfo.GetInstance.userId);
				if (PlayerPrefs.GetInt ("SD_GRADE") == 0 && PlayerPrefs.GetInt ("SD_GAME_ID") == 0) {
					Debug.Log ("REAL FIRST STARTER");
				} else {
					NetworkMng.GetInstance.RequestSaveGameData (PlayerPrefs.GetInt ("SD_GAME_ID"),
					PlayerPrefs.GetInt ("SD_TRIAL_TH"),
					PlayerPrefs.GetInt ("SD_GRADE"),
					PlayerPrefs.GetInt ("SD_GUEST_TH"));
				}
				PlayerPrefs.SetInt ("LOGINLOG",1);

				yield break;
			}
			yield return new WaitForSeconds (0.5f);
		}
	}

	public void ClickEvent()
	{
		SoundManage.instance.efxButtonOn ();

		if(GPGSMng.GetInstance.bLogin == false)
		{
			GPGSMng.GetInstance.LoginGPGS(); // 로그인

			StartCoroutine(LoginCheckerNet());

			/*PlayerPrefs.SetInt ("LOGINLOG",1);
			MainUserInfo.GetInstance.userId = "zzzzz";//GPGSMng.GetInstance.GetIDGPGS();
			MainUserInfo.GetInstance.userName= "zzzzz";//GPGSMng.GetInstance.GetNameGPGS();
			NetworkMng.GetInstance.RequestLogin("zzzzz","zzzzz"); // 서버에 저장
			GPGSMng.GetInstance.bLogin=true;
			PlayerPrefs.SetString ("USER_ID",MainUserInfo.GetInstance.userId);
			PlayerPrefs.SetInt("GOOGLELOGINMNG",1);
			if (PlayerPrefs.GetInt ("SD_GRADE") == 0 && PlayerPrefs.GetInt ("SD_GAME_ID") == 0) {

			} else {
				NetworkMng.GetInstance.RequestSaveGameData (PlayerPrefs.GetInt ("SD_GAME_ID"),
					PlayerPrefs.GetInt ("SD_TRIAL_TH"),
					PlayerPrefs.GetInt ("SD_GRADE"),
					PlayerPrefs.GetInt ("SD_GUEST_TH"));
			}
*/
		}
		else
		{
			GPGSMng.GetInstance.LogoutGPGS(); // 로그아웃
			GPGSMng.GetInstance.bLogin=false;
			PlayerPrefs.SetInt("GOOGLELOGINMNG",0);

		}
	}

	void SettingUser()
	{
		User_Label.text = GPGSMng.GetInstance.GetNameGPGS();
		User_Texture.texture = GPGSMng.GetInstance.GetImageGPGS();
	}

}
