﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Diagnostics;
public class ScoreManager_Temping : MonoBehaviour {
	int nowNum;
	string nowScene;
	StructforMinigame stuff;
	ConstforMinigame cuff;
	Stopwatch sw;
	PopupManagement pop;
	public Text timmer;
	public Sprite[] res;
	float ticks = 0 ;
	public float tempingLimitAmount;
	public float tempingAmount = 0;
	// Use this for initialization
	void Start () {
		nowNum = SceneManager.GetActiveScene ().buildIndex - 1;
		nowScene = SceneManager.GetActiveScene ().name;
		stuff = StructforMinigame.getSingleton (nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum,MainUserInfo.GetInstance.grade-1);
		sw = stuff.Sw;
		sw.Reset ();
		ticks = 0;
		pop = PopupManagement.p;
		pop.makeIt (PP.popupIndex.READY);
		float temp1 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().extractionRecipe [MainUserInfo.GetInstance.menu] [0] - 1] [0];
		float temp2 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().extractionRecipe [MainUserInfo.GetInstance.menu] [0] - 1] [1];
		tempingLimitAmount = (float)Math.Floor (UnityEngine.Random.Range (temp1, temp2 + 1));
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("WONDO GAUGE").transform.FindChild ("etc images").transform.FindChild("GOAL").transform.localPosition = new Vector3(0f,0.16f,0f);
		//ReadyAndStart ();
	}
	public void ReadyAndStart(){
		StartCoroutine (REandSt());
	}

	IEnumerator REandSt(){
		GameObject _obj;
		yield return new WaitForSecondsRealtime(0.5f);
		_obj=GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("READY").gameObject;
		_obj.SetActive (true);
		yield return new WaitForSecondsRealtime(2.0f);
		_obj.SetActive (false);
		_obj=GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("START").gameObject;
		_obj.SetActive (true);
		yield return new WaitForSecondsRealtime(2.0f);
		_obj.SetActive (false);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("TOUCH WHAT YOU WANT").gameObject.SetActive(true);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("Touch").gameObject.SetActive(true);
		StartCoroutine (TimerCheck());
		sw.Start ();
		//Timer Play!
		yield break;
	}
	IEnumerator TimerCheck(){
		while(true){
			ticks = (sw.ElapsedMilliseconds+Time.deltaTime)/1000;
			int it=(int)(cuff.Limittime-Math.Round(ticks,0));
			timmer.text = it.ToString();
			if (it <= 0) {
				StartCoroutine (TimeEnd ());
				yield break;
			}
			yield return null;
		}
	}
	IEnumerator TimeEnd(){
		sw.Stop ();
		yield return new WaitForSeconds(2);
		ticks = 0;
		stuff.Playtime = cuff.Limittime;
		pop.makeIt(PP.popupIndex.JUDGE);
		while(pop.PopupObject != null)
			yield return new  WaitForSecondsRealtime(.1f);

	}
	public void Confirm(){
		float timmerMinus = 0;
		float errorMinus = 0;
		float correctRate = 0;

		stuff.Gameid = nowNum + 1;
		stuff.Level = MainUserInfo.GetInstance.grade;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		//sw.Reset ();
		stuff.Playtime = ticks;
		timmerMinus = Math.Max (0, Math.Min (1, (stuff.Playtime - cuff.Besttime) / (cuff.Limittime - cuff.Besttime)));
		stuff.ErrorRate = Math.Abs ((tempingAmount-tempingLimitAmount)/tempingLimitAmount*100);
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate - cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));
		stuff.Score = 100 - (timmerMinus + errorMinus) * 50;
		pop.makeIt (PP.popupIndex.JUDGE);

	}
	public void EndOfTemping(){
		sw.Stop ();
		GameObject _obj =  GameObject.Find ("NEW UI Canvas").transform.FindChild ("RESULT WINDOW").gameObject;
		_obj.transform.FindChild("BG").transform.FindChild("Data Info").transform.FindChild ("Now_G_01").GetComponent<Text>().text = ""+(int)tempingAmount+"kg";
		_obj.transform.FindChild("BG").transform.FindChild("Data Info").transform.FindChild ("Now_G_02").GetComponent<Text>().text = ""+(int)tempingAmount+"kg";
		_obj.transform.FindChild("BG").transform.FindChild("Data Info").transform.FindChild ("Need G").GetComponent<Text>().text = ""+(int)tempingLimitAmount+"kg";
		float errorRate = 100.0f - Math.Abs ((tempingAmount - tempingLimitAmount) / tempingLimitAmount * 100);
		 res = Resources.LoadAll<Sprite> ("07_pop_result/judge");
		_obj.transform.FindChild("BG").transform.FindChild("Data Info").transform.FindChild("RESULT IMAGES").transform.FindChild("PERFECT").GetComponent<Image>().sprite = res[SwitchErrorNum(errorRate)];
		_obj.SetActive (true);
	}


	public void ButtonPassClick(){
		this.transform.FindChild ("Cafe_scene_complete").transform.FindChild ("Temper").gameObject.SetActive (false);
		this.transform.FindChild ("Cafe_scene_complete").transform.FindChild ("Porter_filter").gameObject.SetActive (false);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("RESULT WINDOW").gameObject.SetActive (false);
		GameObject.Find ("MAIN OBJ").transform.FindChild ("TEMPING OBJs").transform.FindChild("Porter_filter_TEMP").gameObject.SetActive(true);
	}


	public void ButtonRetryClick(){
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("RESULT WINDOW").gameObject.SetActive (false);
		sw.Start ();
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("Touch").transform.FindChild ("TempingTouch").gameObject.SetActive(true);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("WONDO GAUGE").gameObject.SetActive(true);
		TempingPointer _tempingMng = GameObject.Find ("MAIN OBJ").GetComponent<TempingPointer> ();
		_tempingMng.Init ();
	}


	public void EndOfExtracting(){
		Confirm ();
	}


	int SwitchErrorNum(float errorRate){
		if (errorRate >= 95) {
			return 4;
		} else if (errorRate >= 90) {
			return 3;
		} else if (errorRate >= 70) {
			return 2;
		} else if (errorRate >= 60) {
			return 1;
		} else {
			return 0;
		}
	}
}
