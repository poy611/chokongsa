﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using UnityEngine.UI;
using System.Collections.Generic;


public class ScoreManager_Lobby : MonoBehaviour {
	int nowNum;//SceneManager.GetActiveScene().buildIndex;
	Stopwatch sw;
	float ticks = 0;
	public StructforMinigame stuff;
	ConstforMinigame cuff;


	enum menu {ESSPRESSO,
		AMERICANOH,
		AMERICANOI,
		CAFELATTEH,
		CAFELATTEI,
		VANILLALATTEH,
		VANILLALATTEI,
		HAZELNUTH,
		HAZELNUTI,
		CAFFEMOCHAH,
		CARFEMOCHAI,
		MAGGIYATTOH,
		MAGGIYATTOI,
		CARAMELFRAPPUCCINO,
		MOCHAFRAPPUCCINO,
		HAZELNUTFRAPPUCCINO,
		CREAMIN,
		CREAMOUT,
		NOTHINGTOCHOSE
	};
	menu getMenu = menu.NOTHINGTOCHOSE;
	PopupManagement popup;
	public Text timmer;
	public Text MayItakeYourOrder;
	public Image ShowmetheCoffee;
	public Image desk;
	public List<Button> bts = new List<Button>();
	public List<Image> imgs = new List<Image> ();
	public GameObject Bars;
	public GameObject _Ready;
	public GameObject _Start;
	public LobbyTalkBoxAnimation lobby_ani;
	public LobbyDeskAnimation lobby_desk_ani;
	public int currentMenuPage = 0;
	public Button prevButton;
	public Button nextButton;
	public GameObject[] MenuButton;
	public GameObject creamButton;

	public int randomNum=0;
	public int randomMenu=0;
	public int randomHI=0;
	public int randomCream=0;
	int counterBars;
	string[] menuStr = new string[]{"에스프레소","아메리카노","아메리카노","카페라테","카페라테","바닐라 라떼","바닐라 라떼",
		"헤이즐넛","헤이즐넛","카페모카","카페모카","마끼야또","마끼야또",
		"카라멜 프라푸치노", "모카 프라푸치노", "헤이즐넛 프라푸치노"};
	string[] hiStr=new string[]{"따뜻하게","차갑게"};

	string[,] creamText = new string[,]{
		{"아! 휘핑 크링 가득 올려주세요.","휘핑 크림은 빼주세요."},
		{"휘핑 크림도 올려 주실래요?","휘핑 크림은 올리지 말아주세요."},
		{"휘핑 크림 추가요.","휘핑 크림 넣지 말아주세요."},
		{"휘핑 크림도 가득~","휘핑 크림은 안 되요~"},
		{"휘핑 크림도 좀 올려주세.","휘핑 크림 빼주게. 단건 질색이야."},

		{"휘핑 크림을 추가해주게나.","휘핑 크림은 좀 빼주게나."},

	};

	bool	isOkDone	= false;


	// Use this for initialization
	void Start () {
		
		desk.enabled = true;
		nowNum = SceneManager.GetActiveScene().buildIndex-1;
		stuff = StructforMinigame.getSingleton(nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum,MainUserInfo.GetInstance.grade-1);
		popup = PopupManagement.p;
		popup.makeIt (PP.popupIndex.READY);
		sw = stuff.Sw;
		sw.Reset ();
		ticks = 0;
		counterBars = 0;
		stuff.Misscount = 0;
		for (int i = 0; i < MenuButton.Length; i++) {
			Button[] temp = MenuButton [i].GetComponentsInChildren <Button>();
			Image[] temp1 = MenuButton [i].GetComponentsInChildren<Image> ();
			for (int j = 0; j < temp.Length; j++) {
				bts.Add (temp[j]);
				imgs.Add (temp1 [j]);
			}
		}
	SetOnInit ();
	}
	IEnumerator TimerCheck(){
		while(true){
			ticks = (sw.ElapsedMilliseconds+Time.deltaTime)/1000;
			int it=(int)(cuff.Limittime-Math.Round(ticks,0));
			timmer.text = it.ToString();
			if (it <= 0) {
				StartCoroutine (TimeEnd ());
				yield break;
			}
			yield return null;
		}
	}
	IEnumerator TimeEnd(){
		sw.Stop ();
		yield return new WaitForSeconds(2);
		buttonsWhite();
		ticks = 0;
		stuff.Playtime = cuff.Limittime;
		stuff.ErrorRate = 100.0f;
		popup.makeIt(PP.popupIndex.JUDGE);

		while(popup.PopupObject != null)
			yield return new  WaitForSecondsRealtime(.1f);
		
	}
	void makeActive()
	{
		counterBars = 0;
		for(int i =0 ;i<5;i++)
			Bars.transform.GetChild (i).gameObject.SetActive (true);
	}
	public void backButton()
	{
		SoundManage.instance.OffBgm ();
		SoundManage.instance.efxButtonOn ();
		sw.Stop ();
		sw.Reset ();
		if(GPGSMng.GetInstance.bLogin){
			PlayerPrefs.SetInt ("BACKBUTTON", 1);
			NetworkMng.GetInstance.RequestSaveGameData (PlayerPrefs.GetInt ("SD_GAME_ID"),
				PlayerPrefs.GetInt("SD_TRIAL_TH"),
				PlayerPrefs.GetInt("SD_GRADE"),
				PlayerPrefs.GetInt("SD_GUEST_TH"));
		}
		SceneManager.LoadScene ("Title");

	}
	IEnumerator wait() {
		sw.Stop ();
		yield return new WaitForSeconds(2);
		buttonsWhite();
		ticks = 0;
		popup.makeIt(PP.popupIndex.JUDGE);
		//sw.Reset ();
		while(popup.PopupObject != null)
			yield return new  WaitForSecondsRealtime(.1f);
		
		//makeActive ();

	}


	/// <------------------------------------------this is end of General Codes of each Scenes. Start optional functions of Lobby Scene.------------------------------------------->
	public void SetTextOrder(int _randomNum, int _randomMenu, int _randomHI)
	{
		UnityEngine.Debug.Log ("ScoreManager_Lobby.SetTextOrder 0  _randomNum: " + _randomNum + "  _randomMenu: " + _randomMenu + "  _randomHI: " + _randomHI );

		if (_randomMenu != 0 && _randomMenu != 13 && _randomMenu  != 14 && _randomMenu != 15) {
			if (_randomNum == 1) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔 주시죠~");
			} else if (_randomNum == 2) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔 주시겠어요?");
			} else if (_randomNum == 3) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔 부탁드립니다");
			} else if (_randomNum == 4) {
				MayItakeYourOrder.text = ("오호~ " + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔만~");
			}
			else if (_randomNum == 5) {
				MayItakeYourOrder.text = ("에헴!" + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔 주게나.");
			} else if (_randomNum == 6) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + "가 맛있다지?" + hiStr [_randomHI] + " " + "한 잔 줘보게");
			}
		} else {
			if (_randomNum == 1) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + "한 잔 주시죠~");
			} else if (_randomNum == 2) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu]  + " " + "한 잔 주시겠어요?");
			} else if (_randomNum == 3) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + "한 잔 부탁드립니다");
			}else if (_randomNum == 4) {
				MayItakeYourOrder.text = ("오호~ " + menuStr [_randomMenu] + " " + "한 잔만~");
			} 
			else if (_randomNum == 5) {
				MayItakeYourOrder.text = ("에헴!" + menuStr [_randomMenu]  + " " + "한 잔 주게나.");
			} else if (_randomNum == 6) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + "가 맛있다지?" + " " + "한 잔 줘보게");
			}
		}
	}
	void SetTextWrongOrder(int _randomNum, int _randomMenu, int _randomHI){
		if (_randomMenu != 0 && _randomMenu != 13 && _randomMenu  != 14 && _randomMenu != 15) {
			if (_randomNum == 1) {
				MayItakeYourOrder.text = ("아이, 참!" + menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "한 잔 달라구요");
			} else if (_randomNum == 2) {
				MayItakeYourOrder.text = ("저는" + menuStr [_randomMenu] + "를 " + hiStr [_randomHI] + " " + "먹고 싶어요.");
			} else if (_randomNum == 3) {
				MayItakeYourOrder.text = (menuStr [_randomMenu] + " " + hiStr [_randomHI] + " " + "라고 말씀드렸습니다만…");
			} else if (_randomNum == 4) {
				MayItakeYourOrder.text = (menuStr [_randomMenu] + " " + hiStr [_randomHI] +" 라니까~ 장난치면 안 돼~");
			}
			else if (_randomNum == 5) {
						MayItakeYourOrder.text = (menuStr [_randomMenu] + " " + hiStr [_randomHI] + "를 부탁했네만…");
			} else if (_randomNum == 6) {
						MayItakeYourOrder.text = ( menuStr [_randomMenu] + " "+ hiStr [_randomHI] + "일세. 신입인가보군.");
			}
		} else {
			if (_randomNum == 1) {
				MayItakeYourOrder.text = ("아이, 참!" + menuStr [_randomMenu] + " " + "한 잔 달라구요");
			} else if (_randomNum == 2) {
				MayItakeYourOrder.text = ("저는" + menuStr [_randomMenu]  + "를 " + "먹고 싶어요.");
			} else if (_randomNum == 3) {
				MayItakeYourOrder.text = ("" + menuStr [_randomMenu] + " " + "라고 말씀드렸습니다만…");
			}else if (_randomNum == 4) {
				MayItakeYourOrder.text = ( menuStr [_randomMenu]  + " 라니까~ 장난치면 안 돼~");
			} 
			else if (_randomNum == 5) {
				MayItakeYourOrder.text = (menuStr [_randomMenu]  + "를 부탁했네만…");
			} else if (_randomNum == 6) {
				MayItakeYourOrder.text = (menuStr [_randomMenu] + "일세. 신입인가보군.");
			}
		}
	}
	void SetOnInit()
	{
		randomNum = (int)Math.Floor(UnityEngine.Random.Range (1.0f, 7.0f));
		//randomHI = (int)Math.Floor(UnityEngine.Random.Range (0.0f, 2.0f));
		Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+randomNum);
		desk.sprite = source;

		//desk.
		//Button Init;

		switch (MainUserInfo.GetInstance.grade) {
		case 1:
			for (int i = 5; i < bts.Count; i++) {
				imgs [i].color = Color.gray;
				bts [i].enabled = false;
			}
			randomMenu = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 5.0f));
			randomHI = HiIndex (randomMenu);

			SetTextOrder (randomNum, randomMenu, randomHI);
			
			break;
		case 2:
			for (int i = 9; i < bts.Count; i++) {
				imgs [i].color = Color.gray;
				bts [i].enabled = false;
			}
			randomMenu = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 9.0f));
			randomHI = HiIndex (randomMenu);
			SetTextOrder (randomNum, randomMenu, randomHI);
			break;
		case 3:
			for (int i = 13; i < bts.Count; i++) {
				imgs [i].color = Color.gray;
				bts [i].enabled = false;
			}
			randomMenu = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 13.0f));
			randomHI = HiIndex (randomMenu);
			SetTextOrder (randomNum, randomMenu, randomHI);
			break;
		case 4:
			randomMenu = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 16.0f));
			randomHI = HiIndex (randomMenu);
		
			SetTextOrder (randomNum, randomMenu, randomHI);
			break;
		default:
			randomMenu = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 16.0f));
			randomHI = HiIndex (randomMenu);
			SetTextOrder (randomNum, randomMenu, randomHI);
			break;
		}

		MainUserInfo.GetInstance.menu = randomMenu;
		MainUserInfo.GetInstance.cream = randomCream;
		MainUserInfo.GetInstance.guestId = randomNum;
		MainUserInfo.GetInstance.cream = 1;



	}
	public void getOrder(Button bt)
	{
		SoundManage.instance.efxButtonOn ();
		ColorBlock cb = bt.colors;
		//Highlighting.
		if (cb.normalColor == Color.red) {
			buttonWhite (bt);
			getMenu = menu.NOTHINGTOCHOSE;
		} else {
			buttonsWhite ();
			buttonRed (bt);
			getMenu = (menu)Convert.ToInt32(bt.name);
		}
	}
	public void getCreamOrder(Button bt)
	{
		SoundManage.instance.efxButtonOn ();
		ColorBlock cb = bt.colors;
		//Highlighting.
		if (cb.normalColor == Color.red) {
			buttonWhite (bt);
			getMenu = menu.NOTHINGTOCHOSE;
		} else {
			buttonsWhite2 ();
			buttonRed (bt);
			getMenu = (menu)Convert.ToInt32(bt.name);
		}
	}
	void buttonsWhite()
	{	
		
		for (int i = 0; i < bts.Count; i++) {
			if (bts [i].enabled) {
				buttonWhite (bts [i]);
			}
		}
		getMenu = menu.NOTHINGTOCHOSE;

	}
	void buttonsWhite2()
	{	
		Button[] bt = creamButton.GetComponentsInChildren<Button> ();
		for (int i = 0; i < bt.Length; i++) {
			if (bt [i].enabled) {
				buttonWhite (bt [i]);
			}
		}
		getMenu = menu.NOTHINGTOCHOSE;

	}


	void buttonWhite(Button bt)
	{	
		ColorBlock cb = bt.colors;
		cb.normalColor = Color.white;
		cb.highlightedColor = Color.white;
		bt.colors = cb;
	}
	void buttonRed(Button bt)
	{	
		ColorBlock cb = bt.colors;
		cb.normalColor = Color.red;
		cb.highlightedColor = Color.red;
		bt.colors = cb;
	}

	public void confirm()
	{
		SoundManage.instance.efxButtonOn ();

		if (getMenu == menu.CREAMIN || getMenu == menu.CREAMOUT) {
			if (getMenu == (menu)randomCream + 16) {

				RightOrder ();

			}
			else{
				Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+randomNum+"2");
				desk.sprite = source;
				CreamText (randomNum, randomCream);
				SoundManage.instance.efxBadOn ();
				//SetTextWrongOrder (randomNum, randomMenu, randomHI);
				lobby_ani.ShowmetheCoffeSizeUp();
				lobby_desk_ani.DeskCry ();
				stuff.Misscount++;
			}

		}
		else if (getMenu == (menu)(randomMenu)){
			if (getMenu == menu.CAFFEMOCHAH
				|| getMenu == menu.CARFEMOCHAI
			    || getMenu == menu.MAGGIYATTOH
			    || getMenu == menu.MAGGIYATTOI
				|| getMenu == menu.VANILLALATTEH
				|| getMenu == menu.VANILLALATTEI
				|| getMenu == menu.HAZELNUTH
				|| getMenu == menu.HAZELNUTI) {
				SoundManage.instance.efxOkOn ();
				randomCream=(int)Math.Floor(UnityEngine.Random.Range (0.0f, 2.0f));
				MainUserInfo.GetInstance.cream = randomCream;
			    CreamText (randomNum, randomCream);
				MenuButton[currentMenuPage].SetActive (false);
				prevButton.interactable = false;
				nextButton.interactable = false;
				creamButton.SetActive (true);
				lobby_ani.ShowmetheCoffeSizeUp();

			} else {

				RightOrder ();
		
			}
		} 
		else if (getMenu == menu.NOTHINGTOCHOSE) {
			Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+randomNum+"2");
			desk.sprite = source;
			SetTextWrongOrder (randomNum, randomMenu, randomHI);
			SoundManage.instance.efxBadOn ();
			lobby_ani.ShowmetheCoffeSizeUp();
			lobby_desk_ani.DeskCry ();
			stuff.Misscount++;
		}
		else{
			Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+randomNum+"2");
			desk.sprite = source;
			SetTextWrongOrder (randomNum, randomMenu, randomHI);
			SoundManage.instance.efxBadOn ();
			lobby_ani.ShowmetheCoffeSizeUp();
			lobby_desk_ani.DeskCry ();
			stuff.Misscount++;
		}
		PlayerPrefs.SetInt ("Menu", MainUserInfo.GetInstance.menu);
		PlayerPrefs.SetInt ("GuestId", MainUserInfo.GetInstance.guestId);
		PlayerPrefs.SetInt ("Cream", MainUserInfo.GetInstance.cream);
		PlayerPrefs.SetInt ("COFFEE_NUM_I",MainUserInfo.GetInstance.menu+1);



	}
	void RightOrder(){
		SoundManage.instance.efxOkOn ();
		if (isOkDone == true) {
			return;
		}

		isOkDone	= true;

		float timmerMinus = 0;
		float errorMinus = 0;
		//float correctRate = 0;
		Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+randomNum+"3");


		stuff.Gameid = MainUserInfo.GetInstance.gameId;
		stuff.Level = MainUserInfo.GetInstance.grade;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		sw.Stop ();
		//sw.Reset ();
		stuff.Playtime = ticks;
		timmerMinus = Math.Max (0, Math.Min (1, (stuff.Playtime - cuff.Besttime) / (cuff.Limittime - cuff.Besttime)));
		stuff.ErrorRate = stuff.Misscount * cuff.Errorpenalty;
		//correctRate = Math.Max (0, 100 - stuff.ErrorRate);
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate - cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));

		UnityEngine.Debug.Log ("ScoreManager_Lobby mCnt: " + stuff.Misscount);
		UnityEngine.Debug.Log ("ScoreManager_Lobby eP: " + cuff.Errorpenalty);
		UnityEngine.Debug.Log ("ScoreManager_Lobby eR: " + stuff.ErrorRate);
		UnityEngine.Debug.Log ("ScoreManager_Lobby tM: " + timmerMinus);
		UnityEngine.Debug.Log ("ScoreManager_Lobby eM: " + errorMinus);
		//UnityEngine.Debug.Log ("ScoreManager_Lobby cR: " + correctRate);


		stuff.Score = 100 - (timmerMinus + errorMinus) * 50;
		desk.sprite = source;
		int _rightNum = RightText (stuff.Score);
		MayItakeYourOrder.text = "네 바로 그거에요!";//afterText (randomNum, 4);// _rightNum);
		lobby_ani.ShowmetheCoffeSizeUp();

		StartCoroutine (wait ());
	}
	int RightText(float cs)
	{	
		if (cs < 60)
			return 0;
		else if (cs < 70)
			return 1;
		else if (cs < 80)
			return 2;
		else if (cs < 90)
			return 3;
		else
			return 4;
	}
	int HiIndex(int num){
		switch (num) {
		case 0:
			return 0;
		case 1:
			return 0;
		case 2:
			return 1;
		case 3:
			return 0;
		case 4:
			return 1;
		case 5:
			return 0;
		case 6:
			return 1;
		case 7:
			return 0;
		case 8:
			return 1;
		case 9:
			return 0;
		case 10:
			return 1;
		case 11:
			return 0;
		case 12:
			return 1;
		case 13:
			return 0;
		case 14:
			return 0;
		case 15:
			return 0;
		}
		return 0;
	}
	public void CreamText(int _randomNum,int num)
	{
		MayItakeYourOrder.text = creamText [(_randomNum - 1), num];
	}
	public void ReadyAndStart(){
		StartCoroutine (RS ());
	}
	IEnumerator RS(){
		_Ready.SetActive (true);
		yield return new WaitForSecondsRealtime (1.5f);
		_Ready.SetActive (false);
		_Start.SetActive (true);
		yield return new WaitForSecondsRealtime (1.5f);
		_Start.SetActive (false);
		yield return new WaitForSecondsRealtime (0.5f);
		desk.gameObject.SetActive(true);
		yield return new WaitForSecondsRealtime (1.0f);
		//ShowmetheCoffee.gameObject.SetActive(true);
		lobby_ani.ShowmetheCoffeSizeUp();
		StartCoroutine (TimerCheck ());
		stuff.Sw.Start ();

	}
	void TalkBoxShowAndDown(){
		
	}

	public void prevButtonClick(){
		SoundManage.instance.efxButtonOn ();
		if (currentMenuPage > 0) {
			MenuButton [currentMenuPage].SetActive (false);
			currentMenuPage--;
			MenuButton [currentMenuPage].SetActive (true);
			nextButton.interactable = true;
			if (currentMenuPage == 0) {
				prevButton.interactable = false;
			}
				
		} 
	}
	public void nextButtonClick(){
		SoundManage.instance.efxButtonOn ();
		if (currentMenuPage < MenuButton.Length - 1) {
			MenuButton [currentMenuPage].SetActive (false);
			currentMenuPage++;
			MenuButton [currentMenuPage].SetActive (true);
			prevButton.interactable = true;
			if (currentMenuPage == MenuButton.Length - 1) {
				nextButton.interactable = false;
			}
		} 
	}

}
