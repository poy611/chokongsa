﻿using UnityEngine;
using System.Collections;


public class SoundManage : MonoBehaviour 
{
	public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
	public AudioSource musicSource;                 //Drag a reference to the audio source which will play the music.
	public static SoundManage instance = null;     //Allows other scripts to call functions from SoundManager.             
	public AudioClip _bgmTitle;
	public AudioClip _bgmLobby;
	public AudioClip _bgmGrinding;
	public AudioClip _bgmVariaion;
	public AudioClip _bgmTemping;
	public AudioClip _bgmIcing;
	public AudioClip _bgmLearn;
	public AudioClip _bgmMinigame;
	public AudioClip _bgm;
	public AudioClip _efxBtn;
	public AudioClip _efxOk;
	public AudioClip _efxBad;

	public bool isPlaying = false;
	void Awake ()
	{
			//Check if there is already an instance of SoundManager
		if (instance == null)
				//if not, set it to this.
				instance = this;
		//If instance already exists:
		else if (instance != this)
				//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);

			//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

	public void efxButtonOn(){
		efxSource.PlayOneShot (_efxBtn, 1.0f);
	}
	public void efxOkOn(){
		efxSource.PlayOneShot (_efxOk, 1.0f);
	}
	public void efxBadOn(){
		efxSource.PlayOneShot (_efxBad, 1.0f);
	}

		//Used to play single sound clips.
	public void PlayBgm(string sceneName)
	{
		

		if (sceneName == "Title") {
			_bgm = _bgmTitle;
		}
		else if (sceneName == "Lobby") {
			_bgm = _bgmLobby;
		
		}
		else if (sceneName == "Grinding") {
			_bgm = _bgmGrinding;
		
		}
		else if (sceneName == "4_Variation_proto") {
			_bgm = _bgmVariaion;
		
		}
		else if (sceneName == "MiniGame") {
			_bgm = _bgmMinigame;
		
		}
		else if (sceneName == "Temping") {
			_bgm = _bgmTemping;
		
		}
		else if (sceneName == "Icing") {
			_bgm = _bgmIcing;
		
		}
		else if (sceneName == "LEARNPAGE" || 
			sceneName == "LearnCoffeePenut" ||
			sceneName == "LearnCoffeTool" ||
			sceneName == "LearnCalculation") {
			if (!isPlaying) {
				isPlaying = true;
				_bgm = _bgmLearn;

			}
		}
		musicSource.clip = _bgm;
		musicSource.enabled =true;
			
		//Set the clip of our efxSource audio source to the clip passed in as a parameter.

	}
	public void OffBgm(){
		isPlaying = false;
		musicSource.clip = null;
		musicSource.enabled =false;
	}
		


}


