﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class VariationResultWindow : MonoBehaviour {

	public Image coffeImg;
	public Image CreamImg;
	public Text menuText;
	public ShowRecipePre _showRecipePre;
	public Button _btn;
	public Text[] menuTextList;
	public Text[] unitTextList;
	// Use this for initialization
	void OnEnable(){
		Sprite sour = Resources.Load<Sprite> (RecipeList.GetInstance().coffeeImgList[MainUserInfo.GetInstance.menu]);
		coffeImg.sprite = sour;
		MenuList menuList = new MenuList ();
		if (MainUserInfo.GetInstance.cream == 0) {
			CreamImg.enabled = true;
		}
		menuText.text = menuList.GetMenuInPopup (MainUserInfo.GetInstance.menu);
		for (int i = 0; i < _showRecipePre.menuList.Count; i++) {
			menuTextList [i].text = _showRecipePre.menuList [i];
			unitTextList [i].text = _showRecipePre.unitList [i];
		}
	}
	void Update()
	{
		if (_showRecipePre.imgTrans.gameObject.active) {
			_btn.gameObject.SetActive (false);
		} else {
			_btn.gameObject.SetActive (true);
		}
	}
}
