﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LearnPage : MonoBehaviour {

	void Start(){
		SoundManage.instance.OffBgm ();

		SoundManage.instance.PlayBgm (SceneManager.GetActiveScene ().name);
	}
	public void GoLearnMakeCoffee(){
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LearnMakeCoffee");
	}

	public void GoLearnCoffeeTool(){
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LearnCoffeeTool");
	}
	public void GoLearnCoffeePenut(){
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LearnCoffeePenut");
	}

	public void GoLearnCalculation(){
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LearnCalculation");
	}
	public void BackButtonClick()
	{
		SoundManage.instance.efxButtonOn ();
		SoundManage.instance.OffBgm ();
		PlayerPrefs.SetInt ("BACKBUTTON", 1);
		SceneManager.LoadScene ("Title");
	}
}
