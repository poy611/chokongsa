﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using System;
public class ScoreManager_Grinding : MonoBehaviour {
	StructforMinigame stuff;
	ConstforMinigame cuff;
	PopupManagement popup;
	int nowNum;
	//Stopwatch sw;
	//bool isChecked=false;

	void Awake(){
		nowNum = SceneManager.GetActiveScene ().buildIndex - 1;
		stuff = StructforMinigame.getSingleton (nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade-1);

	}
	// Use this for initialization
	void Start () {
		

		//	popup draw.
		popup = PopupManagement.p;
		//sw = stuff.Sw;

	}
	public void ClickSound(){
		SoundManage.instance.efxButtonOn ();
	}
	void OnEnable(){
		StartCoroutine (confirm ());
	}
	IEnumerator confirm()
	{
		yield return new WaitForSecondsRealtime (1.5f);
		UnityEngine.Debug.Log (" " + PlayerPrefs.GetFloat ("LIMIT_TIME_F") + " " + PlayerPrefs.GetFloat ("GOAL_POINT_F") + " " + PlayerPrefs.GetFloat ("TIME_NOW_F") + " "
		+ PlayerPrefs.GetFloat ("GAUGE_POINT_F"));
		float timmerMinus = 0;
		float errorRate = 0;
		float errorMinus = 0;
		float limitTime = PlayerPrefs.GetFloat ("LIMIT_TIME_F");
		float goalPoint = PlayerPrefs.GetFloat ("GOAL_POINT_F");
		float currentTime = cuff.Limittime - PlayerPrefs.GetFloat ("TIME_NOW_F");
		float gaugePoint = PlayerPrefs.GetFloat ("GAUGE_POINT_F");

	
		stuff.Gameid = MainUserInfo.GetInstance.gameId;
		stuff.Level = MainUserInfo.GetInstance.grade;
		stuff.Misscount = 0;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		stuff.Playtime = currentTime;
		UnityEngine.Debug.Log ("PlayTime" + stuff.Playtime);
		timmerMinus = Math.Max(0,Math.Min(1, (currentTime-cuff.Besttime)/(cuff.Limittime-cuff.Besttime)));
		stuff.ErrorRate = Math.Abs ((gaugePoint - goalPoint) / goalPoint* 100);
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate- cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));
		UnityEngine.Debug.Log ("tM: "+timmerMinus);
		UnityEngine.Debug.Log ("eM: "+errorMinus);
		UnityEngine.Debug.Log ("eM: "+errorRate);
		stuff.Score = 100-(timmerMinus+errorMinus)*50;
		popup.makeIt (PP.popupIndex.JUDGE);

	}

}
