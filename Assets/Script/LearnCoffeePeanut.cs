using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LearnCoffeePeanut : MonoBehaviour {
	Sprite[] source;
	Image desk;
	Button prevbt;
	Button nextbt;
	Text tran;
	Text descript;
	Text pageNum;
	int cursur;

	string[] arrToolName = new string[]{
		"콜롬비아수프리모","태국도이창","케냐AA","브라질산토스","에티오피아 시다모",
		"인디아 몬순말라바AA","온두라스SHB","인도네시아 수마트라 만델링","과테말라 안티구아","에티오피아 이가체프",
		"말라위 AAA","하와이안 코나","코스타리카 타라주" };
	
	string[] arrToolDscrpt	= new string[]{
		"콜롬비아 안데스 산맥의 고산지대에서 재배되는 커피이다.\n커피의 맛을 구성하는 주요 요소들인 신맛, 단맛, 그리고 바디감으로 표현되는 쓴맛이 모두 적절한 조화를 이루어서 복합적이면서도 깔끔한 맛을 낸다.",
		"산뜻한 과일 향과 마카다미아 너트의 고소한 향이 고급스럽게 어우러지는 것이 도이창 커피의 특징이다. 로스팅 정도에 따라 가볍게 올라오는 신맛에서 중우한 버터 맛에 이르기까지 다양한 맛을 낸다.",
		"케냐 더블에이는 세계적으로 커피의 품질관리가 우수한 아프리카 최고의 커피이다.\n로스팅 정도에 따라 감미로운 향과 과일의 단맛, 쌉쌀한 맛을 조화롭게 낼 수 있다.",
		"브라질 최고급 커피로서 남부의 산토스 항구에서 브라질 커피의 대부분이 수출된 데에서 나온 이름이다.\n단맛과 신맛 등의 향미가 고르게 조화를 이루며, 순하면서도 감칠맛이 나기 때문에 많은 커피 애호가들의 사랑을 받고 있다.",
		"이가체프와 함께 에티오피아를 대표하는 커피 중 하나이며, 이가체프 보다는 조금 더 깊고, 묵직한 향미가 있는 커피이다.\n산뜻하고 깔끔한 산미가 있어서 볶는 정도에 따라 과일 향에 비유될 정도로 상큼한 산미를 함께 느낄 수 있는 커피이다.",
		"몬순말라바는 다른 커피와 다르게 숙성 가공된 커피이다.\n숙성가공 된 커피여서 향미 또한 독특하며, 로스팅하기 까다로운 원두로 유명하며, 쓴맛이 쌉쌀하고, 고소하고, 흙맛이 느껴지면서 달콤한 뒷맛이 매력적인 커피이다.",
		"고산지대에서 재배되며 일교차가 커서 커피의 조직이 치밀하고 단단해져서 향미가 깊고 맛이 좋다.\n열대과일의 부드러운 단맛과 균형잡힌 풍미가 일품이며, 크리미한 바디감과 깜끔한 맛이 매력적인 커피이다.",
		"수마트라섬에서 생산도니 만델링 커피는 강렬하고 묵직한 바디감과 특유의 감칠맛으로 남성적인 커피로 널리 알려져 있다.\n매력적인 쓴맛과, 짙은 풍미, 단당하고 은은한 향미가 잘 조화된 매력적인 커피이다.",
		"커피나무가 화산 폭발에서 나온 질소를 흡수하여 연기가 타는 듯한 향을 가진 스모크 커피의 대명사이다.\n생두가 단단한 편이며, 알맞은 산도와 달콤한 맛, 풍부한 바디, 생동감 있는 향을 가진 커피다.",
		"이가체프는 와인에 비유될 정도로 화려한 향미와 산뜻한 산미로 에티오피아 커피 중에서 가장 널리 알려져 있는 커피이다.\n부드러우면서도 진한 꽃 향미와 함께 에티오피아 커피 특유의 부드러운 바디감을 지니고 있으며 달콤함과 신맛을 적절히 함유하고 있다.",
		"초콜릿과 과일의 부드럽고 풍부한 향미를 가지고 있으며 부드러운 바디와 매력적인 향을 가지고 있는 커피이다.\n신맛은 케냐나 탄자니아보다 약한 편이다.",
		"미국 하와이섬의 코나 지역에서 재배되는 커피이다.\n과일처럼 상큼한 신맛과 옅은 단맛이 있으며 산뜻한 향이 풍부하고 중간 정도의 바디를 가지고 있다.",
		"코스타리카의 고지대에 있는 타라주 지역의 커피이다.\n상큼하면서도 톡 쏘는 듯한 신맛과 풍부한 바디, 산뜻한 향을 느낄 수 있다." };


	// Use this for initialization
	void Start () {
		source = Resources.LoadAll<Sprite> ("coffeepeanut");
		desk = transform.Find ("learnPopup").transform.Find("tool_image").GetComponent<Image> ();
		prevbt = transform.Find ("learnPopup").transform.Find ("prev").GetComponent<Button> ();
		nextbt = transform.Find ("learnPopup").transform.Find ("next").GetComponent<Button> ();
		tran = transform.Find ("learnPopup").transform.Find("tool_name").GetComponent<Text> ();
		descript = transform.Find ("learnPopup").transform.Find("tool_description").GetComponent<Text> ();
		pageNum = transform.Find ("learnPopup").transform.Find ("pageNum").GetComponent<Text> ();
		cursur = 0;
		deskit ();
	}
	public void backButton()
	{
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LEARNPAGE");
	}
	public void prev()
	{
		SoundManage.instance.efxButtonOn ();
		cursur--;
		deskit ();

	}
	public void next()
	{
		SoundManage.instance.efxButtonOn ();
		UnityEngine.Debug.Log ("LearnCoffeePeanut.next 1   cursur: " + cursur );
		cursur++;
		deskit ();
		Debug.Log ("pressed");
	}
	void deskit()
	{
		desk.sprite 	= source [cursur];
		tran.text 		= "" + arrToolName[cursur];
		descript.text 	= "" + arrToolDscrpt[cursur];
		pageNum.text 	= (cursur + 1) + "/" + source.Length;
		unableButton ();
	}
	void unableButton()
	{
		if (cursur < 1) {
			prevbt.interactable = false;
			cursur = 0;
		}
		else
			prevbt.interactable = true;
		if (cursur> source.Length-2) {
			nextbt.interactable = false;
			cursur = source.Length - 1;
		}
		else
			nextbt.interactable = true;
	}
}
