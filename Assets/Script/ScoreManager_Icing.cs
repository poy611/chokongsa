﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;

using UnityEngine.UI;
public class ScoreManager_Icing : MonoBehaviour {

	int nowNum;
	string nowScene;
	float ticks = 0;
	public Text timmer;
	public ShowRecipePre recipePre;
	public GameObject ready;


	StructforMinigame stuff;
	ConstforMinigame cuff;
	PopupManagement pop;
	PlayMakerFSM myFsm;
	int missCount=0;
	int currentIngredient = 0;
	Stopwatch sw;
	// Use this for initialization
	void Start () {

		nowNum = SceneManager.GetActiveScene ().buildIndex-1;
		nowScene = SceneManager.GetActiveScene ().name;
		pop = PopupManagement.p;
		stuff = StructforMinigame.getSingleton (nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade - 1);
		sw = stuff.Sw;
		sw.Reset ();
		pop.makeIt (PP.popupIndex.READY);


	}

	public void buttonOkClickSound()
	{
		SoundManage.instance.efxButtonOn ();
	}
	public void startIcing(){
		ready.SetActive (true);
	}
	public void ButtonClickEvent(PlayMakerFSM _myFsm){
		myFsm = _myFsm;
		UnityEngine.Debug.Log ("ButtonClickEvent");
	}
	IEnumerator Finish(){
		yield return new WaitForSecondsRealtime (5.0f);
		GameObject.Find("UI Canvas").transform.FindChild("RESULT WINDOW").gameObject.SetActive(true);
	}
	public void isRightSelect(int num)
	{
		
		if (currentIngredient < RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu].Length) {
			

			if (RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu] [currentIngredient]
				== num) {
				//SoundManage.instance.efxOkOn ();
				recipePre.IcingRightChoice ();
				currentIngredient++;
				myFsm.Fsm.Event ("OK");
				if (currentIngredient == RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu].Length) {
					//end of game
					StartCoroutine(Finish());
				}
			} 
			else {
				missCount++;
				SoundManage.instance.efxBadOn ();
				myFsm.Fsm.Event ("BAD");
			}
		} else {
			SoundManage.instance.efxBadOn ();
			missCount++;
			myFsm.Fsm.Event ("BAD");
		}

	}
	public void TimeCheckerStart(){
		StartCoroutine (TimerCheck());
	}
	IEnumerator TimerCheck(){
		while(true){
			ticks = (sw.ElapsedMilliseconds+Time.deltaTime)/1000;
			int it=(int)(cuff.Limittime-Math.Round(ticks,0));
			timmer.text = it.ToString();
			if (it <= 0) {
				StartCoroutine (TimeEnd ());
				yield break;
			}
			yield return null;
		}
	}
	IEnumerator TimeEnd(){
		sw.Stop ();
		yield return new WaitForSeconds(2);

		ticks = 0;
		stuff.Playtime = cuff.Limittime;
		stuff.ErrorRate = 100.0f;
		pop.makeIt(PP.popupIndex.JUDGE);

		while(pop.PopupObject != null)
			yield return new  WaitForSecondsRealtime(.1f);

	}
	public void conFirm(){
		//SoundManage.instance.efxButtonOn ();
		float timmerMinus=0;
		float correctRate = 0;
		float errorMinus = 0;

		stuff.Level = MainUserInfo.GetInstance.grade;
		stuff.Misscount = missCount;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		sw.Stop ();
		stuff.Playtime = ticks;

		timmerMinus = Math.Max (0, Math.Min (1, (stuff.Playtime - cuff.Besttime) / (cuff.Limittime - cuff.Besttime)));
		stuff.ErrorRate = stuff.Misscount * cuff.Errorpenalty;
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate - cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));
		UnityEngine.Debug.Log ("tM: " + timmerMinus);
		UnityEngine.Debug.Log ("eM: " + errorMinus);
		stuff.Score = 100 - (timmerMinus + errorMinus) * 50;
		pop.makeIt (PP.popupIndex.JUDGE);

	}
}
