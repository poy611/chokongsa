using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LearnCoffeeTool : MonoBehaviour {
	Sprite[] source;
	Image desk;
	Button prevbt;
	Button nextbt;
	Text tran;
	Text descript;
	Text pageNum;
	int cursur;

	string[] arrToolName = new string[]{
		"에스프레소 머신","그라인더","포터필터","템퍼","샷 잔",
		"스팀밀크 피쳐","우유","휘핑크림통","머그컵","얼음",

		"저울","바닐라 시럽","초코 시럽","카라멜 시럽","헤이즐넛 시럽",
		"바구니","쓰레기통"
	};
	
	string[] arrToolDscrpt	= new string[]{
		"가압방식으로 에스프레소를 추출하는 기계. 커피를 만들 때 가장 기본이 되는 장치이다.\n주로 우유의 거품을 내기 위한 스팀장치가 포함되어 있다.",
		"볶은 커피콩을 분쇄하는 기구.  수동, 전자동 다양한 종류가 있다, 설정된 중량에 따라 정해진 커피콩을 분쇄하는 편리한 자동기계가 많지만, 수동기계가 가격이 싸기 때문에 인기가 많다.",
		"그라인더를 통해 분쇄된 커피 가루를 담는 필터.  에스프레소 머신에 장착하여 에스프레소 추출에 사용된다.",
		"포터필터에 담긴 원두를 꾹꾹 눌러서 다지는 기구.  원두를 너무 세게 다지거나 너무 약하게 다지면 커피 가루 내부에 필요한 공기 양이 부족하거나 지나쳐서 에스프레소의 품질에 영향을 미친다.",
		"에스프레소를 담아내는 컵.  에스프레소의 단위 잔으로 사용된다.",

		"우유를 데우거나 우유 거품을 낼 때 사용한다.",
		"스팀밀크의 재료.  카페라떼와 같이 부드러운 맛의 커피를 만들 때 사용한다.",
		"휘핑크림과 질소를 함께 넣어두어서 필요할 때 휘핑크림을 거품처럼 뿜어내는 통.",
		"완성된 커피를 담는 컵.  손잡이가 있는 컵과 없는 컵 등 다양한 모양의 컵이 있다.",
		"차가운 커피를 만들 때 사용하는 재료.  얼음을 넣을 때 컵에 액체가 담겨 있으면 액체가 넘쳐 흐를 수 있으므로 커피를 만들 때 주로 가장 먼저 컵에 넣는다.",

		"무게를 젤 때 사용하는 도구.  분쇄된 커피가루, 물, 우유 등의 정확한 양을 측정해서 담을 때 주로 사용한다.  매장에 따라서 저울을 사용하는 곳이 있고, 사용하지 않는 곳이 있다.",
		"바닐라 맛과 향을 낼 때 사용하는 시럽.  바닐라라떼를 만들 때 사용한다.  당도가 높으므로 양 조절에 주의해야 한다.",
		"초컬릿의 맛과 향을 낼 때 사용하는 시럽.  카페모카를 만들 때 사용한다.  당도가 높으므로 양 조절에 주의해야 한다.",
		"카라멜의 맛과 향을 낼 때 사용하는 시럽.  카라멜 마끼아또를 만들 때 사용한다.  당도가 높으므로 양 조절에 주의해야 한다.",
		"헤이즐넛의 맛과 향을 낼 때 사용하는 시럽.  헤이즐넛라떼를 만들 때 사용한다.  당도가 높으므로 양 조절에 주의해야 한다.",

		"좋은 커피콩과 나쁜 커피콩을 구분해서 담을 때 사용한다.",
		"쓰레기들을 담는 통.  매장 내부를 정기적으로 청소해서 깔끔하게 유지해줘야 하므로 쓰레기통은 절반 정도만 채워져 있어도 비워주는 것이 좋다.",
	};


	// Use this for initialization
	void Start () {
		source = Resources.LoadAll<Sprite> ("ingredient");
		desk = transform.Find ("learnPopup").transform.Find("tool_image").GetComponent<Image> ();
		prevbt = transform.Find ("learnPopup").transform.Find ("prev").GetComponent<Button> ();
		nextbt = transform.Find ("learnPopup").transform.Find ("next").GetComponent<Button> ();
		tran = transform.Find ("learnPopup").transform.Find("tool_name").GetComponent<Text> ();
		descript = transform.Find ("learnPopup").transform.Find("tool_description").GetComponent<Text> ();
		pageNum = transform.Find ("learnPopup").transform.Find ("pageNum").GetComponent<Text> ();
		cursur = 0;
		deskit ();
	}
	public void backButton()
	{
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene ("LEARNPAGE");
	}
	public void prev()
	{
		SoundManage.instance.efxButtonOn ();
		cursur--;
		deskit ();

	}
	public void next()
	{
		SoundManage.instance.efxButtonOn ();
		UnityEngine.Debug.Log ("LearnCoffeeTool.next 1   cursur: " + cursur );
		cursur++;
		deskit ();
		Debug.Log ("pressed");
	}
	void deskit()
	{
		desk.sprite 	= source [cursur];
		tran.text 		= "" + arrToolName[cursur];
		descript.text 	= "" + arrToolDscrpt[cursur];
		pageNum.text 	= (cursur + 1) + "/" + source.Length;
		unableButton ();
	}
	void unableButton()
	{
		if (cursur < 1) {
			prevbt.interactable = false;
			cursur = 0;
		}
		else
			prevbt.interactable = true;
		if (cursur> source.Length-2) {
			nextbt.interactable = false;
			cursur = source.Length - 1;
		}
		else
			nextbt.interactable = true;
	}
}
