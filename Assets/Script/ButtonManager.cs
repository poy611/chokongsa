﻿using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour {
	//0_Chokongsa에는 타이틀, 콩고르기, 게임스타트+튜토리얼이 필요하다.
	// Use this for initialization
	//SceneindexInc n = SceneindexInc.getSingleton();
	PopupManagement pop;

	enum StateTitle{
		START,
		MINIGAME,
		STUDY,
		RESULT
	};

	StateTitle state;
	void Awake(){
		SoundManage.instance.PlayBgm(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
		//PlayerPrefs.DeleteAll ();
		if (!PlayerPrefs.HasKey ("USER_ID")) {
			PlayerPrefs.SetString ("USER_ID","LOCAL");
		}

		if (!PlayerPrefs.HasKey ("Menu")) {
			PlayerPrefs.SetInt ("Menu", 0);
		}
		if(!PlayerPrefs.HasKey("GuestId")){
			PlayerPrefs.SetInt ("GuestId",0);
		}	
		if(!PlayerPrefs.HasKey("Cream")){
			PlayerPrefs.SetInt ("Cream",1);
		}
		if(!PlayerPrefs.HasKey("COFFEE_NUM_I")){
			PlayerPrefs.SetInt ("COFFEE_NUM_I",1);
		}
	
	}
	void Start(){

		NetworkMng.GetInstance.onSuccess += new NetworkMng.OnSuccess (OnSuccess);
		NetworkMng.GetInstance.noInfo += new NetworkMng.OnNoInfo (NoInfo);
		NetworkMng.GetInstance.onFail += new NetworkMng.OnFail (OnFail);
		pop = PopupManagement.p;
		state = StateTitle.START;
	}
	void OnSuccess(){
		switch (state) {
		case StateTitle.START:
			PopupManagement.p.makeIt (PP.popupIndex.CONTINUE);
			break;
		case StateTitle.MINIGAME:
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.MinigameStart ());
			break;
		case StateTitle.STUDY:
			SoundManage.instance.OffBgm ();
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.LearnStart ());
			break;
		case StateTitle.RESULT:
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.ResultPageStart ());
			break;
		}

	}
	void OnFail(){
		Debug.Log ("ErrorLoadUser");
	}
	void NoInfo(){
		switch (state) {
		case StateTitle.START:
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.FirstStart());
			break;
		case StateTitle.MINIGAME:

			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.MinigameStart ());
			break;
		case StateTitle.STUDY:
			SoundManage.instance.OffBgm ();
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.LearnStart ());
			break;
		case StateTitle.RESULT:
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.ResultPageStart ());
			break;
		}
	}
	public void start()
	{
		
		state = StateTitle.START;
		SoundManage.instance.efxButtonOn ();
		if (GPGSMng.GetInstance.bLogin) {
			if (!PlayerPrefs.HasKey ("SD_TRIAL_TH")
				&& !PlayerPrefs.HasKey ("SD_GRADE")
				&& !PlayerPrefs.HasKey ("SD_GAME_ID")
				&& !PlayerPrefs.HasKey ("SD_GUEST_TH")) {
				NetworkMng.GetInstance.RequestInfoAgain (MainUserInfo.GetInstance.userId);
			} else {

				NotLogin ();
			}
		} else {
			NotLogin ();
		}

		MainUserInfo.GetInstance.menu= PlayerPrefs.GetInt ("Menu");
		MainUserInfo.GetInstance.guestId = PlayerPrefs.GetInt ("GuestId");
		MainUserInfo.GetInstance.cream = PlayerPrefs.GetInt ("Cream");

	}
	public void minigame()
	{
		state = StateTitle.MINIGAME;
		SoundManage.instance.efxButtonOn ();
		if (GPGSMng.GetInstance.bLogin) {
			if (!PlayerPrefs.HasKey ("SD_TRIAL_TH")
				&& !PlayerPrefs.HasKey ("SD_GRADE")
				&& !PlayerPrefs.HasKey ("SD_GAME_ID")
				&& !PlayerPrefs.HasKey ("SD_GUEST_TH")) {
				NetworkMng.GetInstance.RequestInfoAgain (MainUserInfo.GetInstance.userId);
			} else {

				NotLogin ();
			}
		} else {
			NotLogin ();
		}
		MainUserInfo.GetInstance.menu= PlayerPrefs.GetInt ("Menu");
		MainUserInfo.GetInstance.guestId = PlayerPrefs.GetInt ("GuestId");
		MainUserInfo.GetInstance.cream = PlayerPrefs.GetInt ("Cream");

	}
	public void option()
	{
	}

    public void study()
    {
		state = StateTitle.STUDY;
		SoundManage.instance.efxButtonOn ();
		if (GPGSMng.GetInstance.bLogin) {
			if (!PlayerPrefs.HasKey ("SD_TRIAL_TH")
				&& !PlayerPrefs.HasKey ("SD_GRADE")
				&& !PlayerPrefs.HasKey ("SD_GAME_ID")
				&& !PlayerPrefs.HasKey ("SD_GUEST_TH")) {
				NetworkMng.GetInstance.RequestInfoAgain (MainUserInfo.GetInstance.userId);
			} else {
				
				NotLogin ();
			}
		}else {
			NotLogin ();
		}
		MainUserInfo.GetInstance.menu= PlayerPrefs.GetInt ("Menu");
		MainUserInfo.GetInstance.guestId = PlayerPrefs.GetInt ("GuestId");
		MainUserInfo.GetInstance.cream = PlayerPrefs.GetInt ("Cream");
    }
	public void ResultPage(){
		state = StateTitle.RESULT;
		SoundManage.instance.efxButtonOn ();
		//NetworkMng.GetInstance.SendAllData ();
		if (GPGSMng.GetInstance.bLogin) {
			if (!PlayerPrefs.HasKey ("SD_TRIAL_TH")
				&& !PlayerPrefs.HasKey ("SD_GRADE")
				&& !PlayerPrefs.HasKey ("SD_GAME_ID")
				&& !PlayerPrefs.HasKey ("SD_GUEST_TH")) {
				NetworkMng.GetInstance.RequestInfoAgain (MainUserInfo.GetInstance.userId);
			} else {

				NotLogin ();
			}
		}else {
			NotLogin ();
		}
		MainUserInfo.GetInstance.menu= PlayerPrefs.GetInt ("Menu");
		MainUserInfo.GetInstance.guestId = PlayerPrefs.GetInt ("GuestId");
		MainUserInfo.GetInstance.cream = PlayerPrefs.GetInt ("Cream");
		
	}
	void NotLogin(){
		MainUserInfo.GetInstance.userId = PlayerPrefs.GetString ("USER_ID");
		MainUserInfo.GetInstance.trialTh = PlayerPrefs.GetInt ("SD_TRIAL_TH");
		MainUserInfo.GetInstance.grade = PlayerPrefs.GetInt ("SD_GRADE");
		MainUserInfo.GetInstance.gameId= PlayerPrefs.GetInt ("SD_GAME_ID");
		MainUserInfo.GetInstance.guestTh = PlayerPrefs.GetInt ("SD_GUEST_TH");
		if (MainUserInfo.GetInstance.gameId == 0 && MainUserInfo.GetInstance.grade==0) {
			MainUserInfo.GetInstance.grade = 1;
			MainUserInfo.GetInstance.guestTh = 1;
			if (MainUserInfo.GetInstance.trialTh == 0) {
				MainUserInfo.GetInstance.trialTh = 1;
			}
			NoInfo ();
		} 
		else if(MainUserInfo.GetInstance.gameId == 0 && MainUserInfo.GetInstance.grade!=0)
		{
			NoInfo ();
		}else {
			OnSuccess ();
		}
	}
    public void exit()
	{
		SoundManage.instance.efxButtonOn ();
		pop.makeIt (PP.popupIndex.APPLICATIONQUIT);
    }
	void OnDestroy(){
		NetworkMng.GetInstance.onSuccess -= new NetworkMng.OnSuccess (OnSuccess);
		NetworkMng.GetInstance.noInfo -= new NetworkMng.OnNoInfo (NoInfo);
		NetworkMng.GetInstance.onFail -= new NetworkMng.OnFail (OnFail);
	}
}
