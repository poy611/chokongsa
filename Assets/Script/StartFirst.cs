﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StartFirst : MonoBehaviour {
	public GameObject Blink;
	private float speed = 3.0f;
	void Start () {
		//PlayerPrefs.DeleteAll ();
		StartCoroutine (BlackBlink());
	}


	IEnumerator BlackBlink()
	{
		//blink time
	
		float startTime = 3f;
		Blink.GetComponent<Image> ().color = Color.white;
		Blink.gameObject.SetActive(true);


		while (true)
		{
			startTime -= Time.deltaTime * speed;
			Blink.GetComponent<Image>().color = Color.LerpUnclamped(Blink.GetComponent<Image>().color, Color.clear, Time.deltaTime * speed);

			if (startTime <= 0) {
				break;
			}
			yield return null;
		}

		yield return new WaitForSeconds (1.0f);
		startTime = 3f;
		while (true)
		{
			startTime -= Time.deltaTime ;
			Blink.GetComponent<Image>().color = Color.LerpUnclamped(Blink.GetComponent<Image>().color, Color.white, Time.deltaTime * speed);

			if (startTime <= 0) {
				break;
			}
			yield return null;
		}
			
		SceneManager.LoadScene ("Title");
	}


}
