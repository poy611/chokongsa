﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LookandseeIt : MonoBehaviour {
	Sprite[] source;
	Image desk;
	Button prevbt;
	Button nextbt;
	Text tran;
	Text descript;
	Text pageNum;
	int cursur;

	string[] arrToolName = new string[]{"에스프레소","아메리카노","아메리카노","카페라테","카페라테","바닐라 라떼","바닐라 라떼",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"카라멜 프라푸치노", "모카 프라푸치노", "헤이질럿 프라푸치노"};
	string[] arrToolDscrpt	= new string[]{"에스프레소 설명","아메리카노 설명","아메리카노 설명","카페라테","카페라테","바닐라 라떼","바닐라 라떼",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"헤이질럿","헤이질럿","카페모카","카페모카","마끼야또","마끼야또",
		"카라멜 프라푸치노", "모카 프라푸치노", "헤이질럿 프라푸치노"};


	// Use this for initialization
	void Start () {
		source = Resources.LoadAll<Sprite> ("ingredient");
		desk = transform.Find ("learnPopup").transform.Find("tool_image").GetComponent<Image> ();
		prevbt = transform.Find ("learnPopup").transform.Find ("prev").GetComponent<Button> ();
		nextbt = transform.Find ("learnPopup").transform.Find ("next").GetComponent<Button> ();
		tran = transform.Find ("learnPopup").transform.Find("tool_name").GetComponent<Text> ();
		descript = transform.Find ("learnPopup").transform.Find("tool_description").GetComponent<Text> ();
		pageNum = transform.Find ("learnPopup").transform.Find ("pageNum").GetComponent<Text> ();
		cursur = 0;
		deskit ();
	}
	public void backButton()
	{
		SceneManager.LoadScene ("LEARNPAGE");
	}
	public void prev()
	{
		cursur--;
		deskit ();

	}
	public void next()
	{
		UnityEngine.Debug.Log ("LookandseeIt.next 1   cursur: " + cursur );
		cursur++;
		deskit ();
		Debug.Log ("pressed");
	}
	void deskit()
	{
		desk.sprite 	= source [cursur];
		tran.text 		= "" + arrToolName[cursur];
		descript.text 	= "" + arrToolDscrpt[cursur];
		pageNum.text 	= (cursur + 1) + "/" + source.Length;
		unableButton ();
	}
	void unableButton()
	{
		if (cursur < 1) {
			prevbt.interactable = false;
			cursur = 0;
		}
		else
			prevbt.interactable = true;
		if (cursur> source.Length-2) {
			nextbt.interactable = false;
			cursur = source.Length - 1;
		}
		else
			nextbt.interactable = true;
	}
}
