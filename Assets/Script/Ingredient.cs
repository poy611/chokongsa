﻿using UnityEngine;
using System.Collections;

public class Ingredient : MonoBehaviour {

/*	public static Ingredient ingredient;
	public static Ingredient GetInstance(){
		if (ingredient == null) {
			ingredient = new Ingredient ();
		}
		return ingredient;
	}*/

	public string BEAN_ORGN = "원두";
	public string BEAN_FLOOR = "원두가루";
	public string BEAN_ESPRESSO = "에스프레소";
	public string WATER_ORGN ="물";
	public string WATER_HOT = "뜨거운 물";
	public string WATER_COLD = "차가운 물";
	public string WATER_ICE = "얼음";
	public string MILK_ORGN = "우유";
	public string MILK_STEAM = "스팀 우유";
	public string MILK_COLD = "차가운 우유";
	public string SYRUP_CARAMEL = "카라멜 시럽";
	public string SYRUP_CHOCO = "초코 시럽";
	public string SYRUP_HAZELNUT="헤이즐넛 시럽";
	public string SYRUP_VANILLA = "바닐라 시럽";
	public string HCREAM_ORGN = "휘핑크림";
	public string HCREAM_MACCHIATO = "마끼아또 휘핑크림";
	public string HCREAM_CHOCO = "초코쿠키 휘핑크림";


	public string GetIngredientByNum(int num)
	{
		switch (num) {
		case 1:
			return BEAN_ORGN;
		case 2:
			return BEAN_FLOOR;
		case 3:
			return BEAN_ESPRESSO;
		case 4:
			return WATER_ORGN;
		case 5:
			return WATER_HOT;
		case 6:
			return WATER_COLD;
		case 7:
			return WATER_ICE;
		case 8:
			return MILK_ORGN;
		case 9:
			return MILK_STEAM;
		case 10:
			return MILK_COLD;
		case 21:
			return SYRUP_CARAMEL;
		case 22:
			return SYRUP_CHOCO;
		case 23:
			return SYRUP_HAZELNUT;
		case 24:
			return SYRUP_VANILLA;
		case 41:
			return HCREAM_ORGN;
		case 42:
			return HCREAM_MACCHIATO;
		case 43:
			return HCREAM_CHOCO;
		}
		return "";
	}
	public string GetIngredientUnit(int num)
	{
		switch (num) {
		case 1:
			return "g";
		case 2:
			return "g";
		case 3:
			return "잔";
		case 4:
			return "ml";
		case 5:
			return "ml";
		case 6:
			return "ml";
		case 7:
			return "g";
		case 8:
			return "ml";
		case 9:
			return "ml";
		case 10:
			return "ml";
		case 21:
			return "g";
		case 22:
			return "g";
		case 23:
			return "g";
		case 24:
			return "g";
		case 41:
			return "g";
		case 42:
			return "g";
		case 43:
			return "g";
		}
		return "";
	}

}
