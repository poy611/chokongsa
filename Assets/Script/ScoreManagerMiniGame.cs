﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;

using UnityEngine.UI;
public class ScoreManagerMiniGame : MonoBehaviour {


	int nowNum;
	string nowScene;
	float ticks = 0;


	public GameObject ready;
	StructforMinigame stuff;
	ConstforMinigame cuff;
	PopupManagement pop;

	Stopwatch sw;
	// Use this for initialization
	void Start () {

		nowNum = SceneManager.GetActiveScene ().buildIndex-1;
		nowScene = SceneManager.GetActiveScene ().name;
		pop = PopupManagement.p;
		stuff = StructforMinigame.getSingleton (nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade - 1);
		sw = stuff.Sw;
		sw.Reset ();
		pop.makeIt (PP.popupIndex.READY);
		PlayerPrefs.SetFloat ("LIMIT_TIME_F", cuff.Limittime);

	}

	public void StartMinigame(){
		ready.SetActive (true);
	}

	public void conFirm(){
		float timmerMinus=0;
		float correctRate = 0;
		float errorMinus = 0;
		float currentTime = PlayerPrefs.GetFloat ("TIME_NOW_F");
		int errorCount = PlayerPrefs.GetInt ("ERROR_NUM_I");
		stuff.Level = MainUserInfo.GetInstance.grade;
		//stuff.Misscount = missCount;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		sw.Stop ();
		sw.Reset ();
		stuff.Playtime = cuff.Limittime - currentTime;
		stuff.Misscount = errorCount ;
		timmerMinus = Math.Max (0, Math.Min (1, (stuff.Playtime - cuff.Besttime) / (cuff.Limittime - cuff.Besttime)));
		stuff.ErrorRate = stuff.Misscount * cuff.Errorpenalty;
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate - cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));
		UnityEngine.Debug.Log ("tM: " + timmerMinus);
		UnityEngine.Debug.Log ("eM: " + errorMinus);
		stuff.Score = 100 - (timmerMinus + errorMinus) * 50;
		pop.makeIt (PP.popupIndex.JUDGE);

	}
}
