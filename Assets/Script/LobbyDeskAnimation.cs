﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LobbyDeskAnimation : MonoBehaviour {
	public Image  desk;
	public void DeskCry(){
		StopAllCoroutines ();
		StartCoroutine (DeskCryCo());
	}
	IEnumerator DeskCryCo(){
		yield return new WaitForSecondsRealtime (5.0f);
		Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+MainUserInfo.GetInstance.guestId);
		desk.sprite = source;

	}
}
