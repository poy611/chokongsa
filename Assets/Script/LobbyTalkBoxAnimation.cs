﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LobbyTalkBoxAnimation : MonoBehaviour {
	
	public Image ShowmetheCoffee;
	public ScoreManager_Lobby scoreMng;
	// Use this for initialization

	public void ShowmetheCoffeSizeUp(){
		StopAllCoroutines ();
		StartCoroutine (ShowmetheCoffeSizeUpCo ());

	}
	IEnumerator ShowmetheCoffeSizeUpCo(){
		ShowmetheCoffee.gameObject.transform.localScale = new Vector3(0, 0, 0);
		ShowmetheCoffee.gameObject.SetActive(true);
		float limitSize = 0f;
		while (true) {
			
			float temp =5f * Time.deltaTime;
			limitSize += temp;
			ShowmetheCoffee.gameObject.transform.localScale += new Vector3(temp, temp, 0);

			if (limitSize > 1.3f) {
				Debug.Log ("BREAK");
				break;
			}
			yield return null;
		}
		yield return new WaitForSecondsRealtime (5.0f);
		ShowmetheCoffee.gameObject.SetActive(false);

		if (!scoreMng.creamButton.active) {
			scoreMng.SetTextOrder (MainUserInfo.GetInstance.guestId, MainUserInfo.GetInstance.menu, MainUserInfo.GetInstance.cream);
		} else {
			scoreMng.CreamText (MainUserInfo.GetInstance.guestId, MainUserInfo.GetInstance.cream);
		}

	}

}
