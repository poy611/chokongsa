﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MainUserInfo : Singleton<MainUserInfo> {

	public string userId;
	public string userName;
	public int grade;
	public int gameId;
	public int guestTh;
	public int trialTh;

	public int guestId;
	public int menu;
	public int cream;
	public List<UserExperience> userLog = new List<UserExperience>();
}
