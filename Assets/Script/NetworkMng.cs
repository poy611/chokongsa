﻿using UnityEngine;
using System.Collections;
using JsonFx.Json;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;

//	return JsonWriter.Serialize (dic);
//	return JsonReader.Deserialize<Dictionary<string,object>> (json);
[Serializable]
public class UserId{
	public string user_id;
}
public class NetworkMng : Singleton<NetworkMng> {
	private enum NetWorkState
	{
		LOGIN,
		USERINFO,
		LoadInfoAgain,
		SAVEGAMEDATA,
		NEWGAME,
		SAVERESULTDATA,
		NONE,
		RESULTALLDATA,
		CALCULATIONDATA

	}
	NetWorkState state;
	StructforMinigame stuff;
	public delegate void OnSuccess();
	public delegate void OnFail();
	public delegate void OnNoInfo();
	public delegate void OnAllResult (ResponseResultAllData info);
	public delegate void OnAllResultLoadFail();
	public delegate void OnSave();
	public delegate void OnAllResultFail();
	public OnSave onSave;
	public OnSuccess onSuccess;
	public OnFail onFail;
	public OnNoInfo noInfo;
	public OnAllResult onAllResult;
	public OnAllResultFail onAllResultFail;
	public OnAllResultLoadFail onAllResultLoadFail;
	public string errorLog;
	string urlFront="http://211.196.32.76:28118/";

	void Start(){
		state = NetWorkState.NONE;
		DontDestroyOnLoad (gameObject);
		//RequestUserInfo ("ss");
	}
	public WWW GET(string url)
	{
		WWW www = new WWW (url);
		StartCoroutine (WaitForRequest (www));
		return www; 
	}

	public WWW POST(string url, string data)
	{
		Dictionary<string,string> header = new Dictionary<string,string>();
		header.Add ("content-type", "application/json");
		WWW www = new WWW(url, Encoding.UTF8.GetBytes(data),header);
		StartCoroutine(WaitForRequest(www));
		return www; 
	}

	private IEnumerator WaitForRequest(WWW www)
	{
		yield return www;

		if (www.error == null)
		{
			Debug.Log (www.text);
			switch (state) {
			case NetWorkState.LOGIN:
				Login (www.text);
				break;
			case NetWorkState.USERINFO:
				InfoUser (www.text);
				break;
			case NetWorkState.LoadInfoAgain:
				InfoAgain (www.text);
				break;
			case NetWorkState.SAVEGAMEDATA:
				SaveGameData (www.text);
				break;
			case NetWorkState.NEWGAME:
				NewGame(www.text);
				break;
			case NetWorkState.SAVERESULTDATA:
				SaveResultData (www.text);
				break;
			case NetWorkState.RESULTALLDATA:
				AllData (www.text);
				break;
			case NetWorkState.CALCULATIONDATA:
				AllCalculationData (www.text);
				break;
			}
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}
	/////////////////////////////////
	/// Login
	public void RequestLogin(string user_id, string user_name){
		state = NetWorkState.LOGIN;
		UserInfo user = new UserInfo ();
		user.user_id = user_id;
		user.user_name = user_name;
		string str = JsonWriter.Serialize (user);
		POST (urlFront + "user/joinProc.do", str);


	}
	public void Login(string str)
	{
		ResponseInfo info = new ResponseInfo ();
		info = JsonReader.Deserialize<ResponseInfo> (str);
		if (info == null) {
		}
		else if (info.resultCode == 3041) { //Aleady 
			Debug.Log ("Login");
		} else if (info.resultCode == 2300) { // New User
			Debug.Log ("New User");
		} else {
			Debug.Log ("NetWork Error");
		}
		state = NetWorkState.NONE;
	}
	/// EndOfLogin
	//////////////////////////////

	//////////////////////////////
	public void RequestUserInfo(string user_id){
		state = NetWorkState.USERINFO;
		UserInfo user = new UserInfo ();
		user.user_id = user_id;
		string str = JsonWriter.Serialize (user);

		POST (urlFront + "user/userinfo.do", str);
	}

	public void InfoUser(string str)
	{
		ResponseInfoType info = new ResponseInfoType();
		info = JsonReader.Deserialize<ResponseInfoType> (str);
		if (info.resultCode == 2301) { //sucess
	
		} else if(info.resultCode == 3040) { //fail
		}
		else{ // no Info
		}
		state = NetWorkState.NONE;
	}
	/////////////////////////////////

	/// //////////////////////////////
	public void RequestInfoAgain(string user_id)
	{
		state = NetWorkState.LoadInfoAgain;
		UserId user = new UserId();
		user.user_id = user_id;
		string str = JsonWriter.Serialize (user);
		POST(urlFront + "savedata/playDataLoad.do" , str);

	}
	public void InfoAgain(string str){
		ResponseSaveData info = new ResponseSaveData ();
		info = JsonReader.Deserialize<ResponseSaveData> (str);

		if (info.resultCode == 2303) { //success
			
			if (info.resultData.sdGameId == 0 && info.resultData.sdGrade==0) {
				MainUserInfo.GetInstance.grade = 1;
				MainUserInfo.GetInstance.guestTh = 1;
				MainUserInfo.GetInstance.trialTh = info.resultData.sdTrialTh;
				noInfo ();
			} 
			else if(info.resultData.sdGameId == 0 && info.resultData.sdGrade!=0)
			{
				MainUserInfo.GetInstance.grade = info.resultData.sdGrade;
				MainUserInfo.GetInstance.guestTh = 1;
				MainUserInfo.GetInstance.trialTh = info.resultData.sdTrialTh;
				noInfo ();
			}else {
				
				MainUserInfo.GetInstance.grade=info.resultData.sdGrade;
				MainUserInfo.GetInstance.guestTh=info.resultData.sdGuestTh;
				MainUserInfo.GetInstance.gameId = info.resultData.sdGameId;
				MainUserInfo.GetInstance.trialTh =info.resultData.sdTrialTh;
				onSuccess ();


			}
		} else if (info.resultCode == 3044) { //no info
			noInfo();
		} else { //fail
			onFail();
		}
		state = NetWorkState.NONE;
	}
	////////////////////////////////////

	////////////////////////////////////

	/////////////////////////////////////////////
	/// 
	/////////////////////////////////////////////
	public void RequestNewGame(){
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveData.txt");

		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		ResultData resultData = new ResultData();
		if(str!=null && str!=""){
			resultData = JsonReader.Deserialize<ResultData> (str);
		}
		ResultData tempResultData = new ResultData();
		for(int i=0; i<resultData.resultData.Count; i++){
			if(resultData.resultData[i].prctc_grade==MainUserInfo.GetInstance.grade
				&& resultData.resultData[i].prctc_trial_th == MainUserInfo.GetInstance.trialTh){
			}
			else{
			tempResultData.resultData.Add(resultData.resultData[i]);
			}
		}


		if(tempResultData.resultData.Count>0){
			str = JsonWriter.Serialize(tempResultData);
			FileStream  f = new FileStream( path, FileMode.Create, FileAccess.Write);
			StreamWriter writer = new StreamWriter(f);
			writer.WriteLine(str);
			writer.Close();
			f.Close();
		}
		else{
			FileStream  f = new FileStream(path, FileMode.Create, FileAccess.Write);
			StreamWriter writer = new StreamWriter(f);
			writer.Close();
			f.Close();
		}


		#else

		FileStream file = new FileStream (m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		ResultData resultData = new ResultData();
		if(str!=null && str!=""){
			resultData = JsonReader.Deserialize<ResultData> (str);
		}
		ResultData tempResultData = new ResultData();
		for(int i=0; i<resultData.resultData.Count; i++){
			if(resultData.resultData[i].prctc_grade==MainUserInfo.GetInstance.grade
				&& resultData.resultData[i].prctc_trial_th == MainUserInfo.GetInstance.trialTh){
			}
			else{
				tempResultData.resultData.Add(resultData.resultData[i]);
			}
		}


		if(tempResultData.resultData.Count>0){
			str = JsonWriter.Serialize(tempResultData);
			FileStream  f = new FileStream( m_strPath  + "SaveData.txt", FileMode.Create, FileAccess.Write);
			StreamWriter writer = new StreamWriter(f);
			writer.WriteLine(str);
			writer.Close();
			f.Close();
		}
		else{
			FileStream  f = new FileStream( m_strPath  + "SaveData.txt", FileMode.Create, FileAccess.Write);
			StreamWriter writer = new StreamWriter(f);
			writer.Close();
			f.Close();
		}

		#endif
		//StartCoroutine (NewGameCouroutine ());
	}
	IEnumerator NewGameCouroutine(){
		yield return null;
		state = NetWorkState.NEWGAME;
		UserId user = new UserId();
		user.user_id = MainUserInfo.GetInstance.userId;
		string str = JsonWriter.Serialize (user);
		POST(urlFront + "savedata/playDataReset.do" , str);


	}
	public void NewGame(string str){
		ResponseInfo info = new ResponseInfo ();
		info = JsonReader.Deserialize<ResponseInfo> (str);
		if (info.resultCode == 2304) { //success
	
		} else {

		}
		state = NetWorkState.NONE;
	}
	/////////////////////////////////////////////
	/// 
	/////////////////////////////////////////////
	public void RequestSaveGameDataInLocal(){
		PlayerPrefs.SetString ("USER_ID",MainUserInfo.GetInstance.userId);
		PlayerPrefs.SetInt ("SD_TRIAL_TH",MainUserInfo.GetInstance.trialTh);
		PlayerPrefs.SetInt ("SD_GRADE",MainUserInfo.GetInstance.grade);
		PlayerPrefs.SetInt ("SD_GAME_ID",MainUserInfo.GetInstance.gameId);
		PlayerPrefs.SetInt ("SD_GUEST_TH",MainUserInfo.GetInstance.guestTh);

	}
	public void RequestSaveGameData(int gameId , int trialTh, int grade,int guestTh){
		state = NetWorkState.SAVEGAMEDATA;
		StartCoroutine (SaveGameDataCouroutine(gameId,trialTh,grade,guestTh));
	
	}
	IEnumerator SaveGameDataCouroutine(int gameId , int trialTh, int grade,int guestTh)
	{
		yield return null;
		SaveData save = new SaveData ();
		save.user_id = MainUserInfo.GetInstance.userId;
		save.sd_trial_th = trialTh;
		save.sd_grade = grade;
		save.sd_game_id = gameId;
		save.sd_guest_th = guestTh;
		string str = JsonWriter.Serialize (save);
		Debug.Log (str);
		POST (urlFront + "savedata/playDataSave.do", str);
	}
	public void SaveGameData(string str){

		ResponseInfo info = new ResponseInfo ();
		info = JsonReader.Deserialize<ResponseInfo> (str);
		if (info == null) {
		}
		else if (info.resultCode == 2302) { //success
			
		} else {
	
		}
		state = NetWorkState.NONE;
	}
	public void RequestSaveResultData(int nowNum, int gameId ,int grade,int guestTh,int trialTh){
		stuff = StructforMinigame.getSingleton (nowNum);
		ResultSaveData info = new ResultSaveData ();
		info.user_id = MainUserInfo.GetInstance.userId;
		info.prctc_grade = grade;
		info.prctc_game_id = gameId;
		info.prctc_guest_th = guestTh;
		info.prctc_trial_th = trialTh;
		info.prctc_score = stuff.Score;
		info.prctc_play_time = stuff.Playtime;
		info.prctc_miss_cnt = stuff.Misscount;
		info.prctc_error_range = stuff.ErrorRate;
		ReadAndWrite (info);
		//StartCoroutine (SaveResultDataCouroutine (nowNum,gameId, grade, guestTh));
	}
	IEnumerator SaveResultDataCouroutine(int nowNum, int gameId , int grade,int guestTh){
		while (state!=NetWorkState.NONE) {
			yield return null;
		}
		state = NetWorkState.SAVERESULTDATA;
		stuff = StructforMinigame.getSingleton (nowNum);
		ResultSaveData info = new ResultSaveData ();
		info.user_id = MainUserInfo.GetInstance.userId;
		info.prctc_grade = grade;
		info.prctc_game_id = gameId;
		info.prctc_guest_th = guestTh;
		info.prctc_score = stuff.Score;
		info.prctc_play_time = stuff.Playtime;
		info.prctc_miss_cnt = stuff.Misscount;
		info.prctc_error_range = stuff.ErrorRate;
		string str=JsonWriter.Serialize (info);
		POST (urlFront + "prctcinfo/prctcInfoSave.do", str);



	}
	public void SaveResultData(string str){
		ResponseInfo info = new ResponseInfo ();
		info = JsonReader.Deserialize<ResponseInfo> (str);
		if (info == null) {
			onAllResultFail ();
		}
		else if (info.resultCode == 2305) { //success
			onSave ();
		} else {
			onAllResultFail ();
		}
		state = NetWorkState.NONE;
	}
	public void RequestResullAllData(int startNum, int endNum){
		StartCoroutine (RequstResultAllDataCouroutine (startNum, endNum));
	}
	IEnumerator RequstResultAllDataCouroutine(int startNum, int endNum){
		while (state!=NetWorkState.NONE) {
			yield return null;
		}
		state = NetWorkState.RESULTALLDATA;
		UserInfoCall user = new UserInfoCall();
		user.user_id = MainUserInfo.GetInstance.userId;
		user.start_num = ""+startNum;
		user.end_num = "" + endNum;

		string str = JsonWriter.Serialize (user);
		Debug.Log (str);
		POST (urlFront + "prctcinfo/prctcInfoView.do", str);
	
	}
	public void AllData(string str){
		ResponseResultAllData info = new ResponseResultAllData ();
		try{
			info = JsonReader.Deserialize<ResponseResultAllData> (str);
		}
		catch(Exception e){
			onAllResultLoadFail ();
		}

		if (info == null) {
			onAllResultLoadFail ();
		}
		else if (info.resultCode == 2306) {
			onAllResult (info);
		} else {
			onAllResultLoadFail ();
		}
		state = NetWorkState.NONE;
	}
	public void AllCalculationData(string str){
		ResponseInfo info = new ResponseInfo ();
		info = JsonReader.Deserialize<ResponseInfo> (str);
		if (info == null) {
			
		}
		else if (info.resultCode == 2307) {
			DeleteCalculationLogFile ();
		} else {

		}
		state = NetWorkState.NONE;
	}
	string m_strPath = "Assets/Resources/";

	public void WriteSaveData(string strData)
	{
		#if !UNITY_EDITOR
			string path = pathForDocumentsFile("SaveData.txt");
			FileStream file = new FileStream (path, FileMode.Append, FileAccess.Write);

			StreamWriter sw = new StreamWriter( file );
			sw.WriteLine( strData );
			
			sw.Flush();
			sw.Close();
			file.Close();
		#else
			FileStream  f = new FileStream( m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter writer = new StreamWriter(f);
			writer.WriteLine(strData);
			writer.Flush ();
			writer.Close();
			f.Close();
		#endif 



	}

	public ResultData ReadSaveData()
	{
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveData.txt");

		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();
		ResultData resultData = new ResultData();
		if(str!=null && str!=""){
		resultData = JsonReader.Deserialize<ResultData> (str);
		}
		sr.Close();
		file.Close();
		return resultData;
	
		#else

		FileStream file = new FileStream (m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();
		ResultData resultData = new ResultData();
		if(str!=null && str!=""){
			resultData = JsonReader.Deserialize<ResultData> (str);
		}
		sr.Close();
		file.Close();
		return resultData;
		#endif

	}
	public void SendAllData(){
		state = NetWorkState.SAVERESULTDATA;
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveData.txt");

		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();

		sr.Close();
		file.Close();
		if(str!=null){
			POST (urlFront + "prctcinfo/prctcInfoSave.do", str);
		}
		#else

		FileStream file = new FileStream (m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();

		sr.Close();
		file.Close();
		if(str!=null){
			POST (urlFront + "prctcinfo/prctcInfoSave.do", str);
		}
		#endif

	}
	public void SendAllCalculationData(){
		state = NetWorkState.CALCULATIONDATA;
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveCalculationData.txt");
		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();
		sr.Close();
		file.Close();
		if(str!=null){
			POST (urlFront + "numccltinfo/numCcltInfoSave.do", str);
		}
		#else

		FileStream file = new FileStream (m_strPath  + "SaveCalculationData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = sr.ReadLine ();
		sr.Close();
		file.Close();
		if(str!=null){
			POST (urlFront + "numccltinfo/numCcltInfoSave.do", str);
		}
		#endif
	
	}
	public void ReadAndWrite(ResultSaveData resultSaveData){
		
		#if !UNITY_EDITOR
		try{
		string path = pathForDocumentsFile("SaveData.txt");
		Debug.Log("SaveFile Path : " +path);
		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		ResultData resultData = new ResultData();
		Debug.Log("ss"+str);
		if(str!=null && str!=""){
		resultData = JsonReader.Deserialize<ResultData> (str);
		}
		resultData.resultData.Add(resultSaveData);
		str = JsonWriter.Serialize(resultData);
		FileStream  f = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);
		writer.WriteLine(str);

		writer.Close();
		f.Close();
		}
		catch(Exception e){
		errorLog = e.StackTrace;
		Debug.LogException(e);
		PopupManagement pop;
		pop = PopupManagement.p;
		pop.makeIt(PP.popupIndex.ERROR);
		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));


		}


		#else
	

		FileStream file = new FileStream (m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		ResultData resultData = new ResultData();
		if(str!=null && str!=""){
			resultData = JsonReader.Deserialize<ResultData> (str);
		}
		resultData.resultData.Add(resultSaveData);
		str = JsonWriter.Serialize(resultData);
		FileStream  f = new FileStream( m_strPath  + "SaveData.txt", FileMode.OpenOrCreate, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);
		writer.WriteLine(str);

		writer.Close();
		f.Close();
		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
		#endif
	}
	public void ReadAndWriteCalculationLog(CalculationData resultSaveData){
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveCalculationData.txt");

		FileStream file = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		CaculationResultData resultData = new CaculationResultData();
		Debug.Log("ss"+str);
		if(str!=null && str!=""){
		resultData = JsonReader.Deserialize<CaculationResultData> (str);
		}
		resultData.resultData.Add(resultSaveData);
		str = JsonWriter.Serialize(resultData);
		FileStream  f = new FileStream (path, FileMode.OpenOrCreate, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);
		writer.WriteLine(str);

		writer.Close();
		f.Close();

		#else

		FileStream file = new FileStream (m_strPath  + "SaveCalculationData.txt", FileMode.OpenOrCreate, FileAccess.Read);
		StreamReader sr = new StreamReader( file );
		string str = null;
		str = sr.ReadLine ();
		sr.Close();
		file.Close();
		CaculationResultData resultData = new CaculationResultData();
		if(str!=null && str!=""){
			resultData = JsonReader.Deserialize<CaculationResultData> (str);
		}
		resultData.resultData.Add(resultSaveData);
		str = JsonWriter.Serialize(resultData);
		FileStream  f = new FileStream( m_strPath  + "SaveCalculationData.txt", FileMode.OpenOrCreate, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);
		writer.WriteLine(str);

		writer.Close();
		f.Close();
		#endif
	}
	public string pathForDocumentsFile( string filename ) 
	{ 
		if (Application.platform == RuntimePlatform.IPhonePlayer){
			string path = Application.persistentDataPath.Substring( 0, Application.persistentDataPath.Length - 5 );
			path = path.Substring( 0, path.LastIndexOf( '/' ) );
			return Path.Combine( Path.Combine( path, "Documents" ), filename );
		} else if (Application.platform == RuntimePlatform.Android) {
			string path = Application.persistentDataPath; 
			path = path.Substring (0, path.LastIndexOf ('/')); 
			return Path.Combine (path, filename);
		} else {
			return null;
		}
	}
	public void DeleteLogFile(){
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveData.txt");
		FileStream  f = new FileStream( path, FileMode.Create, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);
	
		writer.Close();
		f.Close();

		#else

		FileStream  f = new FileStream( m_strPath  + "SaveData.txt", FileMode.Create, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);

		writer.Close();
		f.Close();


		#endif
	}
	public void DeleteCalculationLogFile(){
		#if !UNITY_EDITOR
		string path = pathForDocumentsFile("SaveCalculationData.txt");
		FileStream  f = new FileStream( path, FileMode.Create, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);

		writer.Close();
		f.Close();

		#else

		FileStream  f = new FileStream( m_strPath  + "SaveCalculationData.txt", FileMode.Create, FileAccess.Write);
		StreamWriter writer = new StreamWriter(f);

		writer.Close();
		f.Close();


		#endif
	}
	void OnApplicationQuit(){
		Debug.Log ("OnApplicationQuit");
		if(GPGSMng.GetInstance.bLogin){
			NetworkMng.GetInstance.RequestSaveGameData (
				PlayerPrefs.GetInt ("SD_GAME_ID"),
				PlayerPrefs.GetInt ("SD_TRIAL_TH"),
				PlayerPrefs.GetInt ("SD_GRADE"),
				PlayerPrefs.GetInt ("SD_GUEST_TH")
			);

		}
	}
}
