﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class ResultPageManager : MonoBehaviour {
	[SerializeField]
	Image[] scores;
	[SerializeField]
	Text[] texts;
	[SerializeField]
	Text pageText;
	public Button prev;
	public Button next;

	int currentPage =1;
	int LastPage = 1;
	public int trialFirstIndex;
	int reverse=0;
	public ResponseResultAllData allData;
	void Awake () {
		//NetWorkMng call..
		//OnSuccessLoadData();
		trialFirstIndex= MainUserInfo.GetInstance.trialTh-1;
		NetworkMng.GetInstance.onAllResult += new NetworkMng.OnAllResult (OnSuccessLoadData);
		NetworkMng.GetInstance.onAllResultLoadFail += new NetworkMng.OnAllResultLoadFail (OnFailLoadData);

		if (trialFirstIndex <= 0) {
			OnFailLoadData ();
		} else {
			NetworkMng.GetInstance.RequestResullAllData (trialFirstIndex - 8 >= 0 ? trialFirstIndex - 8 : 0, trialFirstIndex);
		}
	}

	void OnSuccessLoadData(ResponseResultAllData allData){
		Debug.Log ("OnSuccessLoadData");
		this.allData = allData;
		transform.FindChild ("MAIN").transform.FindChild ("ScoreWindow").gameObject.SetActive (true);

		if (MainUserInfo.GetInstance.trialTh % 8 > 0) {
			LastPage = MainUserInfo.GetInstance.trialTh / 8 + 1;
		} else {
			LastPage = MainUserInfo.GetInstance.trialTh / 8;
		}
		if (LastPage == 1) {
			prev.interactable = false;
			next.interactable = false;
		}
		currentPage = 1;
		pageText.text = currentPage + "/" + LastPage;
		SetTrialImage ();
	}
	void OnFailLoadData(){
		transform.FindChild ("MAIN").transform.FindChild ("NoInfo").gameObject.SetActive (true);

	}
	void csSwitch(float cs)
	{	
		if(cs<60)
			reverse = (int)CONSTforPOP.score.F;
		else if(cs<63)
			reverse = (int)CONSTforPOP.score.DM;
		else if(cs<67)
			reverse = (int)CONSTforPOP.score.D;
		else if(cs<70)
			reverse = (int)CONSTforPOP.score.DP;
		else if(cs<73)
			reverse = (int)CONSTforPOP.score.CM;
		else if(cs<77)
			reverse = (int)CONSTforPOP.score.C;
		else if(cs<80)
			reverse = (int)CONSTforPOP.score.CP;
		else if(cs<83)
			reverse = (int)CONSTforPOP.score.BM;
		else if(cs<87)
			reverse = (int)CONSTforPOP.score.B;
		else if(cs<90)
			reverse = (int)CONSTforPOP.score.BP;
		else if(cs<94)
			reverse = (int)CONSTforPOP.score.AM;
		else if(cs<97)
			reverse = (int)CONSTforPOP.score.A;
		else if(cs<=100)
			reverse = (int)CONSTforPOP.score.AP;
	}
	void SetTrialImage(){
		Sprite[] source = Resources.LoadAll<Sprite> ("score");
		for (int i = 0; i < 8; i++) {
			if (trialFirstIndex - i <= 0) {
				scores [i].enabled = false;
				texts [i].enabled = false;
			} else {
				scores [i].enabled = true;
				texts [i].enabled = true;
				csSwitch (AverageScoreByTrial(trialFirstIndex - i));
				scores [i].sprite = source [(int)reverse];
				texts [i].text = (trialFirstIndex - i) + "회";
			}
		}
	}
	float AverageScoreByTrial(int trialTh){
		float score = 0f;
		int count = 0;
		for (int i = 0; i < this.allData.resultData.prctcInfoList.Count; i++) {
			if (this.allData.resultData.prctcInfoList [i].prctcTrialTh == trialTh) {
				score += this.allData.resultData.prctcInfoList [i].prctcScore;
				count++;
			}
		}
		return (float)score / count;

	}
	public void NextPageButton(){
		if (currentPage < LastPage) {
			currentPage++;
		}
		if (currentPage == LastPage) {
			next.interactable = false;
		}
		prev.interactable = true;
		trialFirstIndex -= 8;
		NetworkMng.GetInstance.RequestResullAllData(trialFirstIndex-8 > 0 ? trialFirstIndex-8 : 0, trialFirstIndex);
	}
	public void PrevPageButton(){
		if (currentPage > 1) {
			currentPage--;
		}
		if (currentPage == 1) {
			prev.interactable = false;
		}
		next.interactable = true;
		trialFirstIndex += 8;
		NetworkMng.GetInstance.RequestResullAllData(trialFirstIndex-8 > 0 ? trialFirstIndex-8 : 0, trialFirstIndex);
	}
	public void AverageByTrial(int index){
		
	
		Sprite[] source = Resources.LoadAll<Sprite> ("score");
		GameObject _obj = transform.FindChild ("MAIN").gameObject;
		Sprite sprite = _obj.transform.FindChild ("ScoreWindow").transform.FindChild ("Score" + index).GetComponent<Image> ().sprite;
		string str = _obj.transform.FindChild ("ScoreWindow").transform.FindChild ("Score" + index).transform.FindChild ("Text").GetComponent<Text> ().text;
		int trialTh = Convert.ToInt32(str.Substring (0, str.Length-1));
		_obj.transform.FindChild ("List").gameObject.SetActive (false);
		_obj.transform.FindChild ("NoInfo").gameObject.SetActive (false);
		_obj.transform.FindChild ("ScoreWindow").gameObject.SetActive (false);
		GameObject detailList = _obj.transform.FindChild ("DetailList").gameObject;
		detailList.SetActive (true);
		detailList.transform.FindChild ("MainScore").GetComponent<Image> ().sprite = sprite;
		detailList.transform.FindChild ("Title").GetComponent<Text> ().text = trialTh+"회 최종결과";
		for (int grade = 1; grade <= 4; grade++) {
			float[] tempArray = new float[6];
			int[] countOfArray = new int[6];
			for (int p = 0; p < this.allData.resultData.prctcInfoList.Count; p++) {
				if (this.allData.resultData.prctcInfoList [p].prctcGrade == grade
				   && this.allData.resultData.prctcInfoList [p].prctcTrialTh == trialTh) {
					tempArray [this.allData.resultData.prctcInfoList [p].prctcGameId-1] += this.allData.resultData.prctcInfoList [p].prctcScore;
					countOfArray [this.allData.resultData.prctcInfoList [p].prctcGameId-1]++;
				}
			}

			float gradeScore = 0f;
			int countGrade = 0;

			for(int i=0; i<tempArray.Length; i++){
				Image img;
				Text text;
				gradeScore += tempArray [i];
				countGrade += countOfArray [i];
				switch (i) {
				case 0:
					img = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Lobby").GetComponent<Image> ();
					text = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Lobby").transform.FindChild("LobbyText").GetComponent<Text> ();

					if (countOfArray [i] > 0) {
						img.enabled = true;
						text.enabled = true;
						csSwitch ((float)tempArray [i] / countOfArray [i]);
						img.sprite = source [(int)reverse];
					} else {
						text.enabled = false;
						img.enabled = false;
					}
					break;
				case 1:
					img = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Grinding").GetComponent<Image> ();
					text = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Grinding").transform.FindChild("GrindingText").GetComponent<Text> ();

					if (countOfArray [i] > 0) {
						img.enabled = true;
						text.enabled = true;
						csSwitch ((float)tempArray [i] / countOfArray [i]);
						img.sprite = source [(int)reverse];
					} else {
						text.enabled = false;
						img.enabled = false;
					}
					break;
				case 2:
					img = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Temping").GetComponent<Image> ();
					text = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Temping").transform.FindChild("TempingText").GetComponent<Text> ();

					if (countOfArray [i] > 0) {
						img.enabled = true;
						text.enabled = true;
						csSwitch ((float)tempArray [i] / countOfArray [i]);
						img.sprite = source [(int)reverse];
					} else {
						text.enabled = false;
						img.enabled = false;
					}
					break;
				case 3:
					break;
				case 4:
					img = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Icing").GetComponent<Image> ();
					text = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Icing").transform.FindChild("IcingText").GetComponent<Text> ();

					if (countOfArray [i] > 0) {
						img.enabled = true;
						text.enabled = true;
						csSwitch ((float)tempArray [i] / countOfArray [i]);
						img.sprite = source [(int)reverse];
					} else {
						text.enabled = false;
						img.enabled = false;
					}
					break;
				case 5:
					img = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Variation").GetComponent<Image> ();
					text = detailList.transform.FindChild ("Result" + grade + "st").transform.FindChild ("Variation").transform.FindChild("VariationText").GetComponent<Text> ();

					if (countOfArray [i] > 0) {
						img.enabled = true;
						text.enabled = true;
						csSwitch ((float)tempArray [i] / countOfArray [i]);
						img.sprite = source [(int)reverse];
					} else {
						text.enabled = false;
						img.enabled = false;
					}
					break;
				}

			}
			csSwitch ((float)gradeScore / countGrade);
			detailList.transform.FindChild (grade+"stImage").GetComponent<Image> ().sprite = source [(int)reverse];

			
		}


	}
	void OnDestroy(){
		NetworkMng.GetInstance.onAllResult -= new NetworkMng.OnAllResult (OnSuccessLoadData);
		NetworkMng.GetInstance.onAllResultLoadFail -= new NetworkMng.OnAllResultLoadFail (OnFailLoadData);

	}
}
