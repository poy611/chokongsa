﻿using UnityEngine;
using System.Collections;

public class RecipeList : MonoBehaviour {
	public static RecipeList instance = new RecipeList();
	public static RecipeList GetInstance(){
		if (instance == null) {
			instance = new RecipeList ();
		}
		return instance;
	}
	public string[] coffeeImgList = new string[]{
		"CoffeeImgs/icon_coffee_01",
		"CoffeeImgs/icon_coffee_02",
		"CoffeeImgs/icon_coffee_03",
		"CoffeeImgs/icon_coffee_04",
		"CoffeeImgs/icon_coffee_05",
		"CoffeeImgs/icon_coffee_06",
		"CoffeeImgs/icon_coffee_07",
		"CoffeeImgs/icon_coffee_08",
		"CoffeeImgs/icon_coffee_09",
		"CoffeeImgs/icon_coffee_10",
		"CoffeeImgs/icon_coffee_11",
		"CoffeeImgs/icon_coffee_12",
		"CoffeeImgs/icon_coffee_13",
		"CoffeeImgs/icon_coffee_14",
		"CoffeeImgs/icon_coffee_15",
		"CoffeeImgs/icon_coffee_16",
	};
	public Ingredient ingredient = new Ingredient ();
	public MenuList recipeList = new MenuList();
	public int[][] lobbyRecipe = new int[][]{
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{}
	};
	public int[][] coffeeBeanRecipe = new int[][] {
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1},
		new int[]{1}
	};
	public int[][] extractionRecipe = new int[][] {
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4},
		new int[]{4}
	};
	public int[][] steamMilkRecipe = new int[][] {
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{8},
		new int[]{},
		new int[]{8},
		new int[]{},
		new int[]{8},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{}
	};
	public int[][] iceMixerRecipe = new int[][]{
	
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{},
		new int[]{7,3,10,21,41},
		new int[]{7,3,10,22,41},
		new int[]{7,3,10,23,41}
	};
	public int[][] variationRecipe = new int[][]{
		new int[]{3},
		new int[]{3,5},
		new int[]{7,3},
		new int[]{3,9},
		new int[]{7,3,10},
		new int[]{3,9,24},
		new int[]{7,3,10,24},
		new int[]{3,9,23},
		new int[]{7,3,23},
		new int[]{3,9,22},
		new int[]{7,3,10,22},
		new int[]{3,9,21},
		new int[]{7,3,10,21},
		new int[]{},
		new int[]{},
		new int[]{}
	};
	public int[][] randomValueByRecipe = new int[][] {
		new int[]{12,12}, //1
		new int[]{12,12}, //2
		new int[]{1,1},   //3
		new int[]{32,32}, //4
		new int[]{180,180}, //5
		new int[]{12,12}, //6
		new int[]{150,150}, //7
		new int[]{12,12}, //8
		new int[]{140,140}, //9
		new int[]{140,140}, //10
		new int[]{12,12}, //1
		new int[]{12,12}, //2
		new int[]{1,2},   //3
		new int[]{12,12}, //4
		new int[]{180,180}, //5
		new int[]{12,12}, //6
		new int[]{150,150}, //7
		new int[]{12,12}, //8
		new int[]{140,140}, //9
		new int[]{140,140}, //20
		new int[]{12,12}, //1
		new int[]{12,12}, //2
		new int[]{1,2},   //3
		new int[]{12,12}, //4
		new int[]{180,180}, //5
		new int[]{12,12}, //6
		new int[]{150,150}, //7
		new int[]{12,12}, //8
		new int[]{140,140}, //9
		new int[]{140,140}, //30
		new int[]{12,12}, //1
		new int[]{12,12}, //2
		new int[]{1,2},   //3
		new int[]{12,12}, //4
		new int[]{180,180}, //5
		new int[]{12,12}, //6
		new int[]{150,150}, //7
		new int[]{12,12}, //8
		new int[]{140,140}, //9
		new int[]{140,140}, //40
		new int[]{20,20}, //1
		new int[]{12,12}, //2
		new int[]{1,2},   //3
		new int[]{12,12}, //4
		new int[]{180,180}, //5
		new int[]{12,12}, //6
		new int[]{150,150}, //7
		new int[]{12,12}, //8
		new int[]{140,140}, //9
		new int[]{140,140}, //50
	};

}
