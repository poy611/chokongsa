﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
public class ShowRecipePre : MonoBehaviour {

	public Text menuName;
	public Text[] recipes;
	public Image coffeeImg;
	public Image creamImg;
	public Transform[] recipeTrans;
	public Transform imgTrans;
	public Image[] checks;
	public RecipeUpDown UpDown;
	public List<string> menuList = new List<string> ();
	public List<string> unitList = new List<string> ();
	public int currentIngredient=0;
	string nowScene;
	// Use this for initialization
	void Start () {
		//imgTrans.position = recipeTrans [0].position;
		nowScene = SceneManager.GetActiveScene ().name;
		//imgTrans.position = recipeTrans [currentIngredient].position;
		menuName.text = RecipeList.GetInstance ().recipeList.GetMenuName (MainUserInfo.GetInstance.menu);
		if (MainUserInfo.GetInstance.cream == 0) {
			creamImg.enabled = true;
		}
		if (nowScene == "4_Variation_proto") {
			for (int i = 0; i < RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length; i++) {
				float temp1 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [i] - 1] [0];
				float temp2 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [i] - 1] [1];

				string randomWeightString = "" + (float)Math.Floor (UnityEngine.Random.Range (temp1, temp2 + 1)) + "" + RecipeList.GetInstance ().ingredient.GetIngredientUnit (RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [i]);
				string menu = RecipeList.GetInstance ().ingredient.GetIngredientByNum (RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [i]);
				menuList.Add (menu);
				unitList.Add (randomWeightString);
				recipes [i].text = menu + " "+ randomWeightString;
			}
			if(MainUserInfo.GetInstance.cream==0){ // Cream In
				float temp1 = RecipeList.GetInstance ().randomValueByRecipe [41-1][0];
				float temp2 = RecipeList.GetInstance ().randomValueByRecipe [41-1][1];
				string randomWeightString = "" + (float)Math.Floor (UnityEngine.Random.Range (temp1, temp2 + 1)) + "" + RecipeList.GetInstance ().ingredient.GetIngredientUnit (41);

				string menu = RecipeList.GetInstance ().ingredient.GetIngredientByNum (41);
				menuList.Add (menu);
				unitList.Add (randomWeightString);
				recipes [RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length].text = menu + " " + randomWeightString;

			}
		} 
		else if(nowScene == "Icing"){
			for (int i = 0; i < RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu].Length; i++) {
				float temp1 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu] [i] - 1] [0];
				float temp2 = RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu] [i] - 1] [1];

				string randomWeightString = "" + (float)Math.Floor (UnityEngine.Random.Range (temp1, temp2 + 1)) + "" + RecipeList.GetInstance ().ingredient.GetIngredientUnit (RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu] [i]);
				string menu = RecipeList.GetInstance ().ingredient.GetIngredientByNum (RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu] [i]);
					menuList.Add (menu);
					unitList.Add (randomWeightString);
					recipes [i].text = menu + " "+ randomWeightString;
				}
		}
		else if (nowScene == "Grinding") {
			for (int i = 0; i < RecipeList.GetInstance ().coffeeBeanRecipe [MainUserInfo.GetInstance.menu].Length; i++) {
				recipes [i].text = RecipeList.GetInstance ().ingredient.GetIngredientByNum (RecipeList.GetInstance ().coffeeBeanRecipe [MainUserInfo.GetInstance.menu] [i])+" "+PlayerPrefs.GetFloat ("GOAL_POINT_F")+"g";;
			}
		}
		Sprite sour = Resources.Load<Sprite> (RecipeList.GetInstance().coffeeImgList[MainUserInfo.GetInstance.menu]);
		coffeeImg.sprite = sour;

	}
	void OnEnable(){
		StartCoroutine (enableCoru());
	
	}
	IEnumerator enableCoru(){
		yield return new WaitForSecondsRealtime (3.0f);
		UpDown.powerUp ();
	}
	public void RightChoice()
	{	
		checks [currentIngredient].enabled = true;
		int tempCreamNum = 0;
		if (MainUserInfo.GetInstance.cream == 0) {
			tempCreamNum = 1;
		} else {
			tempCreamNum = 0;
		}
		if (currentIngredient < RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length - 1 + tempCreamNum) {
			currentIngredient++;
			StopAllCoroutines ();
			StartCoroutine (FocusDown ());
		} else {
			imgTrans.gameObject.SetActive (false);
		}


	}
	public void IcingRightChoice(){
		checks [currentIngredient].enabled = true;

		if (currentIngredient < RecipeList.GetInstance ().iceMixerRecipe [MainUserInfo.GetInstance.menu].Length - 1 ) {
			currentIngredient++;
			StopAllCoroutines ();
			StartCoroutine (FocusDown ());
		} else {
			imgTrans.gameObject.SetActive (false);
		}
	}
	IEnumerator FocusDown(){
		while (true) {
			imgTrans.position = Vector3.Lerp(imgTrans.position, recipeTrans[currentIngredient].position, 5.0f*Time.deltaTime); 

			if (Vector3.Distance (imgTrans.position, recipeTrans[currentIngredient].position) < 0.1f) {
				break;
			}
			yield return null;
		}

	}

}
