﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;

using UnityEngine.UI;
public class ScoreManager_Variation : MonoBehaviour {

	int nowNum;
	string nowScene;
	float ticks = 0;
	public Text timmer;
	public ShowRecipePre recipePre;
	public GameObject ready;
	public GameObject buttonOk;

	StructforMinigame stuff;
	ConstforMinigame cuff;
	PopupManagement pop;
	PlayMakerFSM myFsm;
	int missCount=0;
	int currentIngredient = 0;
	Stopwatch sw;
	// Use this for initialization
	void Start () {
		
		nowNum = SceneManager.GetActiveScene ().buildIndex-1;
		nowScene = SceneManager.GetActiveScene ().name;
		pop = PopupManagement.p;
		stuff = StructforMinigame.getSingleton (nowNum);
		cuff = ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade - 1);
		sw = stuff.Sw;
		sw.Reset ();
		pop.makeIt (PP.popupIndex.READY);


	}
	IEnumerator CheckIngredient(){
		yield return new WaitForSecondsRealtime (5.0f);
		buttonOk.SetActive (true);
	

	}
	public void buttonOkClickSound()
	{
		SoundManage.instance.efxButtonOn ();
	}
	public void startVariation(){
		ready.SetActive (true);
	}
	public void RestartThisGame(){
		SceneManager.LoadScene (nowScene);
	}
	public void ButtonClickEvent(PlayMakerFSM _myFsm){
		myFsm = _myFsm;
		UnityEngine.Debug.Log ("ButtonClickEvent");
	}
	public void isRightSelect(int num)
	{
		//UnityEngine.Debug.Log ("isRight :" + num);
		//UnityEngine.Debug.Log ("isRight  Ingredient :" + RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [currentIngredient]);
		int tempCreamNum = 0;
		if (MainUserInfo.GetInstance.cream == 0) {
			tempCreamNum = 1;
		} else {
			tempCreamNum = 0;
		}
		if (currentIngredient < RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length+tempCreamNum) {
			if (currentIngredient == RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length && MainUserInfo.GetInstance.cream == 0 && num == 41) {
				recipePre.RightChoice ();
				currentIngredient++;
				myFsm.Fsm.Event ("OK");
				if (currentIngredient == RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length+tempCreamNum) {
					StartCoroutine (CheckIngredient ());
				}
			}
			else if (currentIngredient == RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length && MainUserInfo.GetInstance.cream == 0 && num != 41) {
				missCount++;
				SoundManage.instance.efxBadOn ();
				myFsm.Fsm.Event ("BAD");
			}
			else if (RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu] [currentIngredient]
			    == num) {
				//SoundManage.instance.efxOkOn ();
				recipePre.RightChoice ();
				currentIngredient++;
				myFsm.Fsm.Event ("OK");
				if (currentIngredient == RecipeList.GetInstance ().variationRecipe [MainUserInfo.GetInstance.menu].Length+tempCreamNum) {
					StartCoroutine (CheckIngredient ());
				}
			} 
			else {
				missCount++;
				SoundManage.instance.efxBadOn ();
				myFsm.Fsm.Event ("BAD");
			}
		} else {
			SoundManage.instance.efxBadOn ();
			missCount++;
			myFsm.Fsm.Event ("BAD");
		}

	}
	public void TimeCheckerStart(){
		StartCoroutine (TimerCheck());
	}
	IEnumerator TimerCheck(){
		while(true){
			ticks = (sw.ElapsedMilliseconds+Time.deltaTime)/1000;
			int it=(int)(cuff.Limittime-Math.Round(ticks,0));
			timmer.text = it.ToString();
			if (it <= 0) {
				StartCoroutine (TimeEnd ());
				yield break;
			}
			yield return null;
		}
	}
	IEnumerator TimeEnd(){
		sw.Stop ();
		yield return new WaitForSeconds(2);

		ticks = 0;
		stuff.Playtime = cuff.Limittime;
		stuff.ErrorRate = 100.0f;
		pop.makeIt(PP.popupIndex.JUDGE);

		while(pop.PopupObject != null)
			yield return new  WaitForSecondsRealtime(.1f);

	}
	public void conFirm(){
		SoundManage.instance.efxButtonOn ();
		float timmerMinus=0;
		float correctRate = 0;
		float errorMinus = 0;

		stuff.Level = MainUserInfo.GetInstance.grade;
		stuff.Misscount = missCount;
		stuff.RecordDate = DateTime.Now.ToString ("yyyyMMddHHmmss");
		sw.Stop ();
		stuff.Playtime = ticks;

		timmerMinus = Math.Max (0, Math.Min (1, (stuff.Playtime - cuff.Besttime) / (cuff.Limittime - cuff.Besttime)));
		stuff.ErrorRate = stuff.Misscount * cuff.Errorpenalty;
		errorMinus = Math.Max (0, Math.Min (1, (stuff.ErrorRate - cuff.BestErrorPercent) / (cuff.LimitErrorPercent - cuff.BestErrorPercent)));
		UnityEngine.Debug.Log ("tM: " + timmerMinus);
		UnityEngine.Debug.Log ("eM: " + errorMinus);
		stuff.Score = 100 - (timmerMinus + errorMinus) * 50;
		pop.makeIt (PP.popupIndex.JUDGE);

	}
}
