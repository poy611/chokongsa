﻿using UnityEngine;
using System.Collections;

public class PopupScoring : MonoBehaviour {
	PopupManagement popup;
	// Use this for initialization
	void OnEnable () {
		popup = PopupManagement.p;
		StartCoroutine (MakeItSocre ());
	}
	
	// Update is called once per frame
	IEnumerator MakeItSocre () {
		yield return new WaitForSecondsRealtime (1.0f);

		popup.makeIt (PP.popupIndex.SCORE);
	}
}
