﻿using UnityEngine;
using System.Collections;


public static class PP
{
	public enum popupIndex
	{
		APPLICATIONQUIT,CALCULATION, CONTINUE,ERROR, JUDGE, READY, RETRY, SCORE, TOTALRESULT,RESULTEA2,RESULTEA3,RESULTEA4,RESULTEA5
	};
}
public class PopupManagement : MonoBehaviour{
	public static PopupManagement p;
	public int reverse;
	//public int val;

	public Object[] ad;
    Object pm;

	void Start()
	{
		DontDestroyOnLoad (gameObject);
		ad = Resources.LoadAll ("pop");
		p = pointingMe ();
	}
	public void makeIt(PP.popupIndex state)
	{
		switch ((PP.popupIndex)state) {
		case PP.popupIndex.READY:

			pm = Instantiate (ad [(int)PP.popupIndex.READY]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.JUDGE:
			pm = Instantiate (ad [(int)PP.popupIndex.JUDGE]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.SCORE:
			pm = Instantiate (ad [(int)PP.popupIndex.SCORE]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.RETRY:
			pm = Instantiate (ad [(int)PP.popupIndex.RETRY]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.CONTINUE:
			pm = Instantiate (ad [(int)PP.popupIndex.CONTINUE]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.TOTALRESULT:
			pm = Instantiate (ad [(int)PP.popupIndex.TOTALRESULT]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.CALCULATION:
			pm = Instantiate (ad [(int)PP.popupIndex.CALCULATION]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.APPLICATIONQUIT:
			pm = Instantiate (ad [(int)PP.popupIndex.APPLICATIONQUIT]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		case PP.popupIndex.RESULTEA2:
			pm = Instantiate (ad [(int)PP.popupIndex.RESULTEA2]);
			break;
		case PP.popupIndex.RESULTEA3:
			pm = Instantiate (ad [(int)PP.popupIndex.RESULTEA3]);
			break;
		case PP.popupIndex.RESULTEA4:
			pm = Instantiate (ad [(int)PP.popupIndex.RESULTEA4]);
			break;
		case PP.popupIndex.RESULTEA5:
			pm = Instantiate (ad [(int)PP.popupIndex.RESULTEA5]);
			break;
		case PP.popupIndex.ERROR:
			pm = Instantiate (ad [(int)PP.popupIndex.ERROR]);
			((GameObject)pm).GetComponent<Popup> ().state = state;
			((GameObject)pm).GetComponent<Popup> ().reverse = reverse;
			break;
		
		}


		//((GameObject)pm).GetComponent<Popup> ().val = val;
	}
	public Object PopupObject {
		get {
			return pm;
		}
	}

	public PopupManagement pointingMe()
	{
		if (p == null) {
			p = this;
		}
		return p;
	}

}
