﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class ResultData{

	public List<ResultSaveData> resultData = new List<ResultSaveData>();
}
