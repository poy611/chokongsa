﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ResponseInfoType  {

	public UserInfoType resultData;
	public int resultCode;
	public string resultMsg;
	public ResponseInfoType(){}
}
