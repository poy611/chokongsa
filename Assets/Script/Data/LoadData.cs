﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class LoadData {
	public int sdGuestTh;
	public int sdTrialTh;
	public int sdGameId;
	public int sdGrade;
}
