﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class GradeData{
	public int grand_up;
	public string name;
	public string teacher_name;
	public int guest_cnt;
}
