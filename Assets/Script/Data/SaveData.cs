﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class SaveData{
	public string user_id;
	public int sd_trial_th;
	public int sd_grade;
	public int sd_game_id;
	public int sd_guest_th;
}
