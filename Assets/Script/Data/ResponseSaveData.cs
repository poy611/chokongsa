﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class ResponseSaveData {
	public LoadData resultData;
	public int resultCode;
	public string resultMsg;
}
