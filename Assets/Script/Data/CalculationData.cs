﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class CalculationData{
	public string user_id;
	public int numcclt_trial_th;
	public int numcclt_grade;
	public int numcclt_game_id;
	public int numcclt_operate_type;
	public int numcclt_quiz_value1;
	public int numcclt_quiz_value2;
	public int numcclt_correct_value;
	public float numcclt_play_time;
	public bool numcclt_is_correct;
}
