﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ResponseResultAllData{
	public ResultAllDataUpper resultData;
	public int resultCode;
	public string resultMsg;
}
