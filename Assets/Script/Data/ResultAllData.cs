﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class ResultAllData {
	public float prctcScore;
	public int prctcGameId;
	public float prctcPlayTime;
	public int prctcGrade;
	public float prctcErrorRange;
	public int prctcGuestTh;
	public string prctcRecordDate;
	public int prctcTrialTh;
	public string userId;
	public int prctcMissCnt;
}
