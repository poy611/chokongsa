﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class ResultSaveData {
	public string user_id;
	public int prctc_grade;
	public int prctc_game_id;
	public int prctc_guest_th;
	public float prctc_score;
	public float prctc_play_time;
	public int prctc_miss_cnt;
	public float prctc_error_range;
	public int prctc_trial_th;
}
