﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Serving : MonoBehaviour {
	string[,] rightText =new string[,]{
		{"아, 미안… 못먹겠어요.","읔~ 혀가 저려요!","평범한 커피네요~","맛있어요! 잘먹었습니다~","굉장해요! 다음에 또 올게요~"},
		{"흑흑, 충격적이야","제가 주문한 커피 맞나요?","제 스타일은 아니지만 봐줄만 하네요.","아~ 이 향긋함~ 행복해요!","이럴수가! 너무 멋져요! 반할거 같아~"},
		{"이거… 커피 맞나요?","다음엔 제대로 된 커피를 부탁드립니다.","특이하군요. 독특한 향입니다…","향이 참 좋은 커피입니다.","감동적입니다! 천상의 맛이로군요!"},
		{"어머! 날 놀리는거니?", "이런 맛이면 곤란하다고!", "그럭저럭 먹을만 한거 같아~","어머! 또 오고 싶어지는 맛이야~","어머어머~! 너무너무 맛있어~ 최고야!"},
		{"이걸 커피라고… 나를 시험에 들게 하는겐가?","흠… 아직 노력이 많이 필요하구만. 힘 내게.","약간 아쉽구만.  약간 아쉬워…","좋은 향이군. 잘먹었다네!","으흠~ 훌륭해!  자네 정말 훌륭하구만!"},
		{"에잉! 너무하는구만!","이게 뭔가? 좀 더 분발하게나.","나쁘지 않군. 다음에는 더 맛있는 커피를 부탁하네.","맛있군. 잘먹었네!","엄청나군! 이런 맛은 처음이야!"}
	};
	PopupManagement popup;
	public Text MayItakeYourOrder;
	public GameObject ShowmetheCoffee;
	public Image desk;
	public Image CoffeeImage1;
	public Image CoffeeImage2;
	public GameObject CreamImg1;
	public GameObject CreamImg2;
	float sum = 0;
	float cnt = 0;
	public Transform imgTrans;
	public Transform pos2;
	public GameObject popServing;
	public Image _teacher;
	public Image _scoreAlphabet;
	public Text _greatText;
	public Text coffeeText;
	int nowNum;
	int reverse=0;
	bool okClick=false;
	// Use this for initialization
	void Start () {
		SoundManage.instance.OffBgm ();
		nowNum = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex - 1;

		popup = PopupManagement.p;

		//NetworkMng.GetInstance.onAllResult += new NetworkMng.OnAllResult (OnAllResult);
		//NetworkMng.GetInstance.onSave += new NetworkMng.OnSave (OnSave);
		//NetworkMng.GetInstance.onAllResultFail += new NetworkMng.OnAllResultFail (OnAllResultFail);
		//NetworkMng.GetInstance.RequestResullAllData ();
		if (MainUserInfo.GetInstance.cream == 0) {
			CreamImg1.SetActive (true);
			CreamImg2.SetActive (true);
		}
		Sprite sour = Resources.Load<Sprite> (RecipeList.GetInstance().coffeeImgList[MainUserInfo.GetInstance.menu]);
		CoffeeImage1.sprite = sour;
		CoffeeImage2.sprite = sour;
		CoffeeImage1.enabled = true;
		OnAllResult (NetworkMng.GetInstance.ReadSaveData());

	}
	IEnumerator onStart()
	{
		Debug.Log ("Serving");
		yield return new WaitForSecondsRealtime (4.0f);
		popServing.SetActive (true);
		Sprite sour = Resources.Load<Sprite> (GradeDataInfo.GetInstance().grade_data[MainUserInfo.GetInstance.grade-1].teacher_name);
		_teacher.sprite = sour;
		Sprite[] source = Resources.LoadAll<Sprite> ("score");

		csSwitch (sum);
		_scoreAlphabet.sprite = source [(int)reverse];

		_greatText.text = stringSwitch(sum);

		coffeeText.text = RecipeList.GetInstance ().recipeList.GetMenuInPopup (MainUserInfo.GetInstance.menu);

	}
	int RightText(float cs)
	{	
		if (cs < 60)
			return 0;
		else if (cs < 70)
			return 1;
		else if (cs < 80)
			return 2;
		else if (cs < 90)
			return 3;
		else
			return 4;
	}
	string afterText(int _randomNum,int num)
	{
		return rightText [(_randomNum - 1), num];
	}
	void OnAllResult(ResultData info)
	{

		for (int j = 0; j < info.resultData.Count; j++) {
			if(info.resultData[j].prctc_grade == MainUserInfo.GetInstance.grade
				&&info.resultData[j].prctc_guest_th == MainUserInfo.GetInstance.guestTh
				&&info.resultData[j].prctc_game_id<=6)
			{
				sum += info.resultData [j].prctc_score;
				cnt++;
			}
		}
		sum /= cnt;

		StartCoroutine (ShowData ());

	}
	void OnAllResultFail(){
		//NetworkMng.GetInstance.RequestResullAllData ();
	}
	IEnumerator ShowData()
	{


		Sprite source = Resources.Load<Sprite> ("UselessImp/game_character_0"+MainUserInfo.GetInstance.guestId+ImageSwitch(sum));
		desk.sprite = source;
		desk.enabled = true;
		int tempNum = RightText (sum);
		Debug.Log ("Serving Sum : " + sum);
		MayItakeYourOrder.text = afterText (MainUserInfo.GetInstance.guestId, tempNum);
		while (true) {
			imgTrans.position = Vector3.Lerp(imgTrans.position, pos2.position, 5.0f*Time.deltaTime); 
			float temp =1.5f * Time.deltaTime;
			imgTrans.localScale -= new Vector3(temp, temp, 0);

			if (Vector3.Distance (imgTrans.position, pos2.position) < 0.1f) {
				break;
			}
			yield return null;
		}
		StartCoroutine (ShowmetheCoffeSizeUpCo());
		StartCoroutine (onStart());

	}
	IEnumerator ShowmetheCoffeSizeUpCo(){
		ShowmetheCoffee.gameObject.transform.localScale = new Vector3(0, 0, 0);
		ShowmetheCoffee.gameObject.SetActive(true);
		float limitSize = 0f;
		while (true) {

			float temp =5.0f * Time.deltaTime;
			limitSize += temp;
			ShowmetheCoffee.gameObject.transform.localScale += new Vector3(temp, temp, 0);

			if (limitSize > 1.5f) {
				Debug.Log ("BREAK");
				break;
			}
			yield return null;
		}


	}
	string stringSwitch(float cs)
	{	
		if (cs < 60)
			return "저런, 저런…";
		else if (cs < 67)
			return "공부가 필요해요!";
		else if (cs < 70)
			return "분발하세요!";
		else if (cs < 77)
			return "힘내세요!";
		else if (cs < 80)
			return "아쉬워요!";
		else if (cs < 87)
			return "나쁘지 않아요!";
		else if (cs < 90)
			return "잘했어요!";
		else if (cs < 97)
			return "대단해요!";
		else if (cs <= 100)
			return "완벽해요!";
		else
			return "";
	}
	string ImageSwitch(float cs)
	{
		if (cs < 60)
			return "2";
		else if (cs < 87)
			return "";
		else if (cs <= 100)
			return "3";
		else
			return "";
	}
	void csSwitch(float cs)
	{	
		if(cs<60)
			reverse = (int)CONSTforPOP.score.F;
		else if(cs<63)
			reverse = (int)CONSTforPOP.score.DM;
		else if(cs<67)
			reverse = (int)CONSTforPOP.score.D;
		else if(cs<70)
			reverse = (int)CONSTforPOP.score.DP;
		else if(cs<73)
			reverse = (int)CONSTforPOP.score.CM;
		else if(cs<77)
			reverse = (int)CONSTforPOP.score.C;
		else if(cs<80)
			reverse = (int)CONSTforPOP.score.CP;
		else if(cs<83)
			reverse = (int)CONSTforPOP.score.BM;
		else if(cs<87)
			reverse = (int)CONSTforPOP.score.B;
		else if(cs<90)
			reverse = (int)CONSTforPOP.score.BP;
		else if(cs<94)
			reverse = (int)CONSTforPOP.score.AM;
		else if(cs<97)
			reverse = (int)CONSTforPOP.score.A;
		else if(cs<=100)
			reverse = (int)CONSTforPOP.score.AP;
	}
	void juSwitch(float ju)
	{	
		if(ju<=5)
			reverse = (int)CONSTforPOP.judge.PERPECT;
		else if(ju<=12)
			reverse = (int)CONSTforPOP.judge.BRILLIENT;
		else if(ju<=25)
			reverse = (int)CONSTforPOP.judge.GOOD;
		else if(ju<=40)
			reverse = (int)CONSTforPOP.judge.OK;
		else if(ju<=60)
			reverse = (int)CONSTforPOP.judge.BAD;
	}
	void OnDestroy(){
		//NetworkMng.GetInstance.onSave -= new NetworkMng.OnSave (OnSave);
	//	NetworkMng.GetInstance.onAllResult -= new NetworkMng.OnAllResult (OnAllResult);
		//NetworkMng.GetInstance.onAllResultFail -= new NetworkMng.OnAllResultFail (OnAllResultFail);

	}
	public void OkClick(){
		SoundManage.instance.efxButtonOn ();
		//StartCoroutine (okNet());
		//NetworkMng.GetInstance.RequestSaveResultData (nowNum, MainUserInfo.GetInstance.gameId, MainUserInfo.GetInstance.grade , MainUserInfo.GetInstance.guestTh,MainUserInfo.GetInstance.trialTh);
		NetworkMng.GetInstance.RequestSaveGameDataInLocal ();
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));


	}
	IEnumerator okNet(){
		while (true) {
			if (!okClick) {
				okClick = true;
				NetworkMng.GetInstance.RequestSaveGameData (MainUserInfo.GetInstance.gameId,MainUserInfo.GetInstance.trialTh,MainUserInfo.GetInstance.grade,MainUserInfo.GetInstance.guestTh);
				UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));

			} else {
				yield break;
			}
			yield return null;
		}
	}
	public void OnSave()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
		okClick = false;
	}
}
