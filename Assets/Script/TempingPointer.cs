﻿using UnityEngine;
using System.Collections;
using System;
public class TempingPointer : MonoBehaviour {

	public Camera _camera;
	public PlayMakerFSM _myFsm;
	public Transform _cameraPos;
	public Transform _lookTarget;
	public Transform _filterTarget;
	public Transform _filterDown;
	public GameObject Temper;
	ScoreManager_Temping _tempingMng;

	Transform gaugePoint;
	Vector3 FirstGaugeTrans;
	float speed = 0.1f;
	float speedDown = 0.3f;
	void Start(){
		FirstGaugeTrans = new Vector3 (0,0,0);
		_tempingMng =  transform.GetComponent<ScoreManager_Temping> ();
		gaugePoint = GameObject.Find ("NEW UI Canvas").transform.FindChild ("WONDO GAUGE").transform.FindChild ("Gauge point").transform;
		FirstGaugeTrans = gaugePoint.position;

	}

	public void FirstSelectButton(){
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("TOUCH WHAT YOU WANT").gameObject.SetActive(false);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("Touch").transform.FindChild ("Image").gameObject.SetActive(false);
		StartCoroutine (MainCameraMove());

	}
	IEnumerator MainCameraMove(){

		//iTween.MoveTo (_camera.gameObject,_cameraPos.localPosition,1.0f);	
		iTween.MoveTo(_camera.gameObject, iTween.Hash("x",_cameraPos.localPosition.x,"y",_cameraPos.localPosition.y,"z",_cameraPos.localPosition.z,
			"lookTarget",_lookTarget.position, "easetype","linear","speed", 2.0f));
		iTween.MoveTo(Temper,_filterTarget.position,3.0f);

		yield return new WaitForSeconds(3.0f);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("Touch").transform.FindChild ("TempingTouch").gameObject.SetActive(true);
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("WONDO GAUGE").gameObject.SetActive(true);
	}
	public void Init(){
		_tempingMng.tempingAmount = 0;
		gaugePoint.position = FirstGaugeTrans;
		iTween.MoveTo (Temper,_filterTarget.position,3.0f);
	}
	public void TempingMouseDown(){
		StopAllCoroutines ();
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("KEEP TOUCH_1").gameObject.SetActive(true);
		StartCoroutine (TempingSlideUp());
	}
	IEnumerator TempingSlideUp(){
		iTween.MoveTo (Temper,_filterDown.position,3.0f);
		while (true) {
			if (gaugePoint.localPosition.y >= 0.5f) {
				if (_tempingMng.tempingAmount > 100) {
					_tempingMng.tempingAmount = 100;
				}
				yield break;
			} else {
				Debug.Log (gaugePoint.localPosition.y);
				gaugePoint.localPosition += new Vector3 (0, speed * Time.deltaTime, 0);
				_tempingMng.tempingAmount += speed * Time.deltaTime * 200f;
			}
			yield return null;
		}
	}
	IEnumerator TempingSlideDown(){
		iTween.MoveTo (Temper,_filterTarget.position,3.0f);
		while (true) {
			if (gaugePoint.localPosition.y <= 0.0f) {
				if (_tempingMng.tempingAmount < 0) {
					_tempingMng.tempingAmount = 0;
				}
				yield break;
			} else {
				Debug.Log (gaugePoint.localPosition.y);
				gaugePoint.localPosition -= new Vector3 (0, speedDown * Time.deltaTime, 0);
				_tempingMng.tempingAmount -= speedDown * Time.deltaTime * 200f;
			}
			yield return null;
		}
	}
	public void TempingMouseUp(){
		StopAllCoroutines ();
		GameObject.Find ("NEW UI Canvas").transform.FindChild ("TEXT Images").transform.FindChild ("KEEP TOUCH_1").gameObject.SetActive(false);
		Debug.Log ("오차율 "+Math.Abs ((_tempingMng.tempingAmount-_tempingMng.tempingLimitAmount)/_tempingMng.tempingLimitAmount*100));
		if (100.0f-Math.Abs ((_tempingMng.tempingAmount-_tempingMng.tempingLimitAmount)/_tempingMng.tempingLimitAmount*100)<60.0f) {
			StartCoroutine (TempingSlideDown());
		} else {
			GameObject.Find ("NEW UI Canvas").transform.FindChild ("WONDO GAUGE").gameObject.SetActive(false);
			GameObject.Find ("NEW UI Canvas").transform.FindChild ("RESULT WINDOW").gameObject.SetActive (false);
			GameObject.Find ("NEW UI Canvas").transform.FindChild ("Touch").transform.FindChild ("TempingTouch").gameObject.SetActive(false);
			_tempingMng.EndOfTemping ();

		}
	}
	public void Temping1Button(){
		_myFsm.Fsm.Event ("CLICK1");
	}
	public void Temping2Button(){
		Debug.Log ("CLICK2");
		_myFsm.Fsm.Event ("CLICK2");
	}
}
