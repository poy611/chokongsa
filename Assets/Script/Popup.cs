﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public static class CONSTforPOP{
	public enum judge{BAD,OK,GOOD,BRILLIENT,PERPECT
	};
	public enum score{A, AP, AM, B, BP, BM, C, CP, CM, D, DP, DM, F
	};
	//public static int reverse;
}
public class Popup : MonoBehaviour {
	public PP.popupIndex state;
	public int reverse;
	//public CONSTforPOP.judge ju;
	//public CONSTforPOP.score sc;

	int nowNum;
	string nowScene;
	StructforMinigame[] stuff =new StructforMinigame[20];
	ConstforMinigame cuff; 
	bool okClick = false;
	void Start()
	{
		NetworkMng.GetInstance.onSave += new NetworkMng.OnSave (OnSave);
		nowNum = SceneManager.GetActiveScene().buildIndex-1;
		nowScene = SceneManager.GetActiveScene ().name;
		stuff = StructforMinigame.getSingleton ();
		if (MainUserInfo.GetInstance.grade > 0) { 
			cuff = ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade - 1);
		}
		if(state == PP.popupIndex.READY){
			SoundManage.instance.OffBgm ();
			Debug.Log (state);
			Sprite source = Resources.Load<Sprite> (GradeDataInfo.GetInstance().grade_data[MainUserInfo.GetInstance.grade-1].teacher_name);
			transform.FindChild ("Teacher").GetComponent<Image> ().sprite = source;

			if (nowScene == "Lobby") {
				//transform.FindChild("BG").Find ("Cntt Label").GetComponent<Text> ().fontSize = 60;
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "손님으로부터 커피를 주문받는 과정을 학습합니다. 손님이 원하는 커피를 정확히 터치스크린을 눌러 주문을 받으세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "주문 받기";
			} else if (nowScene == "Grinding") {
				//transform.FindChild("BG").Find ("Cntt Label").GetComponent<Text> ().fontSize = 60;
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "원두를 분쇄하여 포터 필터에 담는 과정을 학습합니다.  포터 필터를 글라인더에 끼운 후 원두를 분쇄하세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "원두 분쇄";
			} 
		   else if (nowScene == "4_Variation_proto") {
				//transform.FindChild("BG").Find ("Cntt Label").GetComponent<Text> ().fontSize = 60;
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "재료들을 순서대로 담아서 주문한 커피를 완성 해 주세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "베리에이션";
			} else if (nowScene == "MiniGame") {
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "건강한 콩과 병든 콩을 감별하는 방법을 학습합니다.\n콩을 손가락으로 끌어 그릇과 쓰레기통에 각각 담아주세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "콩 고르기";
			} else if (nowScene == "Temping") {
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "포터필터에 담긴 원두가루를 눌러 다지는 과정을 학습합니다.\n템퍼를 이용해 포터필터를 적당한 힘으로 눌러 주세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "템핑";
			}
			else if (nowScene == "Icing") {
				transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ().text = "얼음과 믹서를 이용한 커피를 만드는 과정을 배웁니다.\n재료들을 순서대로 담아서 주문한 커피를 완성 해 주세요.\n제한시간 : " + cuff.Limittime;
				transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ().text = "얼음믹서";
			}


		}

		if (state == PP.popupIndex.SCORE) {
			
			Sprite sour = Resources.Load<Sprite> (GradeDataInfo.GetInstance().grade_data[MainUserInfo.GetInstance.grade-1].teacher_name);
			transform.FindChild ("Teacher").GetComponent<Image> ().sprite = sour;
			Sprite[] source = Resources.LoadAll<Sprite> ("score");
			Image desk = transform.FindChild("BG").Find ("Score Alphabet").GetComponent<Image> ();
			csSwitch (stuff [nowNum].Score);
			desk.sprite = source [(int)reverse];
			Text texts = transform.FindChild ("BG").Find ("GREAT!").GetComponent<Text> (); 
			texts.text = stringSwitch(stuff[nowNum].Score);
			Text desktx = transform.FindChild("BG").Find ("Input_Label1").GetComponent<Text> ();
			desktx.text = "" +((int)stuff [nowNum].Playtime).ToString ();
			UnityEngine.Debug.Log ("PlayTimeScore" + stuff [nowNum].Playtime);
			desktx = transform.FindChild("BG").Find ("Input_Label2").GetComponent<Text> ();
			desktx.text = ""+((int)Math.Max (0, 100-stuff [nowNum].ErrorRate)).ToString ();
			desktx = transform.FindChild("BG").Find ("Input_Label3").GetComponent<Text> ();
			desktx.text ="" + ((int)stuff [nowNum].Score).ToString ();
			//NetworkMng.GetInstance.SaveGameData ();


		}
		if (state == PP.popupIndex.JUDGE) {
			if (MainUserInfo.GetInstance.cream == 0) {
				transform.FindChild("BG").Find ("Cream Icon").gameObject.SetActive(true);
			}
			//
			if (nowScene == "MiniGame") {
				Sprite sour = Resources.Load<Sprite> ("CoffeeImgs/title_txt_03");
				transform.FindChild("BG").Find ("Left Icon").GetComponent<Image> ().sprite =sour;
			} else {
				Sprite sour = Resources.Load<Sprite> (RecipeList.GetInstance().coffeeImgList[MainUserInfo.GetInstance.menu]);
				transform.FindChild("BG").Find ("Left Icon").GetComponent<Image> ().sprite =sour;
			}
			Sprite[] source = Resources.LoadAll<Sprite> ("07_pop_result/judge");
			Image desk = transform.FindChild("BG").Find ("Right judgment").GetComponent<Image> ();
			/*
			for(int k=0;k<source.Length;k++)
				Debug.Log (source [k]);
				*/
			juSwitch (stuff [nowNum].ErrorRate);
			desk.sprite=source [(int)reverse];
			Text desktx = transform.FindChild("BG").Find ("Right input1").GetComponent<Text> ();
			desktx.text = ((int)stuff[nowNum].Playtime).ToString();
			desktx = transform.FindChild("BG").Find ("Right input2").GetComponent<Text> ();
			desktx.text = ((int)Math.Max (0, 100-stuff [nowNum].ErrorRate)).ToString();
			MenuList menuList = new MenuList ();
			desktx = transform.FindChild ("BG").Find ("Cntt Label").GetComponent<Text> ();
			//
			if (nowScene == "MiniGame") {
				desktx.text = "콩 고르기";
			} else {
				desktx.text = menuList.GetMenuInPopup (MainUserInfo.GetInstance.menu);
			}

			desktx = transform.FindChild ("BG").Find ("Title Label").GetComponent<Text> ();
			if (nowScene == "Lobby") {
				desktx.text = "주문받기";
			} else if (nowScene == "Grinding") {
				desktx.text = "원두분쇄";
			} else if (nowScene == "MiniGame") {
				desktx.text = "콩고르기";
			} else if (nowScene == "4_Variation_proto") {
				desktx.text = "베리에이션";
			}

		}
		if (state == PP.popupIndex.CALCULATION) {
			Text curText = transform.FindChild("BG").FindChild ("CurrentText").GetComponent<Text> ();
			Text ansText = transform.FindChild("BG").FindChild ("ANSWER").FindChild ("Text").GetComponent<Text> ();
			curText.text = GameObject.Find ("Backgrounds").transform.FindChild ("Question").GetComponent<Text> ().text;
			ansText.text =GameObject.Find ("Backgrounds").transform.FindChild ("Image").FindChild("Text").GetComponent<Text> ().text;
			if (GameObject.Find ("Backgrounds").GetComponent<LearnCalculation> ().isRight) {
				Sprite source = Resources.Load<Sprite> ("UselessImp/learning_04_calculate_O");
				transform.FindChild("BG").FindChild ("Image").GetComponent<Image> ().sprite = source;
				transform.FindChild("BG").FindChild ("RightText").GetComponent<Text> ().text = "맞았습니다";

			} else {
				Sprite source = Resources.Load<Sprite> ("UselessImp/learning_04_calculate_X");
				transform.FindChild("BG").FindChild ("Image").GetComponent<Image> ().sprite = source;
				transform.FindChild("BG").FindChild ("RightText").GetComponent<Text> ().text = "틀렸습니다";
			}

		}
	}
	// Use this for initialization
	public void judgeOk()
	{	
		SoundManage.instance.efxButtonOn ();
		PopupManagement.p.makeIt (PP.popupIndex.SCORE);
		Destroy (this.gameObject);
	}

	public void ok()
	{
		
		SoundManage.instance.efxButtonOn ();
		SoundManage.instance.OffBgm ();
		if (nowScene != "MiniGame") {
			//StartCoroutine (okNet());

			////////////////////
			/// Save Local
			////////////////////
			NetworkMng.GetInstance.RequestSaveResultData (nowNum, MainUserInfo.GetInstance.gameId, MainUserInfo.GetInstance.grade , MainUserInfo.GetInstance.guestTh,MainUserInfo.GetInstance.trialTh);
			NetworkMng.GetInstance.RequestSaveGameDataInLocal ();
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));

		} else {
			
			PlayerPrefs.SetInt ("BACKBUTTON", 1);
			SceneManager.LoadScene ("Title");
			Destroy (this.gameObject);
		}

	}
	/*IEnumerator okNet(){
		while (true) {
			if (!okClick) {
				okClick = true;
				Debug.Log ("okNet");
				NetworkMng.GetInstance.RequestSaveGameData (MainUserInfo.GetInstance.gameId , MainUserInfo.GetInstance.trialTh, MainUserInfo.GetInstance.grade , MainUserInfo.GetInstance.guestTh);
				NetworkMng.GetInstance.RequestSaveResultData (nowNum, MainUserInfo.GetInstance.gameId, MainUserInfo.GetInstance.grade , MainUserInfo.GetInstance.guestTh);
			//	UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
			


			} else {
				break;
			}
			yield return null;
		}
	}
	*/
	public void newGame(){

		//MainUserInfo.GetInstance.grade = 1;
		//MainUserInfo.GetInstance.gameId = 1;
		SoundManage.instance.efxButtonOn ();
		NetworkMng.GetInstance.RequestNewGame ();
		MainUserInfo.GetInstance.guestTh = 1;
		MainUserInfo.GetInstance.gameId = 0;
		NetworkMng.GetInstance.RequestSaveGameDataInLocal ();
		SceneManager.LoadScene (GameStepManager.GetInstance.FirstStart());

	}
	public void cancle()
	{	
		SoundManage.instance.efxButtonOn ();

		if (nowScene == "Lobby") {
			
			GameObject.Find ("Backgrounds").GetComponent<ScoreManager_Lobby> ().ReadyAndStart ();

			//stuff[nowNum].Sw.Start();
		} else if (nowScene == "Grinding") {
			PlayerPrefs.SetFloat ("LIMIT_TIME_F", ConstforMinigame.getSingleton (nowNum, MainUserInfo.GetInstance.grade - 1).Limittime);
			float temp1 = (float)RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().coffeeBeanRecipe [MainUserInfo.GetInstance.menu] [0] - 1] [0];
			float temp2 = (float)RecipeList.GetInstance ().randomValueByRecipe [RecipeList.GetInstance ().coffeeBeanRecipe [MainUserInfo.GetInstance.menu] [0] - 1] [1];
			PlayerPrefs.SetFloat ("GOAL_POINT_F", (float)Math.Floor (UnityEngine.Random.Range (temp1, temp2 + 1)));
			PlayMakerFSM myFSM;
			myFSM = GameObject.Find ("Start Button").GetComponent<PlayMakerFSM> ();
			myFSM.Fsm.Event ("CLICK");

		} else if (nowScene == "4_Variation_proto") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManager_Variation> ().startVariation ();
		} else if (nowScene == "MiniGame") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManagerMiniGame> ().StartMinigame ();
		} else if (nowScene == "Temping") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManager_Temping> ().ReadyAndStart ();
		}
		else if (nowScene == "Icing") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManager_Icing> ().startIcing();
		} 
		Destroy(this.gameObject); 

	}
	public void Retry()
	{
		SoundManage.instance.efxButtonOn ();
		NetworkMng.GetInstance.RequestNewGame ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.Retry());
		Destroy(this.gameObject); 
	}
	public void RetryLater()
	{
		
		SoundManage.instance.efxButtonOn ();
		NetworkMng.GetInstance.RequestSaveGameDataInLocal ();

		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.RetryLater());
		Destroy(this.gameObject); 
	}
	public void Continue(){
		SoundManage.instance.efxButtonOn ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));//MainUserInfo.GetInstance.gameId);
	
	}
	public void OnSave()
	{
		//if(nowScene == "4_Variation_proto" || nowScene == "Icing"){
			UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
		//}
		//Destroy(this.gameObject); 

	}
	void csSwitch(float cs)
	{	
		if(cs<60)
			reverse = (int)CONSTforPOP.score.F;
		else if(cs<63)
			reverse = (int)CONSTforPOP.score.DM;
		else if(cs<67)
			reverse = (int)CONSTforPOP.score.D;
		else if(cs<70)
			reverse = (int)CONSTforPOP.score.DP;
		else if(cs<73)
			reverse = (int)CONSTforPOP.score.CM;
		else if(cs<77)
			reverse = (int)CONSTforPOP.score.C;
		else if(cs<80)
			reverse = (int)CONSTforPOP.score.CP;
		else if(cs<83)
			reverse = (int)CONSTforPOP.score.BM;
		else if(cs<87)
			reverse = (int)CONSTforPOP.score.B;
		else if(cs<90)
			reverse = (int)CONSTforPOP.score.BP;
		else if(cs<94)
			reverse = (int)CONSTforPOP.score.AM;
		else if(cs<97)
			reverse = (int)CONSTforPOP.score.A;
		else if(cs<=100)
			reverse = (int)CONSTforPOP.score.AP;
	}
	void juSwitch(float ju)
	{	
		if(ju<=5)
			reverse = (int)CONSTforPOP.judge.PERPECT;
		else if(ju<=12)
			reverse = (int)CONSTforPOP.judge.BRILLIENT;
		else if(ju<=25)
			reverse = (int)CONSTforPOP.judge.GOOD;
		else if(ju<=40)
			reverse = (int)CONSTforPOP.judge.OK;
		else if(ju<=60)
			reverse = (int)CONSTforPOP.judge.BAD;
	}
	string stringSwitch(float cs)
	{	
		if (cs < 60)
			return "저런, 저런…";
		else if (cs < 67)
			return "공부가 필요해요!";
		else if (cs < 70)
			return "분발하세요!";
		else if (cs < 77)
			return "힘내세요!";
		else if (cs < 80)
			return "아쉬워요!";
		else if (cs < 87)
			return "나쁘지 않아요!";
		else if (cs < 90)
			return "잘했어요!";
		else if (cs < 97)
			return "대단해요!";
		else if (cs <= 100)
			return "완벽해요!";
		else
			return "";
	}
	public void RestartOnThisGame()
	{
		SoundManage.instance.efxButtonOn ();
		SceneManager.LoadScene (nowScene);
		
		Destroy(this.gameObject); 
	}
	public void CalNextQuestion()
	{
		SoundManage.instance.efxButtonOn ();
		//서버에 저장
		//다시시작

		UnityEngine.SceneManagement.SceneManager.LoadScene ("LearnCalculation");
		Destroy(this.gameObject); 
	}
	public void BackLearnPage()
	{
		SoundManage.instance.efxButtonOn ();
		//서버에 저장
		//LearnPage
		NetworkMng.GetInstance.SendAllCalculationData ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("LEARNPAGE");
		Destroy(this.gameObject); 
	}
	public void PopCancle(){
		SoundManage.instance.efxButtonOn ();
		Destroy (gameObject);
	}
	public void Quit(){
		SoundManage.instance.efxButtonOn ();
		Application.Quit ();
	}
	void OnDestroy(){
		
		NetworkMng.GetInstance.onSave -= new NetworkMng.OnSave (OnSave);
	}

}
