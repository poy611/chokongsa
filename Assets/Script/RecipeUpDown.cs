﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RecipeUpDown : MonoBehaviour {

	public GameObject obj;
	public Transform pos1;
	public Transform pos2;
	public Image buttonImage;
	public Sprite[] _btImg;
	public bool isUp=false;

	public void Up(){
		SoundManage.instance.efxButtonOn ();
		StopAllCoroutines ();
		if (isUp == false) {
			isUp = true;
			buttonImage.sprite = _btImg [1];

			StartCoroutine (GoUP ());
		} else {
			isUp = false;
			buttonImage.sprite = _btImg [0];

			StartCoroutine (GoDown ());
		}
	}
	public void powerUp(){
		StopAllCoroutines ();
		isUp = true;
		buttonImage.sprite = _btImg [1];

		StartCoroutine (GoUP ());
	}
	IEnumerator GoUP(){
		while (true) {
			obj.transform.position = Vector3.Lerp (obj.transform.position, pos1.position, 2.0f*Time.deltaTime); 

			if (Vector3.Distance (obj.transform.position, pos1.position) < 0.01f) {
				break;
			}
			yield return null;
		}

	

	}
	IEnumerator GoDown()
	{
		while (true) {
			obj.transform.position = Vector3.Lerp(obj.transform.position, pos2.position, 2.0f*Time.deltaTime); 
		
			if (Vector3.Distance (obj.transform.position, pos2.position) < 0.01f) {
				break;
			}
			yield return null;
		}

	
	}
}
