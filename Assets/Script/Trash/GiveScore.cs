﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using debug = UnityEngine.Debug;


public class GiveScore : MonoBehaviour {
	Stopwatch sw = new Stopwatch();
	//SceneindexInc n =SceneindexInc.getSingleton();
	int nowNum;//SceneManager.GetActiveScene().buildIndex;
	float ticks;
	StructforMinigame[] stuff = StructforMinigame.getSingleton();
	public PopupManagement popup;
	void Start()
	{
		popup = PopupManagement.p;
		sw.Start ();
		nowNum = SceneManager.GetActiveScene().buildIndex-1;
		popup.makeIt (PP.popupIndex.READY);

	}
	public void giveScore(int sc)
	{
		//debug.Log (nowNum);
		//stuff[n.getIndex()] = GameObject.Find("MasterManager").GetComponent<MasterIO>().getStruct (n.getIndex());//얕은 복사가 이루어짐
		//if (stuff == null)
			//debug.Log ("nulls");
		stuff[nowNum].Gameid = MainUserInfo.GetInstance.gameId;
		stuff[nowNum].Level = MainUserInfo.GetInstance.grade;
		stuff[nowNum].RecordDate = DateTime.Now.ToString("yyyyMMddHHmmss");
		stuff[nowNum].Score = sc;
		sw.Stop ();
		ticks = (sw.ElapsedMilliseconds+ Time.deltaTime)/1000;
		stuff [nowNum].Playtime = ticks;
		stuff[nowNum].Misscount = 0;
		stuff[nowNum].ErrorRate = 0;
		//move ();
		popupActive();

	}
	void popupActive()
	{
		popup.makeIt (PP.popupIndex.SCORE);
	}
	public void backButton()
	{
		if(nowNum ==0)
		 	SceneManager.LoadScene (6);
		else
			SceneManager.LoadScene (nowNum-1);
	}

}
