﻿using UnityEngine;
using System.Collections;

public class StartImageBGMS : MonoBehaviour {
	string nowScene;
	// Use this for initialization
	void Start () {
		nowScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
	}
	
	// Update is called once per frame
	void OnDisable () {
		SoundManage.instance.PlayBgm (nowScene);
	}
}
