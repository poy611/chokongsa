﻿using UnityEngine;
using System.Collections;

public class SceneindexInc {
	private static SceneindexInc n = new SceneindexInc();
	int index;
	private SceneindexInc(){
		index = -1;
	}
	public static SceneindexInc getSingleton(){
		if (n == null)
			n = new SceneindexInc ();
		return n;
	}
	public int getIndexInc()
	{
		if (index > 5)
			index = -1;
		return ++index;
	}
	public int getIndex()
	{
		return index;
	}
}
