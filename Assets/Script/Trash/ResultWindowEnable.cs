﻿using UnityEngine;
using System.Collections;

public class ResultWindowEnable : MonoBehaviour {

	public ScoreManagerMiniGame manager;

	void OnEnable(){

		StartCoroutine (WaitTemp());

	}
	IEnumerator WaitTemp(){
		yield return new WaitForSecondsRealtime (1.0f);
		manager.conFirm ();
	}
}
