﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using UnityEngine.UI;
using System.Collections.Generic;

public class StartImageDisapear : MonoBehaviour {

	int nowNum;
	string nowScene;
	StructforMinigame stuff;
	Stopwatch sw;
	void Awake(){
		nowNum = SceneManager.GetActiveScene ().buildIndex - 1;
		nowScene = SceneManager.GetActiveScene ().name;
		stuff = StructforMinigame.getSingleton (nowNum);
		sw = stuff.Sw;
	}
	void OnDisable(){
		sw.Start ();
		if (nowScene == "Icing") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManager_Icing> ().TimeCheckerStart ();
		} else if (nowScene == "4_Variation_proto") {
			GameObject.Find ("MAIN OBJ").GetComponent<ScoreManager_Variation> ().TimeCheckerStart ();
		}
	}
}
