﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Diagnostics;

static class StructforPopup{
	/*
	public static Image[] img = new Image[10];
	public enum imageIndex
	{
		
	};
	public StructforPopup()
	{
	}*/
}
public class PopupManager : MonoBehaviour {
	public static Object pm;
	//Public Popup pm;
	Object[] ad;
	public enum popupIndex
	{
		CONTINUE, JUDGE, READY, RETRY, SCORE, TOTALRESULT
	};
	popupIndex state;
	int nowNum;
	StructforMinigame[] stuff =new StructforMinigame[5];

	Text tx;
	//delegate void ppit();
	void Awake () {
		ad = Resources.LoadAll ("pop");

		MakeThisTheOnlyGameManager ();
		gameObject.SetActive (false);
		tx = GetComponentInChildren<Text> ();
		for(int i=0;i<ad.Length;i++)
			UnityEngine.Debug.Log(ad[i].ToString());
	}
	void MakeThisTheOnlyGameManager(){
		if(pm == null){
			//this가 있기에 instantiate가 사라진다
			//state = popupIndex.CONTINUE;
			switch (state) {
			case popupIndex.READY:
				pm=Instantiate (ad[(int)popupIndex.READY]);
				break;
			case popupIndex.JUDGE:
				pm=Instantiate (ad[(int)popupIndex.JUDGE]);
				break;
			case popupIndex.SCORE:
				pm=Instantiate (ad[(int)popupIndex.SCORE]);
				break;
			case popupIndex.RETRY:
				pm=Instantiate (ad[(int)popupIndex.RETRY]);
				break;
			case popupIndex.CONTINUE:
				pm=Instantiate (ad[(int)popupIndex.CONTINUE]);
				break;
			case popupIndex.TOTALRESULT:
				pm=Instantiate (ad[(int)popupIndex.TOTALRESULT]);
				break;
			}
			pm = this;
			DontDestroyOnLoad(pm);
		}
		else if(pm!=this)
		{
			Destroy(pm);
		}
	}
	public void ok()
	{
		nowNum = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene (nowNum+1);
		gameObject.SetActive (false);

	}
	public void cancle()
	{
		stuff [nowNum].Sw.Start ();
		gameObject.SetActive (false);
	}

	public void setText()
	{
		nowNum = SceneManager.GetActiveScene().buildIndex;
		int i = nowNum;
		stuff = StructforMinigame.getSingleton ();
		tx.text +=" "+i+"'s Score : ";
		tx.text += stuff[i].Score;
		tx.text +="\t";
		tx.text +=" "+i+"'s UserId : ";
		tx.text += stuff[i].Userid;
		tx.text +="\t";
		tx.text +=" "+i+"'s GameId : ";
		tx.text += stuff[i].Gameid;
		tx.text +="\t";
		tx.text +=" "+i+"'s Level : ";
		tx.text += stuff[i].Level;
		tx.text +="\t";
		tx.text +=" "+i+"'s RecordDate : ";
		tx.text += stuff[i].RecordDate;
		tx.text +="\t";
		tx.text +=" "+i+"'s Playtime : ";
		tx.text += stuff[i].Playtime;
		tx.text +="\t";
		tx.text +=" "+i+"'s misscount : ";
		tx.text += stuff[i].Misscount;
		tx.text +="\t";
		tx.text +=" "+i+"'s errorRange : ";
		tx.text += stuff[i].ErrorRate;
		tx.text +="\t";
		tx.text +="\n";
	}

}
