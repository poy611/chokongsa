﻿using UnityEngine;
using System.Collections;

public class LoadGameValue : MonoBehaviour {
	public PlayMakerFSM _pmTrriger2;
	public PlayMakerFSM _pmTrriger3;
	public PlayMakerFSM _dataInfo;
	//public PlayMakerFSM _timer;
	public GameObject reticle_03;

	HutongGames.PlayMaker.FsmFloat _value2;
	HutongGames.PlayMaker.FsmFloat _value3;
	HutongGames.PlayMaker.FsmFloat _need;

//	HutongGames.PlayMaker.FsmFloat _timeValue;

	public float totalValue;
	public float totalNeed;
	void Start(){
		_value2 = _pmTrriger2.FsmVariables.FindFsmFloat ("gauge_point");
		_value3 = _pmTrriger3.FsmVariables.FindFsmFloat ("gauge_point");
		_need = _dataInfo.FsmVariables.FindFsmFloat ("need_powder_f");
//		_timeValue = _timer.FsmVariables.FindFsmFloat ("limit_time_f");
	
	}

	// Update is called once per frame
	void Update(){
		if (_pmTrriger2.Active) {
			PlusCoffee ();
		}
		if (_pmTrriger3.Active) {
			MinusCoffee ();
		}
		if (_dataInfo.Active) {
			SetNeed ();
		}
	}
	public void PlusCoffee () {
		totalValue = _value2.Value;

	}
	public void MinusCoffee(){
		if (!reticle_03.active) {
			totalValue = _value3.Value;
		}
	}
	public void SetNeed(){
		totalNeed = _need.Value;
	}
	public void SetValue(){
		_value2.Value = totalValue;
		_value3.Value = totalValue;
	}
}
