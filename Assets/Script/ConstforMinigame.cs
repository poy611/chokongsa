﻿using UnityEngine;
using System.Collections;

static class SCENE
{
	public const int INDEX = 20;
	/*	0==LOBBY
	 * 	1==GRINDING
	 * 	2==TEMPING
	 * 	3==MILKING
	 * 	4==VARIATION
	 * 	5==NOTSERVED
	 * 	6==CHOOSINGBEANS_MINIGAME
	 */
}
static class CFM
{
	public const int LEVEL = 4;
	/*	0== level 1
	 * 	1== level 2
	 * 	2== level 3
	 * 	3== level 4
	 */
}
public class ConstforMinigame{
	private static ConstforMinigame[,] cuff = new ConstforMinigame[SCENE.INDEX,CFM.LEVEL];
	float bestTime;
	float limitTime;
	float bestErrorPercent;
	float limitErrorPercent;
	int errorPenalty;



	private ConstforMinigame()
	{
		for (int index = 0; index < SCENE.INDEX; index++) {
			for (int level = 0; level < CFM.LEVEL; level++) {
				{
					cuff [index, level] = new ConstforMinigame (index, level);
				}
			}
		}
	}
	private ConstforMinigame(int i,int j){
		switch (i) {
		case 0:
			locateLobby (j);
				break;
		case 1:
			locateGrinding (j);
				break;
		case 2:
			locateTemping (j);
				break;
		case 3:
				break;
		case 4:
			locateIcing (j);
			break;
		case 5:
			locateVariation (j);
				break;
		
		case 7:
			locateMiniGame (j);
			break;
			}
	}
	void locateTemping(int j){
		switch (j) {
		case 0:
			bestTime = 24;
			limitTime = 120;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 10;
			break;
		case 1:
			bestTime = 22;
			limitTime = 110;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 10;
			break;
		case 2:
			bestTime = 20;
			limitTime = 100;
			bestErrorPercent = 8;
			limitErrorPercent = 40;
			errorPenalty = 10;
			break;
		case 3:
			bestTime = 18;
			limitTime = 90;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 10;
			break;
		}
	}
	void locateGrinding(int j)
	{
		switch (j) {
		case 0:
			bestTime = 60;
			limitTime = 120;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 0;
			break;
		case 1:
			bestTime = 50;
			limitTime = 90;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 0;
			break;
		case 2:
			bestTime = 40;
			limitTime = 90;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 0;
			break;
		case 3:
			bestTime = 30;
			limitTime = 60;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 0;
			break;
		}
	}
	void locateLobby(int j)
	{
		switch (j) {
		case 0:
			bestTime = 30;
			limitTime = 120;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 15;
			break;
		case 1:
			bestTime = 20;
			limitTime = 90;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 15;
			break;
		case 2:
			bestTime = 15;
			limitTime = 60;
			bestErrorPercent = 8;
			limitErrorPercent = 40;
			errorPenalty = 15;
			break;
		case 3:
			bestTime = 8;
			limitTime = 30;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 15;
			break;
		}
	}
	void locateIcing(int j){
		switch (j) {
		case 0:
			bestTime = 50;
			limitTime = 180;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 10;
			break;
		case 1:
			bestTime = 40;
			limitTime = 120;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 10;
			break;
		case 2:
			bestTime = 30;
			limitTime = 90;
			bestErrorPercent = 8;
			limitErrorPercent = 40;
			errorPenalty = 10;
			break;
		case 3:
			bestTime = 20;
			limitTime = 60;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 10;
			break;
		}
	}
	void locateVariation(int j)
	{
		switch (j) {
		case 0:
			bestTime = 50;
			limitTime = 180;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 10;
			break;
		case 1:
			bestTime = 40;
			limitTime = 120;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 10;
			break;
		case 2:
			bestTime = 30;
			limitTime = 90;
			bestErrorPercent = 8;
			limitErrorPercent = 40;
			errorPenalty = 10;
			break;
		case 3:
			bestTime = 20;
			limitTime = 60;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 10;
			break;
		}
	}
	void locateMiniGame(int j)
	{
		switch (j) {
		case 0:
			bestTime = 100;
			limitTime = 180;
			bestErrorPercent = 10;
			limitErrorPercent = 50;
			errorPenalty = 5;
			break;
		case 1:
			bestTime = 80;
			limitTime = 180;
			bestErrorPercent = 9;
			limitErrorPercent = 45;
			errorPenalty = 5;
			break;
		case 2:
			bestTime = 70;
			limitTime = 120;
			bestErrorPercent = 8;
			limitErrorPercent = 40;
			errorPenalty = 5;
			break;
		case 3:
			bestTime = 60;
			limitTime = 120;
			bestErrorPercent = 7;
			limitErrorPercent = 35;
			errorPenalty = 5;
			break;
		}
	}
	public float Besttime {
		get {
			return bestTime;
		}
	}
	public float Limittime {
		get {
			return limitTime;
		}
	}
	public float BestErrorPercent {
		get {
			return bestErrorPercent;
		}
	}
	public float LimitErrorPercent {
		get {
			return limitErrorPercent;
		}
	}
	public float Errorpenalty {
		get {
			return errorPenalty;
		}
	}
	public static ConstforMinigame getSingleton(int index,int level)
	{
		
		if (cuff [index, level] == null)
			cuff [index, level] = new ConstforMinigame (index, level);

		return cuff[index,level];
	}

}
