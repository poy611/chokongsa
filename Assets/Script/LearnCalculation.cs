﻿using UnityEngine;
using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class LearnCalculation : MonoBehaviour {
	enum CAL_STATE{
		ADD,
		SUB,
		MUL,
		DIV
	};
	int random_op;
	public Text question;
	public Text ans;
	public bool isRight = false;
	PopupManagement popup;
	int randomNum1;
	int randomNum2;
	int rightNum;
	CAL_STATE state;
	StructforMinigame stuff;
	Stopwatch sw;
	// Use this for initialization
	void Start () {
		int nowNum = SceneManager.GetActiveScene ().buildIndex - 1;
		random_op = (int)Math.Floor (UnityEngine.Random.Range (0.0f, 4.0f));
		state = (CAL_STATE)random_op;
		question = transform.FindChild ("Question").GetComponent<Text> ();
		ans = transform.Find ("Image").FindChild ("Text").GetComponent<Text> ();
		ans.text = "";
		stuff = StructforMinigame.getSingleton (nowNum);
		sw = stuff.Sw;
		popup = PopupManagement.p;
		sw.Reset ();
		SettingRandomNum ();
	}
	void SettingRandomNum()
	{
		switch (state) {
		case CAL_STATE.ADD:
			randomNum1 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f));
			randomNum2 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f - randomNum1));
			rightNum = randomNum1 + randomNum2;
			question.text = randomNum1 + " + " + randomNum2; 
			break;
		case CAL_STATE.DIV:
			randomNum1 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f));
			if (randomNum1 / 10 > 0) {
				randomNum1 = (randomNum1 / 10) * 10;
			}
			randomNum2 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, randomNum1 + 1));
			if (randomNum2 / 10 > 0) {
				randomNum2 %= 10;
			}
			randomNum2 = SetRandNum2DIV (randomNum1);
			rightNum = randomNum1 / randomNum2;
			question.text = randomNum1 + " % " + randomNum2;
			break;
		case CAL_STATE.MUL:
			randomNum1 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f));
			if (randomNum1 / 10 > 0) {
				randomNum1 = (randomNum1 / 10) * 10;
				randomNum2 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 10.0f));
			} else {
				randomNum2 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f));
				if (randomNum2 / 10 > 0) {
					randomNum2 = (randomNum2 / 10) * 10;
				}
			}
			rightNum = randomNum1 * randomNum2;
			question.text = randomNum1 + " x " + randomNum2; 
			break;
		case CAL_STATE.SUB:
			randomNum1 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, 100.0f));
			randomNum2 = (int)Math.Floor (UnityEngine.Random.Range (1.0f, randomNum1+1));
			rightNum = randomNum1 - randomNum2;
			question.text = randomNum1 + " - " + randomNum2; 
			break;
		}
		sw.Start ();

	}
	public void PadClick(string str)
	{
		SoundManage.instance.efxButtonOn ();
		ans.text += str;
	}
	public void deleteClick()
	{
		SoundManage.instance.efxButtonOn ();
		if(ans.text.Length>0)
			ans.text = ans.text.Remove(ans.text.Length-1);
	}
	public void cancelClick()
	{
		SoundManage.instance.efxButtonOn ();
		ans.text = "";
	}
	public void selectClick()
	{
		SoundManage.instance.efxButtonOn ();
		sw.Stop ();

		float tick=(sw.ElapsedMilliseconds+Time.deltaTime)/1000;

		if (rightNum == Convert.ToInt32 (ans.text)) {
			isRight = true;
		} else {
			isRight = false;
		}
		CalculationData resultData = new CalculationData ();
		resultData.user_id = MainUserInfo.GetInstance.userId;
		resultData.numcclt_trial_th = MainUserInfo.GetInstance.trialTh;
		resultData.numcclt_grade = MainUserInfo.GetInstance.grade;
		resultData.numcclt_game_id = 92;
		resultData.numcclt_operate_type = (int)state + 1;
		resultData.numcclt_quiz_value1 = randomNum1;
		resultData.numcclt_quiz_value2 = randomNum2;
		resultData.numcclt_correct_value = rightNum;
		resultData.numcclt_play_time = tick;
		resultData.numcclt_is_correct = isRight;
		NetworkMng.GetInstance.ReadAndWriteCalculationLog (resultData);
		popup.makeIt (PP.popupIndex.CALCULATION);


	}
	public void backButtonClick()
	{
		SoundManage.instance.efxButtonOn ();
		NetworkMng.GetInstance.SendAllCalculationData ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("LEARNPAGE");
	}
	int SetRandNum2DIV(int a)
	{
		ArrayList temp = new ArrayList ();
		int b = 1;
		int returnValue=1;
		while (true) {
			if (a == b || b>10) {
				break;
			}

			if (a % b == 0) {
				temp.Add (b);
			}
			b++;
		}

		return returnValue = (int)temp [(int)Math.Floor (UnityEngine.Random.Range (0.0f, (float)temp.Count))];
	}
}
