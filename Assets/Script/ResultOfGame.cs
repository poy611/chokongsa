﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
public class ResultOfGame : MonoBehaviour {
	int reverse=0;
	float totalScore=0;
	public float[] tempArray = new float[6];
	public float[] countArray = new float[6];
	public Sprite[] _gameImg;
	bool okClick = false;
	PopupManagement pop;
	GameObject resultPref;
	enum ResultState{
		LOADIONG,
		NONE
	};
	ResultState state;
	void Start () {
		SoundManage.instance.OffBgm ();
		state = ResultState.LOADIONG;
		for (int i = 0; i < tempArray.Length; i++) {
			tempArray [i] = 0.0f;
			countArray [i] = 0.0f;
		}
		pop = PopupManagement.p;
		NetworkMng.GetInstance.onSave += new NetworkMng.OnSave (OnSave);
		NetworkMng.GetInstance.onAllResultFail += new NetworkMng.OnAllResultFail (OnAllResultFail);
		//NetworkMng.GetInstance.RequestResullAllData ();
		OnAllResult (NetworkMng.GetInstance.ReadSaveData());
	}
	void OnSave(){
		
		NetworkMng.GetInstance.DeleteLogFile ();
		PlayerPrefs.SetInt ("BACKBUTTON", 1);
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
	}
	public void toMainScene()
	{
		SoundManage.instance.efxButtonOn ();

		Debug.Log ("TotalScore : " + totalScore);
		if (state == ResultState.NONE) {
			if (totalScore >= GradeDataInfo.GetInstance ().grade_data [MainUserInfo.GetInstance.grade-1].grand_up) {
				//StartCoroutine (okNet());
				MainUserInfo.GetInstance.grade++;
				if (MainUserInfo.GetInstance.grade >= 5) {
					MainUserInfo.GetInstance.grade = 0;
					MainUserInfo.GetInstance.gameId = 0;
					MainUserInfo.GetInstance.trialTh++;
					MainUserInfo.GetInstance.guestTh = 1;
					NetworkMng.GetInstance.RequestSaveGameDataInLocal ();
					NetworkMng.GetInstance.SendAllData ();

				} else {
					MainUserInfo.GetInstance.guestTh = 1;
					NetworkMng.GetInstance.RequestSaveGameDataInLocal ();
						UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
				}

			} else {
				PopupManagement.p.makeIt (PP.popupIndex.RETRY);
			}
		}
	}
	IEnumerator okNet(){
		while (true) {
			if (!okClick) {
				okClick = true;
				MainUserInfo.GetInstance.grade++;
				if (MainUserInfo.GetInstance.grade >= 5) {
					MainUserInfo.GetInstance.grade = 1;
					MainUserInfo.GetInstance.trialTh++;
				}
				MainUserInfo.GetInstance.guestTh = 1;
				NetworkMng.GetInstance.RequestSaveGameData (MainUserInfo.GetInstance.gameId,MainUserInfo.GetInstance.trialTh,MainUserInfo.GetInstance.grade,MainUserInfo.GetInstance.guestTh);
				UnityEngine.SceneManagement.SceneManager.LoadScene (GameStepManager.GetInstance.GetNextScene (MainUserInfo.GetInstance.gameId));
			} else {
				yield break;
			}
			yield return null;
		}
	}
	void csSwitch(float cs)
	{	
		if(cs<60)
			reverse = (int)CONSTforPOP.score.F;
		else if(cs<63)
			reverse = (int)CONSTforPOP.score.DM;
		else if(cs<67)
			reverse = (int)CONSTforPOP.score.D;
		else if(cs<70)
			reverse = (int)CONSTforPOP.score.DP;
		else if(cs<73)
			reverse = (int)CONSTforPOP.score.CM;
		else if(cs<77)
			reverse = (int)CONSTforPOP.score.C;
		else if(cs<80)
			reverse = (int)CONSTforPOP.score.CP;
		else if(cs<83)
			reverse = (int)CONSTforPOP.score.BM;
		else if(cs<87)
			reverse = (int)CONSTforPOP.score.B;
		else if(cs<90)
			reverse = (int)CONSTforPOP.score.BP;
		else if(cs<94)
			reverse = (int)CONSTforPOP.score.AM;
		else if(cs<97)
			reverse = (int)CONSTforPOP.score.A;
		else if(cs<=100)
			reverse = (int)CONSTforPOP.score.AP;
	}
	void OnAllResult(ResultData info)
	{
		state = ResultState.LOADIONG;
		for (int j = 0; j < info.resultData.Count; j++) {
			if(info.resultData[j].prctc_grade == MainUserInfo.GetInstance.grade
				&& info.resultData[j].prctc_trial_th == MainUserInfo.GetInstance.trialTh
				&& info.resultData[j].prctc_game_id<=6){
				tempArray [info.resultData [j].prctc_game_id - 1] += info.resultData [j].prctc_score;
				countArray [info.resultData [j].prctc_game_id - 1] += 1.0f;
			}

		}
		int countOfGame = 0;
		for (int i = 0; i < countArray.Length; i++) {
			if (countArray [i] > 0) {
				countOfGame++;
				tempArray [i] /= countArray [i];
			} 
		}
		ShowData (countOfGame);

	}
	void OnAllResultFail(){
		PlayerPrefs.SetInt ("BACKBUTTON", 1);
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
	}
	void ShowData(int _countOfGame)
	{
		Debug.Log ("CountOfGAme: " + _countOfGame);
		Sprite[] source = Resources.LoadAll<Sprite> ("score");
		if (_countOfGame == 2) {
			pop.makeIt (PP.popupIndex.RESULTEA2);
			resultPref = (GameObject)pop.PopupObject;
			GameObject temp;
			Text gameText;
			Image scoreImg;
			Image gameImg;
			int j = 1;
			for (int i = 0; i < tempArray.Length; i++) {
				if (tempArray [i] != 0) {
					csSwitch (tempArray [i]); 
					temp = resultPref.transform.FindChild ("ResultEa2").FindChild ("score" + j).gameObject;
					gameText = temp.transform.FindChild ("gameText").GetComponent<Text> ();
					scoreImg = temp.transform.FindChild ("ScoreImage").GetComponent<Image> ();
					gameImg = temp.transform.FindChild ("gameImage").GetComponent<Image> ();
					gameText.text = GetGameName (i);
					scoreImg.sprite = source [(int)reverse];
					gameImg.sprite = _gameImg [i];
					j++;
				}
			}
		} else if (_countOfGame == 3) {
			pop.makeIt (PP.popupIndex.RESULTEA3);
			resultPref = (GameObject)pop.PopupObject;
			GameObject temp;
			Text gameText;
			Image scoreImg;
			Image gameImg;
			int j = 1;
			for (int i = 0; i < tempArray.Length; i++) {
				if (tempArray [i] != 0) {
					csSwitch (tempArray [i]); 
					temp = resultPref.transform.FindChild ("ResultEa3").FindChild ("score" + j).gameObject;
					gameText = temp.transform.FindChild ("gameText").GetComponent<Text> ();
					scoreImg = temp.transform.FindChild ("ScoreImage").GetComponent<Image> ();
					gameImg = temp.transform.FindChild ("gameImage").GetComponent<Image> ();
					gameText.text = GetGameName (i);
					scoreImg.sprite = source [(int)reverse];
					gameImg.sprite = _gameImg [i];
					j++;
				}
			}
		} else if (_countOfGame == 4) {
			pop.makeIt (PP.popupIndex.RESULTEA4);
			resultPref = (GameObject)pop.PopupObject;
			GameObject temp;
			Text gameText;
			Image scoreImg;
			Image gameImg;
			int j = 1;
			for (int i = 0; i < tempArray.Length; i++) {
				if (tempArray [i] != 0) {
					csSwitch (tempArray [i]); 
					temp = resultPref.transform.FindChild ("ResultEa4").FindChild ("score" + j).gameObject;
					gameText = temp.transform.FindChild ("gameText").GetComponent<Text> ();
					scoreImg = temp.transform.FindChild ("ScoreImage").GetComponent<Image> ();
					gameImg = temp.transform.FindChild ("gameImage").GetComponent<Image> ();
					gameText.text = GetGameName (i);
					scoreImg.sprite = source [(int)reverse];
					gameImg.sprite = _gameImg [i];
					j++;
				}
			}
		} else if(_countOfGame == 5){
			pop.makeIt (PP.popupIndex.RESULTEA5);
			resultPref = (GameObject)pop.PopupObject;
			GameObject temp;
			Text gameText;
			Image scoreImg;
			Image gameImg;
			int j = 1;
			for (int i = 0; i < tempArray.Length; i++) {
				if (tempArray [i] != 0) {
					csSwitch (tempArray [i]); 
					temp = resultPref.transform.FindChild ("ResultEa5").FindChild ("score" + j).gameObject;
					gameText = temp.transform.FindChild ("gameText").GetComponent<Text> ();
					scoreImg = temp.transform.FindChild ("ScoreImage").GetComponent<Image> ();
					gameImg = temp.transform.FindChild ("gameImage").GetComponent<Image> ();
					gameText.text = GetGameName (i);
					scoreImg.sprite = source [(int)reverse];
					gameImg.sprite = _gameImg [i];
					j++;
				}
			}
		}


		Image _mainScoreImg;
		float tempSum = 0;
		_mainScoreImg=transform.FindChild ("MAIN").FindChild ("MainScore").GetComponent<Image> ();
		for (int i = 0; i < tempArray.Length; i++) {
			tempSum += tempArray [i];
		}
		totalScore = tempSum / _countOfGame;
		csSwitch (totalScore);
		_mainScoreImg.sprite = source [(int)reverse];
		state = ResultState.NONE;
	}
	string GetGameName(int i){
		switch (i) {
		case 0:
			return "주문받기";
		case 1:
			return "원두분쇄";
		case 2:
			return "템핑";
		case 3:
			return "밀킹";
		case 4:
			return "얼음믹서";
		case 5:
			return "베리에이션";
		default :
			return "";
		}
	}
	void OnDestroy(){
		NetworkMng.GetInstance.onSave -= new NetworkMng.OnSave (OnSave);
		NetworkMng.GetInstance.onAllResultFail -= new NetworkMng.OnAllResultFail (OnAllResultFail);
	}
}
