﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using System.Collections.Generic;
public class GameStepManager : Singleton<GameStepManager> {
	



	public int[][]  gameStep = new int[][]{
		new int[]{1,2,3,6,81}, //1
		new int[]{1,2,3,6,81}, //2
		new int[]{1,2,3,6,81}, //3
		new int[]{1,2,3,6,81}, //4 
		new int[]{1,2,3,6,81}, //5
		new int[]{1,2,3,6,81}, // 6
		new int[]{1,2,3,6,81}, // 7
		new int[]{1,2,3,6,81}, // 8
		new int[]{1,2,3,6,81}, // 9
		new int[]{1,2,3,6,81}, //10
		new int[]{1,2,3,6,81}, //11
		new int[]{1,2,3,6,81}, // 12
		new int[]{1,2,3,6,81}, //13
		new int[]{1,2,3,5,81}, //14
		new int[]{1,2,3,5,81}, //15
		new int[]{1,2,3,5,81}, //16

	};

	int getNextSceneNum(int menu,int num)
	{
		for(int i =0 ; i<gameStep[menu].Length; i++)
		{
			if (gameStep [menu] [i] == num && i < gameStep[menu].Length-1) {
				return gameStep [menu] [i + 1];
			}
		}
		return -1;
	}
	public string FirstStart()
	{
		MainUserInfo.GetInstance.gameId = 1;
		return "Lobby";
	}
	public string ResultStart()
	{
		MainUserInfo.GetInstance.gameId = 93;
		return "Result";
	}
	public string MinigameStart()
	{
		MainUserInfo.GetInstance.gameId = 91;
		return "MiniGame";
	}
	public string LearnStart()
	{
		MainUserInfo.GetInstance.gameId = 98;
		return "LEARNPAGE";
	}
	public string ResultPageStart(){
		MainUserInfo.GetInstance.gameId = 99;
		return "ResultPage";
	}
	public string GetNextScene(int gameId)
	{
		if (gameId == 81) {
			if (MainUserInfo.GetInstance.guestTh < GradeDataInfo.GetInstance ().grade_data [MainUserInfo.GetInstance.grade - 1].guest_cnt) {
				MainUserInfo.GetInstance.guestTh++;
				return getSceneName(1);
			} else {
				return getSceneName(93);
			}
		} else if (gameId == 93) {
		//	MainUserInfo.GetInstance.guestTh = 1;
			return getSceneName(1);
		}
		else {
			return getSceneName(getNextSceneNum(MainUserInfo.GetInstance.menu,gameId));
		}

	}
	public string Retry()
	{
		MainUserInfo.GetInstance.guestTh = 1;
		return "Lobby";
	}
	public string RetryLater()
	{
		MainUserInfo.GetInstance.gameId = 0;
		MainUserInfo.GetInstance.guestTh = 1;
		return "Title";
	}
	string getSceneName(int num)
	{
		switch (num) {
	
		case 1:
			MainUserInfo.GetInstance.gameId = 1;
			return "Lobby";
		case 2:
			MainUserInfo.GetInstance.gameId = 2;
			return "Grinding";
		case 81:
			MainUserInfo.GetInstance.gameId = 81;
			return "Serving";
		case 6:
			MainUserInfo.GetInstance.gameId = 6;
			return "4_Variation_proto";
		case 91:
			MainUserInfo.GetInstance.gameId = 91;
			return "MiniGame";
		case 93:
			MainUserInfo.GetInstance.gameId = 93;
			return "Result";
		case 3:
			MainUserInfo.GetInstance.gameId = 3;
			return "Temping";
		case 4:
			MainUserInfo.GetInstance.gameId = 4;
			return "SteamMilk";
		case 5:
			MainUserInfo.GetInstance.gameId = 5;
			return "Icing";
		case 92:
			MainUserInfo.GetInstance.gameId = 92;
			return "LearnCalculation";
		default :
			return "Title"; 
		
		}
	}
}