﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class GradeDataInfo {
	public GradeData[] grade_data= new GradeData[4];
	private GradeDataInfo(){
		for (int i = 0; i < 4; i++) {
			grade_data [i] = new GradeData ();
			switch (i) {
			case 0:
				grade_data [i].grand_up = 70;
				grade_data [i].name = "초보자";
				grade_data [i].teacher_name = "UselessImp/game_character_04";
				grade_data [i].guest_cnt = 2;
				break;
			case 1:
				grade_data [i].grand_up = 78;
				grade_data [i].name = "중급자";
				grade_data [i].teacher_name = "UselessImp/game_character_01";
				grade_data [i].guest_cnt = 2;
				break;
			case 2:
				grade_data [i].grand_up = 83;
				grade_data [i].name = "숙련";
				grade_data [i].teacher_name = "UselessImp/game_character_06";
				grade_data [i].guest_cnt = 2;
				break;
			case 3:
				grade_data [i].grand_up = 87;
				grade_data [i].name = "프로";
				grade_data [i].teacher_name = "UselessImp/game_character_03";
				grade_data [i].guest_cnt = 2;
				break;
			}
		}
	}

	private static GradeDataInfo instance=new GradeDataInfo();

	public static GradeDataInfo GetInstance(){
		
		if (instance  == null) {
			instance = new GradeDataInfo ();
		}
		return instance;
	}
}
