﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ShowRecipe : MonoBehaviour {

	public Text menuTitle;
	public Text recipe;
	public Text goalPoint;
	void Start(){
		menuTitle.text = RecipeList.GetInstance ().recipeList.GetMenuName (MainUserInfo.GetInstance.menu);
		recipe.text = RecipeList.GetInstance ().ingredient.GetIngredientByNum (RecipeList.GetInstance().coffeeBeanRecipe[MainUserInfo.GetInstance.menu][0]);
		goalPoint.text = ""+PlayerPrefs.GetFloat ("GOAL_POINT_F")+"g";
	}
}
