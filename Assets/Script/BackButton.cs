﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour {

	public void buttonBack(){
		SoundManage.instance.OffBgm ();
		SoundManage.instance.efxButtonOn ();
		PlayerPrefs.SetInt ("BACKBUTTON", 1);
		if(GPGSMng.GetInstance.bLogin){
			if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name != "MiniGame"
				&& UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name != "ResultPage") {
				NetworkMng.GetInstance.RequestSaveGameData (
					PlayerPrefs.GetInt ("SD_GAME_ID"),
					PlayerPrefs.GetInt ("SD_TRIAL_TH"),
					PlayerPrefs.GetInt ("SD_GRADE"),
					PlayerPrefs.GetInt ("SD_GUEST_TH")
				);
			}
		}
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Title");
   }
}
